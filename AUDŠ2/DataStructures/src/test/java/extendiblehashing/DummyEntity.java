package extendiblehashing;


import sk.uniza.fri.lustyno.datastructures.extendiblehashing.entities.EHEntity;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.Constants;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;

import java.util.Arrays;

public class DummyEntity extends EHEntity<DummyEntity> {

    private Integer identifier;

    private static final int randomText_LENGTH = 5;
    private String randomText;

    public DummyEntity() {}

    public DummyEntity(Integer identifier) {
        this.identifier = identifier;
    }

    public DummyEntity(Integer identifier, String randomText) {
        this.identifier = identifier;
        this.randomText = randomText.substring(0, 5);
    }

    public String getRandomText() {
        return this.randomText;
    }

    public void setRandomText(String randomText) {
        this.randomText = randomText.substring(0, 5);
    }

    @Override
    public byte[] toByteArray() {
        return ToByteArrayConverter.concatenateByteArrays(
                Arrays.asList(
                        ToByteArrayConverter.bytesFromInt(this.identifier),
                        ToByteArrayConverter.bytesFromString(this.randomText)
                )
        );
    }

    @Override
    public DummyEntity fromByteArray(byte[] fromBytes) {
        int id = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(fromBytes, 0, 4));
        String randomText = FromByteArrayConverter.stringFromBytes(Arrays.copyOfRange(fromBytes, 4, 9));
        return new DummyEntity(id, randomText);
    }

    @Override
    public int getSize() {
        return
                Constants.JAVA_INTEGER_SIZE
                        + randomText_LENGTH;

    }

    @Override
    public int getHash() {
        return this.identifier;
    }

    @Override
    public boolean equalsOtherEntity(DummyEntity other) {
        return this.identifier == other.getIdentifier();
    }

    public int getIdentifier() {
        return this.identifier;
    }

    @Override
    public DummyEntity clone() {
        int copiedId = this.identifier;
        String copiedRandomText = this.randomText;
        return new DummyEntity(copiedId, copiedRandomText);
    }

    @Override
    public String toString() {
        return (this.isValid ? "VALID " : "INVALID ") + this.identifier + " " + this.randomText;
    }

    @Override
    public boolean equals(Object o) {
        if (((DummyEntity)o).getIdentifier() == this.identifier) {
            return true;
        }
        return false;
    }


}
