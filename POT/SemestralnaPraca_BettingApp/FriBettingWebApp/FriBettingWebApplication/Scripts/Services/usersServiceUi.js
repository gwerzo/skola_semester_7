﻿/**
 * JS metódy pre integráciu na API poskytovanú controllerom UsersController.cs
 */
function registerUser(registrationCallback) {
    $(".form-box").removeClass("form-successfully-submitted");
    $(".form-box").removeClass("form-unsuccessfully-submitted");
    $(".form-box").addClass("breathing");
    var username = $("#username").val();
    var password = $("#password").val();
    var passwordAgain = $("#passwordagain").val();

    $.ajax({
        'url': "/api/users/register",
        'method': "POST",
        'data': JSON.stringify({
            "Username": username,
            "Password": password,
            "PasswordAgain": passwordAgain
        }),
        'contentType': "application/json",
        'dataType': "json",
        'success': function (data) {
            if (data["Successful"]) {
                $(".form-box").removeClass("breathing");
                $(".form-error-container").addClass("hidden");
                $(".form-box").addClass("form-successfully-submitted");
                window.setTimeout(function() {
                        window.location = "./login";
                    },
                    3000);
            } else {
                $(".form-box").removeClass("breathing");
                $(".form-box").addClass("form-unsuccessfully-submitted");
                $(".form-error-message").text(data["ErrorMessage"]);
                $(".form-error-container").removeClass("hidden");
            }
        }
    });
}

function getAccountDetails(callback) {
    $.ajax({
        url: '/api/users/detail',
        headers: {
            'Authorization': localStorage.getItem("fri-betting-app-token")
        },
        method: 'GET',
        success: function (data) {
            callback(data);
        },
        error: function (jqXHR, textStatus, err) {
            $.notify("Error loading account details");
        }
    });
}

function loginUser(loginCallback) {
    $(".form-box").removeClass("form-successfully-submitted");
    $(".form-box").removeClass("form-unsuccessfully-submitted");
    $(".form-box").addClass("breathing");
    var username = $("#username").val();
    var password = $("#password").val();  


    $.ajax({
        'url': "/api/users/login",
        'method': "POST",
        'data': JSON.stringify({
            "Username": username,
            "Password": password
        }),
        'contentType': "application/json",
        'dataType': "json",
        'success': function (data) {
            if (data["Successful"]) {
                $(".form-box").removeClass("breathing");
                $(".form-error-container").addClass("hidden");
                $(".form-box").addClass("form-successfully-submitted");
                localStorage.setItem("fri-betting-app-token", data["SuccessMessage"]);
                window.setTimeout(function () {
                        window.location = "/";
                    },
                    3000);
            } else {
                $(".form-box").removeClass("breathing");
                $(".form-box").addClass("form-unsuccessfully-submitted");
                $(".form-error-message").text(data["ErrorMessage"]);
                $(".form-error-container").removeClass("hidden");
            }
        }
    });
}

function logoutUser() {
    localStorage.removeItem("fri-betting-app-token");
    window.location = "/";
}

// Autorun/Self invoking metóda, ktorá reautentifikuje používateľa a podľa výsledku reautentifikácie vyrenderuje menu (každý nový MVC request)
$(document).ready(function () {
    $.ajax({
        url: '/api/users',
        headers: {
            'Authorization': localStorage.getItem("fri-betting-app-token")
        },
        method: 'GET',
        success: function (data) {
            $(".logged-in-menu-item").show();
        },
        error: function (jqXHR, textStatus, err) {
            $(".not-logged-in-menu-item").show();
        }
    });
});