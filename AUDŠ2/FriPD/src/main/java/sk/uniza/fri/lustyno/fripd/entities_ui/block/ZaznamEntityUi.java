package sk.uniza.fri.lustyno.fripd.entities_ui.block;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ZaznamEntityUi {

    private String kluc;
    private String hodnota;
    private String validny;

}
