package sk.uniza.fri.lustyno.fripd.components.common;

import sk.uniza.fri.lustyno.fripd.components.vehicles.*;

public class MenuComponent extends AbstractMenu {

    public MenuComponent(RepaintableComponent component) {
        this.menuItems.add(new PridanieNovehoVozidlaForm(component));
        this.menuItems.add(new ZmenaUdajovOVozidleForm(component));
        this.menuItems.add(new VyhladanieVozidlaPodlaVINForm(component));
        this.menuItems.add(new VyhladanieVozidlaPodlaEVCForm(component));
        this.menuItems.add(new VyradenieVozidlaPodlaVINForm(component));
        this.menuItems.add(new VyradenieVozidlaPodlaEVCForm(component));
        initMenuItems();
    }

}
