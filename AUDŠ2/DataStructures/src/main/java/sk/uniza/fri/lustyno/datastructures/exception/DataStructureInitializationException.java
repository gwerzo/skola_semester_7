package sk.uniza.fri.lustyno.datastructures.exception;

public class DataStructureInitializationException extends RuntimeException {

    public DataStructureInitializationException(String ofClass, String data) {
        super("Nepodarilo sa nainicializovať štruktúru " + ofClass + "\n" + data);
    }

}
