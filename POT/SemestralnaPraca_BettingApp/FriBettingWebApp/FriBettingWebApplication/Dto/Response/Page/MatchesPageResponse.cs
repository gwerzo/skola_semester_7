﻿using System.Collections.Generic;

namespace FriBettingWebApplication.Dto.Response.Page
{
    public class MatchesPageResponse
    {

        public int Count { get; set; }
        public List<MatchResponse> Matches { get; set; }

    }
}