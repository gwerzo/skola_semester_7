package sk.uniza.fri.lustiak.auds2.kataster.components.common;

import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustiak.auds2.kataster.components.kataster.DatabaseInfoForm;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;

public class RepaintableComponent extends VerticalLayout {

    private RealEstateService realEstateService;

    private Component actualComponent;

    private DatabaseInfoForm dbInfoForm;

    public RepaintableComponent(Component component) {
        this.realEstateService = UIServices.getRealEstateService();
        this.dbInfoForm = new DatabaseInfoForm(this.realEstateService.getDatabaseInfo());

        this.addComponent(this.dbInfoForm);

        this.actualComponent = component;
        this.setSizeFull();
        addComponent(actualComponent);
    }

    /*
     * Vyhodí z komponentu všetky componenty a pridá len ten, ktorý prišiel v parametri
     */
    public void showComponent(Component component) {
        this.removeAllComponents();
        addComponent(
                new VerticalLayout(
                    this.dbInfoForm, component));
    }

    public void updateFileState() {
        this.dbInfoForm.initFrom(this.realEstateService.getDatabaseInfo());
    }
}
