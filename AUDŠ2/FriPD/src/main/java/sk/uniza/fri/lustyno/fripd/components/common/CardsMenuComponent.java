package sk.uniza.fri.lustyno.fripd.components.common;

import sk.uniza.fri.lustyno.fripd.components.licences.OdstraneniePreukazuForm;
import sk.uniza.fri.lustyno.fripd.components.licences.PridanieNovehoPreukazuForm;
import sk.uniza.fri.lustyno.fripd.components.licences.VyhladaniePreukazuForm;
import sk.uniza.fri.lustyno.fripd.components.licences.ZmenaUdajovPreukazuForm;

public class CardsMenuComponent extends AbstractMenu {

    public CardsMenuComponent(RepaintableComponent component) {
        this.menuItems.add(new PridanieNovehoPreukazuForm(component));
        this.menuItems.add(new VyhladaniePreukazuForm(component));
        this.menuItems.add(new ZmenaUdajovPreukazuForm(component));
        this.menuItems.add(new OdstraneniePreukazuForm(component));
        initMenuItems();
    }

}
