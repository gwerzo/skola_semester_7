package sk.uniza.fri.lustyno.AudsLetisko.persistance;

import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.uniza.fri.lustyno.AudsLetisko.BusinessException;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.HistoriaLietadlaNaLetiskuEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.HistoriaOdletuEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.HistoriaOdletuUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.persistance.utils.OdletImportHelpEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.HistoryService;
import sk.uniza.fri.lustyno.datastructures.entity.keys.DateKey;
import sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTreeNode;
import sk.uniza.fri.lustyno.datastructures.tree.splay.SplayTree;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;
import java.util.List;

@Service
public class HistoriaFileManager {

    private final String ODLETY_FILE =      "fileResources/odlety.csv";

    private final String PRILETY_FILE =     "fileResources/prilety.csv";

    private final String POZIADANIA_FILE =  "fileResources/poziadania.csv";

    @Autowired
    private HistoryService historyService;

    public void vycistiSubory() {

        try {
            PrintWriter writer = new PrintWriter(ODLETY_FILE);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            PrintWriter writer = new PrintWriter(PRILETY_FILE);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            PrintWriter writer = new PrintWriter(POZIADANIA_FILE);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void ulozUdajeOOdletochDoSuboru() {
        List<Pair<Integer, SplayTree<DateKey, HistoriaOdletuEntity>>> odletyZDrah =
                historyService.getHistorickeOdletyZDrah();
        if (odletyZDrah == null || odletyZDrah.size() == 0) {
            return;
        }

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(ODLETY_FILE));

            for (Pair<Integer, SplayTree<DateKey, HistoriaOdletuEntity>> draha : odletyZDrah) {
                SplayTree<DateKey, HistoriaOdletuEntity> odletyZDrahy = draha.getValue();

                List<BinarySearchTreeNode<DateKey, HistoriaOdletuEntity>> odletyZJednotlivejDrahy = odletyZDrahy.levelOrder();

                if (odletyZJednotlivejDrahy == null || odletyZJednotlivejDrahy.size() == 0) {
                    continue;
                }

                for (BinarySearchTreeNode<DateKey, HistoriaOdletuEntity> odlet : odletyZJednotlivejDrahy) {
                    StringBuilder sb = new StringBuilder();

                    sb.append(odlet.getData().getOdletovaDraha().getId() + ",");
                    sb.append(odlet.getData().getLietadloEntity().getMedzinarodnyKod() + ",");
                    sb.append(odlet.getKey().getValue().toString() + ",");
                    sb.append(odlet.getData().getCasDatumOdletu().toString() + ",");
                    sb.append(odlet.getData().getCasDatumPriletu().toString());

                    bw.write(sb.toString());
                    bw.newLine();
                }

            }

            bw.flush();
        } catch (IOException ioEx) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na zápis odletov");
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void nacitajUdajeOOdletochZoSuboru() {

        List<OdletImportHelpEntity> odletyZoSuboru = new LinkedList<>();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(ODLETY_FILE));

            String line = null;
            while ((line = br.readLine()) != null) {

                String[] splitted = line.split(",", -1);

                Integer idDrahy = Integer.parseInt(splitted[0]);
                String kodLietadla = splitted[1];
                String klucDoStromuString = splitted[2];
                String casDatumOdletuString = splitted[3];
                String casDatumPriletuString = splitted[4];

                LocalDateTime klucDoStromu = null;
                LocalDateTime casDatumOdletu = null;
                LocalDateTime casDatumPriletu = null;

                if (klucDoStromuString != null && !klucDoStromuString.isEmpty()) {
                    try {
                        klucDoStromu = LocalDateTime.parse(klucDoStromuString);
                    } catch (DateTimeParseException ex) {
                        klucDoStromu = null;
                    }
                }

                if (casDatumOdletuString != null && !casDatumOdletuString.isEmpty()) {
                    try {
                        casDatumOdletu = LocalDateTime.parse(casDatumOdletuString);
                    } catch (DateTimeParseException ex) {
                        casDatumOdletu = null;
                    }
                }

                if (casDatumPriletuString != null && !casDatumPriletuString.isEmpty()) {
                    try {
                        casDatumPriletu = LocalDateTime.parse(casDatumPriletuString);
                    } catch (DateTimeParseException ex) {
                        casDatumPriletu = null;
                    }
                }

                OdletImportHelpEntity helpEntity = new OdletImportHelpEntity();
                helpEntity.setIdDrahy(idDrahy);
                helpEntity.setKodLietadla(kodLietadla);
                helpEntity.setKeyDoZoznamu(klucDoStromu);
                helpEntity.setDatumOdletu(casDatumOdletu);
                helpEntity.setDatumPriletu(casDatumPriletu);

                odletyZoSuboru.add(helpEntity);
            }
        } catch (FileNotFoundException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o odletoch");
        } catch (IOException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o odletoch");
        }
        this.historyService.nastavDataOOdletochZoSuboru(odletyZoSuboru);

    }

    public void ulozUdajeOPriletochAPoziadaniachDoSuboru() {

    }

    public void nacitajUdajeOPriletochAPoziadaniachZoSuboru() {

    }

}
