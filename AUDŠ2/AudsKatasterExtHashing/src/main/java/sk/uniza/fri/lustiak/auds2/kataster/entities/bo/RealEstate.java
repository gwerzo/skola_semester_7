package sk.uniza.fri.lustiak.auds2.kataster.entities.bo;

import lombok.Data;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sk.uniza.fri.lustyno.datastructures.extendiblehashing.Constants;
import sk.uniza.fri.lustyno.datastructures.extendiblehashing.entities.EHEntity;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.StringUtils;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

@Data
@NoArgsConstructor
@Slf4j
public class RealEstate extends EHEntity<RealEstate> {

    private Integer identifikacneCislo;
    private Integer supisneCislo;

    public final int POPIS_MAX_DLZKA = 20;

    private String popis;

    private Double longtitude1;
    private Boolean isNorth1;
    private Double latitude1;
    private Boolean isEast1;

    private Double longtitude2;
    private Boolean isNorth2;
    private Double latitude2;
    private Boolean isEast2;

    public RealEstate(Integer identifikacneCislo, Integer supisneCislo, String popis,
                      Double longtitude1, Boolean isNorth1, Double latitude1, Boolean isEast1,
                      Double longtitude2, Boolean isNorth2, Double latitude2, Boolean isEast2) {
        this.identifikacneCislo     = identifikacneCislo;
        this.supisneCislo           = supisneCislo;
        this.popis                  = StringUtils.padRightSpaces(StringUtils.safeSubstring(popis, this.POPIS_MAX_DLZKA), this.POPIS_MAX_DLZKA);

        this.longtitude1    = longtitude1;
        this.isNorth1       = isNorth1;
        this.latitude1      = latitude1;
        this.isEast1        = isEast1;

        this.longtitude2    = longtitude2;
        this.isNorth2       = isNorth2;
        this.latitude2      = latitude2;
        this.isEast2        = isEast2;
    }

    public RealEstate(Integer identifikacneCislo) {
        this.identifikacneCislo = identifikacneCislo;
    }

    @Override
    public byte[] toByteArray() {
        byte[] bytes = new byte[0];
        try {
            bytes = ToByteArrayConverter.concatenateByteArrays(
                    Arrays.asList(
                            ToByteArrayConverter.bytesFromInt(this.identifikacneCislo),
                            ToByteArrayConverter.bytesFromInt(this.supisneCislo),
                            ToByteArrayConverter.bytesFromString(StringUtils.padRightSpaces(StringUtils.safeSubstring(this.popis, this.POPIS_MAX_DLZKA), this.POPIS_MAX_DLZKA), "WINDOWS-1250"),
                            ToByteArrayConverter.bytesFromDouble(this.longtitude1),
                            ToByteArrayConverter.bytesFromBoolean(this.isNorth1),
                            ToByteArrayConverter.bytesFromDouble(this.latitude1),
                            ToByteArrayConverter.bytesFromBoolean(this.isEast1),
                            ToByteArrayConverter.bytesFromDouble(this.longtitude2),
                            ToByteArrayConverter.bytesFromBoolean(this.isNorth2),
                            ToByteArrayConverter.bytesFromDouble(this.latitude2),
                            ToByteArrayConverter.bytesFromBoolean(this.isEast2)));
        } catch (UnsupportedEncodingException e) {
            log.error("Neporadilo sa SERIALIZOVAŤ entitu triedy {}", this.getClass().getName());
        }
        return bytes;
    }

    @Override
    public RealEstate fromByteArray(byte[] bytes) {
        Integer id = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(bytes, 0, 4));
        Integer supisneC = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(bytes, 4, 8));

        String popis = null;
        try {
            popis = FromByteArrayConverter.stringFromBytes(Arrays.copyOfRange(bytes, 8, 28), "WINDOWS-1250");
        } catch (UnsupportedEncodingException e) {
            log.error("Nepodarilo sa DESERIALIZOVAŤ entitu triedy {}", this.getClass().getName());
        }

        Double longit1 = FromByteArrayConverter.doubleFromBytes(Arrays.copyOfRange(bytes, 28, 36));
        Boolean isNorth1 = FromByteArrayConverter.booleanFromByte(bytes[36]);
        Double latit1 = FromByteArrayConverter.doubleFromBytes(Arrays.copyOfRange(bytes, 37, 45));
        Boolean isEast1 = FromByteArrayConverter.booleanFromByte(bytes[45]);

        Double longit2 = FromByteArrayConverter.doubleFromBytes(Arrays.copyOfRange(bytes, 46, 54));
        Boolean isNorth2 = FromByteArrayConverter.booleanFromByte(bytes[54]);
        Double latit2 = FromByteArrayConverter.doubleFromBytes(Arrays.copyOfRange(bytes, 55, 63));
        Boolean isEast2 = FromByteArrayConverter.booleanFromByte(bytes[63]);


        return new RealEstate(id, supisneC, popis, longit1, isNorth1, latit1, isEast1, longit2, isNorth2, latit2, isEast2);
    }

    @Override
    public int getSize() {
        return
                Constants.JAVA_INTEGER_SIZE// ID
               +Constants.JAVA_INTEGER_SIZE// SUPISNE Č.
               +this.POPIS_MAX_DLZKA // POPIS (20 znakov), WINDOWS-1250 encoding
               +4*Constants.JAVA_DOUBLE_SIZE // LATITUDE 2x, LONGITUDE 2x
               +4*Constants.JAVA_BOOLEAN_SIZE; // NORTH? 2x, EAST? 2x
    }

    @Override
    public int getHash() {
        return this.identifikacneCislo;
    }

    @Override
    public boolean equalsOtherEntity(RealEstate realEstate) {
        return this.identifikacneCislo.equals(realEstate.getIdentifikacneCislo());
    }

    @Override
    public RealEstate clone() {
        return new RealEstate(this.identifikacneCislo, this.supisneCislo, this.popis,
                this.longtitude1, this.isNorth1, this.latitude1, this.isEast1,
                this.longtitude2, this.isNorth2, this.latitude2, this.isEast2);
    }
}
