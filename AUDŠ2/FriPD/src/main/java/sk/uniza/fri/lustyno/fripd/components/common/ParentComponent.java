package sk.uniza.fri.lustyno.fripd.components.common;

public interface ParentComponent {

    void hideComponent();

    void showComponent();

    String getComponentTitle();

}
