package extendiblehashing;

import org.junit.jupiter.api.*;
import sk.uniza.fri.lustyno.datastructures.extendiblehashing.EHBlockDescription;
import sk.uniza.fri.lustyno.datastructures.extendiblehashing.EHFileConfig;
import sk.uniza.fri.lustyno.datastructures.extendiblehashing.EHPrototypeManager;
import sk.uniza.fri.lustyno.datastructures.extendiblehashing.EHFile;
import sk.uniza.fri.lustyno.datastructures.utils.BitSetUtils;
import sk.uniza.fri.lustyno.datastructures.utils.CustomBitSet;
import sk.uniza.fri.lustyno.datastructures.utils.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ExtendibleHashingBasicTest {

    @Test
    @Order(1)
    public void testWithDeterministicValues() {

        EHPrototypeManager.registerNewPrototype(new DummyEntity(-1, "Blabla"));

        EHFile<DummyEntity> extHashing = new EHFile<>(DummyEntity.class, "dummy_entities", 2, 5);

        DummyEntity dummyEntity0 = new DummyEntity(0, "00000");
        DummyEntity dummyEntity2 = new DummyEntity(2, "22222");
        DummyEntity dummyEntity4 = new DummyEntity(4, "44444");
        DummyEntity dummyEntity6 = new DummyEntity(6, "66666");
        DummyEntity dummyEntity8 = new DummyEntity(8, "88888");
        DummyEntity dummyEntity10 = new DummyEntity(10, "101010");

        // Spôsobia local-expansion 3x, až kým sa nevykoná jedna globálna expanzia
        DummyEntity dummyEntity7 = new DummyEntity(7, "77777");
        DummyEntity dummyEntity15 = new DummyEntity(15, "15151");
        DummyEntity dummyEntity31 = new DummyEntity(31, "31313");

        Assertions.assertTrue(extHashing.insert(dummyEntity0));
        Assertions.assertTrue(extHashing.insert(dummyEntity2));
        Assertions.assertTrue(extHashing.insert(dummyEntity4));
        Assertions.assertTrue(extHashing.insert(dummyEntity6));
        Assertions.assertTrue(extHashing.insert(dummyEntity8));
        Assertions.assertTrue(extHashing.insert(dummyEntity10));
        Assertions.assertTrue(extHashing.insert(dummyEntity7));
        Assertions.assertTrue(extHashing.insert(dummyEntity15));
        Assertions.assertTrue(extHashing.insert(dummyEntity31));

        // Pridané kontroly, či sa poradí/nepodarí vložiť znova tie isté entity (nemôžu sa)
        Assertions.assertFalse(extHashing.insert(dummyEntity0));
        Assertions.assertFalse(extHashing.insert(dummyEntity2));
        Assertions.assertFalse(extHashing.insert(dummyEntity4));
        Assertions.assertFalse(extHashing.insert(dummyEntity6));
        Assertions.assertFalse(extHashing.insert(dummyEntity8));
        Assertions.assertFalse(extHashing.insert(dummyEntity10));
        Assertions.assertFalse(extHashing.insert(dummyEntity7));
        Assertions.assertFalse(extHashing.insert(dummyEntity15));
        Assertions.assertFalse(extHashing.insert(dummyEntity31));

        Assertions.assertNotNull(extHashing.find(dummyEntity0));
        Assertions.assertNotNull(extHashing.find(dummyEntity2));
        Assertions.assertNotNull(extHashing.find(dummyEntity4));
        Assertions.assertNotNull(extHashing.find(dummyEntity6));
        Assertions.assertNotNull(extHashing.find(dummyEntity8));
        Assertions.assertNotNull(extHashing.find(dummyEntity10));
        Assertions.assertNotNull(extHashing.find(dummyEntity7));
        Assertions.assertNotNull(extHashing.find(dummyEntity15));
        Assertions.assertNotNull(extHashing.find(dummyEntity31));

        // Vymazanie všetkých entít s overením, či boli reálne zmazané (nie sú null po vrátení)
        Assertions.assertNotNull(extHashing.delete(dummyEntity0));
        Assertions.assertNotNull(extHashing.delete(dummyEntity2));
        Assertions.assertNotNull(extHashing.delete(dummyEntity4));
        Assertions.assertNotNull(extHashing.delete(dummyEntity6));
        Assertions.assertNotNull(extHashing.delete(dummyEntity8));
        Assertions.assertNotNull(extHashing.delete(dummyEntity10));
        Assertions.assertNotNull(extHashing.delete(dummyEntity7));
        Assertions.assertNotNull(extHashing.delete(dummyEntity15));
        Assertions.assertNotNull(extHashing.delete(dummyEntity31));

        // Overenie, či sa tieto entity nenachádzajú stále v EH (find nad nimi musí vrátiť null)
        Assertions.assertNull(extHashing.find(dummyEntity0));
        Assertions.assertNull(extHashing.find(dummyEntity2));
        Assertions.assertNull(extHashing.find(dummyEntity4));
        Assertions.assertNull(extHashing.find(dummyEntity6));
        Assertions.assertNull(extHashing.find(dummyEntity8));
        Assertions.assertNull(extHashing.find(dummyEntity10));
        Assertions.assertNull(extHashing.find(dummyEntity7));
        Assertions.assertNull(extHashing.find(dummyEntity15));
        Assertions.assertNull(extHashing.find(dummyEntity31));

        extHashing.close();

    }

    @Test
    @Order(2)
    public void testDeletionsWithOverflowBlocks() {
        EHPrototypeManager.registerNewPrototype(new DummyEntity(-1, "Blabla"));

        int blockSize = 14; // 10 bytov entita, 4 bytov popis bloku
        int overflowBlockSize = 14; // 10 bytov entita, 4 byty popis overflow bloku

        // ExtHashing má blokovací faktor pre oba bloky 1 a adresovanie s 3 bitmi
        EHFile<DummyEntity> extHashing = new EHFile<>(DummyEntity.class, "dummy_entities", 1, 1, 3);

        DummyEntity dummyEntity0 = new DummyEntity(0, "00000");
        DummyEntity dummyEntity1 = new DummyEntity(1, "11111");
        DummyEntity dummyEntity2 = new DummyEntity(2, "22222");
        DummyEntity dummyEntity3 = new DummyEntity(3, "33333");
        DummyEntity dummyEntity4 = new DummyEntity(4, "44444");
        DummyEntity dummyEntity5 = new DummyEntity(5, "55555");
        DummyEntity dummyEntity6 = new DummyEntity(6, "66666");
        DummyEntity dummyEntity7 = new DummyEntity(7, "77777");

        List<DummyEntity> ents = Arrays.asList(dummyEntity0, dummyEntity1, dummyEntity2, dummyEntity3,
                dummyEntity4, dummyEntity5, dummyEntity6, dummyEntity7);
        ents.forEach(extHashing::insert);


        Assertions.assertEquals(blockSize * ents.size(), extHashing.getMainFileSizeByCalculation());
        Assertions.assertEquals(blockSize * ents.size(), extHashing.getMainFileSizeByFileLength());
        Assertions.assertEquals(0, extHashing.getOverflowFileSizeByCalculation());
        Assertions.assertEquals(0, extHashing.getOverflowFileSizeByFileLength());

        // Teraz je insertnutých 8 entít v 8 blokoch, kde v každom je práve jedna entita

        DummyEntity dummyEntity8 = new DummyEntity(8, "88888");
        DummyEntity dummyEntity16 = new DummyEntity(16, "16161");
        DummyEntity dummyEntity32 = new DummyEntity(32, "32323");
        DummyEntity dummyEntity64 = new DummyEntity(64, "64646");
        DummyEntity dummyEntity128 = new DummyEntity(128, "12812");

        List<DummyEntity> entsAdditional = Arrays.asList(dummyEntity8, dummyEntity16, dummyEntity32, dummyEntity64, dummyEntity128);
        entsAdditional.forEach(extHashing::insert);

        Assertions.assertEquals(blockSize * ents.size(), extHashing.getMainFileSizeByCalculation());
        Assertions.assertEquals(blockSize * ents.size(), extHashing.getMainFileSizeByFileLength());
        Assertions.assertEquals(entsAdditional.size() * overflowBlockSize, extHashing.getOverflowFileSizeByCalculation());
        Assertions.assertEquals(entsAdditional.size() * overflowBlockSize, extHashing.getOverflowFileSizeByFileLength());

        // Vymazaná entita z konce "reťaze" overflow blokov
        extHashing.delete(dummyEntity128);
        Assertions.assertNotNull(extHashing.find(dummyEntity8));
        Assertions.assertNotNull(extHashing.find(dummyEntity16));
        Assertions.assertNotNull(extHashing.find(dummyEntity32));
        Assertions.assertNotNull(extHashing.find(dummyEntity64));
        Assertions.assertNull(extHashing.find(dummyEntity128)); // Vymazaná

        // Tu sa MALA znížiť aj veľkosť overflow súboru, lebo sa uvolnil kvázi "najvyšší" blok
        Assertions.assertEquals((entsAdditional.size() - 1) * overflowBlockSize, extHashing.getOverflowFileSizeByCalculation());
        Assertions.assertEquals((entsAdditional.size() - 1) * overflowBlockSize, extHashing.getOverflowFileSizeByFileLength());

        // Vymazanie entity zo "stredu" reťaze
        extHashing.delete(dummyEntity32);
        Assertions.assertNotNull(extHashing.find(dummyEntity0));
        Assertions.assertNotNull(extHashing.find(dummyEntity8));
        Assertions.assertNotNull(extHashing.find(dummyEntity16));
        Assertions.assertNull(extHashing.find(dummyEntity32)); // Vymazaná
        Assertions.assertNotNull(extHashing.find(dummyEntity64));
        Assertions.assertNull(extHashing.find(dummyEntity128)); // Vymazaná

        // Tu sa NEMALA znížiť veľkosť súboru, lebo bol uvoľnený len "stredný" overflow blok
        Assertions.assertEquals((entsAdditional.size() - 1) * overflowBlockSize, extHashing.getOverflowFileSizeByCalculation());
        Assertions.assertEquals((entsAdditional.size() - 1) * overflowBlockSize, extHashing.getOverflowFileSizeByFileLength());

        // Vymaže entitu 0 (má sa vykonať konsolidácia/strasenie hlavného bloku)
        extHashing.delete(dummyEntity0);
        Assertions.assertNull(extHashing.find(dummyEntity0)); // Vymazaná
        Assertions.assertNotNull(extHashing.find(dummyEntity8));
        Assertions.assertNotNull(extHashing.find(dummyEntity16));
        Assertions.assertNull(extHashing.find(dummyEntity32)); // Vymazaná
        Assertions.assertNotNull(extHashing.find(dummyEntity64));
        Assertions.assertNull(extHashing.find(dummyEntity128)); // Vymazaná

        Assertions.assertEquals((entsAdditional.size() - 3) * overflowBlockSize, extHashing.getOverflowFileSizeByCalculation());
        Assertions.assertEquals((entsAdditional.size() - 3) * overflowBlockSize, extHashing.getOverflowFileSizeByFileLength());
    }

    @Test
    @Order(3)
    public void testBlocksMerginAfterDeleting() {
        EHPrototypeManager.registerNewPrototype(new DummyEntity(-1, "Blabla"));

        // ExtHashing má blokovací faktor pre oba bloky 1 a adresovanie s 3 bitmi
        EHFile<DummyEntity> extHashing = new EHFile<>(DummyEntity.class, "dummy_entities", 2, 1, 3);

        DummyEntity dummyEntity0 = new DummyEntity(0, "00000");
        DummyEntity dummyEntity1 = new DummyEntity(1, "11111");
        DummyEntity dummyEntity2 = new DummyEntity(2, "22222");
        DummyEntity dummyEntity3 = new DummyEntity(3, "33333");
        DummyEntity dummyEntity4 = new DummyEntity(4, "44444");
        DummyEntity dummyEntity5 = new DummyEntity(5, "55555");
        DummyEntity dummyEntity6 = new DummyEntity(6, "66666");
        DummyEntity dummyEntity7 = new DummyEntity(7, "77777");
        DummyEntity dummyEntity8 = new DummyEntity(8, "88888");
        DummyEntity dummyEntity9 = new DummyEntity(9, "99999");
        DummyEntity dummyEntity10 = new DummyEntity(10, "10101");
        DummyEntity dummyEntity11 = new DummyEntity(11, "11-11");
        DummyEntity dummyEntity12 = new DummyEntity(12, "12-12");
        DummyEntity dummyEntity13 = new DummyEntity(13, "13-13");
        DummyEntity dummyEntity14 = new DummyEntity(14, "14-14");
        DummyEntity dummyEntity15 = new DummyEntity(15, "15-15");

        List<DummyEntity> ents = Arrays.asList(dummyEntity0, dummyEntity1, dummyEntity2, dummyEntity3,
                dummyEntity4, dummyEntity5, dummyEntity6, dummyEntity7, dummyEntity8, dummyEntity8, dummyEntity9,
                dummyEntity10, dummyEntity11, dummyEntity12, dummyEntity13, dummyEntity14, dummyEntity15);
        for (int i = 0; i < ents.size(); i++) {
            extHashing.insert(ents.get(i));
        }

        extHashing.delete(dummyEntity0);

        Assertions.assertNotNull(extHashing.find(dummyEntity1));
        Assertions.assertNotNull(extHashing.find(dummyEntity2));
        Assertions.assertNotNull(extHashing.find(dummyEntity3));
        Assertions.assertNotNull(extHashing.find(dummyEntity4));
        Assertions.assertNotNull(extHashing.find(dummyEntity5));
        Assertions.assertNotNull(extHashing.find(dummyEntity6));
        Assertions.assertNotNull(extHashing.find(dummyEntity7));
        Assertions.assertNotNull(extHashing.find(dummyEntity8));
        Assertions.assertNotNull(extHashing.find(dummyEntity9));
        Assertions.assertNotNull(extHashing.find(dummyEntity10));
        Assertions.assertNotNull(extHashing.find(dummyEntity11));
        Assertions.assertNotNull(extHashing.find(dummyEntity12));
        Assertions.assertNotNull(extHashing.find(dummyEntity13));
        Assertions.assertNotNull(extHashing.find(dummyEntity14));
        Assertions.assertNotNull(extHashing.find(dummyEntity15));

        List<EHBlockDescription> blockDescriptions = extHashing.getBlockDescriptions();

        List<EHBlockDescription> overflowBlockDescriptions = extHashing.getOverflowBlockDescriptions();

    }

    @Test
    @Order(4)
    public void testDirectoryHalving() {
        EHPrototypeManager.registerNewPrototype(new DummyEntity(-1, "Blabla"));

        // ExtHashing má blokovací faktor pre oba bloky 1 a adresovanie s 3 bitmi
        EHFile<DummyEntity> extHashing = new EHFile<>(DummyEntity.class, "dummy_entities", 1, 1, 3);

        DummyEntity dummyEntity0 = new DummyEntity(0, "00000");
        DummyEntity dummyEntity1 = new DummyEntity(1, "11111");
        DummyEntity dummyEntity2 = new DummyEntity(2, "22222");
        DummyEntity dummyEntity3 = new DummyEntity(3, "33333");
        DummyEntity dummyEntity4 = new DummyEntity(4, "44444");
        DummyEntity dummyEntity5 = new DummyEntity(5, "55555");
        DummyEntity dummyEntity6 = new DummyEntity(6, "66666");
        DummyEntity dummyEntity7 = new DummyEntity(7, "77777");

        List<DummyEntity> ents = Arrays.asList(dummyEntity0, dummyEntity1, dummyEntity2, dummyEntity3,
                dummyEntity4, dummyEntity5, dummyEntity6, dummyEntity7);
        for (int i = 0; i < ents.size(); i++) {
            extHashing.insert(ents.get(i));
        }

        extHashing.delete(dummyEntity0);
        extHashing.delete(dummyEntity2);
        extHashing.delete(dummyEntity1);
        extHashing.delete(dummyEntity3);

        Assertions.assertNull(extHashing.find(dummyEntity0));
        Assertions.assertNull(extHashing.find(dummyEntity1));
        Assertions.assertNull(extHashing.find(dummyEntity2));
        Assertions.assertNull(extHashing.find(dummyEntity3));
        Assertions.assertNotNull(extHashing.find(dummyEntity4));
        Assertions.assertNotNull(extHashing.find(dummyEntity5));
        Assertions.assertNotNull(extHashing.find(dummyEntity6));
        Assertions.assertNotNull(extHashing.find(dummyEntity7));

    }

    @Test
    @Order(5)
    public void failingSeed() {
        EHPrototypeManager.registerNewPrototype(new DummyEntity(-1, "Blabla"));

        EHFile<DummyEntity> extHashing = new EHFile<>(DummyEntity.class, "dummy_entities", 2, 1, 3);
        List<DummyEntity> dummyEntities = new ArrayList<>();
        List<Integer> usedIds = new ArrayList<>();
        Random rnd = new Random(2);

        for (int i = 0; i < 8; i++) {
            Integer id = null;
            while (id == null || id < 10000) { // < 10000 lebo sa ID dáva aj ako stringová dátová časť, ktorá musí mať 5 znakov
                int c = rnd.nextInt(Integer.MAX_VALUE);
                if (usedIds.contains(c)) {
                    continue;
                }
                usedIds.add(c);
                id = c;
            }
            DummyEntity dummyEntity = new DummyEntity(id, "" + id);

            // Môže sa stať, že sa entita nakoniec nepridá do EH (pri už vloženom ID) ale vloží sa do paralelnej štrukt.
            // To ale nevadí, len sa nakoniec podľa paralelnej štruktúry 2x vyhľadá
            // Ale to nič nemení na fakte, že insert mohol vrátiť false (nepodarilo sa vložiť entitu), teda sa tam aj tak bude nachádzať len 1x
            dummyEntities.add(dummyEntity);
            extHashing.insert(dummyEntity);
        }

        for (int i = 0; i < 3; i++) {
            int ind = rnd.nextInt(dummyEntities.size());
            DummyEntity c = dummyEntities.remove(ind);
            extHashing.delete(c);
        }

        for (DummyEntity findEnt : dummyEntities) {
            DummyEntity found = extHashing.find(findEnt);
            Assertions.assertNotNull(found);
            Assertions.assertEquals(found.getRandomText(), findEnt.getRandomText());
        }
    }

    @Test
    @Order(6)
    public void testWithRandomizedSeed() {

        EHPrototypeManager.registerNewPrototype(new DummyEntity(-1, "Blabla"));

        for (int failer = 0; failer < 1000; failer++) {
            try {
                EHFile<DummyEntity> extHashing = new EHFile<>(DummyEntity.class, "dummy_entities", 5, 5);
                List<DummyEntity> dummyEntities = new ArrayList<>();
                List<Integer> usedIds = new ArrayList<>();
                Random rnd = new Random(failer);

                for (int i = 0; i < 100; i++) {
                    Integer id = null;
                    while (id == null || id < 10000) { // < 10000 lebo sa ID dáva aj ako stringová dátová časť, ktorá musí mať 5 znakov
                        int c = rnd.nextInt(Integer.MAX_VALUE);
                        if (usedIds.contains(c)) {
                            continue;
                        }
                        usedIds.add(c);
                        id = c;
                    }
                    DummyEntity dummyEntity = new DummyEntity(id, "" + id);

                    // Môže sa stať, že sa entita nakoniec nepridá do EH (pri už vloženom ID) ale vloží sa do paralelnej štrukt.
                    // To ale nevadí, len sa nakoniec podľa paralelnej štruktúry 2x vyhľadá
                    // Ale to nič nemení na fakte, že insert mohol vrátiť false (nepodarilo sa vložiť entitu), teda sa tam aj tak bude nachádzať len 1x
                    dummyEntities.add(dummyEntity);
                    extHashing.insert(dummyEntity);
                }

                for (DummyEntity findEnt : dummyEntities) {
                    DummyEntity found = extHashing.find(findEnt);
                    Assertions.assertNotNull(found);
                    Assertions.assertEquals(found.getRandomText(), findEnt.getRandomText());
                }

                extHashing.close();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(failer);
            }
        }
    }

    /**
     * Otestuje či sa entity vložia a dajú sa správne vytiahnúť,
     * testuje hlavne to, či sa entity správne vkladajú do hlavných blokov
     * s použitím vysokej hodnoty adresovania bitov (32)
     */
    @Test
    @Order(7)
    public void testRandomOperationsOfManyEntities() {

        EHPrototypeManager.registerNewPrototype(new DummyEntity(-1, "Blabla"));

        EHFile<DummyEntity> extHashing = new EHFile<>(DummyEntity.class, "dummy_entities", 15, 10);
        List<DummyEntity> dummyEntities = new ArrayList<>();
        List<DummyEntity> deletedDummyEntities = new ArrayList<>();
        Random rnd = new Random();

        int operationsCount = 100000;
        int progresser = operationsCount / 100;
        int progress = 0;
        long startTime = System.nanoTime();
        for (int i = 0; i < operationsCount; i++) {

            if (i % progresser == 0) {
                long progressTime = System.nanoTime();
                System.out.println(progress++ + "% completed, took " + (progressTime-startTime) / 1000000000);
            }

            // V polke celej iterácie súbor uloží a spätne načíta z configu a ďalej pokračuje s touto inštanciou
            if (i == operationsCount / 2) {
                extHashing.saveState();
                EHFileConfig ehFileConfig = EHFileConfig.fromFile("dummy_entities_save.ehdb");
                extHashing = new EHFile<>(DummyEntity.class, ehFileConfig);

            }
            double op = rnd.nextDouble();
            // V 80% sa robí insert (okrem warmupu, prvých 10% z celkového počtu operácií "operationsCount" je čisto insert)
            if (op > 0.2 || (i < operationsCount / 10)) { // Prvých 10% operácií nech je zaručených insert kvôli "warmupu"
                int id = rnd.nextInt(operationsCount);
                DummyEntity dummyEntity = new DummyEntity(id, StringUtils.randomAlphabeticString(5));

                Boolean inserted = extHashing.insert(dummyEntity);
                if (inserted) {
                    dummyEntities.add(dummyEntity);
                    // To sa stáva, keď sa entita vymazala a potom sa znova insertne (musí sa vyhodiť z deleted paralel. štrukt)
                    deletedDummyEntities.remove(dummyEntity);
                }
            }
            // V 5% sa robí update
            else if (op > 0.15) {
                // Náhodnej entite zo všetkých zmení text na "ZMENA"
                int randIndex = rnd.nextInt(dummyEntities.size());
                DummyEntity dummyEntity = dummyEntities.get(randIndex);
                dummyEntity.setRandomText("ZMENA");
                extHashing.update(dummyEntity);
            }
            // V 15% sa robí delete
            else {
                DummyEntity rndEntityToDelete = dummyEntities.remove(rnd.nextInt(dummyEntities.size()));
                extHashing.delete(rndEntityToDelete);
                dummyEntities.remove(rndEntityToDelete);
                deletedDummyEntities.add(rndEntityToDelete);
            }
        }
        // Z tých entít čo sa nevymazali skúsim ich nájdenie
        for (DummyEntity findEnt : dummyEntities) {
            DummyEntity found = extHashing.find(findEnt);
            Assertions.assertNotNull(found);
            Assertions.assertEquals(found.getRandomText(), findEnt.getRandomText());
        }

        // Z vymazaných skúsim ich nájdenie, má byť null
        for (DummyEntity deleted : deletedDummyEntities) {
            DummyEntity deletedFound = extHashing.find(deleted);
            Assertions.assertNull(deletedFound);
        }

        // Spätná kontrola, ktorá vráti všetky validné entity zo štruktúry
        // a porovná to s paralelnou štruktúrou
        List<DummyEntity> validEntities = extHashing.getAllValidEntities();
        System.out.println("Going to check " + validEntities.size() + " entities");

        int validProgesser = validEntities.size() / 1000; // Progress bude vypisovať po každých 0.1%
        double validProgress = 0.0;
        int validProcessed = 0;

        // Kontrola, či sa medzi "valid entitami" nenachádzajú duplicity
        for (DummyEntity validEntity : validEntities) {
            List<DummyEntity> des = validEntities.stream().filter(e -> e.getIdentifier() == validEntity.getIdentifier()).collect(Collectors.toList());
            Assertions.assertEquals(des.size(), 1);
        }

        // Počet valid entít z extHahingu musí byť rovný počtu vložených a zároveň nevymazaných entít v paralel. štruktúre
        Assertions.assertEquals(validEntities.size(), dummyEntities.size());
        for (DummyEntity validEntity : validEntities) {
            // Entita v zozname paralelnej štruktúry (musí byť nájdená, schválne volaný Optional.get() bez handlingu)
            // Dosť drahá operácia, lebo s každou entitou vyfiltruváva z paralel štruktúry len rovnaké ako je ona, kvôli kontrole, či tam je len jedna
            List<DummyEntity> des = dummyEntities.stream().filter(e -> e.getIdentifier() == validEntity.getIdentifier()).collect(Collectors.toList());

            if (validProcessed % validProgesser == 0) {
                validProgress+=0.1000000000000000;
                System.out.println("Valid progress: " + validProgress + "%");
            }
            // Assertne sa, či v paralelnej štruktúre je entita s týmto ID len raz
            Assertions.assertEquals(des.size(), 1);
            DummyEntity de = des.get(0);

            Assertions.assertEquals(de.getRandomText(), validEntity.getRandomText());
            validProcessed++;
        }

    }

    /**
     * Overí hlavne to, že či sa entity správne dajú vložiť a nájsť tak,
     * že jednak sa použije malá hodnota pre adresovanie (1 bit),
     * čo delí entity na párne a nepárne podľa hashu,
     * a použije sa ale aj nízka hodnota blokovacieho faktoru pre hlavné bloky (1),
     * ale aj pre overflow bloky (1), čo zaručí, že už 3. vkladaná entita
     * bude naisto vložená do overflow bloku a keďže je blokovací faktor
     * overflow blokov nízky (1), ďalej bude vznikať dlhá sieť overflow blokov,
     * ktorý každý bude obsahovať len jednu entitu
     */
    @Test
    @Order(8)
    public void testOverflowBlocks() {

        EHPrototypeManager.registerNewPrototype(new DummyEntity(-1, "Blabla"));

        EHFile<DummyEntity> extHashing = new EHFile<>(DummyEntity.class, "dummy_entities", 1, 1, 1);
        List<DummyEntity> dummyEntities = new ArrayList<>();
        List<Integer> usedIds = new ArrayList<>();
        Random rnd = new Random();

        for (int i = 0; i < 1000; i++) {
            Integer id = null;
            while (id == null || id < 10000) { // < 10000 kvôli dátovej časti, string musí byť dlhší/rovný 5
                int c = rnd.nextInt(Integer.MAX_VALUE);
                if (usedIds.contains(c)) {
                    continue;
                }
                usedIds.add(c);
                id = c;
            }
            DummyEntity dummyEntity = new DummyEntity(id, "" + id);
            dummyEntities.add(dummyEntity);
            extHashing.insert(dummyEntity);
        }

        for (DummyEntity findEnt : dummyEntities) {
            DummyEntity found = extHashing.find(findEnt);
            Assertions.assertNotNull(found);
            Assertions.assertEquals(found.getRandomText(), findEnt.getRandomText());
        }

        extHashing.close();
    }

    @Test
    @Order(9)
    public void testWithDeterministicFailingValues() {
        EHPrototypeManager.registerNewPrototype(new DummyEntity(-1, "Blabla"));
        EHFile<DummyEntity> extHashing = new EHFile<>(DummyEntity.class, "dummy_entities", 2, 2, 3);

        DummyEntity de1 = new DummyEntity(932, StringUtils.randomAlphabeticString(5));
        DummyEntity de2 = new DummyEntity(648, StringUtils.randomAlphabeticString(5));
        DummyEntity de3 = new DummyEntity(490, StringUtils.randomAlphabeticString(5));
        DummyEntity de4 = new DummyEntity(262, StringUtils.randomAlphabeticString(5));
        DummyEntity de5 = new DummyEntity(761, StringUtils.randomAlphabeticString(5));
        DummyEntity de6 = new DummyEntity(609, StringUtils.randomAlphabeticString(5));
        DummyEntity de7 = new DummyEntity(45, StringUtils.randomAlphabeticString(5));
        DummyEntity de8 = new DummyEntity(845, StringUtils.randomAlphabeticString(5));
        DummyEntity de9 = new DummyEntity(475, StringUtils.randomAlphabeticString(5));
        DummyEntity de10 = new DummyEntity(387, StringUtils.randomAlphabeticString(5));

        DummyEntity de14 = new DummyEntity(14, StringUtils.randomAlphabeticString(5));

        extHashing.insert(de1);
        extHashing.insert(de2);
        extHashing.insert(de3);
        extHashing.insert(de4);
        extHashing.insert(de5);
        extHashing.insert(de6);
        extHashing.insert(de7);
        extHashing.insert(de8);
        extHashing.insert(de9);
        extHashing.insert(de10);

        extHashing.insert(de14);

        extHashing.delete(de14);

    }

    @Test
    public void testCustomBitSetConverter() {

        // Binary 1000101011
        int toCustomBitSet = 555;
        boolean[] fixedCustomBitSet = { true, true, false, true, false, true, false, false, false, true }; // 555 "od najnižšieho bitu"
        int[] fixedIntsFromBits = {
                1,              //          1
                3,              //         11
                6,              //        110
                13,             //       1101
                26,             //      11010
                53,             //     110101
                106,            //    1101010
                212,            //   11010100
                424,            //  110101000
                849,            // 1101010001
        };

        for (int i = 0; i < fixedCustomBitSet.length; i++) {
            CustomBitSet bs = BitSetUtils.getBitsOfInt(toCustomBitSet, i+1);
            for (int y = 0; y < i+1; y++) {
                Assertions.assertEquals(bs.get(y), fixedCustomBitSet[y]);
            }
            Assertions.assertEquals(BitSetUtils.fromCustomBitSet(bs), fixedIntsFromBits[i]);
        }

    }

    @Test
    public void testEqualsOmitLastLSFBits() {

        int int1 = 4; // 100
        int int2 = 5; // 101
        int int3 = 6; // 110

        int aa = 6;
        int bb = 6;

        Assertions.assertTrue(BitSetUtils.equalsOmitLastNBits(aa, bb, 1));
        Assertions.assertTrue(BitSetUtils.equalsOmitLastNBits(aa, bb, 2));
        Assertions.assertTrue(BitSetUtils.equalsOmitLastNBits(aa, bb, 3));

        Assertions.assertTrue(BitSetUtils.equalsOmitLastNBits(int1, int2, 1));
        Assertions.assertFalse(BitSetUtils.equalsOmitLastNBits(int1, int2, 0));

        Assertions.assertFalse(BitSetUtils.equalsOmitLastNBits(int1, int3, 0));
        Assertions.assertFalse(BitSetUtils.equalsOmitLastNBits(int1, int3, 1));
        Assertions.assertTrue(BitSetUtils.equalsOmitLastNBits(int1, int3, 2));

        Assertions.assertFalse(BitSetUtils.equalsOmitLastNBits(int1, int2, 0));


    }

    @Test
    public void testBitFlipper() {

        int allOnes = 15;
        int allZeroes = 0;

        Assertions.assertEquals(14, BitSetUtils.invertBit(allOnes, 0));
        Assertions.assertEquals(13, BitSetUtils.invertBit(allOnes, 1));
        Assertions.assertEquals(11, BitSetUtils.invertBit(allOnes, 2));
        Assertions.assertEquals(7, BitSetUtils.invertBit(allOnes, 3));

        Assertions.assertEquals(1, BitSetUtils.invertBit(allZeroes, 0));
        Assertions.assertEquals(2, BitSetUtils.invertBit(allZeroes, 1));
        Assertions.assertEquals(4, BitSetUtils.invertBit(allZeroes, 2));
        Assertions.assertEquals(8, BitSetUtils.invertBit(allZeroes, 3));

    }

}
