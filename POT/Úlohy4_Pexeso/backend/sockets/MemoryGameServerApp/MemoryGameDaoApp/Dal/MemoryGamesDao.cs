﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemoryGameDaoApp.Context;
using PexesoApi.Bo;

namespace MemoryGameDaoApp.Dal
{
    public class MemoryGamesDao
    {

        public MemoryGamesDao()
        {

        }

        async public Task<List<MemoryGame>> GetMemoryGamesFiltered(int gameId, int playerId, string sortingAttr, bool sortingOrder)
        {
            return await Task.Run(() =>
            {
                using (var dbContext = new MemoryGameDbContext())
                {

                    var linqFilter = dbContext.MemoryGames.Include("Player1").Include("Player2").Include("Winner").Select(e => e);

                    if (gameId != -1)
                    {
                        linqFilter = linqFilter.Where(e => e.MemoryGameId == gameId);
                    }

                    if (playerId != -1)
                    {
                        linqFilter = linqFilter.Where(e => e.Player1.PlayerId == playerId || e.Player2.PlayerId == playerId);
                    }

                    if (sortingAttr != null)
                    {
                        if (sortingAttr == "time")
                        {
                            if (sortingOrder)
                            {
                                linqFilter = linqFilter.OrderByDescending(e => e.TotalTime);
                            }
                            else
                            {
                                linqFilter = linqFilter.OrderBy(e => e.TotalTime);
                            }
                        }

                        if (sortingAttr == "start")
                        {
                            if (sortingOrder)
                            {
                                linqFilter = linqFilter.OrderByDescending(e => e.GameStartTime);
                            }
                            else
                            {
                                linqFilter = linqFilter.OrderBy(e => e.GameStartTime);
                            }
                        }

                        if (sortingAttr == "steps")
                        {
                            if (sortingOrder)
                            {
                                linqFilter = linqFilter.OrderByDescending(e => e.StepsCount);
                            }
                            else
                            {
                                linqFilter = linqFilter.OrderBy(e => e.StepsCount);
                            }
                        }
                    }

                    return linqFilter.Select(e => e).ToList();

                }
            });
        }

        async public Task<MemoryGame> GetMemoryGameDetail(int gameId)
        {
            return await Task.Run(() =>
            {
                using (var dbContext = new MemoryGameDbContext())
                {
                    return dbContext.MemoryGames
                        .Include("MemoryGameSteps")
                        .Include("Player1")
                        .Include("Player2")
                        .Include("GameType")
                        .Include("GameStyle")
                        .SingleOrDefault(e => e.MemoryGameId == gameId);
                }
            });
        }


    }
}
