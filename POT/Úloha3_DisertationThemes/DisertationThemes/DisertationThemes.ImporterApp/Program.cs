﻿using System;
using System.IO;
using DisertationThemes.Database;

namespace DisertationThemes.ImporterApp
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                throw new Exception("Prvý parameter pre cestu k súboru musí byť zadaný!");
            }

            string fileName = args[0];
            var csvLines = File.ReadAllLines(fileName);

            using (var ctx = new DisertationDatabaseContext())
            {
                bool titleSkipper = true;
                foreach (var csvLine in csvLines)
                {
                    if (titleSkipper)
                    {
                        titleSkipper = false;
                        continue;
                    }

                    ProcessDisertationLine(csvLine, ctx);

                }

                ctx.SaveChanges();
            }
        }

        private static void ProcessDisertationLine(string csvLine, DisertationDatabaseContext ctx)
        {
            DisertationThemeLine dtl = new DisertationThemeLine(csvLine);

            Theme newTheme = new Theme();

            Supervisor supervisor = ctx.GetSupervisorByName(dtl.Supervisor);
            StProgram stProgram = ctx.GetStProgramByNameAndFieldOfStudy(dtl.StProgram, dtl.FieldOfStudy);


            if (supervisor == null)
            {
                supervisor = new Supervisor();
                supervisor.FullName = dtl.Supervisor;
                ctx.Supervisors.Add(supervisor);
                ctx.SaveChanges();
            }

            if (stProgram == null)
            {
                stProgram = new StProgram();
                stProgram.Name = dtl.StProgram;
                stProgram.FieldOfStudy = dtl.FieldOfStudy;
                ctx.StPrograms.Add(stProgram);
                ctx.SaveChanges();
            }

            newTheme.StProgram = stProgram;
            newTheme.Supervisor = supervisor;
            newTheme.Created = dtl.Created;
            newTheme.Description = dtl.Description;
            newTheme.IsExternalStudy = dtl.IsExternal;
            newTheme.IsFullTimeStudy = dtl.IsFullTime;
            newTheme.Name = dtl.ThemeName;
            newTheme.ResearchType = ctx.GetResearchTypeByName(dtl.ResearchType);

            ctx.Themes.Add(newTheme);
            ctx.SaveChanges();
        }
    }
}
