﻿using System.Collections.Generic;
using System.Linq;
using FriBettingDatabaseApp.Bo;
using FriBettingDatabaseApp.Context;
using FriBettingDatabaseApp.Repository.Base;

namespace FriBettingDatabaseApp.Repository.Impl
{
    public class CompetitorRepository: Repository<Competitor>, ICompetitorRepository
    {
        public ICollection<Competitor> GetAllCompetitorsCompetingInSport(int sportId)
        {

            using (var dbContext = new BettingAppDbContext())
            {
                return dbContext.Competitors.Where(e => e.SportCompetitingAt.SportId == sportId).ToList();
            }
        }

        public ICollection<Competitor> GetAllCompetitorsCompetingInSportWithNameLike(int sportId, string nameLike)
        {
            throw new System.NotImplementedException();
        }
    }
}
