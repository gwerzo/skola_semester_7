package sk.uniza.fri.lustiak.auds2.kataster.entities.bo;

import lombok.Data;
import sk.uniza.fri.lustiak.auds2.kataster.entities.wrapper.ParcelWrapper;

import java.util.LinkedList;
import java.util.List;

@Data
public class RealEstate {

    private int supisneCislo;
    private String popis;
    private List<ParcelWrapper> zoznamParciel;

    public void pridajParcelu(ParcelWrapper parcel) {
        if (this.zoznamParciel == null) {
            this.zoznamParciel = new LinkedList<>();
        }
        this.zoznamParciel.add(parcel);
    }

    public void odoberParcelu(ParcelWrapper parcel) {
        if (this.zoznamParciel != null) {
            ParcelWrapper toDel = null;
            for (ParcelWrapper par : this.zoznamParciel) {
                if (par.getCoordinates2DKey().getValue().getUniqueIdentifier()
                        == parcel.getCoordinates2DKey().getValue().getUniqueIdentifier()) {
                    toDel = par;
                    break;
                }
            }
            this.zoznamParciel.remove(toDel);
        }
    }

}
