package sk.uniza.fri.lustyno.AudsLetisko.entities_bo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class HistoriaLietadlaNaLetiskuEntity {

    private LietadloEntity lietadloEntity;

    private LocalDateTime casDatumPriletu;

    private LocalDateTime casDatumPoziadaniaOPridelenieOdletovejDrahy;

    public static class Builder {

        private LietadloEntity bLietadloEntity;

        private LocalDateTime bCasDatumPriletu;

        private LocalDateTime bCasDatumPoziadaniaOPridelenieOdletovejDrahy;

        public Builder() {

        }

        public Builder preLietadlo(LietadloEntity lietadlo) {
            this.bLietadloEntity = lietadlo;
            return this;
        }

        public Builder sDatumomPriletu(LocalDateTime datumPriletu) {
            this.bCasDatumPriletu = datumPriletu;
            return this;
        }

        public Builder sDatumomPoziadania(LocalDateTime datumPoziadania) {
            this.bCasDatumPoziadaniaOPridelenieOdletovejDrahy = datumPoziadania;
            return this;
        }

        public HistoriaLietadlaNaLetiskuEntity build() {
            HistoriaLietadlaNaLetiskuEntity entity = new HistoriaLietadlaNaLetiskuEntity();
            entity.setLietadloEntity(this.bLietadloEntity);
            entity.setCasDatumPriletu(this.bCasDatumPriletu);
            entity.setCasDatumPoziadaniaOPridelenieOdletovejDrahy(this.bCasDatumPoziadaniaOPridelenieOdletovejDrahy);

            return entity;

        }



    }

}
