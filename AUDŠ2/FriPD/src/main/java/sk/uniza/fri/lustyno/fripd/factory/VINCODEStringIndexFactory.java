package sk.uniza.fri.lustyno.fripd.factory;

import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.FromBytesFactory;
import sk.uniza.fri.lustyno.fripd.indexes.VINCODEStringIndex;

public class VINCODEStringIndexFactory extends FromBytesFactory<VINCODEStringIndex> {

    public VINCODEStringIndexFactory() {

    }

    @Override
    public VINCODEStringIndex fromBytes(byte[] bytes) {
        return VINCODEStringIndex.fromByteArray(bytes);
    }

    @Override
    public VINCODEStringIndex emptyInvalidInstance() {
        return VINCODEStringIndex.emptyInvalidInstance();
    }

}
