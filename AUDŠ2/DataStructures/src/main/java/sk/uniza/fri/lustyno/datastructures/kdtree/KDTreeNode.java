package sk.uniza.fri.lustyno.datastructures.kdtree;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValueOnDimension;
import sk.uniza.fri.lustyno.datastructures.entity.keys.UniquelyIdentifiable;

import java.util.LinkedList;
import java.util.List;

public class KDTreeNode<K extends HasComparableValueOnDimension<V>, D, V extends UniquelyIdentifiable<V>> {

    private K key;

    private D data;

    private int depthModulo;

    private KDTreeNode<K,D,V> parent;
    private KDTreeNode<K,D,V> leftChild;
    private KDTreeNode<K,D,V> rightChild;

    private List<KDTreeNode<K,D,V>> brothers;
    private KDTreeNode<K,D,V> superBrother;

    KDTreeNode(K key, D data, int depth) {
        this.key = key;
        this.data = data;
        this.depthModulo = depth;
        this.brothers = new LinkedList<>();
    }

    public K getKey() {
        return this.key;
    }

    public D getData() {
        return this.data;
    }

    protected void setKey(K key) {
        this.key = key;
    }

    protected void setData(D data) {
        this.data = data;
    }

    public KDTreeNode<K, D, V> getParent() {
        return parent;
    }

    public KDTreeNode<K, D, V> getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(KDTreeNode<K, D, V> leftChild) {
        this.leftChild = leftChild;
    }

    public KDTreeNode<K, D, V> getRightChild() {
        return rightChild;
    }

    public void setRightChild(KDTreeNode<K, D, V> rightChild) {
        this.rightChild = rightChild;
    }

    public void setParent(KDTreeNode<K, D, V> parent) {
        this.parent = parent;
    }

    public int getDepthModulo() {
        return depthModulo;
    }

    public void setDepthModulo(int depthModulo) {
        this.depthModulo = depthModulo;
    }

    public boolean isLeaf() {
        return this.leftChild == null && this.rightChild == null;
    }

    public boolean isRoot() {
        return this.parent == null;
    }

    public boolean hasOnlyOneChild() {
        return (this.leftChild == null && this.rightChild != null) || (this.leftChild != null && this.rightChild == null);
    }

    public boolean isRightChildOfParent() {
        return this.equals(this.parent.getRightChild());
    }

    protected boolean hasRightChild() {
        return this.rightChild != null;
    }

    protected boolean hasLeftChild() {
        return this.leftChild != null;
    }

    protected void removeItselfFromParent() {
        if (this.isRightChildOfParent()) {
            this.parent.setRightChild(null);
            this.parent = null;
        } else {
            this.parent.setLeftChild(null);
            this.parent = null;
        }
    }

    protected void addBrother(KDTreeNode<K,D,V> brother) {
        this.brothers.add(brother);
    }

    protected List<KDTreeNode<K,D,V>> getBrothers() {
        return this.brothers;
    }

    protected void setBrothers(List<KDTreeNode<K,D,V>> brothers) {
        this.brothers = brothers;
    }

    protected void removeBrother(KDTreeNode<K,D,V> brotherToRemove) {
        this.brothers.remove(brotherToRemove);
    }

    protected KDTreeNode<K, D, V> getSuperBrother() {
        return superBrother;
    }

    protected void setSuperBrother(KDTreeNode<K, D, V> superBrother) {
        this.superBrother = superBrother;
    }

    @Override
    public String toString() {
        return this.key.toString();
    }

}
