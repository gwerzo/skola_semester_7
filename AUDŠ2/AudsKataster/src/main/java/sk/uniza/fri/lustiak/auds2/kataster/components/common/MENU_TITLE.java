package sk.uniza.fri.lustiak.auds2.kataster.components.common;

public enum MENU_TITLE {

    MENU_1("1. Vyhľadanie NEHNUTEĽNOSTÍ podľa GPS"),
    MENU_2("2. Vyhľadanie PARCIEL podľa GPS"),
    MENU_3("3. Vyhľadanie VŠETKÝCH OBJEKTOV podľa GPS"),
    MENU_4("4. Pridanie novej NEHNUTEĽNOSTI"),
    MENU_5("5. Pridanie novej PARCELY"),
    MENU_6("6. Editácia údajov NEHNUTEĽNOSTI"),
    MENU_7("7. Editácia údajov PARCELY"),
    MENU_8("8. Vyradenie NEHNUTEĽNOSTI"),
    MENU_9("9. Vyradenie PARCELY"),

    //-----------------------------------------------------------------------------
    SYSTEM_MENU_1("1. Generátor systému"),
    SYSTEM_MENU_2("2. Export systému"),
    SYSTEM_MENU_3("3. Import systému"),
    SYSTEM_MENU_4("4. Výpis všetkého");

    private String title;

    MENU_TITLE(String s) {
        title = s;
    }

    public String getTitle() {
        return title;
    }
}
