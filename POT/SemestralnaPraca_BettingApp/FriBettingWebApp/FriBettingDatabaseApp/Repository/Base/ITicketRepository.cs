﻿using FriBettingDatabaseApp.Bo;

namespace FriBettingDatabaseApp.Repository.Base
{
    interface ITicketRepository: IRepository<Ticket>
    {
    }
}
