package sk.uniza.fri.lustyno.fripd.entities_bo;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Record;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.HasSize;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.StringUtils;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;

import java.time.LocalDateTime;
import java.util.Arrays;

@Data
@Accessors(chain = true)
@HasSize(sizeInBytes = 88)
public class DrivingLicenceEntity extends Record<IntegerKey> {

    private IntegerKey key;

    public DrivingLicenceEntity() {

    }

    public DrivingLicenceEntity(Integer evc, String menoVodica, String priezviskoVodica,
                                LocalDateTime datumUkonceniaPlatnosti, Boolean zakazViestVozidlo,
                                Integer pocetPriestupkovZaPoslednyRok) {
        this.key = new IntegerKey(evc);
        this.EVC = evc;
        this.menoVodica = StringUtils.padRightDollars(menoVodica, this.MENO_VODICA_DLZKA_MAX);
        this.priezviskoVodica = StringUtils.padRightDollars(priezviskoVodica, this.PRIEZVISKO_VODICA_DLZKA_MAX);
        this.datumUkonceniaPlatnosti = datumUkonceniaPlatnosti;
        this.zakazViestVozidlo = zakazViestVozidlo;
        this.pocetPriestupkovZaPoslednyRok = pocetPriestupkovZaPoslednyRok;
    }

    /**
     * Unikátne EVČ preukazu
     */
    private Integer EVC;

    /**
     * Meno vodiča
     */
    private Integer MENO_VODICA_DLZKA_MAX = 35;
    private String menoVodica;

    /**
     * Priezvisko vodiča
     */
    private Integer PRIEZVISKO_VODICA_DLZKA_MAX = 35;
    private String priezviskoVodica;

    /**
     * Dátum ukončenia platnosti vodického preukazu
     */
    private LocalDateTime datumUkonceniaPlatnosti;

    /**
     * Či má vodič zákaz viesť vozidlo
     */
    private Boolean zakazViestVozidlo;

    /**
     * Počet priestupkov vodiča za posledný rok
     *
     *
     */
    private Integer pocetPriestupkovZaPoslednyRok;

    @Override
    public IntegerKey getKey() {
        return this.key == null ? new IntegerKey(this.EVC) : this.key;
    }

    @Override
    public byte[] toBytesArray() {
        byte[] bytes = ToByteArrayConverter.concatenateByteArrays(
                Arrays.asList(
                        ToByteArrayConverter.bytesFromBoolean(this.valid),
                        ToByteArrayConverter.bytesFromInt(this.EVC),
                        ToByteArrayConverter.bytesFromString(this.menoVodica),
                        ToByteArrayConverter.bytesFromString(this.priezviskoVodica),
                        ToByteArrayConverter.bytesFromLocalDateTime(this.datumUkonceniaPlatnosti),
                        ToByteArrayConverter.bytesFromBoolean(this.zakazViestVozidlo),
                        ToByteArrayConverter.bytesFromInt(this.pocetPriestupkovZaPoslednyRok)));
        return bytes;
    }

    public static DrivingLicenceEntity fromByteArray(byte[] bytes) {
        Boolean valid = FromByteArrayConverter.booleanFromByte(bytes[0]);
        if (!valid) {
            return emptyInvalidInstance();
        }
        Integer evc = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(bytes, 1, 5));
        String menoVodica = FromByteArrayConverter.stringFromBytes(Arrays.copyOfRange(bytes, 5, 40));
        String priezviskoVodica = FromByteArrayConverter.stringFromBytes(Arrays.copyOfRange(bytes, 40, 75));
        LocalDateTime platnost = FromByteArrayConverter.localDateTimeFromBytes(Arrays.copyOfRange(bytes, 75, 83));
        Boolean zakaza = FromByteArrayConverter.booleanFromByte(bytes[83]);
        Integer pocetPriestupkov = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(bytes, 84, 88));
        DrivingLicenceEntity entity = new DrivingLicenceEntity(evc, menoVodica, priezviskoVodica, platnost,
                zakaza, pocetPriestupkov);
        entity.setValid(true);
        return entity;
    }

    public static DrivingLicenceEntity emptyInvalidInstance() {
        return new DrivingLicenceEntity(
                -1, "", "", LocalDateTime.now(), false, -1);
    }

    @Override
    public int getHash() {
        return this.EVC;
    }
}
