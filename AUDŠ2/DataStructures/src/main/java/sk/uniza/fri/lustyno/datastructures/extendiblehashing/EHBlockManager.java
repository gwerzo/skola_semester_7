package sk.uniza.fri.lustyno.datastructures.extendiblehashing;

import sk.uniza.fri.lustyno.datastructures.extendiblehashing.entities.EHEntity;

import java.util.Comparator;
import java.util.LinkedList;

/**
 * Block/Overflow block manager,
 * ktorý rieši správu voľných blokov,
 * vracanie indexu nového bloku v prípade,
 * že žiaden voľný blok neexistoval
 */
public class EHBlockManager<T extends EHEntity<T>> {

    private EHFileHandler ehFileHandler;
    private EHPrototypeHolder<T> ehPrototypeHolder;

    private final Integer sizeOfNormalBlock;
    private final Integer sizeOfOverflowBlock;

    private Integer numberOfCreatedBlocks;
    private Integer numberOfCreatedOverflowBlocks;

    private LinkedList<Integer> freeBlocks;
    private LinkedList<Integer> freeOverflowBlocks;

    /**
     * Použiteľné pri vytváraní novej inštancie EHFile
     * @param ehFileHandler
     * @param ehPrototypeManager
     * @param initialBlockCount
     * @param sizeOfNormalBlock
     * @param sizeOfOverflowBlock
     */
    public EHBlockManager(
            EHFileHandler ehFileHandler, EHPrototypeHolder<T> ehPrototypeManager,
            Integer initialBlockCount,
            Integer sizeOfNormalBlock, Integer sizeOfOverflowBlock) {
        this.ehFileHandler = ehFileHandler;
        this.ehPrototypeHolder = ehPrototypeManager;

        this.numberOfCreatedBlocks = initialBlockCount;
        this.numberOfCreatedOverflowBlocks = 0;

        this.sizeOfNormalBlock = sizeOfNormalBlock;
        this.sizeOfOverflowBlock = sizeOfOverflowBlock;

        this.freeBlocks = new LinkedList<>();
        this.freeOverflowBlocks = new LinkedList<>();
    }

    /**
     * Použiteľné pri konštruovaní z načítaného configu
     * @param ehFileHandler
     * @param ehPrototypeManager
     * @param initialBlockCount
     * @param initialOverflowBlocksCount
     * @param sizeOfNormalBlock
     * @param sizeOfOverflowBlock
     */
    public EHBlockManager(
            EHFileHandler ehFileHandler, EHPrototypeHolder<T> ehPrototypeManager,
            Integer initialBlockCount, Integer initialOverflowBlocksCount,
            Integer sizeOfNormalBlock, Integer sizeOfOverflowBlock,
            LinkedList<Integer> freeBlocks, LinkedList<Integer> freeOverflowBlocks) {
        this.ehFileHandler = ehFileHandler;
        this.ehPrototypeHolder = ehPrototypeManager;

        this.numberOfCreatedBlocks = initialBlockCount;
        this.numberOfCreatedOverflowBlocks = initialOverflowBlocksCount;

        this.sizeOfNormalBlock = sizeOfNormalBlock;
        this.sizeOfOverflowBlock = sizeOfOverflowBlock;

        this.freeBlocks = freeBlocks;
        this.freeOverflowBlocks = freeOverflowBlocks;
    }

    /**
     * Vráti prvý voľný blok buď to z uvoľnených blokov,
     * alebo ak taký voľný blok neexistuje, vráti nový
     * index bloku, ktorý sa rovná
     * súčunu počtu už alokovaných blokov s veľkosťou jedného bloku
     *
     * V rámci tejto operácie navýši počet použitých/alokovaných blokov
     *
     * @return - index bloku, ktorý je voľný/resp. index nového bloku
     */
    public Integer getFirstFreeBlockIndex() {
        if (this.freeBlocks.size() > 0) {
            Integer minFreeBlock = this.freeBlocks.stream().min(Integer::compare).get();
            this.freeBlocks.remove(minFreeBlock);
            return minFreeBlock;
        } else {
            Integer toRet = this.sizeOfNormalBlock * this.numberOfCreatedBlocks;
            this.numberOfCreatedBlocks++;
            return toRet;
        }
    }

    /**
     * @see this#getFirstFreeBlockIndex()
     * s rozdielom, že tu vracia hodnotu pre overflow blok
     *
     * @return - index bloku, ktorý je voľný/resp. index nového bloku
     */
    public Integer getFirstFreeOverflowBlockIndex() {
        if (this.freeOverflowBlocks.size() > 0) {
            Integer minFreeBlock = this.freeOverflowBlocks.stream().min(Integer::compare).get();
            this.freeOverflowBlocks.remove(minFreeBlock);
            return minFreeBlock;
        } else {
            Integer toRet = this.sizeOfOverflowBlock * this.numberOfCreatedOverflowBlocks;
            this.numberOfCreatedOverflowBlocks++;
            return toRet;
        }
    }

    /**
     * Prečíta normálny blok z hlavného súboru na indexe
     * @param index - index, na ktorom blok začína
     * @return - prečítaný blok
     */
    protected EHBlock<T> readBlockAt(Integer index) {
        byte[] blockBytes =
                this.ehFileHandler.readFromMainFile(index, this.ehPrototypeHolder.getEHBlockPrototype().getBlockSize());
        return this.ehPrototypeHolder.getEHBlockPrototype().fromBytes(blockBytes);
    }

    /**
     * Prečíta overflow blok z overflow súboru na indexe
     * @param index  - index, na ktorom overflow blok začína
     * @return - prečítaný overflow blok
     */
    protected EHOverflowBlock<T> readOverflowBlockAt(Integer index) {
        byte[] overflowBlockBytes =
                this.ehFileHandler.readFromOverflowFile(index, this.ehPrototypeHolder.getOverflowBlockPrototype().getBlockSize());
        return this.ehPrototypeHolder.getOverflowBlockPrototype().fromBytes(overflowBlockBytes);
    }

    /**
     * Uloží blok tak, že ho serializuje do byte[] a uloží do hlavného súboru
     * @param block - blok na uloženie
     * @param index - index, kam sa má blok uložiť
     */
    protected void saveBlock(EHBlock<T> block, Integer index) {
        this.ehFileHandler.writeToMainFile(block.toBytes(), index);
    }

    /**
     * Uloží overflow blok tak, že ho serializuje do byte[] a uloží do overflow súboru
     * @param overflowBlock - overflow blok na uloženie
     * @param index - index, kam sa má blok uložiť
     */
    protected void saveOverflowBlock(EHOverflowBlock<T> overflowBlock, Integer index) {
        this.ehFileHandler.writeToOverflowFile(overflowBlock.toBytes(), index);
    }

    /**
     * Uvoľní blok (len ho pridá do zoznamu voľných)
     * @param blockIndex
     */
    protected void freeBlock(Integer blockIndex) {
        this.freeBlocks.add(blockIndex);
    }

    /**
     * Uvoľní overflow blok (len ho pridá do zoznamu voľných)
     * @param overflowBlockIndex
     */
    protected void freeOverflowBlock(Integer overflowBlockIndex) {
        this.freeOverflowBlocks.add(overflowBlockIndex);
    }

    /**
     * Skonsoliduje overflow block file,
     * pod čím sa dá predstaviť to,
     * že kvázi z konca súboru "odsekne"
     * nepoužívané bloky - zmenší veľkosť súboru
     *
     * Keďže sa súbor dá skracovať vždy len od konca,
     * algoritmus usporiada voľné bloky od najväčšieho po najmenší
     * a po jednom odstraňuje najväčší, ak môže
     *
     * Na koniec sa jednoducho zmenší overflow súbor o vyrátaný počet
     *
     */
    protected void consolidateOverflowFile() {

        int numberOfPossibleOverflowBlocksToTrim = 0;
        freeOverflowBlocks.sort(Comparator.reverseOrder());

        while (freeOverflowBlocks.size() > 0) {
            Integer max = freeOverflowBlocks.getFirst();
            if (max.equals((this.numberOfCreatedOverflowBlocks - 1) * this.sizeOfOverflowBlock)) {
                numberOfPossibleOverflowBlocksToTrim++;
                this.numberOfCreatedOverflowBlocks--;
                freeOverflowBlocks.removeFirst();
            } else {
                break;
            }
        }
        if (numberOfPossibleOverflowBlocksToTrim != 0) {
            this.ehFileHandler.trimOverflowFile(numberOfPossibleOverflowBlocksToTrim * this.sizeOfOverflowBlock);
        }
    }

    /**
     * @see this#consolidateOverflowFile() len nad main súborom
     */
    protected void consolidateMainBlocks() {

        int numberOfPossibleOverflowBlocksToTrim = 0;
        freeBlocks.sort(Comparator.reverseOrder());

        while (freeBlocks.size() > 0) {
            Integer max = freeBlocks.getFirst();
            if (max.equals((this.numberOfCreatedBlocks - 1) * this.sizeOfNormalBlock)) {
                numberOfPossibleOverflowBlocksToTrim++;
                this.numberOfCreatedBlocks--;
                freeBlocks.removeFirst();
            } else {
                break;
            }
        }
        if (numberOfPossibleOverflowBlocksToTrim != 0) {
            this.ehFileHandler.trimMainFile(numberOfPossibleOverflowBlocksToTrim * this.sizeOfNormalBlock);
        }
    }

    protected Integer getNumberOfCreatedBlocks() {
        return this.numberOfCreatedBlocks;
    }

    protected Integer getNumberOfCreatedOverflowBlocks() {
        return this.numberOfCreatedOverflowBlocks;
    }

    protected LinkedList<Integer> getFreeBlocks() {
        return freeBlocks;
    }

    protected LinkedList<Integer> getFreeOverflowBlocks() {
        return freeOverflowBlocks;
    }

}
