using System;
using System.Collections.Generic;
using System.Linq;
using MemoryGameDaoApp.Bo;
using MemoryGameDaoApp.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MemoryGameWebApp.Pages
{
    public class LoggedInUsersModel : PageModel
    {

        [BindProperty]
        public List<GameStyle> GameStyleSelectList { get; set; }
        [BindProperty]
        public List<IndexModel.GameTypeModel> GameTypeSelectList { get; set; }

        public void OnGet()
        {
            using(var dbContext = new MemoryGameDbContext())
            {

                var allGameTypes =
                    dbContext.GameTypes.ToList()
                        .Select(n => new IndexModel.GameTypeModel(n.GameTypeId, n.XLength + "x" + n.YLength)).ToList();

                var allGameStyles =
                    dbContext.GameStyles
                        .Select(n => n).ToList();

                this.GameTypeSelectList = allGameTypes;
                this.GameStyleSelectList = allGameStyles;

            }
        }

        public class GameTypeModel
        {

            public int GameTypeId { get; set; }
            public string Dimensions { get; set; }

            public GameTypeModel(int gameTypeId, string dimensions)
            {
                this.GameTypeId = gameTypeId;
                this.Dimensions = dimensions;
            }
        }
    }
}
