﻿using System;
using System.Collections.Generic;
using FriBettingWebApplication.Dto.Response;

namespace FriBettingWebApplication.Models
{
    public class MatchesTableModel
    {

        public List<MatchResponse> Matches { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
        public int Count { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

    }
}