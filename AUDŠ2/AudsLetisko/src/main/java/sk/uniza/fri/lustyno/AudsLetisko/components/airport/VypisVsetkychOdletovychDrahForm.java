package sk.uniza.fri.lustyno.AudsLetisko.components.airport;


import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import javafx.util.Pair;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.HistoriaOdletuUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.HistoryService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;

import java.util.LinkedList;
import java.util.List;

public class VypisVsetkychOdletovychDrahForm extends BusinessComponent {

    private HistoryService historyService;

    public VypisVsetkychOdletovychDrahForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_9.getTitle());
        this.historyService = UIServices.getDepartureHistoryService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        VerticalLayout layout = new VerticalLayout();



        HandledOperationExecutor.submit(() -> {
            List<HistoriaOdletuUiEntity> vsetkyOdlety = new LinkedList<>();

            List<Pair<Integer, List<HistoriaOdletuUiEntity>>> odletyPodlaDrah =
                    historyService.getHistoriaOdletovPreVsetkyDrahy();

            if (odletyPodlaDrah == null || odletyPodlaDrah.size() == 0) {
                layout.addComponent(new Label("Zatiaľ sa v systéme nenachádza žiaden odlet"));
                return OperationMarker.OPERATION_SUCCESFUL;
            } else {
                for (Pair<Integer, List<HistoriaOdletuUiEntity>> drahaOdletyPair : odletyPodlaDrah) {
                    vsetkyOdlety.addAll(drahaOdletyPair.getValue());
                    vsetkyOdlety.add(new HistoriaOdletuUiEntity());
                }
            }

            Grid<HistoriaOdletuUiEntity> grid = new Grid<>();

            grid.addColumn(HistoriaOdletuUiEntity::getCasOdletu).setCaption("Čas odletu");
            grid.addColumn(HistoriaOdletuUiEntity::getMedzinarodnyKodLietadla).setCaption("Medzinárodný kód lietadla");
            grid.addColumn(HistoriaOdletuUiEntity::getIdDrahy).setCaption("ID odletovej dráhy");
            grid.addColumn(HistoriaOdletuUiEntity::getCasPriletu).setCaption("Čas príletu");

            grid.setSizeFull();

            grid.setItems(vsetkyOdlety);

            layout.addComponent(grid);

            return OperationMarker.OPERATION_SUCCESFUL;
        });

        layout.setSizeFull();
        layout.setWidth("100%");

        Panel p = new Panel("Všetky odlety zgrupené podľa odletových dráh", layout);
        p.setSizeFull();
        this.parenComponent.showComponent(p);
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_9.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
