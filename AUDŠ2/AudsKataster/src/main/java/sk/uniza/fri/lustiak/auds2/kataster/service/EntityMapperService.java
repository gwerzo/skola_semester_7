package sk.uniza.fri.lustiak.auds2.kataster.service;

import org.springframework.stereotype.Service;
import sk.uniza.fri.lustiak.auds2.kataster.entities.bo.Parcel;
import sk.uniza.fri.lustiak.auds2.kataster.entities.bo.RealEstate;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.entities.wrapper.ParcelWrapper;
import sk.uniza.fri.lustiak.auds2.kataster.entities.wrapper.RealEstateWrapper;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo.Coordinates2DKey;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo.LATITUDE_POSITION;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo.LONGITUDE_POSITION;
import sk.uniza.fri.lustyno.datastructures.kdtree.KDTreeNodeKeyDataWrapper;

import java.util.stream.Collectors;

@Service
public class EntityMapperService {


    public RealEstateWrapper fromCreateNewRealEstateUi(CreateNewRealEstateUi createNewRealEstateUi) {

        Coordinates2DKey key = new Coordinates2DKey(
                createNewRealEstateUi.getLatitude(),
                createNewRealEstateUi.getLongitude(),
                createNewRealEstateUi.getLatitudeOrientation() ? LATITUDE_POSITION.NORTH : LATITUDE_POSITION.SOUTH,
                createNewRealEstateUi.getLongitudeOrientation() ? LONGITUDE_POSITION.EAST : LONGITUDE_POSITION.WEST);

        RealEstate newRealEstate = new RealEstate();
        newRealEstate.setSupisneCislo(createNewRealEstateUi.getSupisneCislo());
        newRealEstate.setPopis(createNewRealEstateUi.getPopisNehnutelnosti());
        return new RealEstateWrapper(key, newRealEstate);
    }

    public ParcelWrapper fromCreateNewParcelUi(CreateNewParcelUi createNewParcelUi) {

        Coordinates2DKey key = new Coordinates2DKey(
                createNewParcelUi.getLatitude(),
                createNewParcelUi.getLongitude(),
                createNewParcelUi.getLatitudeOrientation() ? LATITUDE_POSITION.NORTH : LATITUDE_POSITION.SOUTH,
                createNewParcelUi.getLongitudeOrientation() ? LONGITUDE_POSITION.EAST : LONGITUDE_POSITION.WEST);

        Parcel newParcel = new Parcel();
        newParcel.setCisloParcely(createNewParcelUi.getCisloParcely());
        newParcel.setPopis(createNewParcelUi.getPopisParcely());
        return new ParcelWrapper(key, newParcel);
    }

    public RealEstateUiTableEntity fromRealEstateNodeWrapper(
            KDTreeNodeKeyDataWrapper<Coordinates2DKey, RealEstate> nodeKeyDataWrapper) {

        RealEstate realEstate = nodeKeyDataWrapper.getData();
        Coordinates2DKey realEstateKey = nodeKeyDataWrapper.getKey();
        RealEstateUiTableEntity realEstateUiTableEntity = new RealEstateUiTableEntity();
        realEstateUiTableEntity.setInternalId(realEstateKey.getValue().getUniqueIdentifier());
        realEstateUiTableEntity.setSupisneCislo(realEstate.getSupisneCislo());
        realEstateUiTableEntity.setPopis(realEstate.getPopis());
        realEstateUiTableEntity.setLatitude(realEstateKey.getValue().getLatitudeValue());
        realEstateUiTableEntity.setLongtitude(realEstateKey.getValue().getLongitudeValue());
        realEstateUiTableEntity.setLatitudeOrientation(realEstateKey.getValue().getLatitudePosition() == LATITUDE_POSITION.NORTH ? "SEVER" : "JUH");
        realEstateUiTableEntity.setLongitudeOrientation(realEstateKey.getValue().getLongtitudePosition() == LONGITUDE_POSITION.EAST ? "VÝCHOD" : "ZÁPAD");

        realEstateUiTableEntity.setParcely(realEstate.getZoznamParciel().stream().map(e -> {
            ParcelUiTableEntity parcelUiTableEntity = new ParcelUiTableEntity();
            parcelUiTableEntity.setInternalId(e.getCoordinates2DKey().getValue().getUniqueIdentifier());
            parcelUiTableEntity.setCisloParcely(e.getParcel().getCisloParcely());
            parcelUiTableEntity.setPopisParcely(e.getParcel().getPopis());
            parcelUiTableEntity.setLatitude(e.getCoordinates2DKey().getValue().getLatitudeValue());
            parcelUiTableEntity.setLongtitude(e.getCoordinates2DKey().getValue().getLongitudeValue());
            parcelUiTableEntity.setLatitudeOrientation(e.getCoordinates2DKey().getValue().getLatitudePosition() == LATITUDE_POSITION.NORTH ? "SEVER" : "JUH");
            parcelUiTableEntity.setLongitudeOrientation(e.getCoordinates2DKey().getValue().getLongtitudePosition() == LONGITUDE_POSITION.EAST ? "VÝCHOD" : "ZÁPAD");
            return parcelUiTableEntity;
        }).collect(Collectors.toList()));

        return realEstateUiTableEntity;
    }

    public ParcelUiTableEntity fromParcelNodeWrapper(
            KDTreeNodeKeyDataWrapper<Coordinates2DKey, Parcel> nodeKeyDataWrapper) {

        Parcel parcel = nodeKeyDataWrapper.getData();
        Coordinates2DKey parcelKey = nodeKeyDataWrapper.getKey();
        ParcelUiTableEntity parcelUiTableEntity = new ParcelUiTableEntity();
        parcelUiTableEntity.setInternalId(parcelKey.getValue().getUniqueIdentifier());
        parcelUiTableEntity.setCisloParcely(parcel.getCisloParcely());
        parcelUiTableEntity.setPopisParcely(parcel.getPopis());
        parcelUiTableEntity.setLatitude(parcelKey.getValue().getLatitudeValue());
        parcelUiTableEntity.setLongtitude(parcelKey.getValue().getLongitudeValue());
        parcelUiTableEntity.setLatitudeOrientation(parcelKey.getValue().getLatitudePosition() == LATITUDE_POSITION.NORTH ? "SEVER" : "JUH");
        parcelUiTableEntity.setLongitudeOrientation(parcelKey.getValue().getLongtitudePosition() == LONGITUDE_POSITION.EAST ? "VÝCHOD" : "ZÁPAD");

        parcelUiTableEntity.setNehnutelnosti(parcel.getNehnutelnosti().stream().map(e -> {
            RealEstateUiTableEntity realEstateUiTableEntity = new RealEstateUiTableEntity();
            realEstateUiTableEntity.setInternalId(e.getCoordinates2DKey().getValue().getUniqueIdentifier());
            realEstateUiTableEntity.setSupisneCislo(e.getRealEstate().getSupisneCislo());
            realEstateUiTableEntity.setPopis(e.getRealEstate().getPopis());
            realEstateUiTableEntity.setLatitude(e.getCoordinates2DKey().getValue().getLatitudeValue());
            realEstateUiTableEntity.setLongtitude(e.getCoordinates2DKey().getValue().getLongitudeValue());
            realEstateUiTableEntity.setLatitudeOrientation(e.getCoordinates2DKey().getValue().getLatitudePosition() == LATITUDE_POSITION.NORTH ? "SEVER" : "JUH");
            realEstateUiTableEntity.setLongitudeOrientation(e.getCoordinates2DKey().getValue().getLongtitudePosition() == LONGITUDE_POSITION.EAST ? "VÝCHOD" : "ZÁPAD");
            return realEstateUiTableEntity;
        }).collect(Collectors.toList()));

        return parcelUiTableEntity;
    }

    public CreateNewRealEstateUi fromRealEstateTableUiEntity(RealEstateUiTableEntity fromEntity) {
        CreateNewRealEstateUi createNewRealEstateUi = new CreateNewRealEstateUi();
        createNewRealEstateUi.setSupisneCislo(fromEntity.getSupisneCislo());
        createNewRealEstateUi.setPopisNehnutelnosti(fromEntity.getPopis());
        createNewRealEstateUi.setLatitude(fromEntity.getLatitude());
        createNewRealEstateUi.setLongitude(fromEntity.getLongtitude());
        createNewRealEstateUi.setLatitudeOrientation(fromEntity.getLatitudeOrientation().equals("SEVER"));
        createNewRealEstateUi.setLongitudeOrientation(fromEntity.getLongitudeOrientation().equals("VÝCHOD"));
        return createNewRealEstateUi;
    }

    public CreateNewParcelUi fromParcelTableUiEntity(ParcelUiTableEntity fromEntity) {
        CreateNewParcelUi createNewParcelUi = new CreateNewParcelUi();
        createNewParcelUi.setCisloParcely(fromEntity.getCisloParcely());
        createNewParcelUi.setPopisParcely(fromEntity.getPopisParcely());
        createNewParcelUi.setLatitude(fromEntity.getLatitude());
        createNewParcelUi.setLongitude(fromEntity.getLongtitude());
        createNewParcelUi.setLatitudeOrientation(fromEntity.getLatitudeOrientation().equals("SEVER"));
        createNewParcelUi.setLongitudeOrientation(fromEntity.getLongitudeOrientation().equals("VÝCHOD"));
        return createNewParcelUi;
    }

    public Coordinates2DKey toCoordinatesKey(CoordinatesUi coordinatesUi) {
        return new Coordinates2DKey(coordinatesUi.getLatitude(), coordinatesUi.getLongitude(),
                coordinatesUi.getLatitudeOrientation() ? LATITUDE_POSITION.NORTH : LATITUDE_POSITION.SOUTH,
                coordinatesUi.getLongitudeOrientation() ? LONGITUDE_POSITION.EAST : LONGITUDE_POSITION.WEST);
    }


}
