﻿using FriBettingDatabaseApp.Bo;

namespace FriBettingDatabaseApp.Repository.Base
{
    public interface IMatchRepository: IRepository<Match>
    {
    }
}
