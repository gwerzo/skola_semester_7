﻿using System.Text.Json.Serialization;

namespace MemoryGameWebApp.DTO
{
    public class MemoryGamesListResponseDTO
    {

        [JsonPropertyName("GameId")]
        public int GameId { get; set; }
        [JsonPropertyName("Player1")]
        public string Player1 { get; set; }
        [JsonPropertyName("Player2")]
        public string Player2 { get; set; }
        [JsonPropertyName("Winner")]
        public string Winner { get; set; }
        [JsonPropertyName("StepsCount")] 
        public int StepsCount { get; set; }
        [JsonPropertyName("GameStartDateTime")]
        public long GameStartDateTime { get; set; }
        [JsonPropertyName("GameLengthInSeconds")]
        public long GameLengthInSeconds { get; set; }
    }
}
