package sk.uniza.fri.lustyno.datastructures.entity.keys;

public interface UniquelyIdentifiable<V> {

    boolean equalsUniquely(V other);
    void setUniqueIdIfEmpty(int uniqueId);

}
