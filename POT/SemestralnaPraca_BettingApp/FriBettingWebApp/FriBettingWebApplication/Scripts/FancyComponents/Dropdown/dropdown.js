﻿//Source - https://freefrontend.com/css-select-boxes/
function renderDropdown(uniqClass, itemSelectedCallback) {
    $(".drop." + uniqClass + " .option" ).click(function () {
        var val = $(this).attr("data-value"),
            $drop = $(".drop." + uniqClass),
            prevActive = $(".drop" + uniqClass + " .option.active").attr("data-value"),
            options = $(".drop." + uniqClass + " .option").length;
        $drop.find(".option.active." + uniqClass).addClass("mini-hack");
        $drop.toggleClass("visible");
        $drop.removeClass("withBG");
        $(this).css("top");
        $drop.toggleClass("opacity");
        $(".mini-hack." + uniqClass).removeClass("mini-hack");
        if ($drop.hasClass("visible")) {
            setTimeout(function() {
                    $drop.addClass("withBG");
                },
                400 + options * 100);
        } else {
            itemSelectedCallback(val);
        }
        triggerAnimation();
        if (val !== "placeholder" || prevActive === "placeholder") {
            $(".drop." + uniqClass +  " .option").removeClass("active");
            $(this).addClass("active");
        };
    });

    function triggerAnimation() {
        var finalWidth = $(".drop." + uniqClass).hasClass("visible") ? 14 : 12;
        $(".drop." + uniqClass).css("width", "16em");
        setTimeout(function () {
            $(".drop." + uniqClass).css("width", finalWidth + "em");
        }, 400);
    }
}


$(document).ready(function () {
    renderDropdown("matches-dropdown", function (data) { console.log("Menu item ZÁPASY selected") });
    renderDropdown("tickets-dropdown", function (data) { console.log("Menu item TIKETY selected") });
    renderDropdown("account-dropdown", function (data) { console.log("Menu item ÚČET selected") });
});