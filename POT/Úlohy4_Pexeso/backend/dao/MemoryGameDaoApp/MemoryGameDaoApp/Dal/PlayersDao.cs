﻿using System.Security.Cryptography;
using System.Text;
using MemoryGameDaoApp.Context;
using MemoryGameDaoApp.DalResults;
using PexesoApi.Bo;

namespace MemoryGameServerApp.Dal
{
    public class PlayersDao
    {

        public PlayersDao()
        {

        }

        public Player GetPlayerByUsername(string username)
        {
            using (var dbContext = new MemoryGameDbContext())
            {
                return dbContext.GetPlayerByUsername(username);
            }
        }

        public RegistrationResult RegisterUser(string username, string password1, string password2)
        {

            if (password1 != password2)
            {
                return new RegistrationResult()
                {
                    Successful = false,
                    UnsuccessReason = "Heslá sa nezhodujú"
                };
            }

            using (var dbContext = new MemoryGameDbContext())
            {
                Player withSuchUsernamePlayer = GetPlayerByUsername(username);
                if (withSuchUsernamePlayer != null)
                {
                    return new RegistrationResult()
                    {
                        Successful = false,
                        UnsuccessReason = "Používateľ už existuje"
                    };
                }

                Player player = new Player();
                player.Username = username;
                player.HashedPwd = ComputeSha256Hash(password1);

                dbContext.Players.Add(player);
                dbContext.SaveChanges();

                return new RegistrationResult()
                {
                    Successful = true,
                    UnsuccessReason = ""
                };
            }
        }

        /// <summary>
        /// Vráti SHA256 hash zo stringu
        /// <source>https://www.c-sharpcorner.com/article/compute-sha256-hash-in-c-sharp/</source>
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        public static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

    }
}
