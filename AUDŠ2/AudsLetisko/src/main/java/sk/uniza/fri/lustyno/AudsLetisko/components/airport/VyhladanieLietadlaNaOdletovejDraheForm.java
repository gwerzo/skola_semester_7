package sk.uniza.fri.lustyno.AudsLetisko.components.airport;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.LietadloTableUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.VyhladanieLietadlaUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.AircraftManagementService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;
import sk.uniza.fri.lustyno.AudsLetisko.utils.converters.StringToIntegerConverter;

import java.util.List;

public class VyhladanieLietadlaNaOdletovejDraheForm extends BusinessComponent {

    private Binder<VyhladanieLietadlaUiEntity> binder;
    private VyhladanieLietadlaUiEntity entity;

    private AircraftManagementService aircraftManagementService;

    private VerticalLayout lietadlaDetails;


    public VyhladanieLietadlaNaOdletovejDraheForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_4.getTitle());
        this.aircraftManagementService = UIServices.getAircraftManagementService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new VyhladanieLietadlaUiEntity();
        this.binder = new Binder<>(VyhladanieLietadlaUiEntity.class);

        this.lietadlaDetails = new VerticalLayout();

        VerticalLayout layout = new VerticalLayout();

        Label info1 = new Label("Ak chcete vyhľadať konkrétne lietadlo na konkrétnej dráhe, vyplňte obe hodnoty");
        Label emptyLabel = new Label();
        Label info2 = new Label("Ak chcete vyhľadať všetky lietadlá na konkrétnej dráhe, vyplňte len ID odletovej dráhy");

        FormLayout fl = new FormLayout();

        TextField medzinarodnyKod = new TextField("Medzinárodný kód");

        TextField idOdletovejDrahy = new TextField("ID odletovej dráhy");

        Button vyhladaj = new Button("Vyhľadaj");
        vyhladaj.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                vyhladajCakajuceLietadlo();
            } catch (ValidationException ex) {
                CustomNotification.error("Musíte vyplniť ID odletovej dráhy");
            }
        });

        this.binder.forField(medzinarodnyKod)
                .bind(VyhladanieLietadlaUiEntity::getMedzinarodnyKodLietadla, VyhladanieLietadlaUiEntity::setMedzinarodnyKodLietadla);
        this.binder.forField(idOdletovejDrahy)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(VyhladanieLietadlaUiEntity::getIdDrahy, VyhladanieLietadlaUiEntity::setIdDrahy);

        fl.addComponent(medzinarodnyKod);
        fl.addComponents(idOdletovejDrahy);
        fl.addComponent(vyhladaj);

        layout.addComponent(info1);
        layout.addComponent(info2);
        layout.addComponent(emptyLabel);
        layout.addComponent(fl);

        layout.addComponent(this.lietadlaDetails);

        Panel panelWrapper = new Panel("Vyhľadanie čakajúceho lietadla na pridelenie odletovej dráhy na konkrétnej dráhe", layout);
        panelWrapper.setWidth("100%");
        panelWrapper.setHeight("100%");

        this.parenComponent.showComponent(panelWrapper);
    }


    private void vyhladajCakajuceLietadlo() {
        if (this.entity.getMedzinarodnyKodLietadla().isEmpty()) {
            // Vyhľadanie všetkých lietadiel
            HandledOperationExecutor.submit(() -> {
                List<LietadloTableUiEntity> lietadlaCakajuceNaDrahu =
                        this.aircraftManagementService.getAllCakajuceLietadlaNaDrahu(this.entity);
                ukazInfoOLietadlach(lietadlaCakajuceNaDrahu);
                return OperationMarker.OPERATION_SUCCESFUL;
            });
        } else {
            // Vyhľadanie len konkrétneho lietadla
            HandledOperationExecutor.submit(() -> {
                LietadloTableUiEntity lietadloUi = this.aircraftManagementService.getLietadloCakajuceNaPridelenieOdletovejDrahyNaKonkretnejDrahe(this.entity);
                ukazInfoOLietadle(lietadloUi);
                return OperationMarker.OPERATION_SUCCESFUL;
            });
        }
    }

    private void ukazInfoOLietadlach(List<LietadloTableUiEntity> lietadloTableUiEntities) {
        this.lietadlaDetails.removeAllComponents();

        if (lietadloTableUiEntities == null || lietadloTableUiEntities.size() == 0) {
            this.lietadlaDetails.addComponent(new Label("Žiadne lietadlá nečakajú na túto dráhu"));
            return;
        }

        Grid<LietadloTableUiEntity> grid = new Grid<>();
        grid.addColumn(LietadloTableUiEntity::getMedzinarodnyKod).setCaption("Medzinárodný kód");
        grid.addColumn(LietadloTableUiEntity::getVyrobca).setCaption("Výrobca");
        grid.addColumn(LietadloTableUiEntity::getPriorita).setCaption("Priorita");
        grid.addColumn(LietadloTableUiEntity::getMinDlzkaDrahy).setCaption("Požadovaná dĺžka dráhy");
        grid.addColumn(LietadloTableUiEntity::getCasPriletu).setCaption("Čas príletu");
        grid.addColumn(LietadloTableUiEntity::getCasPoziadavkyOPridelenie).setCaption("Čas požiadavky o pridelenie");

        grid.setItems(lietadloTableUiEntities);
        grid.setSizeFull();

        this.lietadlaDetails.addComponent(grid);
    }

    private void ukazInfoOLietadle(LietadloTableUiEntity lietadloUi) {
        this.lietadlaDetails.removeAllComponents();

        FormLayout fl = new FormLayout();

        Label nazovVyrobcu = new Label(lietadloUi.getVyrobca());
        nazovVyrobcu.setCaption("Názov výrobcu");

        Label medzinarodnyKod = new Label(lietadloUi.getMedzinarodnyKod());
        medzinarodnyKod.setCaption("Medzinárodný kód");

        Label priorita = new Label(lietadloUi.getPriorita().toString());
        priorita.setCaption("Priorita");

        Label minimalnaDlzkaDrahy = new Label(lietadloUi.getMinDlzkaDrahy().toString());
        minimalnaDlzkaDrahy.setCaption("Minimálna dĺžka odletovej dráhy");

        Label casPriletu = new Label(lietadloUi.getCasPriletu().toString());
        casPriletu.setCaption("Čas príletu");

        Label casPoziadavky = new Label(lietadloUi.getCasPoziadavkyOPridelenie().toString());
        casPoziadavky.setCaption("Čas požiadavky o pridelenie odletovej dráhy");

        fl.addComponents(
                nazovVyrobcu,
                medzinarodnyKod,
                priorita,
                minimalnaDlzkaDrahy,
                casPriletu,
                casPoziadavky
        );

        this.lietadlaDetails.addComponent(fl);
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_4.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
