﻿using System.Linq;
using FriBettingDatabaseApp.Bo;
using FriBettingDatabaseApp.Context;
using FriBettingDatabaseApp.Repository.Base;

namespace FriBettingDatabaseApp.Repository.Impl
{
    public class UserRepository: Repository<BettingUser>, IUserRepository
    {
        public BettingUser GetUserByUsername(string username)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                return dbContext.Set<BettingUser>().SingleOrDefault(e => e.Username.Equals(username));
            }
        }
    }
}
