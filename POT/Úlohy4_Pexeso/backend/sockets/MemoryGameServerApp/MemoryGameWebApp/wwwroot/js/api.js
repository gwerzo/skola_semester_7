﻿function registerUser() {

    var username = $("#username").val();
    var password1 = $("#password1").val();
    var password2 = $("#password2").val();

    $.ajax({
            'type': "POST",
            'url': "/api/player/register",
            'contentType': "application/json",
            'data': JSON.stringify({
                Username: username,
                Password1: password1,
                Password2: password2
            }),
            'dataType': "json"
        }
    ).fail(function() {
        $.notify("Registrácia sa nepodarila", "error");
    }).done(function() {
        $.notify("Registrácia bola úspešná, môžete sa prihlásiť", "success");
        window.location = "/login";
    });

}