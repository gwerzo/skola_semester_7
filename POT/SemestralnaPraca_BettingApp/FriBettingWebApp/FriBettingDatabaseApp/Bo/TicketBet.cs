﻿namespace FriBettingDatabaseApp.Bo
{
    /// <summary>
    /// Class reprezentácia stávky na zápas/nejaký typ stávky
    /// už vytvoreného tiketu nejakého používateľa
    /// </summary>
    public class TicketBet: IdentifiableEntity
    {
        public int TicketBetId { get; set; }
        // Používateľ, ktorý stávku vytvoril
        public BettingUser BettingUser { get; set; }
        // Kurz stávky
        public Odd Odd { get; set; }
        // Tiket, na ktorom sa stávka nachádza
        public Ticket Ticket { get; set; }

        public override int GetId()
        {
            return TicketBetId;
        }
    }
}
