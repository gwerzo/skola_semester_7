package sk.uniza.fri.lustyno.AudsLetisko.components.system;

import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.service.GeneratorService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;

public class GeneratorForm extends BusinessComponent {

    private GeneratorService generatorService;

    public GeneratorForm(RepaintableComponent component) {
        super(component, MENU_TITLE.SYSTEM_MENU_5.getTitle());
        this.generatorService = UIServices.getGeneratorService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        VerticalLayout vl = new VerticalLayout();

        Button vygeneruj = new Button("Vygeneruj");
        vygeneruj.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {

                this.generatorService.generateEntities();
                CustomNotification.notification("Dáta boli vygenerované");
                return OperationMarker.OPERATION_SUCCESFUL;
            });
        });

        vl.addComponent(vygeneruj);


        this.parenComponent.showComponent(new Panel("Vygenerovanie dát", vl));

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_5.getTitle();
    }
}
