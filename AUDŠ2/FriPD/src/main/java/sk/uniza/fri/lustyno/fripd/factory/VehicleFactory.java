package sk.uniza.fri.lustyno.fripd.factory;

import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.FromBytesFactory;
import sk.uniza.fri.lustyno.fripd.entities_bo.VehicleEntity;

public class VehicleFactory extends FromBytesFactory<VehicleEntity> {

    public VehicleFactory() {}

    @Override
    public VehicleEntity fromBytes(byte[] bytes) {
        return VehicleEntity.fromByteArray(bytes);
    }

    @Override
    public VehicleEntity emptyInvalidInstance() {
        return VehicleEntity.emptyInvalidInstance();
    }

}
