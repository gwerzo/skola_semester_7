package sk.uniza.fri.lustiak.auds2.kataster.utils;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

public class StringToIntegerConverter implements Converter<String, Integer> {

    private static final long serialVersionUID = 1L;

    @Override
    public Result<Integer> convertToModel(String s, ValueContext valueContext) {
        Integer i;
        try {
            i = new Integer(s);
        } catch (Exception e) {
            return Result.error("Nesprávny formát čísla");
        }
        return Result.ok(i);
    }

    @Override
    public String convertToPresentation(Integer integer, ValueContext valueContext) {
        if (integer == null) {
            return "";
        }
        return integer.toString();
    }
}
