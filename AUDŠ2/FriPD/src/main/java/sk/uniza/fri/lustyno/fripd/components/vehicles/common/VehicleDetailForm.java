package sk.uniza.fri.lustyno.fripd.components.vehicles.common;

import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;

import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.VozidloEntityUi;

public class VehicleDetailForm extends FormLayout {

    private VozidloEntityUi vehicleEntityUi;

    private Label VINLabel;
    private Label EVCLabel;
    private Label vPatraniLabel;
    private Label datumPlatnostiSTKLabel;
    private Label datumPlatnostiEKLabel;
    private Label pocetNapravLabel;
    private Label prevadzkovaHmostnostLabel;

    public VehicleDetailForm(VozidloEntityUi entityUi) {
        this.vehicleEntityUi = entityUi;
        initializeLayout();
        wrap();
    }

    private void initializeLayout() {
        this.VINLabel = new Label(this.vehicleEntityUi.getVIN());
        this.VINLabel.setCaption("VIN Kód");

        this.EVCLabel = new Label(this.vehicleEntityUi.getEVC());
        this.EVCLabel.setCaption("EVČ Kód");

        this.vPatraniLabel = new Label(this.vehicleEntityUi.getVPatrani());
        this.vPatraniLabel.setCaption("V pátraní");

        this.datumPlatnostiSTKLabel = new Label(this.vehicleEntityUi.getDatumPlatnostiSTK());
        this.datumPlatnostiSTKLabel.setCaption("Platnosť TK");

        this.datumPlatnostiEKLabel = new Label(this.vehicleEntityUi.getDatumPlatnostiEK());
        this.datumPlatnostiEKLabel.setCaption("Platnosť EK");

        this.pocetNapravLabel = new Label(this.vehicleEntityUi.getPocetNaprav());
        this.pocetNapravLabel.setCaption("Počet náprav");

        this.prevadzkovaHmostnostLabel = new Label(this.vehicleEntityUi.getPrevadzkovaHmostnost());
        this.prevadzkovaHmostnostLabel.setCaption("Prevádzková hmotnosť");
    }

    private void wrap() {
        this.addComponent(this.VINLabel);
        this.addComponent(this.EVCLabel);
        this.addComponent(this.vPatraniLabel);
        this.addComponent(this.datumPlatnostiSTKLabel);
        this.addComponent(this.datumPlatnostiEKLabel);
        this.addComponent(this.pocetNapravLabel);
        this.addComponent(this.prevadzkovaHmostnostLabel);
    }

}
