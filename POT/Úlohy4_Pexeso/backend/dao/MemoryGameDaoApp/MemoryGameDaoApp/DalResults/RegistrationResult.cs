﻿namespace MemoryGameDaoApp.DalResults
{
    public class RegistrationResult
    {
        public bool Successful { get; set; }

        public string UnsuccessReason { get; set; }
    }
}
