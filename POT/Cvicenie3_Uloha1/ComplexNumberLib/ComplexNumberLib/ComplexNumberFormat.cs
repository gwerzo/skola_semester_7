﻿
namespace ComplexNumberLib {
    public enum ComplexNumberFormat
    {
        AlgebraicForm,
        GeometricForm,
        TrigoniometricForm
    }
}
