package sk.uniza.fri.lustiak;

import sk.uniza.fri.lustiak.orders.OrdersService;
import sk.uniza.fri.lustiak.products.ProductService;
import sk.uniza.fri.lustiak.users.UsersService;

public class Main {

    public static void main(String[] args) {

        ProductService productService = new ProductService();
        OrdersService ordersService = new OrdersService();
        UsersService usersService = new UsersService();

        Long productId = 1L;
        Long orderAmount = 10L;

        Double productPrice = productService.getProductPrice(productId);

        Double userBalance = usersService.getLoggedInUser().getUserBalance();

        if (userBalance < productPrice * orderAmount) {
            throw new RuntimeException("Insufficient funds");
        }

        ordersService.createOrderForItemId(usersService.getLoggedInUser(), productId, orderAmount);
        usersService.getLoggedInUser().decreaseUserBalance(productPrice * orderAmount);

    }
}
