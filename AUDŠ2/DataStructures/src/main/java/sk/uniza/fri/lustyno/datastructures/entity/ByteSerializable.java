package sk.uniza.fri.lustyno.datastructures.entity;

public interface ByteSerializable {

    public byte[] toBytesArray();

}
