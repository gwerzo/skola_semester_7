package sk.uniza.fri.lustyno.AudsLetisko.components.airport;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.LietadloTableUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.VyhladanieLietadlaUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.AircraftManagementService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;

public class VyhladanieLietadlaForm extends BusinessComponent {

    private Binder<VyhladanieLietadlaUiEntity> binder;
    private VyhladanieLietadlaUiEntity entity;

    private AircraftManagementService aircraftManagementService;

    private FormLayout lietadloDetails;

    public VyhladanieLietadlaForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_3.getTitle());
        this.aircraftManagementService = UIServices.getAircraftManagementService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new VyhladanieLietadlaUiEntity();
        this.binder = new Binder<>(VyhladanieLietadlaUiEntity.class);

        this.lietadloDetails = new FormLayout();

        VerticalLayout layout = new VerticalLayout();

        FormLayout fl = new FormLayout();

        TextField medzinarodnyKod = new TextField("Medzinárodný kód");

        Button vyhladaj = new Button("Vyhľadaj");
        vyhladaj.addClickListener(e -> {
           try {
               this.lietadloDetails.removeAllComponents();
               this.binder.writeBean(this.entity);
               vyhladajCakajuceLietadlo();
           } catch (ValidationException ex) {
                CustomNotification.error("Musíte vyplniť medzinárodný kód lietadla");
           }
        });

        this.binder.forField(medzinarodnyKod)
                .asRequired()
                .bind(VyhladanieLietadlaUiEntity::getMedzinarodnyKodLietadla, VyhladanieLietadlaUiEntity::setMedzinarodnyKodLietadla);

        fl.addComponent(medzinarodnyKod);
        fl.addComponent(vyhladaj);

        layout.addComponent(fl);
        layout.addComponent(lietadloDetails);

        Panel panelWrapper = new Panel("Vyhľadanie čakajúceho lietadla na pridelenie odletovej dráhy", layout);
        panelWrapper.setWidth("100%");
        panelWrapper.setHeight("100%");

        this.parenComponent.showComponent(panelWrapper);
    }

    private void vyhladajCakajuceLietadlo() {
        HandledOperationExecutor.submit(() -> {
            LietadloTableUiEntity foundEntity =
                    this.aircraftManagementService.getLietadloCakajuceNaPridelenieOdletovejDrahy(this.entity);
            if (foundEntity == null) {
                CustomNotification.warning("Lietadlo s takýmto medzinárodným kódom nečaká na odletovú dráhu");
                this.entity.flush();
                binder.readBean(this.entity);
            } else {
                ukazInfoOLietadle(foundEntity);
            }
            return OperationMarker.OPERATION_SUCCESFUL;
        });
    }

    private void ukazInfoOLietadle(LietadloTableUiEntity foundEntity) {
        this.lietadloDetails.removeAllComponents();

        Label nazovVyrobcu = new Label(foundEntity.getVyrobca());
        nazovVyrobcu.setCaption("Názov výrobcu");

        Label medzinarodnyKod = new Label(foundEntity.getMedzinarodnyKod());
        medzinarodnyKod.setCaption("Medzinárodný kód");

        Label priorita = new Label(foundEntity.getPriorita().toString());
        priorita.setCaption("Priorita");

        Label minimalnaDlzkaDrahy = new Label(foundEntity.getMinDlzkaDrahy().toString());
        minimalnaDlzkaDrahy.setCaption("Minimálna dĺžka odletovej dráhy");

        Label casPriletu = new Label(foundEntity.getCasPriletu().toString());
        casPriletu.setCaption("Čas príletu");

        Label casPoziadavky = new Label(foundEntity.getCasPoziadavkyOPridelenie().toString());
        casPoziadavky.setCaption("Čas požiadavky o pridelenie odletovej dráhy");

        this.lietadloDetails.addComponents(
                nazovVyrobcu,
                medzinarodnyKod,
                priorita,
                minimalnaDlzkaDrahy,
                casPriletu,
                casPoziadavky
        );

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_3.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
