package sk.uniza.fri.lustyno.datastructures.tree.bst;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureInsertDuplicateException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureNullKeyException;
import sk.uniza.fri.lustyno.datastructures.tree.NodeResult;
import sk.uniza.fri.lustyno.datastructures.tree.splay.SplayTree;

import javax.naming.OperationNotSupportedException;
import java.util.*;

/**
 * Implementácia binárneho vyhľadávacieho stromu
 */
public class BinarySearchTree<K extends HasComparableValue, D> {

    /**
     * Koreň stromu
     */
    protected BinarySearchTreeNode<K, D> root;

    /**
     * Aktuálna veľkosť stromu
     */
    protected int size;

    public BinarySearchTree() {
        this.root = null;
    }

    /**
     * Vráti koreň stromu
     * @return koreň stromu
     */
    public BinarySearchTreeNode<K, D> getRoot() {
        return this.root;
    }

    public void insert(K key, D data) {
        this.insert(new BinarySearchTreeNode<K, D>(key, data));
    }

    /**
     * Vloží do stromu nový node, klasickou operáciou porovnávania
     * hodnoty kľúča s aktuálnym nodom, začínajúc v koreni a presúvajúc
     * sa do ľavého syna v prípade nižšej hodnoty kľúča a vpravo v prípade
     * vyššej hodnoty kľúča
     * <p>
     * @param node node na vloženie do stromu
     */
     public void insert(BinarySearchTreeNode<K, D> node) {
        if (node.getKey() == null) {
            throw new DataStructureNullKeyException(this.getClass().getName(), "insert");
        }
        if (root == null) {
            this.size++;
            this.root = node;
            this.root.setParent(null);
        } else {
            BinarySearchTreeNode<K, D> actualNode = root;
            while(actualNode != null) {

                int whenCompared = actualNode.getKey().getValue().compareTo(node.getKey().getValue());

                if (whenCompared == 0) {
                    throw new DataStructureInsertDuplicateException(this.getClass().getName(), node.getKey().getValue().toString(), node.getData().toString());
                }

                if (whenCompared > 0) {
                    if (actualNode.getLeftChildo() == null) {
                        actualNode.setLeftChildo(node);
                        node.setParent(actualNode);
                        this.size++;
                        return;
                    } else {
                        actualNode = actualNode.getLeftChildo();
                    }
                } else {
                    if (actualNode.getRightChild() == null) {
                        actualNode.setRightChild(node);
                        node.setParent(actualNode);
                        this.size++;
                        return;
                    } else {
                        actualNode = actualNode.getRightChild();
                    }
                }
            }
        }
    }

    /**
     * Vyhľadá v strome node s danou hodnotou  kľúča, začínajúc v koreni
     * a presúvajúc sa do ľavého syna v prípade nižšej hodnoty kľúča
     * a vpravo v prípade vyššej hodnoty kľúča
     * <p>
     * V prípade nenajdenia vyhľadávaného nodu vráti tiež result,
     * ale jeho wrapnutou hodnotou bude node, ktorý bol
     * pri algoritme prehľadávania posledne prehľadaný
     * <p>
     * @param   key  comparable kľúč pre nájdenie nodu
     * @return
     *          NodeResult<Node, True>  <=> ak node bol nájdený
     *          NodeResult<Node, False> <=> s daným kľúčom node nebol nájdený, vrátený je posledne prechádzaný node
     */
     public NodeResult<BinarySearchTreeNode<K, D>> find(K key) {
        BinarySearchTreeNode<K, D> actualNode = this.root;
        while (actualNode != null) {
            int whenCompared = actualNode.getKey().getValue().compareTo(key.getValue());
            if (whenCompared == 0) {
                return new NodeResult<>(actualNode, Boolean.TRUE);
            } else {
                if (whenCompared > 0) {
                    if (actualNode.getLeftChildo() == null) {
                        return new NodeResult<>(actualNode, Boolean.FALSE);
                    }
                    actualNode = actualNode.getLeftChildo();
                } else {
                    if (actualNode.getRightChild() == null) {
                        return new NodeResult<>(actualNode, Boolean.FALSE);
                    }
                    actualNode = actualNode.getRightChild();
                }
            }
        }
        return new NodeResult<>(null, Boolean.FALSE);
    }


    /**
     * Vymaže node zo stromu v kľúčom s danou hodnotou z parametra
     * a vykoná potrebné rotácie pre zachovania konzistentnosti stromu
     * <p>
     * @param    key  kľúč na nájdenie
     * @return
     *          NodeResult<Node, True>  <=> ak node bol nájdený
     *          NodeResult<Node, False> <=> s daným kľúčom node nebol nájdený, vrátený je posledne prechádzaný node
     */
     public NodeResult<BinarySearchTreeNode<K, D>> delete(K key) {
        NodeResult<BinarySearchTreeNode<K, D>> nodeToDeleteResult = find(key);
        if(!nodeToDeleteResult.wasFound()) {
            return nodeToDeleteResult;
        }
        BinarySearchTreeNode<K, D> nodeToDelete = nodeToDeleteResult.get();
        // Vymazanie z nodu, ak node bol listom
        if (nodeToDelete.isLeaf()) {
            if (nodeToDelete.isRoot()) {
                this.root = null;
                this.size = 0;
                return nodeToDeleteResult;
            }

            if (nodeToDelete.isRightChildOfParent()) {
                nodeToDelete.getParent().nullizeRightChild();
                this.size--;
                return nodeToDeleteResult;
            } else {
                nodeToDelete.getParent().nullizeLeftChild();
                this.size--;
                return nodeToDeleteResult;
            }
        }

        // Vymazanie z nodu, ak node má práve jedno dieťa
        if (nodeToDelete.hasOnlyOneChild()) {
            BinarySearchTreeNode<K,D> onlyChild = nodeToDelete.getOneChildIfNodeHasOnlyOneChild();

            if (nodeToDelete.isRoot()) {
                this.root = onlyChild;
                this.root.setParent(null);
                this.size--;
                return nodeToDeleteResult;
            }

            if (nodeToDelete.isRightChildOfParent()) {
                nodeToDelete.getParent().setRightChild(onlyChild);
                onlyChild.setParent(nodeToDelete.getParent());
                this.size--;
                return nodeToDeleteResult;
            } else {
                nodeToDelete.getParent().setLeftChildo(onlyChild);
                onlyChild.setParent(nodeToDelete.getParent());
                this.size--;
                return nodeToDeleteResult;
            }
        }

        // Vymazanie z nodu, ak má obe deti
        if (nodeToDelete.hasBothChildren()) {
            BinarySearchTreeNode<K, D> inOrderSuccessor = inOrderSuccessor(nodeToDelete);

            BinarySearchTreeNode<K, D> lavyPodstrom = nodeToDelete.getLeftChildo();
            BinarySearchTreeNode<K, D> pravyPodstrom = nodeToDelete.getRightChild();

            // Ak vymazávaným nodom je root
            if (nodeToDelete.isRoot()) {

                // Ak inOrderSuccessor je priamy potomok vymazávaného (root) nodu
                if (inOrderSuccessor.equals(pravyPodstrom)) {
                    inOrderSuccessor.setParent(null);
                    inOrderSuccessor.setLeftChildo(lavyPodstrom);
                    lavyPodstrom.setParent(inOrderSuccessor);
                    this.root = inOrderSuccessor;
                    this.size--;
                    return nodeToDeleteResult;
                }
                // Ak inOrderSuccessor nie je priamy potomok vymazávaného nodu
                else {
                    inOrderSuccessor.setLeftChildo(lavyPodstrom);
                    lavyPodstrom.setParent(inOrderSuccessor);
                    inOrderSuccessor.getParent().setLeftChildo(inOrderSuccessor.getRightChild());
                    if (inOrderSuccessor.getRightChild() != null) {
                        inOrderSuccessor.getRightChild().setParent(inOrderSuccessor.getParent());
                    }
                    inOrderSuccessor.setRightChild(pravyPodstrom);
                    pravyPodstrom.setParent(inOrderSuccessor);
                    inOrderSuccessor.setParent(null);
                    this.root = inOrderSuccessor;
                    this.size--;
                    return nodeToDeleteResult;
                }

            }
            // Ak vymazávaným nodom nie je root
            else {
                BinarySearchTreeNode<K, D> nP = nodeToDelete.getParent();
                // Ak inOrderSuccessor je priamy potomok vymazávaného nodu
                if (inOrderSuccessor.equals(pravyPodstrom)) {
                    lavyPodstrom.setParent(inOrderSuccessor);
                    pravyPodstrom.setParent(inOrderSuccessor);
                    inOrderSuccessor.setLeftChildo(lavyPodstrom);

                    inOrderSuccessor.setParent(nP);
                    if (nodeToDelete.isRightChildOfParent()) {
                        nP.setRightChild(inOrderSuccessor);
                    } else {
                        nP.setLeftChildo(inOrderSuccessor);
                    }

                    size--;
                    return nodeToDeleteResult;
                }
                // Ak inOrderSuccessor je nepriamy potomok vymazávaného nodu
                else {
                    lavyPodstrom.setParent(inOrderSuccessor);
                    pravyPodstrom.setParent(inOrderSuccessor);
                    inOrderSuccessor.getParent().setLeftChildo(inOrderSuccessor.getRightChild());
                    if (inOrderSuccessor.getRightChild() != null) {
                        inOrderSuccessor.getRightChild().setParent(inOrderSuccessor.getParent());
                    }
                    inOrderSuccessor.setRightChild(pravyPodstrom);
                    inOrderSuccessor.setLeftChildo(lavyPodstrom);

                    inOrderSuccessor.setParent(nP);
                    if (nodeToDelete.isRightChildOfParent()) {
                        nP.setRightChild(inOrderSuccessor);
                    } else {
                        nP.setLeftChildo(inOrderSuccessor);
                    }

                    size--;
                    return nodeToDeleteResult;
                }
            }
        }
        return null;
    }

    /**
     * Vráti inorder nasledovnika daného nodu,
     * inými slovami vráti najľavejší node pravého podstromu nodu
     *
     * @return inorder nasledovník nodu
     */
    public BinarySearchTreeNode<K, D> inOrderSuccessor(BinarySearchTreeNode<K, D> fromNode) {

        if (fromNode.getRightChild() != null) {
            return findMin(fromNode.getRightChild());
        }

        BinarySearchTreeNode<K, D> actualNode = fromNode.getParent();
        while (actualNode != null && fromNode == actualNode.getRightChild()) {
            fromNode = actualNode;
            actualNode = actualNode.getParent();
        }
        return actualNode;
    }

    /**
     * Z nodu nájde minimálny node
     * t.j. jeho inOrder successor
     */
    private BinarySearchTreeNode<K, D> findMin(BinarySearchTreeNode<K, D> fromNode) {
        BinarySearchTreeNode<K, D> current = fromNode;

        while (current.getLeftChildo() != null) {
            current = current.getLeftChildo();
        }
        return current;
    }


    public int getSize() {
        return this.size;
    }

    private BinarySearchTree<K, D> clone(BinarySearchTree<K, D> toClone) throws OperationNotSupportedException{
        throw new OperationNotSupportedException("Implement if needed");
    }

    public List<BinarySearchTreeNode<K, D>> inOrder() {
        if (this.root == null) {
            return null;
        }

        List<BinarySearchTreeNode<K, D>> listToRet = new LinkedList<>();
        Stack<BinarySearchTreeNode<K, D>> stack = new Stack<>();

        BinarySearchTreeNode<K, D> root = this.root;
        while (root != null || stack.size() > 0) {
            while (root !=  null) {
                stack.push(root);
                root = root.getLeftChildo();
            }

            root = stack.pop();

            listToRet.add(root);

            root = root.getRightChild();
        }
        return listToRet;
    }

    public List<BinarySearchTreeNode<K, D>> levelOrder() {
        if (this.root == null) {
            return null;
        }

        List<BinarySearchTreeNode<K, D>> toRet = new ArrayList<>(this.size);

        LinkedList<BinarySearchTreeNode<K, D>> queue = new LinkedList<>();
        queue.add(this.root);
        while (!queue.isEmpty()) {
            BinarySearchTreeNode<K, D> node = queue.removeFirst();
            toRet.add(node);
            if (node.getLeftChildo() != null) {
                queue.add(node.getLeftChildo());
            }
            if (node.getRightChild() != null) {
                queue.add(node.getRightChild());
            }
        }
        return toRet;
    }


    /**
     * Jednoduchá pravá rotácia nodu N do prava,
     * t.j. nahradenie otca nodu N samotným nodom N
     *
     *          R(root)                                             N
     *          /     \                                            / \
     *         /       \                        =>                /   \
     *        N         RC(root.rightChild)                     T1    R(root)
     *       / \                                                       / \
     *     T1   T2                                                   T2   RC(root.rightChild)
     *
     * @param nodeToRotateToRight node, ktorý je ľavým synom roota
     */
    protected void simpleRightRotationOfNodeOverRoot(BinarySearchTreeNode<K, D> nodeToRotateToRight) {

        BinarySearchTreeNode<K, D> t2 = nodeToRotateToRight.getRightChild();
        BinarySearchTreeNode<K, D> r = nodeToRotateToRight.getParent();

        r.setLeftChildo(t2);
        if (t2 != null) {
            t2.setParent(r);
        }

        r.setParent(nodeToRotateToRight);
        nodeToRotateToRight.setRightChild(r);
        nodeToRotateToRight.setParent(null);

        this.root = nodeToRotateToRight;
    }

    /**
     * Rovnaká rotácia ako pri
     * {@link SplayTree#simpleRightRotationOfNodeOverRoot(BinarySearchTreeNode)}
     * len otočená, t.j. node N je pravým synom roota
     *
     * @param nodeToRotateToLeft - node, ktorý je pravým synom roota
     */
    protected void simpleLeftRotationOfNodeOverRoot(BinarySearchTreeNode<K, D> nodeToRotateToLeft) {

        BinarySearchTreeNode<K, D> t1 = nodeToRotateToLeft.getLeftChildo();
        BinarySearchTreeNode<K, D> r = nodeToRotateToLeft.getParent();


        r.setRightChild(t1);
        if (t1 != null) {
            t1.setParent(r);
        }


        r.setParent(nodeToRotateToLeft);
        nodeToRotateToLeft.setLeftChildo(r);
        nodeToRotateToLeft.setParent(null);

        this.root = nodeToRotateToLeft;
    }

    /**
     * Jednoduchá ľavá rotácia nodu,
     * v prípade že otec nodu nie je root
     * a node je pravým synom otca
     *
     * @param nodeToRotateToLeft - node, ktorý sa má vyrotovať doľava, t.j. dostať sa na pozíciu svojho parenta
     */
    protected void simpleLeftRotationOfNode(BinarySearchTreeNode<K, D> nodeToRotateToLeft) {
        if (!nodeToRotateToLeft.isRightChildOfParent()) {
            throw new RuntimeException("Robíš ľavú rotáciu keď node je pravým synom");
        }
        BinarySearchTreeNode<K, D> parent = nodeToRotateToLeft.getParent();
        BinarySearchTreeNode<K, D> grandParent = nodeToRotateToLeft.getParent().getParent();

        BinarySearchTreeNode<K, D> t1 = nodeToRotateToLeft.getLeftChildo();

        boolean parentWasRightChild = parent.isRightChildOfParent();

        parent.setRightChild(t1);
        if (t1 != null) {
            t1.setParent(parent);
        }

        parent.setParent(nodeToRotateToLeft);
        nodeToRotateToLeft.setLeftChildo(parent);
        nodeToRotateToLeft.setParent(grandParent);

        if (parentWasRightChild) {
            grandParent.setRightChild(nodeToRotateToLeft);
        } else {
            grandParent.setLeftChildo(nodeToRotateToLeft);
        }
    }

    /**
     * Jednoduchá pravá rotácia nodu,
     * v prípade že otec nodu nie je root
     * a node je ľavým synom otca
     *
     * @param nodeToRotateToRight
     */
    protected void simpleRightRotationOfNode(BinarySearchTreeNode<K, D> nodeToRotateToRight) {
        if (nodeToRotateToRight.isRightChildOfParent()) {
            throw new RuntimeException("Robíš pravú rotáciu keď node je ľavým synom");
        }

        BinarySearchTreeNode<K, D> parent = nodeToRotateToRight.getParent();
        BinarySearchTreeNode<K, D> grandParent = nodeToRotateToRight.getParent().getParent();

        BinarySearchTreeNode<K, D> t2 = nodeToRotateToRight.getRightChild();

        boolean parentWarRightChild = parent.isRightChildOfParent();

        parent.setLeftChildo(t2);
        if (t2 != null) {
            t2.setParent(parent);
        }

        parent.setParent(nodeToRotateToRight);
        nodeToRotateToRight.setRightChild(parent);
        nodeToRotateToRight.setParent(grandParent);


        if (parentWarRightChild) {
            grandParent.setRightChild(nodeToRotateToRight);
        } else {
            grandParent.setLeftChildo(nodeToRotateToRight);
        }
    }
}
