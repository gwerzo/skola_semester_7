package sk.uniza.fri.lustyno.datastructures.extendiblehashing.entities;

import sk.uniza.fri.lustyno.datastructures.extendiblehashing.Constants;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;

import java.util.Arrays;

public abstract class EHEntity<T> implements ICustomSerializable<T>, IHasSize, IBitHashable {

    protected Boolean isValid = Boolean.FALSE;

    /**
     * Nabalí interné atribúty + atribúty z podedenej triedy
     * @return
     */
    public final byte[] toByteArrayInternally() {
        return ToByteArrayConverter.concatenateByteArrays(
                Arrays.asList(
                    ToByteArrayConverter.bytesFromBoolean(this.isValid),
                    this.toByteArray()
                )
        );
    }

    /**
     * Vráti internú veľkosť entity + veľkosť rozširovanej entity
     */
    public final int getSizeInternally() {
        return
                Constants.JAVA_BOOLEAN_SIZE +
                        this.getSize();
    }

    /**
     * Zkonštruuje entitu z interných + verejných častí (verejná časť je v podedenej entite)
     * @return
     */
    public final InternalEntity fromByteArrayInternally(byte[] byteArray) {
        Boolean validity = FromByteArrayConverter.booleanFromByte(byteArray[0]);
        T fromByteArrayEntity = this.fromByteArray(Arrays.copyOfRange(byteArray, 1, byteArray.length));
        return new InternalEntity(fromByteArrayEntity, validity);
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public Boolean isValid() {
        return this.isValid;
    }

    @Override
    public abstract byte[] toByteArray();

    @Override
    public abstract T fromByteArray(byte[] byteArray);

    @Override
    public abstract int getSize();

    @Override
    public abstract int getHash();

    public abstract boolean equalsOtherEntity(T other);

    public abstract T clone();

    public class InternalEntity {
        private Boolean isValid;
        private T entity;

        public InternalEntity(T entity, Boolean isValid) {
            this.entity = entity;
            this.isValid = isValid;
        }

        public T getEntity() {
            return this.entity;
        }

        public Boolean getIsValid() {
            return this.isValid;
        }
    }

}
