package sk.uniza.fri.lustyno.fripd.utils;

import com.vaadin.server.BootstrapFragmentResponse;
import com.vaadin.server.BootstrapListener;
import com.vaadin.server.BootstrapPageResponse;
import com.vaadin.server.SessionInitListener;
import com.vaadin.spring.server.SpringVaadinServlet;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;

@Component("vaadinServlet")
public class Faviconizer extends SpringVaadinServlet {

    @Override
    protected void servletInitialized() throws ServletException {
        super.servletInitialized();

        getService().addSessionInitListener((SessionInitListener) event -> event.getSession().addBootstrapListener(new BootstrapListener() {

            @Override
            public void modifyBootstrapFragment(
                    BootstrapFragmentResponse response) {

            }

            @Override
            public void modifyBootstrapPage(BootstrapPageResponse response) {
                response.getDocument().head().
                        getElementsByAttributeValue("rel", "shortcut icon").attr("href", "./VAADIN/themes/material/favicon.ico");
                response.getDocument().head()
                        .getElementsByAttributeValue("rel", "icon")
                        .attr("href", "./VAADIN/themes/material/favicon.ico");
            }}
        ));

    }

}
