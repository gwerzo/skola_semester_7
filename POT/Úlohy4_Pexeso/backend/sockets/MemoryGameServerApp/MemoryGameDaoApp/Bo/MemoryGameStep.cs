﻿namespace PexesoApi.Bo
{
    public class MemoryGameStep
    {
        public int MemoryGameStepId { get; set; }

        public int StepCount { get; set; }

        public MemoryGame MemoryGame { get; set; }

        public int Hit_X { get; set; }

        public int Hit_Y { get; set; }

        public long TookMilis { get; set; }

        public MemoryGameStep()
        {

        }

    }
}
