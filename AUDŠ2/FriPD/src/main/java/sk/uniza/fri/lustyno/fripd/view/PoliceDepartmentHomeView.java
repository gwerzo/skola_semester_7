package sk.uniza.fri.lustyno.fripd.view;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import sk.uniza.fri.lustyno.fripd.components.common.*;
import sk.uniza.fri.lustyno.fripd.components.common.MenuComponent;


@SpringUI
@Theme("material")
@Title(value = "FriPD")
public class PoliceDepartmentHomeView extends GenericVerticalView {

    private RepaintableComponent contentComponent;

    private MenuComponent menu;

    private CardsMenuComponent cardsMenuComponent;

    private SystemMenuComponent systemMenuComponent;

    private HomeViewForm homeViewForm;

    private CardsManagementComponent systemManagementViewForm;

    private Boolean menuShown = true;

    public PoliceDepartmentHomeView() {
        super();
        initComponents();
    }

    private void initComponents() {

        Panel p = new Panel("Aplikácia FriPD");
        p.setWidth("100%");
        p.setHeight("100%");
        p.setIcon(FontAwesome.CAR);

        this.homeViewForm = new HomeViewForm();
        this.systemManagementViewForm = new CardsManagementComponent();
        this.contentComponent = new RepaintableComponent(this.homeViewForm);
        this.menu = new MenuComponent(contentComponent);
        this.cardsMenuComponent = new CardsMenuComponent(contentComponent);
        this.systemMenuComponent = new SystemMenuComponent(contentComponent);

        HorizontalLayout wrapper = new HorizontalLayout();
        wrapper.setWidth("100%");
        wrapper.setHeight("100%");

        this.menu.setWidth("100%");
        this.menu.setIcon(FontAwesome.CAR);
        this.menu.setCaption("Vozidlá");

        this.cardsMenuComponent.setWidth("100%");
        this.cardsMenuComponent.setIcon(FontAwesome.CREDIT_CARD);
        this.cardsMenuComponent.setCaption("Preukazy");

        this.systemMenuComponent.setWidth("100%");
        this.systemMenuComponent.setIcon(FontAwesome.WRENCH);
        this.systemMenuComponent.setCaption("Systém");

        this.contentComponent.setWidth("100%");

        TabSheet menuWrapper = new TabSheet();
        menuWrapper.addTab(this.menu);
        menuWrapper.addTab(this.cardsMenuComponent);
        menuWrapper.addTab(this.systemMenuComponent);

        GridLayout grid = new GridLayout(6, 1);
        grid.setSizeFull();
        grid.addComponent(menuWrapper, 0, 0, 1, 0);
        grid.addComponent(this.contentComponent, 2, 0, 5, 0);

//        menuWrapper.addSelectedTabChangeListener(selectedTabChangeEvent -> {
//            if (!menuShown) {
//                this.contentComponent.removeAllComponents();
//                this.contentComponent.addComponent(this.homeViewForm);
//                menuShown = !menuShown;
//            } else {
//                this.contentComponent.removeAllComponents();
//                this.contentComponent.addComponent(this.systemManagementViewForm);
//                menuShown = !menuShown;
//            }
//        });

        menuWrapper.setSizeFull();

        p.setContent(grid);

        addComponent(p);
    }


}
