﻿using System;

namespace FriBettingWebApplication.Dto.Request
{
    public class CreateNewMatchRequest
    {
        public int SportId { get; set; }
        public int HostId { get; set; }
        public int GuestId { get; set; }
        public DateTime DateTimeOfMatch { get; set; }

    }
}