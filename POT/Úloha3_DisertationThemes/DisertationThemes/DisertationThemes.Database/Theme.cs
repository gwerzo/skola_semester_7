﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DisertationThemes.Database
{
    public class Theme
    {
        public int ThemeId { get; set; }
        public int StProgramId { get; set; }
        public int SupervisorId { get; set; }
        public DateTime Created { get; set; }
        public string Description { get; set; }
        public bool IsExternalStudy { get; set; }
        public bool IsFullTimeStudy { get; set; }
        public string Name { get; set; }
        public ResearchType ResearchType { get; set; }


        public virtual StProgram StProgram { get; set; }
        public virtual Supervisor Supervisor { get; set; }

    }
}
