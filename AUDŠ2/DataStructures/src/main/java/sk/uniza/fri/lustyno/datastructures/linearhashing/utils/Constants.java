package sk.uniza.fri.lustyno.datastructures.linearhashing.utils;

import java.time.LocalDateTime;

public class Constants {

    public static final Integer LOCAL_DATE_TIME_STRING_SIZE     = LocalDateTime.now().toString().length();

    public static final Integer JAVA_INTEGER_SIZE               = 4;

    public static final Integer INITIAL_BLOCKS_COUNT            = 4;

    public static final String DB_FILE_POSTFIX                  = ".lldb";

    public static final String DB_OVERFLOW_FILE_PREFIX          = "overflow_";

    public static final String DB_PREFIX                        = "DATABASE/";


    public static final Double SPLIT_MAXIMUM_THRESHOLD          = .3;
    public static final Double SPLIT_MINIMUM_THRESHOLD          = .33;


}
