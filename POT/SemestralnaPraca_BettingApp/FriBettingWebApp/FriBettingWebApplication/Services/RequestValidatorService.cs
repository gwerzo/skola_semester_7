﻿using System.Web.WebPages;
using FriBettingWebApplication.Dto.Request;

namespace FriBettingWebApplication.Services
{
    /// <summary>
    /// Validačná trieda s iba statickými metódami na validovanie vstupov z NON-BIZNIS pohľadu
    /// </summary>
    public class RequestValidatorService
    {

        public static void ValidateRegistrationRequest(RegisterUserRequest request, out ValidationResult result)
        {
            if (request.Username == null || request.Username.Trim().IsEmpty())
            {
                result =  new ValidationResult
                {
                    Successful = false,
                    ErrorMessage = "Missing username"
                };
                return;
            }

            if (request.Password == null || request.Password.Trim().IsEmpty())
            {
                result = new ValidationResult
                {
                    Successful = false,
                    ErrorMessage = "Missing password"
                };
                return;
            }

            if (request.PasswordAgain == null || request.PasswordAgain.Trim().IsEmpty())
            {
                result =  new ValidationResult
                {
                    Successful = false,
                    ErrorMessage = "Missing password (again)"
                };
                return;
            }

            result = new ValidationResult
            {
                Successful = true,
                ErrorMessage = null
            };
        }


        public static void ValidateLoginRequest(LoginUserRequest request, out ValidationResult result)
        {

            if (request.Username == null || request.Username.Trim().IsEmpty())
            {
                result = new ValidationResult
                {
                    Successful = false,
                    ErrorMessage = "Missing username"
                };
                return;
            }

            if (request.Password == null || request.Password.Trim().IsEmpty())
            {
                result = new ValidationResult
                {
                    Successful = false,
                    ErrorMessage = "Missing password"
                };
                return;
            }

            result = new ValidationResult
            {
                Successful = true,
                ErrorMessage = null
            };
        }
    }
}