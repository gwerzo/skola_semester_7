package sk.uniza.fri.lustyno.AudsLetisko.entities_bo;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class HistoriaOdletuEntity {

    private LietadloEntity lietadloEntity;

    private OdletovaDrahaEntity odletovaDraha;

    private LocalDateTime casDatumOdletu;

    private LocalDateTime casDatumPriletu;

}
