package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.Data;

@Data
public class CreateNewRealEstateUi {

    private Integer identifikacneCislo;

    private Double latitude1;
    private Double longitude1;
    private Boolean isNorth1;
    private Boolean isEast1;

    private Double latitude2;
    private Double longitude2;
    private Boolean isNorth2;
    private Boolean isEast2;

    private Integer supisneCislo;
    private String popis;

    public void flush() {
        this.supisneCislo = null;
        this.identifikacneCislo = null;
        this.popis = null;
        this.latitude1 = 0.0;
        this.longitude1 = 0.0;
        this.latitude2 = 0.0;
        this.longitude2 = 0.0;
        this.isEast1 = Boolean.TRUE;
        this.isNorth1 = Boolean.TRUE;
        this.isEast2 = Boolean.TRUE;
        this.isNorth2 = Boolean.TRUE;
    }

}
