package sk.uniza.fri.lustyno.AudsLetisko.components.airport;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.PriletLietadlaUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entity_mapper.EntityMapper;
import sk.uniza.fri.lustyno.AudsLetisko.service.AircraftManagementService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;
import sk.uniza.fri.lustyno.AudsLetisko.utils.converters.StringToIntegerConverter;

public class ZaevidovaniePriletuForm extends BusinessComponent {

    private Binder<PriletLietadlaUiEntity> binder;
    private PriletLietadlaUiEntity entity;

    private EntityMapper entityMapper;

    private AircraftManagementService aircraftManagementService;

    public ZaevidovaniePriletuForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_1.getTitle());
        this.entity = new PriletLietadlaUiEntity();
        this.entityMapper = UIServices.getEntityMapper();
        this.aircraftManagementService = UIServices.getAircraftManagementService();
        this.entity = new PriletLietadlaUiEntity();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new PriletLietadlaUiEntity();
        this.binder = new Binder<>(PriletLietadlaUiEntity.class);

        VerticalLayout layout = new VerticalLayout();

        FormLayout formLayout = new FormLayout();

        TextField nazovVyrobcuTF = new TextField("Názov výrobcu");
        TextField medzinarodnyKodTF = new TextField("Medzinárodný kód lietadla");
        TextField prioritaTF = new TextField("Priorita lietadla");
        TextField minimalnaDlzkaDrahyTF = new TextField("Minimálna dĺžka odletovej dráhy");
        Button vytvoritPriletButton = new Button("Vytvoriť prílet");
        vytvoritPriletButton.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                vytvorPrilet(this.entity);
            } catch (ValidationException ex) {
                this.entity.flush();
                ex.printStackTrace();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        formLayout.addComponent(nazovVyrobcuTF);
        formLayout.addComponent(medzinarodnyKodTF);
        formLayout.addComponent(prioritaTF);
        formLayout.addComponent(minimalnaDlzkaDrahyTF);
        formLayout.addComponent(vytvoritPriletButton);

        layout.addComponent(formLayout);

        binder.forField(nazovVyrobcuTF)
                .asRequired()
                .bind(PriletLietadlaUiEntity::getNazovVyrobcu, PriletLietadlaUiEntity::setNazovVyrobcu);
        binder.forField(medzinarodnyKodTF)
                .asRequired()
                .bind(PriletLietadlaUiEntity::getMedzinarodnyKod, PriletLietadlaUiEntity::setMedzinarodnyKod);
        binder.forField(prioritaTF)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(PriletLietadlaUiEntity::getPriorita, PriletLietadlaUiEntity::setPriorita);
        binder.forField(minimalnaDlzkaDrahyTF)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(PriletLietadlaUiEntity::getMinimalnaDlzkaOdletovejDrahy, PriletLietadlaUiEntity::setMinimalnaDlzkaOdletovejDrahy);

        this.parenComponent.showComponent(new Panel("Údaje o prílete lietadla", layout));
    }

    private void vytvorPrilet(PriletLietadlaUiEntity entity) {
        HandledOperationExecutor.submit(() -> {
                OperationMarker m = this.aircraftManagementService.zaevidujPriletLietadla(entityMapper.fromPriletLietadlaUiEntity(this.entity));
                if (m == OperationMarker.OPERATION_SUCCESFUL) {
                    CustomNotification.notification("Prílet lietadla bol zaevidovaný");
                    this.entity.flush();
                    binder.readBean(this.entity);
                }
                return m;
        });
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_1.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
