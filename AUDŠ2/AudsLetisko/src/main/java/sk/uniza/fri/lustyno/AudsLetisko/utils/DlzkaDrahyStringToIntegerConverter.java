package sk.uniza.fri.lustyno.AudsLetisko.utils;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;
import sk.uniza.fri.lustyno.AudsLetisko.codelists.DrahyCiselnik;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;

public class DlzkaDrahyStringToIntegerConverter implements Converter<String, Integer> {

    private static final long serialVersionUID = 1L;

    @Override
    public Result<Integer> convertToModel(String s, ValueContext valueContext) {
        Integer i;
        try {
            i = new Integer(s);
        } catch (Exception e) {
            return Result.error("Nesprávny formát čísla");
        }
        DrahyCiselnik drahyCiselnik = UIServices.getDrahyCiselnik();

        if (!drahyCiselnik.getAllDlzkyDrah().contains(i)) {
            StringBuilder sb = new StringBuilder();
            for (Integer dlzka : drahyCiselnik.getAllDlzkyDrah()) {
                sb.append(dlzka + " ");
            }
            return Result.error("Nepovolená dĺžka.<br>Povolené sú len dĺžky<br>" + sb);
        }
        return Result.ok(i);
    }

    @Override
    public String convertToPresentation(Integer integer, ValueContext valueContext) {
        return integer.toString();
    }

}
