package sk.uniza.fri.lustyno.fripd.components.system.vehicle.evc;

import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.fripd.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.fripd.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.fripd.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.fripd.entities_ui.block.BlockDetailEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.block.ZaznamEntityUi;
import sk.uniza.fri.lustyno.fripd.service.UIServices;
import sk.uniza.fri.lustyno.fripd.service.VehicleService;
import sk.uniza.fri.lustyno.fripd.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.fripd.utils.OperationMarker;

import java.util.List;

public class VozidlaEVCPrimarneBlokyForm extends BusinessComponent {

    private VehicleService vehicleService;

    private Panel panel;

    public VozidlaEVCPrimarneBlokyForm(RepaintableComponent parentComponent) {
        super(parentComponent, MENU_TITLE.SYSTEM_MENU_4.getTitle());
        this.vehicleService = UIServices.getVehicleService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        this.panel = new Panel("Detaily blokov");
        this.panel.setSizeFull();

        HandledOperationExecutor.submit(() -> {
            List<BlockDetailEntityUi> bloky = this.vehicleService.getAllVehiclesByEVCPrimaryBlocks();
            renderBlocks(bloky);
            return OperationMarker.OPERATION_SUCCESFUL;
        });


        this.parenComponent.showComponent(this.panel);
    }

    private void renderBlocks(List<BlockDetailEntityUi> bloky) {

        VerticalLayout vl = new VerticalLayout();
        vl.setSizeUndefined();
        vl.setWidth("100%");

        for (BlockDetailEntityUi entityUi : bloky) {

            VerticalLayout layout = new VerticalLayout();

            FormLayout fl = new FormLayout();
            Label offsetBloku = new Label(entityUi.getOffsetVSubore().toString());
            offsetBloku.setCaption("Offset bloku");
            Label offsetPreplnovaciehoBloku = new Label(entityUi.getOffsetPreplnovaciehoBloku().toString());
            offsetPreplnovaciehoBloku.setCaption("Offset preplňovacieho bloku");
            Label pocetValidnychRecordov = new Label(entityUi.getPocetValidnychZaznamov().toString());
            pocetValidnychRecordov.setCaption("Počet validnych záznamov");

            fl.addComponent(offsetBloku);
            fl.addComponent(offsetPreplnovaciehoBloku);
            fl.addComponent(pocetValidnychRecordov);

            Grid<ZaznamEntityUi> gridZaznamovBloku = new Grid<>();
            gridZaznamovBloku.addColumn(ZaznamEntityUi::getKluc).setCaption("Kľúč záznamu");
            gridZaznamovBloku.addColumn(ZaznamEntityUi::getValidny).setCaption("Validny");

            gridZaznamovBloku.setSizeFull();
            gridZaznamovBloku.setItems(entityUi.getZaznamyBloku());

            layout.addComponent(fl);
            layout.addComponent(gridZaznamovBloku);
            layout.setSizeUndefined();
            layout.setWidth("100%");

            Panel pp = new Panel("Blok č." + entityUi.getCisloBloku(), layout);
            pp.setWidth("100%");

            vl.addComponent(pp);
        }

        this.panel.setContent(vl);

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_4.getTitle();
    }
}

