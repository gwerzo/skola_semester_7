package sk.uniza.fri.lustyno.fripd.indexes.bases;

import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Record;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;

import java.util.Arrays;

public abstract class IntegerIndex extends Record<IntegerKey> {

    protected IntegerKey key;

    protected Integer offset;

    public IntegerIndex(Integer key, Integer offset) {
        this.key = new IntegerKey(key);
        this.offset = offset;
    }

    @Override
    public IntegerKey getKey() {
        return this.key;
    }

    public Integer getOffset() {
        return this.offset;
    }

    @Override
    public byte[] toBytesArray() {
        byte[] bytes = ToByteArrayConverter.concatenateByteArrays(
                Arrays.asList(
                        ToByteArrayConverter.bytesFromBoolean(this.valid),
                        ToByteArrayConverter.bytesFromInt(this.key.getValue()),
                        ToByteArrayConverter.bytesFromInt(this.offset)));
        return bytes;
    }

    @Override
    public int getHash() {
        return this.key.getValue();
    }
}
