package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.Data;

@Data
public class GenerovanieSystemuUi {

    private int pocetParciel;
    private int pocetNehnutelnosti;

    public void flush() {
        this.pocetParciel = 0;
        this.pocetNehnutelnosti = 0;
    }

}
