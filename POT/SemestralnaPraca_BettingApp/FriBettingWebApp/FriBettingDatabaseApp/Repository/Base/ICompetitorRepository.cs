﻿using System.Collections.Generic;
using FriBettingDatabaseApp.Bo;

namespace FriBettingDatabaseApp.Repository.Base
{
    public interface ICompetitorRepository: IRepository<Competitor>
    {

        ICollection<Competitor> GetAllCompetitorsCompetingInSport(int sportId);
        ICollection<Competitor> GetAllCompetitorsCompetingInSportWithNameLike(int sportId, string nameLike);

    }
}
