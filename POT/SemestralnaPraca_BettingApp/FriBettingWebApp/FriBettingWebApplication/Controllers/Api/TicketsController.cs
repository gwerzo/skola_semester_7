﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using FriBettingWebApplication.Dto.Request;
using FriBettingWebApplication.Filters;
using FriBettingWebApplication.Security;
using FriBettingWebApplication.Services.Face;

namespace FriBettingWebApplication.Controllers.Api
{
    public class TicketsController : ApiController
    {
        private readonly ITicketService _ticketService;

        public TicketsController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [HttpPost]
        [Route("api/tickets/addodd")]
        [JwtAuthentication]
        public IHttpActionResult AddOddOnUserTicket(HttpRequestMessage request, [FromBody] AddOddToTicketRequest addOddToTicketRequest)
        {
            var username = JwtSecurityManager.ExtractUsernameFromToken(request.Headers.GetValues("Authorization").FirstOrDefault());
            return Ok(_ticketService.AddOddToOpenedTicket(addOddToTicketRequest, username));
        }

        [HttpGet]
        [Route("api/tickets/current")]
        [JwtAuthentication]
        public IHttpActionResult GetCurrentTicket(HttpRequestMessage request)
        {
            var username = JwtSecurityManager.ExtractUsernameFromToken(request.Headers.GetValues("Authorization").FirstOrDefault());
            return Ok(_ticketService.GetCurrentTicketForUser(username));
        }

        [HttpPost]
        [Route("api/tickets/create")]
        [JwtAuthentication]
        public IHttpActionResult CreateTicket(HttpRequestMessage request, [FromBody] CreateCurrentTicketRequest createCurrentTicketRequest)
        {
            var username = JwtSecurityManager.ExtractUsernameFromToken(request.Headers.GetValues("Authorization").FirstOrDefault());
            return Ok(_ticketService.CreateTicket(createCurrentTicketRequest, username));
        }

        [HttpGet]
        [Route("api/tickets/closed")]
        [JwtAuthentication]
        public IHttpActionResult GetAllUserClosedAndNotEvaluatedTickets(HttpRequestMessage request)
        {
            var username = JwtSecurityManager.ExtractUsernameFromToken(request.Headers.GetValues("Authorization").FirstOrDefault());
            return Ok(_ticketService.GetAllClosedAndNotEvaluatedTicketsForUser(username));
        }


        [HttpGet]
        [Route("api/tickets/evaluated")]
        [JwtAuthentication]
        public IHttpActionResult GetAllUserEvaluatedTickets(HttpRequestMessage request)
        {
            var username = JwtSecurityManager.ExtractUsernameFromToken(request.Headers.GetValues("Authorization").FirstOrDefault());
            return Ok(_ticketService.GetAllEvaluatedTicketsForUser(username));
        }

    }
}
