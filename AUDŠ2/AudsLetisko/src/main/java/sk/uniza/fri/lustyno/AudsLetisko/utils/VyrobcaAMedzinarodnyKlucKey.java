package sk.uniza.fri.lustyno.AudsLetisko.utils;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;


public class VyrobcaAMedzinarodnyKlucKey implements HasComparableValue<VyrobcaAMedzinarodnyKlucKey.VyrobcaAMedzinarodnyKluc>{

    private VyrobcaAMedzinarodnyKlucKey.VyrobcaAMedzinarodnyKluc vyrobcaAMedzinarodnyKluc;

    public VyrobcaAMedzinarodnyKlucKey(VyrobcaAMedzinarodnyKlucKey.VyrobcaAMedzinarodnyKluc key) {
        this.vyrobcaAMedzinarodnyKluc = key;
    }

    public VyrobcaAMedzinarodnyKlucKey(String vyrobca, String medzinarodnyKod) {
        this.vyrobcaAMedzinarodnyKluc = new VyrobcaAMedzinarodnyKlucKey.VyrobcaAMedzinarodnyKluc(vyrobca, medzinarodnyKod);
    }

    @Override
    public VyrobcaAMedzinarodnyKlucKey.VyrobcaAMedzinarodnyKluc getValue() {
        return vyrobcaAMedzinarodnyKluc;
    }

    @Override
    public HasComparableValue<VyrobcaAMedzinarodnyKlucKey.VyrobcaAMedzinarodnyKluc> getMinValue() {
        return new VyrobcaAMedzinarodnyKlucKey(new VyrobcaAMedzinarodnyKlucKey.VyrobcaAMedzinarodnyKluc("ZZZZZZZZZZZ", "ZZZZZZZZZZZ"));
    }

    @Override
    public String toString() {
        return this.vyrobcaAMedzinarodnyKluc.vyrobca + " " + this.vyrobcaAMedzinarodnyKluc.medzinarodnyKod;
    }

    public class VyrobcaAMedzinarodnyKluc implements Comparable<VyrobcaAMedzinarodnyKlucKey.VyrobcaAMedzinarodnyKluc> {

        private String vyrobca;

        private String medzinarodnyKod;

        public VyrobcaAMedzinarodnyKluc(String vyrobca, String medzinarodnyKod) {
            this.vyrobca = vyrobca;
            this.medzinarodnyKod = medzinarodnyKod;
        }

        @Override
        public int compareTo(VyrobcaAMedzinarodnyKlucKey.VyrobcaAMedzinarodnyKluc o) {
            int vyrobcaCompared = this.vyrobca.compareTo(o.vyrobca);

            // Ak majú rôzne priority, vráť porovnanie priorít
            if (vyrobcaCompared != 0) {
                return vyrobcaCompared;
            }
            // Ak majú rovnaké priority, porovnaj časy
            else {
                return this.medzinarodnyKod.compareTo(o.medzinarodnyKod);
            }
        }

        @Override
        public String toString() {
            return this.vyrobca + " " + this.medzinarodnyKod;
        }
    }
}
