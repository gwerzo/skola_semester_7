﻿using System.Collections.Generic;

namespace FriBettingDatabaseApp.Bo
{
    /// <summary>
    /// Class reprezentácia typu stávky (na koniec zápasu, na prvú tretinu,..)
    /// </summary>
    public class OddType: IdentifiableEntity
    {
        public int OddTypeId { get; set; }
        // Kód pre tento typ stávky
        public string Code { get; set; }
        // Textový popis typu stávky (eg. "Stávka na prvú tretinu")
        public string OddTypeDescription { get; set; }
        // Číslo časti zápasu, na ktorú sa viaže daný typ stávky
        // (eg. 1 = prvá tretina/polčas/.., 2 = druhá tretina/polčas, 0 = celý zápas)
        public int MatchPartNumber { get; set; }
        public WinnerResult Result { get; set; }


        // Väzba na vytvorenie M:N medzi športami a typmi stávok
        public virtual ICollection<Sport> Sports { get; set; }

        public override int GetId()
        {
            return OddTypeId;
        }


        public enum WinnerResult
        {
            HOST_WINNER,
            DRAW,
            GUEST_WINNER
        }
    }
}
