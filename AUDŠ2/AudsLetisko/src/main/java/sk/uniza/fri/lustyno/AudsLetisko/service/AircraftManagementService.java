package sk.uniza.fri.lustyno.AudsLetisko.service;

import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import sk.uniza.fri.lustyno.AudsLetisko.BusinessException;
import sk.uniza.fri.lustyno.AudsLetisko.codelists.DrahyCiselnik;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.HistoriaOdletuEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.LietadloEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.OdletovaDrahaEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.LietadloTableUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.VyhladanieLietadlaUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.VyradenieLietadlaUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.ZmenaPriorityUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entity_mapper.EntityMapper;
import sk.uniza.fri.lustyno.AudsLetisko.utils.*;
import sk.uniza.fri.lustyno.datastructures.entity.keys.StringKey;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureInsertDuplicateException;
import sk.uniza.fri.lustyno.datastructures.queue.PairingHeap;
import sk.uniza.fri.lustyno.datastructures.queue.QueueNode;
import sk.uniza.fri.lustyno.datastructures.tree.NodeResult;
import sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTreeNode;
import sk.uniza.fri.lustyno.datastructures.tree.splay.SplayTree;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AircraftManagementService {

    @Autowired
    private DrahyCiselnik drahyCiselnik;

    @Autowired
    private DateTimeService dateTimeService;

    @Autowired
    private EntityMapper entityMapper;

    @Autowired
    private RunwayManagementService runwayManagementService;

    @Autowired
    private HistoryService historyService;

    private SplayTree<StringKey, LietadloEntity> vsetkyLietadlaVSysteme;

    private SplayTree<StringKey, LietadloEntity> lietadlaNaLetisku;

    private SplayTree<StringKey, LietadloEntity> vsetkyCakajuceLietadlaNaPridelenieOdletovejDrahy;

    private SplayTree<StringKey, LietadloEntity> lietadlaSPridelenouOdletovouDrahou;

    private List<Pair<Integer, SplayTree<StringKey, LietadloEntity>>> lietadlaCakajuceNaPridelenieOdletovejDrahy;

    //CVICENIE
    private SplayTree<VyrobcaAMedzinarodnyKlucKey, LietadloEntity> lietadlaZoradenePodlaVyrobcuAMedzinarodnehoKoduZaroven;


    public AircraftManagementService() {
        this.vsetkyLietadlaVSysteme = new SplayTree<>();
        this.lietadlaNaLetisku = new SplayTree<>();
        this.lietadlaSPridelenouOdletovouDrahou = new SplayTree<>();
        this.vsetkyCakajuceLietadlaNaPridelenieOdletovejDrahy = new SplayTree<>();
        this.lietadlaCakajuceNaPridelenieOdletovejDrahy = new LinkedList<>();
        this.lietadlaZoradenePodlaVyrobcuAMedzinarodnehoKoduZaroven = new SplayTree<>();
    }


    /**
     * Vráti všetky lietadlá v systéme ako zoznam vykonaním levelOrder prehliadky
     */
    public List<LietadloEntity> getAllLietadlaVSystemePreUlozenieDoSuboru() {
        List<BinarySearchTreeNode<StringKey, LietadloEntity>> levelOrderNodes = this.vsetkyLietadlaVSysteme.levelOrder();
        if (levelOrderNodes == null || levelOrderNodes.size() == 0) {
            return null;
        }
        return levelOrderNodes.stream().map(e -> e.getData()).collect(Collectors.toList());
    }

    /**
     * Zo súboru boli načítané dáta a táto metóda nimi nahradí všetky lietadlá v systéme
     *
     * @param entities - zoznam lietadiel zo súboru
     */
    public void nastavVsetkyLietadlaVSystemeZoSuboru(List<LietadloEntity> entities) {
        this.vsetkyLietadlaVSysteme = new SplayTree<>();
        for (LietadloEntity lietadloEntity : entities) {
            this.vsetkyLietadlaVSysteme.insert(new StringKey(lietadloEntity.getMedzinarodnyKod()), lietadloEntity);
        }
    }

    /**
     * Vráti IDLE lietadlá pre uloženie do súboru level order prehliadkou
     */
    public List<LietadloEntity> getAllIdleLietadlaPreUlozenieDoSuboru() {
        List<BinarySearchTreeNode<StringKey, LietadloEntity>> levelOrderNodes = this.lietadlaNaLetisku.levelOrder();
        if (levelOrderNodes == null || levelOrderNodes.size() == 0) {
            return null;
        }
        return levelOrderNodes.stream().map(e -> e.getData()).collect(Collectors.toList());
    }

    /**
     * Nastaví IDLE lietadlá zo zoznamu zo súboru
     * Staré lietadlá zahodí!
     */
    public void nastavIdleLietadlaZoSuboru(List<String> medzinarodneKodyLietadiel) {
        this.lietadlaNaLetisku = new SplayTree<>();
        for (String kod : medzinarodneKodyLietadiel) {
            NodeResult<BinarySearchTreeNode<StringKey, LietadloEntity>> node =
                    this.vsetkyLietadlaVSysteme.find(new StringKey(kod));
            this.lietadlaNaLetisku.insert(new StringKey(kod), node.get().getData());
        }
    }

    /**
     * Vráti zoznam lietadiel, ktoré majú pridelené odletové dráhy
     */
    public List<LietadloEntity> getAllLietadlaSPridelenouOdletovouDrahou() {
        List<BinarySearchTreeNode<StringKey, LietadloEntity>> levelOrderNodes = this.lietadlaSPridelenouOdletovouDrahou.levelOrder();
        if (levelOrderNodes == null || levelOrderNodes.size() == 0) {
            return null;
        }
        return levelOrderNodes.stream().map(e -> e.getData()).collect(Collectors.toList());
    }

    /**
     * Nastaví lietadlá s pridelenou dráhou zo zoznamu zo súboru
     * @param idsLietadielADrah
     */
    public void nastavLietadlaSPridelenouDrahou(List<Pair<String, Integer>> idsLietadielADrah) {
        for (Pair<String, Integer> ids : idsLietadielADrah) {
            LietadloEntity lietadloEntity = this.vsetkyLietadlaVSysteme.find(new StringKey(ids.getKey())).get().getData();
            OdletovaDrahaEntity odletovaDrahaEntity = this.runwayManagementService.getOdletovaDraha(ids.getValue());

            lietadloEntity.setAktualnePridelenaDraha(odletovaDrahaEntity);
            odletovaDrahaEntity.setAktualnePrideleneLietadlo(lietadloEntity);

            this.lietadlaSPridelenouOdletovouDrahou.insert(new StringKey(ids.getKey()), lietadloEntity);
        }
    }


    /**
     * Vráti zoznam párov integer-zoznam lietadiel reprezentujúci
     * lietadlá, ktoré čakajú na pridelenie odletovej dráhy podľa
     * zgrupené podľa dĺžok dráh
     */
    public List<Pair<Integer, List<LietadloEntity>>> getVsetkyLietadlaCakajuceNaPrideleniOdletovejDrahyPreUlozenieDoSuboru() {
        List<Pair<Integer, List<LietadloEntity>>> zoznam = new LinkedList<>();

        for (Pair<Integer, SplayTree<StringKey, LietadloEntity>> pair : this.lietadlaCakajuceNaPridelenieOdletovejDrahy) {

            List<BinarySearchTreeNode<StringKey, LietadloEntity>> lietadlaNodes = pair.getValue().levelOrder();
            if (lietadlaNodes == null || lietadlaNodes.size() == 0) {
                continue;
            }

            List<LietadloEntity> lietadlaPreDanuDrahu = lietadlaNodes.stream().map(e -> e.getData()).collect(Collectors.toList());

            zoznam.add(new Pair<>(pair.getKey(), lietadlaPreDanuDrahu));
        }
        return zoznam;
    }

    public void nastavLietadlaCakajuceNaPridenieOdletovejDrahyZoSuboru(List<Pair<Integer, String>> dlzkaDrahyNaKodLietadlaList) {

        for (Pair<Integer, String> pair : dlzkaDrahyNaKodLietadlaList) {
            Integer dlzkaDrahy = pair.getKey();
            LietadloEntity lietadloEntity = this.getLietadloZoSystemuByMedzinarodnyKod(pair.getValue());

            this.pridajCakajuceLietadloDoStromov(lietadloEntity);
            this.runwayManagementService.zaradLietadloMedziCakajucePriImporteZoSuboru(dlzkaDrahy, lietadloEntity);
        }
    }


    /**
     * Zaeviduje prílet lietadla do systému
     *
     * Ak sa lietadlo už v systéme nachádza, nevytvára sa nový záznam o lietadle,
     * len sa nájdené lietadlo zo systému priradí medzi lietadlá, ktoré sú aktuálne IDLE na letisku
     *
     * @param lietadloEntity - lietadlo, ktoré sa má do systému zaevidovať
     * @return - či bola operácia úspešná
     */
    public OperationMarker zaevidujPriletLietadla(LietadloEntity lietadloEntity) {

        if (cakaLietadloNaOdlet(lietadloEntity)) {
            throw new BusinessException("Toto lietadlo aktuálne čaká na odlet, preto nie je možné zaevidovať jeho prílet");
        }

        if (cakaLietadloNaPrideleni(lietadloEntity)) {
            throw new BusinessException("Toto lietadlo čaká na pridelenie odletovej dráhy, preto nie je možné zaevidovať jeho prílet");
        }

        NodeResult<BinarySearchTreeNode<StringKey, LietadloEntity>> lietadloZoSystemu =
                vsetkyLietadlaVSysteme.find(new StringKey(lietadloEntity.getMedzinarodnyKod()));

        if (lietadloZoSystemu.wasFound()) {

            try {
                LietadloEntity entity = lietadloZoSystemu.get().getData();
                // Pri prílete už evidovaného lietadla prepíše údaje na nové okrem medzinárodného kódu
                entity.setPriorita(lietadloEntity.getPriorita());
                entity.setCasDatumPriletu(this.dateTimeService.getActualDateTime());
                entity.setNazovVyrobcu(lietadloEntity.getNazovVyrobcu());
                this.lietadlaNaLetisku.insert(lietadloZoSystemu.get().getKey(), entity);
            } catch (DataStructureInsertDuplicateException e) {
                throw new BusinessException("Lietadlo s medzinárodným kódom " + lietadloEntity.getMedzinarodnyKod() + " sa už na letisku nachádza");
            }
        } else {
            try {
                this.vsetkyLietadlaVSysteme.insert(new StringKey(lietadloEntity.getMedzinarodnyKod()), lietadloEntity);
                this.lietadlaNaLetisku.insert(new StringKey(lietadloEntity.getMedzinarodnyKod()), lietadloEntity);
            } catch (DataStructureInsertDuplicateException e) {
                throw new BusinessException("Lietadlo s medzinárodným kódom " + lietadloEntity.getMedzinarodnyKod() + " sa už na letisku nachádza");
            }
        }

        return OperationMarker.OPERATION_SUCCESFUL;
    }

    /**
     * Vytvorí požiadavku na pridelenie odletovej dráhy,
     * lietadlo musí byť evidované ako čakajúce na letisku
     *
     * @param lietadloEntity - lietadlo, ktorému sa má prideliť dráha
     * @return - či bola operácia úspešná
     */
    public OperationMarker vytvorPoziadavkuOPridelenieOdletovejDrahy(LietadloEntity lietadloEntity) {

        NodeResult<BinarySearchTreeNode<StringKey, LietadloEntity>> found =
                this.lietadlaNaLetisku.find(new StringKey(lietadloEntity.getMedzinarodnyKod()));
        if (!found.wasFound()) {
            throw new BusinessException("Takéto lietadlo sa na letisku nenachádza");
        }
        LietadloEntity entity = found.get().getData();

        entity.setCasDatumPoziadaniaOPridelenie(dateTimeService.getActualDateTime());
        Integer najblizsiaMoznaDlzkaDrahy = drahyCiselnik.getNajblizsiaDlzkaMoznaDlzka(entity.getMinimalnaDlzkaOdletovejDrahy());

        // Vyhodenie z lietadiel na letisku (idle lietadlá)
        this.lietadlaNaLetisku.delete(new StringKey(entity.getMedzinarodnyKod()));

        // Pridanie historického záznamu o prílete a požiadaní o odletovú dráhu
        this.historyService.vytvorZaznamOPoziadaniLietadla(entity);

        // Z CVIČENIA PRIDANIE DO STROMU LIETADIEL PODĽA VÝROBCOV A MEDZINÁRODNÉHO KÓDU
        this.lietadlaZoradenePodlaVyrobcuAMedzinarodnehoKoduZaroven.insert(
                new VyrobcaAMedzinarodnyKlucKey(entity.getNazovVyrobcu(), entity.getMedzinarodnyKod()), entity);

        // Ak sa lietadlu podarí rovno priradiť odletovú dráhu, nemusí sa lietadlo nikam inam pridávať
        ZaevidovaniePoziadavkyResult result =
                runwayManagementService.zaevidujCakajuceLietadloNaOdletovuDrahu(entity, najblizsiaMoznaDlzkaDrahy);
        if (result.getPoziadavkaResult().equals(ZaevidovaniePoziadavkyMarker.LIETADLO_AUTOMATICKY_PRIRADENE_NA_DRAHU)) {
            // Pridanie medzi lietadlá, ktoré majú pridelenú odletovú dráhu
            this.lietadlaSPridelenouOdletovouDrahou.insert(found.get().getKey(), found.get().getData());

            Notification n = new Notification(
                    "Lietadlo bolo automaticky pridelené na dráhu s ID: " + entity.getAktualnePridelenaDraha().getId() + " lebo bola voľná",
                    Notification.Type.HUMANIZED_MESSAGE);
            n.setDelayMsec(5000);
            n.setPosition(Position.BOTTOM_RIGHT);
            n.show(UI.getCurrent().getPage());
            return OperationMarker.OPERATION_NEUTRAL;
        }

        // Pridanie lietadla medzi lietadlá čakajúce na pridelenie odletovej dráhy
        this.vsetkyCakajuceLietadlaNaPridelenieOdletovejDrahy.insert(new StringKey(entity.getMedzinarodnyKod()), entity);

        // Pridanie aj do roztriedeného zoznamu čakajúcich lietadiel na odlet podľa dĺžok dráh
        Pair<Integer, SplayTree<StringKey, LietadloEntity>> lietadlaSDrahou= null;
        for (Pair<Integer, SplayTree<StringKey, LietadloEntity>> pair : this.lietadlaCakajuceNaPridelenieOdletovejDrahy) {
            if (pair.getKey().equals(najblizsiaMoznaDlzkaDrahy)) {
                lietadlaSDrahou = pair;
                break;
            }
        }
        if (lietadlaSDrahou == null) {
            // Vytvorenie páru dĺžkaDráhy - zoznam lietadiel čakajúcich
            Pair<Integer, SplayTree<StringKey, LietadloEntity>> newPair = new Pair<>(najblizsiaMoznaDlzkaDrahy, new SplayTree<>());
            this.lietadlaCakajuceNaPridelenieOdletovejDrahy.add(newPair);
            newPair.getValue().insert(new StringKey(entity.getMedzinarodnyKod()), entity);
        } else {
            lietadlaSDrahou.getValue().insert(new StringKey(entity.getMedzinarodnyKod()), entity);
        }

        return OperationMarker.OPERATION_SUCCESFUL;
    }

    /**
     * Vráti všetky lietadlá čakajúce na pridelenie konkrétnej odletovej dráhy
     * podľa toho, aké má dráha ID -> dĺžku
     */
    public List<LietadloTableUiEntity> getAllCakajuceLietadlaNaDrahu(VyhladanieLietadlaUiEntity entity) {

        OdletovaDrahaEntity draha = this.runwayManagementService.getOdletovaDraha(entity.getIdDrahy());
        if (draha == null) {
            throw new BusinessException("Dráha s ID " + entity.getIdDrahy() + " neexistuje");
        }

        SplayTree<StringKey, LietadloEntity> cakajuceLietadla = null;
        for (Pair<Integer, SplayTree<StringKey, LietadloEntity>> podlaDrah : this.lietadlaCakajuceNaPridelenieOdletovejDrahy) {
            if (podlaDrah.getKey().equals(draha.getDlzkaDrahy())) {
                cakajuceLietadla = podlaDrah.getValue();
                break;
            }
        }

        if (cakajuceLietadla == null) {
            return new LinkedList<>();
        }

        List<BinarySearchTreeNode<StringKey, LietadloEntity>> listCakajucichLietadiel = cakajuceLietadla.inOrder();

        if (listCakajucichLietadiel == null) {
            return null;
        }
        return entityMapper.fromLietadaEntitiesToTableUiEntity(listCakajucichLietadiel.stream().map(e -> e.getData()).collect(Collectors.toList()));
    }

    public List<LietadloTableUiEntity> getAllCakajuceLietadlaPodlaVyrobcuNaDrahu() {

        List<BinarySearchTreeNode<VyrobcaAMedzinarodnyKlucKey, LietadloEntity>> vsetkyLietadla
                = this.lietadlaZoradenePodlaVyrobcuAMedzinarodnehoKoduZaroven.inOrder();
        if (vsetkyLietadla != null && vsetkyLietadla.size() != 0) {
            return this.entityMapper.fromLietadaEntitiesToTableUiEntity(
                    vsetkyLietadla.stream().map(e -> e.getData()).collect(Collectors.toList()));
        }
        return null;
    }

    /**
     * Vráti všetky čakajúce lietadlá na pridelenie odletovej dráhy
     * usporiadané podľa medzinárodného kódu
     * @return - list lietadiel čakajúcich na pridelenie odletovej dráhy zoradený podľa medzinárod.kódov lietadil
     */
    public List<LietadloTableUiEntity> getAllCakajuceLietadla() {
        List<BinarySearchTreeNode<StringKey, LietadloEntity>> binarySearchTreeNodes = this.vsetkyCakajuceLietadlaNaPridelenieOdletovejDrahy.inOrder();

        if (binarySearchTreeNodes == null)  {
            return new LinkedList<>();
        } else {
            return entityMapper.fromLietadaEntitiesToTableUiEntity(binarySearchTreeNodes.stream().map(e -> e.getData()).collect(Collectors.toList()));
        }
    }

    /**
     * Vráti lietadlo čakajúce na pridelenie odletovej dráhy,
     * ktorému bude mať byť zmenená priorita
     *
     * @param entity
     * @return
     */
    public LietadloEntity getLietadloCakajuceNaPrideleniOdletovejDrahy(ZmenaPriorityUiEntity entity) {

        NodeResult<BinarySearchTreeNode<StringKey, LietadloEntity>> cakajuceLietadlo =
                this.vsetkyCakajuceLietadlaNaPridelenieOdletovejDrahy.find(new StringKey(entity.getMedzinarodnyKodLietadla()));
        if (cakajuceLietadlo.wasFound()) {
            return cakajuceLietadlo.get().getData();
        } else {
            return null;
        }
    }

    /**
     * Vráti informácie o lietadle, ktoré čaká na pridelenie odletovej dráhy,
     * ak takéto lietadlo existuje
     *
     * @param entity - údaje podľa ktorých treba lietadlo vyhľadať
     * @return - nájdené/nenájdené informácie o čakajúcom lietadle
     */
    public LietadloTableUiEntity getLietadloCakajuceNaPridelenieOdletovejDrahy(VyhladanieLietadlaUiEntity entity) {

        NodeResult<BinarySearchTreeNode<StringKey, LietadloEntity>> cakajuceLietadlo =
                this.vsetkyCakajuceLietadlaNaPridelenieOdletovejDrahy.find(new StringKey(entity.getMedzinarodnyKodLietadla()));
        if (cakajuceLietadlo.wasFound()) {
            return this.entityMapper.fromLietadloEntityToTableUiEntity(cakajuceLietadlo.get().getData());
        } else {
            return null;
        }
    }


    /**
     * Vráti informácie o lietadle, ktoré čaká na pridelenie odletovej dráhy,
     * ak takéto lietadlo existuje a naozaj čaká na dráhu z parametra
     *
     * @param entity - údaje podľa ktorých treba lietadlo vyhľadať
     * @return - nájdené/nenájdené informácie o čakajúcom lietadle
     */
    public LietadloTableUiEntity getLietadloCakajuceNaPridelenieOdletovejDrahyNaKonkretnejDrahe(VyhladanieLietadlaUiEntity entity) {

        NodeResult<BinarySearchTreeNode<StringKey, LietadloEntity>> cakajuceLietadlo =
                this.vsetkyCakajuceLietadlaNaPridelenieOdletovejDrahy.find(new StringKey(entity.getMedzinarodnyKodLietadla()));
        if (cakajuceLietadlo.wasFound()) {
            LietadloEntity lietadlo = cakajuceLietadlo.get().getData();
            OdletovaDrahaEntity draha = runwayManagementService.getOdletovaDraha(entity.getIdDrahy());
            if (draha == null) {
                throw new BusinessException("Odletová dráha s takýmto ID neexistuje");
            }
            if (drahyCiselnik.getNajblizsiaDlzkaMoznaDlzka(lietadlo.getMinimalnaDlzkaOdletovejDrahy()).equals(draha.getDlzkaDrahy())) {
                return entityMapper.fromLietadloEntityToTableUiEntity(lietadlo);
            }
            throw new BusinessException("Lietadlo s týmto medzinárodným kódom čaká na inú odletovú dráhu");
        } else {
            throw new BusinessException("Lietadlo s takýmto medzinárodným kódom nečaká na pridelenie odletovej dráhy");
        }
    }

    public LietadloTableUiEntity getLietadloPodlaVyrobcuAMedzinarodnehoKodu(VyhladanieLietadlaUiEntity entity) {

        NodeResult<BinarySearchTreeNode<VyrobcaAMedzinarodnyKlucKey, LietadloEntity>> foundLiedadlo =
                this.lietadlaZoradenePodlaVyrobcuAMedzinarodnehoKoduZaroven.find(
                    new VyrobcaAMedzinarodnyKlucKey(entity.getVyrobca(), entity.getMedzinarodnyKodLietadla()));

        if (foundLiedadlo.wasFound()) {
            return this.entityMapper.fromLietadloEntityToTableUiEntity(foundLiedadlo.get().getData());
        } else {
            return null;
        }
    }

    /**
     * Vráti všetky lietadlá, ktoré majú pridelenú odletovú dráhu
     * @return - zoznam UI entít lietadiel s pridelenou odletovou dráhou
     */
    public List<LietadloTableUiEntity> getVsetkyLietadlaSPridelenouDrahou() {
        List<BinarySearchTreeNode<StringKey, LietadloEntity>> lietadlaSPridelenouDrahou = this.lietadlaSPridelenouOdletovouDrahou.inOrder();
        if (lietadlaSPridelenouDrahou == null) {
            return new LinkedList<>();
        }
        return this.entityMapper.fromLietadaEntitiesToTableUiEntity(
                lietadlaSPridelenouDrahou.stream().map(e -> e.getData()).collect(Collectors.toList()));
    }

    /**
     * Vytvorenie odletu lietadla
     *
     * @param entity
     */
    public OperationMarker zaevidujOdletLietadla(LietadloTableUiEntity entity) {
        NodeResult<BinarySearchTreeNode<StringKey, LietadloEntity>> lietadloSPridelenouDrahou =
                this.lietadlaSPridelenouOdletovouDrahou.delete(new StringKey(entity.getMedzinarodnyKod()));
        if (!lietadloSPridelenouDrahou.wasFound()) {
            throw new BusinessException("Lietadlo s medzinárodným kódom " + entity.getMedzinarodnyKod() +
                    " nemalo pridelenú žiadnu odletovú dráhu!");
        }

        LietadloEntity lietadlo = lietadloSPridelenouDrahou.get().getData();
        OdletovaDrahaEntity odletovaDrahaEntity = lietadlo.getAktualnePridelenaDraha();

        HistoriaOdletuEntity historiaOdletuEntity = new HistoriaOdletuEntity();
        historiaOdletuEntity.setCasDatumOdletu(this.dateTimeService.getActualDateTime());
        historiaOdletuEntity.setCasDatumPriletu(lietadlo.getCasDatumPriletu());
        historiaOdletuEntity.setLietadloEntity(lietadlo);
        historiaOdletuEntity.setOdletovaDraha(odletovaDrahaEntity);

        this.historyService.pridajHistorickyZaznam(historiaOdletuEntity);

        this.lietadlaZoradenePodlaVyrobcuAMedzinarodnehoKoduZaroven.delete(new VyrobcaAMedzinarodnyKlucKey(
                lietadlo.getNazovVyrobcu(), lietadlo.getMedzinarodnyKod()));

        PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity> cakajuceLietadlaNaUvolnenuDrahu =
                this.runwayManagementService.getCakajuceLietadlaNaDanuDlzku(lietadlo.getAktualnePridelenaDraha().getDlzkaDrahy());

        // Priradenie dráhy najprioritnejši čakajúcemu lietadlu ak také existuje na túto dráhu
        if (cakajuceLietadlaNaUvolnenuDrahu != null && cakajuceLietadlaNaUvolnenuDrahu.getSize() > 0) {
            QueueNode<PrioritaACasPoziadavkyKey, LietadloEntity> najprioritnejsieLietadlo = cakajuceLietadlaNaUvolnenuDrahu.deleteMin();
            lietadlo.setAktualnePridelenaDraha(null);
            odletovaDrahaEntity.setAktualnePrideleneLietadlo(najprioritnejsieLietadlo.getData());
            najprioritnejsieLietadlo.getData().setAktualnePridelenaDraha(odletovaDrahaEntity);
            najprioritnejsieLietadlo.getData().setFrontaNode(null);

            OperationMarker result = OperationMarker.OPERATION_NEUTRAL;
            result.setMessage(najprioritnejsieLietadlo.getData().getMedzinarodnyKod());

            zaevidujPridanieDrahyLietadlu(najprioritnejsieLietadlo.getData());

            return result;
        } else {
            lietadlo.setAktualnePridelenaDraha(null);
            odletovaDrahaEntity.setAktualnePrideleneLietadlo(null);

            this.runwayManagementService.pridajUvolnenuDrahu(odletovaDrahaEntity);
        }
        return OperationMarker.OPERATION_SUCCESFUL;
    }

    /**
     * Zaevidovanie, že lietadlu bola pridelená odletová dráha (má ju ako atribút)
     *
     * @param cakajuceLietadlo - lietadlo ktoré bolo čakajúce a bola mu pridelená odletová dráha
     */
    public void zaevidujPridanieDrahyLietadlu(LietadloEntity cakajuceLietadlo) {

        Assert.notNull(
                this.vsetkyCakajuceLietadlaNaPridelenieOdletovejDrahy.delete(new StringKey(cakajuceLietadlo.getMedzinarodnyKod())),
                "Lietadlo sa nepodarilo vymazať z lietadiel čakajúcich na pridelenie odletovej dráhy");

        SplayTree<StringKey, LietadloEntity> cakajuceLietadlaPodlaDlzkyDrahy = null;
        for (Pair<Integer, SplayTree<StringKey, LietadloEntity>> cakajucePodlaDlzkyDrahy : this.lietadlaCakajuceNaPridelenieOdletovejDrahy) {
            if (cakajuceLietadlo.getAktualnePridelenaDraha().getDlzkaDrahy().equals(cakajucePodlaDlzkyDrahy.getKey())) {
                cakajuceLietadlaPodlaDlzkyDrahy = cakajucePodlaDlzkyDrahy.getValue();
                break;
            }
        }
        Assert.notNull(
                cakajuceLietadlaPodlaDlzkyDrahy.delete(new StringKey(cakajuceLietadlo.getMedzinarodnyKod())),
                "Lietadlo sa nepodarilo vymazať z lietadiel čakajúcich na pridelenie odletovej dráhy rozdelených podľa dĺžky dráhy");

        this.lietadlaSPridelenouOdletovouDrahou.insert(new StringKey(cakajuceLietadlo.getMedzinarodnyKod()), cakajuceLietadlo);

    }

    /**
     * Vráti lietadlo zo všetkých lietadiel v systéme podľa ID
     */
    public LietadloEntity getLietadloZoSystemuByMedzinarodnyKod(String medzinarodnyKod) {

        NodeResult<BinarySearchTreeNode<StringKey, LietadloEntity>> lietadloVSysteme =
                this.vsetkyLietadlaVSysteme.find(new StringKey(medzinarodnyKod));
        if (lietadloVSysteme.wasFound()) {
            return lietadloVSysteme.get().getData();
        } else {
            return null;
        }
    }

    /**
     * Vráti lietadlo z IDLE lietadiel v systéme podľa ID
     */
    public LietadloEntity getIdleLietadloByMedzinarodnyKod(String medzinarodnyKod) {

        NodeResult<BinarySearchTreeNode<StringKey, LietadloEntity>> lietadloVSysteme =
                this.lietadlaNaLetisku.find(new StringKey(medzinarodnyKod));
        if (lietadloVSysteme.wasFound()) {
            return lietadloVSysteme.get().getData();
        } else {
            return null;
        }
    }

    /**
     * Pridá lietadlo do stromu čakajúcich aj do stromu čakajúcich podľa dráh
     */
    private void pridajCakajuceLietadloDoStromov(LietadloEntity lietadloEntity) {

        // Pridanie do stromu všetkých
        this.vsetkyCakajuceLietadlaNaPridelenieOdletovejDrahy.insert(
                new StringKey(lietadloEntity.getMedzinarodnyKod()), lietadloEntity);

        // Pridanie do stromu podľa dráh
        SplayTree<StringKey, LietadloEntity> treeToAddTo = null;
        for (Pair<Integer, SplayTree<StringKey, LietadloEntity>> cakajucePodlaDlzkyDrahy : this.lietadlaCakajuceNaPridelenieOdletovejDrahy) {
            if (this.drahyCiselnik.getNajblizsiaDlzkaMoznaDlzka(lietadloEntity.getMinimalnaDlzkaOdletovejDrahy()).equals(cakajucePodlaDlzkyDrahy.getKey())) {
                treeToAddTo = cakajucePodlaDlzkyDrahy.getValue();
                break;
            }
        }

        if (treeToAddTo == null) {
            treeToAddTo = new SplayTree<>();
            Integer dlzkaDrahy = this.drahyCiselnik.getNajblizsiaDlzkaMoznaDlzka(lietadloEntity.getMinimalnaDlzkaOdletovejDrahy());
            this.lietadlaCakajuceNaPridelenieOdletovejDrahy.add(
                    new Pair<Integer, SplayTree<StringKey, LietadloEntity>>(dlzkaDrahy, treeToAddTo));
        }

        treeToAddTo.insert(new StringKey(lietadloEntity.getMedzinarodnyKod()), lietadloEntity);
    }


    /**
     * Vyradenie lietadla zo zoznamu čakajúcich lietadiel
     */
    public OperationMarker vyradLietadlo(VyradenieLietadlaUiEntity vyradenieLietadlaUiEntity) {
        if (this.getLietadloZoSystemuByMedzinarodnyKod(vyradenieLietadlaUiEntity.getMedzinarodnyKodLietadla()) == null) {
            throw new BusinessException("Lietadlo s medzinárodným kódom " + vyradenieLietadlaUiEntity.getMedzinarodnyKodLietadla() + " v systéme neexistuje!");
        }

        if (this.getIdleLietadloByMedzinarodnyKod(vyradenieLietadlaUiEntity.getMedzinarodnyKodLietadla()) != null) {
            return OperationMarker.OPERATION_UNSUCCESFUL;
        }

        // Ak je lietadlo medzi lietadlami, ktoré majú pridelenú odletovú dráhu
        NodeResult<BinarySearchTreeNode<StringKey, LietadloEntity>> lietadloSPridelenouDrahou =
                this.lietadlaSPridelenouOdletovouDrahou.delete(new StringKey(vyradenieLietadlaUiEntity.getMedzinarodnyKodLietadla()));
        if (lietadloSPridelenouDrahou.wasFound()) {
            LietadloEntity entity = lietadloSPridelenouDrahou.get().getData();
            OdletovaDrahaEntity odletovaDrahaEntity = entity.getAktualnePridelenaDraha();

            entity.setAktualnePridelenaDraha(null);
            entity.setFrontaNode(null);

            PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity> cakajuceLietadla =
                    this.runwayManagementService.getCakajuceLietadlaNaDanuDlzku(odletovaDrahaEntity.getDlzkaDrahy());

            if (cakajuceLietadla != null && cakajuceLietadla.getSize() > 0) {
                LietadloEntity najprioritnejsieLietadlo = cakajuceLietadla.deleteMin().getData();
                najprioritnejsieLietadlo.setFrontaNode(null);
                najprioritnejsieLietadlo.setAktualnePridelenaDraha(odletovaDrahaEntity);
                odletovaDrahaEntity.setAktualnePrideleneLietadlo(najprioritnejsieLietadlo);
                zaevidujPridanieDrahyLietadlu(najprioritnejsieLietadlo);

                this.lietadlaNaLetisku.insert(new StringKey(entity.getMedzinarodnyKod()), entity);

                OperationMarker result = OperationMarker.OPERATION_NEUTRAL;
                result.setMessage(najprioritnejsieLietadlo.getMedzinarodnyKod());

                return result;
            }
            this.lietadlaNaLetisku.insert(new StringKey(entity.getMedzinarodnyKod()), entity);
            this.runwayManagementService.pridajUvolnenuDrahu(odletovaDrahaEntity);
            return OperationMarker.OPERATION_SUCCESFUL;
        }


        // Ak lietadlo len čaká na pridelenie odletovej dráhy
        NodeResult<BinarySearchTreeNode<StringKey, LietadloEntity>> vyradeneZoVsetkychCakajich =
                this.vsetkyCakajuceLietadlaNaPridelenieOdletovejDrahy.delete(new StringKey(vyradenieLietadlaUiEntity.getMedzinarodnyKodLietadla()));

        LietadloEntity vyradene = vyradeneZoVsetkychCakajich.get().getData();

        Integer dlzkaDrahyNaKtoruCakalo =
                this.drahyCiselnik.getNajblizsiaDlzkaMoznaDlzka(vyradene.getMinimalnaDlzkaOdletovejDrahy());

        PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity> cakajuceNaDlzku = this.runwayManagementService.getCakajuceLietadlaNaDanuDlzku(dlzkaDrahyNaKtoruCakalo);

        // Vyhodenie z frontu čakajúcich
        cakajuceNaDlzku.delete(vyradene.getFrontaNode());
        vyradene.setFrontaNode(null);

        // Vyradenie zo stromu cakajich podla dĺžky dráhy
        SplayTree<StringKey, LietadloEntity> cakajuceLietadlaPodlaDlzkyDrahy = null;
        Integer dlzkaDrahy = this.drahyCiselnik.getNajblizsiaDlzkaMoznaDlzka(vyradene.getMinimalnaDlzkaOdletovejDrahy());
        for (Pair<Integer, SplayTree<StringKey, LietadloEntity>> cakajucePodlaDlzkyDrahy : this.lietadlaCakajuceNaPridelenieOdletovejDrahy) {
            if (dlzkaDrahy.equals(cakajucePodlaDlzkyDrahy.getKey())) {
                cakajuceLietadlaPodlaDlzkyDrahy = cakajucePodlaDlzkyDrahy.getValue();
                break;
            }
        }
        cakajuceLietadlaPodlaDlzkyDrahy.delete(new StringKey(vyradene.getMedzinarodnyKod()));

        this.lietadlaNaLetisku.insert(new StringKey(vyradene.getMedzinarodnyKod()), vyradene);

        return OperationMarker.OPERATION_SUCCESFUL;
    }

    /**
     * Vráti, či sa lietadlo nachádza medzi lietadlami čakajúcimi na priradenie odletovej dráhy
     *
     * @param lietadloEntity
     * @return
     */
    private boolean cakaLietadloNaPrideleni(LietadloEntity lietadloEntity) {
        return this.vsetkyCakajuceLietadlaNaPridelenieOdletovejDrahy.find(
                new StringKey(lietadloEntity.getMedzinarodnyKod())).wasFound();
    }

    /**
     * Vráti, či lietadlo čaká na odlet, t.j. je medzi lietadlami ktoré majú pridelenú odletovú dráhu
     * @param lietadloEntity
     * @return
     */
    private boolean cakaLietadloNaOdlet(LietadloEntity lietadloEntity) {
        return this.lietadlaSPridelenouOdletovouDrahou.find(
                new StringKey(lietadloEntity.getMedzinarodnyKod())).wasFound();
    }

    public List<LietadloTableUiEntity> getAllLietadlaVSystemeTableEntities() {
        List<BinarySearchTreeNode<StringKey, LietadloEntity>> levelOrderNodes = this.vsetkyLietadlaVSysteme.levelOrder();
        if (levelOrderNodes == null || levelOrderNodes.size() == 0) {
            return null;
        }
        return entityMapper.fromLietadaEntitiesToTableUiEntity(levelOrderNodes.stream().map(e -> e.getData()).collect(Collectors.toList()));
    }

}
