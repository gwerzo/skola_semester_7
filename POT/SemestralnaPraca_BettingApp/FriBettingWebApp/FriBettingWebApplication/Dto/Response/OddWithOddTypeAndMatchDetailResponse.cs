﻿using Newtonsoft.Json;

namespace FriBettingWebApplication.Dto.Response
{
    public class OddWithOddTypeAndMatchDetailResponse
    {
        [JsonProperty("Odd_Type_Id")]
        public int OddTypeId { get; set; }
        [JsonProperty("Odd_Type_Code")]
        public string OddTypeCode { get; set; }
        [JsonProperty("Odd_Id")]
        public int OddId { get; set; }
        [JsonProperty("Odd_Value")]
        public double OddValue { get; set; }
        [JsonProperty("Odd_Host")]
        public string Host { get; set; }
        [JsonProperty("Odd_Guest")]
        public string Guest { get; set; }
    }
}