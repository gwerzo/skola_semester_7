﻿using System.Collections.Generic;
using FriBettingDatabaseApp.Bo;

namespace FriBettingDatabaseApp.Repository.Base
{
    public interface ISportRepository: IRepository<Sport>
    {
        IEnumerable<OddType> GetAllOddTypesForSport(int sportId);
    }
}
