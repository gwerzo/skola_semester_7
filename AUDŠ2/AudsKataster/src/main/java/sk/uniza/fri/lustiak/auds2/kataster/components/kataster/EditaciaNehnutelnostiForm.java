package sk.uniza.fri.lustiak.auds2.kataster.components.kataster;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.ui.*;
import lombok.Data;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.CoordinatesUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.EditRealEstateUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.RealEstateUiTableEntity;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;
import sk.uniza.fri.lustiak.auds2.kataster.utils.StringToIntegerConverter;


public class EditaciaNehnutelnostiForm extends BusinessComponent {

    private Binder<CoordinatesUi> coordinatesBinder;
    private CoordinatesUi entity;

    private RealEstateService realEstateService;
    private Grid<RealEstateUiTableEntity> realEstateGrid;

    private RealEstateUiTableEntity realEstateBeingEdited;
    private RealEstateUiTableEntity realEstateBeingEditedNewValues;
    private Boolean editRealEstateCoords = Boolean.FALSE;

    private Panel initViewPanel;
    private Panel editorPanel;

    public EditaciaNehnutelnostiForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_6.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new CoordinatesUi();
        this.coordinatesBinder = new Binder<>(CoordinatesUi.class);
        this.realEstateGrid = new Grid<>();
        this.realEstateGrid.setWidth("100%");

        this.realEstateGrid.addComponentColumn(this::buildEditujButton).setCaption("Edituj");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getInternalId).setCaption("#");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getSupisneCislo).setCaption("Súpisné číslo");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getPopis).setCaption("Popis");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getLatitude).setCaption("Zem.výška");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getLongtitude).setCaption("Zem.šírka");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getLatitudeOrientation).setCaption("Orientácia výšky");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getLongitudeOrientation).setCaption("Orientácia šírky");


        VerticalLayout layout = new VerticalLayout();

        FormLayout formLayout = new FormLayout();

        HorizontalLayout latLayout = new HorizontalLayout();
        TextField latitudeTF = new TextField("Zemepisná výška");
        RadioButtonGroup<String> latitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej výšky");
        latitudeOrientation.setItems("SEVER", "JUH");
        latitudeOrientation.setSelectedItem("SEVER");
        latLayout.addComponents(latitudeTF, latitudeOrientation);

        HorizontalLayout longLayout = new HorizontalLayout();
        TextField longitudeTF = new TextField("Zemepisná šírka");
        RadioButtonGroup<String> longitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej šírky");
        longitudeOrientation.setItems("VÝCHOD", "ZÁPAD");
        longitudeOrientation.setSelectedItem("VÝCHOD");
        longLayout.addComponents(longitudeTF, longitudeOrientation);

        Button findRealEstatesButton = new Button("Vyhľadať nehnuteľnosti");
        findRealEstatesButton.addClickListener(e -> {
            try {
                this.coordinatesBinder.writeBean(this.entity);
                if (longitudeOrientation.getSelectedItem().get().equals("VÝCHOD")) {
                    this.entity.setLongitudeOrientation(true);
                } else {
                    this.entity.setLongitudeOrientation(false);
                }
                if (latitudeOrientation.getSelectedItem().get().equals("SEVER")) {
                    this.entity.setLatitudeOrientation(true);
                } else {
                    this.entity.setLatitudeOrientation(false);
                }
                najdiNehnutelnosti(this.entity);
            } catch (ValidationException valEx) {
                this.entity.flush();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        formLayout.addComponent(latLayout);
        formLayout.addComponent(longLayout);
        formLayout.addComponent(findRealEstatesButton);

        layout.addComponent(formLayout);
        layout.addComponent(this.realEstateGrid);

        this.coordinatesBinder.forField(latitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLatitude, CoordinatesUi::setLatitude);

        this.coordinatesBinder.forField(longitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLongitude, CoordinatesUi::setLongitude);

        this.initViewPanel = new Panel("Editácia nehnuteľnosti", layout);

        this.initEditorPanel();

        this.parentComponent.showComponent(initViewPanel);
    }

    private void initEditorPanel() {
        this.editorPanel = new Panel("Editácia nehnuteľnosti");
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_6.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    private void najdiNehnutelnosti(CoordinatesUi coordinatesUi) {
        this.realEstateGrid.setItems(this.realEstateService.getRealEstatesListAtCoordinates(coordinatesUi));
    }


    public Button buildEditujButton(RealEstateUiTableEntity realEstateUiTableEntity) {
        Button editujButton = new Button("Edituj");

        editujButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {
                this.initViewPanel.setVisible(false);

                this.editorPanel = new Panel("Editácia nehnuteľnosti",
                        this.buildEditLayoutForRealEstate(realEstateUiTableEntity));

                this.parentComponent.addComponent(this.editorPanel);
                return OperationMarker.OPERATION_SUCCESFUL;
            });
        });

        return editujButton;
    }

    public VerticalLayout buildEditLayoutForRealEstate(RealEstateUiTableEntity realEstateUiTableEntity) {
        Binder<RealEstateUiTableEntity> entityNewValuesBinder = new Binder<>();
        VerticalLayout mainLayout = new VerticalLayout();

        HorizontalLayout panelsLayout = new HorizontalLayout();

        Panel oldEntityPanel = new Panel(
                "Menená nehnuteľnosť",
                buildRealEstateFields(realEstateUiTableEntity, false, entityNewValuesBinder).getLayout());

        EditFieldsGroup efgNewEntity = buildRealEstateFields(realEstateUiTableEntity, true, entityNewValuesBinder);
        Panel newEntityPanel = new Panel(
                "Nové hodnoty nehnuteľnosti",
                efgNewEntity.getLayout());

        panelsLayout.addComponent(oldEntityPanel);
        panelsLayout.addComponent(newEntityPanel);

        Button buttonEdit = new Button("Potvrdiť zmeny");
        buttonEdit.addStyleName("friendly");
        buttonEdit.addClickListener(e -> {
            try {
                this.realEstateBeingEdited = realEstateUiTableEntity;
                RealEstateUiTableEntity entityChanges = new RealEstateUiTableEntity();
                entityChanges.setLatitudeOrientation(
                        efgNewEntity.getLatitudeOrientation().getSelectedItem().get());
                entityChanges.setLongitudeOrientation(
                        efgNewEntity.getLongitudeOrientation().getSelectedItem().get());
                entityNewValuesBinder.writeBean(entityChanges);
                this.realEstateBeingEditedNewValues = entityChanges;
                editujNehnutelnost();
            } catch (ValidationException valEx) {
                this.entity.flush();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        Button buttonBack = new Button("Späť");
        buttonBack.addClickListener(e -> {
           this.editorPanel.setVisible(false);
           this.initViewPanel.setVisible(true);
        });
        mainLayout.addComponent(panelsLayout);
        mainLayout.addComponent(new HorizontalLayout(buttonEdit, buttonBack));

        return mainLayout;
    }

    private void editujNehnutelnost() {
        this.realEstateService.editRealEstate(new EditRealEstateUi(
                this.editRealEstateCoords,
                this.realEstateBeingEdited,
                this.realEstateBeingEditedNewValues
        ));
        this.showComponent();
    }

    public EditFieldsGroup buildRealEstateFields(RealEstateUiTableEntity entity, Boolean editable,
                                                Binder<RealEstateUiTableEntity> entityNewValuesBinder) {
        VerticalLayout layout = new VerticalLayout();

        FormLayout fl = new FormLayout();
        TextField supisneCisloTF = new TextField("Súpisné číslo");
        TextField popisTF = new TextField("Popis");

        HorizontalLayout latLayout = new HorizontalLayout();
        TextField latitudeTF = new TextField("Zemepisná výška");
        RadioButtonGroup<String> latitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej výšky");
        latitudeOrientation.setItems("SEVER", "JUH");
        latitudeOrientation.setSelectedItem("SEVER");
        latLayout.addComponents(latitudeTF, latitudeOrientation);

        HorizontalLayout longLayout = new HorizontalLayout();
        TextField longitudeTF = new TextField("Zemepisná šírka");
        RadioButtonGroup<String> longitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej šírky");
        longitudeOrientation.setItems("VÝCHOD", "ZÁPAD");
        longitudeOrientation.setSelectedItem("VÝCHOD");
        longLayout.addComponents(longitudeTF, longitudeOrientation);

        supisneCisloTF.setEnabled(editable);
        supisneCisloTF.setValue(entity.getSupisneCislo() + "");
        popisTF.setEnabled(editable);
        popisTF.setValue(entity.getPopis());


        latitudeTF.setEnabled(editable);
        latitudeTF.setValue(Double.valueOf(Math.abs(entity.getLatitude())).toString().replace('.', ','));
        longitudeTF.setEnabled(editable);
        longitudeTF.setValue(Double.valueOf(Math.abs(entity.getLongtitude())).toString().replace('.', ','));
        latitudeOrientation.setEnabled(editable);
        latitudeOrientation.setSelectedItem(entity.getLatitudeOrientation());
        longitudeOrientation.setEnabled(editable);
        longitudeOrientation.setSelectedItem(entity.getLongitudeOrientation());

        entityNewValuesBinder.forField(supisneCisloTF)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(RealEstateUiTableEntity::getSupisneCislo, RealEstateUiTableEntity::setSupisneCislo);

        entityNewValuesBinder.forField(popisTF)
                .asRequired()
                .bind(RealEstateUiTableEntity::getPopis, RealEstateUiTableEntity::setPopis);

        entityNewValuesBinder.forField(latitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(RealEstateUiTableEntity::getLatitude, RealEstateUiTableEntity::setLatitude);

        entityNewValuesBinder.forField(longitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(RealEstateUiTableEntity::getLongtitude, RealEstateUiTableEntity::setLongtitude);

        fl.addComponent(supisneCisloTF);
        fl.addComponent(popisTF);

        if (editable) {
            Button editCoordsButton = new Button("Editovať pozíciu nehnuteľnosti");
            editCoordsButton.setStyleName("danger");
            fl.addComponent(editCoordsButton);
            editCoordsButton.addClickListener(e -> {
               this.editRealEstateCoords = Boolean.TRUE;
               fl.addComponent(latLayout);
               fl.addComponent(longLayout);
            });

        } else {
            fl.addComponent(latLayout);
            fl.addComponent(longLayout);
        }


        layout.addComponent(fl);

        EditFieldsGroup efg = new EditFieldsGroup();
        efg.setLayout(layout);
        efg.setLatitudeOrientation(latitudeOrientation);
        efg.setLongitudeOrientation(longitudeOrientation);

        return efg;
    }

    @Data
    private class EditFieldsGroup {

        private VerticalLayout layout;
        private RadioButtonGroup<String> latitudeOrientation;
        private RadioButtonGroup<String> longitudeOrientation;

    }

}
