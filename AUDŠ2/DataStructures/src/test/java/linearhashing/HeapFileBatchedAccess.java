package linearhashing;

import linearhashing.entities.IntegerEntity;
import org.junit.jupiter.api.*;
import sk.uniza.fri.lustyno.datastructures.linearhashing.heapfile.Batch;
import sk.uniza.fri.lustyno.datastructures.linearhashing.heapfile.HeapFile;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.FromBytesFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class HeapFileBatchedAccess {


    private String DATABASE_VERSION = "HEAP_BATCHED";

    @Test
    @Order(0)
    public void insertEntitiesAndCheckBatches() {
        HeapFile<IntegerEntity> heapFile = new HeapFile<>(
                DATABASE_VERSION, IntegerEntity.class,
                10,
                new FromBytesFactory<IntegerEntity>() {
                    @Override
                    public IntegerEntity fromBytes(byte[] bytes) {
                        return IntegerEntity.fromByteArray(bytes);
                    }

                    @Override
                    public IntegerEntity emptyInvalidInstance() {
                        return new IntegerEntity(-1, -1);
                    }
                }
        );

        Random rnd = new Random();

        for (int i = 0; i < 5237; i++) {
            int r = rnd.nextInt(997970910);
            heapFile.addRecord(new IntegerEntity(r, r));
        }


        List<Batch<IntegerEntity>> batches = heapFile.getBatchedEntities();

        System.out.println("");

    }


    @BeforeEach
    public void clearFiles() {
        FileWriter fwOb = null;
        PrintWriter pwOb = null;
        try {
            fwOb = new FileWriter("HEAP_BATCHED.hdbll", false);
            pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            pwOb.close();
            fwOb.close();
        } catch (IOException e) {

        }
    }

}
