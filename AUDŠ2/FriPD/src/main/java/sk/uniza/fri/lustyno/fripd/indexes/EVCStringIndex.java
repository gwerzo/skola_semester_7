package sk.uniza.fri.lustyno.fripd.indexes;

import lombok.Data;
import sk.uniza.fri.lustyno.datastructures.entity.keys.StringKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.HasSize;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.StringUtils;
import sk.uniza.fri.lustyno.fripd.indexes.bases.StringIndex;

import java.util.Arrays;

@HasSize(sizeInBytes = 12)
@Data
public class EVCStringIndex extends StringIndex {

    public EVCStringIndex(String key, Integer offset) {
        super(StringUtils.padRightDollars(key, 7), offset);
    }

    public static EVCStringIndex emptyInvalidInstance() {
        return new EVCStringIndex("", -1);
    }

    public static EVCStringIndex fromByteArray(byte[] fromBytes) {
        EVCStringIndex en = new EVCStringIndex("", -1);
        en.valid = FromByteArrayConverter.booleanFromByte(fromBytes[0]);
        en.key = new StringKey(FromByteArrayConverter.stringFromBytes(Arrays.copyOfRange(fromBytes, 1, 8)));
        en.offset = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(fromBytes, 8, 12));
        return en;
    }
}
