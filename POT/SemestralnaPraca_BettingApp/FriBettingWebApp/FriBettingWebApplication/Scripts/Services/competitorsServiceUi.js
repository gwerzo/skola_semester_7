﻿function getAllCompetitorsForSport(sportId, callback) {
    $.ajax({
        url: '/api/competitors/list',
        headers: {
            'Authorization': localStorage.getItem("fri-betting-app-token")
        },
        method: 'GET',
        data: {
            sportId: sportId
        },
        success: function (data) {
            callback(data);
        },
        error: function (jqXHR, textStatus, err) {
            $.notify("Nastal problém v spojení so serverom", "error");
        }
    });
}