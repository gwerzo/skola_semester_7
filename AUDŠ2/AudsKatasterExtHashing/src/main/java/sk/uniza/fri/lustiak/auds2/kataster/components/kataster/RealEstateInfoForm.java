package sk.uniza.fri.lustiak.auds2.kataster.components.kataster;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.components.custom.CustomLatitudeOrientationPicker;
import sk.uniza.fri.lustiak.auds2.kataster.components.custom.CustomLongitudeOrientationPicker;
import sk.uniza.fri.lustiak.auds2.kataster.components.custom.CustomTextArea;
import sk.uniza.fri.lustiak.auds2.kataster.components.custom.CustomTextField;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.CreateNewRealEstateUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.EditRealEstateUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.RealEstateUiTableEntity;
import sk.uniza.fri.lustiak.auds2.kataster.utils.StringToIntegerConverter;

public class RealEstateInfoForm extends VerticalLayout {

    private Binder<EditRealEstateUi> binder;
    private EditRealEstateUi entity;

    private TextField idField;
    private Button idFieldUnlocker;

    private TextField supisCisloField;
    private TextArea popisField;

    private TextField sirka1Field;
    private CustomLatitudeOrientationPicker latLayout1;
    private TextField vyska1Field;
    private CustomLongitudeOrientationPicker longLayout1;

    private TextField sirka2Field;
    private CustomLatitudeOrientationPicker latLayout2;
    private TextField vyska2Field;
    private CustomLongitudeOrientationPicker longLayout2;

    public RealEstateInfoForm(RealEstateUiTableEntity showEntity, Boolean editable) {
        this.binder = new Binder<>();
        this.entity = new EditRealEstateUi();

        this.init(showEntity, editable);
    }

    public void init(RealEstateUiTableEntity showEntity, Boolean editable) {
        // ID Field je by default pre obe možnosti vypnutý na editovanie,
        // Editovanie treba povoliť explicitne kliknutím na príslušný button
        // ktorý sa vyrenderuje len v prípade, že forma je vytvorená pre editovanie
        this.idField                 = new CustomTextField("ID", editable);
        this.idField.setIcon(FontAwesome.LOCK);
        this.idField.setEnabled(false);

        this.idFieldUnlocker = new Button("Zmeniť ID");
        this.idFieldUnlocker.addStyleName("danger");
        this.idFieldUnlocker.addClickListener(e -> {
            this.entity.setEditConstraints(Boolean.TRUE);
            this.idField.setEnabled(true);
            this.idField.setIcon(FontAwesome.UNLOCK);
        });

        this.supisCisloField         = new CustomTextField("Súpisné číslo", editable);

        this.popisField              = new CustomTextArea("Popis", editable);

        this.sirka1Field             = new CustomTextField("Zem. šírka 1", editable);

        this.vyska1Field             = new CustomTextField("Zem. výška 1", editable);

        this.sirka2Field             = new CustomTextField("Zem. šírka 2", editable);

        this.vyska2Field             = new CustomTextField("Zem. výška 2", editable);

        this.latLayout1              = new CustomLatitudeOrientationPicker(editable);

        this.longLayout1             = new CustomLongitudeOrientationPicker(editable);

        this.latLayout2              = new CustomLatitudeOrientationPicker(editable);

        this.longLayout2             = new CustomLongitudeOrientationPicker(editable);

        this.addComponents(
                new HorizontalLayout(
                        new VerticalLayout(
                                editable ? new HorizontalLayout(this.idField, this.idFieldUnlocker) : this.idField,
                                this.supisCisloField,
                                this.popisField
                        ),
                        new VerticalLayout(
                                this.sirka1Field,
                                this.latLayout1,
                                this.vyska1Field,
                                this.longLayout1
                        ),
                        new VerticalLayout(
                                this.sirka2Field,
                                this.latLayout2,
                                this.vyska2Field,
                                this.longLayout2
                        )
                )
        );

        this.initFrom(showEntity);
        this.initBinding();
    }

    private void initBinding() {
        this.binder.forField(idField)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(EditRealEstateUi::getIdentifikacneCislo, EditRealEstateUi::setIdentifikacneCislo);

        this.binder.forField(supisCisloField)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(EditRealEstateUi::getSupisneCislo, EditRealEstateUi::setSupisneCislo);

        this.binder.forField(popisField)
                .asRequired()
                .bind(EditRealEstateUi::getPopis, EditRealEstateUi::setPopis);

        this.binder.forField(vyska1Field)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(EditRealEstateUi::getLatitude1, EditRealEstateUi::setLatitude1);

        this.binder.forField(sirka1Field)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(EditRealEstateUi::getLongitude1, EditRealEstateUi::setLongitude1);

        this.binder.forField(vyska2Field)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(EditRealEstateUi::getLatitude2, EditRealEstateUi::setLatitude2);

        this.binder.forField(sirka2Field)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(EditRealEstateUi::getLongitude2, EditRealEstateUi::setLongitude2);
    }

    public void initFrom(RealEstateUiTableEntity entity) {
        this.idField.setValue("" + entity.getIdentifikacneCislo());
        this.supisCisloField.setValue("" + entity.getSupisneCislo());
        this.popisField.setValue(entity.getPopis());

        this.vyska1Field.setValue(("" + entity.getLatitude()).replace(".", ","));
        this.latLayout1.setSelectedItem(entity.getLatitudeOrientation());
        this.sirka1Field.setValue(("" + entity.getLongtitude()).replace(".", ","));
        this.longLayout1.setSelectedItem(entity.getLongitudeOrientation());

        this.vyska2Field.setValue(("" + entity.getLatitude2()).replace(".", ","));
        this.latLayout2.setSelectedItem(entity.getLatitudeOrientation2());
        this.sirka2Field.setValue(("" + entity.getLongtitude2()).replace(".", ","));
        this.longLayout2.setSelectedItem(entity.getLongitudeOrientation2());
    }

    public void writeEntity() throws ValidationException {

        Boolean idEditing = this.entity.getEditConstraints();
        if (longLayout1.getSelectedItem().equals("VÝCHOD")) {
            this.entity.setIsEast1(true);
        } else {
            this.entity.setIsEast1(false);
        }
        if (latLayout1.getSelectedItem().equals("SEVER")) {
            this.entity.setIsNorth1(true);
        } else {
            this.entity.setIsNorth1(false);
        }
        if (longLayout2.getSelectedItem().equals("VÝCHOD")) {
            this.entity.setIsEast2(true);
        } else {
            this.entity.setIsEast2(false);
        }
        if (latLayout2.getSelectedItem().equals("SEVER")) {
            this.entity.setIsNorth2(true);
        } else {
            this.entity.setIsNorth2(false);
        }

        this.entity.setEditConstraints(idEditing);
        this.binder.writeBean(this.entity);

    }

    public EditRealEstateUi getEditEntity() {
        return this.entity;
    }

    public void lockIdField() {
        this.idField.setEnabled(false);
    }

}
