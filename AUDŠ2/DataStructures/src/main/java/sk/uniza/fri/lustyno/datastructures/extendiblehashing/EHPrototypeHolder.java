package sk.uniza.fri.lustyno.datastructures.extendiblehashing;

import sk.uniza.fri.lustyno.datastructures.extendiblehashing.entities.EHEntity;

public class EHPrototypeHolder<T extends EHEntity<T>> {

    /**
     * Prototyp entity, nad ktorou pracuje toto rozšíriteľné hashovanie
     */
    private T entityPrototype;

    /**
     * Prototyp bloku, nad ktorými pracuje toto rozšíriteľné hashovanie
     */
    private EHBlock<T> EHBlockPrototype;

    /**
     * Prototyp overflow bloku, nad ktorými pracuje toto rozšíriteľné hashovanie
     */
    private EHOverflowBlock<T> overflowBlockPrototype;

    public EHPrototypeHolder(T entityPrototype, EHBlock<T> EHBlockPrototype, EHOverflowBlock<T> overflowBlockPrototype) {
        this.entityPrototype = entityPrototype;
        this.EHBlockPrototype = EHBlockPrototype;
        this.overflowBlockPrototype = overflowBlockPrototype;
    }


    public T getEntityPrototype() {
        return entityPrototype;
    }

    public EHBlock<T> getEHBlockPrototype() {
        return EHBlockPrototype;
    }

    public EHOverflowBlock<T> getOverflowBlockPrototype() {
        return overflowBlockPrototype;
    }
}
