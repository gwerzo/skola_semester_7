package sk.uniza.fri.lustiak.auds2.kataster.components.system;

import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;

public class SaveStateForm extends BusinessComponent {

    private RealEstateService realEstateService;

    public SaveStateForm(RepaintableComponent component) {
        super(component, MENU_TITLE.SYSTEM_MENU_4.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        VerticalLayout layout = new VerticalLayout();

        Button saveButton = new Button("Uložiť stav");
        saveButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {
                this.realEstateService.saveState();
                CustomNotification.notification("Stav databázy bol uložený");
                return OperationMarker.OPERATION_SUCCESFUL;
            });
        });

        layout.addComponent(saveButton);

        this.parentComponent.showComponent(new Panel("Uloženie stavu databázy", layout));

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_4.getTitle();
    }
}
