package sk.uniza.fri.lustyno.fripd.components.common;

public enum MENU_TITLE {

    //----------------------MENU PRE VOZIDLÁ--------------------------------------
    VEHICLES_MENU_1("1. Pridanie nového vozidla"),
    VEHICLES_MENU_2("2. Zmena údajov o vozidle"),
    VEHICLES_MENU_3("3. Vyhľadanie vozidla podľa VIN"),
    VEHICLES_MENU_4("4. Vyhľadanie vozidla podľa EVČ"),
    VEHICLES_MENU_5("5. Vymazanie vozidla podľa VIN"),
    VEHICLES_MENU_6("6. Vymazanie vozidla podľa EVČ"),

    //----------------------MENU PRE PREUKAZY-------------------------------------
    CARDS_MENU_1("1. Pridanie nového preukazu"),
    CARDS_MENU_2("2. Vyhľadanie podľa EVČ"),
    CARDS_MENU_3("3. Zmena údajov"),
    CARDS_MENU_4("4. Vymazanie preukazu"),

    //---------------------MENU PRE SYSTÉM----------------------------------------
    SYSTEM_MENU_1("1. Uloženie systému"),
    SYSTEM_MENU_2("2. Vygenerovanie dát do systému"),

    SYSTEM_MENU_3("3. Bloky LinearHashing(VIN) - Vozidlá"),
    SYSTEM_MENU_3_1("3.1 Overflow bloky LinearHashing(VIN) - Vozidlá"),


    SYSTEM_MENU_4("4. Bloky LinearHashing(EVČ) - Vozidlá"),
    SYSTEM_MENU_4_1("4.1 Overflow bloky LinearHashing(EVČ) - Vozidlá"),


    SYSTEM_MENU_5("5. Bloky LinearHashing - Preukazy"),
    SYSTEM_MENU_5_1("5.1 Overflow bloky LinearHashing - Preukazy"),

    SYSTEM_MENU_6("6. Bloky HeapFilu - Vozidlá");

    private String title;

    MENU_TITLE(String s) {
        title = s;
    }

    public String getTitle() {
        return title;
    }
}
