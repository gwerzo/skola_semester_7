﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DisertationThemes.Database
{
    public class StProgram
    {
        public int StProgramId { get; set; }
        public string Name { get; set; }
        public string FieldOfStudy { get; set; }


        public List<Theme> Themes { get; set; }


        public StProgram()
        {
            this.Themes = new List<Theme>();
        }

    }
}
