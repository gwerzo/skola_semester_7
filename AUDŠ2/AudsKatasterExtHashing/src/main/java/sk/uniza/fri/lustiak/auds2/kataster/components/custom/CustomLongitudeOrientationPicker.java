package sk.uniza.fri.lustiak.auds2.kataster.components.custom;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.RadioButtonGroup;

public class CustomLongitudeOrientationPicker extends HorizontalLayout {

    private RadioButtonGroup<Object> options;

    public CustomLongitudeOrientationPicker(Boolean enabled) {
        this.options = new RadioButtonGroup<>("Orientácia zemepisnej šírky");
        this.options.setItems("VÝCHOD", "ZÁPAD");
        this.options.setSelectedItem("VÝCHOD");
        this.addComponents(this.options);
        this.options.setEnabled(enabled);
    }

    public void setSelectedItem(String selectedItem) {
        this.options.setSelectedItem(selectedItem);
    }

    public String getSelectedItem() {
        return (String)this.options.getSelectedItem().get();
    }

}
