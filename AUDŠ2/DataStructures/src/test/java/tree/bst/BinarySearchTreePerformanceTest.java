package tree.bst;

import org.junit.jupiter.api.*;
import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTree;
import sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTreeNode;
import utils.CheckedItemWrapper;

import java.util.*;

import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BinarySearchTreePerformanceTest {

    private final int NUMBER_OF_ITEMS = 1000000;
    private ArrayList<Integer> randomNumbers;

    private BinarySearchTree<IntegerKey, String> bst;
    private HashMap<IntegerKey, String> toCheck;
    private ArrayList<CheckedItemWrapper> checked;

    private ArrayList<BinarySearchTreeNode<IntegerKey, String>> alreadyAllocatedNodes;
    private BinarySearchTree<IntegerKey, String> bstForAlreadyAllocatedNodes;

    private Random rnd;

    @BeforeAll
    public void init() {
        this.randomNumbers = new ArrayList<>(NUMBER_OF_ITEMS);
        this.toCheck  = new HashMap<>();
        this.checked = new ArrayList<>(NUMBER_OF_ITEMS);
        this.bst = new BinarySearchTree<>();
        this.rnd = new Random();

        fillAndShuffleNumbers();
        prepareCheckedIndexes();
        prepareToCheckItems();
        prepareItemsForAlreadyAllocatedTest();
    }

    /**
     * Performance test pre vyplnenie stromu miliónom
     * záznamom, pričom niektoré z nich sú odložené
     * pre využitie v budúcich metódach
     * <p>
     * Vyplnenie BST s 1 000 000 shuffled integermi,
     * kde s 1% šancou je dátová časť nodu náhodný
     * stringový reťazec, ktorý môže byť neskôr
     * využitý na overenie, či podľa key/value
     * páru nájde strom práve prvok s touto hodnotou
     */
    @Test
    @Order(1)
    public void performance_TEST_INSERT_RANDOM_NODES() {
        Iterator<Integer> iter = this.randomNumbers.iterator();
        Iterator<CheckedItemWrapper> iterOverChecked = this.checked.iterator();

        while (iter.hasNext()) {
            IntegerKey key = new IntegerKey(iter.next());

            CheckedItemWrapper item = iterOverChecked.next();

            BinarySearchTreeNode<IntegerKey, String> node = new BinarySearchTreeNode<>(key, item.getString());
            bst.insert(node);
        }
    }

    /**
     * Performance test pre insert
     * {@link BinarySearchTreePerformanceTest#NUMBER_OF_ITEMS}
     * položiek do BST, ktoré sú
     * ale alokované dopredu,
     * pre odhliadnutie času alokovania NODOv
     */
    @Test
    @Order(2)
    public void performance_TEST_INSERT_RANDOM_NODES_ALREADY_ALLOCATED() {
        Iterator<BinarySearchTreeNode<IntegerKey, String>> iter = this.alreadyAllocatedNodes.iterator();

        while (iter.hasNext()) {
            bstForAlreadyAllocatedNodes.insert(iter.next());
        }
    }

    /**
     * Performance test pre vyhľadávanie v strome,
     * kde sa ráta s tým, že vyhľadávaných bude
     * cca. 1% z {@link BinarySearchTreePerformanceTest#NUMBER_OF_ITEMS}
     * <p>
     * Zároveň overí aj to, či nody majú správne dáta
     */
    @Test
    @Order(3)
    public void performance_TEST_FIND_BY_KEY() {
        for(Map.Entry<IntegerKey, String> entry : toCheck.entrySet()) {
           BinarySearchTreeNode<IntegerKey, String> nodeFound = bst.find(entry.getKey()).get();
           if (!nodeFound.getData().equals(entry.getValue())) {
               fail();
           }
        }
    }


    /**
     * Vymaže 1% z {@link BinarySearchTreePerformanceTest#NUMBER_OF_ITEMS}
     * položiek z BST, ktoré sú označené ako "checked",
     */
    @Test
    @Order(4)
    public void performance_TEST_DELETE_NODES() {
        for(Map.Entry<IntegerKey, String> entry : toCheck.entrySet()) {
            bst.delete(entry.getKey());
        }
        Assertions.assertEquals(bst.getSize(), NUMBER_OF_ITEMS - toCheck.size());
    }

    /**
     * Overí, či v BST ostali len nody,
     * ktoré nemali byť vymazané
     * <p>
     * Inými slovami skúsi nájsť v BST nody,
     * ktoré boli vymazané, a padne,
     * ak niektorý z týchto nodov bude nájdený
     */
    @Test
    @Order(5)
    public void performance_CONTAINS_ONLY_NOT_DELETED() {
        List<BinarySearchTreeNode> foundNodesThatWereDeleted = new ArrayList<>();
        for(Map.Entry<IntegerKey, String> entry : toCheck.entrySet()) {
            BinarySearchTreeNode found = bst.find(entry.getKey()).get();
            if (found != null) {
                foundNodesThatWereDeleted.add(found);
            }
        }
        Assertions.assertEquals(0, foundNodesThatWereDeleted.size());
    }

    /**
     * Test vykoná inOrder prehliadku stromu,
     * overí teda, či nevznikde zacyklenie pri
     * prechádzaní nodmi.
     * Overí aj to, či sú nody v inOrder
     * prehliadke správne usporiadané,
     * t.j. v neklesajúcej postupnosti podľa kľúčov
     */
    @Test
    @Order(6)
    public void performance_WALK_IN_ORDER() {
        List<BinarySearchTreeNode<IntegerKey, String>> inOrder = bst.inOrder();

        int lowest = Integer.MIN_VALUE;
        for (BinarySearchTreeNode<IntegerKey, String> node : inOrder) {
            if (node.getKey().getValue().intValue() > lowest) {
                lowest = node.getKey().getValue().intValue();
            } else {
                fail("INORDER prehliadka nie je v správnom poradí");
            }
        }
    }

    // --- Inicializačné metódy pre potreby testu

    private void prepareItemsForAlreadyAllocatedTest() {
        this.bstForAlreadyAllocatedNodes = new BinarySearchTree<>();
        this.alreadyAllocatedNodes = new ArrayList<>(NUMBER_OF_ITEMS);
        for (int i = 0; i < NUMBER_OF_ITEMS; i++) {
            alreadyAllocatedNodes.add(new BinarySearchTreeNode<>(new IntegerKey(this.randomNumbers.get(i)), "STRING"));
        }
    }

    private void prepareToCheckItems() {
        for (int i = 0; i < checked.size(); i++) {
            if (checked.get(i).isChecked()) {
                toCheck.put(new IntegerKey(this.randomNumbers.get(i)), checked.get(i).getString());
            }
        }
    }

    private void prepareCheckedIndexes() {
        for (int i = 0; i < NUMBER_OF_ITEMS; i++) {
            if (rnd.nextInt(100) == 0) {
                checked.add(new CheckedItemWrapper(true));
            } else {
                checked.add(new CheckedItemWrapper(false));
            }
        }
    }

    private void fillAndShuffleNumbers() {
        for (int i = 0; i < NUMBER_OF_ITEMS; i++) {
            this.randomNumbers.add(i);
        }
        Collections.shuffle(this.randomNumbers);
    }
}
