package sk.uniza.fri.lustyno.fripd.service;

import org.springframework.stereotype.Service;
import sk.uniza.fri.lustyno.datastructures.entity.keys.StringKey;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureOperationFailedException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureWrongUsageException;
import sk.uniza.fri.lustyno.datastructures.linearhashing.config.FileConfig;
import sk.uniza.fri.lustyno.datastructures.linearhashing.config.HeapFileConfig;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Block;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.File;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.OverflowBlock;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Record;
import sk.uniza.fri.lustyno.datastructures.linearhashing.heapfile.Batch;
import sk.uniza.fri.lustyno.datastructures.linearhashing.heapfile.HeapFile;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.SPLIT_VARIANT;
import sk.uniza.fri.lustyno.datastructures.utils.StringUtils;
import sk.uniza.fri.lustyno.fripd.BusinessException;
import sk.uniza.fri.lustyno.fripd.entities_bo.VehicleEntity;
import sk.uniza.fri.lustyno.fripd.entities_ui.block.BlockDetailEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.block.ZaznamEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.PridanieVozidlaEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.VozidloEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.VyhladanieVozidlaEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.ZmenaUdajovVozidlaEntityUi;
import sk.uniza.fri.lustyno.fripd.entity_mapper.EntityMapper;
import sk.uniza.fri.lustyno.fripd.factory.EVCStringIndexFactory;
import sk.uniza.fri.lustyno.fripd.factory.VINCODEStringIndexFactory;
import sk.uniza.fri.lustyno.fripd.factory.VehicleFactory;
import sk.uniza.fri.lustyno.fripd.indexes.EVCStringIndex;
import sk.uniza.fri.lustyno.fripd.indexes.VINCODEStringIndex;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

@Service
public class VehicleService {

//    private final String vinCodeIndexFilePath = "";
//    private final String evcCodeIndexFilePath = "";
//    private final String vehiclesDatabaseFilePath = "";

    private final String vinCodeIndexFilePath = "VIN_INDEX";
    private final String evcCodeIndexFilePath = "EVC_INDEX";
    private final String vehiclesDatabaseFilePath = "VEH_DB";

    private File<StringKey, VINCODEStringIndex> vinCodeIndex;
    private Integer VIN_NUMBER_OF_REC_PRIMARY_BLOCK = 2;
    private Integer VIN_NUMBER_OF_REC_OVERFLOW_BLOCK = 3;

    private File<StringKey, EVCStringIndex> evcCodeIndex;
    private Integer EVC_NUMBER_OF_REC_PRIMARY_BLOCK = 2;
    private Integer EVC_NUMBER_OF_REC_OVERFLOW_BLOCK = 3;

    private HeapFile<VehicleEntity> vehicles;
    private Integer VEH_NUMBER_OF_REC_HEAPFILE = 3;

    public VehicleService() {
        initializeVinCodeIndex();
        initializeEvcCodeIndex();
        initializeVehiclesDatabase();
    }

    private void initializeVehiclesDatabase() {
        if (this.vehiclesDatabaseFilePath.isEmpty()) {
            this.vehicles = new HeapFile<>(
                "VEH_DB",
                VehicleEntity.class,
                VEH_NUMBER_OF_REC_HEAPFILE,
                new VehicleFactory());
        } else {
            HeapFileConfig hfc = HeapFileConfig.fromFile(this.vehiclesDatabaseFilePath + "_conf.conf");
            this.vehicles = new HeapFile<>(
                    hfc,
                    VehicleEntity.class,
                    new VehicleFactory());
        }
    }

    private void initializeEvcCodeIndex() {
        if (this.evcCodeIndexFilePath.isEmpty()) {
            this.evcCodeIndex = new File<>(
                "EVC_INDEX",
                    EVC_NUMBER_OF_REC_PRIMARY_BLOCK,
                    EVC_NUMBER_OF_REC_OVERFLOW_BLOCK,
                EVCStringIndex.class,
                SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR,
                new EVCStringIndexFactory()
            );
        } else {
            FileConfig fc = FileConfig.fromFile(this.evcCodeIndexFilePath + "_conf.conf");
            this.evcCodeIndex = new File<>(
                    fc,
                    EVCStringIndex.class,
                    new EVCStringIndexFactory()
            );
        }
    }

    private void initializeVinCodeIndex() {
        if (this.vinCodeIndexFilePath.isEmpty()) {
            this.vinCodeIndex = new File<>(
                "VIN_INDEX",
                    VIN_NUMBER_OF_REC_PRIMARY_BLOCK,
                    VIN_NUMBER_OF_REC_OVERFLOW_BLOCK,
                VINCODEStringIndex.class,
                SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR,
                new VINCODEStringIndexFactory()
            );
        } else {
            FileConfig fc = FileConfig.fromFile(this.vinCodeIndexFilePath + "_conf.conf");
            this.vinCodeIndex = new File<>(
                    fc,
                    VINCODEStringIndex.class,
                    new VINCODEStringIndexFactory()
            );
        }
    }

    /**
     * Pridá nové auto do databázy,
     * pridá ho samostatne do HeapFile
     * a samostatne do indexového súboru
     * @param pridanieVozidlaEntityUi
     */
    public void addNewVehicle(PridanieVozidlaEntityUi pridanieVozidlaEntityUi) {
        VehicleEntity vehicleEntity = EntityMapper.fromPridanieVozidlaEntityUi(pridanieVozidlaEntityUi);
        vehicleEntity.validate();
        VINCODEStringIndex alreadyStored = this.vinCodeIndex.find(new VINCODEStringIndex(vehicleEntity.getVIN_KOD(), -1));
        if (alreadyStored != null) {
            throw new BusinessException("Auto s takýmto VIN sa už v databáze nachádza");
        }
        saveVehicle(vehicleEntity);
    }

    /**
     * Vráti záznam o aute,
     * pokiaľ sa v DB nachádza,
     * ak nie, vráti null
     */
    public VozidloEntityUi findVehicleByVIN(VyhladanieVozidlaEntityUi vyhladanieVozidlaEntityUi) {
        String VIN_CODE = StringUtils.padRightDollars(vyhladanieVozidlaEntityUi.getVin(), 17);

        if (VIN_CODE == null || VIN_CODE.isEmpty()) {
            throw new BusinessException("VIN kód pre vyhľadanie vozidla je povinný parameter");
        }
        VINCODEStringIndex indexOffset = this.vinCodeIndex.find(new VINCODEStringIndex(VIN_CODE, -1));
        if (indexOffset == null || !indexOffset.isValid()) {
            throw new BusinessException("Vozidlo s takýmto VIN sa nenašlo");
        }

        VehicleEntity entity = this.vehicles.getRecordAtOffset(indexOffset.getOffset());
        if (entity == null || !entity.isValid()) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "findVehicleByVIN", "Vozidlo sa nenašlo v databáze, ale podľa indexu existuje");
        }

        return EntityMapper.fromVehicleEntityToVehicleEntityUi(entity);
    }

    /**
     * Vráti záznam o aute podľa EVČ
     * pokiaľ sa v DB nachádza,
     * ak nie, vráti null
     */
    public VozidloEntityUi findVehicleByEVC(VyhladanieVozidlaEntityUi vyhladanieVozidlaEntityUi) {
        String EVC_CODE = StringUtils.padRightDollars(vyhladanieVozidlaEntityUi.getEvc(), 7);

        if (EVC_CODE == null || EVC_CODE.isEmpty()) {
            throw new BusinessException("EVČ kód pre vyhľadanie vozidla je povinný parameter");
        }
        EVCStringIndex indexOffset = this.evcCodeIndex.find(new EVCStringIndex(EVC_CODE, -1));
        if (indexOffset == null || !indexOffset.isValid()) {
            throw new BusinessException("Vozidlo s takýmto EVČ sa nenašlo");
        }

        VehicleEntity entity = this.vehicles.getRecordAtOffset(indexOffset.getOffset());
        if (entity == null || !entity.isValid()) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "findVehicleByEVC", "Vozidlo sa nenašlo v databáze, ale podľa indexu existuje");
        }

        return EntityMapper.fromVehicleEntityToVehicleEntityUi(entity);
    }

    /**
     * Zmení údaje o vozidle,
     * ale keďže nemení unikátne kľúče
     * stačí meniť údaje v HeapFile a indexov sa nedotýkať
     */
    public void updateVehicle(ZmenaUdajovVozidlaEntityUi entityUi) {
        EVCStringIndex evcStringIndex = this.evcCodeIndex.find(new EVCStringIndex(entityUi.getEVC(), -1));
        if (evcStringIndex == null || !evcStringIndex.isValid()) {
            throw new BusinessException("Vozidlo s EVC kódom " + entityUi.getVIN() + " nebolo nájdené");
        }
        VehicleEntity oldForVIN = this.vehicles.getRecordAtOffset(evcStringIndex.getOffset());

        entityUi.setVIN("");
        VehicleEntity entity = EntityMapper.fromZmenaUdajovVozidlaEntityUi(entityUi);
        entity.setVIN_KOD(oldForVIN.getVIN_KOD());
        entity.setValid(true);

        try {
            this.vehicles.updateRecordWithoutConstraintsChange(entity, evcStringIndex.getOffset());
        } catch (DataStructureWrongUsageException e) {
            throw new BusinessException(e.getMessage());
        }
    }


    /**
     * Metóda pre stornutie záznamu o aute do všetkých súborov/indexov
     * @param vehicleEntity
     */
    private void saveVehicle(VehicleEntity vehicleEntity) {
        Integer offset = this.vehicles.addRecord(vehicleEntity);
        this.vinCodeIndex.saveRecord(new VINCODEStringIndex(vehicleEntity.getVIN_KOD(), offset));
        this.evcCodeIndex.saveRecord(new EVCStringIndex(vehicleEntity.getEVC(), offset));
    }

    public List<BlockDetailEntityUi> getAllVehiclesByVINPrimaryBlocks() {
        List<BlockDetailEntityUi> bloky = new LinkedList<>();

        FileConfig fc = this.vinCodeIndex.getActualConfiguration();

        for (int i = 0; i < fc.secondLevelModulo; i++) {
            BlockDetailEntityUi entityUi = new BlockDetailEntityUi();

            Block<VINCODEStringIndex> blok = this.vinCodeIndex.getBlockByIndex(i);

            entityUi.setOffsetVSubore(fc.blockSize * i);
            entityUi.setOffsetPreplnovaciehoBloku(blok.getOverflowBlockOffset());
            entityUi.setPocetValidnychZaznamov(blok.validRecordsCount());
            entityUi.setZaznamyBloku(new LinkedList<>());
            entityUi.setCisloBloku(i);

            for (Record rec : blok.getRecordsList()) {
                ZaznamEntityUi entityUi1 = new ZaznamEntityUi();
                entityUi1.setKluc(rec.getKey().getValue().toString());
                entityUi1.setValidny(rec.isValid() ? "Áno" : "Nie");
                entityUi.getZaznamyBloku().add(entityUi1);
            }

            bloky.add(entityUi);
        }

        return bloky;
    }


    public List<BlockDetailEntityUi> getAllVehiclesByEVCPrimaryBlocks() {
        List<BlockDetailEntityUi> bloky = new LinkedList<>();

        FileConfig fc = this.evcCodeIndex.getActualConfiguration();

        for (int i = 0; i < fc.secondLevelModulo; i++) {
            BlockDetailEntityUi entityUi = new BlockDetailEntityUi();


            Block<EVCStringIndex> blok = this.evcCodeIndex.getBlockByIndex(i);

            entityUi.setOffsetVSubore(fc.blockSize * i);
            entityUi.setOffsetPreplnovaciehoBloku(blok.getOverflowBlockOffset());
            entityUi.setPocetValidnychZaznamov(blok.validRecordsCount());
            entityUi.setZaznamyBloku(new LinkedList<>());
            entityUi.setCisloBloku(i);

            for (Record rec : blok.getRecordsList()) {
                ZaznamEntityUi entityUi1 = new ZaznamEntityUi();
                entityUi1.setKluc(rec.getKey().getValue().toString());
                entityUi1.setValidny(rec.isValid() ? "Áno" : "Nie");
                entityUi.getZaznamyBloku().add(entityUi1);
            }

            bloky.add(entityUi);
        }

        return bloky;
    }

    /**
     * Vráti všetky bloky HeapFile-u vozidiel,
     * podľa veľkosti blokov
     * @return
     */
    public List<BlockDetailEntityUi> getAllVehiclesInBlocks() {
        List<Batch<VehicleEntity>> entities = this.vehicles.getBatchedEntities();

        List<BlockDetailEntityUi> entityUis = new LinkedList<>();

        for (Batch<VehicleEntity> batch : entities) {

            BlockDetailEntityUi entityUi = new BlockDetailEntityUi();
            entityUi.setCisloBloku(batch.getBatchStartingOffset());

            List<ZaznamEntityUi> zaznamy = new LinkedList<>();
            for (VehicleEntity ent : batch.getEntities()) {

                ZaznamEntityUi entUi = new ZaznamEntityUi();
                entUi.setValidny(ent.isValid() ? "Áno" : "Nie");
                entUi.setKluc(ent.getVIN_KOD());
                entUi.setHodnota(ent.getEVC());
                zaznamy.add(entUi);
            }

            entityUi.setZaznamyBloku(zaznamy);

            entityUis.add(entityUi);
        }

        return entityUis;
    }

    /**
     * Vráti overflow bloky indexu vozidiel podľa VIN
     * @return
     */
    public List<BlockDetailEntityUi> getAllVehiclesByVINOverflowBlocks() {
        List<OverflowBlock<VINCODEStringIndex>> entities = this.vinCodeIndex.getAllOverflowBlocks();

        List<BlockDetailEntityUi> entityUis = new LinkedList<>();

        for (OverflowBlock<VINCODEStringIndex> block : entities) {

            BlockDetailEntityUi entityUi = new BlockDetailEntityUi();
            entityUi.setCisloBloku(block.getOverflowOffset());
            entityUi.setOffsetPreplnovaciehoBloku(block.getNextOverflowBlock().getOverflowOffset());

            List<ZaznamEntityUi> zaznamy = new LinkedList<>();
            for (Record ent : block.getRecordList()) {

                VINCODEStringIndex entTyped = (VINCODEStringIndex) ent;

                ZaznamEntityUi entUi = new ZaznamEntityUi();
                entUi.setValidny(ent.isValid() ? "Áno" : "Nie");
                entUi.setKluc(entTyped.getKey().getValue());
                entUi.setHodnota(entTyped.getOffset().toString());
                zaznamy.add(entUi);
            }

            entityUi.setZaznamyBloku(zaznamy);

            entityUis.add(entityUi);
        }

        return entityUis;
    }

    /**
     * Vráti overflow bloky indexu vozidiel podľa EVČ
     * @return
     */
    public List<BlockDetailEntityUi> getAllVehiclesByEVCOverflowBlocks() {
        List<OverflowBlock<EVCStringIndex>> entities = this.evcCodeIndex.getAllOverflowBlocks();

        List<BlockDetailEntityUi> entityUis = new LinkedList<>();

        for (OverflowBlock<EVCStringIndex> block : entities) {

            BlockDetailEntityUi entityUi = new BlockDetailEntityUi();
            entityUi.setCisloBloku(block.getOverflowOffset());
            entityUi.setOffsetPreplnovaciehoBloku(block.getNextOverflowBlock().getOverflowOffset());

            List<ZaznamEntityUi> zaznamy = new LinkedList<>();
            for (Record ent : block.getRecordList()) {

                EVCStringIndex entTyped = (EVCStringIndex) ent;

                ZaznamEntityUi entUi = new ZaznamEntityUi();
                entUi.setValidny(ent.isValid() ? "Áno" : "Nie");
                entUi.setKluc(entTyped.getKey().getValue());
                entUi.setHodnota(entTyped.getOffset().toString());
                zaznamy.add(entUi);
            }

            entityUi.setZaznamyBloku(zaznamy);

            entityUis.add(entityUi);
        }

        return entityUis;
    }

    /**
     * Uzatvorí všetky súbory
     */
    public void finalize() {
        this.evcCodeIndex.saveConfiguration();
        this.vinCodeIndex.saveConfiguration();
        this.vehicles.saveConfiguration();

        this.evcCodeIndex.close();
        this.vehicles.close();
        this.vinCodeIndex.close();
    }


    /**
     * Nageneruje 100 vozidiel do databázy
     */
    public void generateVehicles() {
        for (int i = 0; i < 30; i++) {
            VehicleEntity entity = new VehicleEntity(
                    VehicleEntity.randomVIN(), VehicleEntity.randomSPZ(),
                    2000, 4, false,
                    LocalDate.now().atTime(LocalTime.MAX), LocalDate.now().atTime(LocalTime.MAX));

            this.saveVehicle(entity);
        }
    }

}
