package sk.uniza.fri.lustiak.auds2.kataster.components.common;

import sk.uniza.fri.lustiak.auds2.kataster.components.system.GeneratorSystemuForm;
import sk.uniza.fri.lustiak.auds2.kataster.components.system.LoadStateFromFileForm;
import sk.uniza.fri.lustiak.auds2.kataster.components.system.InfoBlokForm;
import sk.uniza.fri.lustiak.auds2.kataster.components.system.SaveStateForm;

public class SystemMenuComponent extends AbstractMenu {

    public SystemMenuComponent(RepaintableComponent component) {
        this.menuItems.add(new GeneratorSystemuForm(component));
        this.menuItems.add(new LoadStateFromFileForm(component));
        this.menuItems.add(new InfoBlokForm(component));
        this.menuItems.add(new SaveStateForm(component));
        initMenuItems();
    }

}
