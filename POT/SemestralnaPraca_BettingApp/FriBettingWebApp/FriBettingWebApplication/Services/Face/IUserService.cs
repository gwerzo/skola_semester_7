﻿using FriBettingWebApplication.Dto.Request;
using FriBettingWebApplication.Dto.Response;

namespace FriBettingWebApplication.Services.Face
{
    public interface IUserService
    {

        OperationResponse RegisterUser(RegisterUserRequest registerUserRequest);

        OperationResponse LoginUser(LoginUserRequest loginUserRequest);

        AccountDetailsResponse GetAccountDetails(string usernameIdentity);

    }
}