﻿using System;
using System.Collections.Generic;
using System.Text;
using DisertationThemes.Database;

namespace DisertationThemes.ViewerGeneratorApp.menu
{
    class MenuItemShowStPrograms: AbstractMenuItem
    {
        public static string MENU_TEXT = "Zobraziť všetky študejné programy";
        public MenuItemShowStPrograms(MenuHolder menuHolder) : base(menuHolder)
        {
        }

        public override string GetMenuText()
        {
            return MENU_TEXT;
        }

        protected override int PerformActionInternally()
        {
            using (DisertationDatabaseContext ddc = new DisertationDatabaseContext())
            {
                foreach (var stProgram in ddc.StPrograms)
                {
                    Console.WriteLine("\t" + stProgram.Name + "\t" + stProgram.FieldOfStudy);
                }
            }
            return 0;
        }
    }
}
