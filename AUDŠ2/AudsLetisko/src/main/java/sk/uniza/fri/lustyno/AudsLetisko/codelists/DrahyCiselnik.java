package sk.uniza.fri.lustyno.AudsLetisko.codelists;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@Service
public class DrahyCiselnik {

    private ArrayList<Integer> dlzkyDrah;

    private DrahyCiselnik() {
        this.dlzkyDrah = new ArrayList<>();

        this.dlzkyDrah.addAll(Arrays.asList(1500, 2000, 2500, 3000, 3500));
    }

    public ArrayList<Integer> getAllDlzkyDrah() {
        return dlzkyDrah;
    }

    public void addNovaDlzkaDrahy(Integer dlzka) {
        if (!this.dlzkyDrah.contains(dlzka)) {
            this.dlzkyDrah.add(dlzka);
            Collections.sort(this.dlzkyDrah);
        }
    }

    /**
     * Vráti najbližšiu dĺžku dráhy pre zadané číslo
     *
     * @param from - minimálna dĺžka dráhy ktorá je potrebná
     * @return - najbližšia dĺžka dráhy, ktorá je možná
     */
    public Integer getNajblizsiaDlzkaMoznaDlzka(Integer from) {
        for (Integer dlzka : this.dlzkyDrah) {
            if (dlzka >= from) {
                return dlzka;
            }
        }
        return null;
    }


}
