package sk.uniza.fri.lustyno.datastructures.linearhashing.utils;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface HasSize {

    int sizeInBytes() default 0;

}
