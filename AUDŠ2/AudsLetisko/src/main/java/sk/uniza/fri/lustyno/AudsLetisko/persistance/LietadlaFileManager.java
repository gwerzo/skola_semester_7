package sk.uniza.fri.lustyno.AudsLetisko.persistance;

import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.uniza.fri.lustyno.AudsLetisko.BusinessException;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.LietadloEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.AircraftManagementService;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;
import java.util.List;

import static java.util.Optional.ofNullable;

@Service
public class LietadlaFileManager {

    private final String VSETKY_LIETADLA_FILE           = "fileResources/vsetky_lietadla.csv";

    private final String IDLE_LIETADLA_FILE             = "fileResources/idle_lietadla.csv";

    private final String S_PRIDELENOU_DRAHOU_FILE       = "fileResources/s_pridelenou_drahou.csv";

    private final String CAKAJUCE_NA_PRIDELENIE_FILE    = "fileResources/cakajuce_na_pridelenie.csv";


    @Autowired
    private AircraftManagementService aircraftManagementService;

    public void vycistiSubory() {

        try {
            PrintWriter writer1 = new PrintWriter(VSETKY_LIETADLA_FILE);
            writer1.close();

            PrintWriter writer2 = new PrintWriter(IDLE_LIETADLA_FILE);
            writer2.close();

            PrintWriter writer3 = new PrintWriter(S_PRIDELENOU_DRAHOU_FILE);
            writer3.close();

            PrintWriter writer4 = new PrintWriter(CAKAJUCE_NA_PRIDELENIE_FILE);
            writer4.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Uloží všetky lietadlá do súboru, na ktoré sa môžu referencovať ostatné entity
     */
    public void ulozVsetkyLietadla() {
        List<LietadloEntity> vsetkyLietadlaVSysteme =
                aircraftManagementService.getAllLietadlaVSystemePreUlozenieDoSuboru();
        if (vsetkyLietadlaVSysteme == null || vsetkyLietadlaVSysteme.size() == 0) {
            return;
        }

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(VSETKY_LIETADLA_FILE));

            for (LietadloEntity lietadlo : vsetkyLietadlaVSysteme) {
                StringBuilder sb = new StringBuilder();

                sb.append(lietadlo.getMedzinarodnyKod() + ",");
                sb.append(lietadlo.getNazovVyrobcu() + ",");
                sb.append(lietadlo.getMinimalnaDlzkaOdletovejDrahy() + ",");
                sb.append(lietadlo.getPriorita() + ",");
                sb.append(ofNullable(lietadlo.getCasDatumPriletu()).map(e -> e.toString()).orElse("") + ",");
                sb.append(ofNullable(lietadlo.getCasDatumPoziadaniaOPridelenie()).map(e -> e.toString()).orElse("") + ",");
                sb.append(ofNullable(lietadlo.getCasDatumOdletu()).map(e -> e.toString()).orElse("") + ",");

                bw.write(sb.toString());
                bw.newLine();
            }

            bw.flush();
        } catch (IOException ioEx) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na zápis všetkých lietadiel");
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Načíta všetky lietadlá ktoré boli v systéme zo súboru
     */
    public void nacitajVsetkyLietadlaDoSystemu() {
        List<LietadloEntity> vsetkyLietadlaZoSuboru = new LinkedList<>();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(VSETKY_LIETADLA_FILE));

            String line = null;
            while ((line = br.readLine()) != null) {

                String[] splitted = line.split(",", -1);

                String medzinarodnyKod = splitted[0];
                String vyrobca = splitted[1];
                Integer minimalnaDlzkaDrahy = Integer.parseInt(splitted[2]);
                Integer priorita = Integer.parseInt(splitted[3]);

                String casPriletuString = splitted[4];
                String casPoziadaniaString = splitted[5];
                String casOdletuString = splitted[6];

                LocalDateTime casDatumPriletu = null;
                LocalDateTime casDatumPoziadavky = null;
                LocalDateTime casDatumOdletu = null;

                if (casPriletuString != null && !casPriletuString.isEmpty()) {
                    try {
                        casDatumPriletu = LocalDateTime.parse(casPriletuString);
                    } catch (DateTimeParseException ex) {
                        casDatumPriletu = null;
                    }
                }

                if (casPoziadaniaString != null && !casPoziadaniaString.isEmpty()) {
                    try {
                        casDatumPoziadavky = LocalDateTime.parse(casPoziadaniaString);
                    } catch (DateTimeParseException ex) {
                        casDatumPoziadavky = null;
                    }
                }

                if (casOdletuString != null && !casOdletuString.isEmpty()) {
                    try {
                        casDatumOdletu = LocalDateTime.parse(casOdletuString);
                    } catch (DateTimeParseException ex) {
                        casDatumOdletu = null;
                    }
                }

                LietadloEntity built = new LietadloEntity.Builder()
                        .sMedzinarodnymKodom(medzinarodnyKod)
                        .sNazvomVyrobcu(vyrobca)
                        .sPrioritou(priorita)
                        .sMinimalnouDlzkouDrahy(minimalnaDlzkaDrahy)
                        .sCasomPriletu(casDatumPriletu)
                        .sCasomPoziadavky(casDatumPoziadavky)
                        .sCasomOdletu(casDatumOdletu)
                        .build();

                vsetkyLietadlaZoSuboru.add(built);
            }
        } catch (FileNotFoundException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o všetkých lietadlách v systéme");
        } catch (IOException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o všetkých lietadlách v systéme");
        }
        this.aircraftManagementService.nastavVsetkyLietadlaVSystemeZoSuboru(vsetkyLietadlaZoSuboru);
    }

    /**
     * Uloží všetky idle lietadlá do súboru
     */
    public void ulozIdleLietadla() {
        List<LietadloEntity> idleLietadla =
                this.aircraftManagementService.getAllIdleLietadlaPreUlozenieDoSuboru();
        if (idleLietadla == null || idleLietadla.size() == 0) {
            return;
        }

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(IDLE_LIETADLA_FILE));

            for (LietadloEntity idleLietadlo : idleLietadla) {

                bw.write(idleLietadlo.getMedzinarodnyKod());
                bw.newLine();
            }

        } catch (IOException ioEx) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na zápis lietadiel na letisku");
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void nacitajIdleLietadlaZoSuboru() {
        List<String> idleLietadlaZoSuboru = new LinkedList<>();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(IDLE_LIETADLA_FILE));

            String line = null;
            while ((line = br.readLine()) != null) {

                String medzinarodnyKod = line;

                idleLietadlaZoSuboru.add(medzinarodnyKod);
            }
        } catch (FileNotFoundException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o všetkých lietadlách na letisku");
        } catch (IOException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o všetkých lietadlách na letisku");
        }
        this.aircraftManagementService.nastavIdleLietadlaZoSuboru(idleLietadlaZoSuboru);
    }

    public void ulozLietadlaSPridelenouDrahou() {

        List<LietadloEntity> lietadlaSPridelenouDrahou = this.aircraftManagementService.getAllLietadlaSPridelenouOdletovouDrahou();
        if (lietadlaSPridelenouDrahou == null || lietadlaSPridelenouDrahou.size() == 0) {
            return;
        }

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(S_PRIDELENOU_DRAHOU_FILE));

            for (LietadloEntity entity : lietadlaSPridelenouDrahou) {

                StringBuilder sb = new StringBuilder();
                sb.append(entity.getMedzinarodnyKod() + ",");
                sb.append(entity.getAktualnePridelenaDraha().getId());

                bw.write(sb.toString());
                bw.newLine();
            }

        } catch (IOException ioEx) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na zápis lietadiel s pridelenou dráhou");
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void nacitajLietadlaSPridelenouDrahou() {
        List<Pair<String, Integer>> lietadlaSOdletovouDrahou = new LinkedList<>();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(S_PRIDELENOU_DRAHOU_FILE));

            String line = null;
            while ((line = br.readLine()) != null) {

                String[] splitted = line.split(",");

                String medzinarodnyKod = splitted[0];
                Integer idDrahy = Integer.parseInt(splitted[1]);

                lietadlaSOdletovouDrahou.add(new Pair<>(medzinarodnyKod, idDrahy));
            }
        } catch (FileNotFoundException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o všetkých lietadlách s pridelenou dráhou");
        } catch (IOException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o všetkých lietadlách s pridelenou dráhou");
        }
        this.aircraftManagementService.nastavLietadlaSPridelenouDrahou(lietadlaSOdletovouDrahou);
    }

    public void ulozLietadlaCakajuceNaPridelenieDrahy() {
        List<Pair<Integer, List<LietadloEntity>>> lietadlaPodlaDrah =
                this.aircraftManagementService.getVsetkyLietadlaCakajuceNaPrideleniOdletovejDrahyPreUlozenieDoSuboru();

        if (lietadlaPodlaDrah == null || lietadlaPodlaDrah.size() == 0) {
            return;
        }

        BufferedWriter bw = null;
        try {
            bw =  new BufferedWriter(new FileWriter(CAKAJUCE_NA_PRIDELENIE_FILE));

            for (Pair<Integer, List<LietadloEntity>> pair : lietadlaPodlaDrah) {
                for (LietadloEntity lietadloEntity : pair.getValue()) {

                    StringBuilder sb = new StringBuilder();

                    sb.append(pair.getKey() + ",");
                    sb.append(lietadloEntity.getMedzinarodnyKod());

                    bw.write(sb.toString());
                    bw.newLine();

                }
            }

        } catch (IOException ioEx) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na zápis lietadiel čakajúcich na pridelenie dráhy");
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void nacitajLietadlaCakajuceNaPridelenieDrahy() {

        List<Pair<Integer, String>> toAdd = new LinkedList<>();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(CAKAJUCE_NA_PRIDELENIE_FILE));

            String line = null;
            while ((line = br.readLine()) != null) {

                String[] splitted = line.split(",");

                Integer dlzkaDrahy = Integer.parseInt(splitted[0]);
                String medzinarodnyKod = splitted[1];
                toAdd.add(new Pair<>(dlzkaDrahy, medzinarodnyKod));
            }


        } catch (FileNotFoundException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o všetkých lietadlách s pridelenou dráhou");
        } catch (IOException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o všetkých lietadlách s pridelenou dráhou");
        }
        this.aircraftManagementService.nastavLietadlaCakajuceNaPridenieOdletovejDrahyZoSuboru(toAdd);
    }
}
