﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FriBettingDatabaseApp.Bo;
using FriBettingDatabaseApp.Context;
using FriBettingDatabaseApp.Repository.Base;
using FriBettingWebApplication.Dto.Request;
using FriBettingWebApplication.Dto.Response;
using FriBettingWebApplication.Services.Face;

namespace FriBettingWebApplication.Services
{
    public class MatchService: IMatchService
    {
        private readonly IMatchRepository _matchRepository;
        private readonly IMatchPartRepository _matchPartRepository;
        private readonly IOddRepository _oddRepository;
        private readonly Random rnd = new Random();

        public MatchService(IMatchRepository matchRepository, IMatchPartRepository matchPartRepository, IOddRepository oddRepository)
        {
            _matchRepository = matchRepository;
            _matchPartRepository = matchPartRepository;
            _oddRepository = oddRepository;
        }

        /// <summary>
        /// Vytvorí nový zápas, ktorému vytvorí aj časti zápasu a
        /// vygeneruje nejaké random odds ku všetkým typov stávok
        /// ktoré je možné vytvoriť na daný zápas
        /// </summary>
        /// <param name="createNewMatchRequest"></param>
        /// <returns></returns>
        public OperationResponse CreateNewMatch(CreateNewMatchRequest createNewMatchRequest)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                var selectedSport = dbContext.Sports.Include("OddTypes").SingleOrDefault(sport => sport.SportId.Equals(createNewMatchRequest.SportId));
                var selectedHost = dbContext.Competitors.SingleOrDefault(competitor => competitor.CompetitorId.Equals(createNewMatchRequest.HostId));
                var selectedGuest = dbContext.Competitors.SingleOrDefault(competitor => competitor.CompetitorId.Equals(createNewMatchRequest.GuestId));

                if (selectedSport == null)
                {
                    return new OperationResponse
                    {
                        Successful = false,
                        ErrorMessage = "Sport with ID " + createNewMatchRequest.SportId + " does not exist"
                    };
                }

                if (selectedHost == null)
                {
                    return new OperationResponse
                    {
                        Successful = false,
                        ErrorMessage = "Competitor with ID " + createNewMatchRequest.HostId + " does not exist"
                    };
                }

                if (selectedGuest == null)
                {
                    return new OperationResponse
                    {
                        Successful = false,
                        ErrorMessage = "Competitor with ID " + createNewMatchRequest.HostId + " does not exist"
                    };
                }

                Match match = new Match
                {
                    Sport = selectedSport,
                    HostCompetitor = selectedHost,
                    GuestCompetitor = selectedGuest,
                    IsCancelled = false,
                    IsEvaluated = false,
                    MatchStartDateTime = createNewMatchRequest.DateTimeOfMatch,
                    TotalGoalsGuest = 0,
                    TotalGoalsHost = 0
                };

                List<MatchPart> mps = new List<MatchPart>();
                for (int i = 0; i < selectedSport.DefaultNumberOfMatchParts; i++)
                {
                    mps.Add(new MatchPart
                    {
                        Match = match,
                        GoalsGuest = 0,
                        GoalsHost = 0,
                        MatchPartCounter = i
                    });
                }
                match.MatchParts = mps;

                _matchRepository.SaveToContext(match, dbContext);
                foreach (var matchPart in mps)
                {
                    _matchPartRepository.SaveToContext(matchPart, dbContext);
                }

                foreach (var oddType in selectedSport.OddTypes)
                {
                    Odd odd = new Odd
                    {
                        OddType = oddType,
                        OddValue = rnd.NextDouble() + 1, // Generuje kurzy od 1 po 2
                        Match = match,
                        CreatedAt = DateTime.Now,
                        IsEvaluated = false,
                        IsWinning = false
                    };
                    _oddRepository.SaveToContext(odd, dbContext);
                }

                dbContext.SaveChanges();

                return new OperationResponse
                {
                    Successful = true
                };
            }
        }

        public List<MatchResponse> GetMatchesFiltered(GetMatchesFilteredRequest getMatchesFilteredRequest, out int count)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                var filterQuery = dbContext.Matches.Include("HostCompetitor").Include("GuestCompetitor").Include("Sport")
                    .Select(e => e);

                filterQuery = filterQuery.OrderBy(e => e.MatchStartDateTime);

                if (getMatchesFilteredRequest.FromDate != null)
                {
                    filterQuery = filterQuery.Where(e => e.MatchStartDateTime > getMatchesFilteredRequest.FromDate);
                }

                if (getMatchesFilteredRequest.ToDate != null)
                {
                    filterQuery = filterQuery.Where(e => e.MatchStartDateTime < getMatchesFilteredRequest.ToDate);
                }

                count = filterQuery.Count();

                if (getMatchesFilteredRequest.Page != null)
                {
                    filterQuery = filterQuery.Skip(getMatchesFilteredRequest.Page.Value * (getMatchesFilteredRequest.Size ?? 10));
                }

                filterQuery = filterQuery.Take(getMatchesFilteredRequest.Size ?? 10);

                // Vracia iba nevyhodnotené zápasy
                filterQuery = filterQuery.Where(e => !e.IsEvaluated);

                var matches = filterQuery.ToList();
                return matches.Select(e => new MatchResponse
                {
                    MatchId = e.MatchId,
                    SportId = e.Sport.SportId,
                    HostId =  e.HostCompetitor.CompetitorId,
                    GuestId = e.GuestCompetitor.CompetitorId,
                    HostName = e.HostCompetitor.Name,
                    GuestName = e.GuestCompetitor.Name,
                    DateTimeOfMatch = e.MatchStartDateTime
                }).ToList();
            }
        }

        public List<OddWithOddTypeResponse> GetOddsForMatch(int match)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                return dbContext.Odds.Include("Match").Include("OddType")
                    .Select(e => e).Where(e => e.Match.MatchId == match)
                    .Select(e =>
                    new OddWithOddTypeResponse
                    {
                        OddTypeId = e.OddType.OddTypeId,
                        OddId = e.OddId,
                        OddTypeCode = e.OddType.Code,
                        OddValue = e.OddValue
                    }).ToList();
            }
        }

        public List<MatchPartResponse> GetMatchPartsForMatch(int match)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                return dbContext.Matches
                    .Include("MatchParts")
                    .SingleOrDefault(e => e.MatchId == match)
                    ?.MatchParts.Select(e => new MatchPartResponse
                    {
                        MatchPartId = e.MatchPartId
                    }).ToList();
            }
        }

        /// <summary>
        /// Metóda pre vyhodnotenie zápasu z predaných parametrov,
        /// z biznis stránky vykoná
        /// - Zapísanie stavov jednotlivých častí zápasov
        /// - Vyhodnotenie tohto zápasu na uzavretých tiketoch
        /// - Vymazanie tohto zápasu z tiketov, ktoré nie sú uzavreté
        /// 
        /// </summary>
        /// <param name="matchEvaluationResultRequest"></param>
        /// <returns></returns>
        public OperationResponse EvaluateMatch(MatchEvaluationResultRequest matchEvaluationResultRequest)
        {

            using (var dbContext = new BettingAppDbContext())
            {
                Match match = dbContext.Matches
                    .Include("MatchParts")
                    .SingleOrDefault(e => e.MatchId == matchEvaluationResultRequest.MatchId);
                if (match == null)
                {
                    return new OperationResponse
                    {
                        Successful = false,
                        ErrorMessage = "Match with ID " + matchEvaluationResultRequest.MatchId + " does not exist"
                    };
                }

                if (match.IsEvaluated)
                {
                    return new OperationResponse
                    {
                        Successful = false,
                        ErrorMessage = "Match with ID " + matchEvaluationResultRequest.MatchId + " is already evaluated"
                    };
                }

                if (matchEvaluationResultRequest.PartsEvaluation == null ||
                    matchEvaluationResultRequest.PartsEvaluation.Count == 0)
                {
                    return new OperationResponse
                    {
                        Successful = false,
                        ErrorMessage = "Missing evaluation part for match parts for match ID " +
                                       matchEvaluationResultRequest.MatchId
                    };
                }

                foreach (var matchPartEvaluation in matchEvaluationResultRequest.PartsEvaluation)
                {
                    if (matchPartEvaluation.MatchPartId == null)
                    {
                        return new OperationResponse
                        {
                            Successful = false,
                            ErrorMessage = "Match part ID can't be null"
                        };
                    }

                    if (!match.MatchParts.Select(e => e.MatchPartId).ToList()
                        .Contains(matchPartEvaluation.MatchPartId.Value))
                    {
                        return new OperationResponse
                        {
                            Successful = false,
                            ErrorMessage = "Match part with ID " + matchPartEvaluation.MatchPartId +
                                           " does not belong to match with ID " + match.MatchId
                        };
                    }

                    if (matchPartEvaluation.HostGoals == null || matchPartEvaluation.GuestGoals == null)
                    {
                        return new OperationResponse
                        {
                            Successful = false,
                            ErrorMessage =
                                "Goals are required parameter - if there were zero goals in match part insert 0"
                        };
                    }

                    if (matchPartEvaluation.HostGoals < 0 || matchPartEvaluation.GuestGoals < 0)
                    {
                        return new OperationResponse
                        {
                            Successful = false,
                            ErrorMessage =
                                "Goals in match part" + matchPartEvaluation.MatchPartId +  " must not have negative value"
                        };
                    }
                }

                var totalGoalsHost = 0;
                var totalGoalGuest = 0;
                foreach (var matchPartEvaluation in matchEvaluationResultRequest.PartsEvaluation)
                {
                    var mp = match.MatchParts.SingleOrDefault(m => m.MatchPartId == matchPartEvaluation.MatchPartId);
                    mp.GoalsHost = matchPartEvaluation.HostGoals.Value;
                    totalGoalsHost += mp.GoalsHost;
                    mp.GoalsGuest = matchPartEvaluation.GuestGoals.Value;
                    totalGoalGuest += mp.GoalsGuest;
                }

                match.TotalGoalsHost = totalGoalsHost;
                match.TotalGoalsGuest = totalGoalGuest;

                EvaluateMatchInternally(dbContext, match, matchEvaluationResultRequest);

                match.IsEvaluated = true;
                dbContext.SaveChanges();
                return new OperationResponse
                {
                    Successful = true
                };
            }
        }


        /// <summary>
        /// Evaluácia zápasu, ktorá spraví z biz. pohľadu naslednovné
        /// 1. Zo všetkých tiketov, ktoré sú len "otvorené" zmaže stávky na tento zápas
        /// 2. Vyhodnotí stávky pre tento zápas na "uzavretých" tiketoch (ak to bola posledná nevyhodnotená stávka, vyhodnotí celý tiket)
        /// </summary>
        private void EvaluateMatchInternally(BettingAppDbContext openedContext, Match match, MatchEvaluationResultRequest matchPartsEvaluation)
        {
            RemoveBetsFromNotClosedTickets(openedContext, match);
            EvaluateOddsForClosedTikets(openedContext, match, matchPartsEvaluation);
        }

        /// <summary>
        /// Odoberie stávky z tiketov, ktoré neboli uzavreté
        /// </summary>
        /// <param name="openedContext"></param>
        /// <param name="match"></param>
        private void RemoveBetsFromNotClosedTickets(BettingAppDbContext openedContext, Match match)
        {
            var ticketBetsForThisMatch =
                openedContext.TicketMatchBets
                    .Include("Odd")
                    .Include("Odd.Match")
                    .Include("Ticket")
                    .Where(e => e.Odd.Match.MatchId == match.MatchId && !e.Ticket.IsClosed).ToList();
            foreach (var ticketBet in ticketBetsForThisMatch)
            {
                openedContext.TicketMatchBets.Remove(ticketBet);
            }
        }

        /// <summary>
        /// Vyhodnotí stávky na tiketoch, ktoré boli uzavreté
        /// </summary>
        /// <param name="openedContext"></param>
        /// <param name="match"></param>
        /// <param name="matchPartsEvaluation"></param>
        private void EvaluateOddsForClosedTikets(BettingAppDbContext openedContext, Match match, MatchEvaluationResultRequest matchPartsEvaluation)
        {

            var oddsForThisMatch =
                openedContext.Odds
                    .Include("Match")
                    .Include("OddType")
                    .Where(e => e.Match.MatchId == match.MatchId);

            foreach (var odd in oddsForThisMatch)
            {
                var betResult = odd.OddType.Result;
                var matchPartNumber = odd.OddType.MatchPartNumber;

                // Stávky na výsledok celého zápasu
                if (matchPartNumber == 0)
                {
                    OddType.WinnerResult matchResult;
                    if (match.TotalGoalsHost > match.TotalGoalsGuest)
                    {
                        matchResult = OddType.WinnerResult.HOST_WINNER;
                    } 
                    else if (match.TotalGoalsGuest > match.TotalGoalsHost)
                    {
                        matchResult = OddType.WinnerResult.GUEST_WINNER;
                    }
                    else
                    {
                        matchResult = OddType.WinnerResult.DRAW;
                    }

                    if (betResult == matchResult)
                    {
                        odd.IsWinning = true;
                        odd.IsEvaluated = true;
                    }
                    else
                    {
                        odd.IsWinning = false;
                        odd.IsEvaluated = true;
                    }
                }
                // Stávky na polčasy/tretiny/..
                else
                {
                    var matchPart = match.MatchParts.SingleOrDefault(e => e.MatchPartCounter == odd.OddType.MatchPartNumber - 1);
                    OddType.WinnerResult matchPartResult;
                    if (matchPart.GoalsHost > matchPart.GoalsGuest)
                    {
                        matchPartResult = OddType.WinnerResult.HOST_WINNER;
                    }
                    else if (matchPart.GoalsGuest > matchPart.GoalsHost)
                    {
                        matchPartResult = OddType.WinnerResult.GUEST_WINNER;
                    }
                    else
                    {
                        matchPartResult = OddType.WinnerResult.DRAW;
                    }

                    if (betResult == matchPartResult)
                    {
                        odd.IsWinning = true;
                        odd.IsEvaluated = true;
                    }
                    else
                    {
                        odd.IsWinning = false;
                        odd.IsEvaluated = true;
                    }
                }
            }

            var ticketsWithThisMatch = openedContext.TicketMatchBets
                .Include("Ticket")
                .Include("Odd")
                .Include("BettingUser")
                .Where(e => oddsForThisMatch.Contains(e.Odd))
                .Select(e => e.Ticket)
                .Include("TicketBets")
                .Include("TicketBets.BettingUser")
                .Include("TicketBets.Odd")
                .ToList();

            foreach (var ticket in ticketsWithThisMatch)
            {
                // Ak sú vyhodnotené už všetky stávky na tomto tikete
                if (ticket.TicketBets.Count(e => !e.Odd.IsEvaluated) == 0)
                {
                    // Ak tiket obsahuje niektorú prehratú stávku
                    if (ticket.TicketBets.Count(e => !e.Odd.IsWinning) != 0)
                    {
                        ticket.IsWinning = false;
                        ticket.IsEvaluated = true;
                    }
                    // Ak tiket obsahuje iba "výherné" stávky
                    else
                    {
                        var oddsTotalMultiplication =
                            ticket.TicketBets.Select(e => e.Odd.OddValue).Aggregate(1.0, (x, y) => x * y);
                        ticket.BettingUser.Balance += oddsTotalMultiplication * ticket.BetValue;
                        ticket.IsWinning = true;
                        ticket.IsEvaluated = true;
                    }
                }
            }

        }
    }
}
