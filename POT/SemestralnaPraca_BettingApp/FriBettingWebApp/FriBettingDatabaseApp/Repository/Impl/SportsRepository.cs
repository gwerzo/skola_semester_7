﻿using System.Collections.Generic;
using System.Linq;
using FriBettingDatabaseApp.Bo;
using FriBettingDatabaseApp.Context;
using FriBettingDatabaseApp.Repository.Base;

namespace FriBettingDatabaseApp.Repository.Impl
{
    public class SportsRepository: Repository<Sport>, ISportRepository
    {
        public IEnumerable<OddType> GetAllOddTypesForSport(int sportId)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                return dbContext.Sports.Include("OddTypes").SingleOrDefault(e => e.SportId == sportId)?.OddTypes;
            }
        }
    }
}
