package sk.uniza.fri.lustyno.fripd.utils;

public interface Validable {

    public void validate();

}
