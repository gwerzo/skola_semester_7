package sk.uniza.fri.lustiak.auds2.kataster.components.kataster;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.FindRealEstateUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.RealEstateUiTableEntity;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;
import sk.uniza.fri.lustiak.auds2.kataster.utils.StringToIntegerConverter;


public class VyhladanieNehnutelnostiPodlaIDForm extends BusinessComponent {

    private Binder<FindRealEstateUi> binder;
    private FindRealEstateUi entity;

    private RealEstateService realEstateService;

    private RealEstateInfoForm realEstateInfoForm;
    private VerticalLayout mainLayout;


    public VyhladanieNehnutelnostiPodlaIDForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_1.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.binder = new Binder<>();
        this.entity = new FindRealEstateUi();

        this.mainLayout = new VerticalLayout();

        TextField idTF = new TextField("Identifikačné číslo");

        Button findRealEstatesButton = new Button("Vyhľadať nehnuteľnosti");
        findRealEstatesButton.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                najdiNehnutelnost(this.entity);
            } catch (ValidationException valEx) {
                this.entity.flush();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        this.binder.forField(idTF)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(FindRealEstateUi::getIdentifikacneCislo, FindRealEstateUi::setIdentifikacneCislo);

        mainLayout.addComponent(new FormLayout(idTF));
        mainLayout.addComponent(findRealEstatesButton);

        this.parentComponent.showComponent(new Panel("Vyhľadanie NEHNUTEĽNOSTI podľa ID", mainLayout));
    }

    private void najdiNehnutelnost(FindRealEstateUi idNehnutelnosti) {

        HandledOperationExecutor.submit(() -> {
            RealEstateUiTableEntity en = this.realEstateService.findRealEstate(idNehnutelnosti);

            if (this.realEstateInfoForm == null) {
                this.realEstateInfoForm = new RealEstateInfoForm(en, Boolean.FALSE);
                this.mainLayout.addComponent(this.realEstateInfoForm);
            } else {
                this.realEstateInfoForm.initFrom(en);
            }
            return OperationMarker.OPERATION_SUCCESFUL;
        });
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_1.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }


}
