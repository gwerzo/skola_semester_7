package sk.uniza.fri.lustyno.fripd.entities_ui.lincense;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class VodicskyPreukazDetailEntityUi {

    private String menoVodica;
    private String priezviskoVodica;
    private String evcVodica;
    private String datumUkonceniaPlatnostiPreukazu;
    private String zakazViestVozidlo;
    private String pocetDopravnychPriestupkov;

}
