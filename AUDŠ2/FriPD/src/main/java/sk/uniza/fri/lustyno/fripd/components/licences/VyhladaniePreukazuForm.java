package sk.uniza.fri.lustyno.fripd.components.licences;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.fripd.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.fripd.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.fripd.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.fripd.components.vehicles.common.DrivingLicenseDetailForm;
import sk.uniza.fri.lustyno.fripd.components.vehicles.common.VehicleDetailForm;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.VodicskyPreukazDetailEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.VyhladanieVodicskehoPreukazuEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.VozidloEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.VyhladanieVozidlaEntityUi;
import sk.uniza.fri.lustyno.fripd.service.DrivingLicenseService;
import sk.uniza.fri.lustyno.fripd.service.UIServices;
import sk.uniza.fri.lustyno.fripd.utils.CustomNotification;
import sk.uniza.fri.lustyno.fripd.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.fripd.utils.OperationMarker;
import sk.uniza.fri.lustyno.fripd.utils.converters.StringToIntegerConverter;

public class VyhladaniePreukazuForm extends BusinessComponent {

    private DrivingLicenseService drivingLicenseService;

    private Binder<VyhladanieVodicskehoPreukazuEntityUi> binder;
    private VyhladanieVodicskehoPreukazuEntityUi entity;

    private VerticalLayout drivingLicenseDetail;

    public VyhladaniePreukazuForm(RepaintableComponent parentComponent) {
        super(parentComponent, MENU_TITLE.CARDS_MENU_2.getTitle());
        this.drivingLicenseService = UIServices.getDrivingLicenseService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        this.drivingLicenseDetail = new VerticalLayout();
        this.binder = new Binder<>(VyhladanieVodicskehoPreukazuEntityUi.class);
        this.entity = new VyhladanieVodicskehoPreukazuEntityUi();

        FormLayout layout = new FormLayout();

        TextField vinCodeTf = new TextField("EVČ kód");
        Button vyhladaj = new Button("Vyhľadaj");

        layout.addComponents(vinCodeTf, vyhladaj, this.drivingLicenseDetail);

        vyhladaj.addClickListener(this::buttonClick);

        binder.forField(vinCodeTf)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(VyhladanieVodicskehoPreukazuEntityUi::getEvcPreukazu, VyhladanieVodicskehoPreukazuEntityUi::setEvcPreukazu);

        this.parenComponent.showComponent(new Panel("Vyhľadanie vodičského preukazu podľa EVČ kódu", new VerticalLayout(layout)));
    }

    @Override
    public String getComponentTitle() {
        return  MENU_TITLE.CARDS_MENU_2.getTitle();
    }

    private void vyhladaj() {

        HandledOperationExecutor.submit(() -> {
            VodicskyPreukazDetailEntityUi entityUi = this.drivingLicenseService.findLicenseByEvcCode(this.entity);
            zobrazVodicskyPreukaz(entityUi);
            return OperationMarker.OPERATION_SUCCESFUL;
        });

    }

    private void zobrazVodicskyPreukaz(VodicskyPreukazDetailEntityUi entityUi) {
        this.drivingLicenseDetail.removeAllComponents();
        this.drivingLicenseDetail.addComponent(new Panel("Detail vodičského preukazu",
                new VerticalLayout(
                        new DrivingLicenseDetailForm(entityUi))));
    }

    private void buttonClick(Button.ClickEvent e) {
        try {
            this.binder.writeBean(this.entity);
            vyhladaj();
        } catch (ValidationException ex) {
            ex.printStackTrace();
            CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
        }
    }
}
