package sk.uniza.fri.lustiak.auds2.kataster.components.common;

import sk.uniza.fri.lustiak.auds2.kataster.components.system.ExportSystemuDoSuboruForm;
import sk.uniza.fri.lustiak.auds2.kataster.components.system.GeneratorSystemuForm;
import sk.uniza.fri.lustiak.auds2.kataster.components.system.ImportSystemuZoSuboruForm;
import sk.uniza.fri.lustiak.auds2.kataster.components.system.VypisVsetkehoForm;

public class SystemMenuComponent extends AbstractMenu {

    public SystemMenuComponent(RepaintableComponent component) {
        this.menuItems.add(new GeneratorSystemuForm(component));
        this.menuItems.add(new ExportSystemuDoSuboruForm(component));
        this.menuItems.add(new ImportSystemuZoSuboruForm(component));
        this.menuItems.add(new VypisVsetkehoForm(component));
        initMenuItems();
    }

}
