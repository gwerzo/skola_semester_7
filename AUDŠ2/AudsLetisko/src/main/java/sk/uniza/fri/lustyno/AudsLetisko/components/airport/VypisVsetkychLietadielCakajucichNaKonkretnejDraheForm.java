package sk.uniza.fri.lustyno.AudsLetisko.components.airport;

import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;

public class VypisVsetkychLietadielCakajucichNaKonkretnejDraheForm extends BusinessComponent {

    public VypisVsetkychLietadielCakajucichNaKonkretnejDraheForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_7.getTitle());
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        VerticalLayout layout = new VerticalLayout();



        this.parenComponent.showComponent(layout);
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_7.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return false;
    }
}
