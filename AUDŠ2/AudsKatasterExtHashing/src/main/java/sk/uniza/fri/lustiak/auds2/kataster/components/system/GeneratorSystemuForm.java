package sk.uniza.fri.lustiak.auds2.kataster.components.system;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.GenerovanieSystemuUi;
import sk.uniza.fri.lustiak.auds2.kataster.service.ApplicationInitializer;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;
import sk.uniza.fri.lustiak.auds2.kataster.utils.StringToIntegerConverter;

public class GeneratorSystemuForm extends BusinessComponent {

    private ApplicationInitializer initializer;

    private Binder<GenerovanieSystemuUi> binder;
    private GenerovanieSystemuUi entity;

    public GeneratorSystemuForm(RepaintableComponent parentComponent) {
        super(parentComponent, MENU_TITLE.SYSTEM_MENU_1.getTitle());
        this.initializer = UIServices.getInitializer();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        this.entity = new GenerovanieSystemuUi();
        this.binder = new Binder<>(GenerovanieSystemuUi.class);

        VerticalLayout vl = new VerticalLayout();

        FormLayout formLayout = new FormLayout();

        TextField pocetNehnutelnosti = new TextField("Počet nehnuteľností");


        this.binder.forField(pocetNehnutelnosti)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .withValidator(i -> i > 0, "Počet musí byť viac ako 0")
                .bind(GenerovanieSystemuUi::getPocetNehnutelnosti, GenerovanieSystemuUi::setPocetNehnutelnosti);

        Button vygenerujSystemButton = new Button("Inicializuj");
        vygenerujSystemButton.setStyleName("friendly");
        vygenerujSystemButton.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                HandledOperationExecutor.submit(() -> {
                    OperationMarker operationMarker =
                            this.initializer.initData(this.entity.getPocetNehnutelnosti());
                    if (operationMarker.equals(OperationMarker.OPERATION_SUCCESFUL)) {
                        this.parentComponent.updateFileState();
                        CustomNotification.notification("Dáta boli úspešne inicializované");
                        return operationMarker;
                    }
                    return OperationMarker.OPERATION_UNSUCCESFUL;
                });
            } catch (ValidationException valEx) {
                this.entity.flush();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        formLayout.addComponent(pocetNehnutelnosti);

        vl.addComponent(formLayout);
        vl.addComponent(vygenerujSystemButton);

        this.parentComponent.showComponent(new Panel("Inicializátor systému", vl));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_1.getTitle();
    }
}
