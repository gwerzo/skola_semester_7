package sk.uniza.fri.lustyno.datastructures.extendiblehashing.util;

import sk.uniza.fri.lustyno.datastructures.extendiblehashing.EHOverflowBlock;
import sk.uniza.fri.lustyno.datastructures.extendiblehashing.entities.EHEntity;

public class IndexToOverflowBlock<T extends EHEntity<T>> {

    private Integer indexOfBlock;
    private EHOverflowBlock<T> overflowBlock;

    public IndexToOverflowBlock(Integer indexOfBlock, EHOverflowBlock<T> overflowBlock) {
        this.indexOfBlock = indexOfBlock;
        this.overflowBlock = overflowBlock;
    }


    public Integer getIndexOfBlock() {
        return indexOfBlock;
    }

    public EHOverflowBlock<T> getOverflowBlock() {
        return overflowBlock;
    }
}
