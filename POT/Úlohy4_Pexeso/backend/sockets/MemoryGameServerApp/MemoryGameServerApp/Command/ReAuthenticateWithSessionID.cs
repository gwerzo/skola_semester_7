﻿using MemoryGameServerApp.Server;
using SuperWebSocket;

namespace MemoryGameServerApp.Command
{
    public class ReAuthenticateWithSessionID: ICommand
    {

        public const string COMMAND = "-authsession";
        private const string REAUTH_SUCC = "-reauthsucc";
        private const string REAUTH_UNSUCC = "-reauthunsucc";

        public string SocketSessionUuid { get; set; }

        public ReAuthenticateWithSessionID(string socketSessionUuid)
        {
            this.SocketSessionUuid = socketSessionUuid;
        }

        public void executeCommand(string usernameOfCommandCaller, MemoryGameServer memoryGameServer,
            WebSocketSession callerSocketSession)
        {
            if (memoryGameServer.ReAuthenticateUserWithSocketSessionId(this.SocketSessionUuid, callerSocketSession))
            {
                callerSocketSession.Send(REAUTH_SUCC);
            }
            else
            {
                callerSocketSession.Send(REAUTH_UNSUCC);
            }
        }
    }
}
