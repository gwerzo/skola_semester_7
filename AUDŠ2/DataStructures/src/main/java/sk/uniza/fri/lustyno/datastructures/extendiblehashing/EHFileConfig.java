package sk.uniza.fri.lustyno.datastructures.extendiblehashing;

import java.io.*;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class EHFileConfig {

    String databaseName;
    Integer D;
    Integer numberOfAddressingBits;

    Integer numberOfEntitiesInMainBlock;
    Integer numberOfEntitiesInOverflowBlock;

    Integer numberOfUsedMainBlocks;
    Integer numberOfUsedOverflowBlocks;

    LinkedList<Integer> freeBlocks;
    LinkedList<Integer> freeOverflowBlocks;

    EHDirectoryBox[] directoryLines;

    public  EHFileConfig() {

    }

    public EHFileConfig(String databaseName,
                        Integer D, Integer numberOfAddressingBits,
                        Integer numberOfEntitiesInMainBlock, Integer numberOfEntitiesInOverflowBlock,
                        Integer numberOfUsedMainBlocks, Integer numberOfUsedOverflowBlocks,
                        LinkedList<Integer> freeBlocks, LinkedList<Integer> freeOverflowBlocks,
                        EHDirectoryBox[] directoryLines) {
        this.databaseName = databaseName;
        this.D = D;
        this.numberOfAddressingBits = numberOfAddressingBits;
        this.numberOfEntitiesInMainBlock = numberOfEntitiesInMainBlock;
        this.numberOfEntitiesInOverflowBlock = numberOfEntitiesInOverflowBlock;
        this.numberOfUsedMainBlocks = numberOfUsedMainBlocks;
        this.numberOfUsedOverflowBlocks = numberOfUsedOverflowBlocks;
        this.freeBlocks = freeBlocks;
        this.freeOverflowBlocks = freeOverflowBlocks;
        this.directoryLines = directoryLines;
    }


    public static EHFileConfig fromFile(String dbName) {
        EHFileConfig fc = new EHFileConfig();

        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(dbName));

            fc.D = new Integer(br.readLine());
            fc.numberOfAddressingBits = new Integer(br.readLine());
            fc.numberOfEntitiesInMainBlock = new Integer(br.readLine());
            fc.numberOfEntitiesInOverflowBlock = new Integer(br.readLine());
            fc.numberOfUsedMainBlocks = new Integer(br.readLine());
            fc.numberOfUsedOverflowBlocks = new Integer(br.readLine());
            fc.databaseName = br.readLine();

            LinkedList<Integer> freeBlocks = new LinkedList<>();
            String freeBlocksLine = br.readLine();
            String[] splitBlocks = freeBlocksLine.split(",");
            for (int i = 0; i < splitBlocks.length; i++) {
                if (splitBlocks[i].equals("")) {
                    break;
                }
                freeBlocks.add(new Integer(splitBlocks[i]));
            }
            fc.freeBlocks = freeBlocks;

            LinkedList<Integer> freeOverflowBlocks = new LinkedList<>();
            String freeOverflowBlocksLine = br.readLine();
            String[] splitOverflowBlocks = freeOverflowBlocksLine.split(",");
            for (int i = 0; i < splitOverflowBlocks.length; i++) {
                if (splitOverflowBlocks[i].equals("")) {
                    break;
                }
                freeOverflowBlocks.add(new Integer(splitOverflowBlocks[i]));
            }
            fc.freeOverflowBlocks = freeOverflowBlocks;

            EHDirectoryBox[] directoryValues = new EHDirectoryBox[(int)Math.pow(2, fc.D)];
            for (int i = 0; i < directoryValues.length; i++) {
                String line = br.readLine();
                String[] lineSplit = line.split(",");
                EHDirectoryBox box = new EHDirectoryBox();
                box.indexInMainFile = new Integer(lineSplit[0]);
                box.localBlockLevel = new Integer(lineSplit[1]);
                box.numberOfEntitiesInMainBlock = new Long(lineSplit[2]);
                box.numberOfEntitiesInOverflowBlock = new Long(lineSplit[3]);
                box.numberOfOverflowBlocks = new Long(lineSplit[4]);

                directoryValues[i] = box;
            }
            fc.directoryLines = directoryValues;

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fc;
    }

    public void saveToFile(String fileName) {
        BufferedWriter bw;
        try {
            bw = new BufferedWriter(new FileWriter(fileName, false));
            bw.write(this.D.toString());
            bw.newLine();
            bw.write(this.numberOfAddressingBits.toString());
            bw.newLine();
            bw.write(this.numberOfEntitiesInMainBlock.toString());
            bw.newLine();
            bw.write(this.numberOfEntitiesInOverflowBlock.toString());
            bw.newLine();
            bw.write(this.numberOfUsedMainBlocks.toString());
            bw.newLine();
            bw.write(this.numberOfUsedOverflowBlocks.toString());
            bw.newLine();
            bw.write(databaseName);
            bw.newLine();

            // Volné bloky zapíše na jeden riadok oddelené čiarkou
            bw.write(this.freeBlocks.stream().map(e -> "" + e).collect(Collectors.joining(",")));
            bw.newLine();
            // Voľné overflow bloky tiež zapíše na jeden riadok oddelené čiarkou
            bw.write(this.freeOverflowBlocks.stream().map(e -> "" + e).collect(Collectors.joining(",")));
            bw.newLine();

            // Zapíše directory, každý index na nový riadok (riadkov je 2^D)
            for (int i = 0; i < this.directoryLines.length; i++) {
                bw.write(this.directoryLines[i].indexInMainFile + ",");
                bw.write(this.directoryLines[i].localBlockLevel + ",");
                bw.write(this.directoryLines[i].numberOfEntitiesInMainBlock + ",");
                bw.write(this.directoryLines[i].numberOfEntitiesInOverflowBlock + ",");
                bw.write(this.directoryLines[i].numberOfOverflowBlocks + "");
                bw.newLine();
            }

            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
