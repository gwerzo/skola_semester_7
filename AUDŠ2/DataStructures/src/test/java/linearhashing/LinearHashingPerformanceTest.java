package linearhashing;

import linearhashing.entities.IntegerEntity;
import org.junit.jupiter.api.*;
import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.File;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.FromBytesFactory;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.SPLIT_VARIANT;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LinearHashingPerformanceTest {

    private String DATABASE_VERSION = "TEST_VERSION_" + "1";

    @Test
    @Order(1)
    public void SPLIT_VARIANTS_PERFORMANCE_TEST_LOAD_FACTOR() {


        long milis = 0;

        for (int i = 0; i < 100; i++) {

            long startTime = System.currentTimeMillis();

            File<IntegerKey, IntegerEntity> filero = new File<>(
                    DATABASE_VERSION, 10, 4,
                    IntegerEntity.class,
                    SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR,
                    new FromBytesFactory<IntegerEntity>() {
                        @Override
                        public IntegerEntity fromBytes(byte[] bytes) {
                            return IntegerEntity.fromByteArray(bytes);
                        }

                        @Override
                        public IntegerEntity emptyInvalidInstance() {
                            return new IntegerEntity(-1, -1);
                        }
                    });
            Random rnd = new Random();

            List<IntegerEntity> entities = new ArrayList<>();
            List<Integer> usedInts = new ArrayList<>();
            for (int y = 0; y < 10000; y++) {
                Integer rndInt = rnd.nextInt(100000000);
                while (usedInts.contains(rndInt)) {
                    rndInt = rnd.nextInt(100000000);
                }
                IntegerEntity en = new IntegerEntity(rndInt, rndInt);
                entities.add(en);
                usedInts.add(rndInt);
                filero.saveRecord(en);
            }

            long endTimeM = System.currentTimeMillis();

            milis += (endTimeM - startTime);

            filero.close();
        }

        System.out.println("So split variantom pre load factor priemerná dĺžka vloženia 10000 záznamov(ms) : " + (milis / 100));

    }

    @Test
    @Order(2)
    public void SPLIT_VARIANTS_PERFORMANCE_TEST_NUMBER_OF_OVERFLOW_BLOCKS() {

        long milis = 0;

        for (int i = 0; i < 100; i++) {

            long startTime = System.currentTimeMillis();

            File<IntegerKey, IntegerEntity> filero = new File<>(
                    DATABASE_VERSION, 10, 4,
                    IntegerEntity.class,
                    SPLIT_VARIANT.SPLIT_VARIANT_NUMBER_OF_OVERFLOW_BLOCKS,
                    new FromBytesFactory<IntegerEntity>() {
                        @Override
                        public IntegerEntity fromBytes(byte[] bytes) {
                            return IntegerEntity.fromByteArray(bytes);
                        }

                        @Override
                        public IntegerEntity emptyInvalidInstance() {
                            return new IntegerEntity(-1, -1);
                        }
                    });
            Random rnd = new Random();

            List<IntegerEntity> entities = new ArrayList<>();
            List<Integer> usedInts = new ArrayList<>();
            for (int y = 0; y < 10000; y++) {
                Integer rndInt = rnd.nextInt(100000000);
                while (usedInts.contains(rndInt)) {
                    rndInt = rnd.nextInt(100000000);
                }
                IntegerEntity en = new IntegerEntity(rndInt, rndInt);
                entities.add(en);
                usedInts.add(rndInt);
                filero.saveRecord(en);
            }

            long endTimeM = System.currentTimeMillis();

            milis += (endTimeM - startTime);

            filero.close();
        }

        System.out.println("So split variantom pre počet preplňujúcich blokov priemerná dĺžka vloženia 10000 záznamov(ms) : " + (milis / 100));
    }

    @Test
    @Order(3)
    public void SPLIT_VARIANTS_PERFORMANCE_TEST_RATIO() {

        long milis = 0;

        for (int i = 0; i < 100; i++) {

            long startTime = System.currentTimeMillis();

            File<IntegerKey, IntegerEntity> filero = new File<>(
                    DATABASE_VERSION, 10, 4,
                    IntegerEntity.class,
                    SPLIT_VARIANT.SPLIT_VARIANT_RATIO_PRIMARY_OVERFLOW_BLOCKS,
                    new FromBytesFactory<IntegerEntity>() {
                        @Override
                        public IntegerEntity fromBytes(byte[] bytes) {
                            return IntegerEntity.fromByteArray(bytes);
                        }

                        @Override
                        public IntegerEntity emptyInvalidInstance() {
                            return new IntegerEntity(-1, -1);
                        }
                    });
            Random rnd = new Random();

            List<IntegerEntity> entities = new ArrayList<>();
            List<Integer> usedInts = new ArrayList<>();
            for (int y = 0; y < 10000; y++) {
                Integer rndInt = rnd.nextInt(100000000);
                while (usedInts.contains(rndInt)) {
                    rndInt = rnd.nextInt(100000000);
                }
                IntegerEntity en = new IntegerEntity(rndInt, rndInt);
                entities.add(en);
                usedInts.add(rndInt);
                filero.saveRecord(en);
            }

            long endTimeM = System.currentTimeMillis();

            milis += (endTimeM - startTime);

            filero.close();
        }

        System.out.println("So split variantom pre ratio factor priemerná dĺžka vloženia 10000 záznamov(ms) : " + (milis / 100));
    }

    @BeforeEach
    public void clearFiles() {
        FileWriter fwOb = null;
        PrintWriter pwOb = null;
        try {
            fwOb = new FileWriter(this.DATABASE_VERSION + ".lldb", false);
            pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            pwOb.close();
            fwOb.close();
        } catch (IOException e) {

        }


        try {
            fwOb = new FileWriter("overflow_" + this.DATABASE_VERSION + ".lldb", false);
            pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            pwOb.close();
            fwOb.close();
        } catch (IOException e) {

        }
    }

}
