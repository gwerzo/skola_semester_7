﻿using System.Collections.Generic;
using FriBettingWebApplication.Dto.Request;
using FriBettingWebApplication.Dto.Response;

namespace FriBettingWebApplication.Services.Face
{
    public interface IMatchService
    {
        OperationResponse CreateNewMatch(CreateNewMatchRequest createNewMatchRequest);
        List<MatchResponse> GetMatchesFiltered(GetMatchesFilteredRequest getMatchesFilteredRequest, out int count);
        List<OddWithOddTypeResponse> GetOddsForMatch(int match);
        List<MatchPartResponse> GetMatchPartsForMatch(int match);
        OperationResponse EvaluateMatch(MatchEvaluationResultRequest matchEvaluationResultRequest);


    }
}
