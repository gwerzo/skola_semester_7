﻿var localStorageMemoryGameSessionIdKey = "memorygamesessionid";

if (localStorage.getItem(localStorageMemoryGameSessionIdKey) == null) {
    $("#login-menu").show();
    $("#register-menu").show();
    $("#logout-menu").hide();
    $("#index-menu").hide();
    $("#users-list-menu").hide();
    $("#open-games-menu").hide();
    $("#played-games-menu").hide();
    $("#my-invites-menu").hide();
} else {
    $("#login-menu").hide();
    $("#register-menu").hide();
    $("#users-list-menu").show();
    $("#index-menu").show();
    $("#logout-menu").show();
    $("#open-games-menu").show();
    $("#played-games-menu").show();
    $("#my-invites-menu").show();
}

function showLoggedInUsers(loggedInUsersListResponse) {

    var tableBody = $("#logged-in-users-body");
    tableBody.empty();

    for (var i = 1; i < loggedInUsersListResponse.length; i++)  {
        if (loggedInUsersListResponse[i] == "") {
            continue;
        }
        var trx = $("<tr>");
        var tdName = $("<td>" + loggedInUsersListResponse[i] + "</td>");
        var tdConn = $(
            "<td>" +
                "<div class='ckbx-circle-4'>" +
                    "<input checked disabled type='checkbox' id='ckbx-circle-4-1' value='1' name='ckbx-circle-4'>" +
                    "<label for='ckbx-circle-4-1'></label>" +
                "</div>" +
            "</td>");
        var trimmed = loggedInUsersListResponse[i].trim();
        var tdButt = $("<button class='btn btn-success'" + trimmed + "'>Vyzvať</button>");
        tdButt.attr("onclick", "challengePlayer('" + trimmed + "')");

        trx.append(tdName);
        trx.append(tdConn);
        trx.append(tdButt);

        tableBody.append(trx);
    }

}

function showOpenGames(openGamesListResponse) {

    var tableBody = $("#open-games-body");
    tableBody.empty();

    for (var i = 1; i < openGamesListResponse.length; i++) {
        if (openGamesListResponse[i] == "") {
            continue;
        }
        var trimmed = openGamesListResponse[i].trim();

        var button = $("<button gameUuid='" + trimmed + "'>Pripojiť sa</button>");
        button.attr("onclick", "joinGame('" + trimmed + "')");

        var tr = $("<tr>");

        var tdUuid = $("<td>" + openGamesListResponse[i] + "</td>");
        var tdJoin = $("<td>");
        tdJoin.append(button);

        tr.append(tdUuid);
        tr.append(tdJoin);

        tableBody.append(tr);
    }

}

function showMyInvites(myInvitesListResponse) {

    var tableBody = $("#open-games-body");
    tableBody.empty();

    for (var i = 1; i < myInvitesListResponse.length; i+=5) {
        if (myInvitesListResponse[i] == "") {
            continue;
        }
        var trimmed = myInvitesListResponse[i].trim();

        var button = $("<button gameUuid='" + trimmed + "'>Pripojiť sa</button>");
        button.attr("onclick", "joinGame('" + trimmed + "')");

        var tr = $("<tr>");

        var tdUuid = $("<td>" + myInvitesListResponse[i] + "</td>");
        var tdChallenger = $("<td>" + myInvitesListResponse[i+1] + "</td>");
        var tdStyle = $("<td>" + myInvitesListResponse[i+2] + "</td>");
        var tdType = $("<td>" + myInvitesListResponse[i+3] + "</td>");
        var tdJoin = $("<td>");
        tdJoin.append(button);

        tr.append(tdUuid);
        tr.append(tdChallenger);
        tr.append(tdStyle);
        tr.append(tdType);
        tr.append(tdJoin);

        tableBody.append(tr);
    }

}

function startPendingForSecondPlayer(successfulGameCreationResponse) {

    $("#game-creating-container").fadeOut(function() {

        localStorage.removeItem("activeGameUuid");
        localStorage.setItem("activeGameUuid", successfulGameCreationResponse[1]);

        $("#messenger").fadeIn();

        $("#game-grid-table").empty();

        for (var i = 0; i < successfulGameCreationResponse[2]; i++) {
            var row = $("<tr>");

            for (var y = 0; y < successfulGameCreationResponse[3]; y++) {
                var colVal = $("<td id='card-" + i + "-" + y + "' onclick='hitCard(" + i + "," + y + ")' class='memory-card'>");

                row.append(colVal);
            }

            $("#game-grid-table").append(row);
        }
    });
}

function openGameAfterSuccJoin(successfulGameJoinResponse) {

    $("#open-games-container").fadeOut(function () {

        localStorage.removeItem("activeGameUuid");
        localStorage.setItem("activeGameUuid", successfulGameJoinResponse[1]);

        $("#messenger").fadeIn();

        $("#game-grid-table").empty();

        for (var i = 0; i < successfulGameJoinResponse[2]; i++) {
            var row = $("<tr>");

            for (var y = 0; y < successfulGameJoinResponse[3]; y++) {
                var colVal = $("<td id='card-" + i + "-" + y + "' onclick='hitCard(" + i + "," + y + ")' class='memory-card'>");

                row.append(colVal);
            }

            $("#game-grid-table").append(row);
        }
    });
}

function gameFinished(gameFinishedResponse) {
    localStorage.removeItem("activeGameUuid");
    $.notify("Hra skončila, hráč " +
        gameFinishedResponse[1] +
        " odhalil " +
        gameFinishedResponse[2] +
        " párov a hráč " +
        gameFinishedResponse[3] +
        " odhalil " +
        gameFinishedResponse[4] +
        " párov",
        "success",
        function() {
            $.notify("Budete presmerovaný za 5s", "warning", function() {
                window.setTimeout(function () {
                    window.location = "/index";
                }, 5000);
            });
        }
    );
}

function showPrivateMessage(privateMessageResponse) {

    var message = "Správa od používateľa (" + privateMessageResponse[1]  + ") " + privateMessageResponse.slice(2, privateMessageResponse.length).join(" ");

    $.notify(
        message,
        "success");
}

function sendPrivateMessageToUser() {
    var receiver = $("#message-receiver").val();
    var message = $("#message-text").val();
    sendPrivateMessage(receiver, message);
}

function hitCard(x, y) {
    tryHitCard(localStorage.getItem("activeGameUuid"), x, y);
}

function openCard(cardVal, x, y) {
    if (cardVal == "-1") {
        $.notify("Kartu sa nepodarilo obrátiť", "error");
        return;
    }
    if (cardVal.endsWith(".jpg")) {
        var url = "url(../" + cardVal + ")";
        $("#card-" + x + "-" + y).css("background-image", url);
    } else if (cardVal.endsWith(".mp3")) {
        new Audio('../' + cardVal).play();
    }
}


function openCardsFinally(cardVal, x, y, x2, y2) {
    if (cardVal == "-1") {
        $.notify("Kartu sa nepodarilo obrátiť", "error");
        return;
    }

    if (cardVal.endsWith(".jpg")) {
        var url = "url(../" + cardVal + ")";
        $("#card-" + x + "-" + y).css("background-image", url);
        $("#card-" + x2 + "-" + y2).css("background-image", url);

        $("#card-" + x + "-" + y).addClass("card-finally-hit");
        $("#card-" + x2 + "-" + y2).addClass("card-finally-hit");
    } else if (cardVal.endsWith(".mp3")) {
        var url = "url(../sound.jpg)";
        $("#card-" + x + "-" + y).css("background-image", url);
        $("#card-" + x2 + "-" + y2).css("background-image", url);

        $("#card-" + x + "-" + y).addClass("card-finally-hit");
        $("#card-" + x2 + "-" + y2).addClass("card-finally-hit");
        new Audio('../' + cardVal).play();
    }
}

function secondCardHitUnsuccessful(cardVal, x, y) {

    if (cardVal.endsWith(".jpg")) {
        var url = "url(../" + cardVal + ")";
        $("#card-" + x + "-" + y).css("background-image", url);
    } else if (cardVal.endsWith(".mp3")) {
        new Audio('../' + cardVal).play();
    }

    window.setTimeout(
        function () {
            $(".memory-card:not(.card-finally-hit)").css("background-image", "url('../card-back.png')");
        },
    3000);
}

function challengePlayer(username) {
    createGameInvite(username);
    $.notify("Výzva používateľovi bola poslaná", "success");
}

