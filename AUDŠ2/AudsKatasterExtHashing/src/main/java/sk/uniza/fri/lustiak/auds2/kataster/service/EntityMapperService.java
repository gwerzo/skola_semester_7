package sk.uniza.fri.lustiak.auds2.kataster.service;

import org.springframework.stereotype.Service;
import sk.uniza.fri.lustiak.auds2.kataster.entities.bo.RealEstate;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.*;
import sk.uniza.fri.lustyno.datastructures.extendiblehashing.EHBlockDescription;
import sk.uniza.fri.lustyno.datastructures.extendiblehashing.EHFileDescriptor;


@Service
public class EntityMapperService {


    public DatabaseInfoUi fromFileDescriptor(EHFileDescriptor ehFileDescriptor) {
        DatabaseInfoUi ui = new DatabaseInfoUi();

        ui.setHlbkaSuboru(ehFileDescriptor.getFileDepth());
        ui.setPocetAdresovacichBitov(ehFileDescriptor.getAdressingBits());
        ui.setPocetEntitVHlavnomBloku(ehFileDescriptor.getNumberOfEntitiesInMainBlock());
        ui.setPocetEntitVOverflowBloku(ehFileDescriptor.getNumberOfEntitiesInOverflowBlock());
        ui.setPocetBlokov(ehFileDescriptor.getBlocksCount());
        ui.setPocetOverflowBlokov(ehFileDescriptor.getOverflowBlocksCount());
        ui.setVelkostHlavnehoSuboru(ehFileDescriptor.getSizeOfMainFile());
        ui.setVelkostOverflowSuboru(ehFileDescriptor.getSizeOfOverflowFile());
        ui.setVolneBloky(ehFileDescriptor.getFreeBlocks());
        ui.setVolneOverflowBloky(ehFileDescriptor.getFreeOverflowBlocks());

        return ui;
    }

    public BlockInfoUi fromEhBlockDescription(EHBlockDescription ehBlockDescription) {

        BlockInfoUi blockInfoUi = new BlockInfoUi();
        blockInfoUi.setDirectoryIndex(ehBlockDescription.directoryIndex);
        blockInfoUi.setIndexInFile(ehBlockDescription.indexInFile);
        blockInfoUi.setLocalLevel(ehBlockDescription.localLevel);
        blockInfoUi.setNumberOfValidEntities(ehBlockDescription.numberOfValidEntities);
        blockInfoUi.setOverflowBlock(ehBlockDescription.overflowBlock);
        blockInfoUi.setEntities(String.join(",", ehBlockDescription.entities));

        return blockInfoUi;
    }

    public OverflowBlockInfoUi fromEhOverflowBlockDescription(EHBlockDescription ehBlockDescription) {

        OverflowBlockInfoUi blockInfoUi = new OverflowBlockInfoUi();
        blockInfoUi.setIndexInFile(ehBlockDescription.indexInFile);
        blockInfoUi.setNumberOfValidEntities(ehBlockDescription.numberOfValidEntities);
        blockInfoUi.setOverflowBlock(ehBlockDescription.overflowBlock);
        blockInfoUi.setEntities(String.join(",", ehBlockDescription.entities));

        return blockInfoUi;
    }

    public RealEstate fromEditRealEstateUi(EditRealEstateUi editRealEstateUi) {

        RealEstate realEstate = new RealEstate();
        realEstate.setIdentifikacneCislo(editRealEstateUi.getIdentifikacneCislo());
        realEstate.setSupisneCislo(editRealEstateUi.getSupisneCislo());
        realEstate.setPopis(editRealEstateUi.getPopis());

        realEstate.setLatitude1(editRealEstateUi.getLatitude1());
        realEstate.setLongtitude1(editRealEstateUi.getLongitude1());
        realEstate.setIsNorth1(editRealEstateUi.getIsNorth1());
        realEstate.setIsEast1(editRealEstateUi.getIsEast1());

        realEstate.setLatitude2(editRealEstateUi.getLatitude2());
        realEstate.setLongtitude2(editRealEstateUi.getLongitude2());
        realEstate.setIsNorth2(editRealEstateUi.getIsNorth2());
        realEstate.setIsEast2(editRealEstateUi.getIsEast2());

        return realEstate;

    }

    public RealEstate fromCreateNewRealEstateUi(CreateNewRealEstateUi createNewRealEstateUi) {

        RealEstate newRealEstate = new RealEstate();
        newRealEstate.setIdentifikacneCislo(createNewRealEstateUi.getIdentifikacneCislo());
        newRealEstate.setSupisneCislo(createNewRealEstateUi.getSupisneCislo());
        newRealEstate.setPopis(createNewRealEstateUi.getPopis());

        newRealEstate.setLatitude1(createNewRealEstateUi.getLatitude1());
        newRealEstate.setLongtitude1(createNewRealEstateUi.getLongitude1());
        newRealEstate.setIsNorth1(createNewRealEstateUi.getIsNorth1());
        newRealEstate.setIsEast1(createNewRealEstateUi.getIsEast1());

        newRealEstate.setLatitude2(createNewRealEstateUi.getLatitude2());
        newRealEstate.setLongtitude2(createNewRealEstateUi.getLongitude2());
        newRealEstate.setIsNorth2(createNewRealEstateUi.getIsNorth2());
        newRealEstate.setIsEast2(createNewRealEstateUi.getIsEast2());

        return newRealEstate;
    }


    public RealEstateUiTableEntity fromRealEstate(RealEstate realEstate) {

        RealEstateUiTableEntity realEstateUiTableEntity = new RealEstateUiTableEntity();
        realEstateUiTableEntity.setIdentifikacneCislo(realEstate.getIdentifikacneCislo());
        realEstateUiTableEntity.setSupisneCislo(realEstate.getSupisneCislo());
        realEstateUiTableEntity.setPopis(realEstate.getPopis());

        realEstateUiTableEntity.setLatitude(realEstate.getLatitude1());
        realEstateUiTableEntity.setLongtitude(realEstate.getLongtitude1());
        realEstateUiTableEntity.setLatitudeOrientation(realEstate.getIsNorth1() ? "SEVER" : "JUH");
        realEstateUiTableEntity.setLongitudeOrientation(realEstate.getIsEast1() ? "VÝCHOD" : "ZÁPAD");

        realEstateUiTableEntity.setLatitude2(realEstate.getLatitude2());
        realEstateUiTableEntity.setLongtitude2(realEstate.getLongtitude2());
        realEstateUiTableEntity.setLatitudeOrientation2(realEstate.getIsNorth2() ? "SEVER" : "JUH");
        realEstateUiTableEntity.setLongitudeOrientation2(realEstate.getIsEast2() ? "VÝCHOD" : "ZÁPAD");

        return realEstateUiTableEntity;
    }



}
