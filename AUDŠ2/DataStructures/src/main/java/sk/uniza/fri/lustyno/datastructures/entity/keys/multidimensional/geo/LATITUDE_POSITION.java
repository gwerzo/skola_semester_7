package sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo;

public enum LATITUDE_POSITION {
    NORTH,
    SOUTH
}
