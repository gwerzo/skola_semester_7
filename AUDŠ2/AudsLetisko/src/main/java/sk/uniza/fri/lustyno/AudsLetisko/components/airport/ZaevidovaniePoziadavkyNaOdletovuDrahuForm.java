package sk.uniza.fri.lustyno.AudsLetisko.components.airport;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.PoziadavkaNaOdletUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entity_mapper.EntityMapper;
import sk.uniza.fri.lustyno.AudsLetisko.service.AircraftManagementService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;

public class ZaevidovaniePoziadavkyNaOdletovuDrahuForm extends BusinessComponent {

    private Binder<PoziadavkaNaOdletUiEntity> binder;
    private PoziadavkaNaOdletUiEntity entity;

    private EntityMapper entityMapper;

    private AircraftManagementService aircraftManagementService;

    public ZaevidovaniePoziadavkyNaOdletovuDrahuForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_2.getTitle());
        this.entity = new PoziadavkaNaOdletUiEntity();
        this.entityMapper = UIServices.getEntityMapper();
        this.aircraftManagementService = UIServices.getAircraftManagementService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new PoziadavkaNaOdletUiEntity();
        this.binder = new Binder<>(PoziadavkaNaOdletUiEntity.class);

        VerticalLayout layout = new VerticalLayout();

        FormLayout formLayout = new FormLayout();

        TextField medzinarodnyKodLietadla = new TextField("Medzinárodný kód lietadla");
        Button zaevidujPoziadavku = new Button("Zaeviduj požiadavku");
        zaevidujPoziadavku.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                zaevidujPoziadavku();
            } catch (ValidationException ex) {
                this.entity.flush();
                ex.printStackTrace();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        binder.forField(medzinarodnyKodLietadla)
                .asRequired()
                .bind(PoziadavkaNaOdletUiEntity::getMedzinarodnyKodLietadla, PoziadavkaNaOdletUiEntity::setMedzinarodnyKodLietadla);

        formLayout.addComponent(medzinarodnyKodLietadla);
        formLayout.addComponent(zaevidujPoziadavku);

        layout.addComponent(formLayout);

        this.parenComponent.showComponent(new Panel("Zaevidovanie požiadavky na pridelenie odletovej dráhy", layout));
    }

    private void zaevidujPoziadavku() {
        HandledOperationExecutor.submit(() -> {
            OperationMarker m = this.aircraftManagementService.vytvorPoziadavkuOPridelenieOdletovejDrahy(entityMapper.fromPoziadavkaNaPridelenieUiEntity(this.entity));
            if (m == OperationMarker.OPERATION_SUCCESFUL) {
                CustomNotification.notification("Požiadavka na pridelenie dráhy bola vytvorená");
            }
            this.entity.flush();
            this.binder.readBean(this.entity);
            return m;
        });
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_2.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
