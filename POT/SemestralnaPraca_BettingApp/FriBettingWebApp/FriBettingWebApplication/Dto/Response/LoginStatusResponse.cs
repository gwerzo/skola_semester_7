﻿namespace FriBettingWebApplication.Dto.Response
{
    public class LoginStatusResponse
    {
        public string Error { get; set; }
        public string Token { get; set; }
        public bool LoginSuccesfull { get; set; }

    }
}