package sk.uniza.fri.lustyno.fripd.components.vehicles;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.fripd.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.fripd.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.fripd.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.fripd.components.vehicles.common.VehicleDetailForm;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.VozidloEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.VyhladanieVozidlaEntityUi;
import sk.uniza.fri.lustyno.fripd.service.UIServices;
import sk.uniza.fri.lustyno.fripd.service.VehicleService;
import sk.uniza.fri.lustyno.fripd.utils.CustomNotification;
import sk.uniza.fri.lustyno.fripd.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.fripd.utils.OperationMarker;

public class VyhladanieVozidlaPodlaVINForm extends BusinessComponent {

    private VehicleService vehicleService;

    private Binder<VyhladanieVozidlaEntityUi> binder;
    private VyhladanieVozidlaEntityUi entity;

    private VerticalLayout vehicleDetailsLayout;

    public VyhladanieVozidlaPodlaVINForm(RepaintableComponent parentComponent) {
        super(parentComponent, MENU_TITLE.VEHICLES_MENU_3.getTitle());
        this.vehicleService = UIServices.getVehicleService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.vehicleDetailsLayout = new VerticalLayout();
        this.binder = new Binder<>(VyhladanieVozidlaEntityUi.class);
        this.entity = new VyhladanieVozidlaEntityUi();

        FormLayout layout = new FormLayout();

        TextField vinCodeTf = new TextField("VIN Kód");
        Button vyhladaj = new Button("Vyhľadaj");

        layout.addComponents(vinCodeTf, vyhladaj, vehicleDetailsLayout);

        vyhladaj.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                vyhladaj();
            } catch (ValidationException ex) {
                ex.printStackTrace();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        binder.forField(vinCodeTf)
                .asRequired()
                .withValidator(new StringLengthValidator("VIN Kód pre vyhľadanie musí mať dĺžku v rozmedzí 1 - 17", 1, 17))
                .bind(VyhladanieVozidlaEntityUi::getVin, VyhladanieVozidlaEntityUi::setVin);

        this.parenComponent.showComponent(new Panel("Vyhľadanie vozidla podľa VIN kódu", new VerticalLayout(layout)));

    }

    private void vyhladaj() {

        HandledOperationExecutor.submit(() -> {
            VozidloEntityUi entityUi = this.vehicleService.findVehicleByVIN(this.entity);
            zobrazVozidlo(entityUi);
            return OperationMarker.OPERATION_SUCCESFUL;
        });

    }

    private void zobrazVozidlo(VozidloEntityUi entityUi) {
        this.vehicleDetailsLayout.removeAllComponents();
        this.vehicleDetailsLayout.addComponent(new Panel("Detail vozidla",
                new VerticalLayout(
                        new VehicleDetailForm(entityUi))));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.VEHICLES_MENU_3.getTitle();
    }

}
