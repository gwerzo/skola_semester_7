package sk.uniza.fri.lustyno.AudsLetisko.components.common;

import sk.uniza.fri.lustyno.AudsLetisko.components.system.*;

public class SystemMenuComponent extends AbstractMenu {

    public SystemMenuComponent(RepaintableComponent component) {
        this.menuItems.add(new PridanieOdletovejDrahyForm(component));
        this.menuItems.add(new ZmenaCasuSystemuForm(component));
        this.menuItems.add(new ExportSystemuDoSuboruForm(component));
        this.menuItems.add(new ImportSystemuZoSuboruForm(component));
        this.menuItems.add(new GeneratorForm(component));
        this.menuItems.add(new VsetkyLietadlaVSystemeForm(component));
        this.menuItems.add(new VsetkyDrahyVSystemeForm(component));
        initMenuItems();
    }

}
