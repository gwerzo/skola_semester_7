package linearhashing.entities;

import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Record;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.HasSize;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;

import java.util.Arrays;

@HasSize(sizeInBytes = 9)
public class IntegerEntity extends Record<IntegerKey> {

    private IntegerKey integerKey;

    private Integer inty;

    private Integer dataValue;

    public IntegerEntity(Integer integer, Integer dataValue) {
        this.inty = integer;
        this.dataValue = dataValue;
        this.integerKey = new IntegerKey(integer);
    }

    public IntegerEntity(Integer integer, Integer dataValue, Boolean valid) {
        this.inty = integer;
        this.valid = valid;
        this.dataValue = dataValue;
        this.integerKey = new IntegerKey(integer);
    }

    public Integer getDataValue() {
        return this.dataValue;
    }

    public void setDataValue(Integer dataValue) {
        this.dataValue = dataValue;
    }

    @Override
    public IntegerKey getKey() {
        return this.integerKey;
    }

    @Override
    public byte[] toBytesArray() {
        return
                ToByteArrayConverter.concatenateByteArrays(
                        Arrays.asList(
                                ToByteArrayConverter.bytesFromBoolean(this.valid),
                                ToByteArrayConverter.bytesFromInt(this.inty),
                                ToByteArrayConverter.bytesFromInt(this.dataValue)));
    }

    @Override
    public int getHash() {
        return this.inty;
    }

    public static IntegerEntity fromByteArray(byte[] fromArray) {
        Boolean valid = FromByteArrayConverter.booleanFromByte(fromArray[0]);
        Integer key = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(fromArray, 1, 5));
        Integer data = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(fromArray, 5, 9));
        return new IntegerEntity(key, data, valid);
    }
}
