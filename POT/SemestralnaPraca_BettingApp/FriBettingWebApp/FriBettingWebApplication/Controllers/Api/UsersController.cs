﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using FriBettingWebApplication.Dto.Request;
using FriBettingWebApplication.Filters;
using FriBettingWebApplication.Security;
using FriBettingWebApplication.Services.Face;

namespace FriBettingWebApplication.Controllers.Api
{
    public class UsersController : ApiController
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        // Controller metóda pre registrovanie používateľa
        [HttpPost]
        [AllowAnonymous]
        [Route("api/users/register")]
        public IHttpActionResult RegisterUser([FromBody] RegisterUserRequest request)
        {
            var registrationStatus = _userService.RegisterUser(request);
            return Ok(registrationStatus);
        }

        // Controller metóda pre registráciu používateľa
        [HttpPost]
        [AllowAnonymous]
        [Route("api/users/login")]
        public IHttpActionResult LoginUser([FromBody] LoginUserRequest request)
        {
            var loginStatus =  _userService.LoginUser(request);
            return Ok(loginStatus);
        }

        // Controller metóda, z ktorej vie klient určiť, či JWT token, ktorý klient vlastní(alebo aj nevlastní vôbec) je
        // platný => použ. prihlásený
        // neplatný => použ. neprihlásený
        [HttpGet]
        [JwtAuthentication]
        public IHttpActionResult Index()
        {
            return Ok();
        }

        [HttpGet]
        [Route("api/users/detail")]
        [JwtAuthentication]
        public IHttpActionResult GetUserDetails(HttpRequestMessage request)
        {
            var username = JwtSecurityManager.ExtractUsernameFromToken(request.Headers.GetValues("Authorization").FirstOrDefault());
            return Ok(_userService.GetAccountDetails(username));
        }

    }
}
