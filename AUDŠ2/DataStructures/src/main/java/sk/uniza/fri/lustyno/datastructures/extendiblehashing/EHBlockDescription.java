package sk.uniza.fri.lustyno.datastructures.extendiblehashing;

import java.util.List;

public class EHBlockDescription {

    public Integer directoryIndex;
    public Integer indexInFile;
    public Integer localLevel;
    public Integer numberOfValidEntities;
    public Integer overflowBlock;
    public List<String> entities;

}
