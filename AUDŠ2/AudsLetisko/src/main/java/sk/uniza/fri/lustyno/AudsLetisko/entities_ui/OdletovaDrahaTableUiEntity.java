package sk.uniza.fri.lustyno.AudsLetisko.entities_ui;

import lombok.Data;

@Data
public class OdletovaDrahaTableUiEntity {

    private Integer id;

    private Integer dlzka;

    private Boolean jeVolna;

}
