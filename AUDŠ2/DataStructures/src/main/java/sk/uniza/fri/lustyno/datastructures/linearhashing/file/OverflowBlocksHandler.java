package sk.uniza.fri.lustyno.datastructures.linearhashing.file;

import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.config.FileConfig;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.Constants;
import sk.uniza.fri.lustyno.datastructures.queue.PairingHeap;
import sk.uniza.fri.lustyno.datastructures.queue.QueueNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class OverflowBlocksHandler {

    protected Double MAX_THRESHOLD = Constants.SPLIT_MAXIMUM_THRESHOLD;

    private Integer entityCounter;

    private Integer offsetCounter;

    private PairingHeap<IntegerKey, Integer> freeBlocks;

    private ArrayList<NodeUsedWrapper> allOverflowBlocksIndexedUsed;

    private Integer maxOffset = -1;

    private Integer overflowBlockSize;

    private Integer numberOfOverflowBlocksUsed = 0;

    public OverflowBlocksHandler(Integer overflowBlockSize) {
        this.entityCounter = 0;
        this.offsetCounter = 0;
        this.overflowBlockSize = overflowBlockSize;
        this.freeBlocks = new PairingHeap<>();
        this.allOverflowBlocksIndexedUsed = new ArrayList<>();
    }

    public OverflowBlocksHandler(FileConfig fromConfig) {
        this.MAX_THRESHOLD                  = fromConfig.maxThreshold;
        this.entityCounter                  = fromConfig.entityCounter;
        this.offsetCounter                  = fromConfig.offsetCounter;
        this.maxOffset                      = fromConfig.maxOffset;
        this.overflowBlockSize              = fromConfig.overflowBlockSize;
        this.numberOfOverflowBlocksUsed     = fromConfig.numberOfOverflowBlocksUsed;
        this.freeBlocks                     = new PairingHeap<>();
        this.allOverflowBlocksIndexedUsed   = new ArrayList<>(this.maxOffset / this.overflowBlockSize + 1);

        for (int i = 0; i < this.maxOffset / this.overflowBlockSize + 1; i++) {
            this.allOverflowBlocksIndexedUsed.add(new NodeUsedWrapper(null, true));
        }

        for (Integer freeBlockOffset : fromConfig.freeOverflowBlocksOffsets) {
            Integer blockNumber = freeBlockOffset / this.overflowBlockSize;
            QueueNode<IntegerKey, Integer> inserted = this.freeBlocks.insert(new IntegerKey(freeBlockOffset), freeBlockOffset);
            this.allOverflowBlocksIndexedUsed.set(blockNumber, new NodeUsedWrapper(inserted, false));
        }
    }

    public FileConfig fillFileConfig(FileConfig fileConfig) {
        fileConfig.maxThreshold = this.MAX_THRESHOLD;
        fileConfig.entityCounter = this.entityCounter;
        fileConfig.offsetCounter = this.offsetCounter;
        fileConfig.maxOffset = this.maxOffset;
        fileConfig.overflowBlockSize = this.overflowBlockSize;
        fileConfig.numberOfOverflowBlocksUsed = this.numberOfOverflowBlocksUsed;

        List<QueueNode<IntegerKey, Integer>> freeBlocksNodes = this.freeBlocks.levelOrder();
        if (freeBlocksNodes == null) {
            fileConfig.freeOverflowBlocksOffsets = new LinkedList<>();
        } else {
            fileConfig.freeOverflowBlocksOffsets = new LinkedList<>();
            for (QueueNode<IntegerKey, Integer> freeBlock : freeBlocksNodes) {
                fileConfig.freeOverflowBlocksOffsets.add(freeBlock.getData());
            }
        }
        return fileConfig;
    }

    /**
     * Pridá naspäť voľný blok
     * @param offset
     * @return - počet blokov "odseknutých" konsolidáciou o ktoré je možné skrátiť súbor s overflow blokmi
     */
    public Integer freeOverflowBlockAtOffset(Integer offset) {
        QueueNode<IntegerKey, Integer> freedNode = this.freeBlocks.insert(new QueueNode<>(new IntegerKey(offset), offset));

        this.allOverflowBlocksIndexedUsed.set(offset / this.overflowBlockSize, new NodeUsedWrapper(freedNode, false));

        this.numberOfOverflowBlocksUsed--;

        if (offset.equals(this.maxOffset)) {
            return this.consolidate();
        }
        return 0;
    }

    /**
     * Skonsoliduje štruktúru,
     * "usekne" voľné bloky z konca, ktoré "visia"
     *
     * @return - počet "useknutých" blokov
     */
    private Integer consolidate() {
        int counterOfConsolidatedBlocks = 0;

        NodeUsedWrapper nuw = this.allOverflowBlocksIndexedUsed.get(this.allOverflowBlocksIndexedUsed.size() - 1);
        while (!nuw.isUsed) {
            this.freeBlocks.delete(nuw.getNode());
            counterOfConsolidatedBlocks++;
            this.maxOffset -= this.overflowBlockSize;

            this.allOverflowBlocksIndexedUsed.remove(this.allOverflowBlocksIndexedUsed.size() - 1);
            if (this.allOverflowBlocksIndexedUsed.size() > 0) {
                nuw = this.allOverflowBlocksIndexedUsed.get(this.allOverflowBlocksIndexedUsed.size() - 1);
            } else {
                break;
            }
        }
        return counterOfConsolidatedBlocks;
    }

    /**
     * Vráti prvý voľný blok,
     * ak takýto blok neexistuje,
     * vráti offset ďalšieho,
     * ktorý by po ňom mal nasledovať
     *
     * @return
     */
    public Integer getFirstEmptyOffset() {
        if (this.freeBlocks.min() == null) {
            if (this.maxOffset == -1) {
                this.maxOffset = 0;
                this.allOverflowBlocksIndexedUsed.add(new NodeUsedWrapper(null, true));
                return 0;
            }
            this.maxOffset = this.maxOffset + this.overflowBlockSize;
            this.allOverflowBlocksIndexedUsed.add(new NodeUsedWrapper(null, true));
            return this.maxOffset;
        } else {
            QueueNode<IntegerKey, Integer> node = this.freeBlocks.deleteMin();
            Integer offsetOfblock = node.getData();
            this.allOverflowBlocksIndexedUsed.set(offsetOfblock / this.overflowBlockSize, new NodeUsedWrapper(node, true));
            return offsetOfblock;
        }
    }

    public Integer getNumberOfOverflowBlocksUsed() {
        return this.numberOfOverflowBlocksUsed;
    }

    public void increaseNumberOfBlocksUsed() {
        this.numberOfOverflowBlocksUsed++;
    }

    public List<Integer> getOverflowBlocksOffsets() {
        List<Integer> offsets = new LinkedList<>();

        for (int i = 0; i <= this.maxOffset; i+=this.overflowBlockSize) {
            offsets.add(new Integer(i));
        }

        return offsets;
    }

    /**
     * Vráti veľkosť v bytoch, ktorú súbor aktuálne má
     * @return
     */
    public Integer getFullSize() {
        return this.allOverflowBlocksIndexedUsed.size() * this.overflowBlockSize;
    }


    private class NodeUsedWrapper {

        private QueueNode<IntegerKey, Integer> nodeInQueue;

        private Boolean isUsed;

        public NodeUsedWrapper(QueueNode<IntegerKey, Integer> node, Boolean isUsed) {
            this.nodeInQueue = node;
            this.isUsed = isUsed;
        }

        public QueueNode<IntegerKey, Integer> getNode() {
            return this.nodeInQueue;
        }

        public Boolean isUsed() {
            return this.isUsed;
        }

    }

}
