﻿using System.Collections.Generic;

namespace MemoryGameDaoApp.Bo
{
    public class GameStyle
    {
        public int GameStyleId { get; set; }
        public string Description { get; set; }

        public ICollection<GameCard> GameCards { get; set; }

        public GameStyle()
        {

        }


    }
}
