package sk.uniza.fri.lustyno.AudsLetisko;

public class BusinessException extends RuntimeException {

    public BusinessException(String message) {
        super(message);
    }

}
