﻿using FriBettingDatabaseApp.Bo;

namespace FriBettingDatabaseApp.Repository.Base
{
    public interface IUserRepository: IRepository<BettingUser>
    {

        BettingUser GetUserByUsername(string username);

    }
}
