package sk.uniza.fri.lustiak.auds2.kataster.components.custom;


import com.vaadin.ui.TextField;

/**
 * Customizovateľný text field component
 */
public class CustomTextField extends TextField {

    private final String FIELD_WIDTH = "150px";
    private final String FIELD_HEIGHT = "20px";

    public CustomTextField(String caption, Boolean editable) {
        super(caption);
        this.setWidth(this.FIELD_WIDTH);
        this.setHeight(this.FIELD_HEIGHT);
        this.setEnabled(editable);
    }

}
