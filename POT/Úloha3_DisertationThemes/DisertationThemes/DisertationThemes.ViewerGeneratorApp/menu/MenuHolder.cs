﻿using System;
using System.Collections.Generic;
using System.Text;
using DisertationThemes.ViewerGeneratorApp.menu;
using Microsoft.VisualBasic.CompilerServices;

namespace DisertationThemes.ViewerGeneratorApp
{
    public class MenuHolder
    {
        private List<AbstractMenuItem> menuItems;

        public MenuHolder()
        {
            this.menuItems = new List<AbstractMenuItem>();

            this.menuItems.Add(new MenuItemShowSupervisors(this));
            this.menuItems.Add(new MenuItemShowStPrograms(this));
            this.menuItems.Add(new MenuItemShowThemesFiltered(this));
            this.menuItems.Add(new MenuItemGenerateWordDoc(this));
            this.menuItems.Add(new MenuItemCloseApp(this));

            this.Looper();
        }

        private void Looper()
        {
            string message = null;
            while (true)
            {
                this.PrintMenu(message);

                var userInput = Console.ReadLine();
                int menuIndexInput;
                bool parseSuccess = Int32.TryParse(userInput, out menuIndexInput);

                if (!parseSuccess)
                {
                    message = "Zadali ste nesprávne číslo z menu, zadať môžete len celočíselné hodnoty";
                    continue;
                }

                if (menuIndexInput > this.menuItems.Count || menuIndexInput < 1)
                {
                    message = "Nezadali ste správne číslo z menu v rozmedzí 1-" + this.menuItems.Count;
                    continue;
                }

                if (this.menuItems[menuIndexInput - 1].PerformAction() == -1)
                {
                    return;
                }
            }
        }

        public void PrintMenu(string initMessage)
        {
            var oldColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            if (initMessage != null && initMessage.Length != 0)
            {
                Console.WriteLine(initMessage);
            }
            Console.ForegroundColor = oldColor;

            int i = 1;
            foreach (var menuItem in menuItems)
            {
                Console.WriteLine(i++ + ".\t" + menuItem.GetMenuText());
            }
            Console.Write("\nZadajte možnosť z menu:");
        }
    }
}
