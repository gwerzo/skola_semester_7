package sk.uniza.fri.lustiak.auds2.kataster.components.common;

public enum MENU_TITLE {

    MENU_1("1. Vyhľadanie NEHNUTEĽNOSTÍ podľa ID"),
    MENU_2("2. Pridanie novej NEHNUTEĽNOSTI"),
    MENU_3("3. Vyradenie NEHNUTEĽNOSTI"),
    MENU_4("4. Editácia údajov NEHNUTEĽNOSTI"),

    //-----------------------------------------------------------------------------
    SYSTEM_MENU_1("1. Generátor systému"),
    SYSTEM_MENU_2("2. Import systému"),
    SYSTEM_MENU_3("3. BLOKY"),
    SYSTEM_MENU_4("4. Uloženie stavu");

    private String title;

    MENU_TITLE(String s) {
        title = s;
    }

    public String getTitle() {
        return title;
    }
}
