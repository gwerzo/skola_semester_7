﻿using System;
using Newtonsoft.Json;

namespace FriBettingWebApplication.Dto.Response
{
    public class MatchResponse
    {

        [JsonProperty("match_id")]
        public int MatchId { get; set; }
        [JsonProperty("sport_id")]
        public int SportId { get; set; }
        [JsonProperty("host_name")]
        public string HostName { get; set; }
        [JsonProperty("guest_name")]
        public string GuestName { get; set; }
        [JsonProperty("host_id")]
        public int HostId { get; set; }
        [JsonProperty("guest_id")]
        public int GuestId { get; set; }
        [JsonProperty("match_date")]
        public DateTime DateTimeOfMatch { get; set; }


    }
}