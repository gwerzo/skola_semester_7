package sk.uniza.fri.lustyno.fripd.entities_ui.lincense;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class ZmenaUdajovPreukazuEntityUi {

    private String menoVodica;
    private String priezviskoVodica;
    private Integer evcVodica;
    private LocalDate datumUkonceniaPlatnostiPreukazu;
    private Boolean zakazViestVozidlo;
    private Integer pocetDopravnychPriestupkov;

}
