﻿using System;
using System.Collections.Generic;
using MemoryGameServerApp.Server;
using SuperWebSocket;

namespace MemoryGameServerApp.Command
{
    public class GetMyInvitesCommand:ICommand
    {
        public const string COMMAND = "-listmyinvites";
        private const string INVITES_LIST = "-myinviteslist";

        public void executeCommand(string usernameOfCommandCaller, MemoryGameServer memoryGameServer,
            WebSocketSession callerSocketSession)
        {
            List<string> myInvites = memoryGameServer.GetInvitesForUser(usernameOfCommandCaller);

            callerSocketSession.Send(INVITES_LIST + " " + String.Join(" ", myInvites));
        }
    }
}
