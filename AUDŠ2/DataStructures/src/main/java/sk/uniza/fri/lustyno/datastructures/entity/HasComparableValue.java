package sk.uniza.fri.lustyno.datastructures.entity;

/**
 * Interface to force keys to be able to compare itself to another key of same class
 * So that method K1.compareTo(K2) returns
 *  1  <=> if K1 > K2
 *  0  <=> if K1 = K2
 *  -1 <=> if K1 < K2
 * @param <V> - value of key
 */
public interface HasComparableValue<V extends Comparable> {

    V getValue();

    HasComparableValue<V> getMinValue();

}
