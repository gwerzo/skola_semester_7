package sk.uniza.fri.lustyno.datastructures.entity;

public interface HasSize {

    public int getSize();

}
