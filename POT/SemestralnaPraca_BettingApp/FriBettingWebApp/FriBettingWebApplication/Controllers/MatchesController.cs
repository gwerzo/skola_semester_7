﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using FriBettingWebApplication.Dto.Request;
using FriBettingWebApplication.Dto.Response;
using FriBettingWebApplication.Dto.Response.Page;
using FriBettingWebApplication.Models;
using FriBettingWebApplication.Security;
using Newtonsoft.Json;

namespace FriBettingWebApplication.Controllers
{
    public class MatchesController : Controller
    {
        private readonly HttpClient _client = new HttpClient();

        public ActionResult Index()
        {
            return View("MatchList");
        }

        public ActionResult NewMatch()
        {
            return View();
        }

        public async Task<ActionResult> Evaluation([FromUri] EvaluateMatchRequest evaluateMatchRequest)
        {
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["matchId"] = evaluateMatchRequest.MatchId == null ? "0" : evaluateMatchRequest.MatchId.Value.ToString();

            var url = API_CONF.API_URL + "matches/parts?" + query.ToString();

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", JwtSecurityManager.ApplicationJwtToken);

            var responseJson = await _client.GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<List<MatchPartResponse>>(responseJson);

            return View("Evaluation", new MatchEvaluationModel
            {
                MatchId = evaluateMatchRequest.MatchId ?? 0,
                MatchPartsIds = response == null ? new List<int>() : response.Select(e => e.MatchPartId).ToList()
            });
        }

        public async Task<ActionResult> MatchList([FromUri] GetMatchesFilteredRequest getMatchesFilteredRequest)
        {
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["Page"] = getMatchesFilteredRequest.Page.ToString();
            query["Size"] = getMatchesFilteredRequest.Size.ToString();
            query["FromDate"] = getMatchesFilteredRequest.FromDate?.ToString("MM/dd/yyyy");
            query["ToDate"] = getMatchesFilteredRequest.ToDate?.ToString("MM/dd/yyyy");

            var url = API_CONF.API_URL + "matches?" + query.ToString();

            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", JwtSecurityManager.ApplicationJwtToken);

            var responseJson = await _client.GetStringAsync(url);

            var response = JsonConvert.DeserializeObject<MatchesPageResponse>(responseJson);

            return View("MatchList", new MatchesTableModel
            {
                Matches = response.Matches,
                Size = getMatchesFilteredRequest.Size ?? 10,
                Page = getMatchesFilteredRequest.Page ?? 0,
                Count = response.Count,
                FromDate = getMatchesFilteredRequest.FromDate ?? DateTime.MinValue,
                ToDate = getMatchesFilteredRequest.ToDate ?? DateTime.MaxValue
            });
        }

    }
}