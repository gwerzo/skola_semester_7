package sk.uniza.fri.lustiak.viegenere;

public class ViegenereDecryptor {

    public static String decryptTextWithPassword(String text, String password) {
        StringBuilder sb = new StringBuilder();

        int pwdIter = 0;
        for (int i = 0; i < text.length(); i++) {
            char actualChar = text.charAt(i);
            if (actualChar < 'A' || actualChar > 'Z') {
                sb.append(actualChar);
                continue;
            }
            char charPwd = password.charAt(pwdIter % password.length());

            int charPwdDeflated = charPwd - 'A';
            int actualCharDeflated = actualChar - 'A';

            int dec = (actualCharDeflated - (charPwdDeflated - 26)) % 26 + 65;
            sb.append(((char)dec));
            pwdIter++;
        }
        return sb.toString();
    }

}
