﻿/**
 * JS metódy pre integráciu na API poskytovanú controllerom TicketsController.cs
 */
function addOddToUserOpenedTicket(oddId) {
    $.ajax({
        'url': "/api/tickets/addodd",
        'method': "POST",
        'data': JSON.stringify({
            "OddId": oddId
        }),
        'headers': {
            'Authorization': localStorage.getItem("fri-betting-app-token")
        },
        'contentType': "application/json",
        'dataType': "json",
        'success': function (data) {
            if (data["Successful"]) {
                $.notify("Odd was added to your opened ticket successfully", "success");
            } else {
                $.notify(data["ErrorMessage"], "error");
            }
        }
    });
}


function getMyCurrentTicket(callback) {
    $.ajax({
        'url': "/api/tickets/current",
        'method': "GET",
        'headers': {
            'Authorization': localStorage.getItem("fri-betting-app-token")
        },
        'contentType': "application/json",
        'dataType': "json",
        'success': function (data) {
            callback(data);
        },
        'error': function() {
            $.notify("Error occured when loading current ticket state", "error");
        }
    });
}

function getMyNotEvaluatedTickets(callback) {
    $.ajax({
        'url': "/api/tickets/closed",
        'method': "GET",
        'headers': {
            'Authorization': localStorage.getItem("fri-betting-app-token")
        },
        'contentType': "application/json",
        'dataType': "json",
        'success': function (data) {
            callback(data);
        },
        'error': function () {
            $.notify("Error occured when loading tickets", "error");
        }
    });
}

function getEvaluatedTickets(callback) {
    $.ajax({
        'url': "/api/tickets/evaluated",
        'method': "GET",
        'headers': {
            'Authorization': localStorage.getItem("fri-betting-app-token")
        },
        'contentType': "application/json",
        'dataType': "json",
        'success': function (data) {
            callback(data);
        },
        'error': function () {
            $.notify("Error occured when loading tickets", "error");
        }
    });
}

function createCurrentTicket(betValue, callback) {
    $.ajax({
        'url': "/api/tickets/create",
        'method': "POST",
        'headers': {
            'Authorization': localStorage.getItem("fri-betting-app-token")
        },
        'data': JSON.stringify({
            "BetValue": betValue
        }),
        'contentType': "application/json",
        'dataType': "json",
        'success': function (data) {
            if (!data.Successful) {
                $.notify(data.ErrorMessage, "error");
                return;
            }
            callback(data);
        },
        'error': function () {
            $.notify("Error when creating ticket", "error");
        }
    });
}