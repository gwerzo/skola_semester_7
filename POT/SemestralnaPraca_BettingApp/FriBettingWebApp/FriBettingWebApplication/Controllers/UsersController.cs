﻿using System.Web.Mvc;

namespace FriBettingWebApplication.Controllers
{
    public class UsersController : Controller
    {
        public ActionResult Index()
        {
            return MyAccount();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult MyAccount()
        {
            return View();
        }
    }
}