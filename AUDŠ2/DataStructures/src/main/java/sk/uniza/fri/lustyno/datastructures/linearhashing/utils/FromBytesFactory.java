package sk.uniza.fri.lustyno.datastructures.linearhashing.utils;

public abstract class FromBytesFactory<R> {

    public abstract R fromBytes(byte[] bytes);

    public abstract R emptyInvalidInstance();

}
