package sk.uniza.fri.lustyno.datastructures.extendiblehashing;

import sk.uniza.fri.lustyno.datastructures.exception.DataStructureWrongUsageException;
import sk.uniza.fri.lustyno.datastructures.extendiblehashing.entities.EHEntity;
import sk.uniza.fri.lustyno.datastructures.extendiblehashing.util.IndexToOverflowBlock;
import sk.uniza.fri.lustyno.datastructures.utils.BitSetUtils;
import sk.uniza.fri.lustyno.datastructures.utils.CustomBitSet;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class EHFile<T extends EHEntity<T>> {

    /**
     * Názov súboru bez prípon/suffixov
     */
    private String databaseName;

    /**
     * Drží prototypy potrebných tried
     */
    private EHPrototypeHolder<T> ehPrototypeHolder;

    /**
     * Aktuálna hĺbka súboru,
     * by default inicializovaná na 1 BIT
     */
    private int D = 1;

    /**
     * Adresár
     */
    private EHDirectoryBox[] directory;

    /**
     * Block manager, ktorý rieši menežovanie
     * hlavných a overflow blokov
     */
    private EHBlockManager<T> ehBlockManager;

    /**
     * File handler, ktorý manažuje prácu so súbormi
     */
    private EHFileHandler ehFileHandler;

    /**
     * Defaultná hodnota pre veľkosť adresovania,
     * ktorú je možné zmeniť inicializáciou cez
     * preťažený konštruktor
     *
     * By default je to hodnota 32,
     * ktorá reprezenzuje 4 byty (veľkosť int v jave)
     */
    private int numberOfAdressingBits;

    /**
     * Konštruktor pre vytvorenie inštancie EHFile z uloženého configu,
     * treba len poslať názov "databázy" bez všetkých suffixov
     *
     * Príklad ak sa save volá entities_save.ehdb, treba poslať argument "entities"
     *
     * @param classOfEntities - Trieda entít tohto súboru
     */
    public EHFile(Class<? extends EHEntity> classOfEntities, EHFileConfig ehFileConfig) {
        initialize(classOfEntities, ehFileConfig);
    }

    /**
     * Vytvorenie inštancie rozšíriteľného hashovania
     * @param classOfEntities - Trieda entity, nad ktorou pracuje
     * @param databaseName - Názov súboru, do ktorého sa majú dáta ukladať
     * @param numberOfEntitiesInOneBlock - Blokovací faktor pre hlavné bloky
     * @param numberOfEntitiesInOverflowBlock - Blokovací faktor pre overflow bloky
     */
    public EHFile(Class<? extends EHEntity> classOfEntities, String databaseName,
                  Integer numberOfEntitiesInOneBlock, Integer numberOfEntitiesInOverflowBlock) {
        initialize(classOfEntities, databaseName, numberOfEntitiesInOneBlock, numberOfEntitiesInOverflowBlock,32);
    }

    /**
     * Vytvorenie inštancie rozšíriteľného hashovania
     * @param classOfEntities - Trieda entity, nad ktorou pracuje
     * @param databaseName - Názov súboru, do ktorého sa majú dáta ukladať
     * @param numberOfEntitiesInOneBlock - Blokovací faktor pre hlavné bloky
     * @param numberOfEntitiesInOverflowBlock - Blokovací faktor pre overflow bloky
     * @param numberOfAdressingBits - Koľko bitov z hashu entít sa má maximálne používať na adresovanie
     */
    public EHFile(Class<? extends EHEntity> classOfEntities, String databaseName,
                  Integer numberOfEntitiesInOneBlock, Integer numberOfEntitiesInOverflowBlock,
                  Integer numberOfAdressingBits) {
        initialize(classOfEntities, databaseName, numberOfEntitiesInOneBlock, numberOfEntitiesInOverflowBlock, numberOfAdressingBits);
    }

    private void initialize(Class<? extends EHEntity> classOfEntities, String databaseName,
                            Integer numberOfEntitiesInOneBlock, Integer numberOfEntitiesInOverflowBlock,
                            Integer numberOfAdressingBits) {
        this.databaseName = databaseName;
        EHEntity targetEntityObject = EHPrototypeManager.getPrototypeEntityForClass(classOfEntities);
        if (targetEntityObject == null) {
            throw new DataStructureWrongUsageException(
                    this.getClass().getName(),
                    "constructor",
                    "Pre správne fungovanie EHManagera musíš v EHPrototypeManager zaregistrovať dummy entitu");
        }

        T entityPrototype = (T) targetEntityObject;
        EHBlock<T> EHBlockPrototype = new EHBlock<>(numberOfEntitiesInOneBlock, entityPrototype);
        EHOverflowBlock<T> overflowBlockPrototype = new EHOverflowBlock<>(numberOfEntitiesInOverflowBlock, entityPrototype, true);

        this.ehPrototypeHolder = new EHPrototypeHolder<>(entityPrototype, EHBlockPrototype, overflowBlockPrototype);

        // By default adresár o jednom bite (dve adresovania)
        this.directory = new EHDirectoryBox[2];
        // Druhý blok nech má setnutú správnu adresu

        this.directory[0] = new EHDirectoryBox();
        this.directory[1] = new EHDirectoryBox();

        this.directory[1].indexInMainFile = this.ehPrototypeHolder.getEHBlockPrototype().getBlockSize();

        this.ehFileHandler = new EHFileHandler(databaseName, true);

        // Nastavenie správnych lok.hĺbok bloku, spraví 2 zápisy do súboru
        this.ehFileHandler.writeToMainFile(this.ehPrototypeHolder.getEHBlockPrototype().toBytes(), this.directory[0].indexInMainFile);
        this.ehFileHandler.writeToMainFile(this.ehPrototypeHolder.getEHBlockPrototype().toBytes(), this.directory[1].indexInMainFile);

        this.ehBlockManager = new EHBlockManager<>(
                this.ehFileHandler, this.ehPrototypeHolder, 2,
                this.ehPrototypeHolder.getEHBlockPrototype().getBlockSize(), this.ehPrototypeHolder.getOverflowBlockPrototype().getBlockSize());

        this.numberOfAdressingBits = numberOfAdressingBits;
    }

    private void initialize(Class<? extends EHEntity> classOfEntities, EHFileConfig ehFileConfig) {
        this.D = ehFileConfig.D;
        this.databaseName = ehFileConfig.databaseName;
        this.numberOfAdressingBits = ehFileConfig.numberOfAddressingBits;

        EHEntity targetEntityObject = EHPrototypeManager.getPrototypeEntityForClass(classOfEntities);
        if (targetEntityObject == null) {
            throw new DataStructureWrongUsageException(
                    this.getClass().getName(),
                    "constructor",
                    "Pre správne fungovanie EHManagera musíš v EHPrototypeManager zaregistrovať dummy entitu");
        }
        T entityPrototype = (T) targetEntityObject;
        EHBlock<T> EHBlockPrototype = new EHBlock<>(ehFileConfig.numberOfEntitiesInMainBlock, entityPrototype);
        EHOverflowBlock<T> overflowBlockPrototype = new EHOverflowBlock<>(ehFileConfig.numberOfEntitiesInOverflowBlock, entityPrototype, true);

        this.ehPrototypeHolder = new EHPrototypeHolder<>(entityPrototype, EHBlockPrototype, overflowBlockPrototype);

        this.directory = ehFileConfig.directoryLines;

        this.ehFileHandler = new EHFileHandler(ehFileConfig.databaseName, false);

        this.ehBlockManager = new EHBlockManager<>(
                this.ehFileHandler, this.ehPrototypeHolder,
                ehFileConfig.numberOfUsedMainBlocks, ehFileConfig.numberOfUsedOverflowBlocks,
                this.ehPrototypeHolder.getEHBlockPrototype().getBlockSize(), this.ehPrototypeHolder.getOverflowBlockPrototype().getBlockSize(),
                ehFileConfig.freeBlocks, ehFileConfig.freeOverflowBlocks);

    }


    /**
     * Nájde entitu podľa predanej predlohy
     * @param keyEntity - kľúčová entita pre nájdenie
     * @return - nájdená entita alebo <code>NULL</code> v prípade nenájdenia
     */
    public T find(T keyEntity) {

        // Získam si CustomBitSet na aktuálnom D úrovni
        CustomBitSet CustomBitSetAtActualDLevel = BitSetUtils.getBitsOfInt(keyEntity.getHash(), this.D);

        // Získam si z predošlého CustomBitSetu jeho číslelnú reprezentáciu (index v adresári)
        int indexOfBlockInDirectory = BitSetUtils.fromCustomBitSet(CustomBitSetAtActualDLevel);

        // Hodnota, na ktorú smeruje adresár na danom indexe (začiatok bloku)
        int directoryValue = this.directory[indexOfBlockInDirectory].indexInMainFile;

        EHBlock<T> entityEHBlock = this.ehBlockManager.readBlockAt(directoryValue);

        for (T en : entityEHBlock.getEntities()) {
            if (en.equalsOtherEntity(keyEntity) && en.isValid()) {
                return en;
            }
        }
        // Ak sa nenašla entita v hlavnom bloku, prejdú sa ešte overflow bloky
        return this.findInOverflowBlockChain(keyEntity, entityEHBlock);
    }

    /**
     * Prejde celú reťaz overflow blokov a pokúsi sa v nich nájsť
     * vyhľadávanú entitu, ak sa to nepodarí, vráti <code>null</code>
     * @param keyEntity - kľúčová entita pre nájdenie v overflow blokoch
     * @param entityEHBlock - hlavný blok, z ktorého začína chain overflow blokov
     */
    private T findInOverflowBlockChain(T keyEntity, EHBlock<T> entityEHBlock) {
        if (entityEHBlock.getOverflowBlock() == -1) {
            return null;
        }
        EHOverflowBlock<T> overflowBlockDeserialized = this.ehBlockManager.readOverflowBlockAt(entityEHBlock.getOverflowBlock());
        while (true) {
            // Prejdem všetky entity a ak nájdem zhodnú, vrátim ju
            for (T en : overflowBlockDeserialized.getEntities()) {
                if (en.equalsOtherEntity(keyEntity) && en.isValid()) {
                    return en;
                }
            }

            // Ak som prešiel všetky entity a neexistuje ďalší overflow blok viazaný na aktuálny, entita nemá už kde byť nájdená
            if (overflowBlockDeserialized.getNextOverflowBlock() == -1) {
                return null;
            }

            // Do aktuálne spracovávaného bloku si hodím deserializovaný blok, ktorý je viazaný na aktuálny
            overflowBlockDeserialized = this.ehBlockManager.readOverflowBlockAt(overflowBlockDeserialized.getNextOverflowBlock());
        }
    }

    /**
     * Vloží do adresára + súboru entitu
     * @param insertEntity vkladaná entita
     */
    public Boolean insert(T insertEntity) {

        // Získam si CustomBitSet na aktuálnom D úrovni
        CustomBitSet CustomBitSetAtActualDLevel = BitSetUtils.getBitsOfInt(insertEntity.getHash(), this.D);

        // Získam si z predošlého CustomBitSetu jeho číslelnú reprezentáciu (index v adresári)
        int indexOfBlockInDirectory = BitSetUtils.fromCustomBitSet(CustomBitSetAtActualDLevel);

        // Hodnota, na ktorú smeruje adresár na danom indexe (začiatok bloku)
        int directoryValue = this.directory[indexOfBlockInDirectory].indexInMainFile;

        EHBlock<T> entityEHBlock = this.ehBlockManager.readBlockAt(directoryValue);

        // Musia sa prečítať všetky preplň. bloky, kvôli zisteniu, či sa v nich už nenachádza daná entita na vloženie,
        // ale je ich možné neskôr využiť pri už samotnom vkladaní, bez nutnosti ich opätovného načítavania
        List<EHOverflowBlock<T>> overflowBlocks = null;

        // Ak blok už obsahuje túto entitu, jednoducho vráti false
        if (entityEHBlock.contains(insertEntity)) {
            return Boolean.FALSE;
        } else {
            overflowBlocks = this.readAllOverflowBlocksOfMainBlockToInsertEntity(entityEHBlock, insertEntity);
            if (overflowBlocks == null) {
                return Boolean.FALSE;
            }
        }

        Boolean insertedDirectly = entityEHBlock.insertEntity(insertEntity);

        if (!insertedDirectly) {

            boolean notYetInserted = true;
            insertEntity.setIsValid(Boolean.TRUE);

            int blockFileIndexBeingProcessed = directoryValue;
            int actualLevel = this.directory[indexOfBlockInDirectory].localBlockLevel;

            while (notYetInserted) {
                if (actualLevel == this.D) {
                    if (this.D == this.numberOfAdressingBits) {
                        // Príznak, či sa entita vložila do overflow bloku, ak nie, už s takym ID v blokoch entita existovala
                        Boolean insertedIntoOverflowBlock =
                                this.insertEntityToOverflowBlock(insertEntity, entityEHBlock, overflowBlocks, blockFileIndexBeingProcessed, indexOfBlockInDirectory);
                        if (!insertedIntoOverflowBlock) {
                            return Boolean.FALSE;
                        } else {
                            this.directory[indexOfBlockInDirectory].numberOfEntitiesInOverflowBlock++;
                            return Boolean.TRUE;
                        }
                    }
                    this.doubleTheSizeOfDirectory();
                    indexOfBlockInDirectory = indexOfBlockInDirectory * 2;
                }

                // Nájdenie najnižšieho indexu v adresári, ktorý ukazuje na adresu z hodnoty blockFileIndexBeingProcessed
                for (int i = indexOfBlockInDirectory; i >= 0; i--) {
                    if (this.directory[i].indexInMainFile == blockFileIndexBeingProcessed) {
                        indexOfBlockInDirectory = i;
                    } else {
                        break;
                    }
                }

                // Alokovanie nového bloku a updatnutie indexov v adresári
                EHBlock<T> newEHBlock = new EHBlock<>(entityEHBlock.getEntities().size(), this.ehPrototypeHolder.getEntityPrototype());
                int newBlockIndex = this.ehBlockManager.getFirstFreeBlockIndex();

                // Získam si všetky indexy, ktoré ukazujú na rovnakú hodnotu, ako splitovaný blok
                List<Integer> sameDirectoryIndexes = this.getAllIndicesInDirectoryPointingAt(this.directory[indexOfBlockInDirectory].indexInMainFile, indexOfBlockInDirectory);
                int newBlockBitsNumber = actualLevel + 1;
                // Všetkým indexom v adresári, ktoré majú rovnakú hodnotu ako je na danom indexe
                // a zároveň sú znodné na hodnotách prvých niekoľko bitov nastavím hodnotu v adresári smerujúcu na nový blok
                int firstBlockThatDoesNotEqualIndex = -1;
                for (Integer dirIndex : sameDirectoryIndexes) {
                    boolean equalsOnFirstBits = BitSetUtils.equalsOmitLastNBits(indexOfBlockInDirectory, dirIndex, this.D - newBlockBitsNumber);
                    if (equalsOnFirstBits) {
                        this.directory[dirIndex].indexInMainFile = newBlockIndex;
                    } else {
                        if (firstBlockThatDoesNotEqualIndex == -1) {
                            firstBlockThatDoesNotEqualIndex = dirIndex;
                        }
                    }
                    this.directory[dirIndex].localBlockLevel++;
                }

                List<Integer> sameDirectoryIndicesAfter = this.getAllIndicesInDirectoryPointingAt(this.directory[indexOfBlockInDirectory].indexInMainFile, indexOfBlockInDirectory);

                List<T> toSortIntoBlocksEntities = new LinkedList<>();
                toSortIntoBlocksEntities.addAll(entityEHBlock.getEntities().stream().filter(EHEntity::isValid).collect(Collectors.toList()));


                List<T> firstBlockEntities = new LinkedList<>();
                List<T> secondBlockEntities = new LinkedList<>();

                // Rozdelenie pôvodného bloku do starého + novovytvoreného
                for (T entToSort : toSortIntoBlocksEntities) {
                    CustomBitSet cbs = BitSetUtils.getBitsOfInt(entToSort.getHash(), this.D);
                    int newIndex = BitSetUtils.fromCustomBitSet(cbs);
                    if (sameDirectoryIndicesAfter.contains(newIndex)) {
                        firstBlockEntities.add(entToSort);
                    } else {
                        secondBlockEntities.add(entToSort);
                    }
                }

                boolean insertedAfterSplit = false;
                // Nová entita, ktorá má byť vložená, keď už bol vykonaný split
                CustomBitSet cbs = BitSetUtils.getBitsOfInt(insertEntity.getHash(), this.D);
                int newIndex = BitSetUtils.fromCustomBitSet(cbs);

                boolean blockThatWasOverflownWasBlockWithHigherBit = false;

                if (sameDirectoryIndicesAfter.contains(newIndex)) {
                    if (firstBlockEntities.size() != this.ehPrototypeHolder.getEHBlockPrototype().getNumberOfEntitiesInBlock()) {
                        firstBlockEntities.add(insertEntity);
                        insertedAfterSplit = true;
                    }
                } else {
                    if (secondBlockEntities.size() != this.ehPrototypeHolder.getEHBlockPrototype().getNumberOfEntitiesInBlock()) {
                        secondBlockEntities.add(insertEntity);
                        insertedAfterSplit = true;
                    }
                    blockThatWasOverflownWasBlockWithHigherBit = true;
                }

                entityEHBlock.setEntities(firstBlockEntities);
                newEHBlock.setEntities(secondBlockEntities);

                int block1ToWriteAtIndex;
                int block2ToWriteAtIndex;
                if (indexOfBlockInDirectory <= firstBlockThatDoesNotEqualIndex) {
                    block1ToWriteAtIndex = indexOfBlockInDirectory;
                    block2ToWriteAtIndex = firstBlockThatDoesNotEqualIndex;
                } else {
                    block1ToWriteAtIndex = firstBlockThatDoesNotEqualIndex;
                    block2ToWriteAtIndex = indexOfBlockInDirectory;
                }

                this.ehBlockManager.saveBlock(entityEHBlock, this.directory[block1ToWriteAtIndex].indexInMainFile);
                this.ehBlockManager.saveBlock(newEHBlock, this.directory[block2ToWriteAtIndex].indexInMainFile);

                List<Integer> allIndicesTargetingBlock1 = this.getAllIndicesInDirectoryPointingAt(this.directory[block1ToWriteAtIndex].indexInMainFile, block1ToWriteAtIndex);
                long block1ToWriteValidCount = entityEHBlock.countValidEntities();
                for (Integer dirIndex : allIndicesTargetingBlock1) {
                    this.directory[dirIndex].numberOfEntitiesInMainBlock = block1ToWriteValidCount;
                }

                List<Integer> allIndicesTargetingBlock2 = this.getAllIndicesInDirectoryPointingAt(this.directory[block2ToWriteAtIndex].indexInMainFile, block2ToWriteAtIndex);
                long block2ToWriteValidCount = newEHBlock.countValidEntities();
                for (Integer dirIndex : allIndicesTargetingBlock2) {
                    this.directory[dirIndex].numberOfEntitiesInMainBlock = block2ToWriteValidCount;
                }

                if (blockThatWasOverflownWasBlockWithHigherBit) {
                    entityEHBlock = newEHBlock;
                    blockFileIndexBeingProcessed = this.directory[block2ToWriteAtIndex].indexInMainFile;
                    indexOfBlockInDirectory = block2ToWriteAtIndex;
                } else {
                    blockFileIndexBeingProcessed = this.directory[block1ToWriteAtIndex].indexInMainFile;
                }

                actualLevel++;
                if (insertedAfterSplit) {
                    notYetInserted = false;
                }
            }
        } else {
            // Iba sa uloží blok naspäť do súboru
            this.ehBlockManager.saveBlock(entityEHBlock, directoryValue);

            //Všetkým indexom, ktoré ale ukazujú na tento blok navýš hodnotu počtu entít v main bloku
            //Optimalizovanejšie riešenie, kde sa dané indexy vyhľadajú len vpravo/vľavo od nejakého
            //základného indexu, a ak sa na niektorom ďalšom indexe hodnota nerovná, prehliadka skončí
            // ==> neprehľadá teda celý directory, iba vpravo a vľavo až kým nenarazí na inú hodnotu
            for (int i = indexOfBlockInDirectory; i < this.directory.length; i++) {
                if (this.directory[i].indexInMainFile.equals(directoryValue)) {
                    this.directory[i].numberOfEntitiesInMainBlock++;
                } else {
                    break;
                }
            }
            for (int i = indexOfBlockInDirectory - 1; i >= 0; i--) {
                if (this.directory[i].indexInMainFile.equals(directoryValue)) {
                    this.directory[i].numberOfEntitiesInMainBlock++;
                } else {
                    break;
                }
            }
        }
        return Boolean.TRUE;
    }

    /**
     * Načíta a vráti všetky preplň. bloky
     * hlavného súboru, predaného v parametri,
     * prípadne skončí čítanie blokov ak narazí
     * na entitu, ktorá sa má do reťaze vložiť
     *
     * Vykoná teda MAX(N) blokových načítaní,
     * pričom N je dĺžka reťaze preplň. blokov
     *
     * @param ofBlock - Z ktorého hlavného bloku začína reťaz
     * @param entityToInsert - Ktorá entita sa bude mať vložiť do reťaze (ak sa priebežne nájde v niektorom bloku, čítanie môže skončiť)
     * @return reťaz overflow blokov začínajúcu od hlavného bloku predaného z parametra
     *         alebo <code>NULL</code> ak sa entita priebežne našla v niektorom načítanom bloku
     */
    private List<EHOverflowBlock<T>> readAllOverflowBlocksOfMainBlockToInsertEntity(EHBlock<T> ofBlock, T entityToInsert) {
        List<EHOverflowBlock<T>> toRet = new LinkedList<>();
        Integer indexOfOb = ofBlock.getOverflowBlock();
        while (indexOfOb != -1) {
            EHOverflowBlock<T> bl = this.ehBlockManager.readOverflowBlockAt(indexOfOb);
            if (bl.contains(entityToInsert)) {
                return null;
            }
            toRet.add(bl);
            indexOfOb = bl.getNextOverflowBlock();
        }
        return toRet;
    }

    /**
     * Vloží entitu do overflow bloku predaného bloku,
     * resp. niekde do reťaze overflow blokov
     *
     * Nerobí už žiadne ready, overflow bloky prevezme už načítané z predošlej operácie
     *
     * Postará sa aj o prípadne´vytvorenie nového overflow bloku
     *
     * @param insertEntity          - Entita pre vloženie do overflow bloku
     * @param entityEHBlock         - Blok, od ktorého sa začína vkladanie do overflow blokov
     * @param obs                   - Overflow bloky načítané v pamäti, ktoré odstránia nutnosť nového načítania z disku
     * @param addressOfEntityBlock  - Adresa hlavného bloku, z ktorého začína reťaz
     * @param indexOfBlock          - Index bloku v adresári, z ktorého začína reťaz
     */
    private Boolean insertEntityToOverflowBlock(T insertEntity, EHBlock<T> entityEHBlock, List<EHOverflowBlock<T>> obs, Integer addressOfEntityBlock, Integer indexOfBlock) {

        if (obs.size() > 0) {

            EHOverflowBlock<T> previous = null;
            Integer previousIndex = -1;
            Integer actualIndex = entityEHBlock.getOverflowBlock();
            for (EHOverflowBlock<T> ob : obs) {
                Boolean inserted = ob.insertEntity(insertEntity);
                if (inserted) {
                    // Prípad, kedy sa entita vloží do prvého bloku po hlavnom bloku
                    this.ehBlockManager.saveOverflowBlock(ob, actualIndex);
                    return Boolean.TRUE;
                }
                previous = ob;
                previousIndex = actualIndex;
                actualIndex = ob.getNextOverflowBlock();
            }

            // Ak sa dostane sem, nepodarilo sa entitu vložiť ani do jedného z OB z reťaze, treba vytvoriť nový,
            // previazať posledný s novým a uložiť oba
            EHOverflowBlock<T> newOverflowBlock =
                    new EHOverflowBlock<>(
                            this.ehPrototypeHolder.getOverflowBlockPrototype().getNumberOfEntitiesInBlock(),
                            this.ehPrototypeHolder.getEntityPrototype(),
                            true);
            newOverflowBlock.insertEntity(insertEntity);
            Integer newBlockAddress = this.ehBlockManager.getFirstFreeOverflowBlockIndex();

            // Zreferencuj "starý" overflow blok s novým a "preulož" starý s touto referenciou
            previous.setNextOverflowBlock(newBlockAddress);
            this.ehBlockManager.saveOverflowBlock(previous, previousIndex);

            // Ulož nový overflow block
            this.ehBlockManager.saveOverflowBlock(newOverflowBlock, newBlockAddress);
            this.directory[indexOfBlock].numberOfOverflowBlocks++;
            return Boolean.TRUE;

        } else {
            // Tu sa musí vytvoriť nový overflow blok, pretože hlavný blok ho nemal,
            // hlavnému bloku sa musí setnúť index na tento novo vytvorený overflow blok,
            // hlavný blok sa teda musí znova "preuložiť" a následne sa uloží aj overflow blok
            EHOverflowBlock<T> newOverflowBlock =
                    new EHOverflowBlock<>(
                            this.ehPrototypeHolder.getOverflowBlockPrototype().getNumberOfEntitiesInBlock(),
                            this.ehPrototypeHolder.getEntityPrototype(),
                            true);
            Integer overflowBlockAddress = this.ehBlockManager.getFirstFreeOverflowBlockIndex();

            newOverflowBlock.insertEntity(insertEntity);

            // Zreferencovanie hlavného a nového overflow bloku a "preuloženie" hlavného bloku
            entityEHBlock.setOverflowBlock(overflowBlockAddress);
            this.ehBlockManager.saveBlock(entityEHBlock, addressOfEntityBlock);

            // Zapísanie nového owerflow bloku do overflow súboru
            this.ehBlockManager.saveOverflowBlock(newOverflowBlock, overflowBlockAddress);

            this.directory[indexOfBlock].numberOfOverflowBlocks++;
        }
        return Boolean.TRUE;
    }

    /**
     * Vymaže entitu z EH pokiaľ tam taká existuje
     * a vráti ju ako return hodnotu
     *
     * @param deleteEntity - kľúčová entita na zmazanie
     * @return - vymazaná entita alebo <code>NULL</code> ak entita nebola nájdená
     */
    public T delete(T deleteEntity) {

        // Nájdem si blok, v ktorom sa entita nachádza
        CustomBitSet customBitSetAtActualDLevel = BitSetUtils.getBitsOfInt(deleteEntity.getHash(), this.D);
        int indexOfBlockInDirectory = BitSetUtils.fromCustomBitSet(customBitSetAtActualDLevel);

        int indexOfBlockInDirectoryValue = this.directory[indexOfBlockInDirectory].indexInMainFile;

        EHBlock<T> block = this.ehBlockManager.readBlockAt(indexOfBlockInDirectoryValue);

        // Skúsi entitu znevalidovať v hlavnom bloku
        T invalidatedInMainBlock = block.invalidateEntity(deleteEntity);
        if (invalidatedInMainBlock != null) {
            // Entita bola znevalidovaná v hlavnom bloku, zapíše sa hlavný blok a vráti sa takto znevalidovaná entita
            this.ehBlockManager.saveBlock(block, indexOfBlockInDirectoryValue);
            this.directory[indexOfBlockInDirectory].numberOfEntitiesInMainBlock--;

            // Zníženenie hodnoty počtu entít v danom bloku na každom idexe adresára, ukazujúceho na rovnaký blok
            for (int i = indexOfBlockInDirectory + 1; i < this.directory.length; i++) {
                if (this.directory[i].indexInMainFile == indexOfBlockInDirectoryValue) {
                    this.directory[i].numberOfEntitiesInMainBlock--;
                } else {
                    break;
                }
            }
            for (int i = indexOfBlockInDirectory - 1; i >= 0; i--) {
                if (this.directory[i].indexInMainFile == indexOfBlockInDirectoryValue) {
                    this.directory[i].numberOfEntitiesInMainBlock--;
                } else {
                    break;
                }
            }


            // Ak blok mal aj overflow blok(y), overí sa, či nie je možné entity striasť
            if (block.getOverflowBlock() != -1) {
                this.consolidateBlock(indexOfBlockInDirectory, block);
            }

            this.ehBlockManager.consolidateOverflowFile();

            // Teraz ešte ak je to možné, pospájam bloky v hlavnom súbore za účelom uvoľnenia bloku na disku, príp. skrátim directory
            if (this.D > 1) {
                this.mergeBlocksIfPossible(indexOfBlockInDirectory, block);
            }
            this.ehBlockManager.consolidateMainBlocks();

            return invalidatedInMainBlock;
        }
        // Ak blok nemá overflow blok, entita neexistuje
        if (block.getOverflowBlock() == -1) {
            return null;
        }

        EHOverflowBlock<T> blockPrevious = null;
        Integer blockIndexPrevious = null;
        EHOverflowBlock<T> blockActual = this.ehBlockManager.readOverflowBlockAt(block.getOverflowBlock());
        Integer blockIndexActual = block.getOverflowBlock();
        while (blockActual != null) {
            T invalidatedEntInOverflowBlock = blockActual.invalidateEntity(deleteEntity);
            // Ak sa entitu podarilo vymazať
            if (invalidatedEntInOverflowBlock != null) {
                // Previous blok je null, ak som entitu zmazal z hneď prvého overflow bloku po hlavnom bloku
                if (blockPrevious == null) {
                    if (blockActual.isEmpty()) {
                        // Ak sa overflow blok vyprázdnil, hlavnému bloku treba nastaviť odkaz na overflow blok na ďalší z reťaze,
                        // vyprázdený overflow blok uvoľniť
                        this.ehBlockManager.freeOverflowBlock(blockIndexActual);
                        this.directory[indexOfBlockInDirectory].numberOfOverflowBlocks--;
                        block.setOverflowBlock(blockActual.getNextOverflowBlock());
                        this.ehBlockManager.saveBlock(block, indexOfBlockInDirectoryValue);
                    } else {
                        // Len zapíš overflow blok s jednou invalidovanou entitou
                        this.ehBlockManager.saveOverflowBlock(blockActual, blockIndexActual);
                    }
                } else {
                    // Tu už nebola entita vyhodená z overflow bloku nasledujúcom po hlavnom bloku
                    if (blockActual.isEmpty()) {
                        // Ak sa overflow blok vyprázdnil, predošlému bloku nastav adresu ďalšieho bloku na aktuálny.nextOverflowBlock
                        blockPrevious.setNextOverflowBlock(blockActual.getNextOverflowBlock());
                        this.ehBlockManager.saveOverflowBlock(blockPrevious, blockIndexPrevious);
                        // Musí sa zapísať ešte aj aktuálne vyprázdnený blok (aby tam neostala nezapísaná entita - lebo zatiaľ je invalidovaná len v pamäti)
                        this.ehBlockManager.saveOverflowBlock(blockActual, blockIndexActual);
                        // Vyprázdnený blok sa môže uvoľniť
                        this.ehBlockManager.freeOverflowBlock(blockIndexActual);
                        this.directory[indexOfBlockInDirectory].numberOfOverflowBlocks--;
                    } else {
                        // Len zapíš overflow blok s jednou znevalidovanou entitou
                        this.ehBlockManager.saveOverflowBlock(blockActual, blockIndexActual);
                    }
                }
                this.ehBlockManager.consolidateOverflowFile();
                this.directory[indexOfBlockInDirectory].numberOfEntitiesInOverflowBlock--;
                return invalidatedEntInOverflowBlock;
            }
            // Ak nie, aktuálneho bloku načítaj ďalší overfl. blok
            if (blockActual.getNextOverflowBlock() != -1) {
                blockPrevious = blockActual;
                blockIndexPrevious = blockIndexActual;
                blockIndexActual = blockActual.getNextOverflowBlock();
                blockActual = this.ehBlockManager.readOverflowBlockAt(blockActual.getNextOverflowBlock());
            } else {
                // Entitu sa nepodarilo nájsť
                return null;
            }
        }

        return null;
    }

    /**
     * Updatne údaje o entite a to tak,
     * že nájde blok/overflow blok, v ktorom sa daná entita nachádza,
     * entitu v tomto bloku nahradí novými dátami
     * a spraví zápis do súboru
     *
     * Ušetrí jeden zápis do súboru oproti updatnutiu na štýl
     * DELETE(t) ---> INSERT(t),
     * pretože tu je zápis invalidated entity z DELETE(t) operácie
     * kvázi preskočený a blok sa zapíše do súboru len raz
     * a to už s updatnutou entitou
     *
     * @param entityToUpdate
     * @return - Príznak, či sa entitu podarilo updatnúť
     */
    public Boolean update(T entityToUpdate) {

        // Získam si CustomBitSet na aktuálnom D úrovni
        CustomBitSet CustomBitSetAtActualDLevel = BitSetUtils.getBitsOfInt(entityToUpdate.getHash(), this.D);

        // Získam si z predošlého CustomBitSetu jeho číslelnú reprezentáciu (index v adresári)
        int indexOfBlockInDirectory = BitSetUtils.fromCustomBitSet(CustomBitSetAtActualDLevel);

        // Hodnota, na ktorú smeruje adresár na danom indexe (začiatok bloku)
        int directoryValue = this.directory[indexOfBlockInDirectory].indexInMainFile;

        EHBlock<T> entityEHBlock = this.ehBlockManager.readBlockAt(directoryValue);

        boolean invalidated = entityEHBlock.tryInvalidateEntity(entityToUpdate);

        // Ak sa entitu podarilo vymazať z hlavného bloku, naspäť vložím entitu s novými dátami
        if (invalidated) {
            entityEHBlock.insertEntity(entityToUpdate);
            this.ehBlockManager.saveBlock(entityEHBlock, directoryValue);
            return Boolean.TRUE;
        }

        // Ak blok nemá overflow bloky, entita neexistuje, nemá sa čo updatnúť
        if (entityEHBlock.getOverflowBlock() == -1) {
            return Boolean.FALSE;
        }

        // Nájdem entitu v overflow blokoch a ak ju nájdem, nahradím ju v OB updatovanou hodnotou a OB zapíšem
        Integer obIndex = entityEHBlock.getOverflowBlock();
        while (obIndex != -1) {
            EHOverflowBlock<T> ob = this.ehBlockManager.readOverflowBlockAt(entityEHBlock.getOverflowBlock());
            Boolean invalidatedOb = ob.tryInvalidateEntity(entityToUpdate);
            // Ak sa entitu podarilo vyhodiť z entít OB, vloží sa naspäť updatovaná a blok sa uloží
            if (invalidatedOb) {
                ob.insertEntity(entityToUpdate);
                this.ehBlockManager.saveOverflowBlock(ob, obIndex);
                return Boolean.TRUE;
            } else {
                obIndex = ob.getNextOverflowBlock();
            }
        }

        // Ak sa entitu nepodarilo updatnúť (nebola nájdená ani v OB reťazi)
        return Boolean.FALSE;
    }

    /**
     * Cyklicky spája bloky so susedom ak je to možné,
     * čím zníži lok. level bloku
     * a jeden z týchto blokov sa môže uvolniť
     *
     * Ak blok bol posledný s touto hĺbkou && táto hĺbka sa rovnala D
     * môže sa znížiť aj veľkosť adresára na polovicu
     *
     * @param indexOfBlockInDirectory
     * @param block
     */
    private void mergeBlocksIfPossible(int indexOfBlockInDirectory, EHBlock<T> block) {

        while (this.D >= 2) {
            int actualLevel = this.directory[indexOfBlockInDirectory].localBlockLevel;
            // Zistenie indexu suseda
            int neighborIndex = BitSetUtils.invertBit(indexOfBlockInDirectory, this.D - actualLevel);

            // Ak blok má nejaké overflow bloky, NEVYKONÁ sa spájanie
            if (this.directory[indexOfBlockInDirectory].numberOfOverflowBlocks > 0) {
                return;
            }

            // Ak sused má nejaké overflow bloky, NEVYKONÁ sa spájanie
            if (this.directory[neighborIndex].numberOfOverflowBlocks > 0) {
                return;
            }

            // To či môžem bloky spojiť určuje tiež fakt, či majú rovnkaký lok. level
            int neighborLevel = this.directory[neighborIndex].localBlockLevel;
            if (actualLevel != neighborLevel) {
                return;
            }

            // Tu sa levely oboch blokov rovnajú, zistím si, či sa entity z týchto blokov dajú vložiť do jedného
            long numberOfEntsInFirstBlock = this.directory[indexOfBlockInDirectory].numberOfEntitiesInMainBlock;
            long numberOfEntsInSecondBlock = this.directory[neighborIndex].numberOfEntitiesInMainBlock;

            // Entity sa nezmestia do jedného bloku, nemôže sa teda vykonať spojenie blokov
            if ((numberOfEntsInFirstBlock + numberOfEntsInSecondBlock) > this.ehPrototypeHolder.getEHBlockPrototype().getNumberOfEntitiesInBlock()) {
                return;
            }

            // Načítanie susedného bloku bloku zo súboru
            EHBlock<T> blockToMerge = this.ehBlockManager.readBlockAt(this.directory[neighborIndex].indexInMainFile);

            // Blok na uvoľnenie bude ten, ktorý má vyššiu adresu v súbore kvôli väčšej pravdepodobnosti zmenšenia súbpru
            boolean mainBlockHadLowerAddress = this.directory[indexOfBlockInDirectory].indexInMainFile < this.directory[neighborIndex].indexInMainFile;

            // Entity zo suseda prelejem do hlavného bloku
            blockToMerge.getEntities().stream().filter(EHEntity::isValid).forEach(block::insertEntity);
            blockToMerge.clear();

            // Zapíšem oba bloky, aj ten do ktorého sa naliali entity, aj ten ktorý bol vyčistený, aby pri budúcom načítaní nespôsobil problémy
            if (mainBlockHadLowerAddress) {
                this.ehBlockManager.saveBlock(block, this.directory[indexOfBlockInDirectory].indexInMainFile);
                this.ehBlockManager.saveBlock(blockToMerge, this.directory[neighborIndex].indexInMainFile);
                this.ehBlockManager.freeBlock(this.directory[neighborIndex].indexInMainFile);
            } else {
                this.ehBlockManager.saveBlock(block, this.directory[neighborIndex].indexInMainFile);
                this.ehBlockManager.saveBlock(blockToMerge, this.directory[indexOfBlockInDirectory].indexInMainFile);
                this.ehBlockManager.freeBlock(this.directory[indexOfBlockInDirectory].indexInMainFile);
            }

            // Poupdatovanie hodnôt v adresári
            List<Integer> allIndicesTargetingMergedBlock = new LinkedList<>();
            Integer blockIndexValue = this.directory[indexOfBlockInDirectory].indexInMainFile;
            Integer mergeBlockIndexValue = this.directory[neighborIndex].indexInMainFile;

            // Prejdem adresár a nájdem prvky, ktoré smerovali buď na hlavný blok, alebo blok, ktorý sa mergoval do hlavného, vice versa
            for (int i = 0; i < this.directory.length; i++) {
                if (this.directory[i].indexInMainFile.equals(blockIndexValue)) {
                    allIndicesTargetingMergedBlock.add(i);
                }
                if (this.directory[i].indexInMainFile.equals(mergeBlockIndexValue)) {
                    allIndicesTargetingMergedBlock.add(i);
                }
            }

            // Všetkým takým indexom nastavím adresu na mergnutý blok
            if (mainBlockHadLowerAddress) {
                for (Integer e : allIndicesTargetingMergedBlock) {
                    this.directory[e].indexInMainFile = this.directory[indexOfBlockInDirectory].indexInMainFile;
                    this.directory[e].localBlockLevel--;
                    this.directory[e].numberOfEntitiesInMainBlock = block.countValidEntities();
                }
            } else {
                for (Integer e : allIndicesTargetingMergedBlock)  {
                    this.directory[e].indexInMainFile = this.directory[neighborIndex].indexInMainFile;
                    this.directory[e].localBlockLevel--;
                    this.directory[e].numberOfEntitiesInMainBlock = block.countValidEntities();
                }
            }

            // Získam si max blok level v adresári
            int maxBlockLevel = 0;
            for (int i = 0; i < this.directory.length; i++) {
                if (this.directory[i].localBlockLevel > maxBlockLevel) {
                    maxBlockLevel = this.directory[i].localBlockLevel;
                }
            }

            // Ak po mergnutí vznikla situácia, že max level je nižší ako
            if (maxBlockLevel < actualLevel) {
                this.halveTheSizeOfDirectory();
                indexOfBlockInDirectory = indexOfBlockInDirectory / 2;
            }

        }
    }

    /**
     * Skonsoliduje/Utrasie blok tak,
     * že načíta všetky jeho overflow bloky
     * a potom ich spätne naplní a uloží za účelom
     * uvoľnenia aspoň jedného overflow bloku
     *
     * Či sa táto operácia vôbec oplatí, si overí pomocou výpočtu
     *
     * @param indexOfBlockInDirectory - Index v adresár na ktorom sa nachádza hlavný blok
     * @param block - Samotný hlavný blok
     */
    private void consolidateBlock(int indexOfBlockInDirectory, EHBlock<T> block) {
        // Počítania ohľadom striasania overflow blokov do hlavného bloku
        long numberOfOverflowBlocks = this.directory[indexOfBlockInDirectory].numberOfOverflowBlocks;
        long numberOfEntitiesInMainBlock = this.directory[indexOfBlockInDirectory].numberOfEntitiesInMainBlock;
        long numberOfEntitiesInOverflowBlock = this.directory[indexOfBlockInDirectory].numberOfEntitiesInOverflowBlock;
        long countIfMerged = numberOfEntitiesInMainBlock + numberOfEntitiesInOverflowBlock;
        // Počet entít, ktorý by bolo možné rozhodiť do blokov, keby sa jeden overflow blok dealokoval
        long capacityOfBlocksWithOneFreedOverflowBlock =
                this.ehPrototypeHolder.getEHBlockPrototype().getNumberOfEntitiesInBlock() +
                        this.ehPrototypeHolder.getOverflowBlockPrototype().getNumberOfEntitiesInBlock() * (numberOfOverflowBlocks - 1);
        // Ak kapacita blokov s aspoň jedným uvoľneným overflow blokom postačuje na rozhodenie entít, vykoná sa strasenie



        if (capacityOfBlocksWithOneFreedOverflowBlock >= countIfMerged) {
            LinkedList<T> entitiesToConsolidate = new LinkedList<>();
            entitiesToConsolidate.addAll(block.getEntities().stream().filter(EHEntity::isValid).collect(Collectors.toList()));
            block.clear();

            LinkedList<IndexToOverflowBlock<T>> overflowBlocks = new LinkedList<>();
            Integer nextOverflowBlock = block.getOverflowBlock();
            while (!nextOverflowBlock.equals(-1)) {
                EHOverflowBlock<T> ob = this.ehBlockManager.readOverflowBlockAt(nextOverflowBlock);
                entitiesToConsolidate.addAll(ob.getEntities().stream().filter(EHEntity::isValid).collect(Collectors.toList()));
                ob.clear();
                overflowBlocks.add(new IndexToOverflowBlock<>(nextOverflowBlock, ob));
                nextOverflowBlock = ob.getNextOverflowBlock();
            }

            // Usporiadam si overflow bloky podľa adries od najmenšej po najväčšiu kvôli vypĺňaniu od najnižšieho
            // a to z jednoduchého dôvodu, že sa uvoľní blok s najvyššiou hodnotou indexu (najďalej v súbore),
            // teda sa zvyšuje šanca na krátenie súboru
            List<IndexToOverflowBlock<T>> sorted =
                    overflowBlocks.stream()
                            .sorted(Comparator.comparing(IndexToOverflowBlock::getIndexOfBlock)).collect(Collectors.toList());

            ArrayList<IndexToOverflowBlock<T>> used = new ArrayList<>();

            // Teraz vyplním entitami najprv hlavný blok, potom overflow bloky jednoduchým vyťahovaním zo sortnutého listu
            EHOverflowBlock<T> beingFilled = null;
            long mainBlockEntitiesCounter = 0;
            long overflowBlockEntitiesCounter = 0;
            while (!entitiesToConsolidate.isEmpty()) {
                T en = entitiesToConsolidate.removeFirst();

                // Ak vypĺňam normálny blok
                if (beingFilled == null) {
                    boolean insertedToMain = block.insertEntity(en);
                    if (!insertedToMain) {
                        IndexToOverflowBlock<T> pop = sorted.remove(0);
                        beingFilled = pop.getOverflowBlock();
                        used.add(pop);
                        entitiesToConsolidate.add(en);
                        continue;
                    } else {
                        mainBlockEntitiesCounter++;
                        continue;
                    }
                }
                boolean insertedToActualOb = beingFilled.insertEntity(en);
                if (!insertedToActualOb) {
                    IndexToOverflowBlock<T> pop = sorted.remove(0);
                    beingFilled = pop.getOverflowBlock();
                    used.add(pop);
                    entitiesToConsolidate.add(en);
                } else {
                    overflowBlockEntitiesCounter++;
                }
            }

            // Poukladám si všetky bloky (hlavný blok, overflow bloky použité ale aj nepoužité kvôli uloženiu INVALID entít v nich)
            // Zavolám uvoľnenie všetkých nepoužitých overflow blokov

            // Bloku sa nastaví OB prvý z reťaze, alebo -1 ak bola reťaz použitých prázdna
            if (used.size() > 0) {
                block.setOverflowBlock(used.get(0).getIndexOfBlock());
            } else {
                block.setOverflowBlock(-1);
            }
            // Uloženie hlavného bloku
            this.ehBlockManager.saveBlock(block, this.directory[indexOfBlockInDirectory].indexInMainFile);

            // Uloženie použitých blokov
            for (int i = 0; i < used.size(); i++) {
                // Bloky treba nanovo "prereťaziť", t.j. nanovo ponastavovať indexy na ďalšie OB, pričom poslednému ju nastaviť na -1
                if (i == used.size() - 1) {
                    used.get(i).getOverflowBlock().setNextOverflowBlock(-1);
                } else {
                    used.get(i).getOverflowBlock().setNextOverflowBlock(used.get(i+1).getIndexOfBlock());
                }

                // Potom blok uložiť
                this.ehBlockManager.saveOverflowBlock(used.get(i).getOverflowBlock(), used.get(i).getIndexOfBlock());
            }

            // Uloženie nepoužitých blokov + ich uvoľnenie
            sorted.forEach(e -> {
                // Tu sa ešte setne nextOverflow na -1, ak sa bude OB znova používať
                e.getOverflowBlock().setNextOverflowBlock(-1);
                this.ehBlockManager.saveOverflowBlock(e.getOverflowBlock(), e.getIndexOfBlock());
                this.ehBlockManager.freeOverflowBlock(e.getIndexOfBlock());
            });

            // Nastavenie potrebných hodnôt na danom indexe
            this.directory[indexOfBlockInDirectory].numberOfOverflowBlocks = (long)used.size();
            this.directory[indexOfBlockInDirectory].numberOfEntitiesInMainBlock = mainBlockEntitiesCounter;
            this.directory[indexOfBlockInDirectory].numberOfEntitiesInOverflowBlock = overflowBlockEntitiesCounter;

        }
    }

    /**
     * Nájde všetky indexy z adresára,
     * ktoré ukazujú na rovnakú hodnotu,
     * ako index z parametra
     *
     * Od indexu
     * @param fromIndex
     * algoritmus prejde všetky indexy menšie (vľavo) v poli,
     * a väčšie (vpravo) v poli a do výsledného zoznamu
     * pridá hodnoty ktoré sú zhodné s hodnotou z parametra
     * @param pointingAt
     *
     * @param pointingAt - hodnota, voči ktorej sa porovnáva
     * @param fromIndex - počiatočný index, od ktorého sa hľadajú hodnoty
     * @return
     */
    private List<Integer> getAllIndicesInDirectoryPointingAt(Integer pointingAt, Integer fromIndex) {
        // Indexy napravo (vrátane) základného indexu
        List<Integer> indexes = new ArrayList<>();
        for (int i = fromIndex; i < this.directory.length; i++) {
            if (this.directory[i].indexInMainFile.equals(pointingAt)) {
                indexes.add(i);
            } else {
                break;
            }
        }
        // Indexy naľavo od základného indexu
        for (int i = fromIndex - 1; i >= 0; i--) {
            if (this.directory[i].indexInMainFile.equals(pointingAt)) {
                indexes.add(i);
            } else {
                break;
            }
        }

        return indexes;
    }

    /**
     * Zväčší 100%ne veľkosť adresára s tým,
     * že do druhej polovice nakopíruje hodnoty z prvej polovice
     */
    private void doubleTheSizeOfDirectory() {
        EHDirectoryBox[] duplicated = new EHDirectoryBox[this.directory.length * 2];
        for (int i = 0; i < this.directory.length; i++) {
            EHDirectoryBox db0 = new EHDirectoryBox();
            db0.indexInMainFile = this.directory[i].indexInMainFile;
            db0.numberOfEntitiesInMainBlock = this.directory[i].numberOfEntitiesInMainBlock;
            db0.numberOfEntitiesInOverflowBlock = this.directory[i].numberOfEntitiesInOverflowBlock;
            db0.numberOfOverflowBlocks = this.directory[i].numberOfOverflowBlocks;
            db0.localBlockLevel = this.directory[i].localBlockLevel;

            EHDirectoryBox db1 = new EHDirectoryBox();
            db1.indexInMainFile = this.directory[i].indexInMainFile;
            db1.numberOfEntitiesInMainBlock = this.directory[i].numberOfEntitiesInMainBlock;
            db1.numberOfEntitiesInOverflowBlock = this.directory[i].numberOfEntitiesInOverflowBlock;
            db1.numberOfOverflowBlocks = this.directory[i].numberOfOverflowBlocks;
            db1.localBlockLevel = this.directory[i].localBlockLevel;

            duplicated[2*i] = db0;
            duplicated[2*i + 1] = db1;
        }
        this.directory = duplicated;
        this.D++;
    }

    /**
     * Zmenší veľkosť adresára na polovicu,
     * a to tak, že do nového nakopíruje každú druhú hodnotu
     */
    private void halveTheSizeOfDirectory() {
        EHDirectoryBox[] halved = new EHDirectoryBox[this.directory.length / 2];
        for (int i = 0; i < halved.length; i++) {
            halved[i] = this.directory[2*i];
        }
        this.directory = halved;
        this.D--;
    }

    /**
     * Vráti veľkosť hlavného súboru
     * vyrátaním veľkosti jedného bloku
     * krát počet alokovaných blokov
     *
     * @return - veľkosť súboru, aká by MALA byť
     */
    public Integer getMainFileSizeByCalculation() {
        return this.ehBlockManager.getNumberOfCreatedBlocks() * this.ehPrototypeHolder.getEHBlockPrototype().getBlockSize();
    }

    /**
     * Vráti veľkosť hlavného súboru,
     * nie ale vyrátanú, ale reálnu (kvázi file.length)
     */
    public Integer getMainFileSizeByFileLength() {
        return this.ehFileHandler.getMainFileSize().intValue();
    }

    /**
     * @see this#getMainFileSizeByCalculation()
     * Rovnaké, len pre overflow bloky
     *
     * @return - veľkosť overflow súboru aká by MALA byť
     */
    public Integer getOverflowFileSizeByCalculation() {
        return this.ehBlockManager.getNumberOfCreatedOverflowBlocks() * this.ehPrototypeHolder.getOverflowBlockPrototype().getBlockSize();
    }

    /**
     * @see this#getMainFileSizeByFileLength()
     * Rovnaké, len pre overflow bloky
     *
     * @return - reálna veľkosť overflow súboru
     */
    public Integer getOverflowFileSizeByFileLength() {
        return this.ehFileHandler.getOverflowFileSize().intValue();
    }

    public void close() {
        this.ehFileHandler.closeFiles();
    }

    /**
     * Vypíše všetky bloky s internými atribútmi
     * + vypíše aj ich entity
     * (prejde ale celý adresár a naloaduje podľa neho všetky bloky,
     *  čo spôsobí N čítaní zo súboru, kde N je dĺžka adresára)
     */
    public void blockDump() {
        System.out.println("\033[H\033[2J");
        System.out.flush();
        System.out.println("*****************************************");
        for (int i = 0; i < this.directory.length; i++) {
            byte[] blockBytes = this.ehFileHandler.readFromMainFile(this.directory[i].indexInMainFile, this.ehPrototypeHolder.getEHBlockPrototype().getBlockSize());
            EHBlock<T> entityEHBlock = this.ehPrototypeHolder.getEHBlockPrototype().fromBytes(blockBytes);

            System.out.println("Blok " + this.directory[i].indexInMainFile);
            System.out.println("\td: " + this.directory[i].localBlockLevel);
            System.out.println("\tOverflow: " + entityEHBlock.getOverflowBlock());
            System.out.println("\tNumberOfEntities : " + this.directory[i].numberOfEntitiesInMainBlock);
            entityEHBlock.getEntities().forEach(e -> {
                System.out.println("\t\tEntita: " + e.toString());
            });

            System.out.println("--------------------------------------------");
        }
    }


    public EHFileDescriptor getDescription() {
        return new EHFileDescriptor(
                this.D,
                this.numberOfAdressingBits,
                this.ehPrototypeHolder.getEHBlockPrototype().getNumberOfEntitiesInBlock(),
                this.ehPrototypeHolder.getOverflowBlockPrototype().getNumberOfEntitiesInBlock(),
                this.ehBlockManager.getNumberOfCreatedBlocks(),
                this.ehBlockManager.getNumberOfCreatedOverflowBlocks(),
                this.ehFileHandler.getMainFileSize().intValue(),
                this.ehFileHandler.getOverflowFileSize().intValue(),
                this.ehBlockManager.getFreeBlocks(),
                this.ehBlockManager.getFreeOverflowBlocks()
        );
    }

    /**
     * Vráti všetky validne entity zo súboru,
     * musí preto načítať všetky hlavné bloky
     * a všetky overflow bloky
     * a do výsledku pridať len validné entity
     *
     * @return - len validne entity v súbore
     */
    public List<T> getAllValidEntities() {

        List<T> entities = new LinkedList<>();

        // Aby nenačítavalo medzi validné entity po 2+ krát, ak na blok ukazuje niektorý index z adresára viackrát
        List<Integer> alreadyCollected = new LinkedList<>();
        for (int i = 0; i < this.directory.length; i++) {
            if (alreadyCollected.contains(this.directory[i].indexInMainFile)) {
                continue;
            }
            alreadyCollected.add(this.directory[i].indexInMainFile);
            EHBlock<T> block = this.ehBlockManager.readBlockAt(this.directory[i].indexInMainFile);
            entities.addAll(block.getEntities().stream().filter(EHEntity::isValid).collect(Collectors.toList()));
        }


        for (int i = 0; i < this.ehBlockManager.getNumberOfCreatedOverflowBlocks(); i++) {
            Integer obSize = this.ehPrototypeHolder.getOverflowBlockPrototype().getBlockSize();
            EHOverflowBlock<T> block = this.ehBlockManager.readOverflowBlockAt(i * obSize);
            entities.addAll(block.getEntities().stream().filter(EHEntity::isValid).collect(Collectors.toList()));
        }

        return entities;
    }


    public List<EHBlockDescription> getBlockDescriptions() {

        List<EHBlockDescription> blockDescriptions = new LinkedList<>();

        for (int i = 0; i < this.directory.length; i++) {
            EHBlock<T> block = this.ehBlockManager.readBlockAt(this.directory[i].indexInMainFile);

            EHBlockDescription ehBlockDescription = new EHBlockDescription();
            ehBlockDescription.directoryIndex = i;
            ehBlockDescription.indexInFile = this.directory[i].indexInMainFile;
            ehBlockDescription.numberOfValidEntities = this.directory[i].numberOfEntitiesInMainBlock.intValue();
            ehBlockDescription.localLevel = this.directory[i].localBlockLevel;
            ehBlockDescription.overflowBlock = block.getOverflowBlock();
            List<String> entitieDescs = new LinkedList<>();
            for (T en : block.getEntities().stream().filter(EHEntity::isValid).collect(Collectors.toList())) {
                entitieDescs.add("" + en.getHash());
            }
            ehBlockDescription.entities = entitieDescs;
            blockDescriptions.add(ehBlockDescription);
        }

        return blockDescriptions;
    }

    public List<EHBlockDescription> getOverflowBlockDescriptions() {

        List<EHBlockDescription> blockDescriptions = new LinkedList<>();

        for (int i = 0; i < this.ehBlockManager.getNumberOfCreatedOverflowBlocks(); i++) {
            Integer obSize = this.ehPrototypeHolder.getOverflowBlockPrototype().getBlockSize();
            EHOverflowBlock<T> block = this.ehBlockManager.readOverflowBlockAt(i * obSize);

            EHBlockDescription ehBlockDescription = new EHBlockDescription();
            ehBlockDescription.indexInFile = i * obSize;
            // Nepridáva do výstupu uvoľnené OB
            if (this.ehBlockManager.getFreeOverflowBlocks().contains(ehBlockDescription.indexInFile)) {
                continue;
            }
            ehBlockDescription.numberOfValidEntities = (int)block.getEntities().stream().filter(EHEntity::isValid).count();
            ehBlockDescription.overflowBlock = block.getNextOverflowBlock();
            List<String> entitieDescs = new LinkedList<>();
            for (T en : block.getEntities().stream().filter(EHEntity::isValid).collect(Collectors.toList())) {
                entitieDescs.add("" + en.getHash());
            }
            ehBlockDescription.entities = entitieDescs;
            blockDescriptions.add(ehBlockDescription);
        }

        return blockDescriptions;

    }

    public void saveState() {
        EHFileConfig ehFileConfig = new EHFileConfig(
                this.databaseName,
                this.D,
                this.numberOfAdressingBits,
                this.ehPrototypeHolder.getEHBlockPrototype().getNumberOfEntitiesInBlock(),
                this.ehPrototypeHolder.getOverflowBlockPrototype().getNumberOfEntitiesInBlock(),
                this.ehBlockManager.getNumberOfCreatedBlocks(),
                this.ehBlockManager.getNumberOfCreatedOverflowBlocks(),
                this.ehBlockManager.getFreeBlocks(),
                this.ehBlockManager.getFreeOverflowBlocks(),
                this.directory
        );

        ehFileConfig.saveToFile(this.databaseName + "_save.ehdb");
    }

}
