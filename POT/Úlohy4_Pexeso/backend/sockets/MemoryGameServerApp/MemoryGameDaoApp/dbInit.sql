﻿insert into GameTypes values (2,2);
insert into GameTypes values (4,5);
insert into GameTypes values (4,6);
insert into GameTypes values (5,6);
insert into GameTypes values (5,7);

insert into GameStyles values ('Obrázkové pexeso');
insert into GameStyles values ('Zvukové pexeso');

insert into CardPrefixPostfixes values (1, 'obrazkove_', '.jpg');
insert into CardPrefixPostfixes values (2, 'zvukove_', '.mp3');

insert into GameCards values (1, 1);
insert into GameCards values (2, 1);
insert into GameCards values (3, 1);
insert into GameCards values (4, 1);
insert into GameCards values (5, 1);
insert into GameCards values (6, 1);
insert into GameCards values (7, 1);
insert into GameCards values (8, 1);

insert into GameCards values (1, 2);
insert into GameCards values (2, 2);
insert into GameCards values (3, 2);
insert into GameCards values (4, 2);