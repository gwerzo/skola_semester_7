package sk.uniza.fri.lustyno.datastructures.linearhashing.file;

import sk.uniza.fri.lustyno.datastructures.entity.ByteSerializable;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureOperationFailedException;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.FromBytesFactory;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class OverflowBlock<G extends Record> implements ByteSerializable {

    public static final Integer OVERFLOW_BLOCK_INFO_SIZE_BYTES = 8;

    private final Integer numberOfRecordsInOverflowBlock;

    /**
     * Počet validnych záznamov v preplňujúcom bloku
     */
    private Integer numberOfValidRecords = 0;

    /**
     * Offset, na ktorom tento blok začína
     * Netreba storovať v súbore
     */
    private Integer overflowOffset;

    /**
     * Ref. na ďalší preplňujúci blok, ak taký existuje
     * Storuje sa do súboru jeho offset
     */
    private OverflowBlock nextOverflowBlock;

    private List<G> recordList;

    public OverflowBlock(Integer overflowOffset, Integer numberOfRecordsInOverflowBlock) {
        this.overflowOffset = overflowOffset;
        this.numberOfRecordsInOverflowBlock = numberOfRecordsInOverflowBlock;
        this.recordList = new ArrayList<>();
    }

    public OverflowBlock(Integer overflowOffset, Integer numberOfRecordsInOverflowBlock, FromBytesFactory<G> factory) {
        this.overflowOffset = overflowOffset;
        this.numberOfRecordsInOverflowBlock = numberOfRecordsInOverflowBlock;
        this.recordList = new ArrayList<>();
        for (int i = 0; i < numberOfRecordsInOverflowBlock; i++) {
            this.recordList.add(factory.emptyInvalidInstance());
        }
    }

    public List<G> getRecordList() {
        return this.recordList;
    }

    public Integer getOverflowOffset() {
        return this.overflowOffset;
    }

    public void setNextOverflowBlock(OverflowBlock overflowBlock) {
        this.nextOverflowBlock = overflowBlock;
    }

    public OverflowBlock getNextOverflowBlock() {
        return this.nextOverflowBlock;
    }


    public void insertRecordToInvalidPlace(G record) {
        if (numberOfValidRecords.equals(this.numberOfRecordsInOverflowBlock)) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "insertRecordToInvalidPlace",
                    "Nemôžete vkladať do bloku, ktorý je plný");
        }
        for (int i = 0; i < this.recordList.size(); i++) {
            if (!this.recordList.get(i).valid) {
                this.recordList.set(i, record);
                increaseNumberOfRecords();
                return;
            }
        }
    }

    public void increaseNumberOfRecords() {
        if (this.numberOfValidRecords < 0) {
            this.numberOfValidRecords = 0;
        }
        this.numberOfValidRecords++;
    }


    public void appendRecord(G r) {
        this.recordList.add(r);
    }

    public boolean isFull() {
        return this.numberOfValidRecords.equals(this.numberOfRecordsInOverflowBlock);
    }

    /**
     * Prvé 4 bajty sú integer reprezentujúci počet validnych záznamov v bloku
     * Ďalšie 4 bajty sú integer reprezentujúci offset overflow bloku
     * @param fromBytes
     */
    public void setNumberOfValidRecordsAndOverflowBlock(byte[] fromBytes) {
        Integer validRecords = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(fromBytes, 0, 4));
        Integer overflowBlock = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(fromBytes, 4, 8));
        this.numberOfValidRecords = validRecords;
        this.nextOverflowBlock = new OverflowBlock(overflowBlock, this.numberOfRecordsInOverflowBlock);
    }

    @Override
    public byte[] toBytesArray() {
        byte[] records = ToByteArrayConverter.concatenateByteArrays(this.recordList.stream().map(e -> e.toBytesArray()).collect(Collectors.toList()));
        byte[] toRet = ToByteArrayConverter.concatenateByteArrays(
                Arrays.asList(
                        ToByteArrayConverter.bytesFromInt(this.numberOfValidRecords),
                        ToByteArrayConverter.bytesFromInt(this.nextOverflowBlock.getOverflowOffset()),
                        records));
        return toRet;
    }

    public void clearBlock(FromBytesFactory<G> factory) {
        this.recordList.clear();
        for (int i = 0; i < this.numberOfRecordsInOverflowBlock; i++) {
            this.recordList.add(factory.emptyInvalidInstance());
        }

        this.numberOfValidRecords = 0;
        this.nextOverflowBlock = new OverflowBlock(-1, this.numberOfRecordsInOverflowBlock);
    }
}
