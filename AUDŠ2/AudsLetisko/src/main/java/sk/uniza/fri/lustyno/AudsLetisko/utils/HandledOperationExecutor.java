package sk.uniza.fri.lustyno.AudsLetisko.utils;

import lombok.extern.slf4j.Slf4j;
import sk.uniza.fri.lustyno.AudsLetisko.BusinessException;

/**
 * Backendový executor pre operácie, ktoré môžu vyhodiť BusinessException,
 * ktorý chráni output backendových operácií pred nežiadúcimi exceptionami
 */
@Slf4j
public class HandledOperationExecutor {

    /**
     *
     * @param executionListener - Listener pre chránenú action, ktorú treba vykonať
     *
     */
    public static final void submit(OperationExecutorListener executionListener) {
        try {
            executionListener.executeOperation();
        } catch (BusinessException e) {
            CustomNotification.warning(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            CustomNotification.error("Vyskytla sa neznáma chyba");
        }
    }

    /**
     * Functional interface pre executor
     */
    public interface OperationExecutorListener {

        OperationMarker executeOperation();

    }

}

