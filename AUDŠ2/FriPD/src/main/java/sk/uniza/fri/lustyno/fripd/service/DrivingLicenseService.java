package sk.uniza.fri.lustyno.fripd.service;

import org.springframework.stereotype.Service;
import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureWrongUsageException;
import sk.uniza.fri.lustyno.datastructures.linearhashing.config.FileConfig;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Block;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.File;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.OverflowBlock;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Record;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.SPLIT_VARIANT;
import sk.uniza.fri.lustyno.fripd.BusinessException;
import sk.uniza.fri.lustyno.fripd.entities_bo.DrivingLicenceEntity;
import sk.uniza.fri.lustyno.fripd.entities_ui.block.BlockDetailEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.block.ZaznamEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.PridanieVodicskehoPreukazuEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.VodicskyPreukazDetailEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.VyhladanieVodicskehoPreukazuEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.ZmenaUdajovPreukazuEntityUi;
import sk.uniza.fri.lustyno.fripd.entity_mapper.EntityMapper;
import sk.uniza.fri.lustyno.fripd.factory.DrivingLicenseFactory;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Service
public class DrivingLicenseService {

    /**
     * Odkomentovať/Zakomentovať, keď chcem ísť voči práznej databáze
     */
//    private final String evcCodeIndexFilePath = "";

    /**
     * Odkomentovať/Zakomentovať, keď chcem ísť voči už vytvoreným dátam
     */
    private final String evcCodeIndexFilePath = "LIC_EVC_INDEX";


    private static Integer NUMBER_OF_PRIMARY_BLOCKS_IN_LH_EVC = 2;
    private static Integer NUMBER_OF_SECONDARY_BLOCKS_IN_LH_EVC = 3;

    private File<IntegerKey, DrivingLicenceEntity> evcCodeIndex;

    public DrivingLicenseService() {
        initializeEvcCodeIndex();
    }

    /**
     * Pridá nový vodičský preukaz do databázy,
     * pridá ho samostatne do HeapFile
     * a samostatne do indexového súboru
     *
     * @param entityUi
     */
    public void addNewDrivingLicense(PridanieVodicskehoPreukazuEntityUi entityUi) {
        DrivingLicenceEntity entity = EntityMapper.fromPridanieNovehoVodicskehoPreukazuEntityUi(entityUi);

        DrivingLicenceEntity alreadyStored = this.evcCodeIndex.find(entity);
        if (alreadyStored != null) {
            throw new BusinessException("Vodičský preukaz s takýmto EVČ sa už v databáze nachádza");
        }

        storeDrivingLicense(entity);
    }

    /**
     * Vráti záznam o vodičakom pewukaze podľa EVČ
     * pokiaľ sa v DB nachádza,
     * ak nie, vráti null
     */
    public VodicskyPreukazDetailEntityUi findLicenseByEvcCode(VyhladanieVodicskehoPreukazuEntityUi vyhladanieVodicskehoPreukazuEntityUi) {
        Integer EVC_CODE = vyhladanieVodicskehoPreukazuEntityUi.getEvcPreukazu();

        if (EVC_CODE == null) {
            throw new BusinessException("EVČ kód pre vyhľadanie vodičského preukazu je povinný parameter");
        }
        DrivingLicenceEntity entity = EntityMapper.fromVyhladanieVodicskehoPreukazuEntityUi(vyhladanieVodicskehoPreukazuEntityUi);
        DrivingLicenceEntity foundEntity = this.evcCodeIndex.find(entity);
        if (foundEntity == null || !foundEntity.isValid()) {
            throw new BusinessException("Vodičský preukaz s takýmto EVČ sa nenašiel");
        }

        return EntityMapper.fromDrivingLicenseEntity(foundEntity);
    }

    /**
     * Updatne údaje o vodičskom preukaze
     */
    public void updateVodicskyPreukaz(ZmenaUdajovPreukazuEntityUi entityUi) {
        DrivingLicenceEntity entity = EntityMapper.fromZmenaUdajovPreukazuEntityUi(entityUi);

        DrivingLicenceEntity evcIndex = this.evcCodeIndex.find(entity);
        if (evcIndex == null || !evcIndex.isValid()) {
            throw new BusinessException("Vodičský preukaz s EVČ kódom " + entityUi.getEvcVodica() + " nebol nájdený");
        }
        entity.setValid(true);

        try {
            this.evcCodeIndex.update(entity);
        } catch (DataStructureWrongUsageException e) {
            throw new BusinessException(e.getMessage());
        }

    }


    private void initializeEvcCodeIndex() {
        if (this.evcCodeIndexFilePath.isEmpty()) {
            this.evcCodeIndex = new File<>(
                    "LIC_EVC_INDEX",
                    NUMBER_OF_PRIMARY_BLOCKS_IN_LH_EVC,
                    NUMBER_OF_SECONDARY_BLOCKS_IN_LH_EVC,
                    DrivingLicenceEntity.class,
                    SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR,
                    new DrivingLicenseFactory()
            );
        } else {
            FileConfig fc = FileConfig.fromFile(this.evcCodeIndexFilePath + "_conf.conf");
            this.evcCodeIndex = new File<>(
                    fc,
                    DrivingLicenceEntity.class,
                    new DrivingLicenseFactory()
            );
        }
    }

    /**
     * Uloží samotnú entitu do súboru
     * a do indexových súborov
     *
     * @param entity - entita pre store
     */
    private void storeDrivingLicense(DrivingLicenceEntity entity) {
        this.evcCodeIndex.saveRecord(entity);
    }


    /**
     * Vráti primárne bloky lineárneho hashovania preukazov podľa VIN
     * @return
     */
    public List<BlockDetailEntityUi> getDrivingLicensesPrimaryBlocks() {
        List<BlockDetailEntityUi> bloky = new LinkedList<>();

        FileConfig fc = this.evcCodeIndex.getActualConfiguration();

        for (int i = 0; i < fc.secondLevelModulo; i++) {
            BlockDetailEntityUi entityUi = new BlockDetailEntityUi();

            Block<DrivingLicenceEntity> blok = this.evcCodeIndex.getBlockByIndex(i);

            entityUi.setOffsetVSubore(fc.blockSize * i);
            entityUi.setOffsetPreplnovaciehoBloku(blok.getOverflowBlockOffset());
            entityUi.setPocetValidnychZaznamov(blok.validRecordsCount());
            entityUi.setZaznamyBloku(new LinkedList<>());
            entityUi.setCisloBloku(i);

            for (Record rec : blok.getRecordsList()) {
                ZaznamEntityUi entityUi1 = new ZaznamEntityUi();
                entityUi1.setKluc(rec.getKey().getValue().toString());
                entityUi1.setValidny(rec.isValid() ? "Áno" : "Nie");
                entityUi.getZaznamyBloku().add(entityUi1);
            }

            bloky.add(entityUi);
        }

        return bloky;
    }


    /**
     * Vráti overflow bloky lineárneho hashovania pre vodičáky podľa EVČ
     * @return
     */
    public List<BlockDetailEntityUi> getDrivingLicensesOverflowBlocks() {

        List<OverflowBlock<DrivingLicenceEntity>> entities = this.evcCodeIndex.getAllOverflowBlocks();

        List<BlockDetailEntityUi> entityUis = new LinkedList<>();

        int blockCounter = 0;
        for (OverflowBlock<DrivingLicenceEntity> block : entities) {

            BlockDetailEntityUi entityUi = new BlockDetailEntityUi();
            entityUi.setCisloBloku(blockCounter++);

            List<ZaznamEntityUi> zaznamy = new LinkedList<>();
            for (DrivingLicenceEntity ent : block.getRecordList()) {

                ZaznamEntityUi entUi = new ZaznamEntityUi();
                entUi.setValidny(ent.isValid() ? "Áno" : "Nie");
                entUi.setKluc(ent.getEVC().toString());
                entUi.setHodnota(ent.getMenoVodica().replace("$", "") + " " + ent.getPriezviskoVodica().replace("$", ""));
                zaznamy.add(entUi);
            }

            entityUi.setZaznamyBloku(zaznamy);

            entityUis.add(entityUi);
        }

        return entityUis;
    }

    public void finalize() {
        this.evcCodeIndex.saveConfiguration();

        this.evcCodeIndex.close();
    }

    public void generateLicenses() {
        Random rnd = new Random();
        for (int i = 0; i < 30; i++) {
            DrivingLicenceEntity entity = new DrivingLicenceEntity(
                    rnd.nextInt(145142121), "Lukas", "Lustiak",
                    LocalDate.now().atTime(LocalTime.MAX), false, rnd.nextInt(5));
            this.storeDrivingLicense(entity);
        }
    }

}
