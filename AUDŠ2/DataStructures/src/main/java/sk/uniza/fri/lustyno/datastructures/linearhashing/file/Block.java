package sk.uniza.fri.lustyno.datastructures.linearhashing.file;

import sk.uniza.fri.lustyno.datastructures.entity.ByteSerializable;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureOperationFailedException;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.FromBytesFactory;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Block<G extends Record> implements ByteSerializable {

    public static Integer BLOCK_INFO_SIZE_BYTES = 11;

    /**
     * Počet validnych záznamov v bloku,
     * storuje sa do súboru
     */
    private Integer numberOfValidRecords = 0;

    /**
     * Počet záznamov v bloku
     */
    private Integer numberOfRecordsInBlock;

    private Integer numberOfRecordsInOverflowBlock;

    /**
     * Zoznam záznamov
     */
    private List<G> recordList;

    /**
     * Overflow block,
     * storuje sa do súboru jeho offset
     */
    private OverflowBlock overflowBlock;

    public Block(int numberOfRecordsInBlock, int numberOfRecordsInOverflowBlock) {
        this.recordList = new ArrayList<>(numberOfRecordsInBlock);
        this.numberOfRecordsInBlock = numberOfRecordsInBlock;
        this.numberOfRecordsInOverflowBlock = numberOfRecordsInOverflowBlock;
    }

    public Block(int numberOfRecordsInBlock, int numberOfRecordsInOverflowBlock, FromBytesFactory<G> factory) {
        this.recordList = new ArrayList<>(numberOfRecordsInBlock);
        this.numberOfRecordsInBlock = numberOfRecordsInBlock;
        this.numberOfRecordsInOverflowBlock = numberOfRecordsInOverflowBlock;
        for (int i = 0; i < this.numberOfRecordsInBlock; i++) {
            this.recordList.add(factory.emptyInvalidInstance());
        }
    }

    public void increaseNumberOfRecords() {
        if (this.numberOfValidRecords < 0) {
            this.numberOfValidRecords = 0;
        }
        this.numberOfValidRecords++;
    }

    public List<G> getRecordsList() {
        return this.recordList;
    }

    public OverflowBlock getOverflowBlock() {
        return this.overflowBlock;
    }

    public void decreaseNumberOfRecords() {
        this.numberOfValidRecords--;
    }

    public Integer getOverflowBlockOffset() {
        return this.overflowBlock.getOverflowOffset();
    }

    /**
     * Prvé 4 bajty sú integer reprezentujúci počet validnych záznamov v bloku
     * Ďalšie 4 bajty sú integer reprezentujúci offset overflow bloku
     * @param fromBytes
     */
    public void setNumberOfValidRecordsAndOverflowBlock(byte[] fromBytes) {
        Integer validRecords = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(fromBytes, 3, 7));
        Integer overflowBlock = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(fromBytes, 7, 11));

        this.numberOfValidRecords = validRecords;
        this.overflowBlock = new OverflowBlock(overflowBlock, this.numberOfRecordsInOverflowBlock);
    }

    public void setOverflowBlock(OverflowBlock<G> overflowBlock) {
        this.overflowBlock = overflowBlock;
    }

    public void insertRecordToInvalidPlace(G record) {
        if (numberOfValidRecords.equals(numberOfRecordsInBlock)) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "insertRecordToInvalidPlace",
                    "Nemôžete vkladať do bloku, ktorý je plný");
        }
        for (int i = 0; i < this.recordList.size(); i++) {
            if (!this.recordList.get(i).valid) {
                this.recordList.set(i, record);
                increaseNumberOfRecords();
                return;
            }
        }
    }

    /**
     * Appendne record na koniec zoznamu recordov,
     * preto používať len pre vyskladanie bloku zo súboru,
     * nie na "INSERTOVANIE"
     * @param record
     */
    public void appendRecordFromFile(G record) {
        this.recordList.add(record);
    }

    @Override
    public byte[] toBytesArray() {
        byte[] records = ToByteArrayConverter.concatenateByteArrays(this.recordList.stream().map(e -> e.toBytesArray()).collect(Collectors.toList()));
        byte[] align = new byte[3];
        align[0] = 99;
        align[1] = 99;
        align[2] = 99;
        return ToByteArrayConverter.concatenateByteArrays(
                Arrays.asList(
                        align,
                        ToByteArrayConverter.bytesFromInt(this.numberOfValidRecords),
                        ToByteArrayConverter.bytesFromInt(this.overflowBlock.getOverflowOffset()),
                        records)
                );
    }

    public int validRecordsCount() {
        return (int)this.recordList.stream().filter(e -> e.valid).count();
    }

    public void clearBlock(FromBytesFactory<G> factory) {
        this.recordList.clear();
        for (int i = 0; i < this.numberOfRecordsInBlock; i++) {
            this.recordList.add(factory.emptyInvalidInstance());
        }
        this.numberOfValidRecords = 0;
        this.overflowBlock = new OverflowBlock(-1, this.numberOfRecordsInOverflowBlock);
    }

    public boolean isFull() {
        return this.numberOfRecordsInBlock.equals(this.numberOfValidRecords);
    }

}
