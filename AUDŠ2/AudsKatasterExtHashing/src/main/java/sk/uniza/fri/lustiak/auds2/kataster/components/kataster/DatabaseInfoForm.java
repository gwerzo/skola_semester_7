package sk.uniza.fri.lustiak.auds2.kataster.components.kataster;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.DatabaseInfoUi;

import java.util.Objects;
import java.util.stream.Collectors;

public class DatabaseInfoForm extends HorizontalLayout {

    private TextField hlbkaSuboruTF;
    private TextField pocetAdresovacichBitovTF;
    private TextField pocetEntitVBlokuTF;
    private TextField pocetEntitVOverflowBlokuTF;

    private TextField pocetBlokovTF;
    private TextField pocetOverflowBlokovTF;

    private TextField velkostHlavnehoSuboruTF;
    private TextField velkostOverflowSuboruTF;

    private TextArea volneBlokyTA;
    private TextArea volneOverflowBlokyTA;

    public DatabaseInfoForm(DatabaseInfoUi blockInfoUi) {
        this.pocetBlokovTF = new TextField("Počet blokov");
        this.pocetBlokovTF.setEnabled(false);

        this.pocetOverflowBlokovTF = new TextField("Počet overflow blokov");
        this.pocetOverflowBlokovTF.setEnabled(false);

        this.hlbkaSuboruTF = new TextField("HĹBKA SÚBORU (D)");
        this.hlbkaSuboruTF.setEnabled(false);

        this.pocetAdresovacichBitovTF = new TextField("Počet adresovacích bitov");
        this.pocetAdresovacichBitovTF.setEnabled(false);

        this.pocetEntitVBlokuTF = new TextField("Entít v bloku");
        this.pocetEntitVBlokuTF.setEnabled(false);

        this.pocetEntitVOverflowBlokuTF = new TextField("Entít v overflow bloku");
        this.pocetEntitVOverflowBlokuTF.setEnabled(false);

        this.velkostHlavnehoSuboruTF = new TextField("Veľkosť hlavného súboru");
        this.velkostHlavnehoSuboruTF.setEnabled(false);

        this.velkostOverflowSuboruTF = new TextField("Veľkosť overflow súboru");
        this.velkostOverflowSuboruTF.setEnabled(false);

        this.volneBlokyTA = new TextArea("Voľné hlavné bloky");
        this.volneBlokyTA.setEnabled(false);

        this.volneOverflowBlokyTA = new TextArea("Voľné overflow bloky");
        this.volneOverflowBlokyTA.setEnabled(false);

        VerticalLayout vl1 = new VerticalLayout(
                this.hlbkaSuboruTF,
                this.pocetAdresovacichBitovTF
        );

        VerticalLayout vl2 = new VerticalLayout(
                this.pocetEntitVBlokuTF,
                this.pocetEntitVOverflowBlokuTF
        );

        VerticalLayout vl3 =new VerticalLayout(
                this.pocetBlokovTF,
                this.pocetOverflowBlokovTF
        );

        VerticalLayout vl4 = new VerticalLayout(
                this.velkostHlavnehoSuboruTF,
                this.velkostOverflowSuboruTF
        );

        VerticalLayout vl5 = new VerticalLayout(
                this.volneBlokyTA
        );

        VerticalLayout vl6 = new VerticalLayout(
                this.volneOverflowBlokyTA
        );


        vl1.setWidth("220px");
        vl2.setWidth("220px");
        vl3.setWidth("220px");
        vl4.setWidth("220px");
        vl5.setWidth("220px");
        vl6.setWidth("220px");

        this.addComponents(
                vl1,
                vl2,
                vl3,
                vl4,
                vl5,
                vl6
        );

        this.initFrom(blockInfoUi);
    }

    public void initFrom(DatabaseInfoUi blockInfoUi) {
        this.hlbkaSuboruTF.setValue(blockInfoUi.getHlbkaSuboru().toString());
        this.pocetBlokovTF.setValue(blockInfoUi.getPocetBlokov().toString());
        this.pocetOverflowBlokovTF.setValue(blockInfoUi.getPocetOverflowBlokov().toString());
        this.velkostHlavnehoSuboruTF.setValue(blockInfoUi.getVelkostHlavnehoSuboru().toString());
        this.velkostOverflowSuboruTF.setValue(blockInfoUi.getVelkostOverflowSuboru().toString());
        this.pocetAdresovacichBitovTF.setValue(blockInfoUi.getPocetAdresovacichBitov().toString());
        this.pocetEntitVBlokuTF.setValue(blockInfoUi.getPocetEntitVHlavnomBloku().toString());
        this.pocetEntitVOverflowBlokuTF.setValue(blockInfoUi.getPocetEntitVOverflowBloku().toString());

        this.volneBlokyTA.setValue(
                blockInfoUi.getVolneBloky().stream().map(Object::toString).collect(Collectors.joining("\n"))
        );

        this.volneOverflowBlokyTA.setValue(
                blockInfoUi.getVolneOverflowBloky().stream().map(Objects::toString).collect(Collectors.joining("\n"))
        );
    }

}
