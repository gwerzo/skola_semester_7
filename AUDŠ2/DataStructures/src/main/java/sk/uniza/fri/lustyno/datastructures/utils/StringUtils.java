package sk.uniza.fri.lustyno.datastructures.utils;

import java.util.Random;

public class StringUtils {

    public static String padRightDollars(String stringToPadd, int toLength) {
        if (stringToPadd.length() >= toLength) {
            return stringToPadd;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(stringToPadd);
        while (sb.length() < toLength) {
            sb.append('$');
        }
        return sb.toString();
    }

    public static String padRightSpaces(String stringToPadd, int toLength) {
        if (stringToPadd.length() >= toLength) {
            return stringToPadd;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(stringToPadd);
        while (sb.length() < toLength) {
            sb.append(' ');
        }
        return sb.toString();
    }

    /**
     * Vráti "safe" substring zo stringu
     * a to tak, že ak dĺžka stringu je menej
     * ako parameter
     * @param toMaxIndex
     * tak vráti len string samotný
     *
     * Začína od indexu 0 (included), končí na indexe toMaxIndex (excluded)
     *
     * @param fromString - Z ktorého stringu
     * @param toMaxIndex - Po ktorý index
     * @return
     */
    public static String safeSubstring(String fromString, int toMaxIndex) {
        if (fromString.length() < toMaxIndex) {
            return fromString;
        } else {
            return fromString.substring(0, toMaxIndex);
        }
    }

    /**
     * <source>https://www.baeldung.com/java-random-string</source>
     * Upravený pre parametrizovanú dĺžku výstupu
     *
     * Vráti náhodný abecedný string,
     * zložený zo znakov od 'a' po 'z'
     * podľa predanej dĺžky z parametra
     * @param characterCount
     *
     * @return
     */
    public static String randomAlphabeticString(int characterCount) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(characterCount)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

}
