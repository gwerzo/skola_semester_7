package sk.uniza.fri.lustiak.auds2.kataster.components.custom;

import com.vaadin.ui.TextArea;

/**
 * Customizovateľný text area component,
 * s defaultnou šírkou a výškou
 */
public class CustomTextArea extends TextArea {

    private final String FIELD_WIDTH    = "150px";
    private final String FIELD_HEIGHT   = "100px";

    public CustomTextArea(String caption, Boolean enabled) {
        super(caption);
        this.setEnabled(enabled);
        this.setWidth(this.FIELD_WIDTH);
        this.setHeight(this.FIELD_HEIGHT);
    }

}
