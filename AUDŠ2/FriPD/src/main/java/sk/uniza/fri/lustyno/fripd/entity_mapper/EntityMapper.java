package sk.uniza.fri.lustyno.fripd.entity_mapper;


import sk.uniza.fri.lustyno.datastructures.utils.StringUtils;
import sk.uniza.fri.lustyno.fripd.entities_bo.DrivingLicenceEntity;
import sk.uniza.fri.lustyno.fripd.entities_bo.VehicleEntity;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.PridanieVodicskehoPreukazuEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.VodicskyPreukazDetailEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.VyhladanieVodicskehoPreukazuEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.ZmenaUdajovPreukazuEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.PridanieVozidlaEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.VozidloEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.ZmenaUdajovVozidlaEntityUi;

import java.time.LocalTime;

/**
 * EntityMapper je trieda, ktorá premapuváva
 * FE na BE entity a vice versa.
 *
 * Užitočné ako method ref. v streamoch
 */
public class EntityMapper {

    public static VehicleEntity fromPridanieVozidlaEntityUi(PridanieVozidlaEntityUi entityUi) {

        VehicleEntity vehicleEntity = new VehicleEntity();
        vehicleEntity
                .setVIN_KOD(StringUtils.padRightDollars(entityUi.getVIN(), 17))
                .setEVC(StringUtils.padRightDollars(entityUi.getEVC(), 7))
                .setDatumKoncaPlatnostiEK(entityUi.getDatumPlatnostiEK().atTime(LocalTime.MAX))
                .setDatumKoncaPlatnostiSTK(entityUi.getDatumPlatnostiSTK().atTime(LocalTime.MAX))
                .setPocetNaprav(entityUi.getPocetNaprav())
                .setPrevadzkovaHmotnost(entityUi.getPrevadzkovaHmostnost())
                .setVPatrani(entityUi.getVPatrani());

        return vehicleEntity;
    }

    public static VozidloEntityUi fromVehicleEntityToVehicleEntityUi(VehicleEntity entity) {

        VozidloEntityUi vehicleEntityUi = new VozidloEntityUi();
        vehicleEntityUi
                .setVIN(entity.getVIN_KOD().replace("$", ""))
                .setEVC(entity.getEVC().replace("$", ""))
                .setVPatrani(entity.getVPatrani() ? "Áno" : "Nie")
                .setDatumPlatnostiEK(entity.getDatumKoncaPlatnostiEK().toString())
                .setDatumPlatnostiSTK(entity.getDatumKoncaPlatnostiSTK().toString())
                .setPocetNaprav(entity.getPocetNaprav().toString())
                .setPrevadzkovaHmostnost(entity.getPrevadzkovaHmotnost().toString());
        return vehicleEntityUi;

    }

    public static VehicleEntity fromZmenaUdajovVozidlaEntityUi(ZmenaUdajovVozidlaEntityUi entityUi) {
        VehicleEntity vehicleEntity = new VehicleEntity();
        vehicleEntity
                .setVIN_KOD(StringUtils.padRightDollars(entityUi.getVIN(), 17))
                .setEVC(StringUtils.padRightDollars(entityUi.getEVC(), 7))
                .setDatumKoncaPlatnostiEK(entityUi.getDatumPlatnostiEK().atTime(LocalTime.MAX))
                .setDatumKoncaPlatnostiSTK(entityUi.getDatumPlatnostiSTK().atTime(LocalTime.MAX))
                .setPocetNaprav(entityUi.getPocetNaprav())
                .setPrevadzkovaHmotnost(entityUi.getPrevadzkovaHmostnost())
                .setVPatrani(entityUi.getVPatrani());

        return vehicleEntity;
    }


    public static DrivingLicenceEntity fromVyhladanieVodicskehoPreukazuEntityUi(VyhladanieVodicskehoPreukazuEntityUi entityUi) {
        DrivingLicenceEntity drivingLicenceEntity = new DrivingLicenceEntity();
        drivingLicenceEntity.setEVC(entityUi.getEvcPreukazu());

        return drivingLicenceEntity;
    }

    public static DrivingLicenceEntity fromPridanieNovehoVodicskehoPreukazuEntityUi(PridanieVodicskehoPreukazuEntityUi entityUi) {
        DrivingLicenceEntity drivingLicenceEntity = new DrivingLicenceEntity();
        drivingLicenceEntity
                .setEVC(entityUi.getEvcVodica())
                .setMenoVodica(StringUtils.padRightDollars(entityUi.getMenoVodica(), 35))
                .setPriezviskoVodica(StringUtils.padRightDollars(entityUi.getPriezviskoVodica(), 35))
                .setDatumUkonceniaPlatnosti(entityUi.getDatumUkonceniaPlatnostiPreukazu().atTime(LocalTime.MAX))
                .setZakazViestVozidlo(entityUi.getZakazViestVozidlo())
                .setPocetPriestupkovZaPoslednyRok(entityUi.getPocetDopravnychPriestupkov());
        return drivingLicenceEntity;
    }

    public static DrivingLicenceEntity fromZmenaUdajovPreukazuEntityUi(ZmenaUdajovPreukazuEntityUi entityUi) {
        DrivingLicenceEntity drivingLicenceEntity = new DrivingLicenceEntity();
        drivingLicenceEntity
                .setEVC(entityUi.getEvcVodica())
                .setMenoVodica(StringUtils.padRightDollars(entityUi.getMenoVodica(), 35))
                .setPriezviskoVodica(StringUtils.padRightDollars(entityUi.getPriezviskoVodica(), 35))
                .setDatumUkonceniaPlatnosti(entityUi.getDatumUkonceniaPlatnostiPreukazu().atTime(LocalTime.MAX))
                .setZakazViestVozidlo(entityUi.getZakazViestVozidlo())
                .setPocetPriestupkovZaPoslednyRok(entityUi.getPocetDopravnychPriestupkov());
        return drivingLicenceEntity;
    }

    public static VodicskyPreukazDetailEntityUi fromDrivingLicenseEntity(DrivingLicenceEntity entity) {
        VodicskyPreukazDetailEntityUi entityUi = new VodicskyPreukazDetailEntityUi();
        entityUi
                .setEvcVodica(entity.getEVC().toString())
                .setDatumUkonceniaPlatnostiPreukazu(entity.getDatumUkonceniaPlatnosti().toString())
                .setMenoVodica(entity.getMenoVodica().replace("$", ""))
                .setPriezviskoVodica(entity.getPriezviskoVodica().replace("$", ""))
                .setZakazViestVozidlo(entity.getZakazViestVozidlo() ? "Áno" : "Nie")
                .setPocetDopravnychPriestupkov(entity.getPocetPriestupkovZaPoslednyRok().toString());
        return entityUi;
    }


}
