package sk.uniza.fri.lustyno.fripd.utils.converters;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

public class StringToHourConverter implements Converter<String, Integer> {

    private static final long serialVersionUID = 1L;

    @Override
    public Result<Integer> convertToModel(String s, ValueContext valueContext) {
        Integer i;
        try {
            i = new Integer(s);
            if (i < 0 || i > 23) {
                return Result.error("Hodina môžže byť len v rozpätí 1 - 23");
            }
        } catch (Exception e) {
            return Result.error("Nesprávny formát čísla");
        }
        return Result.ok(i);
    }

    @Override
    public String convertToPresentation(Integer integer, ValueContext valueContext) {
        if (integer == null) {
            return "";
        }
        return integer.toString();
    }
}
