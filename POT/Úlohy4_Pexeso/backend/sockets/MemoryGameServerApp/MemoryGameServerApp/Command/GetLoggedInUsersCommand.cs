﻿using System;
using System.Collections.Generic;
using MemoryGameServerApp.Server;
using SuperWebSocket;

namespace MemoryGameServerApp.Command
{
    public class GetLoggedInUsersCommand: ICommand
    {
        public const string COMMAND = "-listloggedinusers";
        private const string LOGGED_IN_LIST = "-loggedinuserslist";
        public void executeCommand(string usernameOfCommandCaller, MemoryGameServer memoryGameServer,
            WebSocketSession callerSocketSession)
        {
            List<string> loggedInUsers = memoryGameServer.GetLoggedInUsernames();

            callerSocketSession.Send(LOGGED_IN_LIST + " " + String.Join(" ", loggedInUsers));
        }
    }
}
