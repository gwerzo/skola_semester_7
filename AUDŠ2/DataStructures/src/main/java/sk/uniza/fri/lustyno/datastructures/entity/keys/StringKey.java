package sk.uniza.fri.lustyno.datastructures.entity.keys;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;

public class StringKey implements HasComparableValue<String> {

    private String value;

    public StringKey(String keyValue) {
        this.value = keyValue;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public StringKey getMinValue() {
        return new StringKey("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
    }
}
