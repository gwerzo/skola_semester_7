package sk.uniza.fri.lustyno.datastructures.exception;

public class DataStructureWrongUsageException extends RuntimeException {

    public DataStructureWrongUsageException(String className, String operationName, String message) {
        super("Wrong usage of operation " + operationName + " in datastructure " + className +
                "\nMessage:\n" + message);
    }

}
