package sk.uniza.fri.lustyno.datastructures.extendiblehashing;

import sk.uniza.fri.lustyno.datastructures.extendiblehashing.entities.EHEntity;

import java.util.ArrayList;

public class EHPrototypeManager {

    private static ArrayList<ClassToEHEntity> prototypes = new ArrayList<>();

    private static EHPrototypeManager instance = null;

    private EHPrototypeManager() {
        prototypes = new ArrayList<>();
    }

    public static void registerNewPrototype(EHEntity ehEntityPrototype) {
        prototypes.add(new ClassToEHEntity(ehEntityPrototype.getClass(), ehEntityPrototype));
    }

    public static EHEntity getPrototypeEntityForClass(Class classOfEntity) {
        ClassToEHEntity cToEhEn =  prototypes
                .stream()
                .filter(e -> e.clazz.equals(classOfEntity))
                .findFirst()
                .orElse(null);
        if (cToEhEn != null) {
            return cToEhEn.entity;
        }
        return null;
    }

    public static EHPrototypeManager getInstance() {
        if (EHPrototypeManager.instance == null) {
            EHPrototypeManager.instance = new EHPrototypeManager();
            return EHPrototypeManager.instance;
        }
        return EHPrototypeManager.instance;
    }


    private static class ClassToEHEntity {

        protected Class clazz;
        protected EHEntity entity;

        protected  ClassToEHEntity(Class clazz, EHEntity ehEntity) {
            this.clazz = clazz;
            this.entity = ehEntity;
        }

    }

}
