﻿using System.Collections.Generic;
using MemoryGameDaoApp.Bo;

namespace PexesoApi.Bo
{
    public class MemoryGame
    {

        public int MemoryGameId { get; set; }

        public GameStyle GameStyle { get; set; }

        public GameType GameType { get; set; }

        public Player Player1 { get; set; }

        public Player Player2 { get; set; }

        public int StepsCount { get; set; }

        public long GameStartTime { get; set; }

        public long TotalTime { get; set; }

        public Player Winner { get; set; }

        public List<MemoryGameStep> MemoryGameSteps { get; set; }


        public MemoryGame()
        {

        }


    }
}
