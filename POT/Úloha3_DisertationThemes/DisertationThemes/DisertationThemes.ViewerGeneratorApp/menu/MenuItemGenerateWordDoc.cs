﻿using System;
using System.Collections.Generic;
using System.Linq;
using DisertationThemes.Database;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Theme = DisertationThemes.Database.Theme;

namespace DisertationThemes.ViewerGeneratorApp.menu
{
    class MenuItemGenerateWordDoc: AbstractMenuItem
    {
        public static string MENU_TEXT = "Vygenerovať word dokument pre dizertačnú prácu";
        public MenuItemGenerateWordDoc(MenuHolder menuHolder) : base(menuHolder)
        {
        }

        public override string GetMenuText()
        {
            return MENU_TEXT;
        }

        protected override int PerformActionInternally()
        {
            using (DisertationDatabaseContext ddc = new DisertationDatabaseContext())
            {
                Console.WriteLine(
                    "Po stlačení ENTER sa ti zobrazia dizertačné témy s ich ID, z ktorých si môžeš vybrať niektorú pre vygenerovanie WORD DOKUMENTU");
                Console.ReadLine();
                Theme selectedTheme = this.SelectTheme(ddc);

                using (WordprocessingDocument mainDoc =
                    WordprocessingDocument.Open("PhD_temy_sablona.docx", true))
                {
                    using (var resultDoc = WordprocessingDocument.Create("generatedTheme" + DateTime.Now.Millisecond + ".docx",
                        WordprocessingDocumentType.Document))
                    {
                        foreach (var part in mainDoc.Parts)
                            resultDoc.AddPart(part.OpenXmlPart, part.RelationshipId);

                        var document = resultDoc.MainDocumentPart.Document;

                        foreach (var text in document.Descendants<Text>())
                        {
                            if (text.Text.Contains("#=ThemeName=#"))
                            {
                                text.Text = text.Text.Replace("#=ThemeName=#", selectedTheme.Name);
                            }

                            if (text.Text.Contains("#=Supervisor=#"))
                            {
                                text.Text = text.Text.Replace("#=Supervisor=#", selectedTheme.Supervisor.FullName);
                            }

                            if (text.Text.Contains("#=StProgram=#"))
                            {
                                text.Text = text.Text.Replace("#=StProgram=#", selectedTheme.StProgram.Name);
                            }

                            if (text.Text.Contains("#=FieldOfStudy=#"))
                            {
                                text.Text = text.Text.Replace("#=FieldOfStudy=#", selectedTheme.StProgram.FieldOfStudy);
                            }

                            if (text.Text.Contains("#=ResearchType=#"))
                            {
                                text.Text = text.Text.Replace("#=ResearchType=#", selectedTheme.ResearchType.ToString());
                            }

                            if (text.Text.Contains("#=Description=#"))
                            {
                                text.Text = text.Text.Replace("#=Description=#", selectedTheme.Description);
                            }
                        }
                    }
                }
            }
            return 0;
        }

        private Theme SelectTheme(DisertationDatabaseContext ddc)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            foreach (var theme in ddc.Themes)
            {
                Console.WriteLine("\t" + theme.ThemeId + ".\t" + theme.Name);
            }

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("Zadaj ID témy, ktorej dokument chceš vygenerovať: ");
                var userInput = Console.ReadLine();

                int themeId;
                bool parseSuccess = Int32.TryParse(userInput, out themeId);

                if (!parseSuccess)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("ID témy zadané v zlom tvare");
                    continue;
                }

                Theme foundById = ddc.Themes.SingleOrDefault(s => s.ThemeId == themeId);
                if (foundById == null)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Zadal si neplatné ID témy");
                    continue;
                }

                return foundById;
            }
        }
    }
}
