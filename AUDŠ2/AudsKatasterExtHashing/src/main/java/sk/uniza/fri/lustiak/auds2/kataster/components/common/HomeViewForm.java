package sk.uniza.fri.lustiak.auds2.kataster.components.common;

import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class HomeViewForm extends VerticalLayout {

    public HomeViewForm() {
        initComponents();
    }

    private void initComponents() {

        Label lMain = new Label("Funkcionalita poskytovaná informačným systémom:");

        Label lEmpty = new Label();

        Label l1 = new Label("1. Vyhľadanie NEHNUTEĽNOSTÍ podľa ID");
        Label l2 = new Label("2. Pridanie novej NEHNUTEĽNOSTI");
        Label l3 = new Label("3. Editácia údajov NEHNUTEĽNOSTI");
        Label l4 = new Label("4. Vyradenie NEHNUTEĽNOSTI podľa zadanej GPS súranice");

        l1.setWidth("100%");
        l2.setWidth("100%");
        l3.setWidth("100%");
        l4.setWidth("100%");

        VerticalLayout labelsWrapper = new VerticalLayout(lMain, lEmpty, l1, l2, l3, l4);

        Panel descPanel = new Panel(
                "Katastrálny informačný systém",
                        labelsWrapper
                );

        addComponent(descPanel);
    }

}
