package sk.uniza.fri.lustyno.datastructures.linearhashing.config;


import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class FileConfig {

    public FileConfig() {

    }

    // Všetko čo je vo File
    public Integer numberOfRecordsInPrimaryBlocks;
    public Integer numberOfRecordsInOverflowBlocks;
    public Integer blocksCount;
    public Integer splitPointer;
    public Integer fileLevel;
    public Integer entitySizeInBytes;
    public Integer firstLevelModulo;
    public Integer secondLevelModulo;
    public Integer blockSize;
    public Integer overflowBlockSize;
    public Integer numberOfRecords;
    public Integer splitVariant; //  0 - load, 1 - number, 2 - ratio


    // Všetko čo je v FileHandleri
    public String databaseName;


    // Všetko čo je v OverflowBlocksHandleri
    public Double maxThreshold;
    public Integer entityCounter;
    public Integer offsetCounter;
    public Integer maxOffset;
    public Integer numberOfOverflowBlocksUsed;
    public List<Integer> freeOverflowBlocksOffsets;


    public void saveToFile() {
        String fileName = this.databaseName + "_conf.conf";

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(fileName, false));
            bw.write(numberOfRecordsInPrimaryBlocks.toString());
            bw.newLine();
            bw.write(numberOfRecordsInOverflowBlocks.toString());
            bw.newLine();
            bw.write(blocksCount.toString());
            bw.newLine();
            bw.write(splitPointer.toString());
            bw.newLine();
            bw.write(fileLevel.toString());
            bw.newLine();
            bw.write(entitySizeInBytes.toString());
            bw.newLine();
            bw.write(firstLevelModulo.toString());
            bw.newLine();
            bw.write(secondLevelModulo.toString());
            bw.newLine();
            bw.write(blockSize.toString());
            bw.newLine();
            bw.write(overflowBlockSize.toString());
            bw.newLine();
            bw.write(numberOfRecords.toString());
            bw.newLine();
            bw.write(splitVariant.toString());
            bw.newLine();
            bw.write(databaseName);
            bw.newLine();

            bw.write(maxThreshold.toString());
            bw.newLine();
            bw.write(entityCounter.toString());
            bw.newLine();
            bw.write(offsetCounter.toString());
            bw.newLine();
            bw.write(maxOffset.toString());
            bw.newLine();
            bw.write(numberOfOverflowBlocksUsed.toString());
            bw.newLine();

            for (Integer i : freeOverflowBlocksOffsets) {
                bw.write(i.toString());
                bw.newLine();
            }

            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static FileConfig fromFile(String filename) {
        FileConfig fc = new FileConfig();

        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(filename));

            fc.numberOfRecordsInPrimaryBlocks = new Integer(br.readLine());
            fc.numberOfRecordsInOverflowBlocks = new Integer(br.readLine());
            fc.blocksCount = new Integer(br.readLine());
            fc.splitPointer = new Integer(br.readLine());
            fc.fileLevel = new Integer(br.readLine());
            fc.entitySizeInBytes = new Integer(br.readLine());
            fc.firstLevelModulo = new Integer(br.readLine());
            fc.secondLevelModulo = new Integer(br.readLine());
            fc.blockSize = new Integer(br.readLine());
            fc.overflowBlockSize = new Integer(br.readLine());
            fc.numberOfRecords = new Integer(br.readLine());
            fc.splitVariant = new Integer(br.readLine());

            fc.databaseName = br.readLine();

            fc.maxThreshold = new Double(br.readLine());

            fc.entityCounter = new Integer(br.readLine());
            fc.offsetCounter = new Integer(br.readLine());
            fc.maxOffset = new Integer(br.readLine());
            fc.numberOfOverflowBlocksUsed = new Integer(br.readLine());


            fc.freeOverflowBlocksOffsets = new LinkedList<>();
            String line = br.readLine();
            while (line != null) {
                fc.freeOverflowBlocksOffsets.add(new Integer(line));
                line = br.readLine();
            }

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fc;
    }

}
