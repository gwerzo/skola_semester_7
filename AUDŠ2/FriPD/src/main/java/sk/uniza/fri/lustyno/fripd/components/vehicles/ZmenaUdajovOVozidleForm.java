package sk.uniza.fri.lustyno.fripd.components.vehicles;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.fripd.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.fripd.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.fripd.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.ZmenaUdajovVozidlaEntityUi;
import sk.uniza.fri.lustyno.fripd.service.UIServices;
import sk.uniza.fri.lustyno.fripd.service.VehicleService;
import sk.uniza.fri.lustyno.fripd.utils.CustomNotification;
import sk.uniza.fri.lustyno.fripd.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.fripd.utils.OperationMarker;
import sk.uniza.fri.lustyno.fripd.utils.converters.StringToIntegerConverter;

public class ZmenaUdajovOVozidleForm extends BusinessComponent {

    private VehicleService vehicleService;

    private Binder<ZmenaUdajovVozidlaEntityUi> binder;
    private ZmenaUdajovVozidlaEntityUi entity;

    public ZmenaUdajovOVozidleForm(RepaintableComponent parentComponent) {
        super(parentComponent, MENU_TITLE.VEHICLES_MENU_2.getTitle());
        this.vehicleService = UIServices.getVehicleService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new ZmenaUdajovVozidlaEntityUi();
        this.binder = new Binder<>(ZmenaUdajovVozidlaEntityUi.class);

        VerticalLayout layout = new VerticalLayout();

        FormLayout formLayout = new FormLayout();

        TextField evcKodTf = new TextField("EVČ");
        CheckBox vPatraniChB = new CheckBox("V hľadaní");
        DateField platnostTKTf = new DateField("Platnosť TK");
        DateField platnostEKTf = new DateField("Platnosť EK");
        TextField prevadzkovaHmotnostTf = new TextField("Prevadzkova hmotnosť");
        TextField pocetNapravTf = new TextField("Počet náprav");

        Button vytvorit = new Button("Editovať vozidlo");

        vytvorit.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                editujVozidlo(this.entity);
            } catch (ValidationException ex) {
                ex.printStackTrace();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        formLayout.addComponent(evcKodTf);
        formLayout.addComponent(vPatraniChB);
        formLayout.addComponent(platnostTKTf);
        formLayout.addComponent(platnostEKTf);
        formLayout.addComponent(prevadzkovaHmotnostTf);
        formLayout.addComponent(pocetNapravTf);
        formLayout.addComponent(vytvorit);

        layout.addComponent(formLayout);

        binder.forField(evcKodTf)
                .asRequired()
                .withValidator(new StringLengthValidator("Dĺžka EVČ kódu musí byť v rozmedzí 1 - 7", 1, 7))
                .bind(ZmenaUdajovVozidlaEntityUi::getEVC, ZmenaUdajovVozidlaEntityUi::setEVC);
        binder.forField(vPatraniChB)
                .bind(ZmenaUdajovVozidlaEntityUi::getVPatrani, ZmenaUdajovVozidlaEntityUi::setVPatrani);
        binder.forField(platnostTKTf)
                .asRequired()
                .bind(ZmenaUdajovVozidlaEntityUi::getDatumPlatnostiSTK, ZmenaUdajovVozidlaEntityUi::setDatumPlatnostiSTK);
        binder.forField(platnostEKTf)
                .asRequired()
                .bind(ZmenaUdajovVozidlaEntityUi::getDatumPlatnostiEK, ZmenaUdajovVozidlaEntityUi::setDatumPlatnostiEK);
        binder.forField(pocetNapravTf)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(ZmenaUdajovVozidlaEntityUi::getPocetNaprav, ZmenaUdajovVozidlaEntityUi::setPocetNaprav);
        binder.forField(prevadzkovaHmotnostTf)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(ZmenaUdajovVozidlaEntityUi::getPrevadzkovaHmostnost, ZmenaUdajovVozidlaEntityUi::setPrevadzkovaHmostnost);

        this.parenComponent.showComponent(new Panel("Editovanie vozidla", layout));
    }

    private void editujVozidlo(ZmenaUdajovVozidlaEntityUi entity) {

        HandledOperationExecutor.submit(() -> {
            this.vehicleService.updateVehicle(entity);
            CustomNotification.notification("Vozidlo bolo zmenené");
            this.entity = new ZmenaUdajovVozidlaEntityUi();
            binder.readBean(this.entity);
            return OperationMarker.OPERATION_SUCCESFUL;
        });
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.VEHICLES_MENU_2.getTitle();
    }

}
