package sk.uniza.fri.lustiak.auds2.kataster.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.CreateNewRealEstateUi;
import sk.uniza.fri.lustiak.auds2.kataster.utils.BusinessException;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Random;

@Service
@Slf4j
public class ApplicationInitializer {


    @Autowired
    private RealEstateService realEstateService;

    //@PostConstruct
    public OperationMarker initData() {

        Random rnd = new Random();

        for (int realEstateI = 0; realEstateI < 8; realEstateI++) {
            CreateNewRealEstateUi createNewRealEstateUi = new CreateNewRealEstateUi();
            createNewRealEstateUi.setSupisneCislo(realEstateI);
            createNewRealEstateUi.setPopis("Nehnuteľnosť " + realEstateI);
            createNewRealEstateUi.setIdentifikacneCislo(realEstateI);

            Double dLat = rnd.nextDouble() * 180;
            Double dLon = rnd.nextDouble() * 180;
            Boolean isNorth1 = rnd.nextBoolean();
            Boolean isEast1 = rnd.nextBoolean();

            Double dLat2 = rnd.nextDouble() * 180;
            Double dLon2 = rnd.nextDouble() * 180;
            Boolean isNorth2 = rnd.nextBoolean();
            Boolean isEast2 = rnd.nextBoolean();

            createNewRealEstateUi.setLatitude1(dLat);
            createNewRealEstateUi.setLongitude1(dLon);
            createNewRealEstateUi.setIsNorth1(isNorth1);
            createNewRealEstateUi.setIsEast1(isEast1);

            createNewRealEstateUi.setLatitude2(dLat2);
            createNewRealEstateUi.setLongitude2(dLon2);
            createNewRealEstateUi.setIsNorth2(isNorth2);
            createNewRealEstateUi.setIsEast2(isEast2);

            this.realEstateService.addNewRealEstate(createNewRealEstateUi);
        }
        return OperationMarker.OPERATION_SUCCESFUL;
    }

    public OperationMarker initData(int numberOfRealEstates) {

        Random rnd = new Random();

        for (int realEstateI = 0; realEstateI < numberOfRealEstates; realEstateI++) {
            CreateNewRealEstateUi createNewRealEstateUi = new CreateNewRealEstateUi();
            createNewRealEstateUi.setSupisneCislo(realEstateI);
            createNewRealEstateUi.setPopis("Nehnuteľnosť " + realEstateI);
            createNewRealEstateUi.setIdentifikacneCislo(rnd.nextInt(1000));

            Double dLat = rnd.nextDouble() * 180;
            Double dLon = rnd.nextDouble() * 180;
            Boolean isNorth1 = rnd.nextBoolean();
            Boolean isEast1 = rnd.nextBoolean();

            Double dLat2 = rnd.nextDouble() * 180;
            Double dLon2 = rnd.nextDouble() * 180;
            Boolean isNorth2 = rnd.nextBoolean();
            Boolean isEast2 = rnd.nextBoolean();

            createNewRealEstateUi.setLatitude1(dLat);
            createNewRealEstateUi.setLongitude1(dLon);
            createNewRealEstateUi.setIsNorth1(isNorth1);
            createNewRealEstateUi.setIsEast1(isEast1);

            createNewRealEstateUi.setLatitude2(dLat2);
            createNewRealEstateUi.setLongitude2(dLon2);
            createNewRealEstateUi.setIsNorth2(isNorth2);
            createNewRealEstateUi.setIsEast2(isEast2);

            try {
                this.realEstateService.addNewRealEstate(createNewRealEstateUi);
            } catch (BusinessException e) {
                // Handling, nech sa userovi nevyhodí hláška zo servisnej metódy o neúspešnom vložení a neprestane sa generovať
                log.warn("Generátor sa pokúsil vložiť už existujúcu nehnuteľnosť {}", createNewRealEstateUi.getIdentifikacneCislo());
            }

        }
        return OperationMarker.OPERATION_SUCCESFUL;
    }

}
