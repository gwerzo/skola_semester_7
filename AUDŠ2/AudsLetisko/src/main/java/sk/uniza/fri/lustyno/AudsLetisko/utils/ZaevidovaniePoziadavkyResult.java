package sk.uniza.fri.lustyno.AudsLetisko.utils;

import lombok.Data;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.LietadloEntity;
import sk.uniza.fri.lustyno.datastructures.queue.QueueNode;

@Data
public class ZaevidovaniePoziadavkyResult {

    private ZaevidovaniePoziadavkyMarker poziadavkaResult;

    private QueueNode<PrioritaACasPoziadavkyKey, LietadloEntity> nodeVoFronte;

}
