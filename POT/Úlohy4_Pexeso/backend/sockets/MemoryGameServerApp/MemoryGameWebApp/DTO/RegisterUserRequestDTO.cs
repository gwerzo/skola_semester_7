﻿namespace MemoryGameRestApi.DTO
{
    public class RegisterUserRequestDTO
    {
        public string Username { get; set; }
        public string Password1 { get; set; }
        public string Password2 { get; set; }

    }
}
