package sk.uniza.fri.lustyno.datastructures.linearhashing.file;

import sk.uniza.fri.lustyno.datastructures.entity.ByteSerializable;
import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;
import sk.uniza.fri.lustyno.datastructures.entity.Hashable;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureWrongUsageException;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.HasSize;


public abstract class Record<K extends HasComparableValue> implements ByteSerializable, Hashable {

    protected Boolean valid = Boolean.FALSE;

    public abstract K getKey();

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public boolean isValid() {
        return this.valid;
    }

    public static int getSize(Class<?> clazz) {
        if (clazz.isAnnotationPresent(HasSize.class)) {
            return clazz.getAnnotation(HasSize.class).sizeInBytes();
        } else {
            throw new DataStructureWrongUsageException("Record", "getSize",
                    "Trieda, ktorá má byť ukladaná do súboru musí byť oanotovaná s anotáciou HasSize");
        }
    }
}
