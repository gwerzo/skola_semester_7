package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditRealEstateUi {

    private Boolean editConstraints = Boolean.FALSE;

    private Integer identifikacneCislo;

    private Double latitude1;
    private Double longitude1;
    private Boolean isNorth1;
    private Boolean isEast1;

    private Double latitude2;
    private Double longitude2;
    private Boolean isNorth2;
    private Boolean isEast2;

    private Integer supisneCislo;
    private String popis;


}
