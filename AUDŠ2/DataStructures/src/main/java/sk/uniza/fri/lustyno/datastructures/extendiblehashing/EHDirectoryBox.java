package sk.uniza.fri.lustyno.datastructures.extendiblehashing;

public class EHDirectoryBox {

    public Integer indexInMainFile;
    public Integer localBlockLevel;
    public Long numberOfEntitiesInMainBlock;
    public Long numberOfEntitiesInOverflowBlock;
    public Long numberOfOverflowBlocks;

    public EHDirectoryBox() {
        this.indexInMainFile = 0;
        this.localBlockLevel = 1;
        this.numberOfEntitiesInMainBlock = 0L;
        this.numberOfEntitiesInOverflowBlock = 0L;
        this.numberOfOverflowBlocks = 0L;
    }

}
