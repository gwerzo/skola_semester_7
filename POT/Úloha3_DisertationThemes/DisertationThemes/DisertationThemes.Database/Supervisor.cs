﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DisertationThemes.Database
{
    public class Supervisor
    {
        public int SupervisorId { get; set; }
        public string FullName { get; set; }
        public List<Theme> Themes { get; set; }

        public Supervisor()
        {
            this.Themes = new List<Theme>();
        }

    }
}
