﻿using ComplexNumberLib;
using System;

namespace ComplexNumbersApp
{
    class Program
    {
        static void Main(string[] args)
        {
            String ex = "Musíte zadať 4 číselné parametre oddelené medzerami";

            if (args.Length < 4) {
                Console.WriteLine(ex);
                return;
            }
            double aReal;
            double aImag;
            double bReal;
            double bImag;
            try
            {
                aReal = double.Parse(args[0]);
                aImag = double.Parse(args[1]);
                bReal = double.Parse(args[2]);
                bImag = double.Parse(args[3]);
            }
            catch (Exception e) {
                Console.WriteLine(ex);
                return;
            }

            ComplexNumber x = new ComplexNumber(aReal, aImag);
            ComplexNumber y = new ComplexNumber(bReal, bImag);

            Console.WriteLine("x:");
            Console.WriteLine("\tGeometric form:\t" + x.ToString(ComplexNumberFormat.GeometricForm));
            Console.WriteLine("\tAlgebraic form:\t" + x.ToString(ComplexNumberFormat.AlgebraicForm));
            Console.WriteLine("\tTrigoniometric form:\t" + x.ToString(ComplexNumberFormat.TrigoniometricForm));


            Console.WriteLine("y:");
            Console.WriteLine("\tGeometric form:\t" + y.ToString(ComplexNumberFormat.GeometricForm));
            Console.WriteLine("\tAlgebraic form:\t" + y.ToString(ComplexNumberFormat.AlgebraicForm));
            Console.WriteLine("\tTrigoniometric form:\t" + y.ToString(ComplexNumberFormat.TrigoniometricForm));

            Console.WriteLine("x == y = " + (x == y));
            Console.WriteLine("x != y = " + (x != y));

            Console.WriteLine("x == ComplexNumber.ZERO = " + (x == ComplexNumber.ZERO));
            Console.WriteLine("x == ComplexNumber.ONE = " + (x == ComplexNumber.ONE));
            Console.WriteLine("x == ComplexNumber.IMAGINARY_ONE = " + (x == ComplexNumber.IMAGINARY_ONE));

            Console.WriteLine("x + y = " + (x + y).ToString());
            Console.WriteLine("x - y = " + (x - y).ToString());
            Console.WriteLine("x * y = " + (x * y).ToString());
            Console.WriteLine("x / y = " + (x / y).ToString());

        }
    }
}
