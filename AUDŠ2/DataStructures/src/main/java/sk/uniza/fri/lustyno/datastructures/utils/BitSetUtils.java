package sk.uniza.fri.lustyno.datastructures.utils;


public class BitSetUtils {

    /**
     * Konvertuje integer na
     * {@link CustomBitSet}
     * Vráti ale hodnoty ODZADU!!!
     *
     * @param fromInteger - zdrojový integer
     * @param numberOfBits - koľko bitov sa má do CustomBitSetu pridať z integeru
     */
    public static CustomBitSet getBitsOfInt(int fromInteger, int numberOfBits) {
        CustomBitSet bits = new CustomBitSet(numberOfBits);
        int index = 0;
        while (fromInteger != 0 && index < numberOfBits) {
            if (fromInteger % 2 != 0) {
                bits.set(index, true);
            } else {
                bits.set(index, false);
            }
            ++index;
            fromInteger = fromInteger >>> 1;
        }
        return bits;
    }

    /**
     * Konvertuje CustomBitSet na int
     * @param fromCustomBitSet - Zdrojový CustomBitSet
     */
    public static int fromCustomBitSet(CustomBitSet fromCustomBitSet) {
        int value = 0;
        for (int i = 0; i < fromCustomBitSet.length(); ++i) {
            boolean d = fromCustomBitSet.get(i);
            if (d) {
                value = (value << 1) + 1;
            } else {
                value = value << 1;
            }
        }
        return value;
    }

    public static boolean equalsOmitLastNBits(Integer int1, Integer int2, int numberOfLSFBitsToOmit) {
        int shifted1 = int1 >>> numberOfLSFBitsToOmit;
        int shifted2 = int2 >>> numberOfLSFBitsToOmit;
        return shifted1 == shifted2;
    }

    /**
     * Invertne jeden bit v čísle (n) na niekoľkej pozícii sprava (k),
     * a vráti takto novo vytvorené číslo
     *
     * @param number - z akého čísla
     * @param nThLSBPosition - koľký bit sa má obrátiť sprava (najmenej dôležitý sprava)
     *                       pričom číslovanie bitov začína od 0,
     *                       t.j. ak napr.
     *                       n = 1111 a k = 0 ==> return 1110
     *                       n = 1111 a k = 1 ==> return 1101
     */
    public static int invertBit(Integer number, int nThLSBPosition) {
        int XOR = 1;
        for (int i = 0; i < nThLSBPosition; i++) {
            XOR = XOR << 1;
        }
        return number ^ XOR;
    }

}
