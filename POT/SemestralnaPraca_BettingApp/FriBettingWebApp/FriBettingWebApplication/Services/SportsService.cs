﻿using System.Collections.Generic;
using System.Linq;
using FriBettingDatabaseApp.Repository.Base;
using FriBettingWebApplication.Dto.Response;
using FriBettingWebApplication.Services.Face;

namespace FriBettingWebApplication.Services
{
    public class SportsService: ISportsService
    {
        private readonly ISportRepository _sportsRepository;

        public SportsService(ISportRepository sportsRepository)
        {
            _sportsRepository = sportsRepository;
        }

        public List<SportResponse> ListAllSports()
        {

            return _sportsRepository.GetAll().Select(e => new SportResponse
            {
                SportId = e.SportId,
                SportName = e.SportName
            }).ToList();
        }

        public List<OddTypeResponse> ListAllOddTypesForSport(int sportId)
        {
            return _sportsRepository.GetAllOddTypesForSport(sportId).Select(e => new OddTypeResponse
                {
                    OddTypeId = e.OddTypeId,
                    OddTypeCode = e.Code,
                    OddTypeDesc = e.OddTypeDescription
                }).ToList();
        }

    }
}