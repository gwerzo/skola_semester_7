package sk.uniza.fri.lustyno.fripd.entities_ui.vehicle;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class VyhladanieVozidlaEntityUi {

    private String vin;
    private String evc;

}
