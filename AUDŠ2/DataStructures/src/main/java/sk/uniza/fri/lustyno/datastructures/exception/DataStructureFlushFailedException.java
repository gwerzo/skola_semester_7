package sk.uniza.fri.lustyno.datastructures.exception;

public class DataStructureFlushFailedException extends RuntimeException {

    public DataStructureFlushFailedException(String databaseName) {
        super("Nepodarilo sa zavrieť súbory pre databázu " + databaseName + "\n" +
                "Je možné že posledné dáta boli stratené");
    }

}
