package sk.uniza.fri.lustyno.AudsLetisko.components.common;

public abstract class BusinessComponent implements ParentComponent {

    protected RepaintableComponent parenComponent;

    protected String componentTitle;

    protected BusinessComponent(RepaintableComponent parentComponent, String componentTitle) {
        this.parenComponent = parentComponent;
        this.componentTitle = componentTitle;
    }

    public abstract boolean isImplemented();
}
