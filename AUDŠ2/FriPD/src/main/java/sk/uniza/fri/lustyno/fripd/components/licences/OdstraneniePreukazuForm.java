package sk.uniza.fri.lustyno.fripd.components.licences;

import sk.uniza.fri.lustyno.fripd.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.fripd.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.fripd.components.common.RepaintableComponent;

public class OdstraneniePreukazuForm extends BusinessComponent {

    public OdstraneniePreukazuForm(RepaintableComponent parentComponent) {
        super(parentComponent, MENU_TITLE.CARDS_MENU_4.getTitle());
    }

    @Override
    public boolean isImplemented() {
        return false;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

    }

    @Override
    public String getComponentTitle() {
        return  MENU_TITLE.CARDS_MENU_4.getTitle();
    }
}
