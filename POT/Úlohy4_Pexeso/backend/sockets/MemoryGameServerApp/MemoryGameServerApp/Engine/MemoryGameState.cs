﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PexesoApi.Bo;

namespace MemoryGameServerApp.Engine
{
    /// <summary>
    /// Trieda, ktorá drží stav hry
    /// </summary>
    public class MemoryGameState
    {

        // UUID hry počas jej behu - nie je ukladané do DB
        public string GameUuid { get; set; }

        // Reprezentácia stavu hry
        public GameState GameStateValue { get; set; }

        // Referencia na hráča č.1
        public string Player1 { get; set; }

        // Počet uhádnutých párov hráča 1
        public int Player1PairsCount { get; set; }

        // Referencia na hráča č. 2
        public string Player2 { get; set; }

        // Počet uhádnutých párov hráča 2
        public int Player2PairdCount { get; set; }

        // Šírka hracieho poľa hry
        public int WidthOfGame { get; set; }

        // Výška hracieho poľa hry
        public int HeightOfGame { get; set; }

        // Reprezentuje, kto je na ťahu (false = Player1, true = Player2)
        public bool PlayerMove { get; set; }

        // Reprezentuje, či sa jedná o (false = prvý ťah, true = druhý ťah)
        public bool MoveStep { get; set; }

        // Hracia plocha, reprezentovaná 2D poľom intov (inty = IDčká obrázkov/znakov/..)
        public CardField[,] GameBoard { get; set; }

        // Seed hry (kvôli nutnosti zrekonštruovania hracej plochy - redukuje nutnosť ukladania celej hrac. plochy)
        public Random GameSeed { get; set; }

        // IDčka kariet, z ktorých môže byť vygenerované hracie pole
        public List<int> CardIds { get; set; }

        public int GameStyleId { get; set; }

        public int GameTypeId { get; set; }

        public int TemporaryXOpened { get; set; }
        public int TemporaryYOpened { get; set; }

        public int OpenedPairsCounter { get; set; }

        // Stopwatch pre začiatok hry
        public long GameStartTime { get; set; }

        // Stopwatch pre posledný ťah
        public Stopwatch LastMoveStopwatch { get; set; }

        // Krokovač - udáva koľký krok v hre sa vykonáva
        public int StepCounter { get; set; }

        // Zoznam krokov vykonaných používateľmi
        public List<MemoryGameStep> Steps { get; set; }

        /// <summary>
        /// Konštruktor pre vytvorenie novej hry podľa predaných parametrov
        /// </summary>
        /// <param name="player1">Hráč 1</param>
        /// <param name="player2">Hráč 2</param>
        /// <param name="widthOfGame">Šírka hracieho poľa</param>
        /// <param name="heightOfGame">Výška hracieho poľa</param>
        /// <param name="cardIds">ID kariet (obrázkov/zvukov/..pod kartami)</param>
        public MemoryGameState(string player1, string player2, int widthOfGame, int heightOfGame, List<int> cardIds, int randomSeed,
            int gameStyleId, int gameTypeId)
        {
            this.Player1 = player1;
            this.Player2 = player2;
            this.WidthOfGame = widthOfGame;
            this.HeightOfGame = heightOfGame;
            this.PlayerMove = false;
            this.MoveStep = false;
            this.CardIds = cardIds;
            this.GameSeed = new Random(randomSeed);
            this.GameStyleId = gameStyleId;
            this.GameTypeId = gameTypeId;
            this.Steps = new List<MemoryGameStep>();

            if (this.Player2 == null)
            {
                this.GameStateValue = GameState.PENDING_RANDOM;
            }
            else
            {
                this.GameStateValue = GameState.PENDING_OPPONENT;
            }

            this.GameBoard = new CardField[this.WidthOfGame, this.HeightOfGame];

            this.GenerateGameBoard();

        }

        /// <summary>
        /// Vygeneruje herné pole pre hru cez seed tejto hry
        /// </summary>
        private void GenerateGameBoard()
        {
            if (this.CardIds.Count == 0)
            {
                throw new Exception("Na vygenerovanie hracieho poľa musí existovať aspoň jedno ID karty");
            }

            // Shuffle pozícií cez fixný seed
            var positions = Enumerable.Range(0, this.WidthOfGame * this.HeightOfGame).ToList();
            positions = positions.OrderBy(x => this.GameSeed.Next()).ToList();

            int cardChanger = 0;
            for (var cardPosition = 0; cardPosition < positions.Count; cardPosition += 2)
            {
                // Pozícia prvej karty do ktorej bude umiestnené ID X
                int pos1 = positions[cardPosition];
                int pos1_X = pos1 % this.WidthOfGame;
                int pos1_Y = pos1 / this.WidthOfGame;

                // Pozícia druhej karty do ktorej bude umiestnené rovnaké ID X
                int pos2 = positions[cardPosition + 1];
                int pos2_X = pos2 % this.WidthOfGame;
                int pos2_Y = pos2 / this.WidthOfGame;

                int cardId = this.CardIds[cardChanger++ % this.CardIds.Count];

                CardField cf1 = new CardField();
                cf1.IdValue = cardId;

                CardField cf2 = new CardField();
                cf2.IdValue = cardId;

                this.GameBoard[pos1_X, pos1_Y] = cf1;
                this.GameBoard[pos2_X, pos2_Y] = cf2;
            }

        }

        /// <summary>
        /// Zruší otvorené karty v aktuálnom ťahu
        /// </summary>
        public void ResetTemporaryOpens()
        {
            this.GameBoard[TemporaryXOpened, TemporaryYOpened].IsOpenedTemporarily = false;
        }

        /// <summary>
        /// Hodnotu karty, ktorá je otvorená "temporary" - prvá otvorená
        /// </summary>
        public int GetTemporarilyOpenedValue()
        {
            return this.GameBoard[TemporaryXOpened, TemporaryYOpened].IdValue;
        }


        public class CardField
        {
            public bool IsOpened { get; set; }
            public bool IsOpenedTemporarily { get; set; }
            public int IdValue { get; set; }

            public CardField()
            {
                this.IsOpened = false;
                this.IsOpenedTemporarily = false;
                this.IdValue = -1;
            }

        }

        public enum GameState
        {
            PENDING_RANDOM,
            PENDING_OPPONENT,
            RUNNING
        }

    }
}
