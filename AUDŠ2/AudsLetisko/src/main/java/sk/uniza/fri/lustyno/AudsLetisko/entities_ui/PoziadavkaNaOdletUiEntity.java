package sk.uniza.fri.lustyno.AudsLetisko.entities_ui;

import lombok.Data;

@Data
public class PoziadavkaNaOdletUiEntity {

    private String medzinarodnyKodLietadla;

    public void flush() {
        this.medzinarodnyKodLietadla = "";
    }

}
