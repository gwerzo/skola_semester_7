﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DisertationThemes.ImporterApp
{
    class DisertationThemeLine
    {
        public string ThemeName { get; set; }
        public string Supervisor { get; set; }
        public string StProgram { get; set; }
        public string FieldOfStudy { get; set; }
        public bool IsFullTime { get; set; }
        public bool IsExternal { get; set; }
        public string ResearchType { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }

        public DisertationThemeLine(string csvLine)
        {
            var splitted = csvLine.Split(';');
            if (splitted.Length != 9)
            {
                throw new Exception("Riadok sa nepodarilo rozparsovať, nemá predpísaný počet stĺpcov (9)");
            }


            this.ThemeName = splitted[0];
            this.Supervisor = splitted[1];
            this.StProgram = splitted[2];
            this.FieldOfStudy = splitted[3];
            this.IsFullTime = splitted[4].Equals("TRUE");
            this.IsExternal = splitted[5].Equals("TRUE");
            this.ResearchType = splitted[6];
            this.Description = splitted[7];
            this.Created = DateTime.Parse(splitted[8]);
        }

    }
}
