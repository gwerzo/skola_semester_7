package sk.uniza.fri.lustyno.fripd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FriPDApplication {
	public static void main(String[] args) {
		SpringApplication.run(FriPDApplication.class, args);
	}

}
