package sk.uniza.fri.lustyno.datastructures.linearhashing.file;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureEntityNotFoundException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureInsertDuplicateException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureNullKeyException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureOperationFailedException;
import sk.uniza.fri.lustyno.datastructures.linearhashing.config.FileConfig;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.Constants;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.FromBytesFactory;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.SPLIT_VARIANT;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Trieda File reprezentujúca menežment
 * Lineárneho Hashovania
 * @param <R> - Typ záznamov v súbore
 * @param <K> - Typ kľúča
 */
public class File<K extends HasComparableValue, R extends Record<K>> {

    private final Boolean LOGGING_ALLOWED = false;

    private final Class<?> clazz;

    private final Integer numberOfRecordsInPrimaryBlocks;

    private final Integer numberOfRecordsInOverflowBlocks;

    private Integer blocksCount = Constants.INITIAL_BLOCKS_COUNT;

    private Integer splitPointer = 0;

    private Integer fileLevel = 0;

    private FileHandler fh;

    private FromBytesFactory<R> entityFactory;

    private Integer entitySizeInBytes;

    private Integer firstLevelModulo;

    private Integer secondLevelModulo;

    private Integer blockSize;

    private Integer overflowBlockSize;

    private Integer numberOfRecords = 0;

    private OverflowBlocksHandler overflowBlocksHandler;

    private SPLIT_VARIANT splitVariant;

    public File(String databaseName, Integer numberOfRecordsInPrimaryBlocks,
                Integer numberOfRecordsInOverflowBlocks, Class<?> clazz,
                SPLIT_VARIANT splitVariant,
                FromBytesFactory<R> factory) {

        this.clazz = clazz;
        this.numberOfRecordsInOverflowBlocks = numberOfRecordsInOverflowBlocks;
        this.numberOfRecordsInPrimaryBlocks = numberOfRecordsInPrimaryBlocks;
        this.entitySizeInBytes = R.getSize(clazz);
        this.splitVariant = splitVariant;
        this.entityFactory = factory;

        this.blockSize = Block.BLOCK_INFO_SIZE_BYTES + numberOfRecordsInPrimaryBlocks * this.entitySizeInBytes;
        this.overflowBlockSize = OverflowBlock.OVERFLOW_BLOCK_INFO_SIZE_BYTES + numberOfRecordsInOverflowBlocks * this.entitySizeInBytes;

        this.firstLevelModulo = new Double(this.blocksCount * Math.pow(2, this.fileLevel)).intValue();
        this.secondLevelModulo = new Double(this.blocksCount * Math.pow(2, this.fileLevel + 1)).intValue();

        this.overflowBlocksHandler = new OverflowBlocksHandler(this.overflowBlockSize);

        this.fh = new FileHandler(databaseName, this.entitySizeInBytes , numberOfRecordsInPrimaryBlocks, numberOfRecordsInOverflowBlocks, blocksCount);
    }

    public File(FileConfig fromConfig, Class<?> clazz, FromBytesFactory<R> factory) {
        this.clazz = clazz;
        this.numberOfRecordsInOverflowBlocks = fromConfig.numberOfRecordsInOverflowBlocks;
        this.numberOfRecordsInPrimaryBlocks = fromConfig.numberOfRecordsInPrimaryBlocks;
        this.entitySizeInBytes = fromConfig.entitySizeInBytes;
        this.splitVariant =
                fromConfig.splitVariant == 0 ? SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR
                        : fromConfig.splitVariant == 1 ? SPLIT_VARIANT.SPLIT_VARIANT_NUMBER_OF_OVERFLOW_BLOCKS : SPLIT_VARIANT.SPLIT_VARIANT_RATIO_PRIMARY_OVERFLOW_BLOCKS;
        this.entityFactory = factory;
        this.blockSize = Block.BLOCK_INFO_SIZE_BYTES + fromConfig.numberOfRecordsInPrimaryBlocks * this.entitySizeInBytes;
        this.overflowBlockSize = OverflowBlock.OVERFLOW_BLOCK_INFO_SIZE_BYTES + fromConfig.numberOfRecordsInOverflowBlocks * this.entitySizeInBytes;
        this.firstLevelModulo = fromConfig.firstLevelModulo;
        this.secondLevelModulo = fromConfig.secondLevelModulo;
        this.overflowBlocksHandler = new OverflowBlocksHandler(fromConfig);
        this.fh = new FileHandler(fromConfig);
    }

    public void saveConfiguration() {
        FileConfig fileConfig = getActualConfiguration();
        fileConfig.saveToFile();
    }

    public FileConfig getActualConfiguration() {
        FileConfig fc = new FileConfig();
        fc.numberOfRecordsInPrimaryBlocks = this.numberOfRecordsInPrimaryBlocks;
        fc.numberOfRecordsInOverflowBlocks = this.numberOfRecordsInOverflowBlocks;
        fc.blocksCount = this.blocksCount;
        fc.splitPointer = this.splitPointer;
        fc.fileLevel = this.fileLevel;
        fc.entitySizeInBytes = this.entitySizeInBytes;
        fc.firstLevelModulo = this.firstLevelModulo;
        fc.secondLevelModulo = this.secondLevelModulo;
        fc.blockSize = this.blockSize;
        fc.overflowBlockSize = this.overflowBlockSize;
        fc.numberOfRecords = this.numberOfRecords;
        if (this.splitVariant.equals(SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR)) {
            fc.splitVariant = 0;
        } else if (this.splitVariant.equals(SPLIT_VARIANT.SPLIT_VARIANT_NUMBER_OF_OVERFLOW_BLOCKS)) {
            fc.splitVariant = 1;
        } else {
            fc.splitVariant = 2;
        }

        this.fh.fillFileConfig(fc);
        this.overflowBlocksHandler.fillFileConfig(fc);

        return fc;
    }

    /**
     * Nájdenie záznamu podľa predaného parametra
     * Record musí mať nastavené UNIKÁATNE atribúty,
     * z ktorých sa robí hash v danej entite
     *
     * @return nájdená entita v súbore, resp. NULL ak je nenájdená
     */
    public R find(R record) {
        int blockNumber;
        int recordHash = record.getHash();

        if (recordHash % this.firstLevelModulo < this.splitPointer) {
            blockNumber = record.getHash() % this.secondLevelModulo;
        } else {
            blockNumber = record.getHash() % this.firstLevelModulo;
        }

        Block<R> blok = this.getBlockByIndex(blockNumber);
        for (Record r : blok.getRecordsList()) {
            if (r.valid && r.getKey().getValue().equals(record.getKey().getValue())) {
                return (R)r;
            }
        }
        if (blok.getOverflowBlockOffset() != -1) {
            OverflowBlock<R> ob = this.getOverflowBlockByOffset(blok.getOverflowBlockOffset());
            for (Record r : ob.getRecordList()) {
                if (r.getKey().getValue().equals(record.getKey().getValue())) {
                    return (R)r;
                }
            }
            while (ob.getNextOverflowBlock() != null && ob.getNextOverflowBlock().getOverflowOffset() != -1) {
                ob = this.getOverflowBlockByOffset(ob.getNextOverflowBlock().getOverflowOffset());
                for (Record r : ob.getRecordList()) {
                    if (r.getKey().getValue().equals(record.getKey().getValue())) {
                        return (R)r;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Update záznamu, ktorý podporuje
     * iba zmenu nekľúčových atribútov
     * Kľúč samotný (kompozit n atribútov)
     * entity sa použije na vyhľadanie
     * entity, ktorú treba updatnúť,
     * t.j. musí byť správne nastavený
     *
     * Ostatné atribúty sa nastavia tak,
     * ako sú nastavené priamo v entite
     * z predaného parametra
     *
     * @param record
     * @throws
     */
    public void update(Record record) {
        if (record == null) {
            throw new DataStructureNullKeyException(this.getClass().getName(), "update");
        }

        int blockNumber;
        int recordHash = record.getHash();

        int offsetToChange = -1;
        boolean isPrimaryBlock = true;

        if (recordHash % this.firstLevelModulo < this.splitPointer) {
            blockNumber = record.getHash() % this.secondLevelModulo;
        } else {
            blockNumber = record.getHash() % this.firstLevelModulo;
        }

        int recordNumberInBlock = 0;

        // Ak je prvok v primárnom bloku
        Block<R> blok = this.getBlockByIndex(blockNumber);
        for (Record r : blok.getRecordsList()) {
            if (r.getKey().getValue().equals(record.getKey().getValue())) {
                // Výpočet offsetu v hlavnom súbore pre entitu
                offsetToChange = blockNumber * this.blockSize + Block.BLOCK_INFO_SIZE_BYTES + recordNumberInBlock * this.entitySizeInBytes;
                isPrimaryBlock = true;
                break;
            }
            recordNumberInBlock++;
        }

        // Ak je prvok v overflow blokoch
        if (offsetToChange == -1) {
            if (blok.getOverflowBlockOffset() != -1) {
                OverflowBlock<R> ob = this.getOverflowBlockByOffset(blok.getOverflowBlockOffset());
                recordNumberInBlock = 0;
                for (Record r : ob.getRecordList()) {
                    if (r.getKey().getValue().equals(record.getKey().getValue())) {
                        // Výpočet offsetu v overflow súbore pre entitu
                        offsetToChange = ob.getOverflowOffset() + OverflowBlock.OVERFLOW_BLOCK_INFO_SIZE_BYTES + recordNumberInBlock * this.entitySizeInBytes;
                        isPrimaryBlock = false;
                        break;
                    }
                    recordNumberInBlock++;
                }
                // Ak nebol nájdený ani v overflow bloku priamo po primárnom bloku, hľadaj ďalej reťazou blokov
                if (offsetToChange == -1) {
                    while (ob.getNextOverflowBlock() != null && ob.getNextOverflowBlock().getOverflowOffset() != -1) {
                        ob = this.getOverflowBlockByOffset(ob.getNextOverflowBlock().getOverflowOffset());
                        recordNumberInBlock = 0;
                        for (Record r : ob.getRecordList()) {
                            if (r.getKey().getValue().equals(record.getKey().getValue())) {
                                offsetToChange = ob.getOverflowOffset() + OverflowBlock.OVERFLOW_BLOCK_INFO_SIZE_BYTES + recordNumberInBlock * this.entitySizeInBytes;
                                isPrimaryBlock = false;
                                break;
                            }
                            recordNumberInBlock++;
                        }
                    }
                }

            }
        }

        if (offsetToChange == -1) {
            throw new DataStructureEntityNotFoundException(this.getClass().getName(), "update", record.getKey().getValue().toString());
        }

        if (isPrimaryBlock) {
            this.fh.writeToMainFile(record.toBytesArray(), offsetToChange);
        } else {
            this.fh.writeToOverflowFile(record.toBytesArray(), offsetToChange);
        }
    }

    /**
     * Vloženie záznamu
     * @param record
     */
    public void saveRecord(R record) {
        this.log("\n\nSaving entity with key: " + record.getKey().getValue());
        if (record == null) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "saveRecord", "Nemôžete vkladať NULL objekt");
        }

        Record foundAlready = this.find(record);
        if (foundAlready != null) {
            throw new DataStructureInsertDuplicateException(this.getClass().getName(), record.getKey().getValue().toString(), "");
        }

        int hash = record.getHash();

        int blockNumber;
        if (hash % this.firstLevelModulo < this.splitPointer) {
            blockNumber = hash % this.secondLevelModulo;
        } else {
            blockNumber = hash % this.firstLevelModulo;
        }
        this.log("\tRecord is being saved into " + blockNumber + " block");
        record.valid = true;

        Block<R> block = getBlockByIndex(blockNumber);
        if (block.validRecordsCount() < this.numberOfRecordsInPrimaryBlocks) {
            this.log("\t\tRecord saved into block directly as it wasn't full yet");
            block.insertRecordToInvalidPlace(record);
            this.fh.writeToMainFile(block.toBytesArray(), blockNumber * this.blockSize);
        } else {
            this.log("\t\tRecord has to be saved into overflow block as primary block is full");
            // Ak je hlavný blok v súbore plný
            Integer overflowBlockOffset = block.getOverflowBlockOffset();
            if (overflowBlockOffset < 0) {
                overflowBlockOffset = this.overflowBlocksHandler.getFirstEmptyOffset();
                this.log("\t\tPrimary block did not have overflow block so the one with offset " + overflowBlockOffset + " was chosen");
            }
            this.log("\t\t\tCurrent overflow block offset is " + overflowBlockOffset);
            OverflowBlock<R> overflowBlock = this.getOverflowBlockByOffset(overflowBlockOffset);

            // Ak doteraz nemal overflow blok
            if (block.getOverflowBlockOffset() < 0) {
                overflowBlock.setNextOverflowBlock(new OverflowBlock(-1, this.numberOfRecordsInOverflowBlocks, this.entityFactory));
                block.setOverflowBlock(overflowBlock);
                this.fh.writeToMainFile(block.toBytesArray(), blockNumber * this.blockSize);
                this.overflowBlocksHandler.increaseNumberOfBlocksUsed();
            }

            while (overflowBlock.isFull()) {
                this.log("\t\t\tOverflow block was full and a new one has to be found for save");
                if (overflowBlock.getNextOverflowBlock() == null || overflowBlock.getNextOverflowBlock().getOverflowOffset() < 0) {
                    OverflowBlock<R> newBlock = new OverflowBlock<>(this.overflowBlocksHandler.getFirstEmptyOffset(), this.numberOfRecordsInOverflowBlocks, this.entityFactory);
                    newBlock.setNextOverflowBlock(new OverflowBlock(-1, this.numberOfRecordsInOverflowBlocks));
                    overflowBlock.setNextOverflowBlock(newBlock);
                    saveOverflowBlock(overflowBlock);
                    overflowBlock = newBlock;
                    this.overflowBlocksHandler.increaseNumberOfBlocksUsed();
                    this.log("\t\t\t\tOverflow block did not have next overflow block so new one with offset " + newBlock.getOverflowOffset() + " was chosen");
                } else {
                    overflowBlock = this.getOverflowBlockByOffset(overflowBlock.getNextOverflowBlock().getOverflowOffset());
                    this.log("\t\t\t\tOverflow block had next overflow block in chain - going into block with offset " + overflowBlock.getOverflowOffset());
                }
            }
            this.log("\t\t\t\tRecord " + record.getKey().getValue() + " saved into overflow block with offset " + overflowBlock.getOverflowOffset());
            overflowBlock.insertRecordToInvalidPlace(record);
            this.saveOverflowBlock(overflowBlock);
        }
        this.numberOfRecords++;

        if (this.splitVariant == SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR) {
            if (this.getLoadFactor() > this.overflowBlocksHandler.MAX_THRESHOLD) {
                split();
            }
        } else if (this.splitVariant == SPLIT_VARIANT.SPLIT_VARIANT_NUMBER_OF_OVERFLOW_BLOCKS) {
            if (this.overflowBlocksHandler.getNumberOfOverflowBlocksUsed() > this.firstLevelModulo) {
                split();
            }
        } else {
            if ((1.0 * this.overflowBlocksHandler.getNumberOfOverflowBlocksUsed() / this.firstLevelModulo) > 0.75) {
                split();
            }
        }
    }

    /**
     * Zavolanie splitu nad súborom,
     * t.j. rozdelenie bloku na ktorý ukazuje splitpointer
     *
     */
    private void split() {
        Block<R> mainBlockToSplit = this.getBlockByIndex(this.splitPointer);
        this.log("\t\t\t\t\t\tSPLIT initiated");
        List<OverflowBlockUsedWrapper> overflowBlocks = new LinkedList<>();
        Integer overflowBlockOffset = mainBlockToSplit.getOverflowBlockOffset();
        while (overflowBlockOffset >= 0) {
            this.log("\t\t\t\t\t\t\tChaining overflow block with offset " + overflowBlockOffset);
            OverflowBlock<R> overflowBlock = this.getOverflowBlockByOffset(overflowBlockOffset);
            overflowBlocks.add(new OverflowBlockUsedWrapper(overflowBlock, false));
            overflowBlockOffset = overflowBlock.getNextOverflowBlock().getOverflowOffset();
        }

        List<R> allRecords = new ArrayList<>();
        allRecords.addAll(mainBlockToSplit.getRecordsList());
        mainBlockToSplit.clearBlock(this.entityFactory);
        for (OverflowBlockUsedWrapper wrapper : overflowBlocks) {
            allRecords.addAll(wrapper.getOverflowBlock().getRecordList());
            wrapper.getOverflowBlock().clearBlock(this.entityFactory);
        }

        allRecords = allRecords.stream().filter(e -> e.valid).collect(Collectors.toList());
        this.log("\t\t\t\t\t\tSplit deals with " + allRecords.size() + " records");

        List<R> firstGroupRecords = new ArrayList<>();
        List<R> secondGroupRecords = new ArrayList<>();

        int blockIndex = -1;
        for (R r : allRecords) {
            blockIndex = r.getHash() % this.secondLevelModulo;
            if (blockIndex < this.firstLevelModulo) {
                firstGroupRecords.add(r);
            } else {
                secondGroupRecords.add(r);
            }
        }
        LinkedList<OverflowBlock> freeOverflowBlocks = new LinkedList<>(
                overflowBlocks.stream().map(e -> e.getOverflowBlock()).collect(Collectors.toList()));
        LinkedList<OverflowBlock> usedOverflowBlocks = new LinkedList<>();

        Block<R> newPrimaryBlock = new Block<>(this.numberOfRecordsInPrimaryBlocks, this.numberOfRecordsInOverflowBlocks, this.entityFactory);
        newPrimaryBlock.setOverflowBlock(new OverflowBlock<>(-1, this.numberOfRecordsInOverflowBlocks));

        for (R recOfGroupOne : firstGroupRecords) {
            this.log("\t\t\t\t\t\t\t\tINSERTING INTO MAIN BLOCK");
            this.log("\t\t\t\t\t\t\t\tRecord being inserted " + recOfGroupOne.getKey().getValue());
            if (mainBlockToSplit.isFull()) {
                this.log("\t\t\t\t\t\t\t\t\tPrimary block was full, going deeper");
                if (mainBlockToSplit.getOverflowBlockOffset() > -1) {
                    // Blok už má overflow blok
                    OverflowBlock ob = mainBlockToSplit.getOverflowBlock();
                    this.log("\t\t\t\t\t\t\t\t\t\tPrimary block had overflow block with offset " + ob.getOverflowOffset());
                    while (ob.isFull()) {
                        if (ob.getNextOverflowBlock() == null || ob.getNextOverflowBlock().getOverflowOffset() < 0) {
                            OverflowBlock join = freeOverflowBlocks.removeFirst();
                            usedOverflowBlocks.add(join);
                            ob.setNextOverflowBlock(join);
                            ob = join;
                            this.log("\t\t\t\t\t\t\t\t\t\t\tOverflow block was full and had no next overflow block in chain so one with offset " + ob.getOverflowOffset() + " was chosen");
                        } else {
                            ob = ob.getNextOverflowBlock();
                            this.log("\t\t\t\t\t\t\t\t\t\t\tOverflow block was full and had next overflow block in chain with offset " + ob.getOverflowOffset());
                        }
                    }
                    this.log("\t\t\t\t\t\t\t\t\t\tRecord inserted into overflow block " + ob.getOverflowOffset());
                    ob.insertRecordToInvalidPlace(recOfGroupOne);
                } else {
                    // Blok nemá overflow blok
                    OverflowBlock ob = freeOverflowBlocks.removeFirst();
                    usedOverflowBlocks.add(ob);
                    mainBlockToSplit.setOverflowBlock(ob);
                    ob.insertRecordToInvalidPlace(recOfGroupOne);
                    this.log("\t\t\t\t\t\t\t\t\t\tPrimary block had no overflow block so the one with offset " + ob.getOverflowOffset() + " was chosen and record was inserted into it");
                }
            } else {
                this.log("\t\t\t\t\t\t\t\t\tPrimary block was not full, inserting " + recOfGroupOne.getKey().getValue() + " into block " + blockIndex);
                mainBlockToSplit.insertRecordToInvalidPlace(recOfGroupOne);
            }
        }

        for (R recOfGroupTwo : secondGroupRecords) {
            this.log("\n\t\t\t\t\t\t\t\tINSERTING INTO NEW PRIMARY BLOCK");
            this.log("\t\t\t\t\t\t\t\tRecord being inserted " + recOfGroupTwo.getKey().getValue());
            if (newPrimaryBlock.isFull()) {
                this.log("\t\t\t\t\t\t\t\t\tPrimary block was full, going deeper");
                if (newPrimaryBlock.getOverflowBlockOffset() > -1) {
                    // Blok už má overflow blok
                    OverflowBlock ob = newPrimaryBlock.getOverflowBlock();
                    this.log("\t\t\t\t\t\t\t\t\t\tPrimary block had overflow block with offset " + ob.getOverflowOffset());
                    while (ob.isFull()) {
                        if (ob.getNextOverflowBlock() == null || ob.getNextOverflowBlock().getOverflowOffset() < 0) {
                            OverflowBlock join = freeOverflowBlocks.removeFirst();
                            usedOverflowBlocks.add(join);
                            ob.setNextOverflowBlock(join);
                            ob = join;
                            this.log("\t\t\t\t\t\t\t\t\t\t\tOverflow block was full and had no next overflow block in chain so one with offset " + ob.getOverflowOffset() + " was chosen");
                        } else {
                            ob = ob.getNextOverflowBlock();
                            this.log("\t\t\t\t\t\t\t\t\t\t\tOverflow block was full and had next overflow block in chain with offset " + ob.getOverflowOffset());
                        }
                    }
                    ob.insertRecordToInvalidPlace(recOfGroupTwo);
                    this.log("\t\t\t\t\t\t\t\t\t\tRecord inserted into overflow block " + ob.getOverflowOffset());
                } else {
                    // Blok nemá overflow blok
                    OverflowBlock ob = freeOverflowBlocks.removeFirst();
                    this.log("\t\t\t\t\t\t\t\t\t\tPrimary block had no overflow block so the one with offset " + ob.getOverflowOffset() + " was chosen and record was inserted into it");
                    usedOverflowBlocks.add(ob);
                    newPrimaryBlock.setOverflowBlock(ob);
                    ob.insertRecordToInvalidPlace(recOfGroupTwo);
                }
            } else {
                this.log("\t\t\t\t\t\t\t\t\tPrimary block was not full, inserting " + recOfGroupTwo.getKey().getValue() + " into block " + (this.splitPointer + this.firstLevelModulo));
                newPrimaryBlock.insertRecordToInvalidPlace(recOfGroupTwo);
            }
        }

        // Uloženie primárnych blokov
        this.fh.writeToMainFile(mainBlockToSplit.toBytesArray(), this.splitPointer * this.blockSize);
        this.fh.writeToMainFile(newPrimaryBlock.toBytesArray(), (this.splitPointer + this.firstLevelModulo) * this.blockSize);

        for (OverflowBlock usedOverflowBlock : usedOverflowBlocks) {
            this.saveOverflowBlock(usedOverflowBlock);
        }

        for (OverflowBlock unusedOverflowBlock : freeOverflowBlocks) {
            Integer consolidatedOverflowBlocks = this.overflowBlocksHandler.freeOverflowBlockAtOffset(unusedOverflowBlock.getOverflowOffset());
            if (consolidatedOverflowBlocks > 0) {
                this.fh.setSizeOfOverflowBlocksFile(this.overflowBlocksHandler.getFullSize().longValue());
            }
        }

        this.blocksCount++;
        this.splitPointer++;
        if (this.splitPointer >= this.firstLevelModulo) {
            hashingExpansion();
        }

        this.log("---- LOAD FACTOR AFTER SPLIT IS " + this.getLoadFactor());

        // Opätovné splitnutie v prípade, že pri expanzii ostal load factor vyšší ako je threshold
        if (this.getLoadFactor() > this.overflowBlocksHandler.MAX_THRESHOLD) {
            split();
        }
    }

    /**
     * Úplná expanzia súboru,
     * t.j. už boli všetky bloky rozdelené,
     * menia sa hashovacie funkcie
     *
     */
    public void hashingExpansion() {
        this.increaseFileLevel();
    }

    /**
     * Oloží overflow blok do súboru podľa toho, na akom offsete sa overflow block nachádza
     * @param overflowBlock
     */
    public void saveOverflowBlock(OverflowBlock<R> overflowBlock) {
        this.fh.writeToOverflowFile(overflowBlock.toBytesArray(), overflowBlock.getOverflowOffset());
    }

    /**
     * Vráti NORMÁLNY BLOK na indexe z predaného parametra
     * @param index
     * @return
     */
    public Block<R> getBlockByIndex(Integer index) {
        byte[] blockBytes = this.fh.readFromMainFile(index * this.blockSize, this.blockSize);

        Block<R> block = new Block<>(this.numberOfRecordsInPrimaryBlocks, this.numberOfRecordsInOverflowBlocks);
        block.setNumberOfValidRecordsAndOverflowBlock(Arrays.copyOfRange(blockBytes, 0, Block.BLOCK_INFO_SIZE_BYTES));

        for (int i = 0;  i < this.numberOfRecordsInPrimaryBlocks; i++) {
            Integer recordOffsetInBlock = Block.BLOCK_INFO_SIZE_BYTES + i*this.entitySizeInBytes;
            Integer recordEndOffsetInBlock = Block.BLOCK_INFO_SIZE_BYTES + (i+1)*this.entitySizeInBytes;
            R rec = this.entityFactory.fromBytes(Arrays.copyOfRange(blockBytes, recordOffsetInBlock, recordEndOffsetInBlock));
            block.appendRecordFromFile(rec);
        }
        return block;
    }

    /**
     * Vráti OVERFLOW BLOK začínajúci na offsete z parametra
     * @param offsetInFile
     * @return
     */
    public OverflowBlock<R> getOverflowBlockByOffset(Integer offsetInFile) {
        byte[] overflowBlockBytes = this.fh.readFromOverflowFile(offsetInFile, this.overflowBlockSize);

        OverflowBlock<R> overflowBlock = new OverflowBlock<>(offsetInFile, this.numberOfRecordsInOverflowBlocks);
        overflowBlock.setNumberOfValidRecordsAndOverflowBlock(Arrays.copyOfRange(overflowBlockBytes, 0, OverflowBlock.OVERFLOW_BLOCK_INFO_SIZE_BYTES));

        for(int i = 0; i < this.numberOfRecordsInOverflowBlocks; i++) {
            Integer recordOffsetInBlock = OverflowBlock.OVERFLOW_BLOCK_INFO_SIZE_BYTES + i*this.entitySizeInBytes;
            Integer recordEndOffsetInBlock = OverflowBlock.OVERFLOW_BLOCK_INFO_SIZE_BYTES + (i+1)*this.entitySizeInBytes;
            R rec = this.entityFactory.fromBytes(Arrays.copyOfRange(overflowBlockBytes, recordOffsetInBlock, recordEndOffsetInBlock));
            overflowBlock.appendRecord(rec);
        }
        return overflowBlock;
    }

    public List<OverflowBlock<R>> getAllOverflowBlocks() {
        List<OverflowBlock<R>> overflowBlocks = new LinkedList<>();

        for (Integer offset : this.overflowBlocksHandler.getOverflowBlocksOffsets()) {
            overflowBlocks.add(this.getOverflowBlockByOffset(offset));
        }

        return overflowBlocks;
    }

    public Integer getSecondLevelModulo() {
        return this.secondLevelModulo;
    }

    /**
     * Vráti naplnenosť štruktúry
     * @return
     */
    private Double getLoadFactor() {
        Integer numberOfRecordsInOverflowBlocksAllocated =
                this.overflowBlocksHandler.getNumberOfOverflowBlocksUsed() * this.numberOfRecordsInOverflowBlocks;
        Integer numberOfRecordsInPrimaryBlocksAllocated =
                this.blocksCount * this.numberOfRecordsInPrimaryBlocks;
        Double loadFactor = (1.0 * this.numberOfRecords)  / (numberOfRecordsInOverflowBlocksAllocated + numberOfRecordsInPrimaryBlocksAllocated);
        return loadFactor;

    }

    public void close() {
        this.fh.closeFiles();
    }

    // Zvýši úroveň súboru, prepočítajú sa znova hashovačky
    public void increaseFileLevel() {
        this.fileLevel++;
        this.splitPointer = 0;
        this.firstLevelModulo = this.secondLevelModulo;
        this.secondLevelModulo = new Double(this.firstLevelModulo * 2).intValue();
        this.log("File level increased, new modulos are " + this.firstLevelModulo + " and " + this.secondLevelModulo);
    }

    // Zníži úroveň súboru, prepočítajú sa znova hashovačky
    public void decreaseFileLevel() {
        this.fileLevel--;
        this.firstLevelModulo = new Double(this.blocksCount * Math.pow(2, this.fileLevel)).intValue();
        this.secondLevelModulo = new Double(this.blocksCount * Math.pow(2, this.fileLevel + 1)).intValue();
    }

    private void log(String log) {
        if (this.LOGGING_ALLOWED) {
            System.out.println(log);
        }
    }

}
