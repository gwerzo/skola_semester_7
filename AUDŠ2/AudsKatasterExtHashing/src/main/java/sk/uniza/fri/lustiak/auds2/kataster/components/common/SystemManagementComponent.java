package sk.uniza.fri.lustiak.auds2.kataster.components.common;

import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class SystemManagementComponent extends VerticalLayout {

    public SystemManagementComponent() {
        initComponents();
    }

    private void initComponents() {

        Label lMain = new Label("Funkcionalita manažmentu systému:");

        Label lEmpty = new Label();

        Label l1 = new Label("1. Inicializovanie systému náhodnými dátami");
        Label l2 = new Label("2. Načítanie katastra zo súboru");
        Label l3 = new Label("3. Zobrazenie obsahov blokov");
        Label l4 = new Label("4. Uloženie aktuálneho stavu");

        l1.setWidth("100%");
        l2.setWidth("100%");
        l3.setWidth("100%");
        l4.setWidth("100%");

        VerticalLayout labelsWrapper = new VerticalLayout(
                lMain,
                lEmpty,
                l1,
                l2,
                l3,
                l4);

        Panel descPanel = new Panel(
                "Manažment systému",
                labelsWrapper
        );

        addComponent(descPanel);
    }
}
