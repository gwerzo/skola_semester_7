package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.Data;

@Data
public class GenerovanieSystemuUi {

    private int pocetNehnutelnosti;

    public void flush() {
        this.pocetNehnutelnosti = 0;
    }

}
