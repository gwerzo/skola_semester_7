package sk.uniza.fri.lustyno.AudsLetisko.entities_ui;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class HistoriaOdletuUiEntity {

    private String idDrahy;

    private String medzinarodnyKodLietadla;

    private LocalDateTime casPriletu;

    private LocalDateTime casOdletu;

}
