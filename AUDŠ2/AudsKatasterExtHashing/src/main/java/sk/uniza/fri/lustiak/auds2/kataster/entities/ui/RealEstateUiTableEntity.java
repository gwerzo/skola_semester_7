package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.Data;

@Data
public class RealEstateUiTableEntity {

    private int identifikacneCislo;
    private int supisneCislo;
    private String popis;

    private Double latitude;
    private Double longtitude;
    private String latitudeOrientation;
    private String longitudeOrientation;

    private Double latitude2;
    private Double longtitude2;
    private String latitudeOrientation2;
    private String longitudeOrientation2;

}
