﻿namespace MemoryGameServerApp.Server
{
    public class MemoryGameServerMessages
    {

        public static string GAME_CANCELLED_BY_HOST           = "-gamecancelhost";
        public static string GAME_CANCELLED_BY_GUEST          = "-gamecancelguest";

    }
}
