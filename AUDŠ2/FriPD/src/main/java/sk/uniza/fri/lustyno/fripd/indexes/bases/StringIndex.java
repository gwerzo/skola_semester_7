package sk.uniza.fri.lustyno.fripd.indexes.bases;

import sk.uniza.fri.lustyno.datastructures.entity.keys.StringKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Record;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;

import java.util.Arrays;

public abstract class StringIndex extends Record<StringKey> {

    protected StringKey key;

    protected Integer offset;


    public StringIndex(String key, Integer offset) {
        this.key = new StringKey(key);
        this.offset = offset;
    }

    public Integer getOffset() {
        return this.offset;
    }

    @Override
    public StringKey getKey() {
        return this.key;
    }

    @Override
    public byte[] toBytesArray() {
        byte[] bytes = ToByteArrayConverter.concatenateByteArrays(
            Arrays.asList(
                ToByteArrayConverter.bytesFromBoolean(this.valid),
                ToByteArrayConverter.bytesFromString(this.key.getValue()),
                ToByteArrayConverter.bytesFromInt(this.offset)));
        return bytes;
    }

    @Override
    public int getHash() {
        int hash = 7;
        for (int i = 0; i < this.key.getValue().length(); i++) {
            hash = hash*31 + this.key.getValue().charAt(i);
        }

        return Math.abs(hash);
    }
}
