package sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValueOnDimension;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureWrongUsageException;

/**
 * Dvojdimenzionálny kľúč reprezentujúci 2D priestor "zemegule",
 * ktorý v prípade, že je je zemepisná súradnica zadaná
 * s pozitívnymi hodnotami pre JUŽNÚ výšku/ZÁPADNÚ šírku
 * napr. SOUTH 120° WEST 50°
 * transformuje pri inicializácii na záporné hodnoty
 * SOUTH -120° a WEST -50° pre ľahšie spracovávanie algiritmom vyhľadávania
 * (nemusí sa overovať v každom kroku porovnania na dimenzii,
 *  či sa jedná o SOUTH/NORTH,.. - inicializuje sa to na začiatku pri vytváraní kľuča)
 */
public final class Coordinates2DKey implements HasComparableValueOnDimension<Coordinate2D> {

    private Coordinate2D keyValue;

    public Coordinates2DKey(double latitudeValue, double longitudeValue,
                            LATITUDE_POSITION latitude_position, LONGITUDE_POSITION LONGITUDE_position,
                            int uniqueIdentifier) {

        if (latitudeValue > 180 || longitudeValue > 180 || latitudeValue < -180 || longitudeValue < -180) {
            throw new DataStructureWrongUsageException(this.getClass().getName(), "Coordinates2DKey",
                    "Hodnoty zemepisnej šírky/výšky môžu byť iba v rozmedzí <-180, 180>");
        }

        if (latitude_position == LATITUDE_POSITION.SOUTH) {
            if (latitudeValue > 0) {
                latitudeValue = -latitudeValue;
            }
        }

        if (LONGITUDE_position == LONGITUDE_POSITION.WEST) {
            if (longitudeValue > 0) {
                longitudeValue = -longitudeValue;
            }
        }
        this.keyValue = new Coordinate2D(latitudeValue,longitudeValue, latitude_position, LONGITUDE_position, uniqueIdentifier);
    }

    public Coordinates2DKey(double latitudeValue, double longitudeValue,
                            LATITUDE_POSITION latitude_position, LONGITUDE_POSITION LONGITUDE_position) {

        if (latitudeValue > 180 || longitudeValue > 180 || latitudeValue < -180 || longitudeValue < -180) {
            throw new DataStructureWrongUsageException(this.getClass().getName(), "Coordinates2DKey",
                    "Hodnoty zemepisnej šírky/výšky môžu byť iba v rozmedzí <-180, 180>");
        }

        if (latitude_position == LATITUDE_POSITION.SOUTH) {
            if (latitudeValue > 0) {
                latitudeValue = -latitudeValue;
            }
        }

        if (LONGITUDE_position == LONGITUDE_POSITION.WEST) {
            if (longitudeValue > 0) {
                longitudeValue = -longitudeValue;
            }
        }
        this.keyValue = new Coordinate2D(latitudeValue,longitudeValue, latitude_position, LONGITUDE_position, -1);
    }

    @Override
    public int compareToOnDimension(HasComparableValueOnDimension<Coordinate2D> other, int dimension) {
        if (dimension == 0) {
            return Double.compare(this.keyValue.getLatitudeValue(), other.getValue().getLatitudeValue());
        } else if (dimension == 1) {
            return Double.compare(this.keyValue.getLongitudeValue(), other.getValue().getLongitudeValue());
        } else {
            throw new DataStructureWrongUsageException(
                    this.getClass().getName(),
                    "compareToOnDimension",
                    "Pokúšaš sa použiť tento typ kľúča s dimenziou, ktorú nepodporuje, dimenzia = " + dimension);
        }
    }

    @Override
    public boolean equalsOnAllDimensions(HasComparableValueOnDimension<Coordinate2D> other) {
        return this.keyValue.getLatitudeValue() == other.getValue().getLatitudeValue()
                &&
                this.keyValue.getLongitudeValue() == other.getValue().getLongitudeValue();
    }

    @Override
    public Coordinate2D getValue() {
        return this.keyValue;
    }

}
