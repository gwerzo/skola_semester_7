package sk.uniza.fri.lustiak.auds2.kataster.components.common;


import sk.uniza.fri.lustiak.auds2.kataster.components.kataster.*;

public class MenuComponent extends AbstractMenu {

    public MenuComponent(RepaintableComponent component) {
        this.menuItems.add(new VyhladanieNehnutelnostiPodlaGPSForm(component));
        this.menuItems.add(new VyhladanieParcielPodlaGPSForm(component));
        this.menuItems.add(new VyhladanieObjektovPodlaGPSForm(component));
        this.menuItems.add(new PridanieNehnutelnostiForm(component));
        this.menuItems.add(new PridanieParcelyForm(component));
        this.menuItems.add(new EditaciaNehnutelnostiForm(component));
        this.menuItems.add(new EditaciaParcelyForm(component));
        this.menuItems.add(new VyradenieNehnutelnostiForm(component));
        this.menuItems.add(new VyradenieParcelyForm(component));
        initMenuItems();
    }

}
