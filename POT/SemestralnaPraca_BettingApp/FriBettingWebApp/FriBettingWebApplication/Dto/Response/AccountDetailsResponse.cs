﻿namespace FriBettingWebApplication.Dto.Response
{
    public class AccountDetailsResponse
    {

        public string Username { get; set; }
        public double Balance { get; set; }
    }
}