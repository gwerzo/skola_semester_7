package sk.uniza.fri.lustyno.fripd.indexes;

import lombok.Data;
import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.HasSize;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.fripd.indexes.bases.IntegerIndex;

import java.util.Arrays;

@HasSize(sizeInBytes = 9)
@Data
public class EVCIntegerIndex extends IntegerIndex {

    public EVCIntegerIndex(Integer key, Integer offset) {
        super(key, offset);
    }

    public static EVCIntegerIndex emptyInvalidInstance() {
        return new EVCIntegerIndex(-1, -1);
    }

    public static EVCIntegerIndex fromByteArray(byte[] fromBytes) {
        EVCIntegerIndex en = new EVCIntegerIndex(-1, -1);
        en.valid = FromByteArrayConverter.booleanFromByte(fromBytes[0]);
        en.key = new IntegerKey(FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(fromBytes, 1, 5)));
        en.offset = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(fromBytes, 5, 9));
        return en;
    }

}
