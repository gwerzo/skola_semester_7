package kdtree;

import org.junit.jupiter.api.*;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.dimension2D.IntegerIntegerUniqueKey;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.dimension2D.IntegerIntegerWrapperKey;
import sk.uniza.fri.lustyno.datastructures.kdtree.KDTree;
import sk.uniza.fri.lustyno.datastructures.kdtree.KDTreeNodeKeyDataWrapper;

import java.util.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class KDTreeBasicTest {

    @Test
    @Order(1)
    public void test_DETERMINISTIC_INSERTION_AND_FIND() {

        KDTree<IntegerIntegerWrapperKey, String, IntegerIntegerUniqueKey> kdTree = new KDTree<>(2);

        kdTree.insert(new IntegerIntegerWrapperKey(50, 50, 0), "50-50");
        kdTree.insert(new IntegerIntegerWrapperKey(20, 100, 1), "20-100");
        kdTree.insert(new IntegerIntegerWrapperKey(20, 50,2), "20-50");
        kdTree.insert(new IntegerIntegerWrapperKey(20, 200,3), "20-200");
        kdTree.insert(new IntegerIntegerWrapperKey(10, 10,4), "10-10");
        kdTree.insert(new IntegerIntegerWrapperKey(25, 10,5), "25-10");
        kdTree.insert(new IntegerIntegerWrapperKey(15, 120,6), "15-120");
        kdTree.insert(new IntegerIntegerWrapperKey(25, 120,7), "25-120");
        kdTree.insert(new IntegerIntegerWrapperKey(100, 75,8), "100-75");
        kdTree.insert(new IntegerIntegerWrapperKey(120, 10,9), "120-10");
        kdTree.insert(new IntegerIntegerWrapperKey(120, 100,10), "120-100");

        Assertions.assertNotNull(kdTree.find(new IntegerIntegerWrapperKey(15,120,6)));
    }

    @Test
    @Order(2)
    public void test_LOOPED_INSERTION_AND_FIND_WITH_CHECK() {

        KDTree<IntegerIntegerWrapperKey, String, IntegerIntegerUniqueKey> kdTree = new KDTree<>(2);

        Random rnd = new Random();
        List<IntegerIntegerWrapperKey> inserted = new ArrayList<>();

        for (int i = 0; i < 1000000; i++) {
            IntegerIntegerWrapperKey iKey = new IntegerIntegerWrapperKey(rnd.nextInt(100000), rnd.nextInt(100000), i);
            inserted.add(iKey);
            kdTree.insert(iKey,"" + i);
        }

        for (int i = 0; i < inserted.size(); i++) {
            KDTreeNodeKeyDataWrapper<IntegerIntegerWrapperKey, String> found = kdTree.find(inserted.get(i));
            Assertions.assertEquals("" + i, found.getData());
        }
    }

    @Test
    @Order(3)
    public void test_FIND_WITH_SAME_KEY_DIMENSIONS_BUT_DIFF_UNIQUE_VAL() {

        KDTree<IntegerIntegerWrapperKey, String, IntegerIntegerUniqueKey> kdTree = new KDTree<>(2);

        final int INTERATIONS_COUNT = 1000000;

        Random rnd = new Random();
        List<IntegerIntegerWrapperKey> inserted = new ArrayList<>();

        int sameX = rnd.nextInt(100000);
        int sameY = rnd.nextInt(100000);

        for (int i = 0; i < INTERATIONS_COUNT; i++) {
            IntegerIntegerWrapperKey iKey;

            if (i != INTERATIONS_COUNT / 2) {
                iKey = new IntegerIntegerWrapperKey(rnd.nextInt(100000), rnd.nextInt(100000), i);
                kdTree.insert(iKey,"" + i);
            } else {
                // Niekde do stredu stromu (stred iteračného countu) vložím 2 rovnaké nody len s iným unique kľúčom
                iKey = new IntegerIntegerWrapperKey(sameX, sameY, i);
                inserted.add(iKey);
                kdTree.insert(iKey,"" + i);

                iKey = new IntegerIntegerWrapperKey(sameX, sameY, ++i);
                inserted.add(iKey);
                kdTree.insert(iKey, "" + i);
            }
        }

        for (int i = 0; i < inserted.size(); i++) {
            KDTreeNodeKeyDataWrapper<IntegerIntegerWrapperKey, String> found = kdTree.find(inserted.get(i));
            Assertions.assertEquals("" + ((INTERATIONS_COUNT / 2) + i), found.getData());
        }

    }

    @Test
    @Order(4)
    public void test_INSERT_DELETE_MIX() {

        double probabilityOfInsert = 0.8;
        double probabilityOfDelete = 1 - probabilityOfInsert;

        KDTree<IntegerIntegerWrapperKey, String, IntegerIntegerUniqueKey> kdTree;
        for (int y = 0; y < 1; y++) {
            try {
                kdTree = new KDTree<>(2);

                // S fixným seedom, dobré na ďalšie hľadanie chýb
                //Random rnd = new Random(y);

                // Random seed
                Random rnd = new Random();

                List<IntegerIntegerWrapperKey> inserted = new ArrayList<>();
                Map<IntegerIntegerWrapperKey, String> mappedData = new HashMap<>();

                int iterationCount = 0;
                int warmupTreshold = 10000;
                int randomizedOperations = warmupTreshold + 1000000;

                int numberOfInserted = 0;
                int numberOfDeleted = 0;

                // Warmup
                for (int i = iterationCount; i < warmupTreshold; i++) {
                    IntegerIntegerWrapperKey iKey = new IntegerIntegerWrapperKey(rnd.nextInt(100000), rnd.nextInt(100000), i);
                    inserted.add(iKey);
                    kdTree.insert(iKey, "" + i);
                    mappedData.put(iKey, "" + i);
                }
                iterationCount = warmupTreshold;

                for (int i = iterationCount; i < randomizedOperations; i++) {
                    if (rnd.nextDouble() < probabilityOfInsert) {
                        IntegerIntegerWrapperKey iKey = new IntegerIntegerWrapperKey(rnd.nextInt(100000), rnd.nextInt(100000), i);
                        inserted.add(iKey);
                        kdTree.insert(iKey, "" + i);
                        mappedData.put(iKey, "" + i);
                        numberOfInserted++;
                    } else {
                        IntegerIntegerWrapperKey iKey = inserted.remove(rnd.nextInt(inserted.size()));
                        kdTree.delete(iKey);
                        inserted.remove(iKey);
                        numberOfDeleted++;
                    }
                }

                for (IntegerIntegerWrapperKey iKey : inserted) {
                    String data = mappedData.get(iKey);
                    Assertions.assertEquals(kdTree.find(iKey).getData(), data);
                }

            } catch (Exception e) {
                System.out.println("When exception occured, Y was: " + y);
                e.printStackTrace();
            }
        }
    }

    @Test
    @Order(5)
    public void test_CRASHING_SEED() {
        KDTree<IntegerIntegerWrapperKey, String, IntegerIntegerUniqueKey> kdTree = new KDTree<>(2);
        double probabilityOfInsert = 0.8;
        double probabilityOfDelete = 1 - probabilityOfInsert;

        Random rnd = new Random(1428631);

        List<IntegerIntegerWrapperKey> inserted = new ArrayList<>();
        Map<IntegerIntegerWrapperKey, String> mappedData = new HashMap<>();

        int iterationCount = 0;
        int warmupTreshold = 6;
        int randomizedOperations = warmupTreshold + 5;
        //int randomizedOperations = warmupTreshold + 3;

        int numberOfInserted = 0;
        int numberOfDeleted = 0;

        // Warmup
        for (int i = iterationCount; i < warmupTreshold; i++) {
            IntegerIntegerWrapperKey iKey = new IntegerIntegerWrapperKey(rnd.nextInt(4), rnd.nextInt(4), i);
            inserted.add(iKey);
            kdTree.insert(iKey,"" + i);
            mappedData.put(iKey, "" + i);
            numberOfInserted++;
        }
        iterationCount = warmupTreshold;

        for (int i = iterationCount; i < randomizedOperations; i++) {
            if (rnd.nextDouble() < probabilityOfInsert) {
                IntegerIntegerWrapperKey iKey = new IntegerIntegerWrapperKey(rnd.nextInt(4), rnd.nextInt(4), i);
                inserted.add(iKey);
                kdTree.insert(iKey,"" + i);
                mappedData.put(iKey, "" + i);
                numberOfInserted++;
            } else {
                IntegerIntegerWrapperKey iKey = inserted.remove(rnd.nextInt(inserted.size()));
                KDTreeNodeKeyDataWrapper<IntegerIntegerWrapperKey, String> delete = kdTree.delete(iKey);
                Assertions.assertEquals(delete.getKey(), iKey);
                numberOfDeleted++;
                Assertions.assertEquals(kdTree.countWithLevelOrder(), numberOfInserted - numberOfDeleted);
            }
        }

        for (IntegerIntegerWrapperKey iKey : inserted) {
            String data = mappedData.get(iKey);
            Assertions.assertEquals(kdTree.find(iKey).getData(), data);
        }

        System.out.println();

    }

    @Test
    @Order(6)
    public void test_COMPARE_KEYS_IN_RANGE() {

        KDTree<IntegerIntegerWrapperKey, String, IntegerIntegerUniqueKey> kdTree = new KDTree<>(2);

        // Schválne kľúče from/to nelogicky, from > to
        IntegerIntegerWrapperKey rangeKeyFrom = new IntegerIntegerWrapperKey(9,11, 7);
        IntegerIntegerWrapperKey rangeKeyTo = new IntegerIntegerWrapperKey(4,7, 8);

        IntegerIntegerWrapperKey iKey0 = new IntegerIntegerWrapperKey(3,4, 0); // OUT
        IntegerIntegerWrapperKey iKey1 = new IntegerIntegerWrapperKey(5,6, 1); // OUT
        IntegerIntegerWrapperKey iKey2 = new IntegerIntegerWrapperKey(6,8, 2); // IN
        IntegerIntegerWrapperKey iKey3 = new IntegerIntegerWrapperKey(4,11, 3); // IN
        IntegerIntegerWrapperKey iKey4 = new IntegerIntegerWrapperKey(7,10, 4); // IN
        IntegerIntegerWrapperKey iKey5 = new IntegerIntegerWrapperKey(10,13, 5); // OUT
        IntegerIntegerWrapperKey iKey6 = new IntegerIntegerWrapperKey(10,12, 6); // OUT

        Map<IntegerIntegerWrapperKey, Boolean> keysInRectangleMap = new HashMap<>();
        keysInRectangleMap.put(iKey0, false);
        keysInRectangleMap.put(iKey1, false);
        keysInRectangleMap.put(iKey2, true);
        keysInRectangleMap.put(iKey3, true);
        keysInRectangleMap.put(iKey4, true);
        keysInRectangleMap.put(iKey5, false);
        keysInRectangleMap.put(iKey6, false);


//        for (Map.Entry<IntegerIntegerWrapperKey, Boolean> iKey : keysInRectangleMap.entrySet()) {
//            boolean inside =
//                    kdTree.keyBelongsToInterval(iKey.getKey(), rangeKeyFrom, rangeKeyTo, 0)
//                    && kdTree.keyBelongsToInterval(iKey.getKey(), rangeKeyFrom, rangeKeyTo, 1);
//            Assertions.assertEquals(inside, iKey.getValue());
//        }

    }

    @Test
    @Order(7)
    public void test_RANGE_SEARCH() {

        KDTree<IntegerIntegerWrapperKey, String, IntegerIntegerUniqueKey> kdTree = new KDTree<>(2);

        // Schválne kľúče from/to nelogicky, from > to
        IntegerIntegerWrapperKey rangeKeyFrom = new IntegerIntegerWrapperKey(9,11, 7);
        IntegerIntegerWrapperKey rangeKeyTo = new IntegerIntegerWrapperKey(4,7, 8);

        IntegerIntegerWrapperKey iKey0 = new IntegerIntegerWrapperKey(3,4, 0); // OUT
        IntegerIntegerWrapperKey iKey1 = new IntegerIntegerWrapperKey(5,6, 1); // OUT
        IntegerIntegerWrapperKey iKey2 = new IntegerIntegerWrapperKey(6,8, 2); // IN
        IntegerIntegerWrapperKey iKey3 = new IntegerIntegerWrapperKey(4,11, 3); // IN
        IntegerIntegerWrapperKey iKey4 = new IntegerIntegerWrapperKey(7,10, 4); // IN
        IntegerIntegerWrapperKey iKey5 = new IntegerIntegerWrapperKey(10,13, 5); // OUT
        IntegerIntegerWrapperKey iKey6 = new IntegerIntegerWrapperKey(10,12, 6); // OUT

        Map<IntegerIntegerWrapperKey, Boolean> keysInRectangleMap = new HashMap<>();
        keysInRectangleMap.put(iKey0, false);
        keysInRectangleMap.put(iKey1, false);
        keysInRectangleMap.put(iKey2, true);
        keysInRectangleMap.put(iKey3, true);
        keysInRectangleMap.put(iKey4, true);
        keysInRectangleMap.put(iKey5, false);
        keysInRectangleMap.put(iKey6, false);

        kdTree.insert(iKey0, "0");
        kdTree.insert(iKey1, "1");
        kdTree.insert(iKey2, "2");
        kdTree.insert(iKey3, "3");
        kdTree.insert(iKey4, "4");
        kdTree.insert(iKey5, "5");
        kdTree.insert(iKey6, "6");

        List<KDTreeNodeKeyDataWrapper<IntegerIntegerWrapperKey, String>> rangeSearchReturn =
                kdTree.findInRange(rangeKeyFrom, rangeKeyTo);

        Assertions.assertEquals(3, rangeSearchReturn.size());
    }

    @Test
    @Order(8)
    public void test_RANGE_SEARCH_DYNAMIC_VALUES() {

        KDTree<IntegerIntegerWrapperKey, String, IntegerIntegerUniqueKey> kdTree = new KDTree<>(2);

        Random rnd = new Random();

        // Vygenerované číslo <10, 30)
        int rangeSearchX_LOW = rnd.nextInt(20) + 10;

        // Vygenerované číslo <30, 60)
        int rangeSearchX_HIGH = rnd.nextInt(30) + 30;

        // Vygenerované číslo <10, 30)
        int rangeSearchY_LOW = rnd.nextInt(20) + 10;

        // Vygenerované číslo <30, 60)
        int rangeSearchY_HIGH = rnd.nextInt(30) + 30;

        int counterOfInRangeNodes = 0;

        List<IntegerIntegerWrapperKey> iKeysInserted = new ArrayList<>();

        // Insertne 10000 náhodných prvkov
        for (int i = 0; i < 100000; i++) {

            int iKeyX = rnd.nextInt(100);
            int iKeyY = rnd.nextInt(100);

            // Určí, či pridávaný node je v "range" intervalu pre range-search vyhľadanie
            boolean isInRange =
                    (iKeyX >= rangeSearchX_LOW && iKeyX <= rangeSearchX_HIGH)
                    &&
                    (iKeyY >= rangeSearchY_LOW && iKeyY <= rangeSearchY_HIGH);

            if (isInRange) {
                counterOfInRangeNodes++;
            }

            IntegerIntegerWrapperKey iKey = new IntegerIntegerWrapperKey(iKeyX, iKeyY, i);
            iKeysInserted.add(iKey);
            kdTree.insert(iKey, "" + i);
        }

        // Vymaže 1000 náhodných prvkov
        for (int i = 0; i < 1000; i++) {
            IntegerIntegerWrapperKey rndKey = iKeysInserted.remove(rnd.nextInt(iKeysInserted.size()));

            int iKeyX = rndKey.getValue().getX();
            int iKeyY = rndKey.getValue().getY();

            boolean isInRange =
                    (iKeyX >= rangeSearchX_LOW && iKeyX <= rangeSearchX_HIGH)
                            &&
                    (iKeyY >= rangeSearchY_LOW && iKeyY <= rangeSearchY_HIGH);
            if (isInRange) {
                counterOfInRangeNodes--;
            }
            kdTree.delete(rndKey);
        }

        IntegerIntegerWrapperKey lowRangeKey = new IntegerIntegerWrapperKey(rangeSearchX_LOW, rangeSearchY_LOW, 0);
        IntegerIntegerWrapperKey highRangeKey = new IntegerIntegerWrapperKey(rangeSearchX_HIGH, rangeSearchY_HIGH, 0);
        Assertions.assertEquals(counterOfInRangeNodes, kdTree.findInRange(lowRangeKey, highRangeKey).size());
    }

}
