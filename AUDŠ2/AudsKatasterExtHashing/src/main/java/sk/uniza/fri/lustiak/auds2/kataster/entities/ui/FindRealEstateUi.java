package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.Data;

@Data
public class FindRealEstateUi {

    private Integer identifikacneCislo;

    public void flush() {
        this.identifikacneCislo = null;
    }

}
