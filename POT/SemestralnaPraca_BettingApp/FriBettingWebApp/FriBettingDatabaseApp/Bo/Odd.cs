﻿using System;

namespace FriBettingDatabaseApp.Bo
{
    /// <summary>
    /// Class reprezentácia jedného kurzu pre vytvorenie stávky,
    /// ktorý sa ovšem môže časom meniť
    /// </summary>
    public class Odd: IdentifiableEntity
    {

        public int OddId { get; set; }
        public Match Match { get; set; }
        public OddType OddType { get; set; }
        public double OddValue { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsEvaluated { get; set; }
        public bool IsWinning { get; set; }

        public override int GetId()
        {
            return OddId;
        }
    }
}
