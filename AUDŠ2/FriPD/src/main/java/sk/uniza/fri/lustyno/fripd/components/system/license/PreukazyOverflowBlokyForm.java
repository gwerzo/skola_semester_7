package sk.uniza.fri.lustyno.fripd.components.system.license;

import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.fripd.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.fripd.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.fripd.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.fripd.entities_ui.block.BlockDetailEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.block.ZaznamEntityUi;
import sk.uniza.fri.lustyno.fripd.service.DrivingLicenseService;
import sk.uniza.fri.lustyno.fripd.service.UIServices;
import sk.uniza.fri.lustyno.fripd.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.fripd.utils.OperationMarker;

import java.util.List;

public class PreukazyOverflowBlokyForm extends BusinessComponent {

    private DrivingLicenseService drivingLicenseService;

    private Panel panel;

    public PreukazyOverflowBlokyForm(RepaintableComponent parentComponent) {
        super(parentComponent, MENU_TITLE.SYSTEM_MENU_5_1.getTitle());
        this.drivingLicenseService = UIServices.getDrivingLicenseService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        this.panel = new Panel("Detaily blokov");
        this.panel.setSizeFull();

        HandledOperationExecutor.submit(() -> {
            List<BlockDetailEntityUi> bloky = this.drivingLicenseService.getDrivingLicensesOverflowBlocks();
            renderBlocks(bloky);
            return OperationMarker.OPERATION_SUCCESFUL;
        });


        this.parenComponent.showComponent(this.panel);
    }

    private void renderBlocks(List<BlockDetailEntityUi> bloky) {

        VerticalLayout vl = new VerticalLayout();
        vl.setSizeUndefined();
        vl.setWidth("100%");

        for (BlockDetailEntityUi entityUi : bloky) {

            VerticalLayout layout = new VerticalLayout();

            FormLayout fl = new FormLayout();
            Label offsetBloku = new Label(entityUi.getCisloBloku().toString());
            offsetBloku.setCaption("Offset bloku");


            fl.addComponent(offsetBloku);

            Grid<ZaznamEntityUi> gridZaznamovBloku = new Grid<>();
            gridZaznamovBloku.addColumn(ZaznamEntityUi::getKluc).setCaption("Kľúč záznamu");
            gridZaznamovBloku.addColumn(ZaznamEntityUi::getHodnota).setCaption("Meno majiteľa preukazu");
            gridZaznamovBloku.addColumn(ZaznamEntityUi::getValidny).setCaption("Validny");

            gridZaznamovBloku.setSizeFull();
            gridZaznamovBloku.setItems(entityUi.getZaznamyBloku());

            layout.addComponent(fl);
            layout.addComponent(gridZaznamovBloku);
            layout.setSizeUndefined();
            layout.setWidth("100%");

            Panel pp = new Panel("Blok č." + entityUi.getCisloBloku(), layout);
            pp.setWidth("100%");

            vl.addComponent(pp);
        }

        this.panel.setContent(vl);

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_5_1.getTitle();
    }
}
