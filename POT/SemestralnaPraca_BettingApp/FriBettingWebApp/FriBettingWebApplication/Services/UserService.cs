﻿using System;
using System.Security.Cryptography;
using System.Text;
using FriBettingDatabaseApp.Bo;
using FriBettingDatabaseApp.Context;
using FriBettingDatabaseApp.Repository.Base;
using FriBettingWebApplication.Dto.Request;
using FriBettingWebApplication.Dto.Response;
using FriBettingWebApplication.Security;
using FriBettingWebApplication.Services.Face;

namespace FriBettingWebApplication.Services
{
    public class UserService: IUserService
    {
        public static double DEFAULT_BALANCE = 1000.0;

        public IUserRepository _userRepository { get; set; }

        public UserService(IUserRepository usersRepository)
        {
            _userRepository = usersRepository;
        }

        // Servisná metóda pre zarehistrovanie používateľa (by default mu dá balance 1000)
        public OperationResponse RegisterUser(RegisterUserRequest registerUserRequest)
        {
            var validation = new ValidationResult();
            RequestValidatorService.ValidateRegistrationRequest(registerUserRequest, out validation);

            if (!validation.Successful)
            {
                return new OperationResponse
                {
                    Successful = false,
                    ErrorMessage = validation.ErrorMessage
                };
            }

            if (!registerUserRequest.Password.Equals(registerUserRequest.PasswordAgain))
            {
                return new OperationResponse
                {
                    Successful = false,
                    ErrorMessage = "Passwords do not match"
                };
            }
            var alreadyExistingUser = _userRepository.GetUserByUsername(registerUserRequest.Username);
            if (alreadyExistingUser != null)
            {
                return new OperationResponse
                {
                    Successful = false,
                    ErrorMessage = "Username already exists"
                };
            }

            var salt = Guid.NewGuid().ToString();
            var saltedPwd = registerUserRequest.Password + salt;
            string hashedSaltedPwd;

            using (SHA512 sha = new SHA512Managed())
            {
                hashedSaltedPwd = Encoding.UTF8.GetString(sha.ComputeHash(Encoding.UTF8.GetBytes(saltedPwd)));
            }

            var newUser = new BettingUser();
            newUser.Username = registerUserRequest.Username;
            newUser.PasswordSalt = salt;
            newUser.HashedSaltedPassword = hashedSaltedPwd.ToString();
            newUser.Balance = DEFAULT_BALANCE;


            using (var dbContext = new BettingAppDbContext())
            {
                var inserted = _userRepository.SaveToContext(newUser, dbContext);
                dbContext.SaveChanges();
                return new OperationResponse
                {
                    Successful = inserted
                };
            }
        }

        // Servisná metóda pre overenie 
        public OperationResponse LoginUser(LoginUserRequest loginUserRequest)
        {
            var validation = new ValidationResult();
            RequestValidatorService.ValidateLoginRequest(loginUserRequest, out validation);

            if (!validation.Successful)
            {
                return new OperationResponse
                {
                    Successful = false,
                    ErrorMessage = validation.ErrorMessage
                };
            }

            BettingUser userBySuchUsername = _userRepository.GetUserByUsername(loginUserRequest.Username);
            if (userBySuchUsername == null)
            {
                return new OperationResponse
                {
                    Successful = false,
                    ErrorMessage = "Wrong credentials"
                };
            }

            var salt = userBySuchUsername.PasswordSalt;
            var saltedPwd = loginUserRequest.Password + salt;
            string hashedSaltedPwd;

            using (SHA512 sha = new SHA512Managed())
            {
                hashedSaltedPwd = Encoding.UTF8.GetString(sha.ComputeHash(Encoding.UTF8.GetBytes(saltedPwd)));
            }

            if (!hashedSaltedPwd.Equals(userBySuchUsername.HashedSaltedPassword))
            {
                return new OperationResponse
                {
                    Successful = false,
                    ErrorMessage = "Wrong credentials"
                };
            }

            return new OperationResponse
            {
                Successful = true,
                SuccessMessage = "Bearer " + JwtSecurityManager.GenerateToken(loginUserRequest.Username)
            };
        }

        public AccountDetailsResponse GetAccountDetails(string usernameIdentity)
        {
            BettingUser userBySuchUsername = _userRepository.GetUserByUsername(usernameIdentity);

            return new AccountDetailsResponse
            {
                Username = usernameIdentity,
                Balance = userBySuchUsername.Balance
            };
        }
    }
}