package sk.uniza.fri.lustyno.AudsLetisko.utils;


import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;

import java.time.LocalDateTime;

public class PrioritaACasPoziadavkyKey implements HasComparableValue<PrioritaACasPoziadavkyKey.PrioritaACasPoziadavky> {

    private PrioritaACasPoziadavky prioritaACasPoziadavky;

    public PrioritaACasPoziadavkyKey(PrioritaACasPoziadavky key) {
        this.prioritaACasPoziadavky = key;
    }

    public PrioritaACasPoziadavkyKey(Integer priorita, LocalDateTime datumCasPoziadavky) {
        this.prioritaACasPoziadavky = new PrioritaACasPoziadavky(priorita, datumCasPoziadavky);
    }

    @Override
    public PrioritaACasPoziadavky getValue() {
        return prioritaACasPoziadavky;
    }

    @Override
    public HasComparableValue<PrioritaACasPoziadavky> getMinValue() {
        return new PrioritaACasPoziadavkyKey(new PrioritaACasPoziadavky(Integer.MIN_VALUE, LocalDateTime.MIN));
    }

    @Override
    public String toString() {
        return this.prioritaACasPoziadavky.priorita + " " + this.prioritaACasPoziadavky.casPoziadania.toString();
    }

    public class PrioritaACasPoziadavky implements Comparable<PrioritaACasPoziadavky> {

        private Integer priorita;

        private LocalDateTime casPoziadania;

        public PrioritaACasPoziadavky(Integer priorita, LocalDateTime casPoziadania) {
            this.priorita = priorita;
            this.casPoziadania = casPoziadania;
        }

        public LocalDateTime getCasPoziadania() {
            return this.casPoziadania;
        }

        @Override
        public int compareTo(PrioritaACasPoziadavky o) {
            int priorityCompared = this.priorita.compareTo(o.priorita);

            // Ak majú rôzne priority, vráť porovnanie priorít
            if (priorityCompared != 0) {
                return priorityCompared;
            }
            // Ak majú rovnaké priority, porovnaj časy
            else {
                return this.casPoziadania.compareTo(o.casPoziadania);
            }
        }

        @Override
        public String toString() {
            return this.priorita + " " + this.casPoziadania.toString();
        }
    }

}
