﻿using MemoryGameServerApp.Server;
using SuperWebSocket;

namespace MemoryGameServerApp.Command
{
    public class CardHitCommand : ICommand
    {
        public const string COMMAND = "-cardhit";

        private const string HIT_UNSUCC = "-cardhitunsucc";
        private const string HIT_BOTH_SUCC = "-cardhitsuccboth";
        private const string HIT_SECOND_UNSUCC = "-cardhitsecondunsucc";

        private int _xHit;
        private int _yHit;
        private string _gameUuid;

        public CardHitCommand(string gameUuid, int xHit, int yHit)
        {
            this._gameUuid = gameUuid;
            this._xHit = xHit;
            this._yHit = yHit;
        }

        public void executeCommand(string usernameOfCommandCaller, MemoryGameServer memoryGameServer,
            WebSocketSession callerSocketSession)
        {
            string error;
            int alreadyHitX;
            int alreadyHitY;
            WebSocketSession secondPlayerWebSocketSession;
            var cardValue = memoryGameServer.HitCard(this._gameUuid, usernameOfCommandCaller, this._xHit, this._yHit, 
                out alreadyHitX, out alreadyHitY, out secondPlayerWebSocketSession, out error);

            if (cardValue == null)
            {
                callerSocketSession.Send(HIT_UNSUCC + " " + error);
                return;
            }

            // Trafenie druhej karty
            if (alreadyHitX != -1)
            {
                callerSocketSession.Send(
                    HIT_BOTH_SUCC + " " +
                    cardValue + " " +
                    this._xHit + " " +
                    this._yHit + " " +
                    alreadyHitX + " " +
                    alreadyHitY);

                secondPlayerWebSocketSession.Send(
                    HIT_BOTH_SUCC + " " +
                    cardValue + " " +
                    this._xHit + " " +
                    this._yHit + " " +
                    alreadyHitX + " " +
                    alreadyHitY);
            }
            else
            {
                if (error != null)
                {
                    // Netrafená druhá karta
                    callerSocketSession.Send(HIT_SECOND_UNSUCC + " " + cardValue + " " + this._xHit + " " + this._yHit);
                    secondPlayerWebSocketSession.Send(HIT_SECOND_UNSUCC + " " + cardValue + " " + this._xHit + " " + this._yHit);
                }
                else
                {
                    // Trafená prvá karta
                    callerSocketSession.Send(COMMAND + " " + cardValue + " " + this._xHit + " " + this._yHit);
                    secondPlayerWebSocketSession.Send(COMMAND + " " + cardValue + " " + this._xHit + " " + this._yHit);
                }
            }
        }
    }
}
