package sk.uniza.fri.lustyno.datastructures.tree;

import sk.uniza.fri.lustyno.datastructures.exception.DataStructureWrongUsageException;

/**
 * Classa, ktorá wrapuje result,
 * ale pridáva možnosť pre overenie,
 * či výsledok bol nájdený.
 * <p>
 * Ak výsledok nebol nájdený,
 * classa môže mať stále nastavenú
 * hodnotu výsledku na niečo iné.
 * <p>
 * Napr. v prípade {@link sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTree}
 * bude výsledkom vyhľadávania posledný
 * node, na ktorý algoritmus vyhľadávania
 * pristúpil predtým, ako zistil, že node
 * s predaným kľúčom neexistuje.
 * <p>
 * Daný prístup je zavedený kvôli potomkom
 *  {@link sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTree}
 * ktorí spomínaný "otec nenájdeného nodu"
 * môžu využiť pri ďalších operáciách, napr.
 *  {@link sk.uniza.fri.lustyno.datastructures.tree.splay.SplayTree}
 * @param <N> typ NODU
 */
public class NodeResult<N> {

    N node;

    private Boolean found;

    public NodeResult(N node, Boolean found) {
        this.node = node;
        this.found = found;
    }

    /**
     * Metóda vráti nájdený node.
     * V prípade, že ale node nájdený nebol,
     * metóda vráti null.
     * <p>
     * K "poslednému pristúpenému nodu"
     * treba pristúpiť cez metódu
     * {@link NodeResult#getLastAccessed()}
     *
     * @return
     *      N node   <=> v prípade, že node BOL nájdený
     *      NULL     <=> v prípade, že node NEBOL nájdený
     */
    public N get() {
        return this.found ? this.node : null;
    }

    /**
     * Metóda vráti "posledný pristúpený node",
     * t.j. v prípade, že node nebol nájdený,
     * vráti posledný node, na ktorý algoritmus pristúpil
     *
     * @return
     *      N node  <=> posledný pristúpený node, v prípade že hľadaný node nebol nájdený
     *      NULL    <=> len v prípade, že strom bol prázdny
     * @throws
     *      DataStructureWrongUsageException v prípade, že sa metóda zavolá, aj keď node bol nájdený
     */
    public N getLastAccessed() {
        if (this.found) {
            throw new DataStructureWrongUsageException(
                    this.getClass().getName(),
                    "get()",
                    "Použitá metóda, ktorá vracia naposledy pristúpený node, ale hľadaný node bol nájdený");
        } else {
            return this.node;
        }
    }

    public Boolean wasFound() {
        return this.found;
    }

}
