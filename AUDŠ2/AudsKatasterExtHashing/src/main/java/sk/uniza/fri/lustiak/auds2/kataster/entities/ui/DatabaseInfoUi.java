package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DatabaseInfoUi {

    private Integer hlbkaSuboru;
    private Integer pocetAdresovacichBitov;
    private Integer pocetEntitVHlavnomBloku;
    private Integer pocetEntitVOverflowBloku;

    private Integer pocetBlokov;
    private Integer pocetOverflowBlokov;

    private Integer velkostHlavnehoSuboru;
    private Integer velkostOverflowSuboru;

    private List<Integer> volneBloky;
    private List<Integer> volneOverflowBloky;

}
