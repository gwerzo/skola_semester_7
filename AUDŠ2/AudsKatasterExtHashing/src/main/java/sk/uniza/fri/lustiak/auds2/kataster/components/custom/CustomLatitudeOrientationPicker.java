package sk.uniza.fri.lustiak.auds2.kataster.components.custom;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.RadioButtonGroup;

public class CustomLatitudeOrientationPicker extends HorizontalLayout {

    private RadioButtonGroup<String> options;

    public CustomLatitudeOrientationPicker(Boolean enabled) {
        this.options = new RadioButtonGroup<>("Orientácia zemepisnej výšky");
        this.options.setItems("SEVER", "JUH");
        this.options.setSelectedItem("SEVER");
        this.addComponents(this.options);
        this.options.setEnabled(enabled);
    }

    public void setSelectedItem(String selectedItem) {
        this.options.setSelectedItem(selectedItem);
    }

    public String getSelectedItem() {
        return (String)this.options.getSelectedItem().get();
    }

}
