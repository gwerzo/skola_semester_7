package sk.uniza.fri.lustyno.fripd.utils;

import java.time.LocalDateTime;
import java.time.LocalTime;

public class LocalDateTimeCanonizer {

    /**
     * Kvôli správnemu ukladaniiu dátumov do DB,
     * zkanonizuje dátum na taký tvar,
     * aby z neho pri .toString()
     * neboli "ukrojené" sekundy/nanos
     *
     * @param localDateTime - dátum pre zkanonizovanie
     * @return - zkanonizovaný dátum na správny tvar
     */
    public static LocalDateTime canonize(LocalDateTime localDateTime) {

        LocalDateTime toRet = LocalDateTime.of(localDateTime.toLocalDate(), LocalTime.MIN);
        toRet.plusSeconds(1);
        toRet.plusNanos(1);

        return toRet;
    }

}
