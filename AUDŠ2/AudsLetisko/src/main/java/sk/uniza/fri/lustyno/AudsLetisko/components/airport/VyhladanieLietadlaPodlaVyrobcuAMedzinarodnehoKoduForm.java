package sk.uniza.fri.lustyno.AudsLetisko.components.airport;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.LietadloTableUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.VyhladanieLietadlaUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.AircraftManagementService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;

import java.util.List;

public class VyhladanieLietadlaPodlaVyrobcuAMedzinarodnehoKoduForm extends BusinessComponent {

    private VyhladanieLietadlaUiEntity entity;

    private Binder<VyhladanieLietadlaUiEntity> binder;

    private AircraftManagementService aircraftManagementService;

    private VerticalLayout lietadlaDetails;

    public VyhladanieLietadlaPodlaVyrobcuAMedzinarodnehoKoduForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_3_1.getTitle());
        this.aircraftManagementService = UIServices.getAircraftManagementService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new VyhladanieLietadlaUiEntity();
        this.binder = new Binder<>(VyhladanieLietadlaUiEntity.class);

        this.lietadlaDetails = new VerticalLayout();

        VerticalLayout layout = new VerticalLayout();

        Label info1 = new Label("Ak chcete vyhľadať konkrétne lietadlo podľa výrobcu a medzinárodného kódu, vyplňte obe hodnoty");
        Label emptyLabel = new Label();
        Label info2 = new Label("Ak chcete vyhľadať všetky lietadlá podľa výrobcu, zadajte len názov výrobcu");

        FormLayout fl = new FormLayout();

        TextField vyrobca = new TextField("Názov výrobcu");

        TextField medzinarodnyKod = new TextField("Medzinárodný kód");

        Button vyhladaj = new Button("Vyhľadaj");
        vyhladaj.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                vyhladajLietadlaPodlaVyrobcov();
            } catch (ValidationException ex) {
                CustomNotification.error("Musíte vyplniť aspoň názov výrobcu");
            }
        });

        Button vyhladajVsetky = new Button("Vyhľadaj všetky");
        vyhladajVsetky.addClickListener(e -> {
           vyhladajVsetkyLietadla();
        });

        this.binder.forField(medzinarodnyKod)
                .asRequired()
                .bind(VyhladanieLietadlaUiEntity::getMedzinarodnyKodLietadla, VyhladanieLietadlaUiEntity::setMedzinarodnyKodLietadla);
        this.binder.forField(vyrobca)
                .asRequired()
                .bind(VyhladanieLietadlaUiEntity::getVyrobca, VyhladanieLietadlaUiEntity::setVyrobca);

        fl.addComponents(vyrobca);
        fl.addComponent(medzinarodnyKod);
        fl.addComponent(vyhladaj);
        fl.addComponent(vyhladajVsetky);

        layout.addComponent(info1);
        layout.addComponent(info2);
        layout.addComponent(emptyLabel);
        layout.addComponent(fl);

        layout.addComponent(this.lietadlaDetails);

        Panel panelWrapper = new Panel("Vyhľadanie čakajúceho lietadla na pridelenie odletovej dráhy podľa výrobcu a medzinárodného kódu", layout);
        panelWrapper.setWidth("100%");
        panelWrapper.setHeight("100%");

        this.parenComponent.showComponent(panelWrapper);
    }

    private void vyhladajLietadlaPodlaVyrobcov() {
        // Vyhľadanie len konkrétneho lietadla
        HandledOperationExecutor.submit(() -> {
            LietadloTableUiEntity lietadloUi = this.aircraftManagementService.getLietadloPodlaVyrobcuAMedzinarodnehoKodu(this.entity);
            ukazInfoOLietadle(lietadloUi);
            return OperationMarker.OPERATION_SUCCESFUL;
        });
    }

    private void vyhladajVsetkyLietadla() {
        // Vyhľadanie len konkrétneho lietadla
        HandledOperationExecutor.submit(() -> {
            List<LietadloTableUiEntity> lietadlaUi = this.aircraftManagementService.getAllCakajuceLietadlaPodlaVyrobcuNaDrahu();
            ukazInfoOLietadlach(lietadlaUi);
            return OperationMarker.OPERATION_SUCCESFUL;
        });
    }


    private void ukazInfoOLietadlach(List<LietadloTableUiEntity> lietadloTableUiEntities) {
        this.lietadlaDetails.removeAllComponents();

        if (lietadloTableUiEntities == null || lietadloTableUiEntities.size() == 0) {
            this.lietadlaDetails.addComponent(new Label("Žiadne lietadlá nečakajú na túto dráhu"));
            return;
        }

        Grid<LietadloTableUiEntity> grid = new Grid<>();
        grid.addColumn(LietadloTableUiEntity::getVyrobca).setCaption("Výrobca");
        grid.addColumn(LietadloTableUiEntity::getMedzinarodnyKod).setCaption("Medzinárodný kód");

        grid.setItems(lietadloTableUiEntities);
        grid.setSizeFull();

        this.lietadlaDetails.addComponent(grid);
    }

    private void ukazInfoOLietadle(LietadloTableUiEntity lietadloUi) {
        this.lietadlaDetails.removeAllComponents();

        if (lietadloUi == null) {
            this.lietadlaDetails.addComponent(new Label("Takéto lietadlo sa nenašlo"));
        }

        FormLayout fl = new FormLayout();

        Label nazovVyrobcu = new Label(lietadloUi.getVyrobca());
        nazovVyrobcu.setCaption("Názov výrobcu");

        Label medzinarodnyKod = new Label(lietadloUi.getMedzinarodnyKod());
        medzinarodnyKod.setCaption("Medzinárodný kód");

        Label priorita = new Label(lietadloUi.getPriorita().toString());
        priorita.setCaption("Priorita");

        Label minimalnaDlzkaDrahy = new Label(lietadloUi.getMinDlzkaDrahy().toString());
        minimalnaDlzkaDrahy.setCaption("Minimálna dĺžka odletovej dráhy");

        Label casPriletu = new Label(lietadloUi.getCasPriletu().toString());
        casPriletu.setCaption("Čas príletu");

        Label casPoziadavky = new Label(lietadloUi.getCasPoziadavkyOPridelenie().toString());
        casPoziadavky.setCaption("Čas požiadavky o pridelenie odletovej dráhy");

        fl.addComponents(
                nazovVyrobcu,
                medzinarodnyKod,
                priorita,
                minimalnaDlzkaDrahy,
                casPriletu,
                casPoziadavky
        );

        this.lietadlaDetails.addComponent(fl);
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_3_1.getTitle();
    }
}
