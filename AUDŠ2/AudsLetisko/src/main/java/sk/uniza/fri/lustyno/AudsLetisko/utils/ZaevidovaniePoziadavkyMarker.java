package sk.uniza.fri.lustyno.AudsLetisko.utils;

public enum ZaevidovaniePoziadavkyMarker {

    LIETADLO_AUTOMATICKY_PRIRADENE_NA_DRAHU,

    LIETADLO_PRIDANE_DO_FRONTU_CAKAJUCICH

}
