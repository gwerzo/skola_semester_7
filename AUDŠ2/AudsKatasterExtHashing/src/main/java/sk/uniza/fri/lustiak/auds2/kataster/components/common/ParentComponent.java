package sk.uniza.fri.lustiak.auds2.kataster.components.common;

public interface ParentComponent {

    void hideComponent();

    void showComponent();

    String getComponentTitle();

}
