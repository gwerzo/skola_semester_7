﻿using System.Data.Entity;
using System.Linq;
using MemoryGameDaoApp.Bo;
using PexesoApi.Bo;

namespace MemoryGameDaoApp.Context
{
    public class MemoryGameDbContext : DbContext
    {

        private static string ConnectionString =
            "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\42191\\OneDrive\\Documents\\memorygame.mdf;Integrated Security=True;Connect Timeout=30";

        public MemoryGameDbContext(): base(ConnectionString)
        {
        }

        public DbSet<GameStyle> GameStyles { get; set; }

        public DbSet<GameType> GameTypes { get; set; }
        public DbSet<MemoryGame> MemoryGames { get; set; }
        public DbSet<MemoryGameStep> MemoryGameSteps { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<GameCard> GameCards { get; set; }

        public DbSet<CardPrefixPostfix> CardPrefixesPostfixes { get; set; }

        public Player GetPlayerByUsername(string username)
        {
            return this.Players.SingleOrDefault(e => e.Username.Equals(username));
        }

        public GameType GetGameTypeById(int id)
        {
            return this.GameTypes.SingleOrDefault(e => e.GameTypeId == id);
        }

        public GameStyle GetGameStyleById(int id)
        {
            return this.GameStyles.SingleOrDefault(e => e.GameStyleId == id);
        }

        public CardPrefixPostfix GetCardPrefixForGameStyle(int gameStyleId)
        {
            CardPrefixPostfix cp = this.CardPrefixesPostfixes.SingleOrDefault(e => e.GameStyleID == gameStyleId);
            return cp;
        }


    }
}
