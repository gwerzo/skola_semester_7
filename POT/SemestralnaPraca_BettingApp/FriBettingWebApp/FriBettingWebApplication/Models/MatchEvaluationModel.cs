﻿using System.Collections.Generic;

namespace FriBettingWebApplication.Models
{
    public class MatchEvaluationModel
    {
        public int MatchId { get; set; }
        public List<int> MatchPartsIds { get; set; }

    }
}