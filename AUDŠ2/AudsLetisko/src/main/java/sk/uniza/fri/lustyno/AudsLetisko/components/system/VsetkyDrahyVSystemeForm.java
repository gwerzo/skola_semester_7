package sk.uniza.fri.lustyno.AudsLetisko.components.system;

import com.vaadin.ui.Grid;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.OdletovaDrahaTableUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.RunwayManagementService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;

import java.util.List;

public class VsetkyDrahyVSystemeForm extends BusinessComponent {

    private RunwayManagementService runwayManagementService;

    public VsetkyDrahyVSystemeForm(RepaintableComponent parentComponent) {
        super(parentComponent, MENU_TITLE.SYSTEM_MENU_7.getTitle());
        this.runwayManagementService = UIServices.getRunwayManagementService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.parenComponent.removeAllComponents();

        VerticalLayout vl = new VerticalLayout();

        Grid<OdletovaDrahaTableUiEntity> odletovaDrahaTableUiEntityGrid = new Grid<>();
        odletovaDrahaTableUiEntityGrid.addColumn(OdletovaDrahaTableUiEntity::getId).setCaption("ID Dráhy");
        odletovaDrahaTableUiEntityGrid.addColumn(OdletovaDrahaTableUiEntity::getDlzka).setCaption("Dĺžka");
        odletovaDrahaTableUiEntityGrid.addColumn(OdletovaDrahaTableUiEntity::getJeVolna).setCaption("Voľná");

        odletovaDrahaTableUiEntityGrid.setSizeFull();

        HandledOperationExecutor.submit(() -> {

            List<OdletovaDrahaTableUiEntity> entityList = this.runwayManagementService.getVsetkyOdletoveDrahyVSystemeTableEntities();
            if (entityList != null && entityList.size() != 0) {
                odletovaDrahaTableUiEntityGrid.setItems(entityList);
            }

            return OperationMarker.OPERATION_SUCCESFUL;
        });

        vl.addComponent(odletovaDrahaTableUiEntityGrid);

        this.parenComponent.showComponent(new Panel("Všetky odletové dráhy v systéme", vl));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_7.getTitle();
    }
}
