﻿using System.Web.Http;
using FriBettingWebApplication.Filters;
using FriBettingWebApplication.Services.Face;

namespace FriBettingWebApplication.Controllers.Api
{

    public class SportsController : ApiController
    {
        private readonly ISportsService _sportsService;

        public SportsController(ISportsService sportsService)
        {
            _sportsService = sportsService;
        }

        [HttpGet]
        [JwtAuthentication]
        public IHttpActionResult List()
        {
            return Ok(_sportsService.ListAllSports());
        }

        [HttpGet]
        [Route("api/sports/oddtypes")]
        [AllowAnonymous]
        public IHttpActionResult ListOddTypesForSport(int sportId)
        {
            return Ok(_sportsService.ListAllOddTypesForSport(sportId));
        }


    }
}
