﻿using System.Collections.Generic;
using FriBettingWebApplication.Dto.Request;
using FriBettingWebApplication.Dto.Response;

namespace FriBettingWebApplication.Services.Face
{
    public interface ITicketService
    {

        OperationResponse AddOddToOpenedTicket(AddOddToTicketRequest request, string usernameIdentity);
        TicketDetailResponse GetCurrentTicketForUser(string usernameIdentity);
        OperationResponse CreateTicket(CreateCurrentTicketRequest request, string usernameIdentity);
        List<ClosedTicketDetailResponse> GetAllClosedAndNotEvaluatedTicketsForUser(string usernameIdentity);
        List<EvaluatedTicketDetailResponse> GetAllEvaluatedTicketsForUser(string usernameIdentity);


    }
}
