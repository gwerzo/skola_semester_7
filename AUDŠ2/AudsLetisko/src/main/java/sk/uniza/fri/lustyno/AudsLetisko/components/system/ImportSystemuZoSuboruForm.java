package sk.uniza.fri.lustyno.AudsLetisko.components.system;

import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.persistance.HistoriaFileManager;
import sk.uniza.fri.lustyno.AudsLetisko.persistance.LietadlaFileManager;
import sk.uniza.fri.lustyno.AudsLetisko.persistance.OdletoveDrahyFileManager;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;

public class ImportSystemuZoSuboruForm extends BusinessComponent {

    private LietadlaFileManager lietadlaFileManager;

    private OdletoveDrahyFileManager odletoveDrahyFileManager;

    private HistoriaFileManager historiaFileManager;

    public ImportSystemuZoSuboruForm(RepaintableComponent component) {
        super(component, MENU_TITLE.SYSTEM_MENU_4.getTitle());
        this.lietadlaFileManager = UIServices.getVsetkyLietadlaFileManager();
        this.odletoveDrahyFileManager = UIServices.getOdletoveDrahyFileManager();
        this.historiaFileManager = UIServices.getHistoriaFileManager();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        VerticalLayout layout = new VerticalLayout();

        ProgressBar progressBar = new ProgressBar(0.0f);
        progressBar.setCaption("Priebeh exportu");

        Button importButton = new Button("Spustiť import");

        importButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {

                this.lietadlaFileManager.nacitajVsetkyLietadlaDoSystemu();
                progressBar.setValue(0.2f);

                this.odletoveDrahyFileManager.nacitajVsetkyOdletoveDrahyZoSuboru();
                progressBar.setValue(0.4f);

                this.lietadlaFileManager.nacitajIdleLietadlaZoSuboru();
                progressBar.setValue(0.6f);

                this.historiaFileManager.nacitajUdajeOOdletochZoSuboru();
                progressBar.setValue(0.7f);

                this.lietadlaFileManager.nacitajLietadlaSPridelenouDrahou();
                progressBar.setValue(0.8f);

                this.lietadlaFileManager.nacitajLietadlaCakajuceNaPridelenieDrahy();
                progressBar.setValue(0.9f);

                this.odletoveDrahyFileManager.nacitajVsetkyVolneDrahyZoSuboru();
                progressBar.setValue(1f);

                CustomNotification.notification("Import prebehol v poriadku");

                return OperationMarker.OPERATION_SUCCESFUL;
            });
        });

        layout.addComponent(progressBar);
        layout.addComponent(importButton);

        this.parenComponent.showComponent(new Panel("Import systému zo súboru", layout));

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_4.getTitle();
    }
}
