package sk.uniza.fri.lustiak.auds2.kataster.components.kataster;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.DeleteRealEstateUi;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;
import sk.uniza.fri.lustiak.auds2.kataster.utils.StringToIntegerConverter;


public class VyradenieNehnutelnostiForm extends BusinessComponent {

    private Binder<DeleteRealEstateUi> binder;
    private DeleteRealEstateUi entity;

    private RealEstateService realEstateService;

    public VyradenieNehnutelnostiForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_3.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new DeleteRealEstateUi();
        this.binder = new Binder<>(DeleteRealEstateUi.class);

        VerticalLayout layout = new VerticalLayout();

        TextField idTf = new TextField("ID entity");

        Button vyraditButton = new Button("Vyradiť");
        vyraditButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {
                try {
                    this.binder.writeBean(this.entity);
                } catch (ValidationException ex) {
                    CustomNotification.error("Musíte vyplniť ID entity v správnom tvare");
                }
                OperationMarker om = this.realEstateService.removeRealEstate(this.entity);
                this.parentComponent.updateFileState();
                if (om.equals(OperationMarker.OPERATION_SUCCESFUL)) {
                    Notification.show("Nehnuteľnosť bola vyradená");
                }
                return om;
            });
        });
        vyraditButton.addStyleName("danger");

        this.binder.forField(idTf)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(DeleteRealEstateUi::getIdentifikacneCislo, DeleteRealEstateUi::setIdentifikacneCislo);

        layout.addComponent(idTf);
        layout.addComponent(vyraditButton);

        this.parentComponent.showComponent(new Panel("Vyradenie NEHNUTEĽNOSTI podľa ID", layout));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_3.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
