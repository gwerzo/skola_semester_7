package sk.uniza.fri.lustyno.AudsLetisko.entities_bo;

import lombok.Data;

@Data
public class OdletovaDrahaEntity {

    private Integer id;

    private Integer dlzkaDrahy;

    private LietadloEntity aktualnePrideleneLietadlo;

    public static class Builder {

        private Integer idDrahy;

        private Integer dlzkaDrahy;

        public Builder() {

        }

        public Builder sId(Integer id) {
            this.idDrahy = id;
            return this;
        }

        public Builder sDlzkouDrahy(Integer dlzkaDrahy) {
            this.dlzkaDrahy = dlzkaDrahy;
            return this;
        }

        public OdletovaDrahaEntity build() {
            OdletovaDrahaEntity entity = new OdletovaDrahaEntity();
            entity.setId(this.idDrahy);
            entity.setDlzkaDrahy(this.dlzkaDrahy);
            return entity;
        }

    }

}
