package sk.uniza.fri.lustyno.datastructures.utils;

import java.util.BitSet;

public class CustomBitSet extends BitSet {

    private int fixedLength;

    public CustomBitSet(int fixedLength) {
        super(fixedLength);
        this.fixedLength = fixedLength;
    }

    @Override
    public int length() {
        return this.fixedLength;
    }

}
