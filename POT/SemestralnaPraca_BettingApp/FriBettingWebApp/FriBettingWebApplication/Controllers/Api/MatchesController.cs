﻿using System.Web.Http;
using FriBettingWebApplication.Dto.Request;
using FriBettingWebApplication.Dto.Response.Page;
using FriBettingWebApplication.Filters;
using FriBettingWebApplication.Services.Face;

namespace FriBettingWebApplication.Controllers.Api
{
    public class MatchesController : ApiController
    {
        private readonly IMatchService _matchService;

        public MatchesController(IMatchService matchService)
        {
            _matchService = matchService;
        }

        [HttpPost]
        [JwtAuthentication]
        public IHttpActionResult CreateNewMatch([FromBody] CreateNewMatchRequest createNewMatchRequest)
        {
            var createdStatus = _matchService.CreateNewMatch(createNewMatchRequest);
            return Ok(createdStatus);
        }

        [HttpGet]
        [JwtAuthentication]
        public IHttpActionResult GetMatchesFiltered([FromUri] GetMatchesFilteredRequest getMatchesFilteredRequest)
        {
            int count;
            var matchesFiltered = _matchService.GetMatchesFiltered(getMatchesFilteredRequest, out count);
            return Ok(new MatchesPageResponse
            {
                Matches = matchesFiltered,
                Count = count
            });
        }

        [HttpGet]
        [Route("api/matches/odds")]
        [JwtAuthentication]
        public IHttpActionResult GetOddsForMatch(int matchId)
        {
            return Ok(_matchService.GetOddsForMatch(matchId));
        }

        [HttpGet]
        [Route("api/matches/parts")]
        [JwtAuthentication]
        public IHttpActionResult GetMatchPartsForMatch(int matchId)
        {
            return Ok(_matchService.GetMatchPartsForMatch(matchId));
        }

        [HttpPost]
        [Route("api/matches/evaluate")]
        [JwtAuthentication]
        public IHttpActionResult EvaluateMatch([FromBody] MatchEvaluationResultRequest request)
        {
            return Ok(_matchService.EvaluateMatch(request));
        }
    }
}
