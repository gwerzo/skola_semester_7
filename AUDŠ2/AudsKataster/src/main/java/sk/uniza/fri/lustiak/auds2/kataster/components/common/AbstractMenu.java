package sk.uniza.fri.lustiak.auds2.kataster.components.common;

import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractMenu extends VerticalLayout {

    protected List<BusinessComponent> menuItems = new ArrayList<>();

    protected void initMenuItems() {

        menuItems.forEach(e -> {
            Button b = new Button(e.getComponentTitle());
            b.setWidth("100%");
            b.setHeight("60px");
            b.setEnabled(e.isImplemented());

            b.addClickListener(but -> {
                e.showComponent();
            });

            addComponent(b);
        });

    }

}
