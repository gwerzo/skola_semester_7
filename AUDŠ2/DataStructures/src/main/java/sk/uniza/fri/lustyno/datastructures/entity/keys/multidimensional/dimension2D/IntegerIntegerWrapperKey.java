package sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.dimension2D;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValueOnDimension;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureWrongUsageException;

public final class IntegerIntegerWrapperKey implements HasComparableValueOnDimension<IntegerIntegerUniqueKey> {

    private IntegerIntegerUniqueKey keyValue;

    public IntegerIntegerWrapperKey(int x, int y, int uniqueIdentifier) {
        this.keyValue = new IntegerIntegerUniqueKey(x,y, uniqueIdentifier);
    }

    @Override
    public int compareToOnDimension(HasComparableValueOnDimension<IntegerIntegerUniqueKey> other, int dimension) {
        if (dimension == 0) {
            return Integer.compare(this.keyValue.getX(), other.getValue().getX());
        } else if (dimension == 1) {
            return Integer.compare(this.keyValue.getY(), other.getValue().getY());
        } else {
            throw new DataStructureWrongUsageException(
                    this.getClass().getName(),
                    "compareToOnDimension",
                    "Pokúšaš sa použiť tento typ kľúča s dimenziou, ktorú nepodporuje, dimenzia = " + dimension);
        }
    }

    @Override
    public boolean equalsOnAllDimensions(HasComparableValueOnDimension<IntegerIntegerUniqueKey> other) {
        return this.keyValue.getX() == other.getValue().getX() && this.keyValue.getY() == other.getValue().getY();
    }

    @Override
    public IntegerIntegerUniqueKey getValue() {
        return this.keyValue;
    }

    @Override
    public String toString() {
        return "X: " + this.keyValue.getX() + ", Y: " + this.keyValue.getY() + ", UNIQ: " + this.keyValue.getUniqueIdentifier();
    }

}
