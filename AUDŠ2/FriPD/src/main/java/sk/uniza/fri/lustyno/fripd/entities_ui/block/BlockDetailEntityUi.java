package sk.uniza.fri.lustyno.fripd.entities_ui.block;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class BlockDetailEntityUi {

    private Integer cisloBloku;
    private Integer offsetVSubore;
    private Integer pocetValidnychZaznamov;
    private Integer offsetPreplnovaciehoBloku;

    private List<ZaznamEntityUi> zaznamyBloku;

}
