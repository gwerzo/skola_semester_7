﻿using Newtonsoft.Json;

namespace FriBettingWebApplication.Dto.Response
{
    public class OddTypeResponse
    {

        [JsonProperty("Odd_Type_Id")] 
        public int OddTypeId { get; set; }

        [JsonProperty("Odd_Type_Code")] 
        public string OddTypeCode { get; set; }

        [JsonProperty("Odd_Type_Desc")] 
        public string OddTypeDesc { get; set; }

    }
}