package sk.uniza.fri.lustyno.AudsLetisko.components.airport;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.VyradenieLietadlaUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.AircraftManagementService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;

public class VyradenieLietadlaForm extends BusinessComponent {

    private VyradenieLietadlaUiEntity entity;

    private Binder<VyradenieLietadlaUiEntity> binder;

    private AircraftManagementService aircraftManagementService;

    public VyradenieLietadlaForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_10.getTitle());
        this.aircraftManagementService = UIServices.getAircraftManagementService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.binder = new Binder<>(VyradenieLietadlaUiEntity.class);
        this.entity = new VyradenieLietadlaUiEntity();

        VerticalLayout layout = new VerticalLayout();

        FormLayout fl = new FormLayout();

        TextField medzinarodnyKodLietadla = new TextField("Medzinárodný kód lietadla");

        this.binder.forField(medzinarodnyKodLietadla)
                .asRequired()
                .bind(VyradenieLietadlaUiEntity::getMedzinarodnyKodLietadla, VyradenieLietadlaUiEntity::setMedzinarodnyKodLietadla);

        Button vyradit = new Button("Vyradiť");
        vyradit.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);

                HandledOperationExecutor.submit(() -> {
                    OperationMarker m = this.aircraftManagementService.vyradLietadlo(this.entity);
                    if (m == OperationMarker.OPERATION_SUCCESFUL) {
                        CustomNotification.notification("Lietadlo bolo vyradené");
                    } else if (m == OperationMarker.OPERATION_NEUTRAL) {
                        CustomNotification.notification("Lietadlo bolo vyradené a jeho dráha bola priradená lietadlu s medzinárodným kódom " + m.getMessage());
                    } else if (m == OperationMarker.OPERATION_UNSUCCESFUL) {
                        CustomNotification.warning("Lietadlo nemôže byť vyradené, lebo je medzi lietadlami na letisku, " +
                                "ktoré nemajú ani pridelenú dráhu a ani nečakajú na pridelenie!");
                    }
                    showComponent();
                    return m;
                });

            } catch (ValidationException ex) {
                CustomNotification.error("Musíte vyplniť medzinárodný kód lietadla");
            }
        });

        fl.addComponents(medzinarodnyKodLietadla);
        fl.addComponents(vyradit);

        layout.addComponent(fl);

        this.parenComponent.showComponent(new Panel("Vyradenie lietadla",  layout));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_10.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
