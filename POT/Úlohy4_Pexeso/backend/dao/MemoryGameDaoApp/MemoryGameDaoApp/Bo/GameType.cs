﻿namespace MemoryGameDaoApp.Bo
{
    public class GameType
    {
        public int GameTypeId { get; set; }
        public int XLength { get; set; }
        public int YLength { get; set; }

        public GameType()
        {

        }
    }
}
