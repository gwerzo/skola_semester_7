package sk.uniza.fri.lustyno.datastructures.entity;

public interface Hashable {

    public int getHash();

}
