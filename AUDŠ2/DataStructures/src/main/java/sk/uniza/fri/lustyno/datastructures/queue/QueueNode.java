package sk.uniza.fri.lustyno.datastructures.queue;

import sk.uniza.fri.lustyno.datastructures.entity.AbstractKeyValueEntity;
import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;

/**
 * Node reprezentujúci uzol prioritného frontu
 *
 * @param <K> - Kľúč, ktorý reprezentuje prioritu vo fronte
 * @param <D> - Dáta, ktoré sú v tomto node uchovávané
 */
public class QueueNode<K extends HasComparableValue, D> extends AbstractKeyValueEntity<K, D> {

    private QueueNode<K, D> parent;

    private QueueNode<K, D> leftChildo;

    private QueueNode<K, D> rightChild;

    public QueueNode(K priority, D data) {
        super(priority, data);
    }

    public QueueNode<K, D> getParent() {
        return parent;
    }

    public void setParent(QueueNode<K, D> parent) {
        this.parent = parent;
    }

    public QueueNode<K, D> getLeftChild() {
        return leftChildo;
    }

    public void setLeftChild(QueueNode<K, D> leftChild) {
        this.leftChildo = leftChild;
    }

    public QueueNode<K, D> getRightChild() {
        return rightChild;
    }

    public void setRightChild(QueueNode<K, D> rightChild) {
        this.rightChild = rightChild;
    }

    public boolean isRightChildOfParent() {
        if (this.parent.getRightChild() != null && this.parent.getRightChild().equals(this)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isRoot() {
        return this.parent == null;
    }

    @Override
    public String toString() {
        return this.getKey().getValue().toString();
    }

}
