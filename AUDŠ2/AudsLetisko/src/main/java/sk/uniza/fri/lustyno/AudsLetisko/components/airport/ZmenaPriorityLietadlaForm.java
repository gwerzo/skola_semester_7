package sk.uniza.fri.lustyno.AudsLetisko.components.airport;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.ZmenaPriorityUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.RunwayManagementService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;
import sk.uniza.fri.lustyno.AudsLetisko.utils.converters.StringToIntegerConverter;

public class ZmenaPriorityLietadlaForm extends BusinessComponent {

    private RunwayManagementService runwayManagementService;

    private Binder<ZmenaPriorityUiEntity> binder;

    private ZmenaPriorityUiEntity entity;

    public ZmenaPriorityLietadlaForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_8.getTitle());
        this.runwayManagementService = UIServices.getRunwayManagementService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.binder = new Binder<>(ZmenaPriorityUiEntity.class);
        this.entity = new ZmenaPriorityUiEntity();

        VerticalLayout layout = new VerticalLayout();

        FormLayout fl = new FormLayout();

        TextField medzinarodnyKod = new TextField("Medzinárodný kód");
        TextField novaPriorita = new TextField("Nová priorita");
        Button zmenit = new Button("Zmeniť");

        zmenit.addClickListener(e -> {
           try {
               this.binder.writeBean(this.entity);
               HandledOperationExecutor.submit(() -> {
                   OperationMarker m = this.runwayManagementService.zmenPriorituLietadlu(this.entity);
                   if (m == OperationMarker.OPERATION_SUCCESFUL) {
                       CustomNotification.notification("Zmena priority lietadla prebehla v poriadku");
                   }
                   return m;
               });
               showComponent();
           } catch (ValidationException ex) {
               CustomNotification.error("Musíte vyplniť aj medzinárodný kód aj novú prioritu lietadla v správnom formáte");
           }
        });

        fl.addComponents(
                medzinarodnyKod,
                novaPriorita,
                zmenit
        );

        this.binder.forField(medzinarodnyKod)
                .asRequired()
                .bind(ZmenaPriorityUiEntity::getMedzinarodnyKodLietadla, ZmenaPriorityUiEntity::setMedzinarodnyKodLietadla);
        this.binder.forField(novaPriorita)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(ZmenaPriorityUiEntity::getNovaPriorita, ZmenaPriorityUiEntity::setNovaPriorita);

        layout.addComponent(fl);

        this.parenComponent.showComponent(new Panel("Zmena priority lietadla", layout));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_8.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
