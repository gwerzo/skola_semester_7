package sk.uniza.fri.lustiak.auds2.kataster.components.common;

import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class SystemManagementComponent extends VerticalLayout {

    public SystemManagementComponent() {
        initComponents();
    }

    private void initComponents() {

        Label lMain = new Label("Funkcionalita manažmentu systému:");

        Label lEmpty = new Label();

        Label l1 = new Label("1. Inicializovanie systému náhodnými dátami");
        Label l2 = new Label("2. Uloženie katastra do súboru");
        Label l3 = new Label("3. Načítanie katastra zo súboru");

        l1.setWidth("100%");
        l2.setWidth("100%");
        l3.setWidth("100%");

        VerticalLayout labelsWrapper = new VerticalLayout(
                lMain,
                lEmpty,
                l1,
                l2,
                l3);

        Panel descPanel = new Panel(
                "Manažment systému",
                labelsWrapper
        );

        addComponent(descPanel);
    }
}
