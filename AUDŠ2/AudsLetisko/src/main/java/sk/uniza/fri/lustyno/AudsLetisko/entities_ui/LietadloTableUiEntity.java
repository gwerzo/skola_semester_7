package sk.uniza.fri.lustyno.AudsLetisko.entities_ui;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LietadloTableUiEntity {

    private String vyrobca;

    private String medzinarodnyKod;

    private LocalDateTime casPriletu;

    private Integer minDlzkaDrahy;

    private LocalDateTime casPoziadavkyOPridelenie;

    private Integer priorita;

    private LocalDateTime casADatumOdletu;

    private String idDrahy;

}
