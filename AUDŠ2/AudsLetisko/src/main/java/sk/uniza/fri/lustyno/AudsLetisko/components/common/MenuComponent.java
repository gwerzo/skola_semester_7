package sk.uniza.fri.lustyno.AudsLetisko.components.common;

import sk.uniza.fri.lustyno.AudsLetisko.components.airport.*;

public class MenuComponent extends AbstractMenu {

    public MenuComponent(RepaintableComponent component) {
        this.menuItems.add(new ZaevidovaniePriletuForm(component));
        this.menuItems.add(new ZaevidovaniePoziadavkyNaOdletovuDrahuForm(component));
        this.menuItems.add(new VyhladanieLietadlaForm(component));
        this.menuItems.add(new VyhladanieLietadlaPodlaVyrobcuAMedzinarodnehoKoduForm(component));
        this.menuItems.add(new VyhladanieLietadlaNaOdletovejDraheForm(component));
        this.menuItems.add(new VlozenieInformaciiOOdleteForm(component));
        this.menuItems.add(new VypisVsetkychLietadielForm(component));
        this.menuItems.add(new VypisVsetkychLietadielCakajucichNaKonkretnejDraheForm(component));
        this.menuItems.add(new ZmenaPriorityLietadlaForm(component));
        this.menuItems.add(new VypisVsetkychOdletovychDrahForm(component));
        this.menuItems.add(new VyradenieLietadlaForm(component));
        initMenuItems();
    }

}
