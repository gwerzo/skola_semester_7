﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;

namespace DisertationThemes.ViewerGeneratorApp
{
    public abstract class AbstractMenuItem
    {
        private MenuHolder MenuHolder;

        public AbstractMenuItem(MenuHolder menuHolder)
        {
        }

        public void GoBackToMainMenu()
        {
            this.MenuHolder.PrintMenu(null);
        }

        public abstract string GetMenuText();

        public int PerformAction()
        {
            var oldColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Blue;
            var returnVal = this.PerformActionInternally();
            Console.ForegroundColor = oldColor;
            return returnVal;
        }

        protected virtual int PerformActionInternally()
        {
            return -1;
        }
    }
}