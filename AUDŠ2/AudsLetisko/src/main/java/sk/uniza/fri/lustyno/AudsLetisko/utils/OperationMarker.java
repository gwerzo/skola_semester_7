package sk.uniza.fri.lustyno.AudsLetisko.utils;

public enum OperationMarker {

    OPERATION_SUCCESFUL(""),
    OPERATION_UNSUCCESFUL(""),
    OPERATION_NEUTRAL("");

    private String message;

    OperationMarker(String message) {
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
