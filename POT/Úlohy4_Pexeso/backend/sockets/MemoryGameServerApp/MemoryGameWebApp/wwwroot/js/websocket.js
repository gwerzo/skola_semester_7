﻿var wsUri = "ws://localhost:8088";
var output;

function init() {
    connectToMemoryGameServer();
}

function connectToMemoryGameServer() {
    websocket = new WebSocket(wsUri);
    websocket.onopen = function (evt) { onOpen(evt) };
    websocket.onclose = function (evt) { onClose(evt) };
    websocket.onmessage = function (evt) { onMessage(evt) };
    websocket.onerror = function (evt) { onError(evt) };
}

function onOpen(evt) {
    $("#ckbx-circle-4-1").prop('checked', true);
    tryReauthenticate();
}

function onClose(evt) {
    $("#ckbx-circle-4-1").prop('checked', false);
}

function onMessage(evt) {
    var response = evt.data.split(" ");

    if (response.length != 0) {

        if (response[0] == "-authsucc") {
            localStorage.setItem(localStorageMemoryGameSessionIdKey, response[1]);
            window.location.href = "./index";
        } else if (response[0] == "-authunsucc") {
            $.notify("Nesprávne prihlasovacie údaje", "error");
        } else if (response[0] == "-reauthsucc") {
            $.notify("Prihlásenie prebehlo v poriadku", "success");
        } else if (response[0] == "-reauthunsucc") {
            localStorage.removeItem(localStorageMemoryGameSessionIdKey);
            window.location.href = "./index";
        } else if (response[0] == "-logoutsucc") {
            localStorage.removeItem(localStorageMemoryGameSessionIdKey);
            window.location = "/index";
        } else if (response[0] == "-loggedinuserslist") {
            showLoggedInUsers(response);
        } else if (response[0] == "-opengameslist") {
            showOpenGames(response);
        } else if (response[0] == "-rndgamesucc") {
            startPendingForSecondPlayer(response);
        } else if (response[0] == "-rndgameunsucc") {
            $.notify(response, "error");
        } else if (response[0] == "-cardhit") {
            openCard(response[1], response[2], response[3]);
        } else if (response[0] == "-cardhitunsucc") {
            $.notify(response.slice(1, response.length).join(" "), "error");
        } else if (response[0] == "-cardhitsuccboth") {
            openCardsFinally(response[1], response[2], response[3], response[4], response[5]);
        } else if (response[0] == "-cardhitsecondunsucc") {
            secondCardHitUnsuccessful(response[1], response[2], response[3]);
        } else if (response[0] == "-joinsucc") {
            openGameAfterSuccJoin(response);
            $.notify("Pripojenie do hry prebehlo v poriadku", "success");
        } else if (response[0] == "-joinsunucc") {
            $.notify(response.slice(1, response.length).join(" "), "error");
        } else if (response[0] == "-finishedgame") {
            gameFinished(response);
        } else if (response[0] == "-pm") {
            showPrivateMessage(response);
        } else if (response[0] == "-pmunsucc") {
            $.notify("Používateľ(" + response[1] + ") - " + response.slice(2, response.length).join(" "), "error");
        } else if (response[0] == "-gamecancelhost" || response[0] == "-gamecancelguest") {
            $.notify("Host prerušil spojenie so serverom, hra je zrušená", "error");
            localStorage.removeItem("activeGameUuid");
        } else if (response[0] == "-invitecreated") {
            startPendingForSecondPlayer(response);
        } else if (response[0] == "-inviteunsucc") {
            $.notify("Chyba pri vytváraní výzvy: " + response.slice(1, response.length).join(" "), "error");
        } else if (response[0] == "-invitedby") {
            $.notify("Používateľ " + response[1] + " vás vyzval do hry");
        } else if (response[0] == "-myinviteslist") {
            showMyInvites(response);
        }
    }

}

function sendMessageToMemoryGameServer(message) {
    websocket.send(message);
}

function tryLoginUser() {
    var username = $("#login-username").val();
    var password = $("#login-password").val();
    sendMessageToMemoryGameServer("-authenticate " + username + " " + password);
}

function logoutUser() {
    sendMessageToMemoryGameServer("-logout " + localStorage.getItem(localStorageMemoryGameSessionIdKey));
}

function tryReauthenticate() {
    if (localStorage.getItem(localStorageMemoryGameSessionIdKey) != null) {
        sendMessageToMemoryGameServer("-authsession " + localStorage.getItem(localStorageMemoryGameSessionIdKey));
    }
}

function getLoggedInUsers() {
    sendMessageToMemoryGameServer("-listloggedinusers");
}

function getMyInvites() {
    sendMessageToMemoryGameServer("-listmyinvites");
}

function getOpenGames() {
    sendMessageToMemoryGameServer("-listopengames");
}

function createNewGame() {
    var gameTypeId = $("#GameTypeSelectList").val();
    var gameStyleId = $("#GameStyleSelectList").val();

    sendMessageToMemoryGameServer("-rndgame " + gameStyleId + " " + gameTypeId);
}

function createGameInvite(username) {
    var gameTypeId = $("#GameTypeSelectList").val();
    var gameStyleId = $("#GameStyleSelectList").val();

    sendMessageToMemoryGameServer("-invitetogame " + gameTypeId + " " + gameStyleId + " " + username);
}

function tryHitCard(gameUuid, x, y) {
    sendMessageToMemoryGameServer("-cardhit " + gameUuid + " " + x + " " + y);
}

function joinGame(gameUuid) {
    sendMessageToMemoryGameServer("-joingame " + gameUuid);
}

function sendPrivateMessage(username, message) {
    sendMessageToMemoryGameServer("-pm " + username + " " + message);
}

window.addEventListener("load", init, false);