package tree.bst;

import org.junit.jupiter.api.*;
import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTree;
import sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTreeNode;

import java.util.Arrays;
import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BinarySearchTreeBasicTest {


    Integer[] deterministicValues = new Integer[] {50, 60, 40, 66, 21, 74, 100, 30, 29, 59, 61, 55};
    private List<Integer> deterministicValuesList = Arrays.asList(deterministicValues);

    private BinarySearchTree<IntegerKey, String> bst = new BinarySearchTree<>();

    @Test
    @Order(1)
    public void test_DETERMINISTIC_INSERTION_AND_FIND() {
        deterministicValuesList.forEach(e -> {
            bst.insert(new BinarySearchTreeNode<>(new IntegerKey(e.intValue()), "NODE"));
        });

        BinarySearchTreeNode<IntegerKey, String> node100 = bst.find(new IntegerKey(100)).get();

        Assertions.assertEquals(
                node100.getParent().getKey().getValue().intValue(), 74);
        Assertions.assertEquals(
                node100.getParent().getParent().getKey().getValue().intValue(), 66);
        Assertions.assertEquals(
                node100.getParent().getParent().getParent().getKey().getValue().intValue(), 60);
        Assertions.assertEquals(
                node100.getParent().getParent().getParent().getParent().getKey().getValue().intValue(), 50);


        BinarySearchTreeNode<IntegerKey, String> node50Root = bst.find(new IntegerKey(50)).get();
        Assertions.assertEquals(
                node50Root.getParent(), null);
        Assertions.assertEquals(
                node50Root.getLeftChildo().getKey().getValue().intValue(), 40);
        Assertions.assertEquals(
                node50Root.getLeftChildo().getLeftChildo().getKey().getValue().intValue(), 21);
        Assertions.assertEquals(
                node50Root.getLeftChildo().getLeftChildo().getRightChild().getKey().getValue().intValue(), 30);
        Assertions.assertEquals(
                node50Root.getLeftChildo().getLeftChildo().getRightChild().getLeftChildo().getKey().getValue().intValue(), 29);

        BinarySearchTreeNode<IntegerKey, String> node59Root = bst.find(new IntegerKey(59)).get();
        Assertions.assertEquals(
                node59Root.getParent().getKey().getValue().intValue(), 60);
        Assertions.assertEquals(
                node59Root.getParent().getParent().getKey().getValue().intValue(), 50);

    }

    @Test
    @Order(2)
    public void test_DELETE_NODE_AS_LEAF() {
        bst.delete(new IntegerKey(100));

        BinarySearchTreeNode<IntegerKey, String> deleted = bst.find(new IntegerKey(100)).get();

        Assertions.assertNull(deleted);

    }

    @Test
    @Order(3)
    public void test_DELETE_NODE_WITH_ONE_CHILD() {
        bst.delete(new IntegerKey(21));

        BinarySearchTreeNode<IntegerKey, String> deleted = bst.find(new IntegerKey(21)).get();

        Assertions.assertNull(deleted);

        BinarySearchTreeNode<IntegerKey, String> node29 = bst.find(new IntegerKey(29)).get();
        Assertions.assertEquals(
                node29.getParent().getParent().getParent().getKey().getValue().intValue(), 50
        );
    }

    @Test
    @Order(4)
    public void test_DELETE_NODE_WITH_BOTH_CHILDREN() {
        bst.delete(new IntegerKey(66));
    }

    @Test
    @Order(5)
    public void test_WALK_LEVEL_ORDER() {
        BinarySearchTree<IntegerKey, String> bstForLevelOrder = new BinarySearchTree<>();
        deterministicValuesList.forEach(e -> {
            bstForLevelOrder.insert(new IntegerKey(e), "LVL");
        });
        Integer[] inOrderVals = new Integer[] {50, 40, 60, 21, 59, 66, 30, 55, 61, 74, 29, 100};
        List<BinarySearchTreeNode<IntegerKey, String>> inOrder = bstForLevelOrder.levelOrder();
        for (int i = 0; i < inOrder.size(); i++) {
            Assertions.assertEquals(inOrder.get(i).getKey().getValue(), inOrderVals[i]);
        }

    }

}
