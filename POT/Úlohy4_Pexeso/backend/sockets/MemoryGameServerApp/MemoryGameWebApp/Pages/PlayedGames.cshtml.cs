using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;
using MemoryGameWebApp.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace MemoryGameWebApp.Pages
{
    public class PlayedGamesModel : PageModel
    {

        private static readonly HttpClient client = new HttpClient();

        [BindProperty]
        public List<MemoryGamesListResponseDTO> Games { get; set; }

        public List<string> TableHeaderColumns = new List<string> { "GameId", "Player1", "Player2", "Winner", "StepsCount", "GameStartDateTime", "GameLengthInSeconds" };

        public PlayedGamesModel()
        {
            this.Games = new List<MemoryGamesListResponseDTO>();
        }

        public async Task<ActionResult> OnGet()
        {
            MemoryGameFilterRequestDTO request = new MemoryGameFilterRequestDTO();
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["GameId"] = request.GameId.ToString();
            query["PlayerId"] = request.PlayerId.ToString();
            query["SortingAttr"] = request.SortingAttr;
            query["SortingOrder"] = request.SortingOrder.ToString();

            var url = API_CONF.API_URL + "games/filter?" + query.ToString();
            var streamTask = await client.GetStreamAsync(url);

            var games = await JsonSerializer.DeserializeAsync<List<MemoryGamesListResponseDTO>>(streamTask);

            this.Games = games;
            return Page();
        }
    }
}
