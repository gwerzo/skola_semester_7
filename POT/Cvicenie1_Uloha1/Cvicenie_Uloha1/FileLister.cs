﻿using System;
using System.IO;

namespace Cvicenie_Uloha1
{
    class FileLister
    {
        const string MISSING_ARGS_EXCEPTION = "Musíte zadať aspoň jeden správny argument, ktorým je cesta k súboru";

        private ArgumentsHolder programArgs = new ArgumentsHolder();

        public FileLister(string[] args) {
            if (args.Length < 1)
            {
                Console.WriteLine(MISSING_ARGS_EXCEPTION);
                return;
            }
            else {
                this.programArgs.FilePath = args[0];
            }

            // Extraktovanie argumentov môže vyhodiť Runtime exception a zhodiť program
            this.programArgs.RecursionValue = ArgumentsExtractor.extractRecursive(args);
            this.programArgs.PaddingValue = ArgumentsExtractor.extractPadding(args);
            this.programArgs.Colorization = ArgumentsExtractor.extractColorize(args);
        }

        public void listFiles(string path, int padding, int depth) {
            if (this.programArgs.RecursionValue == (depth - 1) && this.programArgs.RecursionValue != 0) {
                return;
            }
            DirectoryInfo dir = new DirectoryInfo(path);

            DirectoryInfo[] directories = null;
            try {
                directories = dir.GetDirectories();
            } catch (UnauthorizedAccessException e) {
                Console.WriteLine(String.Format("{0:dd.MM.yyyy hh:mm}   {1, -10}    " + buildPadding(padding) + "{2, -60}\n", dir.LastWriteTime, "", dir.Name));
                return;
            }

            foreach (DirectoryInfo directory in directories) {
                string directoryString = String.Format("{0:dd.MM.yyyy hh:mm}    {1, -10}    " + buildPadding(padding) + "{2, -60}", directory.LastWriteTime, "", directory.Name + "\\");
                Console.WriteLine(directoryString);

                // Ak je povolené rekurzívne vypisovanie, zavolaj vypísanie obsahu aj podadresárov
                if (this.programArgs.RecursionValue != -1) {
                    listFiles(directory.FullName, padding + this.programArgs.PaddingValue, depth + 1);
                }
            }

            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files) {
                string fileString = String.Format("{0:dd.MM.yyyy hh:mm}    {1, -10}    " + buildPadding(padding) + "{2, -60}", file.LastWriteTime, file.Length, file.Name);
                if (this.programArgs.Colorization) {
                    Console.ForegroundColor = ConsoleColor.Blue;
                }
                Console.WriteLine(fileString);
                Console.ResetColor();
            }
        }


        private string buildPadding(int ofSpaces)
        {
            var sb = new System.Text.StringBuilder();
            for (int i = 0; i < ofSpaces; i++)
            {
                sb.Append(" ");
            }
            return sb.ToString();
        }

        static void Main(string[] args)
        {
            FileLister fl = new FileLister(args);

            fl.listFiles(fl.programArgs.FilePath, fl.programArgs.PaddingValue, 0);
        }
    }
}
