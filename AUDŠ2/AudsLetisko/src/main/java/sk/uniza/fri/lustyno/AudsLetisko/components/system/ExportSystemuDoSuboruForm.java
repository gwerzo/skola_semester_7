package sk.uniza.fri.lustyno.AudsLetisko.components.system;

import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.persistance.HistoriaFileManager;
import sk.uniza.fri.lustyno.AudsLetisko.persistance.LietadlaFileManager;
import sk.uniza.fri.lustyno.AudsLetisko.persistance.OdletoveDrahyFileManager;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;


public class ExportSystemuDoSuboruForm extends BusinessComponent {

    private LietadlaFileManager lietadlaFileManager;

    private OdletoveDrahyFileManager odletoveDrahyFileManager;

    private HistoriaFileManager historiaFileManager;

    public ExportSystemuDoSuboruForm(RepaintableComponent component) {
        super(component, MENU_TITLE.SYSTEM_MENU_3.getTitle());
        this.lietadlaFileManager = UIServices.getVsetkyLietadlaFileManager();
        this.odletoveDrahyFileManager = UIServices.getOdletoveDrahyFileManager();
        this.historiaFileManager = UIServices.getHistoriaFileManager();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        VerticalLayout layout = new VerticalLayout();

        ProgressBar progressBar = new ProgressBar(0.0f);
        progressBar.setCaption("Priebeh exportu");

        Button exportButton = new Button("Spustiť export");

        exportButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {

                this.odletoveDrahyFileManager.vycistiSubory();
                this.lietadlaFileManager.vycistiSubory();
                this.historiaFileManager.vycistiSubory();

                this.lietadlaFileManager.ulozVsetkyLietadla();
                progressBar.setValue(0.2f);

                this.odletoveDrahyFileManager.ulozVsetkyOdletoveDrahy();
                progressBar.setValue(0.4f);

                this.historiaFileManager.ulozUdajeOOdletochDoSuboru();
                progressBar.setValue(0.5f);

                this.lietadlaFileManager.ulozIdleLietadla();
                progressBar.setValue(0.6f);

                this.lietadlaFileManager.ulozLietadlaSPridelenouDrahou();
                progressBar.setValue(0.8f);

                this.lietadlaFileManager.ulozLietadlaCakajuceNaPridelenieDrahy();
                progressBar.setValue(0.9f);

                this.odletoveDrahyFileManager.ulozVsetkyVolneDrahyDoSuboru();
                progressBar.setValue(1f);

                CustomNotification.notification("Export prebehol v poriadku");

                return OperationMarker.OPERATION_SUCCESFUL;
            });
        });

        layout.addComponent(progressBar);
        layout.addComponent(exportButton);

        this.parenComponent.showComponent(new Panel("Export systému do súboru", layout));

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_3.getTitle();
    }
}

