﻿namespace MemoryGameDaoApp.Bo
{
    public class CardPrefixPostfix
    {

        public int CardPrefixPostfixId { get; set; }

        public int GameStyleID { get; set; }
        public GameStyle GameStyle { get; set; }

        public string Prefix { get; set; }

        public string Postfix { get; set; }

        public CardPrefixPostfix()
        {

        }

    }
}
