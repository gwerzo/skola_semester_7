package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditParcelUi {

    private Boolean editConstraints;
    private ParcelUiTableEntity oldParcelData;
    private ParcelUiTableEntity newParcelData;

}
