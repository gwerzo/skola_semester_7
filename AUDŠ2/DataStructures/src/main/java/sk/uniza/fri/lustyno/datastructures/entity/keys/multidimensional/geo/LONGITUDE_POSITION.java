package sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo;

public enum LONGITUDE_POSITION {
    EAST,
    WEST
}
