package sk.uniza.fri.lustyno.fripd.components.vehicles.common;

import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.VodicskyPreukazDetailEntityUi;

public class DrivingLicenseDetailForm extends FormLayout {

    private VodicskyPreukazDetailEntityUi vodicskyPreukazDetailEntityUi;

    private Label EVCLabel;
    private Label menoVodicaLabel;
    private Label priezviskoVodicaLabel;
    private Label datumUkonceniaPlatnostiLabel;
    private Label zakazViestVozidloLabel;
    private Label pocetDropravnychPriestupkovLabel;

    public DrivingLicenseDetailForm(VodicskyPreukazDetailEntityUi entityUi) {
        this.vodicskyPreukazDetailEntityUi = entityUi;
        initializeLayout();
        wrap();
    }

    private void initializeLayout() {
        this.EVCLabel = new Label(this.vodicskyPreukazDetailEntityUi.getEvcVodica());
        this.EVCLabel.setCaption("EVČ");

        this.menoVodicaLabel = new Label(this.vodicskyPreukazDetailEntityUi.getMenoVodica());
        this.menoVodicaLabel.setCaption("Meno vodiča");

        this.priezviskoVodicaLabel = new Label(this.vodicskyPreukazDetailEntityUi.getPriezviskoVodica());
        this.priezviskoVodicaLabel.setCaption("Priezvisko vodiča");

        this.datumUkonceniaPlatnostiLabel = new Label(this.vodicskyPreukazDetailEntityUi.getDatumUkonceniaPlatnostiPreukazu());
        this.datumUkonceniaPlatnostiLabel.setCaption("Platnosť do");

        this.zakazViestVozidloLabel = new Label(this.vodicskyPreukazDetailEntityUi.getZakazViestVozidlo());
        this.zakazViestVozidloLabel.setCaption("Zákaz viesť vozidlo");

        this.pocetDropravnychPriestupkovLabel = new Label(this.vodicskyPreukazDetailEntityUi.getPocetDopravnychPriestupkov());
        this.pocetDropravnychPriestupkovLabel.setCaption("Počet dopravných nehôd za posledných 12 rokov");
    }

    private void wrap() {
        this.addComponent(this.EVCLabel);
        this.addComponent(this.menoVodicaLabel);
        this.addComponent(this.priezviskoVodicaLabel);
        this.addComponent(this.datumUkonceniaPlatnostiLabel);
        this.addComponent(this.zakazViestVozidloLabel);
        this.addComponent(this.pocetDropravnychPriestupkovLabel);
    }


}
