package sk.uniza.fri.lustyno.datastructures.linearhashing.heapfile;

import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Record;

import java.util.ArrayList;
import java.util.List;

public class Batch<R extends Record> {

    private List<R> batchEntities;

    private Integer batchStartingOffset;

    public Batch(List<R> batchEntities, Integer batchStartingOffset) {
        this.batchEntities = new ArrayList<>(batchEntities);
        this.batchStartingOffset = batchStartingOffset;
    }

    public List<R> getEntities() {
        return this.batchEntities;
    }

    public Integer getBatchStartingOffset() {
        return this.batchStartingOffset;
    }

}
