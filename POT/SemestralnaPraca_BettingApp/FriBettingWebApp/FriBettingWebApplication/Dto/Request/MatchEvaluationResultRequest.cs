﻿using System.Collections.Generic;

namespace FriBettingWebApplication.Dto.Request
{
    public class MatchEvaluationResultRequest
    {
        public int? MatchId { get; set; }
        public List<MatchPartEvaluation> PartsEvaluation { get; set; }

        public class MatchPartEvaluation
        {
            public int? MatchPartId { get; set; }
            public int? HostGoals { get; set; }
            public int? GuestGoals { get; set; }
        }
    }
}