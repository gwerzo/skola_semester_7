package sk.uniza.fri.lustyno.fripd.factory;

import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.FromBytesFactory;
import sk.uniza.fri.lustyno.fripd.indexes.EVCStringIndex;

public class EVCStringIndexFactory extends FromBytesFactory<EVCStringIndex> {

    public EVCStringIndexFactory() {}

    @Override
    public EVCStringIndex fromBytes(byte[] bytes) {
        return EVCStringIndex.fromByteArray(bytes);
    }

    @Override
    public EVCStringIndex emptyInvalidInstance() {
        return EVCStringIndex.emptyInvalidInstance();
    }

}
