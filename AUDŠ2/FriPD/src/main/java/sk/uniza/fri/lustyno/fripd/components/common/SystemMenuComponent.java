package sk.uniza.fri.lustyno.fripd.components.common;

import sk.uniza.fri.lustyno.fripd.components.system.*;
import sk.uniza.fri.lustyno.fripd.components.system.license.PreukazyPrimarneBlokyForm;
import sk.uniza.fri.lustyno.fripd.components.system.license.PreukazyOverflowBlokyForm;
import sk.uniza.fri.lustyno.fripd.components.system.vehicle.evc.VozidlaEVCOverflowBlokyForm;
import sk.uniza.fri.lustyno.fripd.components.system.vehicle.evc.VozidlaEVCPrimarneBlokyForm;
import sk.uniza.fri.lustyno.fripd.components.system.vehicle.vin.VozidlaVINPrimarneBlokyForm;
import sk.uniza.fri.lustyno.fripd.components.system.vehicle.vin.VozidlaVINOverflowBlokyForm;
import sk.uniza.fri.lustyno.fripd.components.system.vehicle.VozidlaHeapFileBlokyForm;

public class SystemMenuComponent extends AbstractMenu {

    public SystemMenuComponent(RepaintableComponent component) {
        this.menuItems.add(new UlozenieSystemuForm(component));
        this.menuItems.add(new VygenerovanieDatForm(component));

        this.menuItems.add(new VozidlaVINPrimarneBlokyForm(component));
        this.menuItems.add(new VozidlaVINOverflowBlokyForm(component));

        this.menuItems.add(new VozidlaEVCPrimarneBlokyForm(component));
        this.menuItems.add(new VozidlaEVCOverflowBlokyForm(component));

        this.menuItems.add(new VozidlaHeapFileBlokyForm(component));

        this.menuItems.add(new PreukazyPrimarneBlokyForm(component));
        this.menuItems.add(new PreukazyOverflowBlokyForm(component));

        initMenuItems();
    }

}
