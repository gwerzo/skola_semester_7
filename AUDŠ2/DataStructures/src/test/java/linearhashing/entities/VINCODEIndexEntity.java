package linearhashing.entities;

import sk.uniza.fri.lustyno.datastructures.entity.keys.StringKey;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureWrongUsageException;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Record;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.HasSize;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.StringUtils;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;

import java.util.Arrays;

@HasSize(sizeInBytes = 22)
public class VINCODEIndexEntity extends Record<StringKey> {

    private StringKey key;

    private Integer offsetInHeapFile;

    public VINCODEIndexEntity(String VIN_CODE, Integer offsetInHeapFile) {
        this.key = new StringKey(VIN_CODE);
        this.offsetInHeapFile = offsetInHeapFile;
    }

    public VINCODEIndexEntity(Boolean valid, String VIN_CODE, Integer offsetInHeapFile) {
        this.valid = valid;
        this.key = new StringKey(VIN_CODE);
        this.offsetInHeapFile = offsetInHeapFile;
    }

    @Override
    public StringKey getKey() {
        return this.key;
    }

    public Integer getOffsetInHeapFile() {
        return this.offsetInHeapFile;
    }

    @Override
    public byte[] toBytesArray() {
        byte[] bytes = ToByteArrayConverter.concatenateByteArrays(
                Arrays.asList(
                        ToByteArrayConverter.bytesFromBoolean(this.valid),
                        ToByteArrayConverter.bytesFromString(this.key.getValue()),
                        ToByteArrayConverter.bytesFromInt(this.offsetInHeapFile)));
        return bytes;
    }

    @Override
    public int getHash() {
        int hash = 7;
        for (int i = 0; i < this.key.getValue().length(); i++) {
            hash = hash*31 + this.key.getValue().charAt(i);
        }

        return Math.abs(hash);
    }

    public static VINCODEIndexEntity fromByteArray(byte[] fromArray) {
        if (fromArray.length != 22) {
            throw new DataStructureWrongUsageException(VINCODEIndexEntity.class.getName(), "fromByteArray",
                    "Odparsovanie inštancie tejto triedy z poľa bytov vyžaduje 22 bytov");
        }
        Boolean valid = FromByteArrayConverter.booleanFromByte(fromArray[0]);
        String VIN_CODE = FromByteArrayConverter.stringFromBytes(Arrays.copyOfRange(fromArray, 1, 18));
        Integer offsetInHeapFile = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(fromArray, 18, 22));

        return new VINCODEIndexEntity(valid, VIN_CODE, offsetInHeapFile);
    }

    public static VINCODEIndexEntity emptyInvalidInstance() {
        return new VINCODEIndexEntity(false, StringUtils.padRightDollars("", 17), -1);
    }
}
