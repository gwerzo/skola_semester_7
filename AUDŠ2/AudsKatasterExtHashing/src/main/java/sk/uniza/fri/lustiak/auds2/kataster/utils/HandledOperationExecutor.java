package sk.uniza.fri.lustiak.auds2.kataster.utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HandledOperationExecutor {

    /**
     *
     * @param executionListener - Listener pre chránenú action, ktorú treba vykonať
     *
     */
    public static final void submit(OperationExecutorListener executionListener) {
        try {
            executionListener.executeOperation();
        } catch (BusinessException e) {
            CustomNotification.warning(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            CustomNotification.error("Vyskytla sa neznáma chyba");
        }
    }

    /**
     * Functional interface pre executor
     */
    public interface OperationExecutorListener {

        OperationMarker executeOperation();

    }
}
