package sk.uniza.fri.lustiak.auds2.kataster.components.common;

import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class HomeViewForm extends VerticalLayout {

    public HomeViewForm() {
        initComponents();
    }

    private void initComponents() {

        Label lMain = new Label("Funkcionalita poskytovaná informačným systémom:");

        Label lEmpty = new Label();

        Label l1 = new Label("1. Vyhľadanie NEHNUTEĽNOSTÍ podľa zadanej GPS pozície");
        Label l2 = new Label("2. Vyhľadanie PARCIEL podľa zadanej GPS pozície");
        Label l3 = new Label("3. Vyhľadanie VŠETKÝCH OBJEKTOV podľa zadanej GPS pozície");
        Label l4 = new Label("4. Pridanie novej NEHNUTEĽNOSTI");
        Label l5 = new Label("5. Pridanie novej PARCELY");
        Label l6 = new Label("6. Editácia údajov NEHNUTEĽNOSTI podľa zadanej GPS súradnice (aj jej GPS súradnice)");
        Label l7 = new Label("7. Editácia údajov PARCELY podľa zadanej GPS súradnice (aj jej GPS súradnice)");
        Label l8 = new Label("8. Vyradenie NEHNUTEĽNOSTI podľa zadanej GPS súranice");
        Label l9 = new Label("9. Vyradenie PARCELY podľa zadanej GPS súranice");

        l1.setWidth("100%");
        l2.setWidth("100%");
        l3.setWidth("100%");
        l4.setWidth("100%");
        l5.setWidth("100%");
        l6.setWidth("100%");
        l7.setWidth("100%");
        l8.setWidth("100%");
        l9.setWidth("100%");

        VerticalLayout labelsWrapper = new VerticalLayout(lMain, lEmpty, l1, l2, l3, l4, l5, l6, l7, l8, l9);

        Panel descPanel = new Panel(
                "Katastrálny informačný systém",
                        labelsWrapper
                );

        addComponent(descPanel);
    }

}
