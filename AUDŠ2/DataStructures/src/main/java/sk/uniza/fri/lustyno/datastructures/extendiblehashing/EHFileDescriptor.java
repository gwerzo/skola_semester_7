package sk.uniza.fri.lustyno.datastructures.extendiblehashing;

import java.util.List;

public class EHFileDescriptor {

    private Integer fileDepth;
    private Integer adressingBits;
    private Integer numberOfEntitiesInMainBlock;
    private Integer numberOfEntitiesInOverflowBlock;

    private Integer blocksCount;
    private Integer overflowBlocksCount;

    private Integer sizeOfMainFile;
    private Integer sizeOfOverflowFile;

    private List<Integer> freeBlocks;
    private List<Integer> freeOverflowBlocks;

    public EHFileDescriptor(Integer fileDepth,
                            Integer adressingBits, Integer numberOfEntitiesInMainBlock, Integer numberOfEntitiesInOverflowBlock,
                            Integer blocksCount, Integer overflowBlocksCount, Integer sizeOfMainFile, Integer sizeOfOverflowFile, List<Integer> freeBlocks, List<Integer> freeOverflowBlocks) {
        this.fileDepth = fileDepth;
        this.adressingBits = adressingBits;
        this.numberOfEntitiesInMainBlock = numberOfEntitiesInMainBlock;
        this.numberOfEntitiesInOverflowBlock = numberOfEntitiesInOverflowBlock;
        this.blocksCount = blocksCount;
        this.overflowBlocksCount = overflowBlocksCount;
        this.sizeOfMainFile = sizeOfMainFile;
        this.sizeOfOverflowFile = sizeOfOverflowFile;
        this.freeBlocks = freeBlocks;
        this.freeOverflowBlocks = freeOverflowBlocks;
    }

    public Integer getFileDepth() {
        return fileDepth;
    }

    public void setFileDepth(Integer fileDepth) {
        this.fileDepth = fileDepth;
    }

    public Integer getBlocksCount() {
        return blocksCount;
    }

    public void setBlocksCount(Integer blocksCount) {
        this.blocksCount = blocksCount;
    }

    public Integer getOverflowBlocksCount() {
        return overflowBlocksCount;
    }

    public void setOverflowBlocksCount(Integer overflowBlocksCount) {
        this.overflowBlocksCount = overflowBlocksCount;
    }

    public Integer getSizeOfMainFile() {
        return sizeOfMainFile;
    }

    public void setSizeOfMainFile(Integer sizeOfMainFile) {
        this.sizeOfMainFile = sizeOfMainFile;
    }

    public Integer getSizeOfOverflowFile() {
        return sizeOfOverflowFile;
    }

    public void setSizeOfOverflowFile(Integer sizeOfOverflowFile) {
        this.sizeOfOverflowFile = sizeOfOverflowFile;
    }

    public List<Integer> getFreeBlocks() {
        return freeBlocks;
    }

    public void setFreeBlocks(List<Integer> freeBlocks) {
        this.freeBlocks = freeBlocks;
    }

    public List<Integer> getFreeOverflowBlocks() {
        return freeOverflowBlocks;
    }

    public void setFreeOverflowBlocks(List<Integer> freeOverflowBlocks) {
        this.freeOverflowBlocks = freeOverflowBlocks;
    }

    public Integer getAdressingBits() {
        return adressingBits;
    }

    public void setAdressingBits(Integer adressingBits) {
        this.adressingBits = adressingBits;
    }

    public Integer getNumberOfEntitiesInMainBlock() {
        return numberOfEntitiesInMainBlock;
    }

    public void setNumberOfEntitiesInMainBlock(Integer numberOfEntitiesInMainBlock) {
        this.numberOfEntitiesInMainBlock = numberOfEntitiesInMainBlock;
    }

    public Integer getNumberOfEntitiesInOverflowBlock() {
        return numberOfEntitiesInOverflowBlock;
    }

    public void setNumberOfEntitiesInOverflowBlock(Integer numberOfEntitiesInOverflowBlock) {
        this.numberOfEntitiesInOverflowBlock = numberOfEntitiesInOverflowBlock;
    }
}
