﻿using FriBettingDatabaseApp.Bo;

namespace FriBettingDatabaseApp.Repository.Base
{
    interface ITicketBetRepository: IRepository<TicketBet>
    {
    }
}
