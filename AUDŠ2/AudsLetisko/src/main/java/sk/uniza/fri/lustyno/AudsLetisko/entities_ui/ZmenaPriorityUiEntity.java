package sk.uniza.fri.lustyno.AudsLetisko.entities_ui;

import lombok.Data;

@Data
public class ZmenaPriorityUiEntity {

    private String medzinarodnyKodLietadla;

    private Integer novaPriorita;

}
