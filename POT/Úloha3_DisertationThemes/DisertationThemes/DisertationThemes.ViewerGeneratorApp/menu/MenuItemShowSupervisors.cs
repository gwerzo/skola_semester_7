﻿using System;
using System.Collections.Generic;
using System.Text;
using DisertationThemes.Database;

namespace DisertationThemes.ViewerGeneratorApp.menu
{
    class MenuItemShowSupervisors: AbstractMenuItem
    {
        public static string MENU_TEXT = "Zobrazenie všetkých školiteľov";

        public MenuItemShowSupervisors(MenuHolder menuHolder) : base(menuHolder)
        {

        }

        public override string GetMenuText()
        {
            return MENU_TEXT;
        }

        protected override int PerformActionInternally()
        {
            using (DisertationDatabaseContext ddc = new DisertationDatabaseContext())
            {
                foreach (var supervisor in ddc.Supervisors)
                {
                    Console.WriteLine("\t" + supervisor.FullName);
                }
            }
            return 0;
        }
    }
}
