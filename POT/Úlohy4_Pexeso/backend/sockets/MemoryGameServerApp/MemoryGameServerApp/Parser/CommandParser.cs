﻿using System;
using MemoryGameServerApp.Command;

namespace MemoryGameServerApp.Parser
{
    /// <summary>
    /// Parser commandov, ktorý nerobí nič iné,
    /// len podľa predaného commandu od klienta
    /// rozparsuje o aký command sa jedná
    /// a jeho parametre odparsuje a nastaví
    /// už samotnej inštancii commandu
    /// </summary>
    public static class CommandParser
    {
        public static ICommand ParseCommand(string commandString)
        {
            if (commandString.Length == 0 || !commandString.StartsWith("-"))
            {
                return null;
            }

            string[] parsedArgs = commandString.Split(' ');

            switch (parsedArgs[0])
            {
                case AuthenticateCommand.COMMAND:
                    if (parsedArgs.Length != 3)
                    {
                        return null;
                    }
                    return new AuthenticateCommand(parsedArgs[1], parsedArgs[2]);
                case ReAuthenticateWithSessionID.COMMAND:
                    if (parsedArgs.Length != 2)
                    {
                        return null;
                    }
                    return new ReAuthenticateWithSessionID(parsedArgs[1]);
                case LogoutCommand.COMMAND:
                    if (parsedArgs.Length != 2)
                    {
                        return null;
                    }
                    return new LogoutCommand(parsedArgs[1]);
                case CreateInviteToGameCommand.COMMAND:
                    if (parsedArgs.Length != 4)
                    {
                        return null;
                    }
                    return new CreateInviteToGameCommand(int.Parse(parsedArgs[1]), int.Parse(parsedArgs[2]), parsedArgs[3]);
                case CreateRandomInviteToGameCommand.COMMAND:
                    if (parsedArgs.Length != 3)
                    {
                        return null;
                    }
                    return new CreateRandomInviteToGameCommand(int.Parse(parsedArgs[1]), int.Parse(parsedArgs[2]));
                case SendPrivateMessageCommand.COMMAND:
                    if (parsedArgs.Length < 3)
                    {
                        return null;
                    }
                    return new SendPrivateMessageCommand(parsedArgs[1], String.Join(" ", parsedArgs.SubArray(2, parsedArgs.Length - 2)));
                case GetLoggedInUsersCommand.COMMAND:
                    return new GetLoggedInUsersCommand();
                case GetOpenGamesCommand.COMMAND:
                    return new GetOpenGamesCommand();
                case GetMyInvitesCommand.COMMAND:
                    return new GetMyInvitesCommand();
                case JoinGameCommand.COMMAND:
                    if (parsedArgs.Length != 2)
                    {
                        return null;
                    }
                    return new JoinGameCommand(parsedArgs[1]);
                case CardHitCommand.COMMAND:
                    if (parsedArgs.Length != 4)
                    {
                        return null;
                    }
                    return new CardHitCommand(parsedArgs[1], int.Parse(parsedArgs[2]), int.Parse(parsedArgs[3]));
                default:
                    return null;
            }

        }

        /// <summary>
        /// Source: https://www.techiedelight.com/get-subarray-of-array-csharp/ 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="offset"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static T[] SubArray<T>(this T[] array, int offset, int length)
        {
            T[] result = new T[length];
            Array.Copy(array, offset, result, 0, length);
            return result;
        }

    }
}
