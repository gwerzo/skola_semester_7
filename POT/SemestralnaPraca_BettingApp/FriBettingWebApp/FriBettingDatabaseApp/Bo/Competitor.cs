﻿namespace FriBettingDatabaseApp.Bo
{
    /// <summary>
    /// Class reprezentácia súťažiaceho (môže to byť tím alebo jednotlivec)
    /// </summary>
    public class Competitor: IdentifiableEntity
    {
        public int CompetitorId { get; set; }
        public bool IsTeam { get; set; }
        public string Name { get; set; }
        public Sport SportCompetitingAt { get; set; }

        public override int GetId()
        {
            return CompetitorId;
        }
    }
}
