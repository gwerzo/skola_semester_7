package sk.uniza.fri.lustyno.AudsLetisko.persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.uniza.fri.lustyno.AudsLetisko.BusinessException;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.OdletovaDrahaEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.RunwayManagementService;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

@Service
public class OdletoveDrahyFileManager {

    @Autowired
    private RunwayManagementService runwayManagementService;

    private final String VSETKY_ODLETOVE_DRAHY_FILE = "fileResources/vsetky_odletove_drahy.csv";

    private final String VOLNE_ODLETOVE_DRAHY_FILE  = "fileResources/volne_drahy.csv";

    public void vycistiSubory() {

        try {
            PrintWriter writer = new PrintWriter(VSETKY_ODLETOVE_DRAHY_FILE);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            PrintWriter writer = new PrintWriter(VOLNE_ODLETOVE_DRAHY_FILE);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void ulozVsetkyOdletoveDrahy() {
        List<OdletovaDrahaEntity> vsetkyOdletoveDrahyVSysteme =
                runwayManagementService.getVsetkyOdletoveDrahyVSystemePreUlozenieDoSuboru();

        if (vsetkyOdletoveDrahyVSysteme == null || vsetkyOdletoveDrahyVSysteme.size() == 0) {
            return;
        }

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(VSETKY_ODLETOVE_DRAHY_FILE));

            for (OdletovaDrahaEntity odletovaDrahaEntity : vsetkyOdletoveDrahyVSysteme) {
                StringBuilder sb = new StringBuilder();

                sb.append(odletovaDrahaEntity.getId() + ",");
                sb.append(odletovaDrahaEntity.getDlzkaDrahy());

                bw.write(sb.toString());
                bw.newLine();
            }

            bw.flush();
        } catch (IOException ioEx) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na zápis všetkých odletových dráh");
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void nacitajVsetkyOdletoveDrahyZoSuboru() {
        List<OdletovaDrahaEntity> vsetkyOdletoveDrahyZoSuboru = new LinkedList<>();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(VSETKY_ODLETOVE_DRAHY_FILE));

            String line = null;
            while ((line = br.readLine()) != null) {

                String[] splitted = line.split(",", -1);

                Integer idDrahy = Integer.parseInt(splitted[0]);
                Integer dlzkaDrahy = Integer.parseInt(splitted[1]);

                OdletovaDrahaEntity entity = new OdletovaDrahaEntity.Builder()
                        .sId(idDrahy)
                        .sDlzkouDrahy(dlzkaDrahy)
                        .build();
                vsetkyOdletoveDrahyZoSuboru.add(entity);
            }
        } catch (FileNotFoundException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o všetkých odletových dráhach v systéme");
        } catch (IOException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o všetkých odletových dráhach v systéme");
        }
        this.runwayManagementService.nastavVsetkyDrahyZoSuboru(vsetkyOdletoveDrahyZoSuboru);
    }

    public void ulozVsetkyVolneDrahyDoSuboru() {

        List<Integer> volneOdletoveDrahyDoSuboru = this.runwayManagementService.getVsetkyVolneDrahyPreUlozenieDoSuboru();

        if (volneOdletoveDrahyDoSuboru == null || volneOdletoveDrahyDoSuboru.size() == 0) {
            return;
        }

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(VOLNE_ODLETOVE_DRAHY_FILE));

            for (Integer idOdletovejDrahy : volneOdletoveDrahyDoSuboru) {
                StringBuilder sb = new StringBuilder();

                sb.append(idOdletovejDrahy.toString());

                bw.write(sb.toString());
                bw.newLine();
            }
            bw.flush();
        } catch (IOException ioEx) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na zápis voľných odletových dráh");
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void nacitajVsetkyVolneDrahyZoSuboru() {
        List<Integer> volneOdletoveDrahyZoSuboru = new LinkedList<>();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(VOLNE_ODLETOVE_DRAHY_FILE));

            String line = null;
            while ((line = br.readLine()) != null) {

                Integer idDrahy = Integer.parseInt(line);

                volneOdletoveDrahyZoSuboru.add(idDrahy);
            }
        } catch (FileNotFoundException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o voľných odletových dráhach v systéme");
        } catch (IOException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o voľných odletových dráhach v systéme");
        }
        this.runwayManagementService.nastavVolneOdletoveDrahyZoSuboru(volneOdletoveDrahyZoSuboru);
    }

}
