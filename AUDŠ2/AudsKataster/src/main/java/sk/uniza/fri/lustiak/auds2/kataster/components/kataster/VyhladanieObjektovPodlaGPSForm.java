package sk.uniza.fri.lustiak.auds2.kataster.components.kataster;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.CoordinatesUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.ParcelUiTableEntity;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.RealEstateUiTableEntity;
import sk.uniza.fri.lustiak.auds2.kataster.service.ParcelService;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;


public class VyhladanieObjektovPodlaGPSForm extends BusinessComponent {

    private CoordinatesUi entity1;
    private CoordinatesUi entity2;

    private Binder<CoordinatesUi> binder1;
    private Binder<CoordinatesUi> binder2;

    private RealEstateService realEstateService;
    private ParcelService parcelService;

    private Grid<RealEstateUiTableEntity> realEstateUiTableEntityGrid;
    private Grid<ParcelUiTableEntity> parcelUiTableEntityGrid;


    public VyhladanieObjektovPodlaGPSForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_3.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
        this.parcelService = UIServices.getParcelService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.realEstateUiTableEntityGrid = new Grid<>();
        this.realEstateUiTableEntityGrid.setWidth("100%");
        this.parcelUiTableEntityGrid = new Grid<>();
        this.parcelUiTableEntityGrid.setWidth("100%");

        this.realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getSupisneCislo).setCaption("Súpisné číslo");
        this.realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getPopis).setCaption("Popis");
        this.realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getLatitude).setCaption("Zem.výška");
        this.realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getLongtitude).setCaption("Zem.šírka");
        this.realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getLatitudeOrientation).setCaption("Orientácia výšky");
        this.realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getLongitudeOrientation).setCaption("Orientácia šírky");

        this.parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getCisloParcely).setCaption("Číslo parcely");
        this.parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getPopisParcely).setCaption("Popis parcely");
        this.parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getLatitude).setCaption("Zem. výška");
        this.parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getLongtitude).setCaption("Zem. šírka");
        this.parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getLatitudeOrientation).setCaption("Orientácia výšky");
        this.parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getLongitudeOrientation).setCaption("Orientácia šírky");

        this.entity1 = new CoordinatesUi();
        this.binder1 = new Binder<>(CoordinatesUi.class);

        this.entity2 = new CoordinatesUi();
        this.binder2 = new Binder<>(CoordinatesUi.class);

        VerticalLayout layout = new VerticalLayout();

        HorizontalLayout latLayout = new HorizontalLayout();
        TextField latitudeTF = new TextField("Zemepisná výška");
        RadioButtonGroup<String> latitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej výšky");
        latitudeOrientation.setItems("SEVER", "JUH");
        latitudeOrientation.setSelectedItem("SEVER");
        latLayout.addComponents(latitudeTF, latitudeOrientation);

        HorizontalLayout longLayout = new HorizontalLayout();
        TextField longitudeTF = new TextField("Zemepisná šírka");
        RadioButtonGroup<String> longitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej šírky");
        longitudeOrientation.setItems("VÝCHOD", "ZÁPAD");
        longitudeOrientation.setSelectedItem("VÝCHOD");
        longLayout.addComponents(longitudeTF, longitudeOrientation);
        Panel firstCoords = new Panel("Súradnice OD", new VerticalLayout(latLayout, longLayout));

        HorizontalLayout latLayout2 = new HorizontalLayout();
        TextField latitudeTF2 = new TextField("Zemepisná výška");
        RadioButtonGroup<String> latitudeOrientation2 = new RadioButtonGroup<>("Orientácia zemepisnej výšky");
        latitudeOrientation2.setItems("SEVER", "JUH");
        latitudeOrientation2.setSelectedItem("SEVER");
        latLayout2.addComponents(latitudeTF2, latitudeOrientation2);

        HorizontalLayout longLayout2 = new HorizontalLayout();
        TextField longitudeTF2 = new TextField("Zemepisná šírka");
        RadioButtonGroup<String> longitudeOrientation2 = new RadioButtonGroup<>("Orientácia zemepisnej šírky");
        longitudeOrientation2.setItems("VÝCHOD", "ZÁPAD");
        longitudeOrientation2.setSelectedItem("VÝCHOD");
        longLayout2.addComponents(longitudeTF2, longitudeOrientation2);
        Panel secondCoords = new Panel("Súradnice DO", new VerticalLayout(latLayout2, longLayout2));

        Button findAllObjectsButton = new Button("Vyhľadať objekty");
        findAllObjectsButton.addClickListener(e -> {
            try {
                this.binder1.writeBean(this.entity1);
                this.binder2.writeBean(this.entity2);
                if (longitudeOrientation.getSelectedItem().get().equals("VÝCHOD")) {
                    this.entity1.setLongitudeOrientation(true);
                } else {
                    this.entity1.setLongitudeOrientation(false);
                }
                if (latitudeOrientation.getSelectedItem().get().equals("SEVER")) {
                    this.entity1.setLatitudeOrientation(true);
                } else {
                    this.entity1.setLatitudeOrientation(false);
                }

                if (longitudeOrientation2.getSelectedItem().get().equals("VÝCHOD")) {
                    this.entity2.setLongitudeOrientation(true);
                } else {
                    this.entity2.setLongitudeOrientation(false);
                }
                if (latitudeOrientation2.getSelectedItem().get().equals("SEVER")) {
                    this.entity2.setLatitudeOrientation(true);
                } else {
                    this.entity2.setLatitudeOrientation(false);
                }
                najdiObjekty(this.entity1, this.entity2);

            } catch (ValidationException valEx) {
                this.entity1.flush();
                this.entity2.flush();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        this.binder1.forField(latitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLatitude, CoordinatesUi::setLatitude);

        this.binder1.forField(longitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLongitude, CoordinatesUi::setLongitude);

        this.binder2.forField(latitudeTF2)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLatitude, CoordinatesUi::setLatitude);

        this.binder2.forField(longitudeTF2)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLongitude, CoordinatesUi::setLongitude);

        layout.addComponent(new HorizontalLayout(firstCoords, secondCoords, findAllObjectsButton));

        TabSheet entitiesTabs = new TabSheet();
        entitiesTabs.addTab(new Panel(this.realEstateUiTableEntityGrid), "Nehnuteľnosti");
        entitiesTabs.addTab(new Panel(this.parcelUiTableEntityGrid), "Parcely");

        layout.addComponent(entitiesTabs);

        this.parentComponent.showComponent(new Panel("Vyhľadanie OBJEKTOV podľa GPS súradníc", layout));
    }

    private void najdiObjekty(CoordinatesUi entity1, CoordinatesUi entity2) {
        this.realEstateUiTableEntityGrid.setItems(this.realEstateService.getRealEstatesBetweenCoordinates(entity1, entity2));
        this.parcelUiTableEntityGrid.setItems(this.parcelService.getParcelsBetweenCoordinates(entity1, entity2));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_3.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
