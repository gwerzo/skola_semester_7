﻿using System;
using System.Collections.Generic;
using MemoryGameDaoApp.Bo;
using MemoryGameServerApp.Engine;
using MemoryGameServerApp.Server;
using SuperWebSocket;

namespace MemoryGameServerApp.Command
{
    /// <summary>
    /// Command reprezentujúci vytvorenie novej hry/výzvy s náhodným súperom
    /// ktorý naspäť pošle UUID novovytvorenej hry/výzvy
    /// </summary>
    public class CreateRandomInviteToGameCommand:ICommand
    {
        public const string COMMAND                  = "-rndgame";
        private const string GAME_SUCC_CREATED       = "-rndgamesucc";
        private const string GAME_UNSUCC_CREATED     = "-rndgameunsucc";

        public int GameType { get; set; }
        public int GameStyle { get; set; }

        public CreateRandomInviteToGameCommand(int gameStyle, int gameType)
        {
            this.GameStyle = gameStyle;
            this.GameType = gameType;
        }

        public void executeCommand(string usernameOfCommandCaller, 
            MemoryGameServer memoryGameServer, WebSocketSession callerSocketSession)
        {

            if (!memoryGameServer.IsAuthenticated(callerSocketSession))
            {
                callerSocketSession.Send(GAME_UNSUCC_CREATED + " Nie ste prihlásený");
                return;
            }

            GameType gameType = memoryGameServer.GetGameTypeById(this.GameType);
            if (gameType == null)
            {
                callerSocketSession.Send(GAME_UNSUCC_CREATED);
                return;
            }

            List<int> gameCardIds = memoryGameServer.GetCardIdsForGameType(this.GameStyle);
            if (gameCardIds == null || gameCardIds.Count == 0)
            {
                callerSocketSession.Send(GAME_UNSUCC_CREATED);
                return;
            }

            // Nová hra s prázdnym hráčom 2
            MemoryGameState memoryGameState = new MemoryGameState(
                usernameOfCommandCaller, null,
                gameType.XLength, gameType.YLength,
                gameCardIds, new Random().Next(), GameStyle, GameType);

            string gameCreatedUuid = memoryGameServer.CreateNewGame(memoryGameState);
            if (gameCreatedUuid != null)
            {
                callerSocketSession.Send(GAME_SUCC_CREATED + " " + gameCreatedUuid + " " + 
                                         memoryGameState.WidthOfGame + " " + memoryGameState.HeightOfGame);
            }
            else
            {
                callerSocketSession.Send(GAME_UNSUCC_CREATED);
            }
        }
    }
}
