package sk.uniza.fri.lustyno.fripd.entities_ui.vehicle;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;


@Data
@Accessors(chain = true)
public class PridanieVozidlaEntityUi {

    private String VIN;
    private String EVC;
    private Boolean vPatrani;
    private LocalDate datumPlatnostiSTK;
    private LocalDate datumPlatnostiEK;
    private Integer pocetNaprav;
    private Integer prevadzkovaHmostnost;

}
