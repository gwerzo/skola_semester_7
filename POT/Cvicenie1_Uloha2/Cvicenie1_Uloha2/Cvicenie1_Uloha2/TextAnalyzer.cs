﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cvicenie1_Uloha2
{
    /// <summary>
    /// Textová analýza slovenského textu,
    /// do samohlásiek zahŕňa aj dvojhlásky "ia, ie,.."
    /// ako je to popísané v článku https://referaty.aktuality.sk/hlasky-spoluhlasky-samohlasky/referat-22167
    /// </summary>
    class TextAnalyzer
    {

        // Vowels lists
        static readonly string[] VOWELS                     = { "a", "e", "i", "o", "u", "y", "ä", "á", "é", "í", "ó", "ú", "ô", "y", "ý",
                                                                "A", "E", "I", "O", "U", "Y", "Á", "É", "Í", "Ó", "Ú", "Y", "Ý"};
        static readonly string[] MAY_PAIRED_VOWELS          = { "i" };
        static readonly string[] PAIRED_VOWELS              = { "ia", "ie", "iu" };


        // Consonants lists
        static readonly string[] CONSONANTS                 = { "h", "k", "g", "d", "t", "n", "l", "ď", "ť", "ň", "ľ", "č", "ž", "š", "c", "j", "b", "m", "p", "r", "s", "v", "z", "f",
                                                                "H", "K", "G", "D", "T", "N", "L", "D", "Ť", "Ň", "Ľ", "Č", "Ž", "Š", "C", "J", "B", "M", "P", "R", "S", "V", "Z", "F"};
        static readonly string[] MAY_PAIRED_CONSONANTS      = { "c", "d", "ď", "C", "D" };
        static readonly string[] PAIRED_CONSONANTS          = { "ch", "dz", "dž", "Ch", "Dz", "Dž"};
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.Error.WriteLine("Parametrom programu musí byť text, ktorý sa má zanalyzovať");
            }
            else {

                bool doWf = ContainsCommand(args, "-wf");
                bool doCf = ContainsCommand(args, "-cf");

                TextAnalaryzeResult result = analyzeText(args[0], doWf, doCf);


                result.PrintResult();
            }
        }

        static TextAnalaryzeResult analyzeText(string text, Boolean wf, Boolean cf) {
            TextAnalaryzeResult tar = new TextAnalaryzeResult(wf, cf);

            string lastWord = "";

            for (int i = 0; i < text.Length; i++) {
                bool processingTwoChars = false;

                if (text[i].ToString() == ".") {
                    tar.NumberOfSentences++;
                    if (i == (text.Length - 1)) {
                        tar.NumberOfWords++;
                        tar.IncrementFrequencyOfWord(RemovePunctuationAndLowerify(lastWord));
                    }
                    continue;
                }
                if (text[i].ToString() == " ") {
                    tar.IncrementFrequencyOfWord(RemovePunctuationAndLowerify(lastWord));
                    tar.NumberOfWords++;
                    lastWord = "";
                    continue;
                }

                string characterBeingProcessed = text[i].ToString();

                if (IsMayCatchyVowel(characterBeingProcessed)) {
                    if (i < text.Length - 1) {
                        if (IsCatchyVowel(characterBeingProcessed + text[i + 1]))
                        {
                            processingTwoChars = true;
                            characterBeingProcessed += text[i + 1];
                        }
                    }
                }

                if (IsMayCatchyConsonant(characterBeingProcessed)) {
                    if (i < text.Length - 1)
                    {
                        if (IsCatchyConsonant(characterBeingProcessed + text[i + 1])) {
                            processingTwoChars = true;
                            characterBeingProcessed += text[i + 1];
                        }
                    }
                }

                if (IsVowel(characterBeingProcessed) || IsCatchyVowel(characterBeingProcessed)) {
                    tar.NumberOfVowels++;
                }

                if (IsConsonant(characterBeingProcessed) || IsCatchyConsonant(characterBeingProcessed)) {
                    tar.NumberOfConsonants++;
                }


                tar.IncrementFrequencyOfCharacter(RemovePunctuationAndLowerify(characterBeingProcessed));
                lastWord += characterBeingProcessed;

                // Boli spracované dve písmená z dôvodu párových ("ia", "ie",..) ktoré sa rátajú ako jedno
                if (processingTwoChars) {
                    i++;
                }

            }
            tar.NumberOfCharacters = text.Length;
            tar.NumberOfSpaces = text.Length - text.Replace(" ", "").Length;
            return tar;
        }

        static string RemovePunctuationAndLowerify(string s) {
            var sb = new StringBuilder();

            foreach (char c in s)
            {
                if (!char.IsPunctuation(c))
                    sb.Append(c);
            }
            s = sb.ToString();
            return s.ToLower();
        }

        static bool IsVowel(string character) {
            foreach (string s in TextAnalyzer.VOWELS) {
                if (s == character) return true;
            }
            return false;
        }

        static bool IsConsonant(string character)
        {
            foreach (string s in TextAnalyzer.CONSONANTS)
            {
                if (s == character) return true;
            }
            return false;
        }

        static bool IsCatchyVowel(string character) {
            foreach (string s in TextAnalyzer.PAIRED_VOWELS) {
                if (s == character) return true;
            }
            return false;
        }

        static bool IsMayCatchyVowel(string character)
        {
            foreach (string s in TextAnalyzer.MAY_PAIRED_VOWELS)
            {
                if (s == character) return true;
            }
            return false;
        }

        static bool IsCatchyConsonant(string character)
        {
            foreach (string s in TextAnalyzer.PAIRED_CONSONANTS)
            {
                if (s == character) return true;
            }
            return false;
        }

        static bool IsMayCatchyConsonant(string character)
        {
            foreach (string s in TextAnalyzer.MAY_PAIRED_CONSONANTS)
            {
                if (s == character) return true;
            }
            return false;
        }

        static bool ContainsCommand(string[] args, string command) {

            // Preskočenie prvého arg, ktorým je text
            bool skip = true;
            foreach (string s in args) {

                if (skip) {
                    skip = !skip;
                    continue;
                }

                if (s == command) return true;                
            }

            return false;
        }
    }
}
