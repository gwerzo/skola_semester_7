﻿using System.Web.Mvc;

namespace FriBettingWebApplication.Controllers
{
    public class TicketsController : Controller
    {

        public ActionResult Index()
        {
            return View("MyTicket");
        }

        public ActionResult MyTicket()
        {
            return View();
        }

        public ActionResult EvaluatedTickets()
        {
            return View();
        }

    }
}