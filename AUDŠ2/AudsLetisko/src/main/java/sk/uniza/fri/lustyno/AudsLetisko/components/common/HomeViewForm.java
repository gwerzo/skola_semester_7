package sk.uniza.fri.lustyno.AudsLetisko.components.common;

import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class HomeViewForm extends VerticalLayout {

    public HomeViewForm() {
        initComponents();
    }

    private void initComponents() {

        Label lMain = new Label("Funkcionalita poskytovaná informačným systémom:");

        Label lEmpty = new Label();

        Label l1 = new Label("1. Vloženie informácií o prílete lietadla do systému");
        Label l2 = new Label(
                "2. Vloženie informácie o požiadavke na pridelenie odletovej dráhy" +
                        " (lietadlo sa zaradí medzi čakajúce lietadlá na odletovú dráhu) pre lietadlo" +
                        " (lietadlo je identifikované svojim kódom)");
        Label l3 = new Label(
                "3. Vyhľadanie lietadla čakajúceho na pridelenie odletovej " +
                        "dráhy podľa medzinárodného kódu");
        Label l3_1 = new Label(
                "3.1. Vyhľadanie lietadla podľa výrobcu a medzinárodného kódu");
        Label l4 = new Label(
                "4. Vyhľadanie lietadla čakajúceho na pridelenie odletovej dráhy " +
                        "podľa medzinárodného kódu na odletovej dráhe (užívateľ zadá konkrétnu dráhu)");
        Label l5 = new Label(
                "5. Vloženie informácií o odlete lietadla (lietadlo je identifikované svojim kódom) " +
                        "a uvoľnení dráhy - ak nejaké čakajúce lietadlo môže dráhu využiť, je mu ihneď pridelená" +
                        " a systém zobrazí informácie o takomto lete");
        Label l6 = new Label(
                "6. Výpis všetkých lietadiel čakajúcich na pridelenie odletovej dráhy usporiadaných podľa ich kódov");
        Label l7 = new Label(
                "7. Výpis všetkých lietadiel čakjúcich na pridelenie konkrétnej odletovej dráhy usporiadaných " +
                        "podľa ich kódov");
        Label l8 = new Label("8. Zmena priority lietadla čakajúceho na odlet (lietadlo je identifikované svojím kódom)");
        Label l9 = new Label("9. Výpis vśetkých odletových dráh, pričom pre každú dráhu sa vypíšu všetky " +
                        "realizované odlety so všetkými dostupnými informáciami usporiadané podľa poradia v akom sa realizovali");
        Label l10 = new Label(
                "10. Vyradenie lietadla (lietadlo je identifikované svojim kódom) zo zoznamu čakajúcich lietadiel" +
                        " (zrušenie požiadavky na pridelenie odletovej dráhy)");

        l1.setWidth("100%");
        l2.setWidth("100%");
        l3.setWidth("100%");
        l3_1.setWidth("100%");
        l4.setWidth("100%");
        l5.setWidth("100%");
        l6.setWidth("100%");
        l7.setWidth("100%");
        l8.setWidth("100%");
        l9.setWidth("100%");
        l10.setWidth("100%");
        lMain.setWidth("100%");
        lEmpty.setWidth("100%");

        VerticalLayout labelsWrapper = new VerticalLayout(
                                lMain,
                                lEmpty,
                                l1,
                                l2,
                                l3,
                                l3_1,
                                l4,
                                l5,
                                l6,
                                l7,
                                l8,
                                l9,
                                l10);

        Panel descPanel = new Panel(
                "Letiskový informačný systém",
                        labelsWrapper
                );

        addComponent(descPanel);
    }

}
