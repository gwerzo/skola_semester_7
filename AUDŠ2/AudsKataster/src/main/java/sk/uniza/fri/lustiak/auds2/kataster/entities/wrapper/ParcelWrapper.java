package sk.uniza.fri.lustiak.auds2.kataster.entities.wrapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import sk.uniza.fri.lustiak.auds2.kataster.entities.bo.Parcel;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo.Coordinates2DKey;

@Data
@AllArgsConstructor
public class ParcelWrapper {

    private Coordinates2DKey coordinates2DKey;
    private Parcel parcel;

}
