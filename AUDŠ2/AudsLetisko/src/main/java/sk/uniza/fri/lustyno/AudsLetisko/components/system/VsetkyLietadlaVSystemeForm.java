package sk.uniza.fri.lustyno.AudsLetisko.components.system;

import com.vaadin.ui.Grid;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.LietadloTableUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.AircraftManagementService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;

import java.util.List;

public class VsetkyLietadlaVSystemeForm extends BusinessComponent {

    private AircraftManagementService aircraftManagementService;

    public VsetkyLietadlaVSystemeForm(RepaintableComponent component) {
        super(component, MENU_TITLE.SYSTEM_MENU_6.getTitle());
        this.aircraftManagementService = UIServices.getAircraftManagementService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.parenComponent.removeAllComponents();

        VerticalLayout vl = new VerticalLayout();

        Grid<LietadloTableUiEntity> lietadlaVSystemeGrid = new Grid<>();
        lietadlaVSystemeGrid.addColumn(LietadloTableUiEntity::getMedzinarodnyKod).setCaption("Medzinárodný kód");
        lietadlaVSystemeGrid.addColumn(LietadloTableUiEntity::getVyrobca).setCaption("Výrobca");

        lietadlaVSystemeGrid.setSizeFull();

        HandledOperationExecutor.submit(() -> {

            List<LietadloTableUiEntity> entityList = this.aircraftManagementService.getAllLietadlaVSystemeTableEntities();
            if (entityList != null && entityList.size() != 0) {
                lietadlaVSystemeGrid.setItems(entityList);
            }

            return OperationMarker.OPERATION_SUCCESFUL;
        });

        vl.addComponent(lietadlaVSystemeGrid);

        this.parenComponent.showComponent(new Panel("Všetky lietadlá v systéme", vl));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_6.getTitle();
    }
}
