﻿using FriBettingDatabaseApp.Bo;

namespace FriBettingDatabaseApp.Repository.Base
{ 
    public interface IMatchPartRepository: IRepository<MatchPart>
    {
    }
}
