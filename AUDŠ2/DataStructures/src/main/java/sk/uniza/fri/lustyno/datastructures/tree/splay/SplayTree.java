package sk.uniza.fri.lustyno.datastructures.tree.splay;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;
import sk.uniza.fri.lustyno.datastructures.tree.NodeResult;
import sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTree;
import sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTreeNode;

public class SplayTree<K extends HasComparableValue, D> extends BinarySearchTree<K, D> {

    private Boolean splayingAllowed = true;

    public SplayTree() {
        super();
    }

    /**
     * Nájde node v strome,
     * podľa vyhľadávaného kľúča z parametra.
     * <p>
     * Podľa toho, či daný node našiel, alebo nie,
     * vykoná operáciu splay buď nad nájdeným nodom,
     * alebo posledne pristúpeným nodom
     * <p>
     * @param   key  comparable kľúč pre nájdenie nodu
     * @return  {@link BinarySearchTree#find(HasComparableValue)}
     */
    public NodeResult<BinarySearchTreeNode<K, D>> find(K key) {
        NodeResult<BinarySearchTreeNode<K, D>> result = super.find(key);
        if (this.splayingAllowed) {
            if (result.wasFound()) {
                this.splay(result.get());
            } else {
                this.splay(result.getLastAccessed());
            }
        }
        return result;
    }

    /**
     * Vymaže node zo stromu podľa predaného kľúča.
     * <p>
     * Podľa toho, či daný node našiel, alebo nie,
     * vykoná operáciu splay buď nad nájdeným nodom,
     * alebo posledne pristúpeným nodom
     * <p>
     * Ak sa node našiel, bol vymazaný,
     * vykoná sa splay nad otcom vymazaného nodu
     *    - môže pristupovať k otcovi vymazaného nodu,
     *      lebo operácia delete nijak nemodifikuje
     *      vymazávaný node, t.j. ostane mu referencia
     *      na otca
     *
     * @param    key  kľúč nodu na vymazanie
     * @return   {@link BinarySearchTree#delete(HasComparableValue)}
     */
    public NodeResult<BinarySearchTreeNode<K, D>> delete(K key) {
        this.splayingAllowed = false;
        NodeResult<BinarySearchTreeNode<K, D>> result = super.delete(key);
        this.splayingAllowed = true;
        if (result.wasFound()) {
            this.splay(result.get().getParent());
        } else {
            this.splay(result.getLastAccessed());
        }
        return result;
    }

    /**
     * Jednoduchý insert key/data páru
     *
     * @param key - kľúč
     * @param data - dáta
     */
    public void insert(K key, D data) {
        this.insert(new BinarySearchTreeNode<>(key, data));
    }

    /**
     * Rovnaká operácia vloženia do stromu,
     * ako je to pri BST, tu sa ale po
     * vložení zavolá operácia splay nad
     * vloženým nodom
     *
     * @param node node na vloženie do stromu
     */
    public void insert(BinarySearchTreeNode<K, D> node) {
        super.insert(node);
        splay(node);
    }

    /**
     * Inicializačná metóda, ktorá spustí
     * rotácie nodu a rozhodne, či je nutné
     * vykonávať rotácii viac (node nie je synom roota)
     * alebo len jednu (node je synom roota)ň
     * alebo žiadnu ak node je samotným rootom
     *
     * @param nodeToSplay - node, nad ktorým sa má vykonať operácia splay
     */
    private void splay(BinarySearchTreeNode<K, D> nodeToSplay) {
        if (nodeToSplay == null || nodeToSplay.isRoot()) {
            return;
        } else {
            BinarySearchTreeNode<K, D> parentOfNode = nodeToSplay.getParent();
            if (parentOfNode.isRoot()) {
                splayNodeWhenParentIsRoot(nodeToSplay);
            } else {
                splayNode(nodeToSplay);
            }
        }
    }

    /**
     * Loop wrapper metóda pre vykonávanie
     * rotácii nodu podľa toho, či je pravým,
     * alebo ľavým synom, resp. ak je synom roota,
     * až pokým daný node nie je vyrotovaný až do koreňa stromu
     *
     * @param nodeToSplay - node, ktorý treba vyrotovať až do koreňa stromu
     */
    private void splayNode(BinarySearchTreeNode<K, D> nodeToSplay) {

        if (nodeToSplay != null) {
            BinarySearchTreeNode<K, D> actualNode = nodeToSplay;

            while (true) {
                if (actualNode.isRoot()) {
                    return;
                }
                if (actualNode.getParent().isRoot()) {
                    splayNodeWhenParentIsRoot(actualNode);
                } else {
                    if (actualNode.isRightChildOfParent()) {
                        simpleLeftRotationOfNode(actualNode);
                    } else {
                        simpleRightRotationOfNode(actualNode);
                    }
                }
            }
        }
    }

    /**
     * Decision metóda pre rotovanie nodu,
     * ktorého rodič nemá predka,
     * t.j jeho rodič je koreňom stromu
     *
     * @param nodeToSplay - node, ktorého rodič je koreňom stromu
     */
    private void splayNodeWhenParentIsRoot(BinarySearchTreeNode<K, D> nodeToSplay) {
        if (!nodeToSplay.isRightChildOfParent()) {
            simpleRightRotationOfNodeOverRoot(nodeToSplay);
        } else {
            simpleLeftRotationOfNodeOverRoot(nodeToSplay);
        }
    }

}
