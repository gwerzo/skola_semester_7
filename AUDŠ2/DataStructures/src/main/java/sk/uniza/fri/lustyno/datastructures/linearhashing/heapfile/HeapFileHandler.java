package sk.uniza.fri.lustyno.datastructures.linearhashing.heapfile;

import sk.uniza.fri.lustyno.datastructures.exception.DataStructureInitializationException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureOperationFailedException;
import sk.uniza.fri.lustyno.datastructures.linearhashing.config.HeapFileConfig;

import java.io.*;
import java.nio.ByteBuffer;

public class HeapFileHandler {

    private RandomAccessFile raf;

    private String heapFileName;

    public HeapFileHandler(String heapFileName) {
        this.heapFileName = heapFileName;

        try {
            this.raf = new RandomAccessFile(this.heapFileName + ".hdbll", "rw");
        } catch (FileNotFoundException e) {
            throw new DataStructureInitializationException(this.getClass().getName(), e.getMessage());
        }
    }

    public HeapFileHandler(HeapFileConfig heapFileConfig) {
        this.heapFileName = heapFileConfig.heapFileName;

        try {
            this.raf = new RandomAccessFile(heapFileName + ".hdbll", "rw");
        } catch (FileNotFoundException e) {
            throw new DataStructureInitializationException(this.getClass().getName(), e.getMessage());
        }
    }

    public HeapFileConfig fillHeapFileConfig(HeapFileConfig hfc) {
        hfc.heapFileName = this.heapFileName;
        return hfc;
    }

    public void writeBytes(byte[] bytes, Integer offset) {
        try {
            this.raf.seek(offset);
            this.raf.write(bytes, 0, bytes.length);
        } catch (IOException e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "writeBytes", e.getMessage());
        }
    }

    public byte[] readBytes(Integer offset, Integer length) {
        try {
            this.raf.seek(offset);
            byte[] bytes = ByteBuffer.allocate(length).array();
            this.raf.read(bytes, 0, length);

            return bytes;
        } catch (IOException e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "readBytes", e.getMessage());
        }
    }

    public void close() {
        try {
            this.raf.close();
        } catch (IOException e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "close", e.getMessage());
        }
    }

}
