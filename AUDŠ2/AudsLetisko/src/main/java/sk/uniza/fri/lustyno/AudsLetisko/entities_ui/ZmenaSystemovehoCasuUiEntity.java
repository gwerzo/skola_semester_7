package sk.uniza.fri.lustyno.AudsLetisko.entities_ui;

import lombok.Data;

@Data
public class ZmenaSystemovehoCasuUiEntity {

    private Integer rok;

    private Integer mesiac;

    private Integer den;

    private Integer hodina;

    private Integer minuta;

}
