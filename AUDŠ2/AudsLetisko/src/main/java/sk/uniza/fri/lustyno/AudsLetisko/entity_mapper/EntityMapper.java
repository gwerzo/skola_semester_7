package sk.uniza.fri.lustyno.AudsLetisko.entity_mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.HistoriaOdletuEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.LietadloEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.OdletovaDrahaEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.*;
import sk.uniza.fri.lustyno.AudsLetisko.service.DateTimeService;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class EntityMapper {

    @Autowired
    private DateTimeService dateTimeService;

    public LietadloEntity fromPriletLietadlaUiEntity(PriletLietadlaUiEntity priletLietadlaUiEntity) {

        LietadloEntity lietadloEntity = new LietadloEntity();

        lietadloEntity.setNazovVyrobcu(ofNullable(priletLietadlaUiEntity).get().getNazovVyrobcu());
        lietadloEntity.setPriorita(ofNullable(priletLietadlaUiEntity).get().getPriorita());
        lietadloEntity.setMedzinarodnyKod(ofNullable(priletLietadlaUiEntity).get().getMedzinarodnyKod());
        lietadloEntity.setMinimalnaDlzkaOdletovejDrahy(ofNullable(priletLietadlaUiEntity).get().getMinimalnaDlzkaOdletovejDrahy());

        lietadloEntity.setCasDatumPriletu(this.dateTimeService.getActualDateTime());

        return lietadloEntity;
    }

    public LietadloEntity fromPoziadavkaNaPridelenieUiEntity(PoziadavkaNaOdletUiEntity poziadavkaNaOdletUiEntity) {

        LietadloEntity lietadloEntity = new LietadloEntity();
        lietadloEntity.setMedzinarodnyKod(ofNullable(poziadavkaNaOdletUiEntity).get().getMedzinarodnyKodLietadla());

        return lietadloEntity;

    }

    public List<OdletovaDrahaTableUiEntity> fromOdletoveDraheToTableUiEntities(List<OdletovaDrahaEntity> entities) {
        return entities.stream().map(e -> {
            return getOdletovaDrahaTableUiEntity(e);
        }).collect(Collectors.toList());
    }

    public List<LietadloTableUiEntity> fromLietadaEntitiesToTableUiEntity(List<LietadloEntity> entities) {
        return entities.stream().map(e -> {
            return getLietadloTableUiEntity(e);
        }).collect(Collectors.toList());
    }

    public LietadloTableUiEntity fromLietadloEntityToTableUiEntity(LietadloEntity entity) {
        return getLietadloTableUiEntity(entity);
    }

    private LietadloTableUiEntity getLietadloTableUiEntity(LietadloEntity entity) {
        LietadloTableUiEntity uiEntity = new LietadloTableUiEntity();
        uiEntity.setVyrobca(entity.getNazovVyrobcu());
        uiEntity.setMedzinarodnyKod(entity.getMedzinarodnyKod());
        uiEntity.setCasPriletu(entity.getCasDatumPriletu());
        uiEntity.setMinDlzkaDrahy(entity.getMinimalnaDlzkaOdletovejDrahy());
        uiEntity.setCasPoziadavkyOPridelenie(entity.getCasDatumPoziadaniaOPridelenie());
        uiEntity.setPriorita(entity.getPriorita());

        if (entity.getAktualnePridelenaDraha() != null) {
            uiEntity.setIdDrahy(entity.getAktualnePridelenaDraha().getId().toString());
        }

        return uiEntity;
    }


    private OdletovaDrahaTableUiEntity getOdletovaDrahaTableUiEntity(OdletovaDrahaEntity entity) {
        OdletovaDrahaTableUiEntity uiEntity = new OdletovaDrahaTableUiEntity();
        uiEntity.setId(entity.getId());
        uiEntity.setDlzka(entity.getDlzkaDrahy());
        uiEntity.setJeVolna(entity.getAktualnePrideleneLietadlo() == null);
        return uiEntity;
    }

    public OdletovaDrahaEntity fromNovaOdletovaDraha(NovaOdletovaDrahaUiEntity uiEntity) {
        OdletovaDrahaEntity entity = new OdletovaDrahaEntity();
        entity.setDlzkaDrahy(uiEntity.getDlzkaDrahy());
        entity.setId(uiEntity.getId());

        return entity;
    }

    public HistoriaOdletuUiEntity fromHistoriaOdletuEntity(HistoriaOdletuEntity entity) {

        HistoriaOdletuUiEntity uiEntity = new HistoriaOdletuUiEntity();

        uiEntity.setCasOdletu(entity.getCasDatumOdletu());
        uiEntity.setCasPriletu(entity.getCasDatumPriletu());
        uiEntity.setIdDrahy(entity.getOdletovaDraha().getId().toString());
        uiEntity.setMedzinarodnyKodLietadla(entity.getLietadloEntity().getMedzinarodnyKod());

        return uiEntity;

    }

}
