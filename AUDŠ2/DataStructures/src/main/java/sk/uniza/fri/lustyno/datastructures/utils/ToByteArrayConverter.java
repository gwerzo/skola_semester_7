package sk.uniza.fri.lustyno.datastructures.utils;

import org.apache.commons.lang3.ArrayUtils;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

public class ToByteArrayConverter {

    public static byte[] bytesFromString(String string) {
        return string.getBytes(Charset.defaultCharset());
    }


    public static byte[] bytesFromString(String string, Charset charset) {
        return string.getBytes(charset);
    }

    public static byte[] bytesFromString(String string, String charset) throws UnsupportedEncodingException {
        return string.getBytes(charset);
    }

    public static byte[] bytesFromInt(int value) {
        return new byte[] {
                (byte)(value >>> 24),
                (byte)(value >>> 16),
                (byte)(value >>> 8),
                (byte)value};
    }

    public static byte[] bytesFromLong(long l) {
        byte[] result = new byte[8];
        for (int i = 7; i >= 0; i--) {
            result[i] = (byte)(l & 0xFF);
            l >>= 8;
        }
        return result;
    }

    public static byte[] bytesFromBoolean(Boolean bool) {
        byte[] toRet = new byte[1];
        toRet[0] = ((byte) (bool ? 1 : 0 ));
        return toRet;
    }

    public static byte[] bytesFromLocalDateTime(LocalDateTime ldt) {

        long milis = ldt.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();

        return bytesFromLong(milis);
    }

    public static byte[] bytesFromDouble(Double d) {
        return ByteBuffer.allocate(8).putDouble(d).array();
    }



    public static byte[] concatenateByteArrays(List<byte[]> byteArrays) {
        byte[] actual = new byte[0];
        for (byte[] ba : byteArrays) {
            actual = ArrayUtils.addAll(actual, ba);
        }
        return actual;
    }
}
