package sk.uniza.fri.lustyno.AudsLetisko.components.airport;


import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.LietadloTableUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.AircraftManagementService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;

import java.util.List;

public class VlozenieInformaciiOOdleteForm extends BusinessComponent {

    private AircraftManagementService aircraftManagementService;

    public VlozenieInformaciiOOdleteForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_5.getTitle());
        this.aircraftManagementService = UIServices.getAircraftManagementService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        VerticalLayout layout = new VerticalLayout();

        List<LietadloTableUiEntity> lietadlaSPridelenouDrahou = aircraftManagementService.getVsetkyLietadlaSPridelenouDrahou();
        if (lietadlaSPridelenouDrahou.size() == 0) {
            layout.addComponent(new Label("Žiadne lietadlo nemá pridelenú odletovú dráhu"));
        } else {
            Grid<LietadloTableUiEntity> grid = new Grid<>();
            grid.addComponentColumn(this::buildOdletButton).setCaption("Zaevidovanie odletu");
            grid.addColumn(LietadloTableUiEntity::getMedzinarodnyKod).setCaption("Medzinárodný kód");
            grid.addColumn(LietadloTableUiEntity::getVyrobca).setCaption("Výrobca");
            grid.addColumn(LietadloTableUiEntity::getIdDrahy).setCaption("ID odletovej dráhy");
            grid.addColumn(LietadloTableUiEntity::getCasPoziadavkyOPridelenie).setCaption("Čas požiadavky o pridelenie");

            grid.setSizeFull();

            grid.setItems(lietadlaSPridelenouDrahou);
            layout.addComponent(grid);
        }

        this.parenComponent.showComponent(new Panel("Zaevidovanie odletu lietadla", layout));
    }

    public Button buildOdletButton(LietadloTableUiEntity lietadloTableUiEntity) {
        Button odletButton = new Button("Odlet");

        odletButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {
                OperationMarker m = this.aircraftManagementService.zaevidujOdletLietadla(lietadloTableUiEntity);
                if (m == OperationMarker.OPERATION_SUCCESFUL) {
                    CustomNotification.notification("Lietadlu bol zaevidovaný odlet");
                    showComponent();
                } else if (m == OperationMarker.OPERATION_NEUTRAL) {
                    CustomNotification.notification("Lietadlu bol zaevidovaný odlet a dráha bola automaticky pridelená " +
                            "lietadlu s medzinárodným kódom " + m.getMessage());
                    showComponent();
                }
                return m;
            });
        });

        return odletButton;
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_5.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
