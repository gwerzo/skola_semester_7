﻿namespace FriBettingDatabaseApp.Bo
{
    /// <summary>
    /// Class reprezentácia používateľa systému
    /// </summary>
    public class BettingUser : IdentifiableEntity
    {
        // ID používateľa
        public int BettingUserId { get; set; }
        // Prihlasovacie meno používateľa
        public string Username { get; set; }
        // Balance používateľa
        public double Balance { get; set; }
        // Salt používateľa
        public string PasswordSalt { get; set; }
        // Hashed heslo cez SHA512 alg.
        public string HashedSaltedPassword { get; set; }

        public override int GetId()
        {
            return BettingUserId;
        }
    }
}
