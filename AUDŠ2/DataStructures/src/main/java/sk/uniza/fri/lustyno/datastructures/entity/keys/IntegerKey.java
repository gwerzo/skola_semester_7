package sk.uniza.fri.lustyno.datastructures.entity.keys;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;

public class IntegerKey implements HasComparableValue<Integer> {

    private Integer keyValue;

    public IntegerKey(Integer keyValue) {
        this.keyValue = keyValue;
    }
    public Integer getValue() {
        return keyValue;
    }
    public IntegerKey getMinValue() {
        return new IntegerKey(Integer.MIN_VALUE);
    }
}
