package sk.uniza.fri.lustyno.datastructures.exception;

public class DataStructureInsertDuplicateException extends RuntimeException {

    public DataStructureInsertDuplicateException(String ofClass, String keyValue, String data) {
        super(    "Can not insert key instance of " + ofClass + " "
                + "with key value of " + keyValue + " "
                + "because of unique-key constraint!\n"
                + "Data dump:\n"
                + data
        );
    }

}
