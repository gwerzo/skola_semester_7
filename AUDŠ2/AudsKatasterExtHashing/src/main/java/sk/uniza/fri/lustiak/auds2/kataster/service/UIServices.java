package sk.uniza.fri.lustiak.auds2.kataster.service;

import com.vaadin.spring.server.SpringVaadinServlet;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;

@Service
public class UIServices {

    public static RealEstateService getRealEstateService(){
        return getApplicationContext().getBean(RealEstateService.class);
    }

    public static ApplicationInitializer getInitializer(){
        return getApplicationContext().getBean(ApplicationInitializer.class);
    }

    public static ApplicationContext getApplicationContext() {
        ServletContext servletContext = SpringVaadinServlet.getCurrent().getServletContext();
        return WebApplicationContextUtils.getWebApplicationContext(servletContext);
    }

}
