﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DisertationThemes.Database;

namespace DisertationThemes.ViewerGeneratorApp.menu
{
    class MenuItemShowThemesFiltered: AbstractMenuItem
    {
        public static string MENU_TEXT = "Zobraziť filtrované dizertačné témy";
        public MenuItemShowThemesFiltered(MenuHolder menuHolder) : base(menuHolder)
        {
        }

        public override string GetMenuText()
        {
            return MENU_TEXT;
        }

        protected override int PerformActionInternally()
        {
            using (DisertationDatabaseContext ddc = new DisertationDatabaseContext())
            {
                Console.WriteLine("Po stlačení ENTER sa ti zobrazia školitelia, z ktorých si vyberieš jedného, ktorého dizertačné práce chceš zobraziť");
                Console.ReadLine();
                Supervisor supervisorFilter = this.SelectSupervisor(ddc);

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Po stlačení ENTER sa ti zobrazia študijné odbory, z ktorých si vyberieš jeden, pod ktorý dizertačné práce spadajú");
                Console.ReadLine();
                StProgram stProgramFilter = this.SelectStProgram(ddc);

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Po stlačení ENTER sa ti zobrazia roky dizertčných prác, na základe ktorých môžeš filtrovať práce");
                Console.ReadLine();
                int themeYear = this.SelectYear(ddc);

                this.PrintThemesBasedOn(ddc, supervisorFilter, stProgramFilter, themeYear);
            }
            return 0;
        }

        private void PrintThemesBasedOn(DisertationDatabaseContext ddc, Supervisor supervisorFilter, StProgram stProgramFilter, int yearFilter)
        {
            var themeQuery = ddc.Themes.AsQueryable();

            if (supervisorFilter != null)
            {
                themeQuery = themeQuery.Where(t => t.SupervisorId == supervisorFilter.SupervisorId).Select(t => t);
            }

            if (stProgramFilter != null)
            {
                themeQuery = themeQuery.Where(t => t.StProgramId == stProgramFilter.StProgramId).Select(t => t);
            }

            if (yearFilter != -1)
            {
                themeQuery = themeQuery.Where(t => t.Created.Year == yearFilter).Select(t => t);
            }

            themeQuery.Select(t => t);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Nájdené dizertačné témy podľa filtra:");
            Console.ForegroundColor = ConsoleColor.Green;
            foreach (var theme in themeQuery)
            {
                Console.WriteLine("\t" + theme.Name + "\n");
            }
        }


        private Supervisor SelectSupervisor(DisertationDatabaseContext ddc)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            foreach (var supervisor in ddc.Supervisors)
            {
                Console.WriteLine("\t" + supervisor.SupervisorId + ".\t" + supervisor.FullName);
            }

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("Zadaj ID školiteľa, alebo znak 'A' ak nechceš filtrovať na základe školiteľov: ");
                var userInput = Console.ReadLine();
                if (userInput == "A")
                {
                    return null;
                }

                int supervisorId;
                bool parseSuccess = Int32.TryParse(userInput, out supervisorId);

                if (!parseSuccess)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("ID školiteľa zadané v zlom tvare");
                    continue;
                }

                Supervisor foundById = ddc.Supervisors.SingleOrDefault(s => s.SupervisorId == supervisorId);
                if (foundById == null)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Zadal si neplatné ID školiteľa");
                    continue;
                }

                return foundById;
            }
        }

        private StProgram SelectStProgram(DisertationDatabaseContext ddc)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            foreach (var stProgram in ddc.StPrograms)
            {
                Console.WriteLine("\t" + stProgram.StProgramId + ".\t" + stProgram.Name);
            }

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("Zadaj ID študijného programu, alebo znak 'A' ak nechceš filtrovať na základe študijných programov: ");
                var userInput = Console.ReadLine();
                if (userInput == "A")
                {
                    return null;
                }

                int stProgramId;
                bool parseSuccess = Int32.TryParse(userInput, out stProgramId);

                if (!parseSuccess)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("ID študijného programu zadané v zlom tvare");
                    continue;
                }

                StProgram foundById = ddc.StPrograms.SingleOrDefault(s => s.StProgramId == stProgramId);
                if (foundById == null)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Zadal si neplatné ID študijného programu");
                    continue;
                }

                return foundById;
            }
        }

        private int SelectYear(DisertationDatabaseContext ddc)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            List<int> validYears = new List<int>();
            foreach (var year in ddc.Themes.Select(e => e.Created.Year).Distinct())
            {
                validYears.Add(year);
                Console.WriteLine("\t" + year);
            }

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("Zadaj rok, alebo znak 'A' ak nechceš filtrovať na základe roku: ");
                var userInput = Console.ReadLine();
                if (userInput == "A")
                {
                    return -1;
                }

                int year;
                bool parseSuccess = Int32.TryParse(userInput, out year);

                if (!parseSuccess)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Rok zadaný v zlom tvare");
                    continue;
                }

                if (!validYears.Contains(year))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Zadaný neplatný rok");
                    continue;
                }

                return year;
            }
        }

    }
}
