﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FriBettingDatabaseApp.Bo;
using FriBettingDatabaseApp.Context;

namespace FriBettingDatabaseApp.Repository.Base
{
    /// <summary>
    /// Implementácia generického repository
    /// Námet podľa: https://code-maze.com/async-generic-repository-pattern/
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Repository<T>: IRepository<T> where T : IdentifiableEntity
    {
        

        // Vráti VŠETKY entity
        public IEnumerable<T> GetAll()
        {

            using (var dbContext = new BettingAppDbContext())
            {
                return dbContext.Set<T>().Select(e => e).ToList();
            }
        }

        // Vráti entity, podľa predaného filtra
        public IEnumerable<T> GetAllByCondition(Expression<Func<T, bool>> condition)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                return dbContext.Set<T>().Where(condition);
            }
        }

        public T GetOneByCondition(Expression<Func<T, bool>> expression)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                return dbContext.Set<T>().Where(expression).SingleOrDefault();
            }
        }

        // Vráti entity, podľa predaného filtra, ale zaráta aj pagination
        public IEnumerable<T> GetAllByCondition(Expression<Func<T, bool>> condition, int page, int pageSize)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                return dbContext.Set<T>().Where(condition).Skip(page * pageSize).Take(pageSize);
            }
        }

        // Vloží entitu do setu
        public bool SaveToContext(T saveEntity, BettingAppDbContext toDbContext)
        {
            var savedEntity = toDbContext.Set<T>().Add(saveEntity);
            return savedEntity != null;
        }

    }
}
