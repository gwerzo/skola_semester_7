package tree.splay;

import org.junit.jupiter.api.*;
import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTreeNode;
import sk.uniza.fri.lustyno.datastructures.tree.splay.SplayTree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SplayTreeLogicTest {

    private Random rnd;
    private final int NUMBER_OF_ITEMS = 1000000;
    private final int NUMBER_OF_ITEMS_AFTER = 100000;

    private ArrayList<Integer> randomNumbers;
    private ArrayList<Integer> randomNumbersAfter;

    private SplayTree<IntegerKey, String> testSplayTree;

    @BeforeAll
    public void init() {
        this.randomNumbers = new ArrayList<>(NUMBER_OF_ITEMS);
        this.randomNumbersAfter = new ArrayList<>(NUMBER_OF_ITEMS_AFTER);
        this.rnd = new Random();
        this.testSplayTree = new SplayTree<>();

        fillAndShuffleNumbers();
        fillAndShuffleNumbersAfter();
    }

    @Test
    @Order(1)
    public void test_WarmupTree() {
        randomNumbers.forEach(e -> {
            testSplayTree.insert(new IntegerKey(e), "DATA");
        });
    }

    /**
     * V strome sa nachádza aktuálne {@link #NUMBER_OF_ITEMS} prvkov
     * Pridá ďalších {@link #NUMBER_OF_ITEMS_AFTER} prvkov,
     * s tým, že s pravdepodobnosťou 50%
     * bude odoberať náhodný prvok zo stromu
     * a s tým, že s pravdepodobnosťou 50%
     * v strome vyhľadá náhodný prvok
     *
     * S pravdepodobnosťou 1% vykoná
     * inOrder prehliadku a overí,
     * či počet prvkov v strome je rovná tomu,
     * čomu by sa rovnať mala
     *
     */
    @Test
    @Order(2)
    public void test_RandomOperations() {

        int actualSize = NUMBER_OF_ITEMS;

        while (randomNumbersAfter.size() > 0) {
            testSplayTree.insert(new IntegerKey(randomNumbersAfter.remove(randomNumbersAfter.size() - 1)), "AFTER_DATA");
            actualSize++;

            // S 50% pravdepodobnosťou vymaž random node
            if (rnd.nextBoolean()) {
                int rndIndex = rnd.nextInt(randomNumbers.size());
                testSplayTree.delete(new IntegerKey(randomNumbers.remove(rndIndex)));
                actualSize--;
            }

            // S 50% pravdepodobnosťou vyhľadaj náhodný node
            if (rnd.nextBoolean()) {
                int rndIndex = rnd.nextInt(randomNumbers.size());
                testSplayTree.find(new IntegerKey(randomNumbers.get(rndIndex)));
            }

            if (rnd.nextInt(100) == 0) {
                walkInOrder();
                Assertions.assertEquals(actualSize, testSplayTree.getSize());
            }
        }

        Assertions.assertTrue(testSplayTree.getSize() == actualSize);
    }


    private void fillAndShuffleNumbers() {
        for (int i = 0; i < NUMBER_OF_ITEMS; i++) {
            this.randomNumbers.add(i);
        }
        Collections.shuffle(this.randomNumbers);
    }

    private void fillAndShuffleNumbersAfter() {
        for (int i = NUMBER_OF_ITEMS; i < NUMBER_OF_ITEMS_AFTER + NUMBER_OF_ITEMS; i++) {
            this.randomNumbersAfter.add(i);
        }
        Collections.shuffle(this.randomNumbersAfter);
    }


    private void walkInOrder() {
        List<BinarySearchTreeNode<IntegerKey, String>> inOrder = testSplayTree.inOrder();

        int lowest = Integer.MIN_VALUE;
        for (BinarySearchTreeNode<IntegerKey, String> node : inOrder) {
            if (node.getKey().getValue().intValue() > lowest) {
                lowest = node.getKey().getValue().intValue();
            } else {
                fail("INORDER prehliadka nie je v správnom poradí");
            }
        }
    }
}
