﻿namespace FriBettingDatabaseApp.Bo
{
    /// <summary>
    /// Spoločný predok pre identifikovateľné entity cez integerové ID,
    /// využitie hlavne pre generických repository triedach
    /// </summary>
    public abstract class IdentifiableEntity
    {
        public abstract int GetId();
    }
}
