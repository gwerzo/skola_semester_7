﻿using System.Collections.Generic;

namespace FriBettingDatabaseApp.Bo
{
    /// <summary>
    /// Class reprezentácia športu
    /// </summary>
    public class Sport: IdentifiableEntity
    {

        public int SportId { get; set; }
        public string SportName { get; set; }
        // Športovci (jednotlivci/teamy) viazaní na tento šport
        public ICollection<Competitor> Competitors { get; set; }
        // Defaultný počet častí zápasu (futbal 2, hokej 3,..)
        public int DefaultNumberOfMatchParts { get; set; }


        // Naviazané typy stávok na daný šport
        public virtual ICollection<OddType> OddTypes { get; set; }

        public override int GetId()
        {
            return SportId;
        }
    }
}
