package sk.uniza.fri.lustiak;

import sk.uniza.fri.lustiak.utils.AssetsReader;
import sk.uniza.fri.lustiak.viegenere.ViegenereDecryptor;
import sk.uniza.fri.lustiak.viegenere.ViegenereEncryptor;

public class KryptografiaApplication {

    public static void main(String[] args) {

        AssetsReader assetsReader = new AssetsReader();

        String initText = assetsReader.readWholeFile("text1_enc.txt");
        String pwd = "BRMOVBB";

        String enc = ViegenereEncryptor.encryptTextWithPassword(initText, pwd);
        String dec = ViegenereDecryptor.decryptTextWithPassword(enc, pwd);

        if(!initText.equals(dec)) {
            System.out.println("WRONG");
        }

    }

}
