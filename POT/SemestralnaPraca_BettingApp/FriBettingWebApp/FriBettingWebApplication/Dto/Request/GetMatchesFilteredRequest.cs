﻿using System;

namespace FriBettingWebApplication.Dto.Request
{
    public class GetMatchesFilteredRequest
    {
        // Strana pre pageable
        public int? Page { get; set; }
        // Veľkosť strany pre pageable
        public int? Size { get; set; }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }


    }
}