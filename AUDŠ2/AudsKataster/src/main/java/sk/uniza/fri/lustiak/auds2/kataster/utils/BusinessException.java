package sk.uniza.fri.lustiak.auds2.kataster.utils;

public class BusinessException extends RuntimeException {

    public BusinessException(String message) {
        super(message);
    }

}
