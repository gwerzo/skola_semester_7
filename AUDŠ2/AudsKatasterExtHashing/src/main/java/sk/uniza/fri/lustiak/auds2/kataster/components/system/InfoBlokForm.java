package sk.uniza.fri.lustiak.auds2.kataster.components.system;

import com.vaadin.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.BlockInfoUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.DatabaseInfoUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.OverflowBlockInfoUi;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;

public class InfoBlokForm extends BusinessComponent {

    private RealEstateService realEstateService;

    public InfoBlokForm(RepaintableComponent component) {
        super(component, MENU_TITLE.SYSTEM_MENU_3.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSizeFull();

        Panel infoBlock = new Panel("Informácie o blokoch", layout);
        infoBlock.setSizeFull();
        infoBlock.setHeight("100%");
        infoBlock.setWidth("100%");

        Grid<BlockInfoUi> blockInfoGrid = this.buildMainBlocksGrid();
        blockInfoGrid.setSizeFull();
        Panel mainBlocksPanel = new Panel("Hlavné bloky", blockInfoGrid);

        Grid<OverflowBlockInfoUi> infoGrid = this.buildOverflowBlocksGrid();
        infoGrid.setSizeFull();
        Panel overflowBlocksPanel = new Panel("Overflow bloky", infoGrid);

        mainBlocksPanel.setWidth("100%");
        mainBlocksPanel.setHeight("100%");
        overflowBlocksPanel.setWidth("100%");
        overflowBlocksPanel.setHeight("100%");

        Button dump = new Button("Dump");
        dump.addClickListener(e -> {
           realEstateService.blockDump();
        });

        layout.addComponent(mainBlocksPanel);
        layout.addComponent(overflowBlocksPanel);

        this.parentComponent.showComponent(new VerticalLayout(dump, infoBlock));
    }

    private Grid<OverflowBlockInfoUi> buildOverflowBlocksGrid() {
        Grid<OverflowBlockInfoUi> obInfoGrid = new Grid<>();

        obInfoGrid.addColumn(OverflowBlockInfoUi::getIndexInFile).setCaption("Index").setSortable(true);
        obInfoGrid.addColumn(OverflowBlockInfoUi::getNumberOfValidEntities).setCaption("Počet VALID entít");
        obInfoGrid.addColumn(OverflowBlockInfoUi::getOverflowBlock).setCaption("Ďalší overflow blok");
        obInfoGrid.addColumn(OverflowBlockInfoUi::getEntities).setCaption("Entity");

        obInfoGrid.setItems(this.realEstateService.getOverflowBlocksInfo());
        return obInfoGrid;
    }

    private Grid<BlockInfoUi> buildMainBlocksGrid() {
        Grid<BlockInfoUi> infoGrid = new Grid<>();

        DatabaseInfoUi databaseInfo = this.realEstateService.getDatabaseInfo();

        infoGrid.addColumn(BlockInfoUi::getDirectoryIndex).setCaption("Adresár");
        infoGrid.addColumn(BlockInfoUi::getIndexInFile).setCaption("Index").setSortable(true);
        infoGrid.addColumn(BlockInfoUi::getLocalLevel).setCaption("Level");
        infoGrid.addColumn(BlockInfoUi::getNumberOfValidEntities).setCaption("Valid");
        infoGrid.addColumn(BlockInfoUi::getOverflowBlock).setCaption("Overflow");
        infoGrid.addColumn(BlockInfoUi::getEntities).setCaption("Entity");

        infoGrid.setItems(this.realEstateService.getBlocksInfo());
        return infoGrid;
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_3.getTitle();
    }
}
