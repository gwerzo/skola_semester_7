package sk.uniza.fri.lustiak.auds2.kataster.components.kataster;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.CoordinatesUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.ParcelUiTableEntity;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.RealEstateUiTableEntity;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;

import java.util.LinkedList;


public class VyhladanieNehnutelnostiPodlaGPSForm extends BusinessComponent {

    private Binder<CoordinatesUi> binder;
    private CoordinatesUi entity;

    private RealEstateService realEstateService;

    private Grid<RealEstateUiTableEntity> realEstateGrid;

    public VyhladanieNehnutelnostiPodlaGPSForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_1.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new CoordinatesUi();
        this.binder = new Binder<>(CoordinatesUi.class);
        this.realEstateGrid = new Grid<>();
        this.realEstateGrid.setWidth("100%");

        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getInternalId).setCaption("#");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getSupisneCislo).setCaption("Súpisné číslo");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getPopis).setCaption("Popis");
        this.realEstateGrid.addComponentColumn(this::buildParcelyButton).setCaption("Parcely");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getLatitude).setCaption("Zem.výška");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getLongtitude).setCaption("Zem.šírka");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getLatitudeOrientation).setCaption("Orientácia výšky");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getLongitudeOrientation).setCaption("Orientácia šírky");


        VerticalLayout layout = new VerticalLayout();

        FormLayout formLayout = new FormLayout();

        HorizontalLayout latLayout = new HorizontalLayout();
        TextField latitudeTF = new TextField("Zemepisná výška");
        RadioButtonGroup<String> latitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej výšky");
        latitudeOrientation.setItems("SEVER", "JUH");
        latitudeOrientation.setSelectedItem("SEVER");
        latLayout.addComponents(latitudeTF, latitudeOrientation);

        HorizontalLayout longLayout = new HorizontalLayout();
        TextField longitudeTF = new TextField("Zemepisná šírka");
        RadioButtonGroup<String> longitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej šírky");
        longitudeOrientation.setItems("VÝCHOD", "ZÁPAD");
        longitudeOrientation.setSelectedItem("VÝCHOD");
        longLayout.addComponents(longitudeTF, longitudeOrientation);

        Button findRealEstatesButton = new Button("Vyhľadať nehnuteľnosti");
        findRealEstatesButton.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                if (longitudeOrientation.getSelectedItem().get().equals("VÝCHOD")) {
                    this.entity.setLongitudeOrientation(true);
                } else {
                    this.entity.setLongitudeOrientation(false);
                }
                if (latitudeOrientation.getSelectedItem().get().equals("SEVER")) {
                    this.entity.setLatitudeOrientation(true);
                } else {
                    this.entity.setLatitudeOrientation(false);
                }
                najdiNehnutelnosti(this.entity);
            } catch (ValidationException valEx) {
                this.entity.flush();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        formLayout.addComponent(latLayout);
        formLayout.addComponent(longLayout);
        formLayout.addComponent(findRealEstatesButton);

        layout.addComponent(formLayout);
        layout.addComponent(this.realEstateGrid);

        this.binder.forField(latitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLatitude, CoordinatesUi::setLatitude);

        this.binder.forField(longitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLongitude, CoordinatesUi::setLongitude);


        this.parentComponent.showComponent(new Panel("Vyhľadanie NEHNUTEĽNOSTI podľa GPS súradníc", layout));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_1.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    private void najdiNehnutelnosti(CoordinatesUi coordinatesUi) {
        this.realEstateGrid.setItems(this.realEstateService.getRealEstatesListAtCoordinates(coordinatesUi));
    }

    public Button buildParcelyButton(RealEstateUiTableEntity realEstateUiTableEntity) {
        Button parcelyButton = new Button("Parcely");

        parcelyButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {
                VerticalLayout vl = new VerticalLayout();
                Grid<ParcelUiTableEntity> parcelUiTableEntityGrid;

                parcelUiTableEntityGrid = new Grid<>();
                parcelUiTableEntityGrid.setWidth("100%");
                parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getInternalId).setCaption("#");
                parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getCisloParcely).setCaption("Číslo parcely");
                parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getPopisParcely).setCaption("Popis parcely");
                parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getLatitude).setCaption("Zem. výška");
                parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getLongtitude).setCaption("Zem. šírka");
                parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getLatitudeOrientation).setCaption("Orientácia výšky");
                parcelUiTableEntityGrid.addColumn(ParcelUiTableEntity::getLongitudeOrientation).setCaption("Orientácia šírky");
                parcelUiTableEntityGrid.setItems(realEstateUiTableEntity.getParcely() == null ? new LinkedList<>() : realEstateUiTableEntity.getParcely());

                vl.addComponent(parcelUiTableEntityGrid);

                Window modal = new Window("Parcely na ktorých stojí nehnuteľnosť so súpisným číslom " + realEstateUiTableEntity.getSupisneCislo(), vl);
                modal.addStyleName("window-center");
                modal.setPositionY(100);

                UI.getCurrent().addWindow(modal);

                // Add a JS function that can be called from the client.
                JavaScript.getCurrent().addFunction("centerWindow", args -> {
                    modal.setPositionX((int) ((args.getNumber(1) - args.getNumber(0)) / 2));
                });

                // Execute the function now. In real code you might want to execute the function just after the window is displayed, probably in your enter() method.
                JavaScript.getCurrent().execute("centerWindow(document.getElementsByClassName('window-center')[0].offsetWidth, window.innerWidth)");

                return OperationMarker.OPERATION_SUCCESFUL;
            });
        });

        return parcelyButton;
    }

}
