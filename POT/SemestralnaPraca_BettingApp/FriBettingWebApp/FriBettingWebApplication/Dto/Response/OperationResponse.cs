﻿namespace FriBettingWebApplication.Dto.Response
{
    /// <summary>
    /// DTO reprezentácia operácie, ktorá sa posiela
    /// spätne na klienta po každej operácii,
    /// ktorá vyžaduje napr. vytvorenie entity na BE
    ///
    /// Do tohto DTO sa namapuje prípadný error message (biznis, repository,..)
    /// </summary>
    public class OperationResponse
    {
        public bool Successful { get; set; }
        public string ErrorMessage { get; set; }
        public string SuccessMessage { get; set; }


    }
}