package sk.uniza.fri.lustyno.fripd.components.vehicles;

import sk.uniza.fri.lustyno.fripd.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.fripd.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.fripd.components.common.RepaintableComponent;

public class VyradenieVozidlaPodlaEVCForm extends BusinessComponent {

    public VyradenieVozidlaPodlaEVCForm(RepaintableComponent parentComponent) {
        super(parentComponent, MENU_TITLE.VEHICLES_MENU_6.getTitle());
    }

    @Override
    public boolean isImplemented() {
        return false;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.VEHICLES_MENU_6.getTitle();
    }

}
