package sk.uniza.fri.lustyno.AudsLetisko.entities_bo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import sk.uniza.fri.lustyno.AudsLetisko.utils.PrioritaACasPoziadavkyKey;
import sk.uniza.fri.lustyno.datastructures.queue.QueueNode;

import java.time.LocalDateTime;

@Data
public class LietadloEntity {

    private String nazovVyrobcu;

    private String medzinarodnyKod;

    private Integer minimalnaDlzkaOdletovejDrahy;

    private Integer priorita;

    private LocalDateTime casDatumPriletu;

    private LocalDateTime casDatumPoziadaniaOPridelenie;

    private LocalDateTime casDatumOdletu;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private OdletovaDrahaEntity aktualnePridelenaDraha;

    private QueueNode<PrioritaACasPoziadavkyKey, LietadloEntity> frontaNode;


    public static class Builder {

        private String nazovVyrobcu;

        private String medzinarodnyKod;

        private Integer minimalnaDlzkaOdletovejDrahy;

        private Integer priorita;

        private LocalDateTime casDatumPriletu;

        private LocalDateTime casDatumPoziadaniaOPridelenie;

        private LocalDateTime casDatumOdletu;

        public Builder() {

        }

        public LietadloEntity build() {
            LietadloEntity entity = new LietadloEntity();

            entity.setMedzinarodnyKod(this.medzinarodnyKod);
            entity.setNazovVyrobcu(this.nazovVyrobcu);
            entity.setPriorita(this.priorita);
            entity.setMinimalnaDlzkaOdletovejDrahy(this.minimalnaDlzkaOdletovejDrahy);
            entity.setCasDatumPriletu(this.casDatumPriletu);
            entity.setCasDatumOdletu(this.casDatumOdletu);
            entity.setCasDatumPoziadaniaOPridelenie(this.casDatumPoziadaniaOPridelenie);

            return entity;
        }

        public Builder sMedzinarodnymKodom(String medzinarodnyKod) {
            this.medzinarodnyKod = medzinarodnyKod;
            return this;
        }

        public Builder sNazvomVyrobcu(String nazovVyrobcu) {
            this.nazovVyrobcu = nazovVyrobcu;
            return this;
        }

        public Builder sPrioritou(Integer priorita) {
            this.priorita = priorita;
            return this;
        }

        public Builder sMinimalnouDlzkouDrahy(Integer minimalnaDlzkaOdletovejDrahy) {
            this.minimalnaDlzkaOdletovejDrahy = minimalnaDlzkaOdletovejDrahy;
            return this;
        }

        public Builder sCasomPriletu(LocalDateTime casDatumPriletu) {
            this.casDatumPriletu = casDatumPriletu;
            return this;
        }

        public Builder sCasomPoziadavky(LocalDateTime casDatumPoziadaniaOPridelenie) {
            this.casDatumPoziadaniaOPridelenie = casDatumPoziadaniaOPridelenie;
            return this;
        }

        public Builder sCasomOdletu(LocalDateTime casDatumOdletu) {
            this.casDatumOdletu = casDatumOdletu;
            return this;
        }

    }

}
