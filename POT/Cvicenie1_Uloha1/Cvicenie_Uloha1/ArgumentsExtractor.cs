﻿using System;

namespace Cvicenie_Uloha1
{
    class ArgumentsExtractor
    {

        const int DEFAULT_PADDING = 2;

        const string COLORIZE_COMMAND   = "/c";
        const string PADDING_COMMAND    = "/t";
        const string RECURSION_COMMAND  = "/r";

        const string WRONG_ARGS_EXCEPTION = "Zadali ste nesprávne príkazy";

        /**
         * Vráti počet rekurzívnych vnorení z argumentu "/r hodnota"
         * Kde 
         *   0                        =      /r nemalo argument, alebo argument bol 0 => vnáranie bude do "nekonečna"
         *   ArgumentException        =      /r dostalo ako argument neodparsovateľný integer(aj keby bolo zadané záporné číslo)
         *   -1                       =      /r nebolo v príkazoch - nevnára sa
         *   <1;n>                    =      /r argument bol odparsovaný správne a je ním hodnota, ktoré reprezentuje počet vnorení 
         */
        public static int extractRecursive(string[] args) {
            int i = 0;
            foreach (string arg in args) {
                // Preskočenie prvého arg, ten by mal byť cesta k súboru/adresáru
                if (i == 0) {
                    i++;
                    continue;
                }
                if (arg == RECURSION_COMMAND) {
                    if (args.GetLength(0) > i + 1)
                    {
                        try {
                            int recursionValue = Int32.Parse(args[i + 1]);
                            if (recursionValue < 0)
                            {
                                throw new System.ArgumentException(WRONG_ARGS_EXCEPTION);
                            }
                            else {
                                return recursionValue;
                            }
                        }
                        catch (FormatException e) {
                            throw new System.ArgumentException(WRONG_ARGS_EXCEPTION);
                        }
                    }
                    else {
                        return 0;
                    }
                }
                i++;
            }
            return -1;
        }

        /**
         * Vráti počet medzier na odsadzovanie vnorených súborov z argumentu "/t hodnota"
         * Kde 
         *   2                       =      /t príkaz nemal argument alebo príkaz mal ako argument č. 2 alebo args vôbec neobsahovali tento argument
         *   ArgumentException       =      /t dostalo ako argument neodparsovateľný integer(aj keby bolo zadané záporné číslo)
         *   <0;n>                   =      /t argument bol odparsovaný správne a je ním hodnota, ktoré reprezentuje počet medzier paddingu
         */
        public static int extractPadding(string[] args) {
            int i = 0;
            foreach (string arg in args) {
                // Preskočenie prvého arg, ten by mal byť cesta k súboru/adresáru
                if (i == 0)
                {
                    i++;
                    continue;
                }
                if (arg == PADDING_COMMAND) {
                    // Ak veľkosť poľa je viac ako index, na ktorom sa nachádza príkaz "/t", skús odparsovať nasledujúci parameter
                    if (args.GetLength(0) > i)
                    {
                        try
                        {
                            int paddingValue = Int32.Parse(args[i + 1]);
                            if (paddingValue >= 0)
                            {
                                return paddingValue;
                            }
                            else
                            {
                                throw new System.ArgumentException(WRONG_ARGS_EXCEPTION);
                            }
                        }
                        catch (FormatException e)
                        {
                            throw new System.ArgumentException(WRONG_ARGS_EXCEPTION);
                        }
                    }
                    // Ak veľkosť poľa 
                    else {
                        return DEFAULT_PADDING;
                    }
                }
                i++;
            }
            return DEFAULT_PADDING;
        }

        /**
         * Vráti boolean podľa toho, či args obrahovali príkaz /c
         */
        public static Boolean extractColorize(string[] args) {
            bool firstArg = true;
            foreach (string arg in args) {
                // Preskočenie prvého arg, ten by mal byť cesta k súboru/adresáru
                if (firstArg)
                {
                    firstArg = !firstArg;
                    continue;
                }
                if (arg == COLORIZE_COMMAND) {
                    return true;
                }
            }
            return false;
        }

    }

    
}
