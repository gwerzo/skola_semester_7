﻿using System;
using System.Collections.Generic;
using MemoryGameServerApp.Server;
using SuperWebSocket;

namespace MemoryGameServerApp.Command
{
    public class GetOpenGamesCommand: ICommand
    {
        public const string COMMAND = "-listopengames";
        private const string OPEN_GAMES_LIST = "-opengameslist";

        public void executeCommand(string usernameOfCommandCaller, MemoryGameServer memoryGameServer,
            WebSocketSession callerSocketSession)
        {
            List<string> openGames = memoryGameServer.GetOpenGamesUuidsList();

            callerSocketSession.Send(OPEN_GAMES_LIST + " " + String.Join(" ", openGames));
        }
    }
}
