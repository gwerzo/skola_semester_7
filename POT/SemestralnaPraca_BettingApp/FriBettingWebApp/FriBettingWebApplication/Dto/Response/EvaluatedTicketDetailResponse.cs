﻿namespace FriBettingWebApplication.Dto.Response
{
    public class EvaluatedTicketDetailResponse
    {

        public int TicketId { get; set; }
        public double TotalOdds { get; set; }
        public double WinPotential { get; set; }
        public double BetValue { get; set; }
        public bool IsWinning { get; set; }

    }
}