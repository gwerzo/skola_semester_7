package sk.uniza.fri.lustiak.auds2.kataster.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import sk.uniza.fri.lustiak.auds2.kataster.entities.bo.RealEstate;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.utils.BusinessException;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;
import sk.uniza.fri.lustyno.datastructures.extendiblehashing.*;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RealEstateService {

    private final String FILE_NAME = "real_estates";
    private final String fileNameTimestamped;

    private EHFile<RealEstate> realEstates;
    private EntityMapperService entityMapperService;

    public RealEstateService(EntityMapperService entityMapperService) {
        this.entityMapperService = entityMapperService;

        EHPrototypeManager.registerNewPrototype(new RealEstate(-1, -1, "",
                0.0, Boolean.TRUE, 0.0, Boolean.TRUE,
                0.0, Boolean.TRUE, 0.0, Boolean.TRUE));

        Calendar calendar = Calendar.getInstance();
        this.fileNameTimestamped = this.FILE_NAME + calendar.getTimeInMillis();
        this.realEstates = new EHFile<>(RealEstate.class, fileNameTimestamped, 2, 2, 3);
    }


    public OperationMarker addNewRealEstate(CreateNewRealEstateUi createNewRealEstateUi) {
        RealEstate realEstate = this.entityMapperService.fromCreateNewRealEstateUi(createNewRealEstateUi);

        log.info("Adding new real estate with ID {} at coords \n" +
                "{} {}, {} {}\n" +
                "{} {}, {} {}",
                createNewRealEstateUi.getIdentifikacneCislo(),

                createNewRealEstateUi.getIsNorth1() ? "NORTH" : "SOUTH", createNewRealEstateUi.getLatitude1(),
                createNewRealEstateUi.getIsEast1() ? "EAST" : "WEST", createNewRealEstateUi.getLongitude1(),

                createNewRealEstateUi.getIsNorth2() ? "NORTH" : "SOUTH", createNewRealEstateUi.getLatitude2(),
                createNewRealEstateUi.getIsEast2() ? "EAST" : "WEST", createNewRealEstateUi.getLongitude2()
        );

        if (this.realEstates.insert(realEstate)) {
            log.info("Successfully added new real estate with ID {}", createNewRealEstateUi.getIdentifikacneCislo());
            return OperationMarker.OPERATION_SUCCESFUL;
        } else {
            throw new BusinessException("Nepodarilo sa vložiť entitu s ID " + createNewRealEstateUi.getIdentifikacneCislo());
        }
    }

    public OperationMarker editRealEstate(FindRealEstateUi oldEntityUi, EditRealEstateUi editRealEstateUi) {

        RealEstate oldEntity = new RealEstate(oldEntityUi.getIdentifikacneCislo());
        RealEstate realEstate = this.entityMapperService.fromEditRealEstateUi(editRealEstateUi);

        // Ak sa edituje aj ID, musí sa entita zmazať a nanovo vložiť, to sa ale nemôže stať v určitých prípadoch
        if (editRealEstateUi.getEditConstraints()) {

            // Ak nová entita má rovnaké ID ako "stará", zavolá sa iba obyčajný update
            // Používateľ síce zaškrtol, že edituje aj ID, ale nezeditoval ho
            if (oldEntity.getIdentifikacneCislo().equals(editRealEstateUi.getIdentifikacneCislo())) {
                return this.realEstates.update(realEstate) ? OperationMarker.OPERATION_SUCCESFUL : OperationMarker.OPERATION_UNSUCCESFUL;
            }

            // Používateľ zmenil ID na nejaké nové, ale toto ID sa už v databáze nachádza, preto update by spôsobil prepísanie
            // iných dát, ako je myslené (prepísali by sa dáta entity, na ktorú ukazuje dané ID)
            RealEstate alreadyExistingRealEstate = this.realEstates.find(realEstate);
            if (alreadyExistingRealEstate != null) {
                throw new BusinessException("Nemôžeš zmeniť ID entity na ID " + realEstate.getIdentifikacneCislo() + " pretože nehnuteľnosť s týmto ID už existuje");
            }

            // Tu je všetko "ohandlované", už sa len zavolá zmazanie starej entity a pridanie novej
            this.realEstates.delete(oldEntity);

            return this.realEstates.insert(realEstate) ? OperationMarker.OPERATION_SUCCESFUL : OperationMarker.OPERATION_UNSUCCESFUL;
        } else {
            // Ak sa needituje ID, iba sa zavolá update
            return this.realEstates.update(realEstate) ? OperationMarker.OPERATION_SUCCESFUL : OperationMarker.OPERATION_UNSUCCESFUL;
        }
    }

    public OperationMarker removeRealEstate(DeleteRealEstateUi deleteRealEstateUi) {

        RealEstate toDeleteRealEstate = new RealEstate(deleteRealEstateUi.getIdentifikacneCislo());

        return this.realEstates.delete(toDeleteRealEstate) == null ? OperationMarker.OPERATION_UNSUCCESFUL : OperationMarker.OPERATION_SUCCESFUL;
    }

    public RealEstateUiTableEntity findRealEstate(FindRealEstateUi id) {
        RealEstate toFind = new RealEstate(id.getIdentifikacneCislo());

        RealEstate found = this.realEstates.find(toFind);

        if (found == null) {
            throw new BusinessException("Nehnuteľnosť s ID " + id + " neexistuje");
        } else {
            return this.entityMapperService.fromRealEstate(found);
        }
    }

    public DatabaseInfoUi getDatabaseInfo() {

        EHFileDescriptor fileDescriptor = this.realEstates.getDescription();
        return this.entityMapperService.fromFileDescriptor(fileDescriptor);
    }

    public List<BlockInfoUi> getBlocksInfo() {

        List<EHBlockDescription> mainBlocksDescriptions = this.realEstates.getBlockDescriptions();

        return mainBlocksDescriptions.stream().map(e -> this.entityMapperService.fromEhBlockDescription(e)).collect(Collectors.toList());
    }

    public List<OverflowBlockInfoUi> getOverflowBlocksInfo() {

        List<EHBlockDescription> mainBlocksDescriptions = this.realEstates.getOverflowBlockDescriptions();

        return mainBlocksDescriptions.stream().map(e -> this.entityMapperService.fromEhOverflowBlockDescription(e)).collect(Collectors.toList());
    }

    public void saveState() {
        log.info("Saved state of file into file " + fileNameTimestamped);
        this.realEstates.saveState();
    }

    public void loadFromFile(String filename) {

        EHFileConfig loaded = EHFileConfig.fromFile(filename + "_save.ehdb");

        this.realEstates = new EHFile<>(RealEstate.class, loaded);

    }

    public void blockDump() {
        this.realEstates.blockDump();
    }

}
