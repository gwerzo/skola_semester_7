﻿using System;
using System.Collections.Generic;

namespace FriBettingDatabaseApp.Bo
{
    /// <summary>
    /// Class reprezentácia zápasu, nad ktorým je evidovaný
    /// stav, priebeh, čas, ...
    /// </summary>
    public class Match: IdentifiableEntity
    {

        public int MatchId { get; set; }
        public ICollection<MatchPart> MatchParts { get; set; }
        public DateTime MatchStartDateTime { get; set; }
        public Competitor HostCompetitor { get; set; }
        public Competitor GuestCompetitor { get; set; }
        public bool IsEvaluated { get; set; }
        public bool IsCancelled { get; set; }
        public Sport Sport { get; set; }
        // False = Host winner, True = Guest winner
        public bool Winner { get; set; }
        public int TotalGoalsHost { get; set; }
        public int TotalGoalsGuest { get; set; }

        public override int GetId()
        {
            return MatchId;
        }
    }
}
