package sk.uniza.fri.lustyno.fripd.view;

import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public abstract class GenericVerticalView extends UI {

    private VerticalLayout layout = new VerticalLayout();;

    public GenericVerticalView() {
        setHeight("100%");
        setWidth("100%");

        layout.setWidth("100%");
        layout.setHeight("100%");
        setContent(layout);
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
    }

    public void addComponent(Component component) {
        this.layout.addComponent(component);
    }
}
