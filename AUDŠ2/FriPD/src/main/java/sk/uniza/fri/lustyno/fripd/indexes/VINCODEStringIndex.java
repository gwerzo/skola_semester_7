package sk.uniza.fri.lustyno.fripd.indexes;

import lombok.Data;
import sk.uniza.fri.lustyno.datastructures.entity.keys.StringKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.HasSize;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.StringUtils;
import sk.uniza.fri.lustyno.fripd.indexes.bases.StringIndex;

import java.util.Arrays;

@Data
@HasSize(sizeInBytes = 22)
public class VINCODEStringIndex extends StringIndex {

    public VINCODEStringIndex(String key, Integer offset) {
        super(StringUtils.padRightDollars(key, 17), offset);
    }


    public static VINCODEStringIndex emptyInvalidInstance() {
        return new VINCODEStringIndex("", -1);
    }

    public static VINCODEStringIndex fromByteArray(byte[] fromBytes) {
        VINCODEStringIndex en = new VINCODEStringIndex("", -1);
        en.valid = FromByteArrayConverter.booleanFromByte(fromBytes[0]);
        en.key = new StringKey(FromByteArrayConverter.stringFromBytes(Arrays.copyOfRange(fromBytes, 1, 18)));
        en.offset = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(fromBytes, 18, 22));
        return en;
    }


}
