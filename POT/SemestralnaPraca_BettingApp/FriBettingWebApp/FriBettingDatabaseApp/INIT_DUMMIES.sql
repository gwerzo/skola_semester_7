﻿-- Inicializácia športov
insert into Sports values ('Futbal', 2); -- ID 1
insert into Sports values ('Hokej', 3); -- ID 2


-- Inicializácia typov stávok
insert into OddTypes values ('1', 'Výhra domáceho', 0, 0); -- ID 1
insert into OddTypes values ('X', 'Remíza', 0, 1); -- ID 2
insert into OddTypes values ('2', 'Výhra hosťujúceho', 0, 2); -- ID 3

insert into OddTypes values ('1/H', 'Prvý polčas vyhrá domáci', 1, 0); -- ID 4
insert into OddTypes values ('1/X', 'Prvý polčas skončí remízou', 1, 1); -- ID 5
insert into OddTypes values ('1/G', 'Prvý polčas vyhrá hosťujúci', 1, 2); -- ID 6
insert into OddTypes values ('2/H', 'Druhý polčas vyhrá domáci', 2, 0); -- ID 7
insert into OddTypes values ('2/X', 'Druhý polčas skončí remízou', 2, 1); -- ID 8
insert into OddTypes values ('2/G', 'Druhý polčas vyhrá hosťujúci', 2, 2); -- ID 9

insert into OddTypes values ('1/H', 'Prvú tretinu vyhrá domáci', 1, 0); -- ID 10
insert into OddTypes values ('1/X', 'Prvá tretina skončí remízou', 1, 1); -- ID 11
insert into OddTypes values ('1/G', 'Prvú tretinu vyhrá hosťujúci', 1, 2); -- ID 12
insert into OddTypes values ('2/H', 'Druhú tretinu vyhrá domáci', 2, 0); -- ID 13
insert into OddTypes values ('2/X', 'Druhá tretina skončí remízou', 2, 1); -- ID 14
insert into OddTypes values ('2/G', 'Druhú tretinu vyhrá hosťujúci', 2, 2); -- ID 15
insert into OddTypes values ('3/H', 'Tretiu tretinu vyhrá domáci', 3, 0); -- ID 16
insert into OddTypes values ('3/X', 'Tretia tretina skončí remízou', 3, 1); -- ID 17
insert into OddTypes values ('3/G', 'Tretiu tretinu vyhrá hosťujúci', 3, 2); -- ID 18

-- Inicializácia väzby M:N medzi Šport-TypStávky
insert into SportOddTypes values (1, 1); -- Futbal výhra domáci
insert into SportOddTypes values (1, 2); -- Futbal remíza
insert into SportOddTypes values (1, 3); -- Futbal výhra hosť
insert into SportOddTypes values (1, 4); -- Futbal výhra domáci (prvá tretina)
insert into SportOddTypes values (1, 5); -- Futbal remíza (prvá tretina)
insert into SportOddTypes values (1, 6); -- Futbal výhra hosť (prvá tretina)
insert into SportOddTypes values (1, 7); -- Futbal výhra domáci (druhá tretina)
insert into SportOddTypes values (1, 8); -- Futbal remíza (druhá tretina)
insert into SportOddTypes values (1, 9); -- Futbal výhra hosť (druhá tretina)

insert into SportOddTypes values (2, 1); -- Hokej výhra domáci
insert into SportOddTypes values (2, 2); -- Hokej remíza
insert into SportOddTypes values (2, 3); -- Hokej výhra hosť
insert into SportOddTypes values (2, 10); -- Hokej výhra domáci (prvá tretina)
insert into SportOddTypes values (2, 11); -- Hokej remíza (prvá tretina)
insert into SportOddTypes values (2, 12); -- Hokej vyhrá hosť (prvá tretina)
insert into SportOddTypes values (2, 13); -- Hokej výhra domáci (druhá tretina)
insert into SportOddTypes values (2, 14); -- Hokej remíza (druhá tretina)
insert into SportOddTypes values (2, 15); -- Hokej výhra hosť (druhá tretina)
insert into SportOddTypes values (2, 16); -- Hokej výhra domáci (tretia tretina)
insert into SportOddTypes values (2, 17); -- Hokej remíza (tretia tretina)
insert into SportOddTypes values (2, 18); -- Hokej výhra hosť (tretia tretina)

-- Inicializácia competitorov (športovci, tímy)
insert into Competitors values (1, 'FC Barcelona', 1); --Futbal
insert into Competitors values (1, 'Real Madrid', 1); -- Futbal
insert into Competitors values (1, 'Bayern München', 1); -- Futbal
insert into Competitors values (1, 'Manchester City', 1); -- Futbal
insert into Competitors values (1, 'Atlético Madrid', 1); -- Futbal
insert into Competitors values (1, 'Liverpool FC', 1); -- Futbal
insert into Competitors values (1, 'Juventus', 1); -- Futbal
insert into Competitors values (1, 'Inter Milan', 1); -- Futbal
insert into Competitors values (1, 'Manchester United', 1); -- Futbal


insert into Competitors values (1, 'Toronto Maple Leafs', 2); -- Hokej
insert into Competitors values (1, 'Montreal Canadiens', 2); -- Hokej
insert into Competitors values (1, 'Boston Bruins', 2); -- Hokej
insert into Competitors values (1, 'Edmonton Oilers', 2); -- Hokej
insert into Competitors values (1, 'Vancouver Canucks', 2); -- Hokej
insert into Competitors values (1, 'New York Islanders', 2); -- Hokej
insert into Competitors values (1, 'Calgary Flames', 2); -- Hokej
insert into Competitors values (1, 'Colorado Avalanche', 2); -- Hokej
insert into Competitors values (1, 'Pittsburgh Penguins', 2); -- Hokej
