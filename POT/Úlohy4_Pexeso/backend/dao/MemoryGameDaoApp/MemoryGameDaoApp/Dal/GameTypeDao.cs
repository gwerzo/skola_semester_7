﻿using MemoryGameDaoApp.Bo;
using MemoryGameDaoApp.Context;

namespace MemoryGameServerApp.Dal
{
    public class GameTypeDao
    {

        public GameTypeDao()
        {

        }


        // Vytvorí nový typ hry (jednen z parametrov musí byť párny, nemôže byť napr. gameboard 3x3)
        public bool CreateNewGameType(int widthOfGame, int heightOfGame)
        {
            if (widthOfGame % 2 == 1 && heightOfGame % 2 == 0)
            {
                return false;
            }

            using (var dbContext = new MemoryGameDbContext())
            {
                GameType newGameType = new GameType();

                newGameType.XLength = widthOfGame;
                newGameType.YLength = heightOfGame;
                dbContext.GameTypes.Add(newGameType);
                dbContext.SaveChanges();
                return true;
            }
        }

        public GameType GetGameTypeById(int id)
        {
            using (var dbContext = new MemoryGameDbContext())
            {
                return dbContext.GetGameTypeById(id);
            }
        }
    }
}
