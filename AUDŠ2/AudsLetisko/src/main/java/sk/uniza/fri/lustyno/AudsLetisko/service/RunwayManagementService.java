package sk.uniza.fri.lustyno.AudsLetisko.service;

import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.uniza.fri.lustyno.AudsLetisko.BusinessException;
import sk.uniza.fri.lustyno.AudsLetisko.codelists.DrahyCiselnik;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.LietadloEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.OdletovaDrahaEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.OdletovaDrahaTableUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.ZmenaPriorityUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entity_mapper.EntityMapper;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;
import sk.uniza.fri.lustyno.AudsLetisko.utils.PrioritaACasPoziadavkyKey;
import sk.uniza.fri.lustyno.AudsLetisko.utils.ZaevidovaniePoziadavkyMarker;
import sk.uniza.fri.lustyno.AudsLetisko.utils.ZaevidovaniePoziadavkyResult;
import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureInsertDuplicateException;
import sk.uniza.fri.lustyno.datastructures.queue.PairingHeap;
import sk.uniza.fri.lustyno.datastructures.queue.QueueNode;
import sk.uniza.fri.lustyno.datastructures.tree.NodeResult;
import sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTreeNode;
import sk.uniza.fri.lustyno.datastructures.tree.splay.SplayTree;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RunwayManagementService {

    @Autowired
    private DateTimeService dateTimeService;

    @Autowired
    private DrahyCiselnik drahyCiselnik;

    @Autowired
    private AircraftManagementService aircraftManagementService;

    @Autowired
    private EntityMapper entityMapper;

    /**
     * Reprezentácia všetkých odletových dráh evidovaných v systéme
     */
    private SplayTree<IntegerKey, OdletovaDrahaEntity> vsetkyOdletoveDrahy;

    /**
      * Reprezentácia voľných odletových dráh, ktoré sú zgrupené podľa dĺžky
      *  umožňuje rýchle overenie resp. pridanie/odstránenie voľnej dráhy
      */
    private List<Pair<Integer, LinkedList<OdletovaDrahaEntity>>> volneDrahy;

    /**
     * Reprezentácia čakajúcich lietadiel na pridelenie odletovej dráhy,
     * zgrupené podľa dĺžky odletovej dráhy na ktorú čakajú
     */
    private List<Pair<Integer, PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity>>> cakajuceLietadlaPodlaDrah;

    public RunwayManagementService() {
        this.volneDrahy = new LinkedList<>();
        this.cakajuceLietadlaPodlaDrah = new LinkedList<>();
        this.vsetkyOdletoveDrahy = new SplayTree<>();
    }

    /**
     * Vráti PairingHeap lietadiel čakajúcich na konkrétnu dĺžku dráhy
     */
    public PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity> getCakajuceLietadlaNaDanuDlzku(Integer dlzka) {

        PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity> cakajuceLietadla = null;
        for (Pair<Integer, PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity>> dlzkyDrah : this.cakajuceLietadlaPodlaDrah) {
            if (dlzkyDrah.getKey().equals(dlzka)) {
                cakajuceLietadla = dlzkyDrah.getValue();
                break;
            }
        }

        return cakajuceLietadla;
    }

    /**
     * Vráti odletovú dráhu podľa zadaného ID odletovej dráhy
     * @param odletovaDrahaId
     * @return
     */
    public OdletovaDrahaEntity getOdletovaDraha(Integer odletovaDrahaId) {
        NodeResult<BinarySearchTreeNode<IntegerKey, OdletovaDrahaEntity>> odletovaDraha =
                vsetkyOdletoveDrahy.find(new IntegerKey(odletovaDrahaId));
        if (odletovaDraha.wasFound()) {
            return odletovaDraha.get().getData();
        } else {
            return null;
        }
    }

    /**
     * Pridá lietadlo do čakacieho frontu na danú dĺžku dráhy
     * @param lietadloEntity - lietadlo, ktoré čaká na danú dĺžku dráhy
     * @param dlzkaDrahy - dĺžka dráhy, na ktorú čaká
     */
    public ZaevidovaniePoziadavkyResult zaevidujCakajuceLietadloNaOdletovuDrahu(LietadloEntity lietadloEntity, Integer dlzkaDrahy) {

        // Ak je voľná dráha, akú lietadlo vyžaduje
        if (jeVolnaDrahaPreDlzku(dlzkaDrahy)) {
            OdletovaDrahaEntity draha = popVolnaDraha(dlzkaDrahy);
            draha.setAktualnePrideleneLietadlo(lietadloEntity);
            lietadloEntity.setAktualnePridelenaDraha(draha);

            ZaevidovaniePoziadavkyResult result = new ZaevidovaniePoziadavkyResult();
            result.setPoziadavkaResult(ZaevidovaniePoziadavkyMarker.LIETADLO_AUTOMATICKY_PRIRADENE_NA_DRAHU);
            return result;
        }

        // Ak nie je voľná takáto dráha, nájdi front pre danú dĺžku
        PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity> frontDanejDlzky = null;
        for (Pair<Integer, PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity>> pair : cakajuceLietadlaPodlaDrah) {
            if (pair.getKey().equals(dlzkaDrahy)) {
                frontDanejDlzky = pair.getValue();
                break;
            }
        }

        // Ak doteraz neexistuje front na danú dĺžku, vytvor ho
        if (frontDanejDlzky == null) {
            frontDanejDlzky = new PairingHeap<>();
            cakajuceLietadlaPodlaDrah.add(new Pair<>(dlzkaDrahy, frontDanejDlzky));
        }

        ZaevidovaniePoziadavkyResult result = new ZaevidovaniePoziadavkyResult();

        QueueNode<PrioritaACasPoziadavkyKey, LietadloEntity> node = frontDanejDlzky.insert(
                new PrioritaACasPoziadavkyKey(lietadloEntity.getPriorita(), dateTimeService.getActualDateTime()), lietadloEntity);

        lietadloEntity.setFrontaNode(node);

        result.setNodeVoFronte(node);
        result.setPoziadavkaResult(ZaevidovaniePoziadavkyMarker.LIETADLO_PRIDANE_DO_FRONTU_CAKAJUCICH);

        return result;
    }

    /**
     * Vráti, či je voľná nejaká dráha pre zadanú dĺžku
     * @param dlzka - dĺžka dráhy v metroch
     * @return - príznak, či existuje voľná dráha danej dĺžky
     */
    private boolean jeVolnaDrahaPreDlzku(Integer dlzka) {
        Pair<Integer, List<OdletovaDrahaEntity>> volneDrahy = null;
        for (Pair<Integer, LinkedList<OdletovaDrahaEntity>> pair : this.volneDrahy) {
            if (pair.getKey().equals(dlzka)) {
                return pair.getValue().size() > 0;
            }
        }
        return false;
    }

    /**
     * Vráti voľnú dráhu danej dĺžky, musí pred ňou byť ale isté,
     * že daná dráha danej dĺžky existuje, inak
     * @throws RuntimeException
     *
     * @param dlzka - dĺžka, podľa ktorej sa má vrátiť dráha
     * @return - dráha s danou dĺžkou
     */
    private OdletovaDrahaEntity popVolnaDraha(Integer dlzka) {
        Pair<Integer, LinkedList<OdletovaDrahaEntity>> volneDrahy = null;
        for (Pair<Integer, LinkedList<OdletovaDrahaEntity>> pair : this.volneDrahy) {
            if (pair.getKey().equals(dlzka)) {
                volneDrahy = pair;
                break;
            }
        }
        if (volneDrahy == null) {
            throw new RuntimeException("Nemôžete volať vytiahnutie voľnej dráhy, keď žiadna voľná dráha s dĺžkou " + dlzka + " neexistuje");
        } else {
            return volneDrahy.getValue().removeFirst();
        }
    }

    /**
     * Pridá do systému novú odletovú dráhu
     * @param entity - nová dráha
     * @return - marker či sa to podarilo alebo nie
     */
    public OperationMarker pridajNovuDrahu(OdletovaDrahaEntity entity) {
        try {
            this.vsetkyOdletoveDrahy.insert(new IntegerKey(entity.getId()), entity);
            this.drahyCiselnik.addNovaDlzkaDrahy(entity.getDlzkaDrahy());
            return this.pridajVolnuDrahu(entity);
        } catch (DataStructureInsertDuplicateException e) {
            throw new BusinessException("Dráha s takýmto ID už existuje!");
        }
    }

    /**
     * Pridá novú voľnú dráhu do systému ak na dráhu s danou dĺžkou nečaká
     * nejaké lietadlo, ak áno, je mu rovno pridelená
     *
     * @param entity
     */
    private OperationMarker pridajVolnuDrahu(OdletovaDrahaEntity entity) {

        PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity> lietadlaCakajuceNaDanuDlzkuDrahy =
                this.getCakajuceLietadlaNaDanuDlzku(entity.getDlzkaDrahy());
        if (lietadlaCakajuceNaDanuDlzkuDrahy != null && lietadlaCakajuceNaDanuDlzkuDrahy.getSize() != 0) {
            // Existuje lietadlo, ktoré čaká práve na takúto dĺžku dráhy
            LietadloEntity cakajuceLietadlo = lietadlaCakajuceNaDanuDlzkuDrahy.deleteMin().getData();
            cakajuceLietadlo.setFrontaNode(null);
            cakajuceLietadlo.setAktualnePridelenaDraha(entity);
            entity.setAktualnePrideleneLietadlo(cakajuceLietadlo);
            aircraftManagementService.zaevidujPridanieDrahyLietadlu(cakajuceLietadlo);

            OperationMarker result = OperationMarker.OPERATION_NEUTRAL;
            result.setMessage(cakajuceLietadlo.getMedzinarodnyKod());
            return result;
        }


        Pair<Integer, LinkedList<OdletovaDrahaEntity>> volneDrahy = null;
        for (Pair<Integer, LinkedList<OdletovaDrahaEntity>> pair : this.volneDrahy) {
            if (pair.getKey().equals(entity.getDlzkaDrahy())) {
                volneDrahy = pair;
                break;
            }
        }

        // Ak voľné dráhy s takouto dĺžkou ešte neexistujú, vytvor nový zoznam
        if (volneDrahy == null) {
            LinkedList<OdletovaDrahaEntity> drahy = new LinkedList<>();
            drahy.add(entity);
            volneDrahy = new Pair<Integer, LinkedList<OdletovaDrahaEntity>>(entity.getDlzkaDrahy(), drahy);
            this.volneDrahy.add(volneDrahy);
        } else {
            volneDrahy.getValue().add(entity);
        }
        return OperationMarker.OPERATION_SUCCESFUL;
    }

    /**
     * Pridá dráhu naspäť medzi voľné dráhy
     * @param entity
     */
    public void pridajUvolnenuDrahu(OdletovaDrahaEntity entity) {
        Pair<Integer, LinkedList<OdletovaDrahaEntity>> volneDrahy = null;
        for (Pair<Integer, LinkedList<OdletovaDrahaEntity>> pair : this.volneDrahy) {
            if (pair.getKey().equals(entity.getDlzkaDrahy())) {
                pair.getValue().add(entity);
                break;
            }
        }
    }


    /**
     * Zmenúi prioritu lietadlu čakajúcemu vo fronte
     */
    public OperationMarker zmenPriorituLietadlu(ZmenaPriorityUiEntity entity) {

        LietadloEntity lietadlo = this.aircraftManagementService.getLietadloCakajuceNaPrideleniOdletovejDrahy(entity);

        if (lietadlo == null) {
            LietadloEntity lietadloNezaevidovaneNaCakanie =
                    this.aircraftManagementService.getLietadloZoSystemuByMedzinarodnyKod(entity.getMedzinarodnyKodLietadla());
            if (lietadloNezaevidovaneNaCakanie == null) {
                throw new BusinessException("Lietadlo s takýmto medzinárodným kódom neexistuje v systéme");
            }
            lietadloNezaevidovaneNaCakanie.setPriorita(entity.getNovaPriorita());
            return OperationMarker.OPERATION_SUCCESFUL;
        }

        // Toto by sa nikdy nemalo stať, preto takáto exception
        if (lietadlo.getFrontaNode() == null) {
            throw new RuntimeException();
        }

        PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity> cakajuceLietadlaNaDanuDlzku =
                getCakajuceLietadlaNaDanuDlzku(this.drahyCiselnik.getNajblizsiaDlzkaMoznaDlzka(lietadlo.getMinimalnaDlzkaOdletovejDrahy()));

        cakajuceLietadlaNaDanuDlzku.changePriority(
                lietadlo.getFrontaNode(),
                new PrioritaACasPoziadavkyKey(entity.getNovaPriorita(), lietadlo.getFrontaNode().getKey().getValue().getCasPoziadania()));

        lietadlo.setPriorita(entity.getNovaPriorita());

        return OperationMarker.OPERATION_SUCCESFUL;
    }

    /**
     * Vráti všetky odletové dráhy ktoré je neskôr možné referencovať prejdené level orderom
     *
     * @return - level order všetkých dráh v systéme
     */
    public List<OdletovaDrahaEntity> getVsetkyOdletoveDrahyVSystemePreUlozenieDoSuboru() {
        List<BinarySearchTreeNode<IntegerKey, OdletovaDrahaEntity>> odletoveDrahyNodes = this.vsetkyOdletoveDrahy.levelOrder();
        if (odletoveDrahyNodes == null || odletoveDrahyNodes.size() == 0) {
            return null;
        }
        return odletoveDrahyNodes.stream().map(e -> e.getData()).collect(Collectors.toList());
    }

    /**
     * Nastaví všetky dráhy v systéme z listu z parametra,
     * vymaže ale dráhy, ktoré boli v systéme doteraz!
     *
     * @param entities - dráhy zo súboru
     */
    public void nastavVsetkyDrahyZoSuboru(List<OdletovaDrahaEntity> entities) {
        this.vsetkyOdletoveDrahy = new SplayTree<>();
        for (OdletovaDrahaEntity odletovaDrahaEntity : entities) {
            this.vsetkyOdletoveDrahy.insert(new IntegerKey(odletovaDrahaEntity.getId()), odletovaDrahaEntity);
        }
    }

    /**
     * Pridanie naspäť do prioritného frontu z importu
     *
     * @param naDlzkuDrahy - dĺžka dráhy v ktorej lietadlo bolo vo fronte
     * @param lietadloEntity - samotné lietadlo
     */
    public void zaradLietadloMedziCakajucePriImporteZoSuboru(Integer naDlzkuDrahy, LietadloEntity lietadloEntity) {

        PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity> frontPreDlzku =
                getCakajuceLietadlaNaDanuDlzku(naDlzkuDrahy);
        if (frontPreDlzku == null) {
            frontPreDlzku = new PairingHeap<>();
            this.cakajuceLietadlaPodlaDrah.add(
                    new Pair<Integer, PairingHeap<PrioritaACasPoziadavkyKey, LietadloEntity>>(naDlzkuDrahy, frontPreDlzku));
        }

        PrioritaACasPoziadavkyKey klucPreZaranie =
                new PrioritaACasPoziadavkyKey(lietadloEntity.getPriorita(), lietadloEntity.getCasDatumPoziadaniaOPridelenie());

        QueueNode<PrioritaACasPoziadavkyKey, LietadloEntity> insertedEntity =
                frontPreDlzku.insert(klucPreZaranie, lietadloEntity);

        lietadloEntity.setFrontaNode(insertedEntity);
    }

    /**
     * Vráti IDčka voľných dráh pre uloženie do súboru
     *
     * @return - zoznam IDčiek voľných dráh
     */
    public List<Integer> getVsetkyVolneDrahyPreUlozenieDoSuboru() {
        List<Integer> volneDrahyIds = new LinkedList<>();

        for (Pair<Integer, LinkedList<OdletovaDrahaEntity>> drahyDlzky : this.volneDrahy) {
            for (OdletovaDrahaEntity draha : drahyDlzky.getValue()) {
                volneDrahyIds.add(draha.getId());
            }
        }

        return volneDrahyIds;
    }

    private LinkedList<OdletovaDrahaEntity> getVolneOdletoveDrahyPreDlzku(Integer dlzka) {

        for (Pair<Integer, LinkedList<OdletovaDrahaEntity>> pair : this.volneDrahy) {
            if (pair.getKey().equals(dlzka)) {
                return pair.getValue();
            }
        }

        return null;
    }

    public void pridajVolnuOdletovuDrahuZoSuboru(OdletovaDrahaEntity entity) {

        LinkedList<OdletovaDrahaEntity> volneDrahyDlzky = getVolneOdletoveDrahyPreDlzku(entity.getDlzkaDrahy());

        if (volneDrahyDlzky == null) {
            volneDrahyDlzky = new LinkedList<>();
            this.volneDrahy.add(new Pair<Integer, LinkedList<OdletovaDrahaEntity>>(entity.getDlzkaDrahy(), volneDrahyDlzky));
        }
        volneDrahyDlzky.add(entity);
    }

    /**
     * Nastaví voľné odletové dráhy na dráhy predané zo súboru
     * @param odletoveDrahyIds - ids odletových voľných dráh
     */
    public void nastavVolneOdletoveDrahyZoSuboru(List<Integer> odletoveDrahyIds) {
        this.volneDrahy = new LinkedList<>();
        for (Integer id : odletoveDrahyIds) {
            this.pridajVolnuOdletovuDrahuZoSuboru(this.getOdletovaDraha(id));
        }
    }

    public List<OdletovaDrahaTableUiEntity> getVsetkyOdletoveDrahyVSystemeTableEntities() {
        List<BinarySearchTreeNode<IntegerKey, OdletovaDrahaEntity>> odletoveDrahyNodes = this.vsetkyOdletoveDrahy.levelOrder();
        if (odletoveDrahyNodes == null || odletoveDrahyNodes.size() == 0) {
            return null;
        }
        return this.entityMapper.fromOdletoveDraheToTableUiEntities(odletoveDrahyNodes.stream().map(e -> e.getData()).collect(Collectors.toList()));
    }
}
