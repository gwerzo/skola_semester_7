package sk.uniza.fri.lustyno.datastructures.entity;

/**
 * @author Lukáš Luštiak
 * @param <K> Class of key - has to be able to compare itself against another <K> lustyno.fri.uniza.datastructures.entity
 * @param <D> Class of data of lustyno.fri.uniza.datastructures.entity - can be basically anything that is needed to be stored
 */
public abstract class AbstractKeyValueEntity<K extends HasComparableValue, D> {

    private K key;

    private D data;


    public AbstractKeyValueEntity(K key, D data) {
        this.key = key;
        this.data = data;
    }

    public HasComparableValue getMinValue() {
        return this.key.getMinValue();
    }

    public K getKey() {
        return this.key;
    }

    public D getData() {
        return this.data;
    }

    /**
     * Operácia pre zmenu kľúča entity,
     * bolo by lepšie nejak upraviť architektúru
     * kódu, nech nie je možné len tak mutovať kľúč
     *
     * @param newKey - nová hodnota kľúča
     */
    public void setKey(K newKey) {
        this.key = newKey;
    }
}
