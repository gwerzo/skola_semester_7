﻿namespace FriBettingWebApplication.Services
{
    /// <summary>
    /// Trieda reprezentujúca výsledok validácie
    /// </summary>
    public class ValidationResult
    {
        public bool Successful { get; set; }
        public string ErrorMessage { get; set; }
    }
}