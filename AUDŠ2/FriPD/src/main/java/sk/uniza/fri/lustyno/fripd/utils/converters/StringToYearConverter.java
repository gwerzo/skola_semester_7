package sk.uniza.fri.lustyno.fripd.utils.converters;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

public class StringToYearConverter implements Converter<String, Integer> {

    private static final long serialVersionUID = 1L;

    @Override
    public Result<Integer> convertToModel(String s, ValueContext valueContext) {
        Integer i;
        try {
            i = new Integer(s);
            if (i < 1900) {
                return Result.error("Rok musí byť viac ako rok 1900");
            }
        } catch (Exception e) {
            return Result.error("Nesprávny formát čísla");
        }
        return Result.ok(i);
    }

    @Override
    public String convertToPresentation(Integer integer, ValueContext valueContext) {
        if (integer == null) {
            return "";
        }
        return integer.toString();
    }
}
