package sk.uniza.fri.lustyno.datastructures.exception;

public class DataStructureEntityNotFoundException extends RuntimeException {

    public DataStructureEntityNotFoundException(String clazz, String operation, String keyString) {
        super("V triede " + clazz + " pri použití metódy " + operation + " nebola entita s kľúčom " + keyString + " nájdená");
    }

}
