﻿using Microsoft.AspNetCore.Mvc;
using MemoryGameRestApi.DTO;
using MemoryGameServerApp.Dal;

namespace MemoryGameRestApi.Controllers
{
    public class PlayerController : Controller
    {
        private readonly PlayersDao _playersDao;

        public PlayerController()
        {
            this._playersDao = new PlayersDao();
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost("api/player/register")]
        public IActionResult Register([FromBody] RegisterUserRequestDTO requestDto)
        {
            var registered =
                this._playersDao.RegisterUser(requestDto.Username, requestDto.Password1, requestDto.Password2);

            return Ok(registered);
        }

    }
}
