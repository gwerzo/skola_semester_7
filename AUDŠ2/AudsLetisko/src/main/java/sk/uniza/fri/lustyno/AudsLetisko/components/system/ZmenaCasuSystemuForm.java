package sk.uniza.fri.lustyno.AudsLetisko.components.system;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.ZmenaSystemovehoCasuUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.DateTimeService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;
import sk.uniza.fri.lustyno.AudsLetisko.utils.converters.*;


public class ZmenaCasuSystemuForm extends BusinessComponent {

    private Binder<ZmenaSystemovehoCasuUiEntity> binder;

    private ZmenaSystemovehoCasuUiEntity entity;

    private DateTimeService dateTimeService;

    public ZmenaCasuSystemuForm(RepaintableComponent component) {
        super(component, MENU_TITLE.SYSTEM_MENU_2.getTitle());
        this.dateTimeService = UIServices.getDateTimeService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.binder = new Binder<>(ZmenaSystemovehoCasuUiEntity.class);
        this.entity = new ZmenaSystemovehoCasuUiEntity();

        VerticalLayout layout = new VerticalLayout();

        Label aktualnyCas = new Label("Aktuálny čas je: " + this.dateTimeService.getActualDateTime());

        FormLayout dateLayout = new FormLayout();
        FormLayout timeLayout = new FormLayout();

        TextField den = new TextField("Deň");
        TextField mesiac = new TextField("Mesiac");
        TextField rok = new TextField("Rok");

        TextField hodina = new TextField("Hodina");
        TextField minuta = new TextField("Minuta");


        this.binder.forField(rok)
                .asRequired()
                .withConverter(new StringToYearConverter())
                .bind(ZmenaSystemovehoCasuUiEntity::getRok, ZmenaSystemovehoCasuUiEntity::setRok);

        this.binder.forField(mesiac)
                .asRequired()
                .withConverter(new StringToMonthConverter())
                .bind(ZmenaSystemovehoCasuUiEntity::getMesiac, ZmenaSystemovehoCasuUiEntity::setMesiac);

        this.binder.forField(den)
                .asRequired()
                .withConverter(new StringToDayConverter())
                .bind(ZmenaSystemovehoCasuUiEntity::getDen, ZmenaSystemovehoCasuUiEntity::setDen);

        this.binder.forField(hodina)
                .asRequired()
                .withConverter(new StringToHourConverter())
                .bind(ZmenaSystemovehoCasuUiEntity::getHodina, ZmenaSystemovehoCasuUiEntity::setHodina);

        this.binder.forField(minuta)
                .asRequired()
                .withConverter(new StringToMinuteConverter())
                .bind(ZmenaSystemovehoCasuUiEntity::getMinuta, ZmenaSystemovehoCasuUiEntity::setMinuta);

        Button zmenit = new Button("Zmeniť");

        zmenit.addClickListener(e -> {
           try {
               this.binder.writeBean(this.entity);
               HandledOperationExecutor.submit(() -> {
                   this.dateTimeService.setActualDateTime(this.entity);
                   CustomNotification.notification("Zmena systémového času prebehla v poriadku");
                   showComponent();
                   return OperationMarker.OPERATION_SUCCESFUL;
               });
           } catch (ValidationException ex) {
               CustomNotification.error("Musíte vyplniť správne všetky položky");
           }
        });

        dateLayout.addComponents(
                den,
                mesiac,
                rok
        );

        timeLayout.addComponents(
                hodina,
                minuta,
                zmenit
        );

        layout.addComponent(aktualnyCas);
        layout.addComponent(dateLayout);
        layout.addComponent(timeLayout);

        this.parenComponent.showComponent(new Panel("Zmena systémového dátumu a času", layout));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_2.getTitle();
    }
}
