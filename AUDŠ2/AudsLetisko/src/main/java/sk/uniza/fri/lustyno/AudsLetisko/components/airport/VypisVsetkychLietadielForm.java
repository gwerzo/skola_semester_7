package sk.uniza.fri.lustyno.AudsLetisko.components.airport;

import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.LietadloTableUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.service.AircraftManagementService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;

import java.util.List;

public class VypisVsetkychLietadielForm extends BusinessComponent {

    private AircraftManagementService aircraftManagementService;

    public VypisVsetkychLietadielForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_6.getTitle());
        this.aircraftManagementService = UIServices.getAircraftManagementService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        VerticalLayout layout = new VerticalLayout();

        layout.setSizeFull();
        Grid<LietadloTableUiEntity> grid = new Grid<>();
        grid.addColumn(LietadloTableUiEntity::getMedzinarodnyKod).setCaption("Medzinárodný kód");
        grid.addColumn(LietadloTableUiEntity::getVyrobca).setCaption("Výrobca");
        grid.addColumn(LietadloTableUiEntity::getPriorita).setCaption("Priorita");
        grid.addColumn(LietadloTableUiEntity::getMinDlzkaDrahy).setCaption("Požadovaná dĺžka dráhy");
        grid.addColumn(LietadloTableUiEntity::getCasPriletu).setCaption("Čas príletu");
        grid.addColumn(LietadloTableUiEntity::getCasPoziadavkyOPridelenie).setCaption("Čas požiadavky o pridelenie");

        grid.setSizeFull();

        List<LietadloTableUiEntity> cakajuceLietadla = aircraftManagementService.getAllCakajuceLietadla();
        if (cakajuceLietadla.size() != 0) {
            grid.setItems(cakajuceLietadla);
            layout.addComponent(grid);
        } else {
            layout.addComponent(new Label("Žiadne lietadlo nečaká na odlet"));
        }

        this.parenComponent.showComponent(new Panel("Lietadlá čakajúce na pridelenie odletovej dráhy", layout));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_6.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
