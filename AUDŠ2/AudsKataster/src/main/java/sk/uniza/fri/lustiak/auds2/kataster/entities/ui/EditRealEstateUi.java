package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditRealEstateUi {

    private Boolean editConstraints;
    private RealEstateUiTableEntity oldRealEstateData;
    private RealEstateUiTableEntity newRealEstateData;

}
