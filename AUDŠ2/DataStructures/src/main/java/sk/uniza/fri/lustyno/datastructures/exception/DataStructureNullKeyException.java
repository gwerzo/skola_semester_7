package sk.uniza.fri.lustyno.datastructures.exception;

public class DataStructureNullKeyException extends RuntimeException {

    public DataStructureNullKeyException(String className, String methodName) {
        super("Cannot use key that is null in class " + className + " in method " + methodName);
    }

}
