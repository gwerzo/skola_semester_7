package sk.uniza.fri.lustyno.fripd.components.common;

import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class CardsManagementComponent extends VerticalLayout {

    public CardsManagementComponent() {
        initComponents();
    }

    private void initComponents() {

        Label lMain = new Label("Funkcionalita manažmentu preukazov:");

        Label lEmpty = new Label();

        Label l1 = new Label("1. ***");

        l1.setWidth("100%");
        lMain.setWidth("100%");
        lEmpty.setWidth("100%");

        VerticalLayout labelsWrapper = new VerticalLayout(
                lMain,
                lEmpty,
                l1);

        Panel descPanel = new Panel(
                "Manažment preukazov",
                labelsWrapper
        );

        addComponent(descPanel);
    }
}
