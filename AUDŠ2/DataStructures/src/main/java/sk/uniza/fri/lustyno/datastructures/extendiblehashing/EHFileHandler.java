package sk.uniza.fri.lustyno.datastructures.extendiblehashing;

import sk.uniza.fri.lustyno.datastructures.exception.DataStructureFlushFailedException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureInitializationException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureOperationFailedException;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

public class EHFileHandler {

    private RandomAccessFile mainFileRaf;
    private RandomAccessFile overflowFileRaf;

    private String databaseName;

    public EHFileHandler(String databaseName, Boolean startWithEmptyFile) {
        this.databaseName = databaseName;

        String mainFileName = databaseName + Constants.DB_FILE_POSTFIX;
        String overflowFileName = databaseName + "_overflow" + Constants.DB_FILE_POSTFIX;

        try {

            this.mainFileRaf = new RandomAccessFile(mainFileName, "rw");
            this.overflowFileRaf = new RandomAccessFile(overflowFileName, "rw");

            // Vyčistí súbor s daným menom (zahodí z neho staré dáta, ak tam boli)
            if (startWithEmptyFile) {
                this.mainFileRaf.setLength(0L);
                this.overflowFileRaf.setLength(0L);
            }

        } catch (IOException e) {
            throw new DataStructureInitializationException(this.getClass().getName(),
                    "Nepodarilo sa inicializovať súbory pre novú štruktúru");
        }
    }

    /**
     * Prečíta z hlavného súboru pole bajtov
     * @param offsetInFile - offset skade začať čítať
     * @param numberOfBytes - počet bajtov na prečítanie
     * @return
     */
    public byte[] readFromMainFile(Integer offsetInFile, Integer numberOfBytes) {
        try {
            byte[] bb = ByteBuffer.allocate(numberOfBytes).array();
            this.mainFileRaf.seek(offsetInFile);
            this.mainFileRaf.read(bb, 0, bb.length);
            return bb;
        } catch (IOException e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "readFromMainFile", e.getMessage());
        }
    }

    /**
     * Zapíše pole bajtov do hlavného súboru
     * @param bytes - pole bajtov
     * @param offsetInFile - offset odkiaľ začať zapisovanie
     */
    public void writeToMainFile(byte[] bytes, Integer offsetInFile) {
        try {
            this.mainFileRaf.seek(offsetInFile);
            this.mainFileRaf.write(bytes, 0, bytes.length);
        } catch (IOException e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "writeToMainFile", e.getMessage());
        }
    }

    /**
     * Prečíta z overflow súboru pole bajtov
     * @param offsetInFile - offset skade začať čítať
     * @param numberOfBytes - počet bajtov na prečítanie
     * @return
     */
    public byte[] readFromOverflowFile(Integer offsetInFile, Integer numberOfBytes) {
        try {
            byte[] bb = ByteBuffer.allocate(numberOfBytes).array();
            this.overflowFileRaf.seek(offsetInFile);
            this.overflowFileRaf.read(bb, 0, bb.length);
            return bb;
        } catch (IOException e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "readFromOverflowFile", e.getMessage());
        }
    }

    /**
     * Zapíše pole bajtov do overflow súboru
     * @param bytes - pole bajtov
     * @param offsetInFile - offset odkiaľ začať zapisovanie
     */
    public void writeToOverflowFile(byte[] bytes, Integer offsetInFile) {
        try {
            this.overflowFileRaf.seek(offsetInFile);
            this.overflowFileRaf.write(bytes, 0, bytes.length);
        } catch (IOException e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "writeToOverflowFile", e.getMessage());
        }
    }

    /**
     * Zníži veľkosť overflow súboru o počet bytov predaný z parametra
     * @param numberOfBytesToTrim
     */
    public void trimOverflowFile(Integer numberOfBytesToTrim) {
        try {
            this.overflowFileRaf.setLength(this.overflowFileRaf.length() - numberOfBytesToTrim);
        } catch (Exception e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "trimOverflowFile", e.getMessage());
        }
    }

    /**
     * Zníži veľkosť hlavného súboru o počet bytov predaný z parametra
     * @param numberOfBytesToTrim
     */
    public void trimMainFile(Integer numberOfBytesToTrim) {
        try {
            this.mainFileRaf.setLength(this.mainFileRaf.length() - numberOfBytesToTrim);
        } catch (Exception e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "trimMainFile", e.getMessage());
        }
    }

    public void closeFiles() {
        try {
            this.mainFileRaf.close();
            this.overflowFileRaf.close();
        } catch (IOException e) {
            throw new DataStructureFlushFailedException(this.databaseName);
        }
    }

    public Long getMainFileSize() {
        try {
            return this.mainFileRaf.length();
        } catch (IOException e) {
            System.out.println("Nepodarilo sa získať veľkosť hlavného súboru");
            e.printStackTrace();
        }
        return 0L;
    }

    public Long getOverflowFileSize() {
        try {
            return this.overflowFileRaf.length();
        } catch (IOException e) {
            System.out.println("Nepodarilo sa získať veľkosť overflow súboru");
            e.printStackTrace();
        }
        return 0L;
    }

}
