package linearhashing.entities;

import sk.uniza.fri.lustyno.datastructures.entity.keys.DoubleStringKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Record;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.HasSize;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.StringUtils;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

@HasSize(sizeInBytes = 46)
public class CarHashingEntity extends Record<DoubleStringKey> {

    /**
     * VIN Kód
     */
    private static final Integer VIN_KOD_DLZKA = 17;
    private String VIN_KOD;
    /***/

    /**
     * ŠPZ auta
     */
    private static final Integer SPZ_DLZKA = 7;
    private String SPZ;
    /***/

    /**
     * Počet náprav auta
     */
    private Integer pocetNaprav;

    /**
     * Je auto v pátraní
     */
    private Boolean vPatrani;

    /**
     * Dátum konca platnosti STK
     */
    private LocalDateTime datumKoncaPlatnostiSTK;

    /**
     * Dátum konca platnosti EK
     */
    private LocalDateTime datumKoncaPlatnostiEK;

    public CarHashingEntity() {

    }

    public CarHashingEntity(String VIN_KOD, String SPZ,
                            Integer pocetNaprav, Boolean vPatrani,
                            LocalDateTime datumKoncaPlatnostiSTK, LocalDateTime datumKoncaPlatnostiEK) {
        this.VIN_KOD = StringUtils.padRightDollars(VIN_KOD, VIN_KOD_DLZKA);
        this.SPZ = StringUtils.padRightDollars(SPZ, SPZ_DLZKA);
        this.pocetNaprav = pocetNaprav;
        this.vPatrani = vPatrani;
        this.datumKoncaPlatnostiSTK = datumKoncaPlatnostiSTK;
        this.datumKoncaPlatnostiEK = datumKoncaPlatnostiEK;
    }

    @Override
    public byte[] toBytesArray() {
        byte[] bytes = ToByteArrayConverter.concatenateByteArrays(
                Arrays.asList(
                        ToByteArrayConverter.bytesFromBoolean(this.valid),
                        ToByteArrayConverter.bytesFromString(VIN_KOD),
                        ToByteArrayConverter.bytesFromString(SPZ),
                        ToByteArrayConverter.bytesFromInt(this.pocetNaprav),
                        ToByteArrayConverter.bytesFromBoolean(this.vPatrani),
                        ToByteArrayConverter.bytesFromLocalDateTime(this.datumKoncaPlatnostiSTK),
                        ToByteArrayConverter.bytesFromLocalDateTime(this.datumKoncaPlatnostiEK)));
        return bytes;

    }

    public String getDatumKoncaPlatnostiSTK() {
        return this.datumKoncaPlatnostiSTK.toString();
    }

    public String getDatumKoncaPlatnostiEK() {
        return this.datumKoncaPlatnostiEK.toString();
    }

    public static CarHashingEntity fromByteArray(byte[] bytes) {
        CarHashingEntity che = new CarHashingEntity();
        che.valid = FromByteArrayConverter.booleanFromByte(bytes[0]);
        if (!che.valid) {
            return emptyInvalidInstance();
        }
        che.VIN_KOD = FromByteArrayConverter.stringFromBytes(Arrays.copyOfRange(bytes, 1, 18));
        che.SPZ = FromByteArrayConverter.stringFromBytes(Arrays.copyOfRange(bytes, 18, 25));
        che.pocetNaprav = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(bytes, 25, 29));
        che.vPatrani = FromByteArrayConverter.booleanFromByte(bytes[29]);
        che.datumKoncaPlatnostiSTK = FromByteArrayConverter.localDateTimeFromBytes(Arrays.copyOfRange(bytes, 30, 38));
        che.datumKoncaPlatnostiEK = FromByteArrayConverter.localDateTimeFromBytes(Arrays.copyOfRange(bytes, 38, 46));
        return che;
    }

    public static CarHashingEntity emptyInvalidInstance() {
        return new CarHashingEntity(
                "", "", 0, false, LocalDateTime.now(), LocalDateTime.now());
    }

    @Override
    public int getHash() {
        int hash = 7;
        int cc = CarHashingEntity.SPZ_DLZKA;
        for (int i = 0; i < this.VIN_KOD.length(); i++) {
            hash = hash*31 + this.VIN_KOD.charAt(i) + this.SPZ.charAt(i % cc);
        }

        return Math.abs(hash);
    }

    @Override
    public DoubleStringKey getKey() {
        return new DoubleStringKey(this.VIN_KOD, this.SPZ);
    }


    public static String randomVIN() {
        String VIN_VALS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 17; i++) {
            sb.append(VIN_VALS.charAt(rnd.nextInt(VIN_VALS.length())));
        }
        return sb.toString();
    }

    public static String randomSPZ() {
        String SPZ_VALS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 7; i++) {
            sb.append(SPZ_VALS.charAt(rnd.nextInt(SPZ_VALS.length())));
        }
        return sb.toString();
    }

    public String getVIN_KOD() {
        return this.VIN_KOD;
    }

}
