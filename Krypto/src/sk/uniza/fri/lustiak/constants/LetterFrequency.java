package sk.uniza.fri.lustiak.constants;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LetterFrequency {

    public static Map<Character, Double> SVK_FREQUENCIES = new HashMap<>();
    public static Map<Character, Double> ENG_FREQUENCIES = new HashMap<>();

    static {
        initSvkFrequencies();
        initEngFrequencies();
    }

    private static void initSvkFrequencies() {
        SVK_FREQUENCIES.put('A', 0.11160);
        SVK_FREQUENCIES.put('B', 0.01778);
        SVK_FREQUENCIES.put('C', 0.02463);
        SVK_FREQUENCIES.put('D', 0.03760);
        SVK_FREQUENCIES.put('E', 0.09316);
        SVK_FREQUENCIES.put('F', 0.00165);
        SVK_FREQUENCIES.put('G', 0.00175);
        SVK_FREQUENCIES.put('H', 0.02482);
        SVK_FREQUENCIES.put('I', 0.05745);
        SVK_FREQUENCIES.put('J', 0.02158);
        SVK_FREQUENCIES.put('K', 0.03961);
        SVK_FREQUENCIES.put('L', 0.04375);
        SVK_FREQUENCIES.put('M', 0.03578);
        SVK_FREQUENCIES.put('N', 0.05949);
        SVK_FREQUENCIES.put('O', 0.09540);
        SVK_FREQUENCIES.put('P', 0.03007);
        SVK_FREQUENCIES.put('Q', 0.00000);
        SVK_FREQUENCIES.put('R', 0.04706);
        SVK_FREQUENCIES.put('S', 0.06121);
        SVK_FREQUENCIES.put('T', 0.05722);
        SVK_FREQUENCIES.put('U', 0.03308);
        SVK_FREQUENCIES.put('V', 0.04604);
        SVK_FREQUENCIES.put('W', 0.00001);
        SVK_FREQUENCIES.put('X', 0.00028);
        SVK_FREQUENCIES.put('Y', 0.02674);
        SVK_FREQUENCIES.put('Z', 0.03064);
        runProbabilityTest(SVK_FREQUENCIES);
    }

    private static void initEngFrequencies() {
        ENG_FREQUENCIES.put('A', 0.08167);
        ENG_FREQUENCIES.put('B', 0.01492);
        ENG_FREQUENCIES.put('C', 0.02782);
        ENG_FREQUENCIES.put('D', 0.04253);
        ENG_FREQUENCIES.put('E', 0.12702);
        ENG_FREQUENCIES.put('F', 0.02228);
        ENG_FREQUENCIES.put('G', 0.02015);
        ENG_FREQUENCIES.put('H', 0.06094);
        ENG_FREQUENCIES.put('I', 0.06966);
        ENG_FREQUENCIES.put('J', 0.00153);
        ENG_FREQUENCIES.put('K', 0.00772);
        ENG_FREQUENCIES.put('L', 0.04025);
        ENG_FREQUENCIES.put('M', 0.02406);
        ENG_FREQUENCIES.put('N', 0.06769);
        ENG_FREQUENCIES.put('O', 0.07507);
        ENG_FREQUENCIES.put('P', 0.01929);
        ENG_FREQUENCIES.put('Q', 0.00095);
        ENG_FREQUENCIES.put('R', 0.05987);
        ENG_FREQUENCIES.put('S', 0.06327);
        ENG_FREQUENCIES.put('T', 0.09056);
        ENG_FREQUENCIES.put('U', 0.02758);
        ENG_FREQUENCIES.put('V', 0.00978);
        ENG_FREQUENCIES.put('W', 0.02360);
        ENG_FREQUENCIES.put('X', 0.00150);
        ENG_FREQUENCIES.put('Y', 0.01974);
        ENG_FREQUENCIES.put('Z', 0.00074);
        runProbabilityTest(ENG_FREQUENCIES);
    }

    private static void runProbabilityTest(Map<Character, Double> frequencies) {
        Double cumulativeProbablity = .0;
        Iterator<Map.Entry<Character, Double>> iterator = frequencies.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Character, Double> entry = iterator.next();
            cumulativeProbablity += entry.getValue();
        }

        if (cumulativeProbablity > 1.01 || cumulativeProbablity < 0.99) {
            throw new RuntimeException(
                    "Frequency probabilities are wrong!\n" +
                    "Cumulative probability is: " + cumulativeProbablity);
        }
    }

}
