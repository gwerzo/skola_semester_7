package sk.uniza.fri.lustyno.fripd.codelists;

public class Constants {

    public static final Integer VIN_CODE_LENGTH             = 17;

    public static final Integer EVC_CODE_LENGTH             = 7;

}
