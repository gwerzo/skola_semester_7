﻿namespace FriBettingWebApplication.Dto.Response
{
    public class ClosedTicketDetailResponse
    {
        public int TicketId { get; set; }
        public int EvaluatedMatches { get; set; }
        public int NotEvaluatedMatches { get; set; }
        public double TotalOdds { get; set; }
        public double WinPotential { get; set; }

    }
}