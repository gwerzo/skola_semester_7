﻿using System.Collections.Generic;
using System.Linq;
using FriBettingDatabaseApp.Repository.Base;
using FriBettingWebApplication.Dto.Response;
using FriBettingWebApplication.Services.Face;

namespace FriBettingWebApplication.Services
{
    public class CompetitorService: ICompetitorService
    {
        private ICompetitorRepository _competitorRepository;

        public CompetitorService(ICompetitorRepository competitorRepository)
        {
            _competitorRepository = competitorRepository;
        }

        public List<CompetitorResponse> GetCompetitorsCompetingAtSport(int sportId)
        {

            return _competitorRepository.GetAllCompetitorsCompetingInSport(sportId).Select(e => new CompetitorResponse
            {
                CompetitorId = e.CompetitorId,
                CompetitorName = e.Name
            }).ToList();
        }
    }
}