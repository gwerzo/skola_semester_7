package sk.uniza.fri.lustiak.auds2.kataster.components.common;


import sk.uniza.fri.lustiak.auds2.kataster.components.kataster.*;

public class MenuComponent extends AbstractMenu {

    public MenuComponent(RepaintableComponent component) {
        this.menuItems.add(new VyhladanieNehnutelnostiPodlaIDForm(component));
        this.menuItems.add(new PridanieNehnutelnostiForm(component));
        this.menuItems.add(new VyradenieNehnutelnostiForm(component));
        this.menuItems.add(new EditaciaNehnutelnostiForm(component));
        initMenuItems();
    }

}
