package sk.uniza.fri.lustyno.datastructures.extendiblehashing.entities;

public interface IHasSize {

    int getSize();

}
