﻿function getAllSports(callback) {
    $.ajax({
        url: '/api/sports/list',
        headers: {
            'Authorization': localStorage.getItem("fri-betting-app-token")
        },
        method: 'GET',
        success: function (data) {
            callback(data);
        },
        error: function (jqXHR, textStatus, err) {
            $.notify("Nastal problém v spojení so serverom", "error");
        }
    });
}