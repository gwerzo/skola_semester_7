package sk.uniza.fri.lustyno.fripd.service;

import com.vaadin.spring.server.SpringVaadinServlet;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;

@Service
public class UIServices {

    public static VehicleService getVehicleService(){
        return getApplicationContext().getBean(VehicleService.class);
    }

    public static DrivingLicenseService getDrivingLicenseService(){
        return getApplicationContext().getBean(DrivingLicenseService.class);
    }


    public static ApplicationContext getApplicationContext() {
        ServletContext servletContext = SpringVaadinServlet.getCurrent().getServletContext();
        return WebApplicationContextUtils.getWebApplicationContext(servletContext);
    }

}
