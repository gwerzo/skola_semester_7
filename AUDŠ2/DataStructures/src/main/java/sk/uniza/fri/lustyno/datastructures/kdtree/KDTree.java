package sk.uniza.fri.lustyno.datastructures.kdtree;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValueOnDimension;
import sk.uniza.fri.lustyno.datastructures.entity.keys.UniquelyIdentifiable;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureEntityNotFoundException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureInsertDuplicateException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureNullKeyException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureWrongUsageException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class KDTree<K extends HasComparableValueOnDimension<V>, D, V extends UniquelyIdentifiable<V>> {

    private KDTreeNode<K,D,V> root;

    private int ID_SEQUENCER;

    private int dimensions;

    public KDTree(int depth) {
        this.ID_SEQUENCER = 0;
        this.dimensions = depth;
    }

    private int nextInIDSequence() {
        return ++ID_SEQUENCER;
    }

    /**
     * Find v KDTree vracajúci inštanciu triedy
     * {@link KDTreeNodeKeyDataWrapper
     * z toho dôvodu, že kľúč typu
     * {@link K}}
     * je vstupným parametrom, ale nie je súčasťou dát,
     * kvôli zbytočnej redundancii dát,
     * avšak "wrapper" trieda spomenutá vyššie
     * zaobalí obe hodnoty ({@link K} a {@link D})
     * do jednej triedy a vráti ju
     *
     * @param key - Vstupný parameter pre nájdenie dátovej časti podľa predaného kľúča
     * @return - inštanciu triedy {@link KDTreeNodeKeyDataWrapper <K,D>} zaobaľujúcu hodnotu kľúča a hodnotu dát
     */
    public KDTreeNodeKeyDataWrapper<K,D> find(K key) {
        KDTreeNode<K,D,V> foundNode = this.findInternally(key, true);
        return new KDTreeNodeKeyDataWrapper<>(foundNode.getKey(), foundNode.getData());
    }

    /**
     * Find v KDTree vracajúci nájdené nody
     * pričom neskontroluje unikátnosť kľúča,
     * overia sa iba dimenzie kľúča
     * a vráti takto nájdený node,
     * resp. aj pribalených "bratov" nodu
     * {@link KDTreeNode#getBrothers()}
     *
     * @param key - dimenzie kľúča
     * @return - zoznam takto nájdených nodov
     */
    public List<KDTreeNodeKeyDataWrapper<K,D>> findByDimensionalKey(K key) {
        List<KDTreeNodeKeyDataWrapper<K,D>> toRet = new LinkedList<>();
        KDTreeNode<K,D,V> foundNode = this.findInternally(key, false);
        if (foundNode == null) {
            return new LinkedList<>();
        }
        toRet.add(new KDTreeNodeKeyDataWrapper<>(foundNode.getKey(), foundNode.getData()));
        if(foundNode.getBrothers() != null) {
            foundNode.getBrothers().forEach(e -> {
                toRet.add(new KDTreeNodeKeyDataWrapper<>(e.getKey(), e.getData()));
            });
        }
        return toRet;
    }

    /**
     * Vráti množinu wrapper tried
     * {@link KDTreeNodeKeyDataWrapper <K,D>}
     * podľa predaných parametrov,
     * ktoré symbolizujú najnižšiu a najvyššiu hodnotu (VRÁTANE!)
     * intervalu kľúča, z ktorého je požiadavka vrátiť dáta
     *
     * @param fromKey - Spodná hodnota intervalu kľúča
     * @param toKey - Vrchná hodnota intervalu kľúča
     * @return - Zoznam ({@link List}) takto nájdených hodnôt
     */
    public List<KDTreeNodeKeyDataWrapper<K,D>> findInRange(K fromKey, K toKey) {

        if (this.root == null) {
            return new LinkedList<>();
        }

        List<KDTreeNode<K,D,V>> foundNodes = new LinkedList<>();

        LinkedList<KDTreeNode<K,D,V>> queue = new LinkedList<>();
        queue.add(this.root);
        while (!queue.isEmpty()) {
            KDTreeNode<K,D,V> node = queue.removeFirst();

            boolean actualNodeIsInRange = true;
            for (int i = 0; i < this.dimensions; i++) {
                if (!this.keyBelongsToInterval(node.getKey(), fromKey, toKey, i)) {
                    actualNodeIsInRange = false;
                    break;
                }
            }

            if (actualNodeIsInRange) {
                foundNodes.add(node);
                if (node.getBrothers() != null && !node.getBrothers().isEmpty()) {
                    foundNodes.addAll(node.getBrothers());
                }
            }

            // Splitter na aktuálnej dimenzii, ktorú node rozdeľuje
            int comparedToRange = this.keyCompareToRangeOnDimension(node.getKey(), fromKey, toKey, node.getDepthModulo());

            // Ak node je uprostred range z parametrov, pridám na spracovanie aj jeho ľavého aj pravého syna (iba not-null)
            if (comparedToRange == 0) {
                if (node.getLeftChild() != null) {
                    queue.add(node.getLeftChild());
                }

                if (node.getRightChild() != null) {
                    queue.add(node.getRightChild());
                }
            }

            // Ak node je mimo intervalu z "ľavej strany" (menší ako spodná časť intervalu), na spracovanie sa pridá jeho pravý syn
            if (comparedToRange < 0) {
                if (node.getRightChild() != null) {
                    queue.add(node.getRightChild());
                }
            }

            // Ak node je mimo intervalu z "pravej strany" (väčší ako horná časť intervalu), na spracovanie sa pridá jeho ľavý syn
            if (comparedToRange > 0) {
                if (node.getLeftChild() != null) {
                    queue.add(node.getLeftChild());
                }
            }

        }
        return foundNodes.stream().map(e -> new KDTreeNodeKeyDataWrapper<>(e.getKey(), e.getData())).collect(Collectors.toList());
    }

    /**
     * Vloží do štruktúry novú entitu podľa
     * predaných argumentov, ktoré predstavujú
     * kľúč a dáta vkladanej entity
     *
     * @param key <b>NOT NULL</b> Kľúč vkladanej entity
     * @param data
     *         Dáta vkladanej entity
     *
     * @throws DataStructureNullKeyException - ak je do metódy poslaný parameter kľúča s NULL hodnotou
     */
    public void insert(K key, D data) {
        if (key == null) {
            throw new DataStructureNullKeyException(this.getClass().getName(), "insert");
        }

        if (this.root == null) {
            this.root = new KDTreeNode<>(key, data, 0);
            this.root.getKey().getValue().setUniqueIdIfEmpty(this.ID_SEQUENCER);
            return;
        }

        int actualDepthDimension = 0;
        KDTreeNode<K,D,V> actualNode = this.root;
        KDTreeNode<K,D,V> newNode = new KDTreeNode<>(key, data, 0);

        insertNodeZigZagger(newNode, actualDepthDimension, actualNode);
    }


    /**
     * Pokúsi sa vymazať entitu podľa predaného kľúča,
     * ak sa to poradí, vráti kľúč spolu s dátami v entite
     * {@link KDTreeNodeKeyDataWrapper<K,D>}
     *
     * @param key - Kľúč, podľa ktorého sa má vymazať entita
     * @return
     *          <p>NULL</p> ak sa entitu nepodarilo vymazať
     *          {@link KDTreeNodeKeyDataWrapper<K,D>} ak sa ju podarilo vymazať (obsahuje kľúč a dáta vymazanej entity)
     */
    public KDTreeNodeKeyDataWrapper<K,D> delete(K key) {
        KDTreeNode<K,D,V> toDeleteNode = this.findInternally(key, true);
        if (toDeleteNode == null) {
            throw new DataStructureEntityNotFoundException(this.getClass().getName(), "delete", key.toString());
        }

        // Pre prípad, že node je rootom a nemá žiadnych potomkov
        if (toDeleteNode.isRoot() && !toDeleteNode.hasRightChild() && !toDeleteNode.hasLeftChild() && toDeleteNode.getSuperBrother() == null) {
            if (this.root.getBrothers().size() > 0) {
                KDTreeNode<K,D,V> replacer = this.root.getBrothers().remove(0);
                this.replaceNodeWithNode(this.root, replacer);
                this.root.setBrothers(replacer.getBrothers());
                return new KDTreeNodeKeyDataWrapper<>(toDeleteNode.getKey(), toDeleteNode.getData());
            }
            this.root = null;
            return new KDTreeNodeKeyDataWrapper<>(toDeleteNode.getKey(), toDeleteNode.getData());
        }

        if (toDeleteNode.isRoot() && toDeleteNode.getBrothers().size() > 0) {
            KDTreeNode<K,D,V> replacer = this.root.getBrothers().remove(0);
            replacer.setDepthModulo(this.root.getDepthModulo());
            this.replaceNodeWithNode(this.root, replacer);
            // Special case s rootom a bratmi
            this.root.setBrothers(replacer.getBrothers());
            if (toDeleteNode.getRightChild() != null)
                toDeleteNode.getRightChild().setParent(this.root);
            if (toDeleteNode.getLeftChild() != null)
                toDeleteNode.getLeftChild().setParent(this.root);
            return new KDTreeNodeKeyDataWrapper<>(replacer.getKey(), replacer.getData());
        }

        if (toDeleteNode.isLeaf() && toDeleteNode.getSuperBrother() == null) {
            if (toDeleteNode.getBrothers().size() > 0) {
                KDTreeNode<K,D,V> replacer = toDeleteNode.getBrothers().remove(0);
                this.replaceNodeWithNode(toDeleteNode, replacer);
                toDeleteNode.setBrothers(replacer.getBrothers());
                return new KDTreeNodeKeyDataWrapper<>(replacer.getKey(), replacer.getData());
            }
            toDeleteNode.removeItselfFromParent();
            return new KDTreeNodeKeyDataWrapper<>(toDeleteNode.getKey(), toDeleteNode.getData());
        }

        // Ak je node súčasťou "bratstva", iba sa z neho vyhodí, ak je však "superBratom", treba ho nahradiť prvým možným bratom
        if (toDeleteNode.getSuperBrother() != null) {
            toDeleteNode.getSuperBrother().removeBrother(toDeleteNode);
            return new KDTreeNodeKeyDataWrapper<>(toDeleteNode.getKey(), toDeleteNode.getData());
        } else if (toDeleteNode.getBrothers() != null && toDeleteNode.getBrothers().size() != 0) {
            KDTreeNode<K,D,V> replacer = toDeleteNode.getBrothers().remove(0);
            replacer.setBrothers(toDeleteNode.getBrothers());
            replacer.setParent(toDeleteNode.getParent());
            replacer.setSuperBrother(null);
            if (toDeleteNode.isRoot()) {
                // Omit this case
            } else if (toDeleteNode.isRightChildOfParent()) {
                toDeleteNode.getParent().setRightChild(replacer);
            } else {
                toDeleteNode.getParent().setLeftChild(replacer);
            }
            replacer.setLeftChild(toDeleteNode.getLeftChild());
            replacer.setRightChild(toDeleteNode.getRightChild());
            if (toDeleteNode.getRightChild() != null)
                toDeleteNode.getRightChild().setParent(replacer);
            if (toDeleteNode.getLeftChild() != null)
                toDeleteNode.getLeftChild().setParent(replacer);
            return new KDTreeNodeKeyDataWrapper<>(toDeleteNode.getKey(), toDeleteNode.getData());
        }

        // Niektoré nody sa musia "vyseknúť" zo stromu z problému rovnakej hodnoty kľúča na dimenzii,
        // sú preto zo stromu "vyseknuté" a po operácii vymazania naspäť vložené do stromu,
        // čo zaručí, že sa neporuší logická štruktúra stromu
        LinkedList<KDTreeNode<K,D,V>> markedNodes = new LinkedList<>();

        // Kým sa mi vymazávaný node nepodarí dostať do listu, vymieňam ho za minimum/maximum z jeho podstromov podľa potreby
        KDTreeNode<K,D,V> removed = removeNodeDownwards(toDeleteNode, markedNodes, true);
        return new KDTreeNodeKeyDataWrapper<>(removed.getKey(), removed.getData());
    }

    /**
     * Nájde node podľa predaného kľúča,
     * ale len pre interné využitie štruktúry
     *
     * Pri hľadaní sa použije UNIKÁTNA ČASŤ kľúča
     * {@link UniquelyIdentifiable}
     * ak je hodnota parametra findByUniqeness = true
     *
     * @param key - Kľúč pre nájdenie
     * @param findByUniqueness - či node vyhľadávam podľa unikátnosti, alebo podľa dimenzií
     * @return - Nájdený node, resp. <b>NULL</b> ak sa entita nenašla podľa predného kľúča
     */
    protected KDTreeNode<K,D,V> findInternally(K key, boolean findByUniqueness) {
        if (this.root == null) {
            return null;
        }
        KDTreeNode<K,D,V> actualNode = this.root;
        int actualDimension = 0;

        while (actualNode != null) {
            if (findByUniqueness) {
                if (actualNode.getKey().getValue().equalsUniquely(key.getValue())) {
                    return actualNode;
                }
            } else {
                if (actualNode.getKey().equalsOnAllDimensions(key)) {
                    return actualNode;
                }
            }

            // Nájdenie node-u v bratoch (rovnaké hodnoty na každej dimenzii kľúča, iba iná unikátna časť kľúča)
            if (actualNode.getBrothers().size() != 0) {
                for (KDTreeNode<K,D,V> brother : actualNode.getBrothers()) {
                    if (brother.getKey().getValue().equalsUniquely(key.getValue())) {
                        return brother;
                    }
                }
            }

            if (key.compareToOnDimension(actualNode.getKey(), actualDimension % this.dimensions) > 0) {
                actualNode = actualNode.getRightChild();
            } else {
                actualNode = actualNode.getLeftChild();
            }

            actualDimension++;
        }

        return null;
    }

    /**
     * Insertnutie nodu už vytvoreného (ide využiť ako vloženie stromu do stromu)
     * @param newNode
     */
    private void insertNode(KDTreeNode<K,D,V> newNode) {
        if (newNode == null) {
            throw new DataStructureNullKeyException(this.getClass().getName(), "insertNode");
        }

        if (this.root == null) {
            this.root = newNode;
            return;
        }

        int actualDepthDimension = 0;
        KDTreeNode<K,D,V> actualNode = this.root;

        insertNodeZigZagger(newNode, actualDepthDimension, actualNode);
    }

    /**
     * ZigZag metóda, ktorá hľadá miesto pre node v strome
     * podľa porovnávania hodnoty dimenzie kľúča
     * smerom nadol, pričom
     * väčšia hodnota kľúča -> pravý podstrom
     * menšia/rovná hodnota kľúča -> ľavý podstrom
     *
     * @param newNode
     * @param actualDepthDimension
     * @param actualNode
     */
    private void insertNodeZigZagger(KDTreeNode<K, D, V> newNode, int actualDepthDimension, KDTreeNode<K, D, V> actualNode) {
        while (true) {
            int actualDepthDimensionModulo = actualDepthDimension % this.dimensions;
            int comparisonOnDimension = actualNode.getKey().compareToOnDimension(newNode.getKey(), actualDepthDimensionModulo);

            // Prípad, kedy sa aktuálny node rovná na všetkých dimenziách, vložíme nový node do jeho bratov
            if (actualNode.getKey().equalsOnAllDimensions(newNode.getKey())) {

                // Nepovolím vložiť rovnaký node, s rovnakými dimenziami a aj rovnakým UNIQUE ID (ak ho má vkladaný nastavený)
                actualNode.getBrothers().forEach(e -> {
                    if (e.getKey().getValue().equalsUniquely(newNode.getKey().getValue())) {
                        throw new DataStructureInsertDuplicateException(
                                this.getClass().getName(),
                                newNode.getKey().toString(),
                                newNode.getData().toString());
                    }
                });
                newNode.getKey().getValue().setUniqueIdIfEmpty(this.nextInIDSequence());
                newNode.setDepthModulo(actualNode.getDepthModulo());
                newNode.setParent(actualNode.getParent());
                newNode.setSuperBrother(actualNode);
                actualNode.addBrother(newNode);
                return;
            }

            if (comparisonOnDimension >= 0) {
                if (actualNode.getLeftChild() == null) {
                    actualNode.setLeftChild(newNode);
                    newNode.setParent(actualNode);
                    newNode.setDepthModulo(++actualDepthDimensionModulo % this.dimensions);
                    newNode.getKey().getValue().setUniqueIdIfEmpty(this.nextInIDSequence());
                    return;
                } else {
                    actualNode = actualNode.getLeftChild();
                }
            } else {
                if (actualNode.getRightChild() == null) {
                    actualNode.setRightChild(newNode);
                    newNode.setParent(actualNode);
                    newNode.setDepthModulo(++actualDepthDimensionModulo % this.dimensions);
                    newNode.getKey().getValue().setUniqueIdIfEmpty(this.nextInIDSequence());
                    return;
                } else {
                    actualNode = actualNode.getRightChild();
                }
            }
            actualDepthDimension++;
        }
    }

    /**
     * Pri mazaní nodu zo stromu sa môžu niektoré ďalšie nody označiť ako "marked",
     * pretože pri výbere minima/maxima kolidovali s nejakým iným nodeom
     *
     * Takto označené nody treba po odstránení hlavného vymazávaného
     * tiež zo stromu podobne ako pri operácii
     * {@link KDTree#delete(HasComparableValueOnDimension)}
     * vymazať rovnakou logikou, prípadne vymazať ďalšie nody,
     * ktoré pri tejto operácii pribudnú
     *
     * @param listOfMarkedNodes
     */
    private void handleMarkedNodes(LinkedList<KDTreeNode<K,D,V>> listOfMarkedNodes) {

        List<KDTreeNode<K,D,V>> successfullyRemovedNodes = new LinkedList<>();
        while (listOfMarkedNodes.size() > 0) {
            KDTreeNode<K,D,V> toDelete = listOfMarkedNodes.removeFirst();
            successfullyRemovedNodes.add(this.removeNodeDownwards(toDelete, listOfMarkedNodes, false));
        }

        successfullyRemovedNodes.forEach(this::insertNode);
    }

    /**
     * Helper metóda, ktorá zo stromu vymaže node,
     * aj s vnorenými štrukturálnymi zmenami,
     * ktoré je nutné dodržať - rekurzívne posúvanie
     * vymazávaného node-u až do listu
     *
     * @param toDeleteNode - Node na vymazanie
     */
    private KDTreeNode<K,D,V> removeNodeDownwards(KDTreeNode<K, D, V> toDeleteNode, LinkedList<KDTreeNode<K, D, V>> markedNodes, boolean doMarkedHandling) {

        KDTreeNode<K,D,V> deletedNode = null;

        while (true) {
            // Nájdenie prvku s najmenšou hodnotou kľúča na dimenzii, na ktorej sa nájdený node pre vymazanie nachádza
            LinkedList<KDTreeNode<K,D,V>> minimalFromRightSubtree = this.findMinInRightSubtreeOnDimension(toDeleteNode, toDeleteNode.getDepthModulo());
            // Skúsim najprv nahradiť najnižším nodom v pravom podstrome, ak ovšem pravý podstrom nebol NULL/prázdny
            if (minimalFromRightSubtree != null && minimalFromRightSubtree.size() != 0) {

                KDTreeNode<K,D,V> firstMinimal = minimalFromRightSubtree.removeFirst();

                // Označenie ostatných minim. nodov zo stromu
                minimalFromRightSubtree.forEach(e -> {
                    if (!markedNodes.contains(e)) {
                        markedNodes.add(e);
                    }
                });

                if (firstMinimal.isLeaf()) {
                    boolean wasMarked = markedNodes.contains(firstMinimal);
                    markedNodes.remove(firstMinimal);
                    this.replaceNodeWithNode(toDeleteNode, firstMinimal);
                    KDTreeNode<K, D, V> finalToDeleteNode = toDeleteNode;
                    toDeleteNode.getBrothers().forEach(e -> {
                        e.setSuperBrother(finalToDeleteNode);
                        e.setDepthModulo(finalToDeleteNode.getDepthModulo());
                    });
                    firstMinimal.getBrothers().forEach(e -> {
                        e.setSuperBrother(firstMinimal);
                        e.setDepthModulo(firstMinimal.getDepthModulo());
                    });

                    firstMinimal.removeItselfFromParent();
                    if (wasMarked) {
                        markedNodes.add(toDeleteNode);
                    }
                    deletedNode = firstMinimal;
                    break;
                } else {
                    boolean wasMarked = markedNodes.contains(firstMinimal);
                    markedNodes.remove(firstMinimal);
                    this.replaceNodeWithNode(toDeleteNode, firstMinimal);
                    KDTreeNode<K, D, V> finalToDeleteNode = toDeleteNode;
                    toDeleteNode.getBrothers().forEach(e -> {
                        e.setSuperBrother(finalToDeleteNode);
                        e.setDepthModulo(finalToDeleteNode.getDepthModulo());
                    });
                    if (wasMarked) {
                        markedNodes.add(toDeleteNode);
                    }
                    toDeleteNode = firstMinimal;
                }
                continue;
            }

            LinkedList<KDTreeNode<K,D,V>> maximalFromLeftSubtree = this.findMaxInLeftSubtreeOnDimension(toDeleteNode, toDeleteNode.getDepthModulo());
            if (maximalFromLeftSubtree != null && maximalFromLeftSubtree.size() != 0) {

                KDTreeNode<K,D,V> firstMaximal = maximalFromLeftSubtree.removeFirst();

                // Označenie ostatných maxim. nodov zo stromu
                maximalFromLeftSubtree.forEach(e -> {
                    if (!markedNodes.contains(e)) {
                        markedNodes.add(e);
                    }
                });

                if (firstMaximal.isLeaf()) {
                    boolean wasMarked = markedNodes.contains(firstMaximal);
                    markedNodes.remove(firstMaximal);
                    this.replaceNodeWithNode(toDeleteNode, firstMaximal);
                    KDTreeNode<K, D, V> finalToDeleteNode = toDeleteNode;
                    toDeleteNode.getBrothers().forEach(e -> {
                        e.setSuperBrother(finalToDeleteNode);
                        e.setDepthModulo(finalToDeleteNode.getDepthModulo());
                    });
                    firstMaximal.getBrothers().forEach(e -> {
                        e.setSuperBrother(firstMaximal);
                        e.setDepthModulo(firstMaximal.getDepthModulo());
                    });
                    firstMaximal.removeItselfFromParent();
                    if (wasMarked) {
                        markedNodes.add(toDeleteNode);
                    }
                    deletedNode = firstMaximal;
                    break;
                } else {
                    boolean wasMarked = markedNodes.contains(firstMaximal);
                    markedNodes.remove(firstMaximal);
                    this.replaceNodeWithNode(toDeleteNode, firstMaximal);
                    KDTreeNode<K, D, V> finalToDeleteNode1 = toDeleteNode;
                    toDeleteNode.getBrothers().forEach(e -> {
                        e.setSuperBrother(finalToDeleteNode1);
                        e.setDepthModulo(finalToDeleteNode1.getDepthModulo());
                    });
                    firstMaximal.getBrothers().forEach(e -> {
                        e.setSuperBrother(firstMaximal);
                        e.setDepthModulo(firstMaximal.getDepthModulo());
                    });
                    if (wasMarked) {
                        markedNodes.add(toDeleteNode);
                    }
                    toDeleteNode = firstMaximal;
                }
                continue;
            }

            if (toDeleteNode.isLeaf()) {

                // Replacovanie bratom len v prípade že node je leaf a nerobím odstránenie MARKED nodu!
                // doMarkedHandling = true v prípade, že robím klasické vybublávanie nodu nadol cez jeho výmeny
                if (doMarkedHandling && toDeleteNode.getBrothers().size() > 0) {
                    KDTreeNode<K,D,V> replacer = toDeleteNode.getBrothers().remove(0);
                    this.replaceNodeWithNode(toDeleteNode, replacer);
                    deletedNode = toDeleteNode;
                    break;
                }
                toDeleteNode.removeItselfFromParent();
                deletedNode = toDeleteNode;
                break;
            }
        }
        if (doMarkedHandling) {
            this.handleMarkedNodes(markedNodes);
        }
        return deletedNode;
    }

    /**
     * Nájde minimálny prvok na dimenzii kľúča z parametra,
     * pričom vychádza s optimálnejším hľadaním z predpokladu,
     * že pri vyhľadávaní je poskytnutá v atribúte
     * {@link KDTreeNode#getDepthModulo}
     * aj hĺbka samoteného nodu, od ktorého sa začína,
     * čo určí, na základe ktorej dimenzie kľúča vychádzalo
     * delenie na prvotnej a nasledujúcich podúrovniach
     * (umožní niektoré vetvy stromu vylúčiť)
     *
     * Využije na to LEVEL-ORDER prehliadku
     * LEVEL-ORDER pre to, aby vo FIFO bol na prvom mieste vrchol s čo najmenšou hĺbkou
     *
     * LEVEL-ORDER prehliadka prechádza stromom po úrovniach "zľava doprava"
     * a v prípade, že narazí na "aktuálne najnižší" kľúč,
     * vyhodí z plánovaného spracovávania/prechádzania nody (celé vetvy)
     * NAPRAVO od tohto kľúča a ďalej spracováva len nody pod týmto kľúčom
     *
     * @param fromNode - od ktorého node-u sa začína vyhľadávanie
     * @param dimension - dimenzia kľúča, na ktorej chcem nájsť minimum
     * @return <b>NULL</b> ak node nemá pravý podstrom, inak minimum(množina s rovnakým kľúčom na dimenzii) (FIFO!)
     */
    private LinkedList<KDTreeNode<K,D,V>> findMinInRightSubtreeOnDimension(KDTreeNode<K,D,V> fromNode, int dimension) {

        if (fromNode == null) {
            throw new DataStructureWrongUsageException(this.getClass().getName(), "findMinInRightSubtreeOnDimension",
                    "Pokúšaš sa nájsť najmenší node v podstrome nodu, ktorý je NULL");
        }

        KDTreeNode<K,D,V> smallestYet = fromNode.getRightChild();
        if (smallestYet == null) {
            return null;
        }

        LinkedList<KDTreeNode<K,D,V>> smallestSet = new LinkedList<>();

        // Ak pravý syn nemá žiadne ďalšie podstromy, rovno vráti tento node (je listom)
        if (!smallestYet.hasLeftChild() && !smallestYet.hasRightChild()) {
            smallestSet.add(smallestYet);
            return smallestSet;
        }

        LinkedList<KDTreeNode<K,D,V>> queue = new LinkedList<>();

        KDTreeNode<K,D,V> root = smallestYet;

        // LEVEL-ORDER PREHLIADKA
        queue.add(root);
        while (!queue.isEmpty()) {
            KDTreeNode<K,D,V> node = queue.removeFirst();

            if (node.getKey().compareToOnDimension(smallestYet.getKey(), dimension) < 0) {
                smallestSet.clear();
                smallestSet.add(node);
                smallestYet = node;
            } else if (node.getKey().compareToOnDimension(smallestYet.getKey(), dimension) == 0) {
                smallestSet.add(node);
            }

            if (node.getLeftChild() != null) {
                queue.add(node.getLeftChild());
            }
            if (node.getRightChild() != null) {
                queue.add(node.getRightChild());
            }
        }
        return smallestSet;
    }


    /**
     * Nájde maximálny prvok na dimenzii kľúča z parametra,
     * pričom vychádza s optimálnejším hľadaním z predpokladu,
     * že pri vyhľadávaní je poskytnutá v atribúte
     * {@link KDTreeNode#getDepthModulo}
     * aj hĺbka samoteného nodu, od ktorého sa začína,
     * čo určí, na základe ktorej dimenzie kľúča vychádzalo
     * delenie na prvotnej a nasledujúcich podúrovniach
     * (umožní niektoré vetvy stromu vylúčiť)
     *
     * Využije na to LEVEL-ORDER prehliadku
     * LEVEL-ORDER pre to, aby vo FIFO bol na prvom mieste vrchol s čo najmenšou hĺbkou
     *
     * LEVEL-ORDER prehliadka prechádza stromom po úrovniach "zprava doľava"
     * a v prípade, že narazí na "aktuálne najvyšší" kľúč,
     * vyhodí z plánovaného spracovávania/prechádzania nody (celé vetvy)
     * NAĽAVO od tohto kľúča a ďalej spracováva len nody pod týmto kľúčom
     *
     * @param fromNode - od ktorého node-u sa začína vyhľadávanie
     * @param dimension - dimenzia kľúča, na ktorej chcem nájsť maximum
     * @return <b>NULL</b> ak node nemá ľavý podstrom, inak maximum (množina nodeov s rovnakou hodnotou na danej dimenzii)(FIFO)
     */
    private LinkedList<KDTreeNode<K,D,V>> findMaxInLeftSubtreeOnDimension(KDTreeNode<K,D,V> fromNode, int dimension) {

        if (fromNode == null) {
            throw new DataStructureWrongUsageException(this.getClass().getName(), "findMaxInLeftSubtreeOnDimension",
                    "Pokúšaš sa nájsť najmenší node v podstrome nodu, ktorý je NULL");
        }

        KDTreeNode<K,D,V> greatestYet = fromNode.getLeftChild();
        if (greatestYet == null) {
            return null;
        }

        LinkedList<KDTreeNode<K,D,V>> greatestSet = new LinkedList<>();

        // Ak pravý syn nemá žiadne ďalšie podstromy, rovno vráti tento node (je listom)
        if (!greatestYet.hasLeftChild() && !greatestYet.hasRightChild()) {
            greatestSet.add(greatestYet);
            return greatestSet;
        }

        LinkedList<KDTreeNode<K,D,V>> queue = new LinkedList<>();

        KDTreeNode<K,D,V> root = greatestYet;

        // LEVEL-ORDER PREHLIADKA
        queue.add(root);
        while (!queue.isEmpty()) {
            KDTreeNode<K,D,V> node = queue.removeFirst();

            if (node.getKey().compareToOnDimension(greatestYet.getKey(), dimension) > 0) {
                greatestSet.clear();
                greatestSet.add(node);
                greatestYet = node;
            } else if (node.getKey().compareToOnDimension(greatestYet.getKey(), dimension) == 0) {
                greatestSet.add(node);
            }

            if (node.getRightChild() != null) {
                queue.add(node.getRightChild());
            }

            if (node.getLeftChild() != null) {
                queue.add(node.getLeftChild());
            }
        }
        return greatestSet;
    }


    /**
     * Prejde strom level-order prehliadkou
     * a vypíše zaradom hodnoty kľúčov
     */
    public void printKeysInLevelOrder() {
        if (this.root == null) {
            return;
        }

        List<KDTreeNode<K,D,V>> toRet = new ArrayList<>();

        LinkedList<KDTreeNode<K,D,V>> queue = new LinkedList<>();
        queue.add(this.root);
        while (!queue.isEmpty()) {
            KDTreeNode<K,D,V> node = queue.removeFirst();
            System.out.println("Key: " + node.getKey());
            toRet.add(node);
            if (node.getLeftChild() != null) {
                queue.add(node.getLeftChild());
            }
            if (node.getRightChild() != null) {
                queue.add(node.getRightChild());
            }
        }
    }

    /**
     * Vráti všetky kľúč-hodnota zaobalené v triede
     * {@link KDTreeNodeKeyDataWrapper}
     *
     * @return - level order prehliadka stromu, zaobalená do wrapper triedy
     */
    public List<KDTreeNodeKeyDataWrapper<K,D>> listOfDataByLevelOrder() {
        if (this.root == null) {
            return new LinkedList<>();
        }

        List<KDTreeNodeKeyDataWrapper<K,D>> toRet = new LinkedList<>();

        LinkedList<KDTreeNode<K,D,V>> queue = new LinkedList<>();
        queue.add(this.root);
        while (!queue.isEmpty()) {
            KDTreeNode<K,D,V> node = queue.removeFirst();
            toRet.add(new KDTreeNodeKeyDataWrapper<>(node.getKey(), node.getData()));

            if (node.getBrothers() != null) {
                node.getBrothers().forEach(e -> {
                    toRet.add(new KDTreeNodeKeyDataWrapper<>(e.getKey(), e.getData()));
                });
            }

            if (node.getLeftChild() != null) {
                queue.add(node.getLeftChild());
            }
            if (node.getRightChild() != null) {
                queue.add(node.getRightChild());
            }
        }

        return toRet;
    }

    /**
     * Prehodí nody medzi sebou (vymenia si kľúč a dátovú časť - referencie ostanú - OKREM BRATOV!)
     * @param toReplace
     * @param replaceWith
     */
    private void replaceNodeWithNode(KDTreeNode<K,D,V> toReplace, KDTreeNode<K,D,V> replaceWith) {
        K oldKey = toReplace.getKey();
        D oldData = toReplace.getData();
        List<KDTreeNode<K,D,V>> oldBrothers = toReplace.getBrothers();
        toReplace.setKey(replaceWith.getKey());
        toReplace.setData(replaceWith.getData());
        toReplace.setBrothers(replaceWith.getBrothers());
        replaceWith.setKey(oldKey);
        replaceWith.setData(oldData);
        replaceWith.setBrothers(oldBrothers);
    }

    public int countWithLevelOrder() {
        if (this.root == null) {
            return 0;
        }

        int count = 0;

        LinkedList<KDTreeNode<K,D,V>> queue = new LinkedList<>();
        queue.add(this.root);
        while (!queue.isEmpty()) {
            KDTreeNode<K,D,V> node = queue.removeFirst();
            count++;
            count+=node.getBrothers().size();
            if (node.getLeftChild() != null) {
                queue.add(node.getLeftChild());
            }
            if (node.getRightChild() != null) {
                queue.add(node.getRightChild());
            }
        }
        return count;
    }

    /**
     * Vráti, či predaný kľúč z parametra
     * definitívne patrí do intervalu reprezentovaného
     * dvomi predanými kľúčmi na určitej dimenzii
     *
     * Problém OD-DO pre prípad že DO < OD
     * je vyriešený porovnaním OD-DO kľúčom na
     * predanej dimenzii, pre určenie,
     * ktorý je nižší a ktorý vyšší
     *
     * @param key - kľúč, ktorý overujem
     * @param from - od kľúča
     * @param to - po kľúč
     * @param dimension - na ktorej dimenzii kľúča
     * @return
     */
    protected boolean keyBelongsToInterval(K key, K from, K to, int dimension) {

        K lowerKeyOnDimension;
        K higherKeyOnDimension;

        int keyCompared = from.compareToOnDimension(to, dimension);
        // Rovnajíce sa kľúče tu nehrajú rolu, môžu byť nastavené ľubovoľne
        if (keyCompared >= 0) {
            higherKeyOnDimension = from;
            lowerKeyOnDimension = to;
        } else {
            higherKeyOnDimension = to;
            lowerKeyOnDimension = from;
        }

        if (key.compareToOnDimension(lowerKeyOnDimension, dimension) >= 0 && key.compareToOnDimension(higherKeyOnDimension, dimension) <= 0) {
            return true;
        }
        return false;
    }

    /**
     * Metóda, ktorá vráti porovnanie kľúča Kx
     * @param key
     * na dimenzii
     * @param dimension
     * voči rozsahu/intervalu dvoch iných kľúčov (range/interval) K1 a K2
     * @param from K1
     * @param to K2
     * a to tak, že ak
     * Kx(dimension) >= K1(dimension) && Kx(dimension) <= K2(dimension) ====> return 0
     * Kx(dimension) < K1(dimension)                                    ====> return -1
     * Kx(dimension) > K2(dimension)                                    ====> return 1
     *
     * O situáciu, kedy by bol kľúč K1 vyšší ako K2 na danej dimenzii
     * je tu postarané obyčajnou zámenou týchto kľúčov
     * (situácia DO < OD)
     *
     * @return - hodnota porovnania podľa vyššie popísaných pravidiel
     */
    protected int keyCompareToRangeOnDimension(K key, K from, K to, int dimension) {
        K lowerKeyOnDimension;
        K higherKeyOnDimension;

        int keyCompared = from.compareToOnDimension(to, dimension);
        // Rovnajíce sa kľúče tu nehrajú rolu, môžu byť nastavené ľubovoľne
        if (keyCompared >= 0) {
            higherKeyOnDimension = from;
            lowerKeyOnDimension = to;
        } else {
            higherKeyOnDimension = to;
            lowerKeyOnDimension = from;
        }

        int comparedToLower = key.compareToOnDimension(lowerKeyOnDimension, dimension);
        int comparedToHigher = key.compareToOnDimension(higherKeyOnDimension, dimension);

        if (comparedToLower >= 0 && comparedToHigher <= 0) {
            return 0;
        } else if (comparedToLower < 0)  {
            return -1;
        } else {
            return 1;
        }

    }
}
