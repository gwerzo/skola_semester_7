﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using FriBettingDatabaseApp.Context;

namespace FriBettingDatabaseApp.Repository.Base
{
    /// <summary>
    /// Základný interface pre všetky repository
    /// </summary>
    /// <typeparam name="T">Biznis trieda, pre ktorú je tento repository implemetovaný</typeparam>
    public interface IRepository<T>
    {
        // Vráti VŠETKY entity daného typu
        IEnumerable<T> GetAll();

        // Vráti entity podľa predaného expressionu
        IEnumerable<T> GetAllByCondition(Expression<Func<T, bool>> expression);

        // Vráti jednu entitu podľa predaného expressionu
        T GetOneByCondition(Expression<Func<T, bool>> expression);

        // Vráti PAGINATED data z predaných parametrov
        IEnumerable<T> GetAllByCondition(Expression<Func<T, bool>> expression, int page, int pageSize);

        // Vloží entitu do setu predaného contextu
        bool SaveToContext(T saveEntity, BettingAppDbContext toDbContext);
    }
}
