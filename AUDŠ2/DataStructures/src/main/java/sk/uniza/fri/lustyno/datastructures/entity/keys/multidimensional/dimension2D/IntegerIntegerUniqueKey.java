package sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.dimension2D;

import sk.uniza.fri.lustyno.datastructures.entity.keys.UniquelyIdentifiable;

public class IntegerIntegerUniqueKey implements UniquelyIdentifiable<IntegerIntegerUniqueKey> {

    private int uniqueIdentifier;
    private int x;
    private int y;

    IntegerIntegerUniqueKey(int x, int y, int uniqueIdentifier) {
        this.x = x;
        this.y = y;
        this.uniqueIdentifier = uniqueIdentifier;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public int getUniqueIdentifier() {
        return this.uniqueIdentifier;
    }

    @Override
    public boolean equalsUniquely(IntegerIntegerUniqueKey other) {
        return this.uniqueIdentifier == other.getUniqueIdentifier();
    }

    @Override
    public void setUniqueIdIfEmpty(int uniqueId) {
        if (this.uniqueIdentifier == -1)
            this.uniqueIdentifier = uniqueId;
    }
}
