package sk.uniza.fri.lustiak.auds2.kataster.components.common;

public abstract class BusinessComponent implements ParentComponent {

    protected RepaintableComponent parentComponent;

    protected String componentTitle;

    protected BusinessComponent(RepaintableComponent parentComponent, String componentTitle) {
        this.parentComponent = parentComponent;
        this.componentTitle = componentTitle;
    }

    protected void updateFileState() {
        this.parentComponent.updateFileState();
    }

    public abstract boolean isImplemented();
}
