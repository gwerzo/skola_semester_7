package utils;

import org.apache.commons.lang3.RandomStringUtils;

public class CheckedItemWrapper {

    private Boolean isChecked;
    private String data;

    public CheckedItemWrapper(Boolean isChecked) {
        this.isChecked = isChecked;
        if (isChecked) {
            this.data = RandomStringUtils.random(10);
        } else {
            this.data = "STRING";
        }
    }

    public String getString() {
        return this.data;
    }

    public boolean isChecked() {
        return this.isChecked;
    }

}
