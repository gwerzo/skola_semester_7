﻿namespace FriBettingWebApplication.Dto.Request
{
    public class CreateCurrentTicketRequest
    {
        public double BetValue { get; set; }
    }
}