﻿using Newtonsoft.Json;

namespace FriBettingWebApplication.Dto.Response
{
    public class CompetitorResponse
    {
        [JsonProperty("competitor_id")]
        public int CompetitorId { get; set; }
        [JsonProperty("competitor_name")]
        public string CompetitorName { get; set; }

    }
}