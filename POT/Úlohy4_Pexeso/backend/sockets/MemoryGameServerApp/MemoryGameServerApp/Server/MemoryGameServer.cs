﻿using SuperWebSocket;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MemoryGameDaoApp.Bo;
using MemoryGameDaoApp.Context;
using MemoryGameServerApp.Command;
using MemoryGameServerApp.Dal;
using MemoryGameServerApp.Engine;
using MemoryGameServerApp.Parser;
using PexesoApi.Bo;

namespace MemoryGameServerApp.Server
{
    public class MemoryGameServer
    {

        // WebSocketový server
        public WebSocketServer WsServer { get; }

        // Aktívne sessiony - nemusia byť autentifikované
        private ConcurrentBag<WebSocketSession> ActiveSessions { get; }

        // Mená autentifikovaných použ. s ich session
        private ConcurrentDictionary<string, WebSocketSession> AuthenticatedSessions { get; }

        // Sessiony previazané s menom usera
        private ConcurrentDictionary<WebSocketSession, string> AuthenticatedSessionsBack { get; }

        // SocketSessionUuid - autentifikované session UUIDs
        private ConcurrentDictionary<string, string> SocketSessionUuids { get; }

        // Aktívne hry/výzvy podľa UUID
        private ConcurrentDictionary<string, MemoryGameState> ActiveGames { get; }

        // Zoznam všetkých aktívnych hier/výziev
        private ConcurrentBag<MemoryGameState> ActiveGamesList { get; }

        public GameStyleDao GameStyleDao { get; set; }
        public GameTypeDao GameTypeDao { get; set; }

        public PlayersDao PlayersDao { get; set; }

        public MemoryGameServer(int port)
        {
            this.WsServer = new WebSocketServer();

            this.ActiveSessions = new ConcurrentBag<WebSocketSession>();

            this.AuthenticatedSessions = new ConcurrentDictionary<string, WebSocketSession>();
            this.AuthenticatedSessionsBack = new ConcurrentDictionary<WebSocketSession, string>();
            this.SocketSessionUuids = new ConcurrentDictionary<string, string>();

            this.ActiveGamesList = new ConcurrentBag<MemoryGameState>();
            this.ActiveGames = new ConcurrentDictionary<string, MemoryGameState>();

            this.GameTypeDao = new GameTypeDao();
            this.GameStyleDao = new GameStyleDao();
            this.PlayersDao = new PlayersDao();

            WsServer.Setup(port);

            WsServer.NewSessionConnected += WsServer_NewSessionConnected;
            WsServer.NewMessageReceived += WsServer_NewMessageReceived;
            WsServer.SessionClosed += WsServer_SessionClosed;
            WsServer.Start();
        }

        /// <summary>
        /// Ukončenie session
        /// </summary>
        /// <param name="session"></param>
        /// <param name="value"></param>
        private void WsServer_SessionClosed(WebSocketSession session, SuperSocket.SocketBase.CloseReason value)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Removed session " + session.SessionID);
            Console.ResetColor();

            WebSocketSession removedSession;
            this.ActiveSessions.TryTake(out removedSession);

            string usernameSessionClosed;
            if (this.AuthenticatedSessionsBack.TryRemove(session, out usernameSessionClosed))
            {
                WebSocketSession removedAuthenticatedSession;
                this.AuthenticatedSessions.TryRemove(usernameSessionClosed, out removedAuthenticatedSession);

                // Existujúca hra, kde používateľ s prerušenou session je hostom
                var existingGameHost = this.ActiveGamesList.SingleOrDefault(e => e.Player1 == usernameSessionClosed);
                if (existingGameHost != null)
                {
                    MemoryGameState removedGame;
                    this.ActiveGames.TryRemove(existingGameHost.GameUuid, out removedGame);
                    this.ActiveGamesList.TryTake(out removedGame);

                    if (removedGame.Player2 != null)
                    {
                        if (removedGame.GameStateValue == MemoryGameState.GameState.RUNNING)
                        {
                            WebSocketSession webSocketSessionSecondPlayer;
                            if (this.AuthenticatedSessions.TryGetValue(removedGame.Player2,
                                out webSocketSessionSecondPlayer))
                            {
                                webSocketSessionSecondPlayer.Send(MemoryGameServerMessages.GAME_CANCELLED_BY_HOST + " " + removedGame.GameUuid);
                            }
                        }
                    }
                }

                var existingGameGuest = this.ActiveGamesList.SingleOrDefault(e => e.Player2 == usernameSessionClosed && e.GameStateValue == MemoryGameState.GameState.RUNNING);
                if (existingGameGuest != null)
                {
                    MemoryGameState removedGame;
                    this.ActiveGames.TryRemove(existingGameGuest.GameUuid, out removedGame);
                    this.ActiveGamesList.TryTake(out removedGame);

                    WebSocketSession webSocketSessionSecondPlayer;
                    if (this.AuthenticatedSessions.TryGetValue(removedGame.Player1, out webSocketSessionSecondPlayer))
                    {
                        webSocketSessionSecondPlayer.Send(MemoryGameServerMessages.GAME_CANCELLED_BY_GUEST + " " + removedGame.GameUuid);
                    }
                }
            }

            this.PrintServerStatus();

        }

        /// <summary>
        /// Pri pripojení nového spojenia sa vloží toto spojenie
        /// medzi aktívne spojenia, s ktorými dokáže server komunikovať
        ///
        /// Po úspešnej "autentifikácii" sa session prehodí
        /// z tohto listu do dictionary prihlásených použ. podľa mena
        /// 
        /// </summary>
        /// <param name="session">Aktívna session</param>
        private void WsServer_NewSessionConnected(WebSocketSession session)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("New session created " + session.SessionID);
            if (!this.ActiveSessions.Contains(session))
            {
                this.ActiveSessions.Add(session);
            }
            Console.ResetColor();

            this.PrintServerStatus();
        }

        /// <summary>
        /// Vstupná metóda pri každej message od clienta,
        /// ktorú je najprv treba rozparsovať
        /// a následne vykonať potrebné úkony
        /// </summary>
        /// <param name="session"></param>
        /// <param name="value"></param>
        private void WsServer_NewMessageReceived(WebSocketSession session, string value)
        {
            // Odparsovaný command aj s argumentami
            ICommand parsedCommand = CommandParser.ParseCommand(value);
            if (parsedCommand == null)
            {
                session.Send("Neplatný command");
                return;
            }

            parsedCommand.executeCommand(this.GetUserNameFromSession(session), this, session);
        }

        /// <summary>
        /// Autentifikácia používateľa
        /// </summary>
        /// <param name="username">Používateľské meno používateľa</param>
        /// <param name="password">Používateľské heslo používateľa</param>
        /// <param name="socketSession">Aktuálna session používateľa</param>
        /// <param name="socketSessionUUidGenerated">Vygenerované socket session uuid</param>
        /// <returns>bool podľa toho, či sa používateľa podarilo autentifikovať</returns>
        public bool AuthenticateUser(string username, string password, WebSocketSession socketSession, out string socketSessionUUidGenerated)
        {
            if (this.IsAuthenticated(socketSession))
            {
                socketSessionUUidGenerated = null;
                return false;
            }

            Player player = this.PlayersDao.GetPlayerByUsername(username);

            if (player == null)
            {
                socketSessionUUidGenerated = null;
                return false;
            }

            if (player.HashedPwd == PlayersDao.ComputeSha256Hash(password))
            {
                this.ActiveSessions.TryTake(out socketSession);
                this.AuthenticatedSessions.TryAdd(username, socketSession);
                this.AuthenticatedSessionsBack.TryAdd(socketSession, username);

                string generatedSocketSessionGuid = Guid.NewGuid().ToString();
                this.SocketSessionUuids.TryAdd(generatedSocketSessionGuid, username);
                socketSessionUUidGenerated = generatedSocketSessionGuid;
                return true;
            }

            socketSessionUUidGenerated = null;
            return false;
        }

        /// <summary>
        /// Odhlásenie session
        /// </summary>
        /// <param name="sessionUuid"></param>
        /// <returns></returns>
        public bool LogoutUser(string sessionUuid)
        {
            string username;
            if (this.SocketSessionUuids.TryRemove(sessionUuid, out username))
            {
                WebSocketSession session;
                this.AuthenticatedSessions.TryRemove(username, out session);
                string authUsername;
                this.AuthenticatedSessionsBack.TryRemove(session, out authUsername);

                // Existujúca hra, kde používateľ s prerušenou session je hostom
                var existingGameHost = this.ActiveGamesList.SingleOrDefault(e => e.Player1 == username);
                if (existingGameHost != null)
                {
                    MemoryGameState removedGame;
                    this.ActiveGames.TryRemove(existingGameHost.GameUuid, out removedGame);
                    this.ActiveGamesList.TryTake(out removedGame);

                    if (removedGame.Player2 != null)
                    {
                        if (removedGame.GameStateValue == MemoryGameState.GameState.RUNNING)
                        {
                            WebSocketSession webSocketSessionSecondPlayer;
                            if (this.AuthenticatedSessions.TryGetValue(removedGame.Player2,
                                out webSocketSessionSecondPlayer))
                            {
                                webSocketSessionSecondPlayer.Send(MemoryGameServerMessages.GAME_CANCELLED_BY_HOST + " " + removedGame.GameUuid);
                            }
                        }
                    }
                }

                var existingGameGuest = this.ActiveGamesList.SingleOrDefault(e => e.Player2 == username && e.GameStateValue == MemoryGameState.GameState.RUNNING);
                if (existingGameGuest != null)
                {
                    MemoryGameState removedGame;
                    this.ActiveGames.TryRemove(existingGameGuest.GameUuid, out removedGame);
                    this.ActiveGamesList.TryTake(out removedGame);

                    WebSocketSession webSocketSessionSecondPlayer;
                    if (this.AuthenticatedSessions.TryGetValue(removedGame.Player1, out webSocketSessionSecondPlayer))
                    {
                        webSocketSessionSecondPlayer.Send(MemoryGameServerMessages.GAME_CANCELLED_BY_GUEST + " " + removedGame.GameUuid);
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Reautentifikuje session cez socketSessionId
        /// využitie pri ukončení spojenia a nutnosti nadviazania nového spojenia
        /// bez toho, aby používateľ musel znova zadávať credentials
        /// </summary>
        /// <param name="socketSessionUuid">UUID reautentifikovanej session</param>
        /// <param name="socketSession">Samotná session</param>
        /// <returns></returns>
        public bool ReAuthenticateUserWithSocketSessionId(string socketSessionUuid, WebSocketSession socketSession)
        {
            string username;
            this.SocketSessionUuids.TryGetValue(socketSessionUuid, out username);
            if (username != null)
            {
                this.AuthenticatedSessions.TryAdd(username, socketSession);
                this.AuthenticatedSessionsBack.TryAdd(socketSession, username);

                Console.WriteLine("Requthenticated session " + socketSession.SessionID);

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Vytvorí novú hru pre hráča, za podmienok že:
        /// 1. Hráč č.1 už nemá vytvorenú nejakú výzvu
        /// 2. Hráč č.2 je buď null (náh.súper), alebo Hráč č.2 nie je v hre
        /// </summary>
        /// <param name="newGame"></param>
        /// <returns>UUID novovytvorenej požiadavky na hru</returns>
        public string CreateNewGame(MemoryGameState newGame)
        {
            MemoryGameState player1ActiveGame = this.ActiveGamesList
                .Where(e => e.Player1 == newGame.Player1)
                .Select(e => e)
                .FirstOrDefault();
            if (player1ActiveGame != null)
            {
                return null;
            }

            MemoryGameState player2ActiveGame = this.ActiveGamesList
                .Where(e => (e.Player1 == newGame.Player2 || e.Player2 == newGame.Player2) 
                            && e.GameStateValue == MemoryGameState.GameState.RUNNING)
                .Select(e => e)
                .FirstOrDefault();
            if (player2ActiveGame != null)
            {
                return null;
            }

            string gameGeneratedUuid = Guid.NewGuid().ToString();
            newGame.GameUuid = gameGeneratedUuid;
            this.ActiveGamesList.Add(newGame);
            this.ActiveGames.TryAdd(gameGeneratedUuid, newGame);
            return gameGeneratedUuid;
        }

        /// <summary>
        /// Pripojí hráča do hry 
        /// </summary>
        /// <param name="gameUuid">UUID hry</param>
        /// <param name="usernameOfCommandCaller">Username pripájaného hráča</param>
        public bool JoinToGame(string gameUuid, string usernameOfCommandCaller, 
            out int xLength, out int yLength, out string error)
        {
            if (!this.IsAuthenticated(usernameOfCommandCaller))
            {
                error = "Nie ste prihlásený";
                xLength = -1;
                yLength = -1;
                return false;
            }

            MemoryGameState game;
            if (this.ActiveGames.TryGetValue(gameUuid, out game))
            {
                if ((game.Player2 == null && game.GameStateValue != MemoryGameState.GameState.RUNNING) 
                    || (game.Player2 == usernameOfCommandCaller && game.GameStateValue == MemoryGameState.GameState.PENDING_OPPONENT))
                {
                    game.Player2 = usernameOfCommandCaller;
                    error = null;
                    xLength = game.WidthOfGame;
                    yLength = game.HeightOfGame;
                    // Source https://stackoverflow.com/questions/17632584/how-to-get-the-unix-timestamp-in-c-sharp/35425123
                    var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
                    var epochSeconds = (long)timeSpan.TotalSeconds;
                    game.GameStartTime = epochSeconds;
                    game.LastMoveStopwatch = Stopwatch.StartNew();
                    game.GameStateValue = MemoryGameState.GameState.RUNNING;
                    return true;
                }
                else
                {
                    error = "Hra už beží, alebo je v nej pripojený niekto iný";
                    xLength = -1;
                    yLength = -1;
                    return false;
                }
            }
            else
            {
                error = "Neexistujúca hra";
                xLength = -1;
                yLength = -1;
                return false;
            }
        }

        /// <summary>
        /// Trafenie niektorej karty v hre s predanym UUID
        /// </summary>
        /// <param name="gameUuid"></param>
        /// <param name="xHit"></param>
        /// <param name="yHit"></param>
        /// <returns>Názov súboru tragenej karty</returns>
        public string HitCard(string gameUuid, string username, int xHit, int yHit, 
            out int alreadyHitX, out int AlreadyHitY, out WebSocketSession secondPlayerSession, out string error)
        {

            MemoryGameState game;
            if (!this.ActiveGames.TryGetValue(gameUuid, out game))
            {
                error = "Hra s týmto UUID neexistuje";
                alreadyHitX = -1;
                AlreadyHitY = -1;
                secondPlayerSession = null;
                return null;
            }

            if (game.GameStateValue == MemoryGameState.GameState.PENDING_RANDOM ||
                game.GameStateValue == MemoryGameState.GameState.PENDING_OPPONENT)
            {
                error = "Hra ešte nezačala, čaká sa na druhého hráča";
                alreadyHitX = -1;
                AlreadyHitY = -1;
                secondPlayerSession = null;
                return null;
            }

            if (xHit >= game.WidthOfGame || yHit >= game.HeightOfGame)
            {
                error = "Chybné súradnice";
                alreadyHitX = -1;
                AlreadyHitY = -1;
                secondPlayerSession = null;
                return null;
            }

            // Ak sa pokúsi trafiť kartu, ktorá je už otvorená ako prvá
            if (game.GameBoard[xHit, yHit].IsOpenedTemporarily)
            {
                error = "Karta je v tomto ťahu už odkrytá";
                alreadyHitX = -1;
                AlreadyHitY = -1;
                secondPlayerSession = null;
                return null;
            }

            // Ak sa pokúsi ťrafiť kartu, ktorá je otvorená už finálne
            if (game.GameBoard[xHit, yHit].IsOpened)
            {
                error = "Karta je už odkrytá";
                alreadyHitX = -1;
                AlreadyHitY = -1;
                secondPlayerSession = null;
                return null;
            }

            // Ak má ísť hráč 1, ale neklikol on
            if (!game.PlayerMove)
            {
                if (username != game.Player1)
                {
                    error = "Nie ste na ťahu";
                    alreadyHitX = -1;
                    AlreadyHitY = -1;
                    secondPlayerSession = null;
                    return null;
                }
            }
            // Ak má ísť hráč 2, ale neklikol on
            else
            {
                if (username != game.Player2)
                {
                    error = "Nie ste na ťahu";
                    alreadyHitX = -1;
                    AlreadyHitY = -1;
                    secondPlayerSession = null;
                    return null;
                }
            }

            // Prípad, kedy sa odkrýva prvá karta
            if (!game.MoveStep)
            {
                game.MoveStep = true;
                game.TemporaryXOpened = xHit;
                game.TemporaryYOpened = yHit;
                game.GameBoard[game.TemporaryXOpened, game.TemporaryYOpened].IsOpenedTemporarily = true;
                error = null;
                alreadyHitX = -1;
                AlreadyHitY = -1;

                if (game.PlayerMove)
                {
                    this.AuthenticatedSessions.TryGetValue(game.Player1, out secondPlayerSession);
                }
                else
                {
                    this.AuthenticatedSessions.TryGetValue(game.Player2, out secondPlayerSession);
                }

                MemoryGameStep memoryGameStep = new MemoryGameStep();
                memoryGameStep.Hit_X = xHit;
                memoryGameStep.Hit_Y = yHit;
                memoryGameStep.StepCount = game.StepCounter++;
                memoryGameStep.TookMilis = game.LastMoveStopwatch.ElapsedMilliseconds;

                game.Steps.Add(memoryGameStep);

                game.LastMoveStopwatch.Restart();

                return this.BuildCardFileName(game.GameStyleId, game.GameBoard[xHit, yHit].IdValue);
            }
            else
            {
                game.MoveStep = false;
                // Trafenie druhej karty
                if (game.GameBoard[game.TemporaryXOpened, game.TemporaryYOpened].IdValue ==
                    game.GameBoard[xHit, yHit].IdValue)
                {
                    game.GameBoard[game.TemporaryXOpened, game.TemporaryYOpened].IsOpened = true;
                    game.GameBoard[xHit, yHit].IsOpened = true;
                    game.OpenedPairsCounter++;

                    if (game.PlayerMove)
                    {
                        game.Player2PairdCount++;
                    }
                    else
                    {
                        game.Player1PairsCount++;
                    }

                    error = null;
                    alreadyHitX = game.TemporaryXOpened;
                    AlreadyHitY = game.TemporaryYOpened;

                    if (game.PlayerMove)
                    {
                        this.AuthenticatedSessions.TryGetValue(game.Player1, out secondPlayerSession);
                    }
                    else
                    {
                        this.AuthenticatedSessions.TryGetValue(game.Player2, out secondPlayerSession);
                    }

                    MemoryGameStep memoryGameStep = new MemoryGameStep();
                    memoryGameStep.Hit_X = xHit;
                    memoryGameStep.Hit_Y = yHit;
                    memoryGameStep.StepCount = game.StepCounter++;
                    memoryGameStep.TookMilis = game.LastMoveStopwatch.ElapsedMilliseconds;

                    game.Steps.Add(memoryGameStep);

                    game.LastMoveStopwatch.Restart();

                    // Koniec hry, iba sa dá info obom používateľom
                    if (game.OpenedPairsCounter == (game.HeightOfGame * game.WidthOfGame) / 2)
                    {
                        WebSocketSession player1Session;
                        WebSocketSession player2Session;
                        this.AuthenticatedSessions.TryGetValue(game.Player1, out player1Session);
                        this.AuthenticatedSessions.TryGetValue(game.Player2, out player2Session);

                        player1Session.Send("-finishedgame " + 
                                            game.Player1 + " " + game.Player1PairsCount + " " +
                                            game.Player2 + " " + game.Player2PairdCount);

                        player2Session.Send("-finishedgame " +
                                            game.Player1 + " " + game.Player1PairsCount + " " +
                                            game.Player2 + " " + game.Player2PairdCount);

                        MemoryGameState removedGame;
                        this.ActiveGames.TryRemove(gameUuid, out removedGame);
                        this.ActiveGamesList.TryTake(out removedGame);

                        // Zaeviduje odohranú hru
                        this.MakeGameEvidence(removedGame);

                        return this.BuildCardFileName(game.GameStyleId, game.GameBoard[xHit, yHit].IdValue);
                    }

                    return this.BuildCardFileName(game.GameStyleId, game.GameBoard[xHit, yHit].IdValue);
                }
                else
                {
                    game.ResetTemporaryOpens();
                    error = "Netrafená druhá karta";
                    alreadyHitX = -1;
                    AlreadyHitY = -1;

                    MemoryGameStep memoryGameStep = new MemoryGameStep();
                    memoryGameStep.Hit_X = xHit;
                    memoryGameStep.Hit_Y = yHit;
                    memoryGameStep.StepCount = game.StepCounter++;
                    memoryGameStep.TookMilis = game.LastMoveStopwatch.ElapsedMilliseconds;

                    game.Steps.Add(memoryGameStep);

                    game.LastMoveStopwatch.Restart();

                    if (game.PlayerMove)
                    {
                        this.AuthenticatedSessions.TryGetValue(game.Player1, out secondPlayerSession);
                    }
                    else
                    {
                        this.AuthenticatedSessions.TryGetValue(game.Player2, out secondPlayerSession);
                    }
                    game.PlayerMove = !game.PlayerMove;

                    return this.BuildCardFileName(game.GameStyleId, game.GameBoard[xHit, yHit].IdValue);
                }

            }
        }

        /// <summary>
        /// Zaevidovanie stavu hry do databázy
        /// </summary>
        /// <param name="removedGame"></param>
        private void MakeGameEvidence(MemoryGameState removedGame)
        {
            MemoryGame mg = new MemoryGame();

            using (var dbContext = new MemoryGameDbContext())
            {
                var player1 = dbContext.GetPlayerByUsername(removedGame.Player1);
                var player2 = dbContext.GetPlayerByUsername(removedGame.Player2);
                mg.Player1 = player1;
                mg.Player2 = player2;

                if (removedGame.PlayerMove)
                {
                    mg.Winner = player2;
                }
                else
                {
                    mg.Winner = player1;
                }

                mg.GameStyle = GetGameStyleById(removedGame.GameStyleId);
                mg.GameType = GetGameTypeById(removedGame.GameTypeId);
                mg.GameStartTime = removedGame.GameStartTime;
                mg.StepsCount = removedGame.StepCounter;
                var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
                var epochSeconds = (long)timeSpan.TotalSeconds;

                mg.TotalTime = epochSeconds - removedGame.GameStartTime;
                mg.MemoryGameSteps = removedGame.Steps;

                dbContext.MemoryGames.Add(mg);
                dbContext.SaveChanges();
            }
        }


        /// <summary>
        /// Vráti zoznam usernamov prihlásených používateľov
        /// </summary>
        /// <returns></returns>
        public List<string> GetLoggedInUsernames()
        {
            return this.AuthenticatedSessions.Keys.ToList();
        }

        /// <summary>
        /// Vráti zoznam hier, do ktorých je používateľ pozvaný v tvare
        /// UUID(medzera)USERNAME_VYZYVATELA(medzera)STYL_HRY(medzera)TYP_HRY
        /// </summary>
        /// <returns></returns>
        public List<string> GetInvitesForUser(string username)
        {

            return this.ActiveGamesList
                .Where(e => e.GameStateValue == MemoryGameState.GameState.PENDING_OPPONENT)
                .Where(e => e.Player2 == username)
                .Select(e => "" + e.GameUuid + " " + e.Player1 + " " + e.WidthOfGame + "x" + e.HeightOfGame + " " + GetGameStyleById(e.GameStyleId).Description)
                .ToList();
        }

        /// <summary>
        /// Vráti zoznam uuid otvorených hier
        /// </summary>
        /// <returns></returns>
        public List<string> GetOpenGamesUuidsList()
        {
            return this.ActiveGames.Keys.ToList();
        }

        /// <summary>
        /// Cez predanú session vráti username naviazaný na tento session
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        private string GetUserNameFromSession(WebSocketSession session)
        {
            string username;
            if (this.AuthenticatedSessionsBack.TryGetValue(session, out username))
            {
                return username;
            }

            return null;
        }

        /// <summary>
        /// Nájde používateľa podľa predaného parametra
        /// a jeho session pošle predanú správu
        /// </summary>
        /// <param name="usernameOfReceiver">Meno receivera</param>
        /// <param name="message">Správa</param>
        /// <returns></returns>
        public bool SendMessageTo(string usernameOfReceiver, string message)
        {
            WebSocketSession sendToSession;
            if (this.AuthenticatedSessions.TryGetValue(usernameOfReceiver, out sendToSession))
            {
                sendToSession.Send(message);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Vráti, či je session autentifikovaná
        /// </summary>
        /// <param name="webSocketSession"></param>
        /// <returns></returns>
        public bool IsAuthenticated(WebSocketSession webSocketSession)
        {
            return this.AuthenticatedSessionsBack.ContainsKey(webSocketSession);
        }

        /// <summary>
        /// Vráti, či je username autentifikované
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public bool IsAuthenticated(string username)
        {
            if (username == null) return false;
            return this.AuthenticatedSessions.ContainsKey(username);
        }

        private void PrintServerStatus()
        {
            Console.WriteLine("\n------------SERVER STATUS-------------");
            Console.WriteLine("Active sessions: " + this.ActiveSessions.Count);
            Console.WriteLine("Authenticated sessions: " + this.AuthenticatedSessions.Count);
            Console.WriteLine("Authenticated sessions back: " + this.AuthenticatedSessionsBack.Count);
            Console.WriteLine("------------SERVER STATUS END-------------\n\n");
        }

        /// <summary>
        /// Pre nejaký štýl hry (obrázkové, znakové,..)
        /// vráti zoznam ID kariet, ktoré môžu nadobúdať
        /// </summary>
        /// <param name="gameStyle">Štýl hry (obrázková, znaková,..)</param>
        /// <returns></returns>
        public List<int> GetCardIdsForGameType(int gameStyleId)
        {
            return this.GameStyleDao.GetCardsIdsForGameStyle(gameStyleId);
        }

        public GameType GetGameTypeById(int id)
        {
            return this.GameTypeDao.GetGameTypeById(id);
        }

        public GameStyle GetGameStyleById(int id)
        {
            return this.GameStyleDao.GetGameStyleById(id);
        }

        /// <summary>
        /// Vráti názov súboru pre štýl hry + hodnotu karty
        /// </summary>
        /// <param name="gameStyle"></param>
        /// <param name="cardValue"></param>
        /// <returns></returns>
        public string BuildCardFileName(int gameStyle, int cardValue)
        {
            using (var dbContext = new MemoryGameDbContext())
            {

                CardPrefixPostfix prefixPostfix = dbContext.GetCardPrefixForGameStyle(gameStyle);
                return prefixPostfix.Prefix + cardValue + prefixPostfix.Postfix;
            }
        }

        /// <summary>
        /// Vytvorí novú hru (výzvu) ktorá má vyplneného hráča 2, ale ešte nebeží
        /// </summary>
        /// <param name="newGame"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public string CreateInviteToGame(MemoryGameState newGame, out string error)
        {
            if (string.IsNullOrEmpty(newGame.Player2) || !this.AuthenticatedSessions.ContainsKey(newGame.Player2))
            {
                error = "Používateľ neexistuje, alebo nie je prihlásený";
                return null;
            }

            MemoryGameState player1ActiveGame = this.ActiveGamesList
                .Where(e => e.Player1 == newGame.Player1)
                .Select(e => e)
                .FirstOrDefault();
            if (player1ActiveGame != null)
            {
                error = "Už máte vytvorenú hru";
                return null;
            }

            MemoryGameState player2ActiveGame = this.ActiveGamesList
                .Where(e => (e.Player1 == newGame.Player2 || e.Player2 == newGame.Player2)
                            && e.GameStateValue == MemoryGameState.GameState.RUNNING)
                .Select(e => e)
                .FirstOrDefault();
            if (player2ActiveGame != null)
            {
                error = "Vyzývaný hráč sa už nachádza v inej hre";
                return null;
            }

            string gameGeneratedUuid = Guid.NewGuid().ToString();
            newGame.GameUuid = gameGeneratedUuid;
            newGame.GameStateValue = MemoryGameState.GameState.PENDING_OPPONENT;
            this.ActiveGamesList.Add(newGame);
            this.ActiveGames.TryAdd(gameGeneratedUuid, newGame);

            error = null;
            return gameGeneratedUuid;
        }
    }
}
