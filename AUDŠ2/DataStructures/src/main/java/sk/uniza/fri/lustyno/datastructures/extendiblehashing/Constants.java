package sk.uniza.fri.lustyno.datastructures.extendiblehashing;


public class Constants {

    public static final Integer JAVA_INTEGER_SIZE               = 4;

    public static final Integer JAVA_BOOLEAN_SIZE               = 1;

    public static final Integer JAVA_DOUBLE_SIZE                = 8;

    public static final String DB_FILE_POSTFIX                  = ".ehdb";

    public static final String DB_OVERFLOW_FILE_PREFIX          = "overflow_";

    public static final String DB_PREFIX                        = "DATABASE/EXTENDIBLE/";


}
