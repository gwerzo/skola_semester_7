﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DisertationThemes.ViewerGeneratorApp.menu
{
    class MenuItemCloseApp: AbstractMenuItem
    {
        public static string MENU_TEXT = "Ukončiť aplikáciu";
        public MenuItemCloseApp(MenuHolder menuHolder) : base(menuHolder)
        {
        }

        public override string GetMenuText()
        {
            return MENU_TEXT;
        }
    }
}
