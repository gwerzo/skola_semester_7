package sk.uniza.fri.lustyno.datastructures.entity.keys;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;

public class DoubleStringKey implements HasComparableValue<DoubleStringKey.DoubleString> {

    private DoubleString doubleString;

    public DoubleStringKey(String s1, String s2) {
        this.doubleString = new DoubleString(s1, s2);
    }

    @Override
    public DoubleString getValue() {
        return this.doubleString;
    }

    @Override
    public HasComparableValue<DoubleString> getMinValue() {
        return new DoubleStringKey("ZZZZZZZZZZ", "ZZZZZZZZZZ");
    }

    public class DoubleString implements Comparable<DoubleString> {

        private String s1;

        private String s2;

        public DoubleString(String s1, String s2) {
            this.s1 = s1;
            this.s2 = s2;
        }

        @Override
        public boolean equals(Object o) {
            DoubleString ds = (DoubleString)o;
            return this.s1.equals(ds.s1) && this.s2.equals(ds.s2);
        }

        @Override
        public int compareTo(DoubleString o) {
            int compared = this.s1.compareTo(o.s1);

            if (compared != 0) {
                return compared;
            }
            else {
                return this.s2.compareTo(o.s2);
            }
        }

        @Override
        public String toString() {
            return this.s1 + " " + this.s2;
        }
    }

}
