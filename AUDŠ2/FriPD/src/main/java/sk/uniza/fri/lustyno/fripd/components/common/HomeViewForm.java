package sk.uniza.fri.lustyno.fripd.components.common;

import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class HomeViewForm extends VerticalLayout {

    public HomeViewForm() {
        initComponents();
    }

    private void initComponents() {

        Label lMain = new Label("Funkcionalita poskytovaná informačným systémom:");

        Label lEmpty = new Label();


        Label l1 = new Label("1. Pridanie nového auta do sytému");
        Label l2 = new Label("2. Vyhľadanie vozidla podľa VIN");
        Label l3 = new Label("3. Vyhľadanie vozidla podľa EVČ");
        Label l4 = new Label("4. Editovanie údajov o vozidle");

        Label p1 = new Label("1. Pridanie nového vodičského preukazu");
        Label p2 = new Label("2. Vyhľadanie vodičkého preukazu podľa EVČ");
        Label p3 = new Label("3. Zmena údajov vodičského preukazu");

        Label s1 = new Label("1. Uloženie a ukončenie systému");

        l1.setWidth("100%");
        l2.setWidth("100%");
        l3.setWidth("100%");
        l4.setWidth("100%");

        p1.setWidth("100%");
        p2.setWidth("100%");
        p3.setWidth("100%");

        s1.setWidth("100%");


        lMain.setWidth("100%");
        lEmpty.setWidth("100%");

        VerticalLayout labelsWrapper = new VerticalLayout(
                                lMain,
                                new VerticalLayout(
                                        l1, l2, l3, l4
                                ),
                                new VerticalLayout(
                                        p1, p2, p3
                                ),
                                new VerticalLayout(
                                        s1
                                )
                );

        Panel descPanel = new Panel(
                "Policajné oddelenie FRI",
                        labelsWrapper
                );

        addComponent(descPanel);
    }

}
