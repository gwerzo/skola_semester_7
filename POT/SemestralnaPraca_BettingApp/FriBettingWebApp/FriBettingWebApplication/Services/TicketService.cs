﻿using System;
using System.Collections.Generic;
using System.Linq;
using FriBettingDatabaseApp.Bo;
using FriBettingDatabaseApp.Context;
using FriBettingWebApplication.Dto.Request;
using FriBettingWebApplication.Dto.Response;
using FriBettingWebApplication.Services.Face;

namespace FriBettingWebApplication.Services
{
    public class TicketService: ITicketService
    {
        public TicketService()
        {
            
        }

        public OperationResponse AddOddToOpenedTicket(AddOddToTicketRequest request, string usernameIdentity)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                // Nájde 1 tiket používateľa, ktorý nie je uzatvorený
                var ticket = dbContext.Tickets
                    .Include("BettingUser")
                    .Include("TicketBets")
                    .Include("TicketBets.Odd")
                    .Include("TicketBets.Odd.Match")
                    .SingleOrDefault(e => e.BettingUser.Username == usernameIdentity && e.IsClosed == false);
                var user = dbContext.BettingUsers.SingleOrDefault(e => e.Username == usernameIdentity);

                // Ak používateľ takýto tiket nemá, treba ho vytvoriť
                if (ticket == null)
                {
                    ticket = new Ticket
                    {
                        BettingUser = user,
                        IsClosed = false,
                        BetValue = 1.0,
                        DateTimeCreated = DateTime.Now,
                        IsEvaluated = false,
                        TicketBets = new List<TicketBet>()
                    };
                    dbContext.Tickets.Add(ticket);
                }

                var odd = dbContext.Odds.Include("Match").SingleOrDefault(e => e.OddId == request.OddId);
                if (odd == null)
                {
                    return new OperationResponse
                    {
                        Successful = false,
                        ErrorMessage = "Odd does not exist"
                    };
                }

                if (odd.Match.IsEvaluated)
                {
                    return new OperationResponse
                    {
                        Successful = false,
                        ErrorMessage = "Match was already evaluated"
                    };
                }

                // Restriction, že na jednom tikete môže byť každý zápas len raz
                foreach (var ticketBet in ticket.TicketBets)
                {
                    if (ticketBet.Odd.Match.MatchId == odd.Match.MatchId)
                    {
                        return new OperationResponse
                        {
                            Successful = false,
                            ErrorMessage = "Odd for this match is already present at your current opened ticket"
                        };
                    }
                }

                TicketBet tb = new TicketBet
                {
                    Odd = odd,
                    BettingUser = user,
                    Ticket = ticket
                };
                dbContext.TicketMatchBets.Add(tb);


                dbContext.SaveChanges();
                return new OperationResponse
                {
                    Successful = true
                };
            }
        }

        public TicketDetailResponse GetCurrentTicketForUser(string usernameIdentity)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                var ticket = dbContext.Tickets
                    .Include("BettingUser")
                    .Include("TicketBets")
                    .Include("TicketBets.Odd")
                    .Include("TicketBets.Odd.OddType")
                    .Include("TicketBets.Odd.Match")
                    .Include("TicketBets.Odd.Match.HostCompetitor")
                    .Include("TicketBets.Odd.Match.GuestCompetitor")
                    .SingleOrDefault(e => e.BettingUser.Username == usernameIdentity && e.IsClosed == false);

                if (ticket == null)
                {
                    return new TicketDetailResponse();
                }

                var oddsTotalMultiplication =
                    ticket.TicketBets.Select(e => e.Odd.OddValue).Aggregate(1.0, (x, y) => x * y);

                return new TicketDetailResponse
                {
                    TicketId = ticket.TicketId,
                    TotalOddsValue = oddsTotalMultiplication,
                    OddsOnTicket = ticket.TicketBets.Select(e => new OddWithOddTypeAndMatchDetailResponse
                    {
                        OddValue = e.Odd.OddValue,
                        OddId = e.Odd.OddId,
                        OddTypeId = e.Odd.OddType.OddTypeId,
                        OddTypeCode = e.Odd.OddType.Code,
                        Host = e.Odd.Match.HostCompetitor.Name,
                        Guest = e.Odd.Match.GuestCompetitor.Name
                    }).ToList()
                };
            }
        }

        public OperationResponse CreateTicket(CreateCurrentTicketRequest request, string usernameIdentity)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                if (request.BetValue < 0.1)
                {
                    {
                        return new OperationResponse
                        {
                            Successful = false,
                            ErrorMessage = "Bet value must be at least 0.1"
                        };
                    }
                }

                var ticket = dbContext.Tickets
                    .Include("BettingUser")
                    .Include("TicketBets")
                    .SingleOrDefault(e => e.BettingUser.Username == usernameIdentity && e.IsClosed == false);
                if (ticket == null)
                {
                    return new OperationResponse
                    {
                        Successful = false,
                        ErrorMessage = "Ticket is not created, bet on something first"
                    };
                }

                if (ticket.TicketBets.Count == 0)
                {
                    return new OperationResponse
                    {
                        Successful = false,
                        ErrorMessage = "Ticket has zero bets, bet on something first"
                    };
                }

                if (ticket.BettingUser.Balance < request.BetValue)
                {
                    return new OperationResponse
                    {
                        Successful = false,
                        ErrorMessage = "Insufficient funds for this bet value"
                    };
                }

                ticket.BetValue = request.BetValue;
                ticket.IsClosed = true;
                ticket.BettingUser.Balance -= request.BetValue;

                dbContext.SaveChanges();

                return new OperationResponse
                {
                    Successful = true
                };
            }
        }

        public List<ClosedTicketDetailResponse> GetAllClosedAndNotEvaluatedTicketsForUser(string usernameIdentity)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                var tickets = dbContext.Tickets
                    .Include("BettingUser")
                    .Include("TicketBets")
                    .Include("TicketBets.Odd")
                    .Include("TicketBets.Odd.OddType")
                    .Include("TicketBets.Odd.Match")
                    .Include("TicketBets.Odd.Match.HostCompetitor")
                    .Include("TicketBets.Odd.Match.GuestCompetitor")
                    .Where(e => e.BettingUser.Username == usernameIdentity && e.IsClosed && e.IsEvaluated == false);

                if (!tickets.Any())
                {
                    return new List<ClosedTicketDetailResponse>();
                }

                List<ClosedTicketDetailResponse> toRet = new List<ClosedTicketDetailResponse>();
                foreach (var ticket in tickets)
                {
                    var oddsTotalMultiplication =
                        ticket.TicketBets.Select(e => e.Odd.OddValue).Aggregate(1.0, (x, y) => x * y);
                    toRet.Add(new ClosedTicketDetailResponse
                    {
                        TicketId = ticket.TicketId,
                        TotalOdds = oddsTotalMultiplication,
                        WinPotential = oddsTotalMultiplication * ticket.BetValue,
                        EvaluatedMatches = ticket.TicketBets.Count(e => e.Odd.Match.IsEvaluated),
                        NotEvaluatedMatches = ticket.TicketBets.Count
                    });
                }
                return toRet;
            }
        }

        public List<EvaluatedTicketDetailResponse> GetAllEvaluatedTicketsForUser(string usernameIdentity)
        {
            using (var dbContext = new BettingAppDbContext())
            {
                var tickets = dbContext.Tickets
                    .Include("BettingUser")
                    .Include("TicketBets")
                    .Include("TicketBets.Odd")
                    .Include("TicketBets.Odd.OddType")
                    .Include("TicketBets.Odd.Match")
                    .Include("TicketBets.Odd.Match.HostCompetitor")
                    .Include("TicketBets.Odd.Match.GuestCompetitor")
                    .Where(e => e.BettingUser.Username == usernameIdentity && e.IsClosed && e.IsEvaluated);

                if (!tickets.Any())
                {
                    return new List<EvaluatedTicketDetailResponse>();
                }

                List<EvaluatedTicketDetailResponse> toRet = new List<EvaluatedTicketDetailResponse>();
                foreach (var ticket in tickets)
                {
                    var oddsTotalMultiplication =
                        ticket.TicketBets.Select(e => e.Odd.OddValue).Aggregate(1.0, (x, y) => x * y);
                    toRet.Add(new EvaluatedTicketDetailResponse
                    {
                        TicketId = ticket.TicketId,
                        TotalOdds = oddsTotalMultiplication,
                        WinPotential = oddsTotalMultiplication * ticket.BetValue,
                        IsWinning = ticket.IsWinning,
                        BetValue = ticket.BetValue
                    });
                }
                return toRet;
            }
        }
    }
}