package sk.uniza.fri.lustyno.AudsLetisko.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.LietadloEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.LietadloTableUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.NovaOdletovaDrahaUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.PriletLietadlaUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entity_mapper.EntityMapper;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Service
public class GeneratorService  {

    @Autowired
    private AircraftManagementService aircraftManagementService;

    @Autowired
    private RunwayManagementService runwayManagementService;

    @Autowired
    private EntityMapper entityMapper;

    public void generateEntities() {
        Random rnd = new Random();

        // 50% bude mať spravené požiadanie o pridelenie dráhy a odlet
        List<PriletLietadlaUiEntity> odlozeneLietadlaPreBuduceSpracovanie = new LinkedList<>();

        List<PriletLietadlaUiEntity> odlozeneLietadlaPreBuducePoziadanie = new LinkedList<>();

        // Vygenerovanie 200 random príletov
        for (int i = 0; i < 10000; i++) {

            PriletLietadlaUiEntity priletLietadlaUiEntity = PriletLietadlaUiEntity.getRandomPriletLietadla();

            aircraftManagementService.zaevidujPriletLietadla(
                    entityMapper.fromPriletLietadlaUiEntity(priletLietadlaUiEntity));
            if (rnd.nextInt(10) == 0) {
                odlozeneLietadlaPreBuduceSpracovanie.add(priletLietadlaUiEntity);
            } else {
                odlozeneLietadlaPreBuducePoziadanie.add(priletLietadlaUiEntity);
            }
        }

        // Pridanie random dráh do systému
        for (int i = 0; i < 20; i++) {

            NovaOdletovaDrahaUiEntity entity = new NovaOdletovaDrahaUiEntity();
            entity.setId(i);
            entity.setDlzkaDrahy(NovaOdletovaDrahaUiEntity.randomDlzka());

            runwayManagementService.pridajNovuDrahu(entityMapper.fromNovaOdletovaDraha(entity));
        }

        for (PriletLietadlaUiEntity odlozenaEntita : odlozeneLietadlaPreBuduceSpracovanie) {

            LietadloEntity entity = new LietadloEntity();
            entity.setMedzinarodnyKod(odlozenaEntita.getMedzinarodnyKod());
            this.aircraftManagementService.vytvorPoziadavkuOPridelenieOdletovejDrahy(entity);
        }

        // Vytvor odlet zo všetkých 50% lietadiel čo boli v systéme
        // While true lebo pri odlete môže byť zase pridané lietadlo na dráhu ktoré čakalo
        while (true) {
            List<LietadloEntity> sDrahou = this.aircraftManagementService.getAllLietadlaSPridelenouOdletovouDrahou();
            if (sDrahou == null || sDrahou.size() == 0) {
                break;
            }

            for (LietadloEntity e : sDrahou) {
                LietadloTableUiEntity te = new LietadloTableUiEntity();
                te.setMedzinarodnyKod(e.getMedzinarodnyKod());
                this.aircraftManagementService.zaevidujOdletLietadla(te);
            }
        }

        // Pridanie ostatných 50% do čakacích lietadiel
        for (PriletLietadlaUiEntity odlozenaEntita : odlozeneLietadlaPreBuducePoziadanie) {

            LietadloEntity entity = new LietadloEntity();
            entity.setMedzinarodnyKod(odlozenaEntita.getMedzinarodnyKod());
            this.aircraftManagementService.vytvorPoziadavkuOPridelenieOdletovejDrahy(entity);
        }

    }
}
