﻿using System.Collections.Generic;
using FriBettingWebApplication.Dto.Response;

namespace FriBettingWebApplication.Services.Face
{
    public interface ISportsService
    {

        List<SportResponse> ListAllSports();
        List<OddTypeResponse> ListAllOddTypesForSport(int sportId);

    }
}
