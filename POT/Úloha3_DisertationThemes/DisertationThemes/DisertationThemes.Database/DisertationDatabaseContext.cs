﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

namespace DisertationThemes.Database
{
    public class DisertationDatabaseContext : DbContext
    {

        private static string ConnectionString =
            "Data Source=localhost;Initial Catalog=pos_disertation;Integrated Security=True";

        public DisertationDatabaseContext() : base(ConnectionString)
        {

        }

        public DbSet<Supervisor> Supervisors { get; set; }
        public DbSet<Theme> Themes { get; set; }
        public DbSet<StProgram> StPrograms { get; set; }

        public Supervisor GetSupervisorByName(string name)
        {
            return Supervisors
                    .SingleOrDefault(s => s.FullName.Equals(name));
        }

        public StProgram GetStProgramByNameAndFieldOfStudy(string name, string fieldOfStudy)
        {
            return StPrograms
                .SingleOrDefault(s => s.Name.Equals(name) && s.FieldOfStudy.Equals(fieldOfStudy));
        }

        public ResearchType GetResearchTypeByName(string name)
        {
            if (name == "základný výskum")
            {
                return ResearchType.AppliedResearch;
            }
            else if (name == "aplikovaný výskum a experimentálny vývoj")
            {
                return ResearchType.AppliedResearchExpDevelopment;
            }
            else if (name == "aplikovaný výskum")
            {
                return ResearchType.BasicResearch;
            }
            else
            {
                return ResearchType.Unspecified;
            }
        }
    }
}
