package sk.uniza.fri.lustiak.auds2.kataster.components.kataster;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.custom.CustomTextField;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.EditRealEstateUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.FindRealEstateUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.RealEstateUiTableEntity;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;
import sk.uniza.fri.lustiak.auds2.kataster.utils.StringToIntegerConverter;


public class EditaciaNehnutelnostiForm extends BusinessComponent {

    private RealEstateService realEstateService;

    private Binder<FindRealEstateUi> findEntityBinder;

    private FindRealEstateUi findEntity;

    private RealEstateInfoForm realEstateInfoForm;
    private RealEstateInfoForm realEstateInfoFormEditable;

    private VerticalLayout mainLayout;
    private HorizontalLayout oldNewValuesLayout;


    public EditaciaNehnutelnostiForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_4.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        this.findEntityBinder = new Binder<>();
        this.findEntity = new FindRealEstateUi();

        this.mainLayout = new VerticalLayout();

        TextField idTF = new CustomTextField("Identifikačné číslo", true);

        Button findRealEstatesButton = new Button("Vyhľadať nehnuteľnosti");
        findRealEstatesButton.addClickListener(e -> {
            try {
                this.findEntityBinder.writeBean(this.findEntity);
                najdiNehnutelnost(this.findEntity);
            } catch (ValidationException valEx) {
                this.findEntity.flush();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        Button editRealEstateButton = new Button("EDITOVAŤ NEHNUTEĽNOSŤ");
        editRealEstateButton.addClickListener(e -> {
           try {
               if (this.realEstateInfoFormEditable == null) {
                   return;
               }
               this.realEstateInfoFormEditable.writeEntity();
               EditRealEstateUi en = this.realEstateInfoFormEditable.getEditEntity();
               HandledOperationExecutor.submit(() -> {
                   OperationMarker om = this.realEstateService.editRealEstate(this.findEntity, en);
                   if (om.equals(OperationMarker.OPERATION_SUCCESFUL)) {
                       this.realEstateInfoFormEditable.lockIdField();
                       CustomNotification.notification("Entita bola zmenená");
                       this.oldNewValuesLayout.setVisible(false);
                   } else {
                       CustomNotification.error("Entitu sa nepodarilo editovať");
                   }
                   return om;
               });
           } catch (ValidationException valEx) {
               CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
           }
        });
        editRealEstateButton.addStyleName("friendly");

        this.findEntityBinder.forField(idTF)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(FindRealEstateUi::getIdentifikacneCislo, FindRealEstateUi::setIdentifikacneCislo);

        mainLayout.addComponent(new HorizontalLayout(idTF, findRealEstatesButton, editRealEstateButton));

        this.parentComponent.showComponent(new Panel("Editácia NEHNUTEĽNOSTI podľa ID", mainLayout));

    }

    private void najdiNehnutelnost(FindRealEstateUi idNehnutelnosti) {

        HandledOperationExecutor.submit(() -> {
            RealEstateUiTableEntity en = this.realEstateService.findRealEstate(idNehnutelnosti);

            if (this.realEstateInfoForm == null) {
                oldNewValuesLayout = new HorizontalLayout();
                this.realEstateInfoForm = new RealEstateInfoForm(en, Boolean.FALSE);
                this.realEstateInfoFormEditable = new RealEstateInfoForm(en, Boolean.TRUE);
                Panel oldValues = new Panel("Staré hodnoty", this.realEstateInfoForm);
                oldValues.setIcon(FontAwesome.LOCK);
                oldNewValuesLayout.addComponent(oldValues);
                Panel newValues = new Panel("Nové hodnoty", this.realEstateInfoFormEditable);
                newValues.setIcon(FontAwesome.UNLOCK);
                oldNewValuesLayout.addComponent(newValues);
                this.mainLayout.addComponent(oldNewValuesLayout);
            } else {
                this.realEstateInfoForm.initFrom(en);
                this.realEstateInfoFormEditable.initFrom(en);

                this.oldNewValuesLayout.setVisible(true);
            }
            return OperationMarker.OPERATION_SUCCESFUL;
        });
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_4.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }


}
