﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace FriBettingWebApplication.Dto.Response
{
    public class SportListResponse
    {

        [JsonProperty("sports")]
        public List<SportResponse> SportsListResponse { get; set; }

    }
}