package sk.uniza.fri.lustiak.auds2.kataster.components.system;

import com.vaadin.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;

public class LoadStateFromFileForm extends BusinessComponent {

    private RealEstateService realEstateService;

    public LoadStateFromFileForm(RepaintableComponent component) {
        super(component, MENU_TITLE.SYSTEM_MENU_2.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        VerticalLayout layout = new VerticalLayout();

        TextField fileNameTF = new TextField("Názov súboru");

        Button importButton = new Button("Načítať stav zo súboru");
        importButton.addStyleName("friendly");
        importButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {
                this.realEstateService.loadFromFile(fileNameTF.getValue());
                CustomNotification.notification("Stav bol úspešne načítaný");
                this.parentComponent.updateFileState();
                return OperationMarker.OPERATION_SUCCESFUL;
            });
        });

        layout.addComponent(fileNameTF);
        layout.addComponent(importButton);

        this.parentComponent.showComponent(new Panel("Import systému zo súboru", layout));

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_2.getTitle();
    }
}
