package sk.uniza.fri.lustyno.datastructures.linearhashing.config;

import java.io.*;

public class HeapFileConfig {

    public String heapFileName;
    public Integer lastOffsetUsed;
    public Integer recordSizeInBytes;
    public Integer batchSize;


    public HeapFileConfig() {

    }

    public void saveToFile() {
        String fileName = this.heapFileName + "_conf.conf";

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(fileName, false));
            bw.write(heapFileName);
            bw.newLine();
            bw.write(lastOffsetUsed.toString());
            bw.newLine();
            bw.write(recordSizeInBytes.toString());
            bw.newLine();
            bw.write(batchSize.toString());

            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static HeapFileConfig fromFile(String filename) {
        HeapFileConfig fc = new HeapFileConfig();

        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(filename));

            fc.heapFileName = br.readLine();
            fc.lastOffsetUsed = new Integer(br.readLine());
            fc.recordSizeInBytes = new Integer(br.readLine());
            fc.batchSize = new Integer(br.readLine());

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fc;
    }

}
