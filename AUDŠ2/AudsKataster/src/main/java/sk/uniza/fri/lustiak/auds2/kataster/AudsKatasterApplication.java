package sk.uniza.fri.lustiak.auds2.kataster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AudsKatasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(AudsKatasterApplication.class, args);
	}

}
