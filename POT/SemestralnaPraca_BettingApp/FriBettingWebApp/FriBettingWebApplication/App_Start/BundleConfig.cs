﻿using System.Web.Optimization;

namespace FriBettingWebApplication
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/notify").Include(
                "~/Scripts/notify.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            // Project specific JS
            bundles.Add(new ScriptBundle("~/bundles/services/sportsService").Include(
                "~/Scripts/Services/sportsServiceUi.js"));
            bundles.Add(new ScriptBundle("~/bundles/services/usersService").Include(
                "~/Scripts/Services/usersServiceUi.js"));
            bundles.Add(new ScriptBundle("~/bundles/services/competitorsService").Include(
                "~/Scripts/Services/competitorsServiceUi.js"));
            bundles.Add(new ScriptBundle("~/bundles/services/matchesService").Include(
                "~/Scripts/Services/matchesServiceUi.js"));
            bundles.Add(new ScriptBundle("~/bundles/services/ticketsService").Include(
                "~/Scripts/Services/ticketsServiceUi.js"));

            bundles.Add(new ScriptBundle("~/bundles/components/dropdown").Include(
                "~/Scripts/FancyComponents/Dropdown/dropdown.js"));

            // Project specific CSS
            bundles.Add(new StyleBundle("~/bundles/components/dropdown/css").Include(
                "~/Scripts/FancyComponents/Dropdown/dropdown.css"));
            bundles.Add(new StyleBundle("~/bundles/components/fancyform/css").Include(
                "~/Scripts/FancyComponents/FancyForm/fancyForm.css"));

        }
    }
}
