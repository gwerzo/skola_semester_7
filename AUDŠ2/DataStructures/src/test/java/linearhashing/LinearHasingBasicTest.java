package linearhashing;

import linearhashing.entities.CarHashingEntity;
import linearhashing.entities.IntegerEntity;
import org.junit.jupiter.api.*;
import org.opentest4j.AssertionFailedError;
import sk.uniza.fri.lustyno.datastructures.entity.keys.DoubleStringKey;
import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.File;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Record;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.FromBytesFactory;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.SPLIT_VARIANT;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LinearHasingBasicTest {

    private String DATABASE_VERSION = "TEST_VERSION_" + "1";

    @Test
    @Order(1)
    public void testOtvoreniaSuborov() {
        File<DoubleStringKey, CarHashingEntity> filero = new File<>(
                DATABASE_VERSION, 10, 3,
                CarHashingEntity.class,
                SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR,
                new FromBytesFactory<CarHashingEntity>() {
                    @Override
                    public CarHashingEntity fromBytes(byte[] bytes) {
                        return CarHashingEntity.fromByteArray(bytes);
                    }

                    @Override
                    public CarHashingEntity emptyInvalidInstance() {
                        return CarHashingEntity.emptyInvalidInstance();
                    }
                });

        CarHashingEntity en1 = new CarHashingEntity("1", "A", 4, false, LocalDateTime.now(), LocalDateTime.now());
        CarHashingEntity en2 = new CarHashingEntity("11111111111111111", "BBBBBBB", 4, false, LocalDateTime.now(), LocalDateTime.now());
        CarHashingEntity en3 = new CarHashingEntity("3", "C", 4, false, LocalDateTime.now(), LocalDateTime.now());
        CarHashingEntity en4 = new CarHashingEntity("4", "D", 4, false, LocalDateTime.now(), LocalDateTime.now());

        filero.saveRecord(en1);
        filero.saveRecord(en2);
        filero.saveRecord(en3);
        filero.saveRecord(en4);

        filero.find(en1);
        filero.find(en2);
        filero.find(en3);
        filero.find(en4);

        filero.close();
    }

    @Test
    @Order(2)
    public void testOverenia_BUG() {
        File<DoubleStringKey, CarHashingEntity> filero = new File<>(
                DATABASE_VERSION, 1, 1,
                CarHashingEntity.class,
                SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR,
                new FromBytesFactory<CarHashingEntity>() {
                    @Override
                    public CarHashingEntity fromBytes(byte[] bytes) {
                        return CarHashingEntity.fromByteArray(bytes);
                    }

                    @Override
                    public CarHashingEntity emptyInvalidInstance() {
                        return CarHashingEntity.emptyInvalidInstance();
                    }
                });

        CarHashingEntity en1 = new CarHashingEntity("H935Z26$$$$$$$$$$", "BF8CEOO", 4, false, LocalDateTime.now(), LocalDateTime.now());
        CarHashingEntity en2 = new CarHashingEntity("71BTEX7$$$$$$$$$$", "8LD9KMN", 4, false, LocalDateTime.now(), LocalDateTime.now());
        CarHashingEntity en3 = new CarHashingEntity("JH9Y2MR$$$$$$$$$$", "WOWTENU", 4, false, LocalDateTime.now(), LocalDateTime.now());
        CarHashingEntity en4 = new CarHashingEntity("D7IDSEG$$$$$$$$$$", "Q1K1VH9", 4, false, LocalDateTime.now(), LocalDateTime.now());
        CarHashingEntity en5 = new CarHashingEntity("ER6KFQV$$$$$$$$$$", "7FCHZN4", 4, false, LocalDateTime.now(), LocalDateTime.now());
        CarHashingEntity en6 = new CarHashingEntity("BT9ARTV$$$$$$$$$$", "WVDNRPL", 4, false, LocalDateTime.now(), LocalDateTime.now());
        CarHashingEntity en7 = new CarHashingEntity("96WW53J$$$$$$$$$$", "ER2RQ7W", 4, false, LocalDateTime.now(), LocalDateTime.now());
        CarHashingEntity en8 = new CarHashingEntity("SJY26ET$$$$$$$$$$", "WOURH9F", 4, false, LocalDateTime.now(), LocalDateTime.now());
        CarHashingEntity en9 = new CarHashingEntity("7YCPAB8$$$$$$$$$$", "KQTSEIF", 4, false, LocalDateTime.now(), LocalDateTime.now());
        CarHashingEntity en10 = new CarHashingEntity("7JDIM6Z$$$$$$$$$$", "EB2841R", 4, false, LocalDateTime.now(), LocalDateTime.now());

        filero.saveRecord(en1);
        filero.saveRecord(en2);
        filero.saveRecord(en3);
        filero.saveRecord(en4);
        filero.saveRecord(en5);
        filero.saveRecord(en6);
        filero.saveRecord(en7);
        filero.saveRecord(en8);
        filero.saveRecord(en9);
        filero.saveRecord(en10);

        CarHashingEntity entity;
        entity = (CarHashingEntity) filero.find(en1);
        Assertions.assertEquals(en1.getKey().getValue(), entity.getKey().getValue());
        entity = (CarHashingEntity) filero.find(en2);
        Assertions.assertEquals(en2.getKey().getValue(), entity.getKey().getValue());
        entity = (CarHashingEntity) filero.find(en3);
        Assertions.assertEquals(en3.getKey().getValue(), entity.getKey().getValue());
        entity = (CarHashingEntity) filero.find(en4);
        Assertions.assertEquals(en4.getKey().getValue(), entity.getKey().getValue());
        entity = (CarHashingEntity) filero.find(en5);
        Assertions.assertEquals(en5.getKey().getValue(), entity.getKey().getValue());
        entity = (CarHashingEntity) filero.find(en6);
        Assertions.assertEquals(en6.getKey().getValue(), entity.getKey().getValue());
        entity = (CarHashingEntity) filero.find(en7);
        Assertions.assertEquals(en7.getKey().getValue(), entity.getKey().getValue());
        entity = (CarHashingEntity) filero.find(en8);
        Assertions.assertEquals(en8.getKey().getValue(), entity.getKey().getValue());
        entity = (CarHashingEntity) filero.find(en9);
        Assertions.assertEquals(en9.getKey().getValue(), entity.getKey().getValue());
        entity = (CarHashingEntity) filero.find(en10);
        Assertions.assertEquals(en10.getKey().getValue(), entity.getKey().getValue());

        filero.close();
    }

    @Test
    @Order(3)
    public void testOverenia_RANDOMIZED() {
        Random rnd = new Random();

        File<DoubleStringKey, CarHashingEntity> filero = new File<>(
                DATABASE_VERSION, 4, 2,
                CarHashingEntity.class,
                SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR,
                new FromBytesFactory<CarHashingEntity>() {
                    @Override
                    public CarHashingEntity fromBytes(byte[] bytes) {
                        return CarHashingEntity.fromByteArray(bytes);
                    }

                    @Override
                    public CarHashingEntity emptyInvalidInstance() {
                        return CarHashingEntity.emptyInvalidInstance();
                    }
                });

        List<CarHashingEntity> toFindEntities = new ArrayList<>();

        for (int i = 0; i < 10000; i++) {
             CarHashingEntity che = new CarHashingEntity(
             CarHashingEntity.randomVIN(), CarHashingEntity.randomSPZ(), 4, true, LocalDateTime.now(), LocalDateTime.now());
             filero.saveRecord(che);
             if (rnd.nextInt(10) >= -1) {
                 toFindEntities.add(che);
             }
        }

        for (CarHashingEntity entity : toFindEntities) {
            Record<DoubleStringKey> foundEntity = filero.find(entity);
            Assertions.assertEquals(foundEntity.getKey().getValue(), entity.getKey().getValue());
        }

        filero.close();
    }

    @Test
    @Order(4)
    public void withIntegerEntitiesTest() {

        File<IntegerKey, IntegerEntity> filero = new File<>(
                DATABASE_VERSION, 1, 1,
                IntegerEntity.class,
                SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR,
                new FromBytesFactory<IntegerEntity>() {
                    @Override
                    public IntegerEntity fromBytes(byte[] bytes) {
                        return IntegerEntity.fromByteArray(bytes);
                    }

                    @Override
                    public IntegerEntity emptyInvalidInstance() {
                        return new IntegerEntity(-1, -1);
                    }
                }
        );

        filero.saveRecord(new IntegerEntity(0,0));
        filero.saveRecord(new IntegerEntity(4,4));
        filero.saveRecord(new IntegerEntity(1,1));
        filero.saveRecord(new IntegerEntity(2,2));


        filero.close();
    }

    @Test
    @Order(5)
    public void RANDOM_INTEGERS_INSERT() {
        Random rnd = new Random();

        File<IntegerKey, IntegerEntity> filero = new File<>(
                DATABASE_VERSION, 4, 2,
                IntegerEntity.class,
                SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR,
                new FromBytesFactory<IntegerEntity>() {
                    @Override
                    public IntegerEntity fromBytes(byte[] bytes) {
                        return IntegerEntity.fromByteArray(bytes);
                    }

                    @Override
                    public IntegerEntity emptyInvalidInstance() {
                        return new IntegerEntity(-1, -1);
                    }
                }
        );
        List<IntegerEntity> entities = new ArrayList<>();
        List<Integer> usedInts = new ArrayList<>();
        for (int y = 0; y < 10000; y++) {
            Integer rndInt = rnd.nextInt(100000000);
            while (usedInts.contains(rndInt)) {
                rndInt = rnd.nextInt(100000000);
            }
            IntegerEntity en = new IntegerEntity(rndInt, rndInt);
            entities.add(en);
            usedInts.add(rndInt);
            filero.saveRecord(en);
        }

        for (int y = 0; y < 10000; y++) {
            IntegerEntity e = entities.get(y);
            IntegerEntity e2 = (IntegerEntity)filero.find(e);
            try {
                Assertions.assertEquals(e.getKey().getValue(), e2.getKey().getValue());
            } catch (AssertionFailedError | NullPointerException ex) {
                for (IntegerEntity ent : entities) {
                    System.out.println("val: " + ent.getKey().getValue());
                }
                fail();
            }
        }
        filero.close();
    }

    @Test
    @Order(6)
    public void BUG() {
        File<IntegerKey, IntegerEntity> filero = new File<>(
                DATABASE_VERSION, 1, 1,
                IntegerEntity.class,
                SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR,
                new FromBytesFactory<IntegerEntity>() {
                    @Override
                    public IntegerEntity fromBytes(byte[] bytes) {
                        return IntegerEntity.fromByteArray(bytes);
                    }

                    @Override
                    public IntegerEntity emptyInvalidInstance() {
                        return new IntegerEntity(-1, -1);
                    }
                });

        IntegerEntity en1 = new IntegerEntity(105196155, 105196155);
        IntegerEntity en2 = new IntegerEntity(46524582, 46524582);
        IntegerEntity en3 = new IntegerEntity(64027402, 64027402);
        IntegerEntity en4 = new IntegerEntity(53958480, 53958480);
        IntegerEntity en5 = new IntegerEntity(54959756, 54959756);
        IntegerEntity en6 = new IntegerEntity(88732130, 88732130);
        IntegerEntity en7 = new IntegerEntity(73628817, 73628817);
        IntegerEntity en8 = new IntegerEntity(19924843, 19924843);
        IntegerEntity en9 = new IntegerEntity(18192660, 18192660);
        IntegerEntity en10 = new IntegerEntity(59737026, 59737026);

        filero.saveRecord(en1);
        filero.saveRecord(en2);
        filero.saveRecord(en3);
        filero.saveRecord(en4);
        filero.saveRecord(en5);
        filero.saveRecord(en6);
        filero.saveRecord(en7);
        filero.saveRecord(en8);
        filero.saveRecord(en9);
        filero.saveRecord(en10);

        IntegerEntity entity;
        entity = (IntegerEntity) filero.find(en1);
        Assertions.assertEquals(en1.getKey().getValue(), entity.getKey().getValue());
        entity = (IntegerEntity) filero.find(en2);
        Assertions.assertEquals(en2.getKey().getValue(), entity.getKey().getValue());
        entity = (IntegerEntity) filero.find(en3);
        Assertions.assertEquals(en3.getKey().getValue(), entity.getKey().getValue());
        entity = (IntegerEntity) filero.find(en4);
        Assertions.assertEquals(en4.getKey().getValue(), entity.getKey().getValue());
        entity = (IntegerEntity) filero.find(en5);
        Assertions.assertEquals(en5.getKey().getValue(), entity.getKey().getValue());
        entity = (IntegerEntity) filero.find(en6);
        Assertions.assertEquals(en6.getKey().getValue(), entity.getKey().getValue());
        entity = (IntegerEntity) filero.find(en7);
        Assertions.assertEquals(en7.getKey().getValue(), entity.getKey().getValue());
        entity = (IntegerEntity) filero.find(en8);
        Assertions.assertEquals(en8.getKey().getValue(), entity.getKey().getValue());
        entity = (IntegerEntity) filero.find(en9);
        Assertions.assertEquals(en9.getKey().getValue(), entity.getKey().getValue());
        entity = (IntegerEntity) filero.find(en10);
        Assertions.assertEquals(en10.getKey().getValue(), entity.getKey().getValue());

        filero.close();
    }

    @Test
    @Order(7)
    public void test_COMPLETELY_RANDOMIZED_OPERATIONS() {

        File<IntegerKey, IntegerEntity> filero = new File<>(
                DATABASE_VERSION, 10, 4,
                IntegerEntity.class,
                SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR,
                new FromBytesFactory<IntegerEntity>() {
                    @Override
                    public IntegerEntity fromBytes(byte[] bytes) {
                        return IntegerEntity.fromByteArray(bytes);
                    }

                    @Override
                    public IntegerEntity emptyInvalidInstance() {
                        return new IntegerEntity(-1, -1);
                    }
                });

        Random rnd = new Random();

        LinkedList<IntegerEntity> entitiesToUpdate = new LinkedList<>();
        LinkedList<IntegerEntity> entitiesToFind = new LinkedList<>();

        List<Integer> usedInts = new ArrayList<>();

        // Warmup
        for (int i = 0; i < 10000; i++) {

            Integer rndInt = rnd.nextInt(100000000);
            while (usedInts.contains(rndInt)) {
                rndInt = rnd.nextInt(100000000);
            }
            //System.out.println("Adding entity " + i);

            usedInts.add(rndInt);

            IntegerEntity inty = new IntegerEntity(rndInt, rndInt);

            Integer switcher = rnd.nextInt(10);

            if (switcher == 0) {
                entitiesToFind.add(inty);
            } else if (switcher == 1) {
                entitiesToUpdate.add(inty);
            }

            filero.saveRecord(inty);
        }

        while (!entitiesToFind.isEmpty() || !entitiesToUpdate.isEmpty()) {

            //System.out.println("Looping " + entitiesToFind.size() + " and " + entitiesToUpdate.size());
            Integer rndOp = rnd.nextInt(3);

            if (rndOp == 0) {
                IntegerEntity en;
                try {
                    en = entitiesToFind.removeFirst();
                } catch (NoSuchElementException e) {
                    continue;
                }
                IntegerEntity found = (IntegerEntity) filero.find(en);
                Assertions.assertEquals(en.getKey().getValue(), found.getKey().getValue());
            } else if (rndOp == 1) {
                IntegerEntity en;
                try {
                    en = entitiesToUpdate.removeFirst();
                } catch (NoSuchElementException e) {
                    continue;
                }
                Integer randData = rnd.nextInt(100000);
                en.setDataValue(randData);
                filero.update(en);
                IntegerEntity found = (IntegerEntity) filero.find(en);
                Assertions.assertEquals(found.getDataValue(), en.getDataValue());
            } else {
                Integer newInt = rnd.nextInt(100000000);
                while (usedInts.contains(newInt)) {
                    newInt = rnd.nextInt(100000000);
                }
                usedInts.add(newInt);
                filero.saveRecord(new IntegerEntity(newInt, newInt));
            }
        }
        filero.close();
    }

    @BeforeEach
    public void clearFiles() {
        FileWriter fwOb = null;
        PrintWriter pwOb = null;
        try {
            fwOb = new FileWriter(this.DATABASE_VERSION + ".lldb", false);
            pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            pwOb.close();
            fwOb.close();
        } catch (IOException e) {

        }


        try {
            fwOb = new FileWriter("overflow_" + this.DATABASE_VERSION + ".lldb", false);
            pwOb = new PrintWriter(fwOb, false);
            pwOb.flush();
            pwOb.close();
            fwOb.close();
        } catch (IOException e) {

        }
    }
}
