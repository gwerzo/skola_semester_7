package sk.uniza.fri.lustyno.AudsLetisko.components.common;

public enum MENU_TITLE {

    MENU_1("1. Zaevidovanie príletu lietadla"),
    MENU_2("2. Zaevidovanie požiadavky na pridelenie odletovej dráhy"),
    MENU_3("3. Vyhľadanie lietadla na pridelenie odletovej dráhy"),
    MENU_3_1("3.1 Vyhľadanie lietadla podľa výrobcu a medzinárodného kódu"),
    MENU_4("4. Vyhľadanie lietadla na pridelenie dráhy na odletovej dráhe"),
    MENU_5("5. Evidencia odletu lietadla"),
    MENU_6("6. Lietadlá čakajúce na pridelenie odletovej dráhy"),
    MENU_7("7. Lietadlá čakajúce na pridelenie KONKRÉTNEJ odletovej dráhy"),
    MENU_8("8. Zmena priority lietadla"),
    MENU_9("9. Výpis všetkých odletových dráh"),
    MENU_10("10. Vyradenie lietadla"),

    //-----------------------------------------------------------------------------
    SYSTEM_MENU_1("1. Pridanie odletovej dráhy"),
    SYSTEM_MENU_2("2. Zmena času systému"),
    SYSTEM_MENU_3("3. Export systému"),
    SYSTEM_MENU_4("4. Import systému"),
    SYSTEM_MENU_5("5. Vygenerovanie dát"),
    SYSTEM_MENU_6("6. Všetky lietadlá v systéme"),
    SYSTEM_MENU_7("7. Všetky dráhy v systéme");

    private String title;

    MENU_TITLE(String s) {
        title = s;
    }

    public String getTitle() {
        return title;
    }
}
