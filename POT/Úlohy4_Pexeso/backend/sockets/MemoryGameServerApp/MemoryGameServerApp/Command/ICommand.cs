﻿using MemoryGameServerApp.Server;
using SuperWebSocket;

namespace MemoryGameServerApp.Command
{
    public interface ICommand
    {
        void executeCommand(string usernameOfCommandCaller, MemoryGameServer memoryGameServer, 
            WebSocketSession callerSocketSession);
    }
}
