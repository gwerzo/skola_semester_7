package sk.uniza.fri.lustyno.datastructures.linearhashing.heapfile;

import sk.uniza.fri.lustyno.datastructures.exception.DataStructureWrongUsageException;
import sk.uniza.fri.lustyno.datastructures.linearhashing.config.HeapFileConfig;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Record;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.FromBytesFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Reprezentácia heap file,
 * kde dáta sú nijak neusporiadané,
 * vracia pri vkladaní len offsety
 * na vložené záznamy
 *
 * @param <R> typ záznamov
 */
public class HeapFile<R extends Record> {

    private Integer lastOffsetUsed;

    private Class clazz;

    private Integer recordSizeInBytes;

    private Integer batchSize;

    private FromBytesFactory<R> entityFactory;

    private HeapFileHandler fh;

    /**
     * Vytvorenie nového súboru
     * na disku, prázneho
     *
     * @param heapFileName - názov súboru
     * @param clazz - Class-a entit storovaných v súbore
     * @param batchSize - Po ako veľkých batchoch sa má vyťahovať zo súboru
     * @param factory - Factory pre tieto entity
     */
    public HeapFile(String heapFileName, Class<?> clazz, Integer batchSize, FromBytesFactory<R> factory) {
        this.lastOffsetUsed = 0;
        this.clazz = clazz;
        this.batchSize = batchSize;
        this.recordSizeInBytes = R.getSize(this.clazz);
        this.entityFactory = factory;

        this.fh = new HeapFileHandler(heapFileName);
    }

    public HeapFile(HeapFileConfig heapFileConfig, Class<?> clazz, FromBytesFactory<R> factory) {
        this.lastOffsetUsed = heapFileConfig.lastOffsetUsed;
        this.clazz = clazz;
        this.recordSizeInBytes = heapFileConfig.recordSizeInBytes;
        this.entityFactory = factory;
        this.batchSize = heapFileConfig.batchSize;

        this.fh = new HeapFileHandler(heapFileConfig);
    }

    /**
     * Kvôli spracovávaniu všetkých entít
     * v súbore sa zo súboru vyťahujú entity
     * v batchoch namiesto toho, aby sa
     * vyťahovali jedna po druhej sekvenčne
     *
     * @return - zoznam batchov, ktoré obsahujú entity z HeapFilu
     */
    public List<Batch<R>> getBatchedEntities() {
        List<Batch<R>> batchList = new LinkedList<>();

        int startIndexToPullEntities = 0;
        int endIndexToPullEntities = this.batchSize * this.recordSizeInBytes;

        if (endIndexToPullEntities > this.lastOffsetUsed) {

            byte[] bytes = this.fh.readBytes(startIndexToPullEntities, this.lastOffsetUsed + this.recordSizeInBytes);

            List<R> entities = new ArrayList<>();
            for (int i = 0; i < this.lastOffsetUsed / this.recordSizeInBytes; i++) {
                R r = this.entityFactory.fromBytes(Arrays.copyOfRange(bytes, startIndexToPullEntities, (startIndexToPullEntities + (i+1) * this.recordSizeInBytes)));
                entities.add(r);
            }
            batchList.add(new Batch<>(entities, startIndexToPullEntities));
            return batchList;
        }

        while (endIndexToPullEntities < this.lastOffsetUsed) {
            byte[] bytes = this.fh.readBytes(startIndexToPullEntities, endIndexToPullEntities);

            int byteOffsetCounter = 0;
            List<R> entities = new ArrayList<>();
            for (int i = 0; i < this.batchSize; i++) {
                R r = this.entityFactory.fromBytes(Arrays.copyOfRange(bytes, byteOffsetCounter, (byteOffsetCounter + (i+1) * this.recordSizeInBytes)));
                entities.add(r);
                byteOffsetCounter += this.recordSizeInBytes;
            }

            Batch<R> b = new Batch<>(entities, startIndexToPullEntities);
            batchList.add(b);

            startIndexToPullEntities = endIndexToPullEntities;
            endIndexToPullEntities = startIndexToPullEntities + this.batchSize * this.recordSizeInBytes;
        }
        return batchList;
    }

    public void saveConfiguration() {
        HeapFileConfig hfc = this.getActualConfiguration();
        hfc.saveToFile();
    }


    public HeapFileConfig getActualConfiguration() {
        HeapFileConfig hfc = new HeapFileConfig();
        hfc.recordSizeInBytes = this.recordSizeInBytes;
        hfc.lastOffsetUsed = this.lastOffsetUsed;
        hfc.batchSize = this.batchSize;

        this.fh.fillHeapFileConfig(hfc);

        return hfc;
    }


    /**
     * Pridá záznam do súboru,
     * a vráti offset, na ktorý ho uložilo
     *
     * @param r - záznam
     * @return - index/offset do pamäte, kam bol záznam uložený
     */
    public Integer addRecord(R r) {
        r.setValid(true);
        this.fh.writeBytes(r.toBytesArray(), lastOffsetUsed);
        Integer toRet = lastOffsetUsed;
        lastOffsetUsed += this.recordSizeInBytes;
        return toRet;
    }

    /**
     * Vráti záznam typu R z offsetu,
     * ktorý je motóde predaný ako parameter
     *
     * @param offsetInFile - offset v súbore
     * @return - nájdený záznam
     */
    public R getRecordAtOffset(Integer offsetInFile) {
        byte[] readBytes = this.fh.readBytes(offsetInFile, this.recordSizeInBytes);
        return this.entityFactory.fromBytes(readBytes);
    }


    /**
     * Zmení údaje entity v súbore,
     * vykoná predtým ale kontrolu
     * na porovnanie kľúčov starej a novej entity,
     * aby nebolo možné zmeniť unikátne kľúče entity,
     * na ktorú môžu byť viazané indexy
     *
     * @param entity - nové údaje entity
     * @param offsetInFile - offset v súbore, na ktorom sa entita nacházda
     */
    public void updateRecordWithoutConstraintsChange(R entity, Integer offsetInFile) {

        // Entita na danom indexe musí mať kľúč zhodný s entitou z parametra
        R recordInFile = this.getRecordAtOffset(offsetInFile);
        if (!recordInFile.getKey().getValue().equals(entity.getKey().getValue())) {
            throw new DataStructureWrongUsageException(this.getClass().getName(), "updateRecord",
                    "Kľúč entity predanej v parametri sa nezhoduje s kľúčom entity nachádzajúcej sa na offsete " + offsetInFile + "\n" +
                    "Kľúč entity z parametra: " + entity.getKey().getValue() + "\n" +
                    "Kľúč entity v súbore: " + recordInFile.getKey().getValue()
            );
        }

        this.fh.writeBytes(entity.toBytesArray(), offsetInFile);
    }

    public void close() {
        this.fh.close();
    }

}
