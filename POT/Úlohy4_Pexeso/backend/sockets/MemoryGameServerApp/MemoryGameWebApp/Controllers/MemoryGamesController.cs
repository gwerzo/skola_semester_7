﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemoryGameDaoApp.Dal;
using MemoryGameWebApp.DTO;

namespace MemoryGameWebApp.Controllers
{
    public class MemoryGamesController : Controller
    {
        public MemoryGamesDao MemoryGamesDao { get; set; }

        public MemoryGamesController()
        {
            this.MemoryGamesDao = new MemoryGamesDao();
        }

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Mapping ktorý vráti vyfiltrované hry podľa parametrov
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet("api/games/filter")]
        public async Task<List<MemoryGamesListResponseDTO>> GetMemoryGamesWithFilter(MemoryGameFilterRequestDTO filter)
        {
            var filteredGames = await this.MemoryGamesDao.GetMemoryGamesFiltered(filter.GameId, filter.PlayerId, filter.SortingAttr, filter.SortingOrder);
            var listDtoResponse = filteredGames.Select(c => new MemoryGamesListResponseDTO
            {
                GameId = c.MemoryGameId,
                Player1 = c.Player1.Username,
                Player2 = c.Player2.Username,
                Winner = c.Winner.Username,
                GameStartDateTime = c.GameStartTime,
                GameLengthInSeconds = c.TotalTime,
                StepsCount = c.StepsCount
            }).ToList();

            return listDtoResponse;
        }

        /// <summary>
        /// Mapping, ktorý vráti detail hry podľa jej ID aj s jej krokmi
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet("api/games/detail")]
        public async Task<GameInfoResponseDTO> GetMemoryGameDetails(GameInfoRequestDTO request)
        {
            var gameDetail = await this.MemoryGamesDao.GetMemoryGameDetail(request.GameId);

            GameInfoResponseDTO toMapToDto = new GameInfoResponseDTO();
            toMapToDto.Player1Username = gameDetail.Player1.Username;
            toMapToDto.Player2Username = gameDetail.Player2.Username;
            toMapToDto.GameWidth = gameDetail.GameType.XLength;
            toMapToDto.GameHeight = gameDetail.GameType.YLength;
            toMapToDto.GameStepsCount = gameDetail.StepsCount;
            toMapToDto.GameSteps = gameDetail.MemoryGameSteps.Select(e =>
                new GameInfoResponseDTO.GameInfoStepResponseDTO
                {
                    XHit = e.Hit_X,
                    YHit = e.Hit_Y,
                    StepCount = e.StepCount
                }).ToList();

            return toMapToDto;
        }

    }
}
