package sk.uniza.fri.lustiak.auds2.kataster.entities.bo;

import lombok.Data;
import sk.uniza.fri.lustiak.auds2.kataster.entities.wrapper.RealEstateWrapper;

import java.util.LinkedList;
import java.util.List;

@Data
public class Parcel {

    private int cisloParcely;
    private String popis;
    private List<RealEstateWrapper> nehnutelnosti;

    public void pridajNehnutelnost(RealEstateWrapper realEstate) {
        if (this.nehnutelnosti == null) {
            this.nehnutelnosti = new LinkedList<>();
        }
        this.nehnutelnosti.add(realEstate);
    }

    public void odoberNehnutelnost(RealEstateWrapper realEstate) {
        if (this.nehnutelnosti != null) {
            RealEstateWrapper toDel = null;
            for (RealEstateWrapper neh : this.nehnutelnosti) {
                if (neh.getCoordinates2DKey().getValue().getUniqueIdentifier()
                        == realEstate.getCoordinates2DKey().getValue().getUniqueIdentifier()) {
                    toDel = neh;
                    break;
                }
            }
            if (toDel != null) {
                this.nehnutelnosti.remove(toDel);
            }
        }
    }

}
