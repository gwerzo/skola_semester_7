package sk.uniza.fri.lustyno.datastructures.exception;

public class DataStructureOperationFailedException extends RuntimeException {

    public DataStructureOperationFailedException(String className, String methodName, String childExceptionStack) {
        super("Operácia " + methodName + " v triede " + className + " sa nepodarila.\n" +
                "Exception\n" + childExceptionStack);
    }

}
