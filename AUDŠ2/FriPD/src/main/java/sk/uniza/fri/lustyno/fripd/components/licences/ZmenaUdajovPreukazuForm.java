package sk.uniza.fri.lustyno.fripd.components.licences;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.fripd.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.fripd.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.fripd.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.PridanieVodicskehoPreukazuEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.lincense.ZmenaUdajovPreukazuEntityUi;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.ZmenaUdajovVozidlaEntityUi;
import sk.uniza.fri.lustyno.fripd.service.DrivingLicenseService;
import sk.uniza.fri.lustyno.fripd.service.UIServices;
import sk.uniza.fri.lustyno.fripd.service.VehicleService;
import sk.uniza.fri.lustyno.fripd.utils.CustomNotification;
import sk.uniza.fri.lustyno.fripd.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.fripd.utils.OperationMarker;
import sk.uniza.fri.lustyno.fripd.utils.converters.StringToIntegerConverter;

public class ZmenaUdajovPreukazuForm extends BusinessComponent {


    private DrivingLicenseService drivingLicenseService;

    private Binder<ZmenaUdajovPreukazuEntityUi> binder;
    private ZmenaUdajovPreukazuEntityUi entity;

    public ZmenaUdajovPreukazuForm(RepaintableComponent parentComponent) {
        super(parentComponent, MENU_TITLE.CARDS_MENU_3.getTitle());
        this.drivingLicenseService = UIServices.getDrivingLicenseService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new ZmenaUdajovPreukazuEntityUi();
        this.binder = new Binder<>(ZmenaUdajovPreukazuEntityUi.class);

        VerticalLayout layout = new VerticalLayout();

        FormLayout formLayout = new FormLayout();

        TextField evcKodTf = new TextField("EVČ");
        TextField menoVodicaTf = new TextField("Meno vodiča");
        TextField priezviskoVodicaTf = new TextField("Priezvisko vodiča");
        CheckBox zakazViestVodidloChB = new CheckBox("Zákaz viesť vozidlo");
        DateField platnostVodickehoDf = new DateField("Platnosť vodičského preukazu");
        TextField pocetDopravnychPriestupkovTf = new TextField("Počet dopravných priestupkov");

        Button vytvorit = new Button("Zmeniť údaje vodičského preukazu");

        vytvorit.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                zmenUdajeVodicskehoPreukazu(this.entity);
            } catch (ValidationException ex) {
                ex.printStackTrace();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        formLayout.addComponent(evcKodTf);
        formLayout.addComponent(menoVodicaTf);
        formLayout.addComponent(priezviskoVodicaTf);
        formLayout.addComponent(zakazViestVodidloChB);
        formLayout.addComponent(platnostVodickehoDf);
        formLayout.addComponent(pocetDopravnychPriestupkovTf);

        formLayout.addComponent(vytvorit);

        layout.addComponent(formLayout);

        binder.forField(evcKodTf)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(ZmenaUdajovPreukazuEntityUi::getEvcVodica, ZmenaUdajovPreukazuEntityUi::setEvcVodica);

        binder.forField(menoVodicaTf)
                .asRequired()
                .withValidator(new StringLengthValidator("Mena vodiča musí byť v rozmedzí 1 - 35", 1, 35))
                .bind(ZmenaUdajovPreukazuEntityUi::getMenoVodica, ZmenaUdajovPreukazuEntityUi::setMenoVodica);

        binder.forField(priezviskoVodicaTf)
                .asRequired()
                .withValidator(new StringLengthValidator("Priezvisko vodiča musí byť v rozmedzí 1 - 35", 1, 35))
                .bind(ZmenaUdajovPreukazuEntityUi::getPriezviskoVodica, ZmenaUdajovPreukazuEntityUi::setPriezviskoVodica);

        binder.forField(zakazViestVodidloChB)
                .bind(ZmenaUdajovPreukazuEntityUi::getZakazViestVozidlo, ZmenaUdajovPreukazuEntityUi::setZakazViestVozidlo);

        binder.forField(platnostVodickehoDf)
                .asRequired()
                .bind(ZmenaUdajovPreukazuEntityUi::getDatumUkonceniaPlatnostiPreukazu, ZmenaUdajovPreukazuEntityUi::setDatumUkonceniaPlatnostiPreukazu);

        binder.forField(pocetDopravnychPriestupkovTf)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(ZmenaUdajovPreukazuEntityUi::getPocetDopravnychPriestupkov, ZmenaUdajovPreukazuEntityUi::setPocetDopravnychPriestupkov);

        this.parenComponent.showComponent(new Panel("Editovanie údajov o vodičskom preukaze", layout));
    }

    private void zmenUdajeVodicskehoPreukazu(ZmenaUdajovPreukazuEntityUi entity) {

        HandledOperationExecutor.submit(() -> {
            this.drivingLicenseService.updateVodicskyPreukaz(entity);
            CustomNotification.notification("Údaje vodičského preukazu boli zmenené");
            this.entity = new ZmenaUdajovPreukazuEntityUi();
            binder.readBean(this.entity);
            return OperationMarker.OPERATION_SUCCESFUL;
        });

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.CARDS_MENU_3.getTitle();
    }

}
