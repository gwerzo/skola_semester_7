package sk.uniza.fri.lustyno.AudsLetisko.components.common;

public interface ParentComponent {

    void hideComponent();

    void showComponent();

    String getComponentTitle();

}
