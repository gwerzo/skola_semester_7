﻿using System.Net.Http.Headers;
using System.Web.Http;
using FriBettingDatabaseApp.Repository.Base;
using FriBettingDatabaseApp.Repository.Impl;
using FriBettingWebApplication.Services;
using FriBettingWebApplication.Services.Face;
using Unity;
using Unity.WebApi;

namespace FriBettingWebApplication
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.Formatters.JsonFormatter.SupportedMediaTypes
                .Add(new MediaTypeHeaderValue("text/html"));

            var container = new UnityContainer();
            container.RegisterType<ISportRepository, SportsRepository>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<ICompetitorRepository, CompetitorRepository>();
            container.RegisterType<IMatchRepository, MatchRepository>();
            container.RegisterType<IMatchPartRepository, MatchPartRepository>();
            container.RegisterType<IOddRepository, OddsRepository>();

            container.RegisterType<ISportsService, SportsService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<ICompetitorService, CompetitorService>();
            container.RegisterType<IMatchService, MatchService>();
            container.RegisterType<ITicketService, TicketService>();


            config.DependencyResolver = new UnityDependencyResolver(container);

            // JWT Auth atribút config
            config.Filters.Add(new AuthorizeAttribute());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
