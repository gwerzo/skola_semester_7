package sk.uniza.fri.lustyno.AudsLetisko.service;

import com.vaadin.spring.server.SpringVaadinServlet;
import org.springframework.stereotype.Service;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import sk.uniza.fri.lustyno.AudsLetisko.codelists.DrahyCiselnik;
import sk.uniza.fri.lustyno.AudsLetisko.entity_mapper.EntityMapper;
import sk.uniza.fri.lustyno.AudsLetisko.persistance.HistoriaFileManager;
import sk.uniza.fri.lustyno.AudsLetisko.persistance.LietadlaFileManager;
import sk.uniza.fri.lustyno.AudsLetisko.persistance.OdletoveDrahyFileManager;

import javax.servlet.ServletContext;

@Service
public class UIServices {

    public static AircraftManagementService getAircraftManagementService(){
        return getApplicationContext().getBean(AircraftManagementService.class);
    }

    public static RunwayManagementService getRunwayManagementService() {
        return getApplicationContext().getBean(RunwayManagementService.class);
    }

    public static HistoryService getDepartureHistoryService() {
        return getApplicationContext().getBean(HistoryService.class);
    }

    public static DateTimeService getDateTimeService() {
        return getApplicationContext().getBean(DateTimeService.class);
    }

    // --------------------MAPPERS----------------------
    public static EntityMapper getEntityMapper() {
        return getApplicationContext().getBean(EntityMapper.class);
    }

    // --------------------CONVERTERS--------------------
    public static DrahyCiselnik getDrahyCiselnik() {
        return getApplicationContext().getBean(DrahyCiselnik.class);
    }

    // -----------------------IO-------------------------
    public static LietadlaFileManager getVsetkyLietadlaFileManager() {
        return getApplicationContext().getBean(LietadlaFileManager.class);
    }

    public static OdletoveDrahyFileManager getOdletoveDrahyFileManager() {
        return getApplicationContext().getBean(OdletoveDrahyFileManager.class);
    }

    public static HistoriaFileManager getHistoriaFileManager() {
        return getApplicationContext().getBean(HistoriaFileManager.class);
    }

    // -----------------------GENERATOR-------------------------------
    public static GeneratorService getGeneratorService() {
        return getApplicationContext().getBean(GeneratorService.class);
    }


    public static ApplicationContext getApplicationContext() {
        ServletContext servletContext = SpringVaadinServlet.getCurrent().getServletContext();
        return WebApplicationContextUtils.getWebApplicationContext(servletContext);
    }

}
