﻿using MemoryGameServerApp.Server;
using SuperWebSocket;

namespace MemoryGameServerApp.Command
{
    public class LogoutCommand: ICommand
    {
        public const string COMMAND = "-logout";
        private const string LOGOUT_SUCC = "-logoutsucc";

        private string _sessionUuid;
        public LogoutCommand(string sessionUuid)
        {
            this._sessionUuid = sessionUuid;
        }

        public void executeCommand(string usernameOfCommandCaller, MemoryGameServer memoryGameServer,
            WebSocketSession callerSocketSession)
        {
            if (memoryGameServer.LogoutUser(this._sessionUuid))
            {
                callerSocketSession.Send(LOGOUT_SUCC);
            }
        }
    }
}
