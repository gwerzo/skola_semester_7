package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CoordinatesUi {

    private Double latitude;
    private Double longitude;

    // true = NORTH, false = SOUTH
    private Boolean latitudeOrientation;

    // true = EAST, false = WEST
    private Boolean longitudeOrientation;

    public void flush() {
        this.latitude = null;
        this.longitude = null;
        this.latitudeOrientation = null;
        this.longitudeOrientation = null;
    }
}
