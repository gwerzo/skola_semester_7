﻿using MemoryGameServerApp.Server;
using SuperWebSocket;

namespace MemoryGameServerApp.Command
{
    /// <summary>
    /// Command reprezentúci posielanie správ medzi dvomi používateľmi
    /// </summary>
    class SendPrivateMessageCommand:ICommand
    {
        public const string COMMAND = "-pm";
        private const string PRIVATE_MESSAGE_UNSUCCESFULL = "-pmunsucc";
        public string UsernameOfReceiver { get; set; }
        public string Message { get; set; }

        public SendPrivateMessageCommand(string usernameOfReceiver, string message)
        {
            this.UsernameOfReceiver = usernameOfReceiver;
            this.Message = message;
        }

        public void executeCommand(string usernameOfCommandCaller, MemoryGameServer memoryGameServer,
            WebSocketSession callerSocketSession)
        {
            if (!memoryGameServer.SendMessageTo(this.UsernameOfReceiver, COMMAND + " " + 
                                                                         usernameOfCommandCaller + " " + this.Message))
            {
                callerSocketSession.Send(PRIVATE_MESSAGE_UNSUCCESFULL + " " + this.UsernameOfReceiver + " Používateľ nie je online");
            }
        }
    }
}
