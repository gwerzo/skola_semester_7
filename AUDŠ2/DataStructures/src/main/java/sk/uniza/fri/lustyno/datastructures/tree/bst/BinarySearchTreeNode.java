package sk.uniza.fri.lustyno.datastructures.tree.bst;

import sk.uniza.fri.lustyno.datastructures.entity.AbstractKeyValueEntity;
import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureWrongUsageException;

public class BinarySearchTreeNode<K extends HasComparableValue, D> extends AbstractKeyValueEntity<K, D> {

    private BinarySearchTreeNode<K, D> leftChildo;

    private BinarySearchTreeNode<K, D> rightChild;

    private BinarySearchTreeNode<K, D> parent;

    public BinarySearchTreeNode(K key, D data) {
        super(key, data);
    }

    public BinarySearchTreeNode<K, D> getLeftChildo() {
        return this.leftChildo;
    }

    public BinarySearchTreeNode<K, D> getRightChild() {
        return this.rightChild;
    }

    public BinarySearchTreeNode<K, D> getParent() {
        return this.parent;
    }

    protected void setLeftChildo(BinarySearchTreeNode<K, D> node) {
        this.leftChildo = node;
    }

    protected void setRightChild(BinarySearchTreeNode<K, D> node) {
        this.rightChild = node;
    }

    protected void nullizeLeftChild() {
        this.leftChildo = null;
    }

    protected void nullizeRightChild() {
        this.rightChild = null;
    }

    protected void setParent(BinarySearchTreeNode<K, D> node) {
        this.parent = node;
    }

    public boolean isRoot() {
        return this.parent == null;
    }

    public boolean isLeaf() {
        return this.leftChildo == null && this.rightChild == null;
    }

    public boolean hasOnlyOneChild() {
        return (this.leftChildo == null && this.rightChild != null) || (this.leftChildo != null && this.rightChild == null);
    }

    /**
     * Vráti práve jedno dieťa nodu, odporúčané volať až po metóde
     * {@link BinarySearchTreeNode#hasOnlyOneChild}
     * <p>
     * @return jediné dieťa nodu
     * @throws DataStructureWrongUsageException ak node nemal žiadne, alebo mal obe deti
     */
    protected BinarySearchTreeNode<K,D> getOneChildIfNodeHasOnlyOneChild() {
        if (this.leftChildo == null && this.rightChild != null) {
            return this.rightChild;
        } else if (this.leftChildo != null && this.rightChild == null) {
            return this.leftChildo;
        } else {
            throw new DataStructureWrongUsageException(
                    this.getClass().getName(),
                    "getOneChildIfNodeHasOnlyOneChild",
                    "Zavolaná operácia, ktorá má vrátiť jediné dieťa nodu, ale node nemá práve jedno dieťa");
        }
    }

    public boolean hasBothChildren() {
        return this.leftChildo != null && this.rightChild != null;
    }


    public boolean isRightChildOfParent() {
        return this.getKey().getValue().compareTo(this.parent.getKey().getValue()) > 0;
    }

    @Override
    public void setKey(K newKey) {
        throw new DataStructureWrongUsageException(
                this.getClass().getName(), "setKey", "Vrcholu v BST nie je možné meniť hodnotu kľúča");
    }

    @Override
    public String toString() {
        return this.getKey().getValue().toString();
    }

}
