﻿using FriBettingDatabaseApp.Bo;

namespace FriBettingDatabaseApp.Repository.Base
{
    public interface IOddRepository: IRepository<Odd>
    {
    }
}
