package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OverflowBlockInfoUi {

    Integer indexInFile;
    Integer numberOfValidEntities;
    Integer overflowBlock;
    // Entity joinnuté do jedného stringu, oddelené čiarkou
    String entities;

}
