package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.Data;

import java.util.List;

@Data
public class RealEstateUiTableEntity {

    private int internalId;
    private int supisneCislo;
    private String popis;
    private Double latitude;
    private Double longtitude;
    private String latitudeOrientation;
    private String longitudeOrientation;
    private List<ParcelUiTableEntity> parcely;

}
