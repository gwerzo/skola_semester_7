package sk.uniza.fri.lustyno.fripd.utils;

import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

public class CustomNotification {

    public static void notification(String text) {
        Notification n = new Notification(text, Notification.Type.HUMANIZED_MESSAGE);
        n.setPosition(Position.BOTTOM_RIGHT);
        n.setDelayMsec(3000);
        n.show(UI.getCurrent().getPage());
    }

    public static void notification(String text, Integer delay) {
        Notification n = new Notification(text, Notification.Type.HUMANIZED_MESSAGE);
        n.setPosition(Position.BOTTOM_RIGHT);
        n.setDelayMsec(delay);
        n.show(UI.getCurrent().getPage());
    }

    public static void error(String error) {
        Notification n = new Notification(error, Notification.Type.ERROR_MESSAGE);
        n.setPosition(Position.BOTTOM_RIGHT);
        n.setDelayMsec(3000);
        n.show(UI.getCurrent().getPage());
    }

    public static void warning(String warning) {
        Notification n = new Notification(warning, Notification.Type.WARNING_MESSAGE);
        n.setPosition(Position.BOTTOM_RIGHT);
        n.setDelayMsec(3000);
        n.show(UI.getCurrent().getPage());
    }

}
