package sk.uniza.fri.lustiak.viegenere;

public class ViegenereEncryptor {

    public static String encryptTextWithPassword(String text, String password) {
        String toUpperText = text.toUpperCase();
        StringBuilder sb = new StringBuilder();

        int pwdIter = 0;
        for (int i = 0; i < toUpperText.length(); i++) {
            int actualChar = toUpperText.charAt(i);
            char charPwd = password.charAt(pwdIter % password.length());
            if (actualChar < 'A' || actualChar > 'Z') {
                sb.append((char)actualChar);
                continue;
            }
            int pwdDeflatedValue = charPwd - 'A';
            int enc = (((actualChar - 'A') + (charPwd - 'A')) % 26) + 'A';
            sb.append(((char)enc));
            pwdIter++;
        }
        return sb.toString();
    }
}
