package sk.uniza.fri.lustyno.datastructures.kdtree;

public class KDTreeNodeKeyDataWrapper<K,D> {

    private K key;
    private D data;

    public KDTreeNodeKeyDataWrapper(K key, D data) {
        this.key = key;
        this.data = data;
    }

    public K getKey() {
        return key;
    }

    public D getData() {
        return data;
    }
}
