﻿using FriBettingDatabaseApp.Bo;

namespace FriBettingDatabaseApp.Repository.Base
{
    interface IOddTypeRepository: IRepository<OddType>
    {
    }
}
