package sk.uniza.fri.lustiak.auds2.kataster.components.kataster;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.ui.*;
import lombok.Data;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.service.ParcelService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;
import sk.uniza.fri.lustiak.auds2.kataster.utils.StringToIntegerConverter;


public class EditaciaParcelyForm extends BusinessComponent {

    private Binder<CoordinatesUi> coordinatesBinder;
    private CoordinatesUi entity;

    private ParcelService parcelService;
    private Grid<ParcelUiTableEntity> parcelGrid;

    private ParcelUiTableEntity parcelBeingEdited;
    private ParcelUiTableEntity parcelBeingEditedNewValues;
    private Boolean editParcelCoords = Boolean.FALSE;

    private Panel initViewPanel;
    private Panel editorPanel;

    public EditaciaParcelyForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_7.getTitle());
        this.parcelService = UIServices.getParcelService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new CoordinatesUi();
        this.coordinatesBinder = new Binder<>(CoordinatesUi.class);
        this.parcelGrid = new Grid<>();
        this.parcelGrid.setWidth("100%");

        this.parcelGrid.addComponentColumn(this::buildEditujButton).setCaption("Edituj");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getInternalId).setCaption("#");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getCisloParcely).setCaption("Číslo parcely");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getPopisParcely).setCaption("Popis");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getLatitude).setCaption("Zem.výška");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getLongtitude).setCaption("Zem.šírka");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getLatitudeOrientation).setCaption("Orientácia výšky");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getLongitudeOrientation).setCaption("Orientácia šírky");


        VerticalLayout layout = new VerticalLayout();

        FormLayout formLayout = new FormLayout();

        HorizontalLayout latLayout = new HorizontalLayout();
        TextField latitudeTF = new TextField("Zemepisná výška");
        RadioButtonGroup<String> latitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej výšky");
        latitudeOrientation.setItems("SEVER", "JUH");
        latitudeOrientation.setSelectedItem("SEVER");
        latLayout.addComponents(latitudeTF, latitudeOrientation);

        HorizontalLayout longLayout = new HorizontalLayout();
        TextField longitudeTF = new TextField("Zemepisná šírka");
        RadioButtonGroup<String> longitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej šírky");
        longitudeOrientation.setItems("VÝCHOD", "ZÁPAD");
        longitudeOrientation.setSelectedItem("VÝCHOD");
        longLayout.addComponents(longitudeTF, longitudeOrientation);

        Button findParcelsButton = new Button("Vyhľadať parcely");
        findParcelsButton.addClickListener(e -> {
            try {
                this.coordinatesBinder.writeBean(this.entity);
                if (longitudeOrientation.getSelectedItem().get().equals("VÝCHOD")) {
                    this.entity.setLongitudeOrientation(true);
                } else {
                    this.entity.setLongitudeOrientation(false);
                }
                if (latitudeOrientation.getSelectedItem().get().equals("SEVER")) {
                    this.entity.setLatitudeOrientation(true);
                } else {
                    this.entity.setLatitudeOrientation(false);
                }
                najdiParcely(this.entity);
            } catch (ValidationException valEx) {
                this.entity.flush();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        formLayout.addComponent(latLayout);
        formLayout.addComponent(longLayout);
        formLayout.addComponent(findParcelsButton);

        layout.addComponent(formLayout);
        layout.addComponent(this.parcelGrid);

        this.coordinatesBinder.forField(latitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLatitude, CoordinatesUi::setLatitude);

        this.coordinatesBinder.forField(longitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLongitude, CoordinatesUi::setLongitude);

        this.initViewPanel = new Panel("Editácia parcely", layout);

        this.initEditorPanel();

        this.parentComponent.showComponent(initViewPanel);
    }

    private void initEditorPanel() {
        this.editorPanel = new Panel("Editácia parcely");
    }

    public Button buildEditujButton(ParcelUiTableEntity parcelUiTableEntity) {
        Button editujButton = new Button("Edituj");

        editujButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {
                this.initViewPanel.setVisible(false);

                this.editorPanel = new Panel("Editácia parcely",
                        this.buildEditLayoutForParcel(parcelUiTableEntity));

                this.parentComponent.addComponent(this.editorPanel);
                return OperationMarker.OPERATION_SUCCESFUL;
            });
        });

        return editujButton;
    }

    private void najdiParcely(CoordinatesUi coordinatesUi) {
        this.parcelGrid.setItems(this.parcelService.getParcelListAttCoordinates(coordinatesUi));
    }

    public VerticalLayout buildEditLayoutForParcel(ParcelUiTableEntity parcelUiTableEntity) {
        Binder<ParcelUiTableEntity> entityNewValuesBinder = new Binder<>();
        VerticalLayout mainLayout = new VerticalLayout();

        HorizontalLayout panelsLayout = new HorizontalLayout();

        Panel oldEntityPanel = new Panel(
                "Menená parcela",
                buildParcelFields(parcelUiTableEntity, false, entityNewValuesBinder).getLayout());

        EditFieldsGroup efgNewEntity = buildParcelFields(parcelUiTableEntity, true, entityNewValuesBinder);
        Panel newEntityPanel = new Panel(
                "Nové hodnoty parcely",
                efgNewEntity.getLayout());

        panelsLayout.addComponent(oldEntityPanel);
        panelsLayout.addComponent(newEntityPanel);

        Button buttonEdit = new Button("Potvrdiť zmeny");
        buttonEdit.addStyleName("friendly");
        buttonEdit.addClickListener(e -> {
            try {
                this.parcelBeingEdited = parcelUiTableEntity;
                ParcelUiTableEntity entityChanges = new ParcelUiTableEntity();
                entityChanges.setLatitudeOrientation(
                        efgNewEntity.getLatitudeOrientation().getSelectedItem().get());
                entityChanges.setLongitudeOrientation(
                        efgNewEntity.getLongitudeOrientation().getSelectedItem().get());
                entityNewValuesBinder.writeBean(entityChanges);
                this.parcelBeingEditedNewValues = entityChanges;
                editujParcelu();
            } catch (ValidationException valEx) {
                this.entity.flush();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        Button buttonBack = new Button("Späť");
        buttonBack.addClickListener(e -> {
            this.editorPanel.setVisible(false);
            this.initViewPanel.setVisible(true);
        });
        mainLayout.addComponent(panelsLayout);
        mainLayout.addComponent(new HorizontalLayout(buttonEdit, buttonBack));

        return mainLayout;
    }

    private void editujParcelu() {
        this.parcelService.editParcel(new EditParcelUi(
                this.editParcelCoords,
                this.parcelBeingEdited,
                this.parcelBeingEditedNewValues
        ));
        this.showComponent();
    }

    public EditFieldsGroup buildParcelFields(ParcelUiTableEntity entity, Boolean editable,
                                             Binder<ParcelUiTableEntity> entityNewValuesBinder) {
        VerticalLayout layout = new VerticalLayout();

        FormLayout fl = new FormLayout();
        TextField cisloParcelyTF = new TextField("Číslo parcely");
        TextField popisTF = new TextField("Popis");

        HorizontalLayout latLayout = new HorizontalLayout();
        TextField latitudeTF = new TextField("Zemepisná výška");
        RadioButtonGroup<String> latitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej výšky");
        latitudeOrientation.setItems("SEVER", "JUH");
        latitudeOrientation.setSelectedItem("SEVER");
        latLayout.addComponents(latitudeTF, latitudeOrientation);

        HorizontalLayout longLayout = new HorizontalLayout();
        TextField longitudeTF = new TextField("Zemepisná šírka");
        RadioButtonGroup<String> longitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej šírky");
        longitudeOrientation.setItems("VÝCHOD", "ZÁPAD");
        longitudeOrientation.setSelectedItem("VÝCHOD");
        longLayout.addComponents(longitudeTF, longitudeOrientation);

        cisloParcelyTF.setEnabled(editable);
        cisloParcelyTF.setValue(entity.getCisloParcely() + "");
        popisTF.setEnabled(editable);
        popisTF.setValue(entity.getPopisParcely());


        latitudeTF.setEnabled(editable);
        latitudeTF.setValue(Double.valueOf(Math.abs(entity.getLatitude())).toString().replace('.', ','));
        longitudeTF.setEnabled(editable);
        longitudeTF.setValue(Double.valueOf(Math.abs(entity.getLongtitude())).toString().replace('.', ','));
        latitudeOrientation.setEnabled(editable);
        latitudeOrientation.setSelectedItem(entity.getLatitudeOrientation());
        longitudeOrientation.setEnabled(editable);
        longitudeOrientation.setSelectedItem(entity.getLongitudeOrientation());

        entityNewValuesBinder.forField(cisloParcelyTF)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(ParcelUiTableEntity::getCisloParcely, ParcelUiTableEntity::setCisloParcely);

        entityNewValuesBinder.forField(popisTF)
                .asRequired()
                .bind(ParcelUiTableEntity::getPopisParcely, ParcelUiTableEntity::setPopisParcely);

        entityNewValuesBinder.forField(latitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(ParcelUiTableEntity::getLatitude, ParcelUiTableEntity::setLatitude);

        entityNewValuesBinder.forField(longitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(ParcelUiTableEntity::getLongtitude, ParcelUiTableEntity::setLongtitude);

        fl.addComponent(cisloParcelyTF);
        fl.addComponent(popisTF);

        if (editable) {
            Button editCoordsButton = new Button("Editovať pozíciu parcely");
            editCoordsButton.setStyleName("danger");
            fl.addComponent(editCoordsButton);
            editCoordsButton.addClickListener(e -> {
                this.editParcelCoords = Boolean.TRUE;
                fl.addComponent(latLayout);
                fl.addComponent(longLayout);
            });

        } else {
            fl.addComponent(latLayout);
            fl.addComponent(longLayout);
        }


        layout.addComponent(fl);

        EditFieldsGroup efg = new EditFieldsGroup();
        efg.setLayout(layout);
        efg.setLatitudeOrientation(latitudeOrientation);
        efg.setLongitudeOrientation(longitudeOrientation);

        return efg;
    }

    @Data
    private class EditFieldsGroup {

        private VerticalLayout layout;
        private RadioButtonGroup<String> latitudeOrientation;
        private RadioButtonGroup<String> longitudeOrientation;

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_7.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
