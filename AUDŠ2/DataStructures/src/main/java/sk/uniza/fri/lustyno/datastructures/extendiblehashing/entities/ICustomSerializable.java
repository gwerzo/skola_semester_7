package sk.uniza.fri.lustyno.datastructures.extendiblehashing.entities;

public interface ICustomSerializable<T> {

    byte[] toByteArray();
    T fromByteArray(byte[] fromBytes);

}
