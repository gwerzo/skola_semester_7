package sk.uniza.fri.lustiak.auds2.kataster.components.kataster;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.CreateNewRealEstateUi;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.StringToIntegerConverter;


public class PridanieNehnutelnostiForm extends BusinessComponent {

    private Binder<CreateNewRealEstateUi> binder;
    private CreateNewRealEstateUi entity;

    private RealEstateService realEstateService;

    public PridanieNehnutelnostiForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_4.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new CreateNewRealEstateUi();
        this.binder = new Binder<>(CreateNewRealEstateUi.class);

        VerticalLayout layout = new VerticalLayout();
        FormLayout formLayout = new FormLayout();

        TextField supisneCisloTF = new TextField("Súpisné číslo");
        TextField popisTF = new TextField("Popis");

        HorizontalLayout latLayout = new HorizontalLayout();
        TextField latitudeTF = new TextField("Zemepisná výška");
        RadioButtonGroup<String> latitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej výšky");
        latitudeOrientation.setItems("SEVER", "JUH");
        latitudeOrientation.setSelectedItem("SEVER");
        latLayout.addComponents(latitudeTF, latitudeOrientation);

        HorizontalLayout longLayout = new HorizontalLayout();
        TextField longitudeTF = new TextField("Zemepisná šírka");
        RadioButtonGroup<String> longitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej šírky");
        longitudeOrientation.setItems("VÝCHOD", "ZÁPAD");
        longitudeOrientation.setSelectedItem("VÝCHOD");
        longLayout.addComponents(longitudeTF, longitudeOrientation);


        Button createNewRealEstateButton = new Button("Vytvoriť novú nehnuteľnosť");
        createNewRealEstateButton.addClickListener(e -> {
           try {
               this.binder.writeBean(this.entity);
               if (longitudeOrientation.getSelectedItem().get().equals("VÝCHOD")) {
                   this.entity.setLongitudeOrientation(true);
               } else {
                   this.entity.setLongitudeOrientation(false);
               }
               if (latitudeOrientation.getSelectedItem().get().equals("SEVER")) {
                   this.entity.setLatitudeOrientation(true);
               } else {
                   this.entity.setLatitudeOrientation(false);
               }
               vytvorNovuNehnutelnost(this.entity);
           } catch (ValidationException valEx) {
                this.entity.flush();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
           }
        });

        formLayout.addComponent(supisneCisloTF);
        formLayout.addComponent(popisTF);
        formLayout.addComponent(latLayout);
        formLayout.addComponent(longLayout);
        formLayout.addComponent(createNewRealEstateButton);

        layout.addComponent(formLayout);

        this.binder.forField(supisneCisloTF)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(CreateNewRealEstateUi::getSupisneCislo, CreateNewRealEstateUi::setSupisneCislo);

        this.binder.forField(popisTF)
                .asRequired()
                .bind(CreateNewRealEstateUi::getPopisNehnutelnosti, CreateNewRealEstateUi::setPopisNehnutelnosti);

        this.binder.forField(latitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CreateNewRealEstateUi::getLatitude, CreateNewRealEstateUi::setLatitude);

        this.binder.forField(longitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CreateNewRealEstateUi::getLongitude, CreateNewRealEstateUi::setLongitude);

        this.parentComponent.showComponent(new Panel("Pridanie novej nehnuteľnosti", layout));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_4.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    private void vytvorNovuNehnutelnost(CreateNewRealEstateUi entity) {

        HandledOperationExecutor.submit(() -> {
            OperationMarker m = this.realEstateService.addNewRealEstate(entity);
            if (m == OperationMarker.OPERATION_SUCCESFUL) {
                CustomNotification.notification("Nová nehnuteľnosť bola vytvorená");
                this.entity.flush();
                binder.readBean(this.entity);
            }
            return m;
        });
    }
}
