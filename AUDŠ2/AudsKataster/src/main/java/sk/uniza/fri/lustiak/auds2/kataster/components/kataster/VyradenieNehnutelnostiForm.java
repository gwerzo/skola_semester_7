package sk.uniza.fri.lustiak.auds2.kataster.components.kataster;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.CoordinatesUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.RealEstateUiTableEntity;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;


public class VyradenieNehnutelnostiForm extends BusinessComponent {

    private Binder<CoordinatesUi> binder;
    private CoordinatesUi entity;

    private RealEstateService realEstateService;

    private Grid<RealEstateUiTableEntity> realEstateGrid;

    public VyradenieNehnutelnostiForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_8.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new CoordinatesUi();
        this.binder = new Binder<>(CoordinatesUi.class);
        this.realEstateGrid = new Grid<>();
        this.realEstateGrid.setWidth("100%");

        this.realEstateGrid.addComponentColumn(this::buildVyraditButton).setCaption("Vyradiť");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getInternalId).setCaption("#");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getSupisneCislo).setCaption("Súpisné číslo");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getPopis).setCaption("Popis");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getLatitude).setCaption("Zem.výška");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getLongtitude).setCaption("Zem.šírka");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getLatitudeOrientation).setCaption("Orientácia výšky");
        this.realEstateGrid.addColumn(RealEstateUiTableEntity::getLongitudeOrientation).setCaption("Orientácia šírky");


        VerticalLayout layout = new VerticalLayout();

        FormLayout formLayout = new FormLayout();

        HorizontalLayout latLayout = new HorizontalLayout();
        TextField latitudeTF = new TextField("Zemepisná výška");
        RadioButtonGroup<String> latitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej výšky");
        latitudeOrientation.setItems("SEVER", "JUH");
        latitudeOrientation.setSelectedItem("SEVER");
        latLayout.addComponents(latitudeTF, latitudeOrientation);

        HorizontalLayout longLayout = new HorizontalLayout();
        TextField longitudeTF = new TextField("Zemepisná šírka");
        RadioButtonGroup<String> longitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej šírky");
        longitudeOrientation.setItems("VÝCHOD", "ZÁPAD");
        longitudeOrientation.setSelectedItem("VÝCHOD");
        longLayout.addComponents(longitudeTF, longitudeOrientation);

        Button findRealEstatesButton = new Button("Vyhľadať nehnuteľnosti");
        findRealEstatesButton.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                if (longitudeOrientation.getSelectedItem().get().equals("VÝCHOD")) {
                    this.entity.setLongitudeOrientation(true);
                } else {
                    this.entity.setLongitudeOrientation(false);
                }
                if (latitudeOrientation.getSelectedItem().get().equals("SEVER")) {
                    this.entity.setLatitudeOrientation(true);
                } else {
                    this.entity.setLatitudeOrientation(false);
                }
                najdiNehnutelnosti(this.entity);
            } catch (ValidationException valEx) {
                this.entity.flush();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        formLayout.addComponent(latLayout);
        formLayout.addComponent(longLayout);
        formLayout.addComponent(findRealEstatesButton);

        layout.addComponent(formLayout);
        layout.addComponent(this.realEstateGrid);

        this.binder.forField(latitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLatitude, CoordinatesUi::setLatitude);

        this.binder.forField(longitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLongitude, CoordinatesUi::setLongitude);


        this.parentComponent.showComponent(new Panel("Vyhľadanie NEHNUTEĽNOSTI podľa GPS súradníc", layout));
    }

    private void najdiNehnutelnosti(CoordinatesUi coordinatesUi) {
        this.realEstateGrid.setItems(this.realEstateService.getRealEstatesListAtCoordinates(coordinatesUi));
    }

    public Button buildVyraditButton(RealEstateUiTableEntity realEstateUiTableEntity) {
        Button vyraditButton = new Button("Vyradiť");

        vyraditButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {
                this.realEstateService.removeRealEstate(realEstateUiTableEntity);
                this.najdiNehnutelnosti(this.entity);
                return OperationMarker.OPERATION_SUCCESFUL;
            });
        });
        return vyraditButton;
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_8.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }
}
