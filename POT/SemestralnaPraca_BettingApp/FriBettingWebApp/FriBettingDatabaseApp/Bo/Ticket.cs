﻿using System;
using System.Collections.Generic;

namespace FriBettingDatabaseApp.Bo
{
    public class Ticket: IdentifiableEntity
    {

        public int TicketId { get; set; }
        public ICollection<TicketBet> TicketBets { get; set; }
        public BettingUser BettingUser { get; set; }
        public double BetValue { get; set; }
        public bool IsEvaluated { get; set; }
        public bool IsWinning { get; set; }
        public bool IsClosed { get; set; }
        public DateTime DateTimeCreated { get; set; }

        public override int GetId()
        {
            return TicketId;
        }
    }
}
