﻿namespace FriBettingDatabaseApp.Bo
{
    /// <summary>
    /// Class reprezentácia časti zápasu (polčas, tretina,..)
    /// </ummary>
    public class MatchPart: IdentifiableEntity
    {

        public int MatchPartId { get; set; }
        // Reprezentuje naviazanie časti zápasu na niektorý zápas
        public Match Match { get; set; }
        // Reprezentuje o koľký polčas/tretinu.. sa jedná
        public int MatchPartCounter { get; set; }
        // Počet gólov domáceho tímu
        public int GoalsHost { get; set; }
        // Počet gólov hosťujúceho tímu
        public int GoalsGuest { get; set; }

        public override int GetId()
        {
            return MatchPartId;
        }
    }
}
