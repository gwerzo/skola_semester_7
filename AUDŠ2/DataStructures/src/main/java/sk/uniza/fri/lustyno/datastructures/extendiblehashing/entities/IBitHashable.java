package sk.uniza.fri.lustyno.datastructures.extendiblehashing.entities;

public interface IBitHashable {

    /**
     * Vráti int hash pre entitu
     */
    int getHash();

}
