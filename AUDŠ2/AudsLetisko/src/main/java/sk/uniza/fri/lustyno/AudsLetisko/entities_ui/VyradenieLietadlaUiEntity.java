package sk.uniza.fri.lustyno.AudsLetisko.entities_ui;

import lombok.Data;

@Data
public class VyradenieLietadlaUiEntity {

    private String medzinarodnyKodLietadla;

}
