﻿using System.Data.Entity;
using FriBettingDatabaseApp.Bo;

namespace FriBettingDatabaseApp.Context
{
    public class BettingAppDbContext: DbContext
    {

        private const string CONN_STRING = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\42191\\OneDrive\\Documents\\fri_betting.mdf;Integrated Security=True;Connect Timeout=30";

        public BettingAppDbContext() : base(CONN_STRING)
        {

        }

        public DbSet<BettingUser> BettingUsers { get; set; }
        public DbSet<Competitor> Competitors { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<MatchPart> MatchParts { get; set; }
        public DbSet<Odd> Odds { get; set; }
        public DbSet<OddType> OddTypes { get; set; }
        public DbSet<Sport> Sports { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<TicketBet> TicketMatchBets { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Sport>()
                .HasMany<OddType>(s => s.OddTypes)
                .WithMany(c => c.Sports)
                .Map(cs =>
                {
                    cs.MapLeftKey("SportRefId");
                    cs.MapRightKey("OddTypeRefId");
                    cs.ToTable("SportOddTypes");
                });
        }
    }
}
