﻿namespace FriBettingWebApplication.Dto.Request
{
    public class RegisterUserRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string PasswordAgain { get; set; }

    }
}