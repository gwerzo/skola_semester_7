﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace MemoryGameWebApp.DTO
{
    public class GameInfoResponseDTO
    {
        [JsonPropertyName("GameWidth")]
        public int GameWidth { get; set; }
        [JsonPropertyName("GameHeight")]
        public int GameHeight { get; set; }
        [JsonPropertyName("GameStepsCount")]
        public int GameStepsCount { get; set; }
        [JsonPropertyName("Player1Username")]
        public string Player1Username { get; set; }
        [JsonPropertyName("Player2Username")]
        public string Player2Username { get; set; }
        [JsonPropertyName("GameSteps")]
        public List<GameInfoStepResponseDTO> GameSteps { get; set; }

        public GameInfoResponseDTO()
        {
            this.GameSteps = new List<GameInfoStepResponseDTO>();
        }

        public class GameInfoStepResponseDTO
        {
            [JsonPropertyName("XHit")]
            public int XHit { get; set; }
            [JsonPropertyName("YHit")]
            public int YHit { get; set; }
            [JsonPropertyName("StepCount")]
            public int StepCount { get; set; }
        }

    }
}
