package sk.uniza.fri.lustiak.auds2.kataster.components.system;

import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.service.ParcelService;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;


public class ExportSystemuDoSuboruForm extends BusinessComponent {


    private RealEstateService realEstateService;
    private ParcelService parcelService;

    public ExportSystemuDoSuboruForm(RepaintableComponent component) {
        super(component, MENU_TITLE.SYSTEM_MENU_2.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
        this.parcelService = UIServices.getParcelService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        VerticalLayout layout = new VerticalLayout();

        ProgressBar progressBar = new ProgressBar(0.0f);
        progressBar.setCaption("Priebeh exportu");

        Button exportButton = new Button("Spustiť export");

        exportButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {
                this.realEstateService.vycistiSubor();
                this.realEstateService.ulozVsetkyNehnutelnosti();
                progressBar.setValue(0.5f);
                CustomNotification.notification("Nehnuteľnosti boli úspešne uložené");

                this.parcelService.vycistiSubor();
                this.parcelService.ulozVsetkyParcely();
                progressBar.setValue(1.0f);
                CustomNotification.notification("Parcely boli úspešne uložené");

                return OperationMarker.OPERATION_UNSUCCESFUL;
            });
        });

        layout.addComponent(progressBar);
        layout.addComponent(exportButton);

        this.parentComponent.showComponent(new Panel("Export systému do súboru", layout));

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_2.getTitle();
    }
}

