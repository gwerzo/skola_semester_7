package sk.uniza.fri.lustyno.datastructures.linearhashing.file;

import sk.uniza.fri.lustyno.datastructures.exception.DataStructureFlushFailedException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureInitializationException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureOperationFailedException;
import sk.uniza.fri.lustyno.datastructures.linearhashing.config.FileConfig;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.Constants;

import java.io.*;
import java.nio.ByteBuffer;

public class FileHandler {

    private RandomAccessFile mainFileRaf;

    private RandomAccessFile overflowFileRaf;

    private String databaseName;

    public FileHandler(String databaseName, Integer recordSize,
                       Integer numberOfRecordsInPrimaryBlock, Integer numberOfRecordsInOverflowBlock,
                       Integer initialBlocksCount) {
        this.databaseName = databaseName;

        String mainFileName = databaseName + Constants.DB_FILE_POSTFIX;
        String overflowFileName = Constants.DB_OVERFLOW_FILE_PREFIX + databaseName + Constants.DB_FILE_POSTFIX;

        try {

            this.mainFileRaf = new RandomAccessFile(mainFileName, "rw");

            this.overflowFileRaf = new RandomAccessFile(overflowFileName, "rw");

        } catch (FileNotFoundException e) {
            throw new DataStructureInitializationException(this.getClass().getName(),
                    "Nepodarilo sa inicializovať súbory pre novú štruktúru");
        }
        initializeEmptyFilesWithEmptyBlocks(initialBlocksCount, recordSize, numberOfRecordsInPrimaryBlock);
    }

    public FileHandler(FileConfig fromConfig) {
        this.databaseName = fromConfig.databaseName;

        String mainFileName = databaseName + Constants.DB_FILE_POSTFIX;
        String overflowFileName = Constants.DB_OVERFLOW_FILE_PREFIX + databaseName + Constants.DB_FILE_POSTFIX;

        try {

            this.mainFileRaf = new RandomAccessFile(mainFileName, "rw");

            this.overflowFileRaf = new RandomAccessFile(overflowFileName, "rw");

        } catch (FileNotFoundException e) {
            throw new DataStructureInitializationException(this.getClass().getName(),
                    "Nepodarilo sa inicializovať súbory pre novú štruktúru");
        }
    }

    public FileConfig fillFileConfig(FileConfig fileConfig) {
        fileConfig.databaseName = this.databaseName;
        return fileConfig;
    }

    /**
     * Naplní súbory prázdnymi blokmi
     *
     * Hlavný naplní prázdnymi hodnotami
     *
     * @param initialBlocksCount - počet blokov na začiatku
     * @param recordSize - veľkosť jednotlivých recordov v bytoch
     */
    public void initializeEmptyFilesWithEmptyBlocks(
            Integer initialBlocksCount, Integer recordSize, Integer numberOfRecordsInPrimaryBlock) {
        byte[] mainFileEmptiness =
                new byte[initialBlocksCount * recordSize * numberOfRecordsInPrimaryBlock
                                    + initialBlocksCount * Block.BLOCK_INFO_SIZE_BYTES];
        // Každému bloku nastav offset overflowbloku na -1 by default (8th bajt v bloku)
        int blockSize = recordSize * numberOfRecordsInPrimaryBlock + Block.BLOCK_INFO_SIZE_BYTES;
        for (int i = 0; i < mainFileEmptiness.length; i++) {
            if (i % blockSize == 10) {
                mainFileEmptiness[i] = -1;
                mainFileEmptiness[i-1] = -1;
                mainFileEmptiness[i-2] = -1;
                mainFileEmptiness[i-3] = -1;
            }
        }
        // Kvôli začiatku bloku
        for (int i = 0; i < mainFileEmptiness.length; i++) {
            if (i % blockSize == 2) {
                mainFileEmptiness[i] = 99;
                mainFileEmptiness[i-1] = 99;
                mainFileEmptiness[i-2] = 99;
            }
        }

        byte[] overflowFileEmptiness = new byte[0];

        try {
            this.mainFileRaf.seek(0);
            this.mainFileRaf.write(mainFileEmptiness);

            this.overflowFileRaf.seek(0);
            this.overflowFileRaf.write(overflowFileEmptiness);

        } catch (IOException e) {
            e.printStackTrace();
            throw new DataStructureInitializationException(this.getClass().getName(),
                    "Nepodarilo sa inicializovať hlavný alebo preplňujúci súbor na defaultnú veľkosť blokov");
        }
    }

    /**
     * Zapíše pole bajtov do hlavného súboru
     * @param bytes - pole bajtov
     * @param offsetInFile - offset odkiaľ začať zapisovanie
     */
    public void writeToMainFile(byte[] bytes, Integer offsetInFile) {
        try {
            this.mainFileRaf.seek(offsetInFile);
            this.mainFileRaf.write(bytes, 0, bytes.length);
        } catch (IOException e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "writeToMainFile", e.getMessage());
        }
    }

    /**
     * Zapíše pole bajtov do preplňujúceho súboru
     * @param bytes - pole bajtov
     * @param offsetInFile - offset odkiaľ začať zapisovanie
     */
    public void writeToOverflowFile(byte[] bytes, Integer offsetInFile) {
        try {
            this.overflowFileRaf.seek(offsetInFile);
            this.overflowFileRaf.write(bytes, 0, bytes.length);
        } catch (IOException e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "writeToOverflowFile", e.getMessage());
        }
    }

    /**
     * Prečíta z hlavného súboru pole bajtov
     * @param offsetInFile - offset skade začať čítať
     * @param numberOfBytes - počet bajtov na prečítanie
     * @return
     */
    public byte[] readFromMainFile(Integer offsetInFile, Integer numberOfBytes) {
        try {
            byte[] bb = ByteBuffer.allocate(numberOfBytes).array();
            this.mainFileRaf.seek(offsetInFile);
            this.mainFileRaf.read(bb, 0, bb.length);
            return bb;
        } catch (IOException e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "readFromMainFile", e.getMessage());
        }
    }

    /**
     * Prečíta z predlňujúceho súboru pole bajtov
     * @param offsetInFile - offset skade začať čítať
     * @param numberOfBytes - počet bajtov na prečítanie
     * @return
     */
    public byte[] readFromOverflowFile(Integer offsetInFile, Integer numberOfBytes) {
        try {
            byte[] bb = ByteBuffer.allocate(numberOfBytes).array();
            this.overflowFileRaf.seek(offsetInFile);
            this.overflowFileRaf.read(bb, 0, bb.length);
            return bb;
        } catch (IOException e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "readFromOverflowFile", e.getMessage());
        }
    }

    /**
     * Zmení veľkosť súboru s overflow blokmi,
     * využiteľné pri konsolidácii (useknutí blokov z konca)
     * @param numberOfBytes
     */
    public void setSizeOfOverflowBlocksFile(Long numberOfBytes) {
        try {
            this.overflowFileRaf.setLength(numberOfBytes);
        } catch (IOException e) {
            throw new DataStructureOperationFailedException(this.getClass().getName(), "setSizeOfOverflowBlocksFile",
                    "Nepodarilo sa zmeniť veľkosť súboru s overflow blokmi pri konsolidácii");
        }
    }


    public void closeFiles() {
        try {
            this.mainFileRaf.close();
            this.overflowFileRaf.close();
        } catch (IOException e) {
            throw new DataStructureFlushFailedException(this.databaseName);
        }
    }

}
