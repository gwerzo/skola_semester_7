package sk.uniza.fri.lustyno.fripd.factory;

import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.FromBytesFactory;
import sk.uniza.fri.lustyno.fripd.entities_bo.DrivingLicenceEntity;

public class DrivingLicenseFactory extends FromBytesFactory<DrivingLicenceEntity> {

    public DrivingLicenseFactory() {}

    @Override
    public DrivingLicenceEntity fromBytes(byte[] bytes) {
        return DrivingLicenceEntity.fromByteArray(bytes);
    }

    @Override
    public DrivingLicenceEntity emptyInvalidInstance() {
        return DrivingLicenceEntity.emptyInvalidInstance();
    }

}
