﻿using System.Web.Mvc;

namespace FriBettingWebApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}