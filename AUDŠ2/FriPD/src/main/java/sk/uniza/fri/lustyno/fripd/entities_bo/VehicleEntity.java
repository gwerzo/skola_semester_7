package sk.uniza.fri.lustyno.fripd.entities_bo;

import lombok.Data;
import lombok.experimental.Accessors;
import sk.uniza.fri.lustyno.datastructures.entity.keys.DoubleStringKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.Record;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.HasSize;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.StringUtils;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;
import sk.uniza.fri.lustyno.fripd.BusinessException;
import sk.uniza.fri.lustyno.fripd.codelists.Constants;
import sk.uniza.fri.lustyno.fripd.utils.Validable;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

@HasSize(sizeInBytes = 50)
@Data
@Accessors(chain = true)
public class VehicleEntity extends Record<DoubleStringKey> implements Validable {

    /**
     * VIN Kód
     */
    private static final Integer VIN_KOD_DLZKA = Constants.VIN_CODE_LENGTH;
    private String VIN_KOD;
    /***/

    /**
     * ŠPZ auta
     */
    private static final Integer EVC_DLZKA = Constants.EVC_CODE_LENGTH;
    private String EVC;
    /***/

    /**
     * Prevádzková hmotnosť
     */
    private Integer prevadzkovaHmotnost;

    /**
     * Počet náprav auta
     */
    private Integer pocetNaprav;

    /**
     * Je auto v pátraní
     */
    private Boolean vPatrani;

    /**
     * Dátum konca platnosti STK
     */
    private LocalDateTime datumKoncaPlatnostiSTK;

    /**
     * Dátum konca platnosti EK
     */
    private LocalDateTime datumKoncaPlatnostiEK;

    public VehicleEntity() {

    }

    public VehicleEntity(String VIN_KOD, String EVC,
                         Integer prevadzkovaHmotnost, Integer pocetNaprav,
                         Boolean vPatrani,
                         LocalDateTime datumKoncaPlatnostiSTK, LocalDateTime datumKoncaPlatnostiEK) {
        this.VIN_KOD = StringUtils.padRightDollars(VIN_KOD, VIN_KOD_DLZKA);
        this.EVC = StringUtils.padRightDollars(EVC, EVC_DLZKA);
        this.prevadzkovaHmotnost = prevadzkovaHmotnost;
        this.pocetNaprav = pocetNaprav;
        this.vPatrani = vPatrani;
        this.datumKoncaPlatnostiSTK = datumKoncaPlatnostiSTK;
        this.datumKoncaPlatnostiEK = datumKoncaPlatnostiEK;
    }

    @Override
    public byte[] toBytesArray() {
        byte[] bytes = ToByteArrayConverter.concatenateByteArrays(
                Arrays.asList(
                        ToByteArrayConverter.bytesFromBoolean(this.valid),
                        ToByteArrayConverter.bytesFromString(this.VIN_KOD),
                        ToByteArrayConverter.bytesFromString(this.EVC),
                        ToByteArrayConverter.bytesFromInt(this.prevadzkovaHmotnost),
                        ToByteArrayConverter.bytesFromInt(this.pocetNaprav),
                        ToByteArrayConverter.bytesFromBoolean(this.vPatrani),
                        ToByteArrayConverter.bytesFromLocalDateTime(this.datumKoncaPlatnostiSTK),
                        ToByteArrayConverter.bytesFromLocalDateTime(this.datumKoncaPlatnostiEK)));
        return bytes;

    }

    public static VehicleEntity fromByteArray(byte[] bytes) {
        VehicleEntity che = new VehicleEntity();
        che.valid = FromByteArrayConverter.booleanFromByte(bytes[0]);
        if (!che.valid) {
            return emptyInvalidInstance();
        }
        che.VIN_KOD = FromByteArrayConverter.stringFromBytes(Arrays.copyOfRange(bytes, 1, 18));
        che.EVC = FromByteArrayConverter.stringFromBytes(Arrays.copyOfRange(bytes, 18, 25));
        che.prevadzkovaHmotnost = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(bytes, 25, 29));
        che.pocetNaprav = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(bytes, 29, 33));
        che.vPatrani = FromByteArrayConverter.booleanFromByte(bytes[33]);
        che.datumKoncaPlatnostiSTK = FromByteArrayConverter.localDateTimeFromBytes(Arrays.copyOfRange(bytes, 34, 42));
        che.datumKoncaPlatnostiEK = FromByteArrayConverter.localDateTimeFromBytes(Arrays.copyOfRange(bytes, 42, 50));
        return che;
    }

    public static VehicleEntity emptyInvalidInstance() {
        return new VehicleEntity(
                "", "", 1000, 0, false, LocalDateTime.now(), LocalDateTime.now());
    }

    @Override
    public int getHash() {
        int hash = 7;
        int cc = VehicleEntity.EVC_DLZKA;
        for (int i = 0; i < this.VIN_KOD.length(); i++) {
            hash = hash*31 + this.VIN_KOD.charAt(i) + this.EVC.charAt(i % cc);
        }

        return Math.abs(hash);
    }

    @Override
    public DoubleStringKey getKey() {
        return new DoubleStringKey(this.VIN_KOD, this.EVC);
    }


    public static String randomVIN() {
        String VIN_VALS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 17; i++) {
            sb.append(VIN_VALS.charAt(rnd.nextInt(VIN_VALS.length())));
        }
        return sb.toString();
    }

    public static String randomSPZ() {
        String SPZ_VALS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 7; i++) {
            sb.append(SPZ_VALS.charAt(rnd.nextInt(SPZ_VALS.length())));
        }
        return sb.toString();
    }

    @Override
    public void validate() {
        if (this.VIN_KOD == null || this.VIN_KOD.isEmpty()) {
            throw new BusinessException("VIN kód je povinný parameter");
        }
        if (this.EVC == null || this.EVC.isEmpty()) {
            throw new BusinessException("EVČ vozidla je povinný parameter");
        }
    }
}
