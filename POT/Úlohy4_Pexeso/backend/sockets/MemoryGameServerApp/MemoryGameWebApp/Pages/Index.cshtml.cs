﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using MemoryGameDaoApp.Bo;
using MemoryGameDaoApp.Context;

namespace MemoryGameWebApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        
        [BindProperty]
        public List<GameStyle> GameStyleSelectList { get; set; } 
        [BindProperty]
        public List<GameTypeModel> GameTypeSelectList { get; set; } 

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            using (var dbContext = new MemoryGameDbContext())
            {

                var allGameTypes =
                    dbContext.GameTypes.ToList()
                        .Select(n => new GameTypeModel(n.GameTypeId, n.XLength + "x" + n.YLength)).ToList();

                var allGameStyles = 
                    dbContext.GameStyles
                        .Select(n => n).ToList();

                this.GameTypeSelectList = allGameTypes;
                this.GameStyleSelectList = allGameStyles;

            }
        }

        public class GameTypeModel
        {

            public int GameTypeId { get; set; }
            public string Dimensions { get; set; }

            public GameTypeModel(int gameTypeId, string dimensions)
            {
                this.GameTypeId = gameTypeId;
                this.Dimensions = dimensions;
            }
        }
    }
}
