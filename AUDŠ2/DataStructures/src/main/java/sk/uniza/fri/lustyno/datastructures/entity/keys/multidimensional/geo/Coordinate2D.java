package sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo;

import sk.uniza.fri.lustyno.datastructures.entity.keys.UniquelyIdentifiable;

public class Coordinate2D implements UniquelyIdentifiable<Coordinate2D> {

    private int uniqueIdentifier;
    private double latitudeValue;
    private double longitudeValue;
    private LATITUDE_POSITION latitudePosition;
    private LONGITUDE_POSITION longtitudePosition;

    Coordinate2D(double latitudeValue, double longitudeValue, LATITUDE_POSITION latitude_position, LONGITUDE_POSITION LONGITUDE_position,
                 int uniqueIdentifier) {
        this.latitudeValue = latitudeValue;
        this.longitudeValue = longitudeValue;
        this.latitudePosition = latitude_position;
        this.longtitudePosition = LONGITUDE_position;
        this.uniqueIdentifier = uniqueIdentifier;
    }

    public double getLatitudeValue() {
        return latitudeValue;
    }

    public double getLongitudeValue() {
        return longitudeValue;
    }

    public LATITUDE_POSITION getLatitudePosition() {
        return latitudePosition;
    }

    public LONGITUDE_POSITION getLongtitudePosition() {
        return longtitudePosition;
    }

    @Override
    public boolean equalsUniquely(Coordinate2D other) {
        return this.uniqueIdentifier == other.uniqueIdentifier;
    }

    public int getUniqueIdentifier() {
        return this.uniqueIdentifier;
    }

    @Override
    public void setUniqueIdIfEmpty(int uniqueId) {
        if (this.uniqueIdentifier == -1) {
            this.uniqueIdentifier = uniqueId;
        }
    }
}
