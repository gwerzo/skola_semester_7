package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.Data;

@Data
public class CreateNewRealEstateUi {

    private Double latitude;
    private Double longitude;

    // true = NORTH, false = SOUTH
    private Boolean latitudeOrientation;

    // true = EAST, false = WEST
    private Boolean longitudeOrientation;

    private Integer supisneCislo;
    private String popisNehnutelnosti;

    public void flush() {
        this.latitude = 0.0;
        this.longitude = 0.0;
        this.latitudeOrientation = null;
        this.longitudeOrientation = null;
        this.supisneCislo = null;
        this.popisNehnutelnosti = null;
    }

}
