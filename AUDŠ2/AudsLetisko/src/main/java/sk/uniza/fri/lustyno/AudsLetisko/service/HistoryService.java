package sk.uniza.fri.lustyno.AudsLetisko.service;

import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.HistoriaLietadlaNaLetiskuEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.HistoriaOdletuEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.LietadloEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_bo.OdletovaDrahaEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.HistoriaOdletuUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entity_mapper.EntityMapper;
import sk.uniza.fri.lustyno.AudsLetisko.persistance.utils.OdletImportHelpEntity;
import sk.uniza.fri.lustyno.datastructures.entity.keys.DateKey;
import sk.uniza.fri.lustyno.datastructures.entity.keys.StringKey;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureInsertDuplicateException;
import sk.uniza.fri.lustyno.datastructures.tree.NodeResult;
import sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTreeNode;
import sk.uniza.fri.lustyno.datastructures.tree.splay.SplayTree;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HistoryService {

    @Autowired
    private EntityMapper entityMapper;

    @Autowired
    private RunwayManagementService runwayManagementService;

    @Autowired
    private AircraftManagementService aircraftManagementService;

    // Odlety z dráh (id dráhy na samotné odlety usporiadané podľa toho ako sa vykonali)
    private List<Pair<Integer, SplayTree<DateKey, HistoriaOdletuEntity>>> historickeOdlety;

    // Prilety lietadiel/požiadania o pridelenie dráhy(kód lietadla na časy príletov/časy požiadaní)
    private SplayTree<StringKey, List<HistoriaLietadlaNaLetiskuEntity>> historiaLietadlaNaLetisku;

    public HistoryService() {
        this.historickeOdlety = new LinkedList<>();
        this.historiaLietadlaNaLetisku = new SplayTree<>();
    }

    /**
     * Pridá nový záznam o odlete pre danú dráhu
     * @param entity
     */
    public void pridajHistorickyZaznam(HistoriaOdletuEntity entity) {
        SplayTree<DateKey, HistoriaOdletuEntity> data =
                dajAleboVytvorHistorickeZaznamyPreDrahu(entity.getOdletovaDraha().getId());

        try {
            data.insert(new DateKey(entity.getCasDatumOdletu()), entity);
        } catch (DataStructureInsertDuplicateException e) {
            // Toto je len workaround pre zaznamenanie historických dát, aj keď by
            // na jednej dráhe mali byť zaznamenané dva odlety s rovnakým časom odletu
            // keďže systémový čas je v aplikácii dosť "statický"
            boolean work = true;
            while (work) {
                try {
                    entity.setCasDatumOdletu(entity.getCasDatumOdletu().plusNanos(1));
                    data.insert(new DateKey(entity.getCasDatumOdletu()), entity);
                    work = false;
                } catch (DataStructureInsertDuplicateException ex) {

                }
            }
        }
    }

    public List<Pair<Integer, List<HistoriaOdletuUiEntity>>> getHistoriaOdletovPreVsetkyDrahy() {
        List<Pair<Integer, List<HistoriaOdletuUiEntity>>> toRet = new LinkedList<>();

        for (Pair<Integer, SplayTree<DateKey, HistoriaOdletuEntity>> pair : this.historickeOdlety) {
            List<BinarySearchTreeNode<DateKey, HistoriaOdletuEntity>> odletyNodes = pair.getValue().inOrder();
            if (odletyNodes != null && odletyNodes.size() > 0) {
                toRet.add(new Pair<>(
                        pair.getKey(),
                        odletyNodes.stream()
                            .map(e -> {
                                return this.entityMapper.fromHistoriaOdletuEntity(e.getData());
                            })
                        .collect(Collectors.toList()))
                );
            }
        }
        return toRet;
    }

    /**
     * Vráti historické dáta pre dráhu
     * @param idDrahy
     * @return
     */
    private SplayTree<DateKey, HistoriaOdletuEntity> getHistorickeDataPreDrahu(Integer idDrahy) {
        for (Pair<Integer, SplayTree<DateKey, HistoriaOdletuEntity>> pair : historickeOdlety) {
            if (pair.getKey().equals(idDrahy)) {
                return pair.getValue();
            }
        }
        return null;
    }

    /**
     * Vytvorí prázdny zoznam historických dát pre dráhu,
     * neurobí nič ak taký zoznam existuje
     *
     * @param idDrahy
     */
    private SplayTree<DateKey, HistoriaOdletuEntity> dajAleboVytvorHistorickeZaznamyPreDrahu(Integer idDrahy) {
        Pair<Integer, SplayTree<DateKey, HistoriaOdletuEntity>> found = null;
        for (Pair<Integer, SplayTree<DateKey, HistoriaOdletuEntity>> pair : historickeOdlety) {
            if (pair.getKey().equals(idDrahy)) {
                found = pair;
            }
        }
        if (found == null) {
            SplayTree<DateKey, HistoriaOdletuEntity> data = new SplayTree<>();
            this.historickeOdlety.add(new Pair<>(idDrahy, data));
            return data;
        } else {
            return found.getValue();
        }
    }

    /**
     * Vráti zoznam príletov/požiadaní o dráhu pre lietadlo
     * a ak takýto neexistuje, vytvorí nový a vráti ten
     *
     * @param kodLietadla
     * @return
     */
    private List<HistoriaLietadlaNaLetiskuEntity> dajAleboVytvorHistorickeZaznamyNaLetiskuPreLietadlo(String kodLietadla) {
        NodeResult<BinarySearchTreeNode<StringKey, List<HistoriaLietadlaNaLetiskuEntity>>> zaznamyLietadla =
                this.historiaLietadlaNaLetisku.find(new StringKey(kodLietadla));

        if (zaznamyLietadla.wasFound()) {
            return zaznamyLietadla.get().getData();
        } else {
            List<HistoriaLietadlaNaLetiskuEntity> noveZaznamy = new LinkedList<>();
            this.historiaLietadlaNaLetisku.insert(new StringKey(kodLietadla), noveZaznamy);
            return noveZaznamy;
        }
    }

    /**
     * Pridá záznam o požiadaní lietadla o pridelenie dráhy
     * a o prílete lietadla
     *
     * @param lietadloEntity
     */
    public void vytvorZaznamOPoziadaniLietadla(LietadloEntity lietadloEntity) {

        List<HistoriaLietadlaNaLetiskuEntity> zaznamyLietadla =
                this.dajAleboVytvorHistorickeZaznamyNaLetiskuPreLietadlo(lietadloEntity.getMedzinarodnyKod());

        HistoriaLietadlaNaLetiskuEntity novyZaznam = new HistoriaLietadlaNaLetiskuEntity.Builder()
                .preLietadlo(lietadloEntity)
                .sDatumomPriletu(lietadloEntity.getCasDatumPriletu())
                .sDatumomPoziadania(lietadloEntity.getCasDatumPoziadaniaOPridelenie())
                .build();
        zaznamyLietadla.add(novyZaznam);
    }

    /**
     * Vráti mergnuté všetky dáta o príletoch/požiadaniach do jedného listu
     */
    public List<HistoriaLietadlaNaLetiskuEntity> dajDataPreZapisDoSuboruOPriletochAPoziadaniach() {

        List<BinarySearchTreeNode<StringKey, List<HistoriaLietadlaNaLetiskuEntity>>> nodes =
                this.historiaLietadlaNaLetisku.levelOrder();

        if (nodes == null || nodes.size() == 0) {
            return null;
        }
        List<HistoriaLietadlaNaLetiskuEntity> toRet = new LinkedList<>();
        for (BinarySearchTreeNode<StringKey, List<HistoriaLietadlaNaLetiskuEntity>> node : nodes) {
            for (HistoriaLietadlaNaLetiskuEntity entity : node.getData()) {
                toRet.add(entity);
            }
        }

        return toRet;
    }

    /**
     * Vráti dáta pre zápis do súboru o odletoch z drah
     */
    public List<Pair<Integer, SplayTree<DateKey, HistoriaOdletuEntity>>> getHistorickeOdletyZDrah() {
        return this.historickeOdlety;
    }

    /**
     * Nastaví odlety zo súboru
     * @param odletyZoSuboru
     */
    public void nastavDataOOdletochZoSuboru(List<OdletImportHelpEntity> odletyZoSuboru) {
        this.historickeOdlety = new LinkedList<>();

        for (OdletImportHelpEntity odlet : odletyZoSuboru) {

            OdletovaDrahaEntity odletovaDrahaEntity = runwayManagementService.getOdletovaDraha(odlet.getIdDrahy());
            LietadloEntity lietadloEntity = aircraftManagementService.getLietadloZoSystemuByMedzinarodnyKod(odlet.getKodLietadla());

            SplayTree<DateKey, HistoriaOdletuEntity> zaznamy = dajAleboVytvorHistorickeZaznamyPreDrahu(odletovaDrahaEntity.getId());
            HistoriaOdletuEntity historiaOdletuEntity = new HistoriaOdletuEntity();
            historiaOdletuEntity.setOdletovaDraha(odletovaDrahaEntity);
            historiaOdletuEntity.setCasDatumOdletu(odlet.getDatumOdletu());
            historiaOdletuEntity.setLietadloEntity(lietadloEntity);
            historiaOdletuEntity.setCasDatumPriletu(odlet.getDatumPriletu());

            zaznamy.insert(new DateKey(odlet.getKeyDoZoznamu()), historiaOdletuEntity);
        }

    }

}
