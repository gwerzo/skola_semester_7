package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BlockInfoUi {

    Integer directoryIndex;
    Integer indexInFile;
    Integer localLevel;
    Integer numberOfValidEntities;
    Integer overflowBlock;
    // Entity joinnuté do jedného stringu, oddelené čiarkou
    String entities;

}
