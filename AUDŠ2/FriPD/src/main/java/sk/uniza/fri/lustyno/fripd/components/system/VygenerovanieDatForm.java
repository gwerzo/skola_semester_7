package sk.uniza.fri.lustyno.fripd.components.system;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Panel;
import sk.uniza.fri.lustyno.fripd.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.fripd.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.fripd.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.fripd.service.DrivingLicenseService;
import sk.uniza.fri.lustyno.fripd.service.UIServices;
import sk.uniza.fri.lustyno.fripd.service.VehicleService;
import sk.uniza.fri.lustyno.fripd.utils.CustomNotification;
import sk.uniza.fri.lustyno.fripd.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.fripd.utils.OperationMarker;

public class VygenerovanieDatForm extends BusinessComponent {

    private VehicleService vehicleService;

    private DrivingLicenseService drivingLicenseService;

    public VygenerovanieDatForm(RepaintableComponent parentComponent) {
        super(parentComponent, MENU_TITLE.SYSTEM_MENU_2.getTitle());
        this.vehicleService = UIServices.getVehicleService();
        this.drivingLicenseService = UIServices.getDrivingLicenseService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        FormLayout layout = new FormLayout();

        Button vygenerovat = new Button("Vygenerovať dáta");
        vygenerovat.addClickListener(e -> {
            vygenerovat();
        });
        layout.addComponents(vygenerovat);


        this.parenComponent.showComponent(new Panel("Vygenerovanie dát pre systém", layout));
    }

    private void vygenerovat() {
        HandledOperationExecutor.submit(() -> {
            this.vehicleService.generateVehicles();
            this.drivingLicenseService.generateLicenses();
            CustomNotification.notification("Dáta boli úspešne vygenerované");
            return OperationMarker.OPERATION_SUCCESFUL;
        });

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_2.getTitle();
    }


}
