package sk.uniza.fri.lustyno.AudsLetisko.persistance.utils;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class OdletImportHelpEntity {

    private Integer idDrahy;

    private String kodLietadla;

    private LocalDateTime keyDoZoznamu;

    private LocalDateTime datumOdletu;

    private LocalDateTime datumPriletu;

}
