package sk.uniza.fri.lustiak.auds2.kataster.components.system;

import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.service.ParcelService;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;

public class ImportSystemuZoSuboruForm extends BusinessComponent {

    private ParcelService parcelService;
    private RealEstateService realEstateService;

    public ImportSystemuZoSuboruForm(RepaintableComponent component) {
        super(component, MENU_TITLE.SYSTEM_MENU_3.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
        this.parcelService = UIServices.getParcelService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        VerticalLayout layout = new VerticalLayout();

        ProgressBar progressBar = new ProgressBar(0.0f);
        progressBar.setCaption("Priebeh exportu");

        Button importButton = new Button("Spustiť import");

        importButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {
                this.realEstateService.nacitajNehnutelnostiZoSuboru();
                progressBar.setValue(0.5f);
                CustomNotification.notification("Nehnuteľnosti boli načítané");

                this.parcelService.nacitajParcelyZoSuboru();
                progressBar.setValue(1.0f);
                CustomNotification.notification("Parcely boli načítané");

                return OperationMarker.OPERATION_SUCCESFUL;
            });
        });

        layout.addComponent(progressBar);
        layout.addComponent(importButton);

        this.parentComponent.showComponent(new Panel("Import systému zo súboru", layout));

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_3.getTitle();
    }
}
