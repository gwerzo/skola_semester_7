package sk.uniza.fri.lustyno.datastructures.entity;

import sk.uniza.fri.lustyno.datastructures.entity.keys.UniquelyIdentifiable;

public interface HasComparableValueOnDimension<V extends UniquelyIdentifiable<V>> {

    /**
     * Porovnanie kľúča na ľubovoľnej dimenzii
     * @param other
     * @param dimension
     * @return
     */
    int compareToOnDimension(HasComparableValueOnDimension<V> other, int dimension);

    /**
     * Porovnanie kľúča na všetkých dimenziách
     * @param other
     * @return
     */
    boolean equalsOnAllDimensions(HasComparableValueOnDimension<V> other);

    /**
     * Porovnanie kľúča logickým spôsobom
     * Napr. pri 2D bodoch R1,R2 je potrebné implementovať tak, že ak
     * R1.x < R2.x && R1.y > R2.y, z porovnania
     * {@link this#compareLogically(HasComparableValueOnDimension)}
     * vráti -1
     *
     * @param other
     * @return
     */
    //int compareLogically(HasComparableValueOnDimension<V> other);

    /**
     *
     * @return
     */
    V getValue();


}
