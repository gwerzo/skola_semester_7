﻿using System.Collections.Generic;
using System.Linq;
using MemoryGameDaoApp.Bo;
using MemoryGameDaoApp.Context;

namespace MemoryGameServerApp.Dal
{
    public class GameStyleDao
    {

        public GameStyleDao()
        {
        }

        public bool CreateNewGameStyle(string gameStyleDescription)
        {
            using (var dbContext = new MemoryGameDbContext())
            {
                GameStyle newGameStyle = new GameStyle();
                newGameStyle.Description = gameStyleDescription;

                dbContext.GameStyles.Add(newGameStyle);
                dbContext.SaveChanges();
                return true;
            }
        }

        public GameStyle GetGameStyleById(int id)
        {
            using (var dbContext = new MemoryGameDbContext())
            {
                return dbContext.GetGameStyleById(id);
            }

        }

        public List<int> GetCardsIdsForGameStyle(int gameStyleId)
        {
            using (var dbContext = new MemoryGameDbContext())
            {
                return dbContext.GameCards
                    .Where(e => e.GameStyle.GameStyleId == gameStyleId)
                    .Select(e => e.CardCode).ToList();
            }
        }

    }
}
