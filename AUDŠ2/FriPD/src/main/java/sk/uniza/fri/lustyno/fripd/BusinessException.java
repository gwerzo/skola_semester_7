package sk.uniza.fri.lustyno.fripd;

public class BusinessException extends RuntimeException {

    public BusinessException(String message) {
        super(message);
    }

}
