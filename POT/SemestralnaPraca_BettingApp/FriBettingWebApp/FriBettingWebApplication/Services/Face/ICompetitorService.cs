﻿using System.Collections.Generic;
using FriBettingWebApplication.Dto.Response;

namespace FriBettingWebApplication.Services.Face
{
    public interface ICompetitorService
    {
        List<CompetitorResponse> GetCompetitorsCompetingAtSport(int sportId);
    }
}
