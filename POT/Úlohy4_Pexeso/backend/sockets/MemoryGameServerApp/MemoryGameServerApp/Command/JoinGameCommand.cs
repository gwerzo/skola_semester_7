﻿using MemoryGameServerApp.Server;
using SuperWebSocket;

namespace MemoryGameServerApp.Command
{
    public class JoinGameCommand: ICommand
    {

        public const string COMMAND = "-joingame";
        private const string JOIN_SUCC = "-joinsucc";
        private const string JOIN_UNSUCC = "-joinsunucc";
        private string _gameUuid;

        public JoinGameCommand(string gameUuid)
        {
            this._gameUuid = gameUuid;
        }

        public void executeCommand(string usernameOfCommandCaller, MemoryGameServer memoryGameServer,
            WebSocketSession callerSocketSession)
        {
            string error;
            int xLength;
            int yLength;
            if (memoryGameServer.JoinToGame(this._gameUuid, usernameOfCommandCaller, out xLength, out yLength, out error))
            {
                callerSocketSession.Send(JOIN_SUCC + " " + this._gameUuid + " " + xLength + " " + yLength);
            }
            else
            {
                callerSocketSession.Send(JOIN_UNSUCC + " " + error);
            }
        }
    }
}
