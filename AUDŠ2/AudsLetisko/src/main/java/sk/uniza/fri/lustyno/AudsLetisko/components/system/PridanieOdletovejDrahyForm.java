package sk.uniza.fri.lustyno.AudsLetisko.components.system;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.AudsLetisko.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.NovaOdletovaDrahaUiEntity;
import sk.uniza.fri.lustyno.AudsLetisko.entity_mapper.EntityMapper;
import sk.uniza.fri.lustyno.AudsLetisko.service.RunwayManagementService;
import sk.uniza.fri.lustyno.AudsLetisko.service.UIServices;
import sk.uniza.fri.lustyno.AudsLetisko.utils.CustomNotification;
import sk.uniza.fri.lustyno.AudsLetisko.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.AudsLetisko.utils.OperationMarker;
import sk.uniza.fri.lustyno.AudsLetisko.utils.converters.StringToIntegerConverter;

public class PridanieOdletovejDrahyForm extends BusinessComponent {

    private Binder<NovaOdletovaDrahaUiEntity> binder;

    private NovaOdletovaDrahaUiEntity entity;

    private RunwayManagementService runwayManagementService;

    private EntityMapper entityMapper;

    public PridanieOdletovejDrahyForm(RepaintableComponent component) {
        super(component, MENU_TITLE.SYSTEM_MENU_1.getTitle());
        this.binder = new Binder<>(NovaOdletovaDrahaUiEntity.class);
        this.entity = new NovaOdletovaDrahaUiEntity();
        this.runwayManagementService = UIServices.getRunwayManagementService();
        this.entityMapper = UIServices.getEntityMapper();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new NovaOdletovaDrahaUiEntity();
        this.binder = new Binder<>(NovaOdletovaDrahaUiEntity.class);

        VerticalLayout layout = new VerticalLayout();

        FormLayout fl = new FormLayout();

        TextField idDrahy = new TextField("ID dráhy");
        TextField dlzkaDrahy = new TextField("Dĺžka dráhy");
        Button vytvorit = new Button("Pridať dráhu");

        fl.addComponent(idDrahy);
        fl.addComponent(dlzkaDrahy);
        fl.addComponent(vytvorit);

        layout.addComponent(fl);

        binder.forField(dlzkaDrahy)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(NovaOdletovaDrahaUiEntity::getDlzkaDrahy, NovaOdletovaDrahaUiEntity::setDlzkaDrahy);

        binder.forField(idDrahy)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(NovaOdletovaDrahaUiEntity::getId, NovaOdletovaDrahaUiEntity::setId);

        vytvorit.addClickListener(e -> {
           try {
               this.binder.writeBean(this.entity);
               pridajNovuDrahu();
           } catch (ValidationException ex) {
               CustomNotification.error("Musíte vyplniť oba údaje o dráhe, ktoré sú oba celočíselné");
           }
        });

        this.parenComponent.showComponent(new Panel("Pridanie novej odletovej dráhy", layout));
    }

    private void pridajNovuDrahu() {
        HandledOperationExecutor.submit(() -> {
            OperationMarker m = this.runwayManagementService.pridajNovuDrahu(entityMapper.fromNovaOdletovaDraha(this.entity));
            if (m == OperationMarker.OPERATION_SUCCESFUL) {
                CustomNotification.notification("Nová odletová dráha bola pridaná");
                this.entity.flush();
                binder.readBean(this.entity);
            } else if (m == OperationMarker.OPERATION_NEUTRAL) {
                CustomNotification.notification(
                        "Nová odletová dráha bola pridaná a rovno aj priradená lietadlu s medzinárodným kódom "
                                + m.getMessage() +  " ktoré čakalo na danú dĺžku dráhy", 10000);
                this.entity.flush();
                binder.readBean(this.entity);
            }
            return m;
        });
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_1.getTitle();
    }
}
