package sk.uniza.fri.lustyno.datastructures.entity.keys;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class DateKey implements HasComparableValue<LocalDateTime> {

    private LocalDateTime value;

    public DateKey(LocalDateTime dateKey) {
        this.value = dateKey;
    }

    @Override
    public LocalDateTime getValue() {
        return this.value;
    }

    @Override
    public DateKey getMinValue() {
        return new DateKey(LocalDateTime.MIN);
    }
}
