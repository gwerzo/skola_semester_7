package sk.uniza.fri.lustyno.datastructures.linearhashing.file;

public class OverflowBlockUsedWrapper {

    private OverflowBlock overflowBlock;

    private Boolean used;

    public OverflowBlockUsedWrapper(OverflowBlock overflowBlock, Boolean used) {
        this.overflowBlock = overflowBlock;
        this.used = used;
    }

    public OverflowBlock getOverflowBlock() {
        return this.overflowBlock;
    }

    public Boolean wasUsed() {
        return this.used;
    }

}
