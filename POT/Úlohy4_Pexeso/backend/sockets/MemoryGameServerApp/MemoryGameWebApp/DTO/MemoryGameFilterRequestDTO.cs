﻿namespace MemoryGameWebApp.DTO
{
    public class MemoryGameFilterRequestDTO
    {
        public int GameId { get; set; }
        public int PlayerId { get; set; }

        public string SortingAttr { get; set; }

        public bool SortingOrder { get; set; }

        public MemoryGameFilterRequestDTO()
        {
            this.GameId = -1;
            this.PlayerId = - 1;
            this.SortingAttr = null;
            this.SortingOrder = false;
        }


    }
}
