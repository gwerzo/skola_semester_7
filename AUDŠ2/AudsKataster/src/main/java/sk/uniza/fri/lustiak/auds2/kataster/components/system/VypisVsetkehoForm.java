package sk.uniza.fri.lustiak.auds2.kataster.components.system;

import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.service.ParcelService;
import sk.uniza.fri.lustiak.auds2.kataster.service.RealEstateService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;

public class VypisVsetkehoForm extends BusinessComponent {

    private ParcelService parcelService;
    private RealEstateService realEstateService;

    public VypisVsetkehoForm(RepaintableComponent component) {
        super(component, MENU_TITLE.SYSTEM_MENU_4.getTitle());
        this.realEstateService = UIServices.getRealEstateService();
        this.parcelService = UIServices.getParcelService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {

        VerticalLayout layout = new VerticalLayout();

        Button vypisParcelyButton = new Button("Vypíš parcely");
        vypisParcelyButton.addClickListener(e -> {
            this.parcelService.vypisParcelyNaKonzolu();
        });

        Button vypisNehnutelnostiButton = new Button("Vypíš nehnuteľnosti");
        vypisNehnutelnostiButton.addClickListener(e -> {
            this.realEstateService.vypisNehnutelnostiNaKonzolu();
        });

        layout.addComponent(vypisNehnutelnostiButton);
        layout.addComponent(vypisParcelyButton);

        this.parentComponent.showComponent(new Panel("Vypísanie na konzolu", layout));

    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.SYSTEM_MENU_4.getTitle();
    }

}
