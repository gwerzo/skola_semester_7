﻿using System;

namespace Cvicenie_Uloha1
{

    class ArgumentsHolder
    {

        private string filePath = "";
        public String FilePath {
            get { return filePath; }
            set { filePath = value; }
        }

        private Int32 recursionValue = 0;
        public Int32 RecursionValue {
            get { return recursionValue; }
            set { recursionValue = value; }
        }

        private Int32 paddingValue = 0;
        public Int32 PaddingValue {
            get { return paddingValue; }
            set { paddingValue = value; }
        }

        private Boolean colorization = false;
        public Boolean Colorization {
            get { return colorization; }
            set { colorization = value; }
        }

        public ArgumentsHolder() {

        }
    }
}
