package sk.uniza.fri.lustiak.auds2.kataster.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.CreateNewParcelUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.CreateNewRealEstateUi;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;

import javax.annotation.PostConstruct;
import java.util.Random;

@Service
public class ApplicationInitializer {


    @Autowired
    private RealEstateService realEstateService;

    @Autowired
    private ParcelService parcelService;

    /**
     * Vytvorí dáta nehnuteľností v počte
     * @param numberOfParcels
     *
     * a parciel v počte
     * @param numberOfRealEstates
     *
     * Nakoniec pridá po 5 inštancií aj nehnuteľností aj parciel
     * všetky s rovnakými súradnicami ([0,0])
     */
    public OperationMarker initData(int numberOfParcels, int numberOfRealEstates) {

        this.parcelService.clearStoredItems();
        this.realEstateService.clearStoredItems();

        for (int parcelI = 0; parcelI < numberOfParcels; parcelI++) {

            CreateNewParcelUi createNewParcelUi = new CreateNewParcelUi();
            createNewParcelUi.setCisloParcely(parcelI);
            createNewParcelUi.setPopisParcely("Parcela " + parcelI + " - 0");

            Random rnd = new Random();
            Double dLon = rnd.nextDouble() * 180;
            boolean isNorth = true;
            boolean isEast = true;
            if (rnd.nextBoolean()) {
                dLon = dLon * -1.0;
                isEast = false;
            }

            Double dLat = rnd.nextDouble() * 180;
            if (rnd.nextBoolean()) {
                dLat = dLat * -1.0;
                isNorth = false;
            }

            createNewParcelUi.setLatitude(dLat);
            createNewParcelUi.setLongitude(dLon);
            createNewParcelUi.setLatitudeOrientation(isNorth);
            createNewParcelUi.setLongitudeOrientation(isEast);
            this.parcelService.addNewParcel(createNewParcelUi);

            // Pridá ešte 5 parciel, kde všetky ležia na súradniciach [0,0]
            if (parcelI == numberOfParcels -1) {
                for (int i = 0; i < 5; i++) {
                    createNewParcelUi = new CreateNewParcelUi();
                    createNewParcelUi.setCisloParcely(parcelI);
                    createNewParcelUi.setPopisParcely("Parcela " + parcelI + " - 0 - " + i);
                    createNewParcelUi.setLatitude(0d);
                    createNewParcelUi.setLongitude(0d);
                    createNewParcelUi.setLatitudeOrientation(true);
                    createNewParcelUi.setLongitudeOrientation(true);
                    this.parcelService.addNewParcel(createNewParcelUi);
                }
            }

        }

        for (int realEstateI = 0; realEstateI < numberOfRealEstates; realEstateI++) {
            CreateNewRealEstateUi createNewRealEstateUi = new CreateNewRealEstateUi();
            createNewRealEstateUi.setSupisneCislo(realEstateI);
            createNewRealEstateUi.setPopisNehnutelnosti("Nehnuteľnosť " + realEstateI);

            Random rnd = new Random();
            Double dLat = rnd.nextDouble() * 180;
            Double dLon = rnd.nextDouble() * 180;
            boolean isNorth = true;
            boolean isEast = true;
            if (rnd.nextBoolean()) {
                dLat = dLat * -1.0;
                isNorth = false;
            }
            if (rnd.nextBoolean()) {
                isEast = false;
                dLon = dLon * -1.0;
            }

            createNewRealEstateUi.setLongitude(dLon);
            createNewRealEstateUi.setLatitude(dLat);
            createNewRealEstateUi.setLongitudeOrientation(isEast);
            createNewRealEstateUi.setLatitudeOrientation(isNorth);
            this.realEstateService.addNewRealEstate(createNewRealEstateUi);

            // Nakoniec pridá ešte 5 nehnuteľností, všetky na [0,0] (budú stáť na 5 parcelách pridaných vyššie)
            if (realEstateI == numberOfRealEstates -1) {
                for (int i = 0; i < 5; i++) {
                    createNewRealEstateUi.setSupisneCislo(realEstateI);
                    createNewRealEstateUi.setPopisNehnutelnosti("Nehnuteľnosť " + realEstateI + " - " + i);
                    createNewRealEstateUi.setLongitude(0d);
                    createNewRealEstateUi.setLatitude(0d);
                    createNewRealEstateUi.setLongitudeOrientation(true);
                    createNewRealEstateUi.setLatitudeOrientation(true);
                    this.realEstateService.addNewRealEstate(createNewRealEstateUi);
                }
            }
        }
        return OperationMarker.OPERATION_SUCCESFUL;
    }

}
