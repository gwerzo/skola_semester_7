﻿using System.Collections.Generic;

namespace FriBettingWebApplication.Dto.Response
{
    public class TicketDetailResponse
    {

        public List<OddWithOddTypeAndMatchDetailResponse> OddsOnTicket { get; set; }
        public double TotalOddsValue { get; set; }
        public int TicketId { get; set; }

    }
}