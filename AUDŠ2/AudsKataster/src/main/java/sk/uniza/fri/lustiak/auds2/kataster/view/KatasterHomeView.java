package sk.uniza.fri.lustiak.auds2.kataster.view;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.*;


@SpringUI
@Theme("material")
@Title(value = "KatasterIS")
public class KatasterHomeView extends GenericVerticalView {

    private RepaintableComponent contentComponent;

    private MenuComponent menu;

    private SystemMenuComponent systemMenuComponent;

    private HomeViewForm homeViewForm;

    private SystemManagementComponent systemManagementViewForm;

    private Boolean menuShown = true;

    public KatasterHomeView() {
        super();
        initComponents();
    }

    private void initComponents() {

        Panel p = new Panel("Aplikácia AUDŠ2 KATASTER");
        p.setWidth("100%");
        p.setHeight("100%");
        p.setIcon(FontAwesome.CUBE);

        this.homeViewForm = new HomeViewForm();
        this.systemManagementViewForm = new SystemManagementComponent();
        this.contentComponent = new RepaintableComponent(this.homeViewForm);
        this.menu = new MenuComponent(contentComponent);
        this.systemMenuComponent = new SystemMenuComponent(contentComponent);

        HorizontalLayout wrapper = new HorizontalLayout();
        wrapper.setWidth("100%");
        wrapper.setHeight("100%");

        this.menu.setWidth("100%");
        this.menu.setIcon(FontAwesome.CUBES);
        this.menu.setCaption("Kataster");

        this.systemMenuComponent.setWidth("100%");
        this.systemMenuComponent.setIcon(FontAwesome.WRENCH);
        this.systemMenuComponent.setCaption("Systém");

        this.contentComponent.setWidth("100%");

        TabSheet menuWrapper = new TabSheet();
        menuWrapper.addTab(this.menu);
        menuWrapper.addTab(this.systemMenuComponent);

        GridLayout grid = new GridLayout(6, 1);
        grid.setSizeFull();
        grid.addComponent(menuWrapper, 0, 0, 1, 0);
        grid.addComponent(this.contentComponent, 2, 0, 5, 0);

        menuWrapper.addSelectedTabChangeListener(selectedTabChangeEvent -> {
            if (!menuShown) {
                this.contentComponent.removeAllComponents();
                this.contentComponent.addComponent(this.homeViewForm);
                menuShown = !menuShown;
            } else {
                this.contentComponent.removeAllComponents();
                this.contentComponent.addComponent(this.systemManagementViewForm);
                menuShown = !menuShown;
            }
        });

        menuWrapper.setSizeFull();

        p.setContent(grid);

        addComponent(p);
    }


}
