package sk.uniza.fri.lustyno.AudsLetisko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AudsLetiskoApplication {
	public static void main(String[] args) {
		SpringApplication.run(AudsLetiskoApplication.class, args);
	}

}
