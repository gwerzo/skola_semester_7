package sk.uniza.fri.lustyno.datastructures.queue;

import sk.uniza.fri.lustyno.datastructures.entity.HasComparableValue;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureNullKeyException;
import sk.uniza.fri.lustyno.datastructures.exception.DataStructureWrongUsageException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PairingHeap<K extends HasComparableValue, D> {

    private QueueNode<K, D> root;

    private int size;

    public PairingHeap() {
        this.root = null;
        this.size = 0;
    }

    public QueueNode<K, D> min() {
        return this.root;
    }

    public void changePriority(QueueNode<K, D> ofNode, K newPriority) {
        if (newPriority == null) {
            throw new DataStructureWrongUsageException(
                    this.getClass().getName(), "changePriority", "Nová priorita nesmie byť null");
        }
        int whenCompared = ofNode.getKey().getValue().compareTo(newPriority.getValue());
        if (whenCompared > 0) {
            increasePriority(ofNode, newPriority);
        } else if (whenCompared < 0) {
            decreasePriority(ofNode, newPriority);
        } else {
            return;
        }
    }

    /**
     * Operácia zvýšenia priority nodu X,
     * ktorá sa riadi nasledujúcimi pravidlami
     *  -> Vrcholu X zvýš prioritu
     *  -> Odstrihni vrcholu X pravého syna a otcovi X nahraď pravé/ľavé dieťa odstrihnutým pravým synom vrchola X
     *  -> Vrchol X mergni s vrcholom haldy
     *
     * @param ofNode - node, ktorému sa má zvýšiť priorita
     * @param newPriority - nová priorita, ktorá má byť nodu nastavená
     */
    private void increasePriority(QueueNode<K, D> ofNode, K newPriority) {
        // Ak node je root, len zvýš prioritu
        if (ofNode.isRoot()) {
            ofNode.setKey(newPriority);
        } else {

            ofNode.setKey(newPriority);
            boolean wasRightChild = ofNode.isRightChildOfParent();
            QueueNode<K, D> nodeParent = ofNode.getParent();
            QueueNode<K, D> rightSonOnNode = ofNode.getRightChild();

            ofNode.setRightChild(null);
            ofNode.setParent(null);

            if (wasRightChild) {
                nodeParent.setRightChild(rightSonOnNode);
            } else {
                nodeParent.setLeftChild(rightSonOnNode);
            }

            if (rightSonOnNode != null) {
                rightSonOnNode.setParent(nodeParent);
            }

            this.root = this.merge(ofNode, this.root);
        }
    }

    /**
     * Zníži prioritu danému nodu
     *
     * @param ofNode - node, ktorému má znížiť prioritu
     * @param newPriority - nová nižšia priorita
     */
    private void decreasePriority(QueueNode<K, D> ofNode, K newPriority) {
        if (ofNode == null) {
            return;
        }
        if (ofNode.isRoot()) {
            LinkedList<QueueNode<K, D>> mergeStack = new LinkedList<>();
            mergeStack.add(ofNode);
            ofNode.setKey(newPriority);

            QueueNode<K, D> actualNode = ofNode.getLeftChild();
            ofNode.setLeftChild(null);
            while (actualNode != null) {
                QueueNode<K, D> toTheRight = actualNode.getRightChild();
                actualNode.setParent(null);
                actualNode.setRightChild(null);
                mergeStack.add(actualNode);
                actualNode = toTheRight;
            }

            while(mergeStack.size() > 1) {
                for (int i = 0; i < mergeStack.size(); i+=2) {
                    QueueNode<K, D> merger1 = mergeStack.removeFirst();
                    QueueNode<K, D> merger2 = mergeStack.removeFirst();
                    QueueNode<K, D> merged = this.merge(merger1, merger2);
                    mergeStack.add(merged);
                }
            }
            this.root = mergeStack.size() != 0 ? mergeStack.getFirst() : null;
            if (this.root != null) {
                this.root.setParent(null);
                this.root.setRightChild(null);
            }

        } else {
            QueueNode<K, D> parentNode = ofNode.getParent();
            QueueNode<K, D> rightChild = ofNode.getRightChild();
            QueueNode<K, D> leftChild = ofNode.getLeftChild();
            boolean wasRightChild = ofNode.isRightChildOfParent();
            ofNode.setKey(newPriority);

            ofNode.setRightChild(null);
            ofNode.setParent(null);
            ofNode.setLeftChild(null);

            LinkedList<QueueNode<K, D>> mergeStack = new LinkedList<>();
            mergeStack.add(ofNode);

            QueueNode<K, D> actualNode = leftChild;
            while (actualNode != null) {
                QueueNode<K, D> toTheRight = actualNode.getRightChild();
                actualNode.setParent(null);
                actualNode.setRightChild(null);
                mergeStack.add(actualNode);
                actualNode = toTheRight;
            }

            while(mergeStack.size() > 1) {
                for (int i = 0; i < mergeStack.size(); i+=2) {
                    QueueNode<K, D> merger1 = mergeStack.removeFirst();
                    QueueNode<K, D> merger2 = mergeStack.removeFirst();
                    QueueNode<K, D> merged = this.merge(merger1, merger2);
                    mergeStack.add(merged);
                }
            }

            QueueNode<K, D> merged = mergeStack.size() != 0 ? mergeStack.getFirst() : null;
            if (merged == null) {
                return;
            }
            // Nastavenie referencií zmergovaného so starým parentom
            merged.setParent(parentNode);
            if (wasRightChild) {
                parentNode.setRightChild(merged);
            } else {
                parentNode.setLeftChild(merged);
            }
            // Nastavenie referencií zmergovaného s bývalým pravým stromom
            merged.setRightChild(rightChild);
            if (rightChild != null) {
                rightChild.setParent(merged);
            }

        }
    }

    /**
     * Operácia vymazania minimálneho prvku,
     * ktorá odoberie prvok z vrchola stromu
     * a mergne ľavého syna vrcholu s jeho bratmi
     * ktorí sú ale v binárnej reprezentácii
     * zreťazenými pravými synmi,
     * t.j. ak ľavý syn vrchola je X,
     * tak myslený zoznam bude vyzerať ako
     * X -> X.pravýSyn -> X.pravýSyn.pravýSyn -> X.pravýSyn.pravýSyn.pravýSyn -> ...
     *
     * @return - node, ktorý bol vymazaný
     */
    public QueueNode<K, D> deleteMin() {
        QueueNode<K, D> beingDeleted = this.root;
        QueueNode<K, D> leftChildOfRoot = this.root.getLeftChild();

        LinkedList<QueueNode<K, D>> mergeStack = new LinkedList<>();
        QueueNode<K, D> actualNode = leftChildOfRoot;
        while (actualNode != null) {
            mergeStack.add(actualNode);
            actualNode = actualNode.getRightChild();
        }

        while (mergeStack.size() > 1) {
            for (int i = 0; i < mergeStack.size(); i+=2) {
                QueueNode<K, D> merger1 = mergeStack.removeFirst();
                QueueNode<K, D> merger2 = mergeStack.removeFirst();

                QueueNode<K, D> merged = this.merge(merger1, merger2);
                mergeStack.add(merged);
            }
        }

        this.root = mergeStack.size() != 0 ? mergeStack.getFirst() : null;
        if (this.root != null) {
            this.root.setParent(null);
            this.root.setRightChild(null);
        }
        this.size--;
        return beingDeleted;
    }

    /**
     * Vymaže node, na ktorý ukazuje parameter,
     * t.j. metóda, ktorá vymaže prvok, ktorý bol už predtým nájdený,
     * resp. bola na neho odložená referencia
     *
     * @param nodeToDelete - node, ktorý sa má vymazať z frontu
     */
    @SuppressWarnings(value = "unchecked")
    public void delete(QueueNode<K, D> nodeToDelete) {
        K minKey = (K) nodeToDelete.getKey().getMinValue();
        this.increasePriority(nodeToDelete, minKey);
        if (!nodeToDelete.equals(this.root)) {
            throw new DataStructureWrongUsageException(
                    this.getClass().getName(),
                    "delete",
                    "Nodu sa mala zmeniť priorita na maximum a mal sa dostať do koreňa, čo sa ale nepodarilo");
        }
        this.deleteMin();
    }

    /**
     * Insertne do haldy predaný node z parametra,
     * pričom využije operáciu {@link #merge(QueueNode, QueueNode)}
     *
     * @param nodeToInsert - node, ktorý sa má do haldy pridať
     * @return - vrchol haldy
     * @throws sk.uniza.fri.lustyno.datastructures.exception.DataStructureNullKeyException ak predaný kľúč je null
     */
    public QueueNode<K, D> insert(QueueNode<K, D> nodeToInsert) {
        if (nodeToInsert.getKey() == null || nodeToInsert.getKey().getValue() == null) {
            throw new DataStructureNullKeyException(this.getClass().getName(), "insert");
        }
        QueueNode<K, D> merged = merge(this.root, nodeToInsert);
        this.root = merged;
        this.size++;
        return nodeToInsert;
    }

    /**
     * Inserte do haldy nový node s danou prioritou z kľúča a danými dátami
     *
     * @param key - priorita insertovaného prvku
     * @param data - dáta insertovaného prvku
     * @return - vráti vložený node
     */
    public QueueNode<K, D> insert(K key, D data) {
        return this.insert(new QueueNode<K, D>(key, data));
    }


    /**
     * Mergne dva binárne stromy reprezentované nodami {@link QueueNode}
     * kde strom môže pozostávať aj z jedného vrcholu
     *
     * @param node - node, ktorý sa má mergnúť s druhým nodom
     * @param mergeWith - node, ktorý sa má mergnúť s prvým nodom
     * @return - vráti mergnutý strom, t.j. jeden {@link QueueNode}
     *           ktorý reprezentuje spojené stromy,
     *           alebo reprezentuje jediný @notNull strom, ktorý bol v parametroch
     */
    private QueueNode<K, D> merge(QueueNode<K, D> node, QueueNode<K, D> mergeWith) {
        if (node == null && mergeWith == null) {
            return null;
        }
        // Ak niektorý zo stromov z parametrov je null, vráť len jeden @notNull strom
        // Využitie napríklad pri vkladaní root nodu do haldy
        if (node == null || mergeWith == null) {
            return node == null ? mergeWith : node;
        }

        // Oba stromy nie sú null, vykonaj dve pravidlá
        QueueNode<K, D> higherPriorityTree =
                node.getKey().getValue().compareTo(mergeWith.getKey().getValue()) < 0 ? node : mergeWith;
        QueueNode<K, D> lowerPriorityTree =
                node.getKey().getValue().compareTo(mergeWith.getKey().getValue()) < 0 ? mergeWith : node;
        QueueNode<K, D> leftSonOfHigherPriorityTree =
                higherPriorityTree.getLeftChild();

        // Vrchol s nižšou prioritou sa stane ľavým synom vrchola s vyššou prioritou
        lowerPriorityTree.setParent(higherPriorityTree);
        higherPriorityTree.setLeftChild(lowerPriorityTree);


        // Predošlý ľavý syn vrchola s vyššou prioritou sa stane pravým synom vrchola s nižšou prioritou
        lowerPriorityTree.setRightChild(leftSonOfHigherPriorityTree);
        if (leftSonOfHigherPriorityTree != null) {
            leftSonOfHigherPriorityTree.setParent(lowerPriorityTree);
        }

        return higherPriorityTree;
    }

    /**
     * Vráti lever order prehliadku haldy,
     * ktorá je reprezentovaná zoznamom QueneNode
     *
     * @return - zoznam nodov v level order prehliadke
     */
    public List<QueueNode<K, D>> levelOrder() {
        if (this.root == null) {
            return null;
        }

        List<QueueNode<K, D>> toRet = new ArrayList<>(this.size);

        LinkedList<QueueNode<K, D>> queue = new LinkedList<>();
        queue.add(this.root);
        while (!queue.isEmpty()) {
            QueueNode<K, D> node = queue.removeFirst();
            toRet.add(node);
            if (node.getLeftChild() != null) {
                queue.add(node.getLeftChild());
            }
            if (node.getRightChild() != null) {
                queue.add(node.getRightChild());
            }
        }
        return toRet;
    }

    public int getSize() {
        return this.size;
    }
}
