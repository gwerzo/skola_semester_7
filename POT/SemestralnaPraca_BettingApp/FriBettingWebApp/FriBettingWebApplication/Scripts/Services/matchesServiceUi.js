﻿/**
 * JS metódy pre integráciu na API poskytovanú controllerom MatchController.cs
 */
function createNewMatch(sportId, competitor1Id, competitor2Id, dateTime) {

    $.ajax({
        'url': "/api/matches",
        'method': "POST",
        'data': JSON.stringify({
            "SportId": sportId,
            "HostId": competitor1Id,
            "GuestId": competitor2Id,
            "DateTimeOfMatch": dateTime
        }),
        'headers': {
            'Authorization': localStorage.getItem("fri-betting-app-token")
        },
        'contentType': "application/json",
        'dataType': "json",
        'success': function (data) {
            if (data["Successful"]) {
                $(".form-box").removeClass("breathing");
                $(".form-error-container").addClass("hidden");
                $(".form-box").addClass("form-successfully-submitted");
                $(".action-button").unbind('click');
                window.setTimeout(function () {
                        window.location = "/";
                    },
                    3000);
            } else {
                $(".form-box").removeClass("breathing");
                $(".form-box").addClass("form-unsuccessfully-submitted");
                $(".form-error-message").text(data["ErrorMessage"]);
                $(".form-error-container").removeClass("hidden");
            }
        }
    });
}

// Resetne page/size a nastaví len dátumy a presmeruje použ. na GET MVC daného controllera
function findMatchesFiltered() {
    var fromDate = $("#datepickerFrom").val();
    var toDate = $("#datepickerTo").val();
    window.location = "?Page=" + 0 + "&Size=" + 10 + "&FromDate=" + fromDate + "&ToDate=" + toDate;
}

function getOddsForMatch(matchId, callback) {
    $.ajax({
        'url': "/api/matches/odds?matchId=" + matchId,
        'method': "GET",
        'headers': {
            'Authorization': localStorage.getItem("fri-betting-app-token")
        },
        'success': function (data) {
            callback(data);
        }
    });
}


function evaluateMatchRequest(matchEvaluation, callback) {
    $.ajax({
        'url': "/api/matches/evaluate",
        'method': "POST",
        'data': JSON.stringify({
            "MatchId": matchEvaluation["MatchId"],
            "PartsEvaluation": matchEvaluation["PartsEvaluation"]
        }),
        'headers': {
            'Authorization': localStorage.getItem("fri-betting-app-token")
        },
        'contentType': "application/json",
        'dataType': "json",
        'success': function (data) {
            callback(data);
        }
    });
}