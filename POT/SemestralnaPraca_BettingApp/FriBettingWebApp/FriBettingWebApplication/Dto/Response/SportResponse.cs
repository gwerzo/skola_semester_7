﻿using System;
using Newtonsoft.Json;

namespace FriBettingWebApplication.Dto.Response
{
    public class SportResponse
    {
        [JsonProperty("sport_id")]
        public int SportId { get; set; }
        [JsonProperty("sport_name")]
        public String SportName { get; set; }

    }
}