﻿using System;
using System.Collections.Generic;
using MemoryGameDaoApp.Bo;
using MemoryGameServerApp.Engine;
using MemoryGameServerApp.Server;
using SuperWebSocket;

namespace MemoryGameServerApp.Command
{
    class CreateInviteToGameCommand: ICommand
    {
        public const string COMMAND = "-invitetogame";
        private const string INVITE_CREATED = "-invitecreated";
        private const string INVITE_UNSUCC = "-inviteunsucc";
        private const string INVITED_BY = "-invitedby";

        private int _gameType;
        private int _gameStyle;
        private string _forPlayer;

        public CreateInviteToGameCommand(int gameType, int gameStyle, string forPlayer)
        {
            this._gameType = gameType;
            this._gameStyle = gameStyle;
            this._forPlayer = forPlayer;
        }

        public void executeCommand(string usernameOfCommandCaller, 
            MemoryGameServer memoryGameServer, WebSocketSession callerSocketSession)
        {
            if (usernameOfCommandCaller.Equals(this._forPlayer))
            {
                callerSocketSession.Send(INVITE_UNSUCC + " Nemôžete vyzvať sám seba!");
                return;
            }

            GameType gameType = memoryGameServer.GetGameTypeById(this._gameType);
            if (gameType == null)
            {
                callerSocketSession.Send(INVITE_UNSUCC + " Typ hry neexistuje");
                return;
            }

            List<int> gameCardIds = memoryGameServer.GetCardIdsForGameType(this._gameStyle);
            if (gameCardIds == null || gameCardIds.Count == 0)
            {
                callerSocketSession.Send(INVITE_UNSUCC + " K tomuto štýlu neexistujú karty");
                return;
            }

            // Nová hra s vyplneným hráčom 2
            MemoryGameState memoryGameState = new MemoryGameState(
                usernameOfCommandCaller, this._forPlayer,
                gameType.XLength, gameType.YLength,
                gameCardIds, new Random().Next(), this._gameStyle, this._gameType);
            string error;
            string gameUuid = memoryGameServer.CreateInviteToGame(memoryGameState, out error);
            if (gameUuid != null)
            {
                callerSocketSession.Send(INVITE_CREATED + " " + gameUuid + " " + gameType.XLength + " " + gameType.YLength);
                memoryGameServer.SendMessageTo(this._forPlayer, INVITED_BY + " " + usernameOfCommandCaller);
            }
            else
            {
                callerSocketSession.Send(INVITE_UNSUCC + " " + error);
            }
        }
    }
}
