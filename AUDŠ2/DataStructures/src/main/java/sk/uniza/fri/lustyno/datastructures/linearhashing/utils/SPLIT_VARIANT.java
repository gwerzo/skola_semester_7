package sk.uniza.fri.lustyno.datastructures.linearhashing.utils;

public enum SPLIT_VARIANT {

    SPLIT_VARIANT_LOAD_FACTOR,

    SPLIT_VARIANT_NUMBER_OF_OVERFLOW_BLOCKS,

    SPLIT_VARIANT_RATIO_PRIMARY_OVERFLOW_BLOCKS;

}
