package sk.uniza.fri.lustyno.datastructures.utils;


public class BitUtils {

    /**
     * Konvertuje integer na
     * {@link CustomBitSet}
     * Vráti ale hodnoty ODZADU!!!
     *
     * @param fromInteger - zdrojový integer
     * @param numberOfBits - koľko bitov sa má do CustomBitSetu pridať z integeru
     */
    public static CustomBitSet getBitsOfInt(int fromInteger, int numberOfBits) {
        CustomBitSet bits = new CustomBitSet(numberOfBits);
        int index = 0;
        while (fromInteger != 0 && index < numberOfBits) {
            if (fromInteger % 2 != 0) {
                bits.set(index, true);
            } else {
                bits.set(index, false);
            }
            ++index;
            fromInteger = fromInteger >>> 1;
        }
        return bits;
    }

    /**
     * Konvertuje CustomBitSet na int
     * @param fromCustomBitSet - Zdrojový CustomBitSet
     */
    public static int fromCustomBitSet(CustomBitSet fromCustomBitSet) {
        int value = 0;
        for (int i = 0; i < fromCustomBitSet.length(); ++i) {
            boolean d = fromCustomBitSet.get(i);
            if (d) {
                value = (value << 1) + 1;
            } else {
                value = value << 1;
            }
        }
        return value;
    }

    public static boolean equalsOmitLastNBits(Integer int1, Integer int2, int numberOfLSFBitsToOmit) {
        int shifted1 = int1 >>> numberOfLSFBitsToOmit;
        int shifted2 = int2 >>> numberOfLSFBitsToOmit;
        return shifted1 == shifted2;
    }

    /**
     * Vráti, či n-tá hodnota LSB (najmenej významného bitu čísla)
     * je bit 0, alebo bit 1
     *
     * Inými slovami, vráti hodnotu n-tého bitu sprava
     *
     * @param fromNumber - Z akého čísla
     * @param nThLSB - n (koľký bit sprava) (číslovanie začína od 0)
     * @return
     */
    public static boolean getNthLSBValue(Integer fromNumber, int nThLSB) {
        int shifted = fromNumber >>> nThLSB;
        return shifted % 2 == 1;
    }

    /**
     * Z čísla vezme posledný n bitov
     * a zostrojí z nich nové číslo
     *
     * Ex. n = 3 a cislo = 1110111101 ==> vráti 5 (101)
     *
     * @param fromNumber
     * @param numberOfBits
     * @return
     */
    public static Integer constructNumberFromLastNBitsOfNumber(Integer fromNumber, int numberOfBits) {
        return null;
    }

}
