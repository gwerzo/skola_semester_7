﻿using MemoryGameServerApp.Server;
using SuperWebSocket;

namespace MemoryGameServerApp.Command
{
    class AuthenticateCommand: ICommand
    {
        public const string COMMAND            = "-authenticate";
        private const string AUTH_SUCC          = "-authsucc";
        private const string AUTH_UNSUCC        = "-authunsucc";

        public string Username { get; set; }
        public string Password { get; set; }

        public AuthenticateCommand(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }

        public void executeCommand(string usernameOfCommandCaller, 
            MemoryGameServer memoryGameServer, WebSocketSession callerSocketSession)
        {
            string generatedSocketSessionUuid;
            bool authenticated = memoryGameServer.AuthenticateUser(this.Username, this.Password, callerSocketSession, out generatedSocketSessionUuid);
            if (authenticated)
            {
                callerSocketSession.Send(AUTH_SUCC + " " + generatedSocketSessionUuid);
            }
            else
            {
                callerSocketSession.Send(AUTH_UNSUCC);
            }
        }
    }
}
