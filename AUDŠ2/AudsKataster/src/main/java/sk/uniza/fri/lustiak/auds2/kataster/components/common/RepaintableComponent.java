package sk.uniza.fri.lustiak.auds2.kataster.components.common;

import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

public class RepaintableComponent extends VerticalLayout {

    private Component actualComponent;

    public RepaintableComponent(Component component) {
        this.actualComponent = component;
        this.setSizeFull();
        addComponent(actualComponent);
    }

    /*
     * Vyhodí z komponentu všetky componenty a pridá len ten, ktorý prišiel v parametri
     */
    public void showComponent(Component component) {
        this.removeAllComponents();
        addComponent(component);
    }

}
