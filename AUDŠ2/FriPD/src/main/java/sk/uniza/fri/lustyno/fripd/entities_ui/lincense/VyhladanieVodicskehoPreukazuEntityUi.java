package sk.uniza.fri.lustyno.fripd.entities_ui.lincense;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class VyhladanieVodicskehoPreukazuEntityUi {

    private Integer evcPreukazu;

}
