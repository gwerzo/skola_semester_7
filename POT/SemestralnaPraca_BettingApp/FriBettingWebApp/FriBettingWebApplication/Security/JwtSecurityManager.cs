﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace FriBettingWebApplication.Security
{
    /// <summary>
    /// Implementácia generovania JWT tokenu podľa zdroja
    /// <source>https://stackoverflow.com/a/40284152/11190073</source>
    /// <source>https://github.com/cuongle/WebApi.Jwt/blob/master/WebApi.Jwt/JwtManager.cs</source>
    /// </summary>
    public class JwtSecurityManager
    {
        public const string SECRET = "idkLYx/Yo/NzjP7efKeKd87bVRtpCwGXGdIVSNM/YQ3dolfYH9RYqBmQNjGjLECs55UHMDXlerBBeY95dYTysQ==";

        // Defaultný token, ktorý môže využívať MVC pre volanie zakrytej API, pričom API môže ostať stále zakrytá cez JWT auth
        // Vygenerovaný na cca. 10 rokov v minútach (Int.MaxValue tam spôsoboval problémy)
        public static string ApplicationJwtToken = GenerateToken("app_jwt", 5259487);


        public static string GenerateToken(string username, int expireMinutes = 20)
        {
            var symmetricKey = Convert.FromBase64String(SECRET);
            var tokenHandler = new JwtSecurityTokenHandler();

            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, username)
                }),

                Expires = now.AddMinutes(Convert.ToInt32(expireMinutes)),

                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(symmetricKey),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);

            return token;
        }

        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                var symmetricKey = Convert.FromBase64String(SECRET);

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };

                var principal = tokenHandler.ValidateToken(token, validationParameters, out _);

                return principal;
            }

            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Z tokenu v tvare "Bearer XXXXX.." extrahuje používateľskú identitu
        /// Metódu treba volať iba v autentifikovanom kontexte, t.j.
        /// </summary>
        /// <param name="token">Token v tvare "Bearer XXXX..." kde XXXX je encoded časť tokenu</param>
        /// <returns>Username(identitu) používateľa z tokenu, resp. null ak je token nevalídny</returns>
        public static string ExtractUsernameFromToken(string token)
        {
            if (token == null)
            {
                return null;
            }

            if (!token.StartsWith("Bearer "))
            {
                return null;
            }
            token = token.Replace("Bearer ", "");
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                var symmetricKey = Convert.FromBase64String(SECRET);

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };

                var principal = tokenHandler.ValidateToken(token, validationParameters, out _);

                return principal.Identity.Name;
            }

            catch (Exception)
            {
                return null;
            }
        }

    }
}