package sk.uniza.fri.lustyno.datastructures.extendiblehashing;

import sk.uniza.fri.lustyno.datastructures.extendiblehashing.entities.EHEntity;
import sk.uniza.fri.lustyno.datastructures.utils.FromByteArrayConverter;
import sk.uniza.fri.lustyno.datastructures.utils.ToByteArrayConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * // TODO hodilo by sa veľa funkcionality vytiahnúť do spoločného predka s
 * @see EHBlock
 */
public class EHOverflowBlock<T extends EHEntity<T>> {

    /**
     * Blokovací faktor
     */
    private Integer numberOfEntitiesInBlock;

    /**
     * Adresa/Index ďalšieho overflow bloku
     */
    private Integer nextOverflowBlock = -1;

    /**
     * Entity
     */
    private List<T> entities;

    private T entityPrototype;

    public EHOverflowBlock(Integer numberOfEntitiesInBlock, T entityPrototype, boolean initWithInvalidEntities) {
        this.numberOfEntitiesInBlock = numberOfEntitiesInBlock;
        this.entityPrototype = entityPrototype;
        this.entities = new ArrayList<>(numberOfEntitiesInBlock);
        if (initWithInvalidEntities) {
            this.initWithInvalidEntities();
        }
    }

    private void initWithInvalidEntities() {
        for (int i = 0; i < this.numberOfEntitiesInBlock; i++) {
            this.entities.add(this.entityPrototype);
        }
    }

    /**
     * Vráti príznak, či blok obsahuje entitu podľa predanej kľúčovej entity
     * @param entity - kľúčová entita
     *
     * @return - príznak, či blok obsahoval entitu
     */
    protected boolean contains(T entity) {
        for (T en : this.entities) {
            if (en.isValid() && en.equalsOtherEntity(entity)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Pokúsi sa znevalidovať entitu a vráti, či sa to podarilo,
     * nepodarí sa to v prípade, že blok entitu neobsahuje
     *
     * @param entity - kľúčová entita
     * @return - či sa podarilo entitu znevalidovať
     */
    protected T invalidateEntity(T entity) {
        for (T en : this.entities) {
            if (en.isValid() && en.equalsOtherEntity(entity)) {
                en.setIsValid(Boolean.FALSE);
                return en;
            }
        }
        return null;
    }

    public Integer getNextOverflowBlock() {
        return this.nextOverflowBlock;
    }

    public void setNextOverflowBlock(Integer nextOverflowBlock) {
        this.nextOverflowBlock = nextOverflowBlock;
    }

    public Integer getNumberOfEntitiesInBlock() {
        return this.numberOfEntitiesInBlock;
    }

    protected int getBlockSize() {
        return
                Constants.JAVA_INTEGER_SIZE + // Overflow block index
                this.numberOfEntitiesInBlock * entityPrototype.getSizeInternally(); // počet entít v bloku krát ich veľkosť
    }

    public List<T> getEntities() {
        return this.entities;
    }

    /**
     * Vráti, či je blok "plný", t.j. či počet
     * validných entít sa rovná blokovaciemu faktoru
     *
     * @return - príznak, či je blok plný alebo nie
     */
    private Boolean isFull() {
        long countOfValidEntities = this.entities.stream().filter(EHEntity::isValid).count();
        if (countOfValidEntities == this.numberOfEntitiesInBlock) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * Vyčistí blok tak, že ho celý vyplní novými (INVALID) prototypmi entít
     */
    protected void clear() {
        List<T> clearEntities = new ArrayList<>();
        for (int i = 0; i < this.numberOfEntitiesInBlock; i++) {
            clearEntities.add(this.entityPrototype);
        }
        this.entities = clearEntities;
    }

    /**
     * Vráti, či počet valid entít v bloku je 0, t.j. či je prázdny
     * @return
     */
    public boolean isEmpty() {
        long countOfValidEntities = this.entities.stream().filter(EHEntity::isValid).count();
        return countOfValidEntities == 0L;
    }

    /**
     * Pridá do bloku entitu a to takou formou,
     * že ju umiestni do zoznamu entít na
     * prvé miesto, ktoré obsahuje "NEVALIDNU" entitu
     *
     * @return - príznak, či sa entita vložila alebo nevložila (nevloží sa ak je blok plný)
     */
    public Boolean insertEntity(T entity) {
        if (this.isFull()) {
            return Boolean.FALSE;
        }
        // Nájdenie prvého indexu s "nevalidnou" entitou a rovno vloženie entity na tento index
        for (int i = 0; i < this.numberOfEntitiesInBlock; i++) {
            if (!this.entities.get(i).isValid()) {
                entity.setIsValid(Boolean.TRUE);
                this.entities.set(i, entity);
                break;
            }
        }
        return Boolean.TRUE;
    }

    /**
     * Pokúsi sa znevalidovať entitu a vráti,
     * či sa to podarilo alebo nie
     * @param entity
     * @return
     */
    public Boolean tryInvalidateEntity(T entity) {
        for (int i = 0; i < this.numberOfEntitiesInBlock; i++) {
            if (this.entities.get(i).equalsOtherEntity(entity)) {
                this.entities.get(i).setIsValid(Boolean.FALSE);
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    /**
     * Serializuje celý blok do byte[]
     * @return
     */
    public byte[] toBytes() {

        List<byte[]> bytes = new ArrayList<>();

        bytes.add(ToByteArrayConverter.bytesFromInt(this.nextOverflowBlock));

        bytes.addAll(this.entities.stream().map(EHEntity::toByteArrayInternally).collect(Collectors.toList()));

        return ToByteArrayConverter.concatenateByteArrays(
                bytes
        );
    }

    public EHOverflowBlock<T> fromBytes(byte[] bytes) {
        EHOverflowBlock<T> fromBytesBlock = new EHOverflowBlock<>(this.numberOfEntitiesInBlock, this.entityPrototype, false);

        fromBytesBlock.nextOverflowBlock = FromByteArrayConverter.integerFromBytes(Arrays.copyOfRange(bytes, 0, 4));

        int entitiesStartingIndex = 4;

        // For cyklus od počiatočného indexu, skáče o veľkosť v bytoch jednej entity
        for (int i = entitiesStartingIndex; i < entitiesStartingIndex + (this.numberOfEntitiesInBlock * this.entityPrototype.getSizeInternally()); i+= this.entityPrototype.getSizeInternally()) {
            byte[] singleEntityBytes = Arrays.copyOfRange(bytes, i, i + this.entityPrototype.getSizeInternally());
            T deserializedEntity = this.entityPrototype.clone();
            EHEntity<T>.InternalEntity internalEntity = deserializedEntity.fromByteArrayInternally(singleEntityBytes);
            deserializedEntity = internalEntity.getEntity();
            deserializedEntity.setIsValid(internalEntity.getIsValid());
            fromBytesBlock.entities.add(deserializedEntity);
        }
        return fromBytesBlock;
    }

}
