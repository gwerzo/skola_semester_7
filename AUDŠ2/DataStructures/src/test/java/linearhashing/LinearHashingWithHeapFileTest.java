package linearhashing;

import linearhashing.entities.CarHashingEntity;
import linearhashing.entities.VINCODEIndexEntity;
import org.junit.jupiter.api.*;
import sk.uniza.fri.lustyno.datastructures.entity.keys.StringKey;
import sk.uniza.fri.lustyno.datastructures.linearhashing.file.File;
import sk.uniza.fri.lustyno.datastructures.linearhashing.heapfile.HeapFile;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.FromBytesFactory;
import sk.uniza.fri.lustyno.datastructures.linearhashing.utils.SPLIT_VARIANT;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LinearHashingWithHeapFileTest {

    private String DATABASE_VERSION = "VIN_INDEX_VERSION_" + "1";
    private String DATABASE_HEAP_VERSION = "HEAPFILE_VERSION_" + "1";

    @Test
    @Order(1)
    public void testCreatingHeapFileAndLinearHashingFile() {
        File<StringKey, VINCODEIndexEntity> filero = new File<>(
                DATABASE_VERSION, 10, 3,
                VINCODEIndexEntity.class,
                SPLIT_VARIANT.SPLIT_VARIANT_LOAD_FACTOR,
                new FromBytesFactory<VINCODEIndexEntity>() {
                    @Override
                    public VINCODEIndexEntity fromBytes(byte[] bytes) {
                        return VINCODEIndexEntity.fromByteArray(bytes);
                    }

                    @Override
                    public VINCODEIndexEntity emptyInvalidInstance() {
                        return VINCODEIndexEntity.emptyInvalidInstance();
                    }
                });

        HeapFile<CarHashingEntity> heapFile = new HeapFile<>(
                DATABASE_HEAP_VERSION, CarHashingEntity.class,
                10,
                new FromBytesFactory<CarHashingEntity>() {
                    @Override
                    public CarHashingEntity fromBytes(byte[] bytes) {
                        return CarHashingEntity.fromByteArray(bytes);
                    }

                    @Override
                    public CarHashingEntity emptyInvalidInstance() {
                        return CarHashingEntity.emptyInvalidInstance();
                    }
                }
        );


        List<CarHashingEntity> entitiesToFind = new ArrayList<>();

        for (int i = 0; i < 10000; i++) {
            CarHashingEntity en = new CarHashingEntity(CarHashingEntity.randomVIN(), CarHashingEntity.randomSPZ(),
                    4, false, LocalDateTime.now(), LocalDateTime.now());
            en.setValid(true);
            entitiesToFind.add(en);
            int storedOffset = heapFile.addRecord(en);
            filero.saveRecord(new VINCODEIndexEntity(en.getVIN_KOD(), storedOffset));
        }

        for (CarHashingEntity en : entitiesToFind) {
            VINCODEIndexEntity foundOffset =
                    (VINCODEIndexEntity) filero.find(new VINCODEIndexEntity(en.getVIN_KOD(), /*NOT NEEDED*/0));

            CarHashingEntity foundEntity = (CarHashingEntity) heapFile.getRecordAtOffset(foundOffset.getOffsetInHeapFile());
            Assertions.assertEquals(foundEntity.getVIN_KOD(), en.getVIN_KOD());
        }

        filero.close();
        heapFile.close();

    }

}
