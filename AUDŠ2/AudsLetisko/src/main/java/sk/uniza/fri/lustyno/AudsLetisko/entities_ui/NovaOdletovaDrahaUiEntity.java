package sk.uniza.fri.lustyno.AudsLetisko.entities_ui;

import lombok.Data;

import java.util.Random;

@Data
public class NovaOdletovaDrahaUiEntity {

    private Integer id;

    private Integer dlzkaDrahy;

    private static Random rnd = new Random();

    public void flush() {
        this.id = null;
        this.dlzkaDrahy = null;
    }

    public static Integer randomDlzka() {
        Integer[] dlzky = {1500, 2000, 2500, 3000, 3500};
        return dlzky[rnd.nextInt(dlzky.length)];
    }

}
