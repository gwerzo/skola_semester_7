package sk.uniza.fri.lustyno.AudsLetisko.service;

import org.springframework.stereotype.Service;
import sk.uniza.fri.lustyno.AudsLetisko.BusinessException;
import sk.uniza.fri.lustyno.AudsLetisko.entities_ui.ZmenaSystemovehoCasuUiEntity;

import java.time.LocalDateTime;

@Service
public class DateTimeService {

    private LocalDateTime actualDateTime;

    public DateTimeService() {
        this.actualDateTime = LocalDateTime.now();
    }

    public LocalDateTime getActualDateTime() {
        return this.actualDateTime;
    }

    public void setActualDateTime(LocalDateTime to) {
        if (to.compareTo(actualDateTime) <= 0) {
            throw new BusinessException("Čas je možné meniť len dopredu");
        }
        this.actualDateTime = to;
    }

    public void setActualDateTime(ZmenaSystemovehoCasuUiEntity entity) {

        LocalDateTime to = null;
        try {
            to = LocalDateTime.of(entity.getRok(), entity.getMesiac(), entity.getDen(), entity.getHodina(), entity.getMinuta());
        } catch (Exception e) {
            throw new BusinessException("Zlý formát dátumu");
        }

        if (to.compareTo(actualDateTime) <= 0) {
            throw new BusinessException("Čas je možné meniť len dopredu");
        }
        this.actualDateTime = to;
    }

}
