﻿using System;

namespace ComplexNumberLib
{
    public struct ComplexNumber: IEquatable<ComplexNumber>, IEquatable<double>
    {

        public static ComplexNumber IMAGINARY_ONE = new ComplexNumber(0, 1);
        public static ComplexNumber ONE = new ComplexNumber(1);
        public static ComplexNumber ZERO = new ComplexNumber(0, 0);

        private double _imaginary;
        private double _magnitude;
        private double _real;

        public double Imaginary
        {
            get => _imaginary;
            set => _imaginary = value;
        }

        public double Magnitude
        {
            get => _magnitude;
            set => _magnitude = value;
        }

        public double Real
        {
            get => _real;
            set => _real = value;
        }

        public ComplexNumber(double real) {
            this._real = real;
            this._imaginary = 0;
            if (this._real == 0 && this._imaginary == 0)
            {
                this._magnitude = 0;
            }
            else 
            {
                this._magnitude = Math.Sqrt(Math.Pow(this._real, 2) + Math.Pow(this._imaginary, 2));
            }
        }

        public ComplexNumber(double real, double imaginary) {
            this._real = real;
            this._imaginary = imaginary;
            if (this._real == 0 && this._imaginary == 0)
            {
                this._magnitude = 0;
            }
            else
            {
                this._magnitude = Math.Sqrt(Math.Pow(this._real, 2) + Math.Pow(this._imaginary, 2));
            }
        }

        private void CalculateMagnitude() {
            if (this.Real == 0 && this.Imaginary == 0)
            {
                this.Magnitude = 0;
            }
            else 
            {
                this.Magnitude = Math.Sqrt(Math.Pow(this.Real, 2) + Math.Pow(this.Imaginary, 2));
            }
        }

        public bool Equals(ComplexNumber other)
        {
            return this.Real == other.Real && this.Imaginary == other.Imaginary;
        }

        public bool Equals(double other)
        {
            return this.Real == other && this.Imaginary == ZERO.Imaginary;
        }

        public static ComplexNumber Add(ComplexNumber a, ComplexNumber b) {
            return new ComplexNumber(a.Real + b.Real, a.Imaginary + b.Imaginary);
        }

        public static ComplexNumber Substract(ComplexNumber a, ComplexNumber b) {
            return new ComplexNumber(a.Real - b.Real, a.Imaginary - b.Imaginary);
        }

        public static ComplexNumber Divide(ComplexNumber divident, ComplexNumber divisor) {

            ComplexNumber customDivisor = new ComplexNumber(divisor.Real, (-1) * divisor.Imaginary);

            ComplexNumber topMultiplied = ComplexNumber.Multiply(divident, customDivisor);
            ComplexNumber bottomMultipied = ComplexNumber.Multiply(divisor, customDivisor);

            return new ComplexNumber(topMultiplied.Real / bottomMultipied.Real, topMultiplied.Imaginary / bottomMultipied.Real);

        }

        public static ComplexNumber operator + (ComplexNumber a, ComplexNumber b) {
            return ComplexNumber.Add(a, b);
        }

        public static ComplexNumber operator - (ComplexNumber a, ComplexNumber b)
        {
            return ComplexNumber.Substract(a, b);
        }

        public static ComplexNumber operator * (ComplexNumber a, ComplexNumber b)
        {
            return ComplexNumber.Multiply(a, b);
        }

        public static ComplexNumber operator / (ComplexNumber a, ComplexNumber b)
        {
            return ComplexNumber.Divide(a, b);
        }

        public static bool operator== (ComplexNumber a, ComplexNumber b)
        {
            return ComplexNumber.Equals(a, b);
        }

        public static bool operator!= (ComplexNumber a, ComplexNumber b)
        {
            return !ComplexNumber.Equals(a, b);
        }

        public static ComplexNumber operator -(ComplexNumber a) {
            return new ComplexNumber(-a.Real, -a.Imaginary);
        }


        public static ComplexNumber Multiply(ComplexNumber left, ComplexNumber right) {
            double r1 = left.Real * right.Real;
            double r2 = left.Real * right.Imaginary;
            double r3 = left.Imaginary * right.Real;
            double r4 = left.Imaginary * right.Imaginary;

            double resReal = r1 + (r4 * (-1));
            double resImag = r2 + r3;

            return new ComplexNumber(resReal, resImag);
        }

        public override string ToString()
        {
            return "[" + this.Real + ", " + this.Imaginary + "]";
        }

        public string ToString(ComplexNumberFormat format) {
            if (format == ComplexNumberFormat.AlgebraicForm)
            {
                return this.ToString();
            }
            else if (format == ComplexNumberFormat.GeometricForm)
            {
                return this.Real + " " + this.Imaginary + "i";
            }
            else
            {
                return
                    Math.Abs(this.Magnitude) +
                    " * (cos " +
                    RadianToDegree(Math.Asin(this.Imaginary / Math.Abs(this.Magnitude))) +
                    "° + sin " +
                    RadianToDegree(Math.Asin(this.Imaginary / Math.Abs(this.Magnitude))) +
                    "°i)";
            }
        }

        private static double RadianToDegree(double angle)
        {
            return angle * (180.0 / Math.PI);
        }

    }
}
