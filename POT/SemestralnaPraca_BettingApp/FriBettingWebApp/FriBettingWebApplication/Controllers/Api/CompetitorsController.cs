﻿using System.Web.Http;
using FriBettingWebApplication.Services.Face;

namespace FriBettingWebApplication.Controllers.Api
{
    public class CompetitorsController : ApiController
    {

        private ICompetitorService _competitorService;

        public CompetitorsController(ICompetitorService competitorService)
        {
            _competitorService = competitorService;
        }

        [HttpGet]
        [Route("api/competitors/list")]
        [AllowAnonymous]
        public IHttpActionResult GetAllCompetitorsCompetingInSport(int sportId)
        {
            var resp = _competitorService.GetCompetitorsCompetingAtSport(sportId);
            return Ok(resp);
        }

    }
}
