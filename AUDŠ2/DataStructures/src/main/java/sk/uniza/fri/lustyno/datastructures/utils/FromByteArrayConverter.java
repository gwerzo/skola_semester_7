package sk.uniza.fri.lustyno.datastructures.utils;

import sk.uniza.fri.lustyno.datastructures.exception.DataStructureOperationFailedException;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class FromByteArrayConverter {

    public static String stringFromBytes(byte[] bytes) {
        return new String(bytes, Charset.defaultCharset());
    }

    public static String stringFromBytes(byte[] bytes, Charset charset) {
        return new String(bytes, charset);
    }

    public static String stringFromBytes(byte[] bytes, String charset) throws UnsupportedEncodingException {
        return new String(bytes, charset);
    }

    public static Boolean booleanFromByte(byte fromByte) {
        return fromByte == 1;
    }

    public static Integer integerFromBytes(byte[] bytes) {
        if (bytes.length != 4) {
            throw new DataStructureOperationFailedException(FromByteArrayConverter.class.getName(), "integerFromBytes",
                    "Nemožno odparsovať INTEGER z bytov, pretože ich počet musí byť 4");
        }
        return ByteBuffer.wrap(bytes).getInt();
    }

    public static Long longFromBytes(byte[] bytes) {
        if (bytes.length != 8) {
            throw new DataStructureOperationFailedException(FromByteArrayConverter.class.getName(), "bytesToLong",
                    "Nemožno odparsovať LONG z bytov, pretože ich počet musí byť 8");
        }
        long result = 0;
        for (int i = 0; i < 8; i++) {
            result <<= 8;
            result |= (bytes[i] & 0xFF);
        }
        return result;
    }

    public static Double doubleFromBytes(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getDouble();
    }

    public static LocalDateTime localDateTimeFromBytes(byte[] bytes) {
        Long epochMilisDateTime = FromByteArrayConverter.longFromBytes(bytes);
        LocalDateTime date =
                LocalDateTime.ofInstant(Instant.ofEpochMilli(epochMilisDateTime), ZoneId.systemDefault());
        return date;
    }

}
