package sk.uniza.fri.lustiak.auds2.kataster.service;

import org.springframework.beans.factory.annotation.Autowired;
import sk.uniza.fri.lustiak.auds2.kataster.entities.bo.Parcel;
import org.springframework.stereotype.Service;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.*;

import sk.uniza.fri.lustiak.auds2.kataster.entities.wrapper.ParcelWrapper;
import sk.uniza.fri.lustiak.auds2.kataster.entities.wrapper.RealEstateWrapper;
import sk.uniza.fri.lustiak.auds2.kataster.utils.BusinessException;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo.Coordinate2D;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo.Coordinates2DKey;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo.LATITUDE_POSITION;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo.LONGITUDE_POSITION;
import sk.uniza.fri.lustyno.datastructures.kdtree.KDTree;
import sk.uniza.fri.lustyno.datastructures.kdtree.KDTreeNodeKeyDataWrapper;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

@Service
public class ParcelService {

    private final String VSETKY_PARCELY_FILE           = "fileResources/vsetky_parcely.csv";

    @Autowired
    private EntityMapperService entityMapperService;

    @Autowired
    private RealEstateService realEstateService;

    private KDTree<Coordinates2DKey, Parcel, Coordinate2D> parcels = new KDTree<>(2);

    public OperationMarker addNewParcel(CreateNewParcelUi createNewParcelUi) {
        ParcelWrapper parcelWrapper =
                this.entityMapperService.fromCreateNewParcelUi(createNewParcelUi);

        CoordinatesUi newRealEstateCoordinates = new CoordinatesUi(
                createNewParcelUi.getLatitude(), createNewParcelUi.getLongitude(),
                createNewParcelUi.getLatitudeOrientation(), createNewParcelUi.getLongitudeOrientation());

        // Nájdenie všetkých parciel na coords novovytváranej nehnutelnosti a každej takto nájdenej nehnutelnosti
        // pridaná referencia na novovytváranú nehnutelnosti
        List<RealEstateWrapper> realEstatesAtNewParcelCoords = this.realEstateService.getRealEstatesAtCoordinates(newRealEstateCoordinates);
        realEstatesAtNewParcelCoords.forEach(e -> {
            e.getRealEstate().pridajParcelu(parcelWrapper);
        });
        // Novovytváranej entite nehnutelnosti jednoducho pridat vyssie najdeny zoznam do referencii parciel
        // na ktorých nehnutelnost stojí - nie "add", pretoze je to uplne nova entita
        parcelWrapper.getParcel().setNehnutelnosti(realEstatesAtNewParcelCoords);

        this.parcels.insert(parcelWrapper.getCoordinates2DKey(), parcelWrapper.getParcel());
        return OperationMarker.OPERATION_SUCCESFUL;
    }

    public List<ParcelUiTableEntity> getParcelListAttCoordinates(CoordinatesUi coordinatesUi) {
        Coordinates2DKey findByKey = this.entityMapperService.toCoordinatesKey(coordinatesUi);

        List<KDTreeNodeKeyDataWrapper<Coordinates2DKey, Parcel>> parcelsAtCoordinates =
                this.parcels.findByDimensionalKey(findByKey);

        List<ParcelUiTableEntity> parcelsFound = new LinkedList<>();
        parcelsAtCoordinates.forEach(e -> {
            parcelsFound.add(this.entityMapperService.fromParcelNodeWrapper(e));
        });

        return parcelsFound;
    }

    protected List<ParcelWrapper> getParcelsAtCoordinates(CoordinatesUi coordinatesUi) {
        Coordinates2DKey findByKey = this.entityMapperService.toCoordinatesKey(coordinatesUi);

        List<KDTreeNodeKeyDataWrapper<Coordinates2DKey, Parcel>> parcelsAtCoordinates =
                this.parcels.findByDimensionalKey(findByKey);

        List<ParcelWrapper> parcelsFound = new LinkedList<>();
        parcelsAtCoordinates.forEach(e -> {
            parcelsFound.add(new ParcelWrapper(e.getKey(),e.getData()));
        });
        return parcelsFound;
    }

    public List<ParcelUiTableEntity> getParcelsBetweenCoordinates(CoordinatesUi fromCoordinates, CoordinatesUi toCoordinates) {
        Coordinates2DKey findByKeyFrom = this.entityMapperService.toCoordinatesKey(fromCoordinates);
        Coordinates2DKey findByKeyTo = this.entityMapperService.toCoordinatesKey(toCoordinates);

        List<KDTreeNodeKeyDataWrapper<Coordinates2DKey, Parcel>> parcelsAtCoordinates =
                this.parcels.findInRange(findByKeyFrom, findByKeyTo);

        List<ParcelUiTableEntity> parcelsFound = new LinkedList<>();
        parcelsAtCoordinates.forEach(e -> {
            parcelsFound.add(this.entityMapperService.fromParcelNodeWrapper(e));
        });

        return parcelsFound;
    }

    public OperationMarker editParcel(EditParcelUi editParcelUi) {
        Coordinates2DKey oldEntityKey = new Coordinates2DKey(
                editParcelUi.getOldParcelData().getLatitude(),
                editParcelUi.getOldParcelData().getLongtitude(),
                editParcelUi.getOldParcelData().getLatitudeOrientation().equals("SEVER") ? LATITUDE_POSITION.NORTH : LATITUDE_POSITION.SOUTH,
                editParcelUi.getOldParcelData().getLongitudeOrientation().equals("VÝCHOD") ? LONGITUDE_POSITION.EAST : LONGITUDE_POSITION.EAST,
                editParcelUi.getOldParcelData().getInternalId()
        );
        // V prípade, že editujem aj pozíciu parcely
        if (editParcelUi.getEditConstraints().equals(Boolean.TRUE)) {
            // Vymažem starú nehnuteľnosť
            KDTreeNodeKeyDataWrapper<Coordinates2DKey, Parcel> deletedParcel =
                    this.parcels.delete(oldEntityKey);
            // Nehnuteľnostiam, ktoré patrili pod túto parcelu odoberiem danú parcelu
            deletedParcel.getData().getNehnutelnosti().stream().forEach(e -> {
                e.getRealEstate().odoberParcelu(new ParcelWrapper(oldEntityKey, deletedParcel.getData()));
            });
            this.addNewParcel(this.entityMapperService.fromParcelTableUiEntity(editParcelUi.getNewParcelData()));
        }
        // V prípade že sa editujú iba voliteľné fieldy
        else {
            KDTreeNodeKeyDataWrapper<Coordinates2DKey, Parcel> entity = this.parcels.find(oldEntityKey);
            entity.getData().setCisloParcely(editParcelUi.getNewParcelData().getCisloParcely());
            entity.getData().setPopis(editParcelUi.getNewParcelData().getPopisParcely());
        }
        return OperationMarker.OPERATION_SUCCESFUL;
    }

    public OperationMarker removeParcel(ParcelUiTableEntity parcelUiTableEntity) {
        Coordinates2DKey oldEntityKey = new Coordinates2DKey(
                parcelUiTableEntity.getLatitude(),
                parcelUiTableEntity.getLongtitude(),
                parcelUiTableEntity.getLatitudeOrientation().equals("SEVER") ? LATITUDE_POSITION.NORTH : LATITUDE_POSITION.SOUTH,
                parcelUiTableEntity.getLongitudeOrientation().equals("VÝCHOD") ? LONGITUDE_POSITION.EAST : LONGITUDE_POSITION.EAST,
                parcelUiTableEntity.getInternalId()
        );
        KDTreeNodeKeyDataWrapper<Coordinates2DKey, Parcel> deletedParcel = this.parcels.delete(oldEntityKey);
        deletedParcel.getData().getNehnutelnosti().forEach(e -> {
            e.getRealEstate().odoberParcelu(new ParcelWrapper(deletedParcel.getKey(), deletedParcel.getData()));
        });
        return OperationMarker.OPERATION_SUCCESFUL;
    }

    public void clearStoredItems() {
        this.parcels = new KDTree<>(2);
    }

    public void vycistiSubor() {
        try {
            PrintWriter pw = new PrintWriter(VSETKY_PARCELY_FILE);
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void ulozVsetkyParcely() {
        List<KDTreeNodeKeyDataWrapper<Coordinates2DKey, Parcel>> levelOrderOfData = this.parcels.listOfDataByLevelOrder();

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(VSETKY_PARCELY_FILE));

            for (KDTreeNodeKeyDataWrapper<Coordinates2DKey, Parcel> parcelNode : levelOrderOfData) {
                StringBuilder sb = new StringBuilder();

                sb.append(parcelNode.getKey().getValue().getLatitudeValue() + ",");
                sb.append(parcelNode.getKey().getValue().getLongitudeValue() + ",");
                sb.append(parcelNode.getData().getCisloParcely() + ",");
                sb.append(parcelNode.getData().getPopis());

                bw.write(sb.toString());
                bw.newLine();
            }

        } catch (IOException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na zápis parciel");
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        }

    }

    public void nacitajParcelyZoSuboru() {
        this.clearStoredItems();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(VSETKY_PARCELY_FILE));

            String line = null;
            while ((line = br.readLine()) != null) {

                String[] splitted = line.split(",", -1);

                Double latitude  = Double.valueOf(splitted[0]);
                Double longitude = Double.valueOf(splitted[1]);
                Integer cisloParcely = Integer.parseInt(splitted[2]);
                String popis = splitted[3];

                CreateNewParcelUi createNewParcelUi = new CreateNewParcelUi();
                createNewParcelUi.setCisloParcely(cisloParcely);
                createNewParcelUi.setPopisParcely(popis);
                createNewParcelUi.setLatitude(latitude);
                createNewParcelUi.setLongitude(longitude);
                createNewParcelUi.setLatitudeOrientation(latitude >= 0d ? Boolean.TRUE : Boolean.FALSE);
                createNewParcelUi.setLongitudeOrientation(longitude >= 0d ? Boolean.TRUE : Boolean.FALSE);
                this.addNewParcel(createNewParcelUi);
            }
        } catch (IOException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o nehnuteľnostiach");
        }
    }

    public void vypisParcelyNaKonzolu() {
        System.out.println("Nehnuteľnosti\n");
        this.parcels.listOfDataByLevelOrder().forEach(e -> {
            System.out.println("\t-Parcela: #" + e.getKey().getValue().getUniqueIdentifier() +
                    ", LAT: " + e.getKey().getValue().getLatitudeValue() +
                    ", LON: " + e.getKey().getValue().getLongitudeValue());
        });
    }

}
