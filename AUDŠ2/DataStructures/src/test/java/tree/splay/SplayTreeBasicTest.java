package tree.splay;

import org.junit.jupiter.api.*;
import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.entity.keys.StringKey;
import sk.uniza.fri.lustyno.datastructures.tree.bst.BinarySearchTreeNode;
import sk.uniza.fri.lustyno.datastructures.tree.splay.SplayTree;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SplayTreeBasicTest {


    Integer[] deterministicValues = new Integer[] {50, 60, 40, 66, 21, 74, 100, 30, 29, 59, 61, 55};
    private List<Integer> deterministicValuesList = Arrays.asList(deterministicValues);

    private SplayTree<IntegerKey, String> st = new SplayTree<>();

    /**
     * Otestuje, či vkladanie nodu spôsobí,
     * že vložený node bude vyrotovaný
     * až do koreňa stromu
     */
    @Test
    @Order(1)
    public void test_DETERMINISTIC_INSERTION() {
        deterministicValuesList.forEach(e -> {
            st.insert(new IntegerKey(e), "NODE");
            Assertions.assertTrue(st.getRoot().getKey().getValue().equals(e));
        });
    }

    /**
     * Test, ktorý overí, či sa v strome
     * nevyskytli nejaké cykly a overí aj to,
     * či inorder prehliadkou získa
     * neklesajúcu postupnosť kľúčov
     */
    @Test
    @Order(2)
    public void test_CHECK_INORDER_FOR_LOOPS() {
        List<BinarySearchTreeNode<IntegerKey, String>> inOrder = st.inOrder();

        int lowest = Integer.MIN_VALUE;
        for (BinarySearchTreeNode<IntegerKey, String> node : inOrder) {
            if (node.getKey().getValue() > lowest) {
                lowest = node.getKey().getValue();
            } else {
                fail("INORDER prehliadka nie je v správnom poradí");
            }
        }
    }

    /**
     * Vymaže zo stromu list a overí,
     * či sa otec vymazávaného listu
     * správne vyrotoval až do vrchola
     */
    @Test
    @Order(3)
    public void test_DELETING_WITH_PARENT_TO_ROOT_SPLAY() {
        st.delete(new IntegerKey(60));
        Assertions.assertTrue(st.getRoot().getKey().getValue().equals(59));
        st.delete(new IntegerKey(100));
        Assertions.assertTrue(st.getRoot().getKey().getValue().equals(61));
    }

}
