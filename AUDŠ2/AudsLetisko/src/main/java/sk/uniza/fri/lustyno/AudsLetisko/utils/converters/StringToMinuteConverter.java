package sk.uniza.fri.lustyno.AudsLetisko.utils.converters;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

public class StringToMinuteConverter implements Converter<String, Integer> {

    private static final long serialVersionUID = 1L;

    @Override
    public Result<Integer> convertToModel(String s, ValueContext valueContext) {
        Integer i;
        try {
            i = new Integer(s);
            if (i < 0 || i > 59) {
                return Result.error("Minúta môže byť len v rozpätí 0 - 59");
            }
        } catch (Exception e) {
            return Result.error("Nesprávny formát čísla");
        }
        return Result.ok(i);
    }

    @Override
    public String convertToPresentation(Integer integer, ValueContext valueContext) {
        if (integer == null) {
            return "";
        }
        return integer.toString();
    }
}
