package queue.pairingHeap;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.*;
import sk.uniza.fri.lustyno.datastructures.entity.keys.IntegerKey;
import sk.uniza.fri.lustyno.datastructures.queue.PairingHeap;
import sk.uniza.fri.lustyno.datastructures.queue.QueueNode;

import java.util.*;

import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PairingHeapBasicTest {

    // Povolenie logovania, že ktoré operácie sa vykonávajú
    private boolean loggingAllowed = false;

    // Povolenie vykonania levelOrder prehliadky pri každej operácii na skontrolovanie veľkosti
    // Odhalí či sa napr. neodsekávajú niektoré nody po operácii
    // Spraví ale veľa na performance
    private boolean strictChecking = false;

    private final Integer NUMBER_OF_ITEMS = 1000000;

    private PairingHeap<IntegerKey, String> ph;
    private List<Integer> testPriorities;


    private enum OPERATION {
        OPERATION_MIN,
        OPERATION_INCREASE_KEY,
        OPERATION_DECREASE_KEY,
        OPERATION_DELETE_MIN,
        OPERATION_DELETE,
        OPERATION_INSERT
    }

    @BeforeAll
    public void init() {
         this.ph = new PairingHeap<>();
         this.testPriorities = new LinkedList<>();

         randomizePriorities();
    }

    @Test
    @Order(1)
    public void testInsertionValues() {

        ph.insert(new IntegerKey(3), "HEAP_DATA");
        ph.insert(new IntegerKey(5), "HEAP_DATA");
        ph.insert(new IntegerKey(4), "HEAP_DATA");
        ph.insert(new IntegerKey(10), "HEAP_DATA");
        ph.insert(new IntegerKey(2), "HEAP_DATA");

        QueueNode<IntegerKey, String> actualNode = ph.min();
        Assertions.assertEquals(2, actualNode.getKey().getValue());
        actualNode = actualNode.getLeftChild();
        Assertions.assertEquals(3, actualNode.getKey().getValue());
        actualNode = actualNode.getLeftChild();
        Assertions.assertEquals(10, actualNode.getKey().getValue());
        actualNode = actualNode.getRightChild();
        Assertions.assertEquals(4, actualNode.getKey().getValue());
        actualNode = actualNode.getRightChild();
        Assertions.assertEquals(5, actualNode.getKey().getValue());

    }

    @Test
    @Order(2)
    public void testDeterministicDeletionOfAllNodes_BY_MIN_VALUE() {
        QueueNode<IntegerKey, String> deleted = ph.deleteMin();
        Assertions.assertEquals(3, ph.min().getKey().getValue());
        deleted = ph.deleteMin();
        Assertions.assertEquals(4, ph.min().getKey().getValue());
        deleted = ph.deleteMin();
        Assertions.assertEquals(5, ph.min().getKey().getValue());
        deleted = ph.deleteMin();
        Assertions.assertEquals(10, ph.min().getKey().getValue());
        deleted = ph.deleteMin();
        Assertions.assertEquals(ph.min(), null);

        Assertions.assertEquals(0, ph.getSize());
    }

    /**
     * Vyplní PairingHeap s {@link #testPriorities},
     * t.j. pridá do haldy všetky nody s prioritami
     * z {@link #testPriorities} - čo je zoznam
     * priorít vygenerovaných tak, aby sa opakovali
     * s pravdepodobnosťou 5% (random z 20)
     */
    @Test
    @Order(3)
    public void testInsertManyNodesWithRandomPriorities() {
        this.testPriorities.forEach(e -> {
            this.ph.insert(new IntegerKey(e), "BIG_DATA");
        });
    }

    /**
     * Vymaže všetky nody z PairingHeapu
     * a overí, či boli zoradené správne
     * po každom kroku deleteMin,
     * t.j. či sa novovymazávaná priorita rovná,
     * alebo je nižšia (vyššie číslo) ako posledne odobraná
     */
    @Test
    @Order(4)
    public void testDeletionOfManyNodesByMinValue() {
        int lastHighestPriority = Integer.MIN_VALUE;
        int size = NUMBER_OF_ITEMS;

        while (ph.min() != null) {

            //List<QueueNode<IntegerKey, String>> queueNodes = ph.levelOrder();

            Assertions.assertEquals(size, ph.getSize());
            //Assertions.assertEquals(ph.getSize(), queueNodes.size());
            QueueNode<IntegerKey, String> deleted = ph.deleteMin();
            size--;

            // Assert či je posledná vyhadzovaná priorita vyššia alebo rovná ako aktuálne vyhadzovaná
            if (deleted != null) {
                Assertions.assertTrue(deleted.getKey().getValue().compareTo(lastHighestPriority) >= 0);
                lastHighestPriority = deleted.getKey().getValue();
            }
        }
    }

    /**
     * Vyplnenie heapu {@link #NUMBER_OF_ITEMS} prvkami
     * ktoré zaručia warmup fázu, pričom
     * 1% z týchto nodov sa odloží pre ďalšie operácie
     * (delete, increasePriority, decreasePriority)
     *
     */
    @Test
    @Order(5)
    public void testRandomizedOperations() {

        Random rnd = new Random();
        ArrayList<Integer> randoms = new ArrayList<>(NUMBER_OF_ITEMS);
        Stack<QueueNode<IntegerKey, String>> operatedNodes = new Stack<>();

        int actualSize = NUMBER_OF_ITEMS;

        for (int i = 0; i < NUMBER_OF_ITEMS; i++) {
            int rndPriority = rnd.nextInt(100);
            randoms.add(i, rndPriority);
            int rndOnePercent = rnd.nextInt(100);
            QueueNode<IntegerKey, String> nodeToAdd = new QueueNode<>(new IntegerKey(rndPriority), RandomStringUtils.random(10));
            if (rndPriority == rndOnePercent) {
                operatedNodes.add(nodeToAdd);
            }
            this.ph.insert(nodeToAdd);
        }
        Assertions.assertEquals(NUMBER_OF_ITEMS, this.ph.getSize());

        while (operatedNodes.size() > 0) {
            int rndOp = rnd.nextInt(10);
            if (rndOp < 2) {
                // S pravdepodobnosťou 20% vymaž node
                if (loggingAllowed)
                    logOperation(OPERATION.OPERATION_DELETE);
                this.ph.delete(operatedNodes.pop());
                actualSize--;
                Assertions.assertEquals(actualSize, this.ph.getSize());
                if (strictChecking)
                    Assertions.assertEquals(actualSize, this.ph.levelOrder().size());
            }
            // S pravdepodobnosťou 10% do heapu pridaj nový prvok
            else if (rndOp < 3) {
                if (loggingAllowed)
                    logOperation(OPERATION.OPERATION_INSERT);
                this.ph.insert(new IntegerKey(rnd.nextInt(50)), "NEW_DATA");
                actualSize++;
                Assertions.assertEquals(actualSize, this.ph.getSize());
                if (strictChecking)
                    Assertions.assertEquals(actualSize, this.ph.levelOrder().size());
            }
            // S pravdepodobnosťou 40% vymaž z haldy minimum (aj z operatedNodes ak sa tam nachádza)
            else if (rndOp < 7) {
                if (loggingAllowed)
                    logOperation(OPERATION.OPERATION_DELETE_MIN);
                QueueNode<IntegerKey, String> deleted = this.ph.deleteMin();
                operatedNodes.remove(deleted);

                actualSize--;
                Assertions.assertEquals(actualSize, this.ph.getSize());
                if (strictChecking)
                    Assertions.assertEquals(actualSize, this.ph.levelOrder().size());
            }
            // S pravdepodobnosťou 10% zvýš nodu prioritu (50% o random hodnotu, 50% na max prioritu)
            else if (rndOp < 8) {
                if (loggingAllowed)
                    logOperation(OPERATION.OPERATION_DECREASE_KEY);
                boolean randomizeKey = rnd.nextBoolean();
                int prio = operatedNodes.peek().getKey().getValue();
                if (randomizeKey) {
                    // Kvôli pretekaniu z mínusu do plusu
                    if (prio < 0) {
                        continue;
                    }
                    prio = prio - rnd.nextInt(10);
                } else {
                    prio = Integer.MIN_VALUE;
                }
                ph.changePriority(operatedNodes.peek(), new IntegerKey(prio));
                Assertions.assertEquals(actualSize, this.ph.getSize());
                if (strictChecking)
                    Assertions.assertEquals(actualSize, this.ph.levelOrder().size());

                // Ak som nastavoval na minimum hodnotu kľúča, over či je v roote
                if (!randomizeKey) {
                    Assertions.assertEquals(operatedNodes.peek(), this.ph.min());
                }
            }
            // S pravdepodobnosťou 10% zníž nodu prioritu o random z 10
            else if (rndOp < 9) {
                if (loggingAllowed)
                    logOperation(OPERATION.OPERATION_INCREASE_KEY);
                int prio = operatedNodes.peek().getKey().getValue() + rnd.nextInt(10);
                ph.changePriority(operatedNodes.peek(), new IntegerKey(prio));
                Assertions.assertEquals(actualSize, this.ph.getSize());
                if (strictChecking)
                    Assertions.assertEquals(actualSize, this.ph.levelOrder().size());
            }
            // S pravdepodobnosťou 10% len nakukni na minimum
            else if (rndOp < 10) {
                if (loggingAllowed)
                    logOperation(OPERATION.OPERATION_MIN);
                this.ph.min();
            }
            else {
                fail();
            }
        }
    }

    // --------------------------INITIALIZATION---------------------------------------------------
    private void randomizePriorities() {
        Random rnd = new Random();
        for (int i = 0; i < this.NUMBER_OF_ITEMS; i++) {
            testPriorities.add(rnd.nextInt(50));
        }

    }

    private void logOperation(OPERATION operation) {
        switch (operation) {
            case OPERATION_MIN:
                System.out.println("Operácia nazretia na najprioritnejší");
                break;
            case OPERATION_INCREASE_KEY:
                System.out.println("Operácia zníženia priority");
                break;
            case OPERATION_DECREASE_KEY:
                System.out.println("Operácia zvýšenia priority");
                break;
            case OPERATION_DELETE_MIN:
                System.out.println("Operácia vymazania minima");
                break;
            case OPERATION_DELETE:
                System.out.println("Operácia vymazania");
                break;
            case OPERATION_INSERT:
                System.out.println("Operácia pridania nodu");
                break;
            default:
                break;
        }
    }

}
