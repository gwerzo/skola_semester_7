package sk.uniza.fri.lustyno.fripd.components.vehicles;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.*;
import sk.uniza.fri.lustyno.fripd.components.common.BusinessComponent;
import sk.uniza.fri.lustyno.fripd.components.common.MENU_TITLE;
import sk.uniza.fri.lustyno.fripd.components.common.RepaintableComponent;
import sk.uniza.fri.lustyno.fripd.entities_ui.vehicle.PridanieVozidlaEntityUi;
import sk.uniza.fri.lustyno.fripd.service.UIServices;
import sk.uniza.fri.lustyno.fripd.service.VehicleService;
import sk.uniza.fri.lustyno.fripd.utils.CustomNotification;
import sk.uniza.fri.lustyno.fripd.utils.HandledOperationExecutor;
import sk.uniza.fri.lustyno.fripd.utils.OperationMarker;
import sk.uniza.fri.lustyno.fripd.utils.converters.StringToIntegerConverter;

public class PridanieNovehoVozidlaForm extends BusinessComponent {

    private VehicleService vehicleService;

    private Binder<PridanieVozidlaEntityUi> binder;
    private PridanieVozidlaEntityUi entity;

    public PridanieNovehoVozidlaForm(RepaintableComponent parentComponent) {
        super(parentComponent, MENU_TITLE.VEHICLES_MENU_1.getTitle());
        this.vehicleService = UIServices.getVehicleService();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new PridanieVozidlaEntityUi();
        this.binder = new Binder<>(PridanieVozidlaEntityUi.class);

        VerticalLayout layout = new VerticalLayout();

        FormLayout formLayout = new FormLayout();

        TextField vinKodTf = new TextField("VIN");
        TextField evcKodTf = new TextField("EVČ");
        CheckBox vPatraniChB = new CheckBox("V hľadaní");
        DateField platnostTKTf = new DateField("Platnosť TK");
        DateField platnostEKTf = new DateField("Platnosť EK");
        TextField prevadzkovaHmotnostTf = new TextField("Prevadzkova hmotnosť");
        TextField pocetNapravTf = new TextField("Počet náprav");

        Button vytvorit = new Button("Zaevidovať vozidlo");

        vytvorit.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                zaevidujVozidlo(this.entity);
            } catch (ValidationException ex) {
                ex.printStackTrace();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        formLayout.addComponent(vinKodTf);
        formLayout.addComponent(evcKodTf);
        formLayout.addComponent(vPatraniChB);
        formLayout.addComponent(platnostTKTf);
        formLayout.addComponent(platnostEKTf);
        formLayout.addComponent(prevadzkovaHmotnostTf);
        formLayout.addComponent(pocetNapravTf);
        formLayout.addComponent(vytvorit);

        layout.addComponent(formLayout);

        binder.forField(vinKodTf)
                .asRequired()
                .withValidator(new StringLengthValidator("Dĺžka VIN kódu musí byť v rozmedzí 1 - 17", 1, 17))
                .bind(PridanieVozidlaEntityUi::getVIN, PridanieVozidlaEntityUi::setVIN);
        binder.forField(evcKodTf)
                .asRequired()
                .withValidator(new StringLengthValidator("Dĺžka EVČ kódu musí byť v rozmedzí 1 - 7", 1, 7))
                .bind(PridanieVozidlaEntityUi::getEVC, PridanieVozidlaEntityUi::setEVC);
        binder.forField(vPatraniChB)
                .bind(PridanieVozidlaEntityUi::getVPatrani, PridanieVozidlaEntityUi::setVPatrani);
        binder.forField(platnostTKTf)
                .asRequired()
                .bind(PridanieVozidlaEntityUi::getDatumPlatnostiSTK, PridanieVozidlaEntityUi::setDatumPlatnostiSTK);
        binder.forField(platnostEKTf)
                .asRequired()
                .bind(PridanieVozidlaEntityUi::getDatumPlatnostiEK, PridanieVozidlaEntityUi::setDatumPlatnostiEK);
        binder.forField(pocetNapravTf)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(PridanieVozidlaEntityUi::getPocetNaprav, PridanieVozidlaEntityUi::setPocetNaprav);
        binder.forField(prevadzkovaHmotnostTf)
                .asRequired()
                .withConverter(new StringToIntegerConverter())
                .bind(PridanieVozidlaEntityUi::getPrevadzkovaHmostnost, PridanieVozidlaEntityUi::setPrevadzkovaHmostnost);

        this.parenComponent.showComponent(new Panel("Pridanie nového vozidla", layout));
    }

    private void zaevidujVozidlo(PridanieVozidlaEntityUi entity) {

        HandledOperationExecutor.submit(() -> {
            this.vehicleService.addNewVehicle(entity);
            CustomNotification.notification("Vozidlo bolo zaevidované");
            this.entity = new PridanieVozidlaEntityUi();
            binder.readBean(this.entity);
            return OperationMarker.OPERATION_SUCCESFUL;
        });
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.VEHICLES_MENU_1.getTitle();
    }
}
