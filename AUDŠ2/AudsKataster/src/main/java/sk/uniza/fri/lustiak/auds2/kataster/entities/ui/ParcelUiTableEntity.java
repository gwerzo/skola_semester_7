package sk.uniza.fri.lustiak.auds2.kataster.entities.ui;

import lombok.Data;

import java.util.List;

@Data
public class ParcelUiTableEntity {

    private int internalId;
    private int cisloParcely;
    private String popisParcely;
    private Double latitude;
    private Double longtitude;
    private String latitudeOrientation;
    private String longitudeOrientation;
    private List<RealEstateUiTableEntity> nehnutelnosti;

}
