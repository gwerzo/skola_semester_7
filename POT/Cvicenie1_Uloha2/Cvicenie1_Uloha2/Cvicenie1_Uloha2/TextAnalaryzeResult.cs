﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cvicenie1_Uloha2
{
    class TextAnalaryzeResult
    {

        private int numberOfCharacters;

        private int numberOfSpaces;

        private int numberOfCharactersWithoutSpaces;

        private int numberOfVowels;

        private int numberOfConsonants;

        private int numberOfWords;

        private int numberOfUniqueWord;

        private int numberOfSentences;

        private Dictionary<string, int> characterFrequencies;

        private Dictionary<string, int> wordFrequencies;

        private bool withWf;

        private bool withCf;

        public int NumberOfCharacters { get => numberOfCharacters; set => numberOfCharacters = value; }
        public int NumberOfCharactersWithoutSpaces { get => numberOfCharactersWithoutSpaces; set => numberOfCharactersWithoutSpaces = value; }
        public int NumberOfConsonants { get => numberOfConsonants; set => numberOfConsonants = value; }
        public int NumberOfWords { get => numberOfWords; set => numberOfWords = value; }
        public int NumberOfUniqueWord { get => numberOfUniqueWord; set => numberOfUniqueWord = value; }
        public int NumberOfSentences { get => numberOfSentences; set => numberOfSentences = value; }
        public int NumberOfVowels { get => numberOfVowels; set => numberOfVowels = value; }
        public int NumberOfSpaces { get => numberOfSpaces; set => numberOfSpaces = value; }

        public void IncrementFrequencyOfCharacter(string s) {
            if (s.Length == 0) {
                return;
            }
            if (this.characterFrequencies.ContainsKey(s))
            {
                this.characterFrequencies[s] += 1;
            }
            else
            {
                this.characterFrequencies.Add(s, 1);
            }
        }

        public void IncrementFrequencyOfWord(string word) {
            if (this.wordFrequencies.ContainsKey(word))
            {
                this.wordFrequencies[word] += 1;
            }
            else 
            {
                this.wordFrequencies.Add(word, 1);
            }
            
        }

        public TextAnalaryzeResult(bool withWf, bool withCf) { 
            this.characterFrequencies = new Dictionary<string, int>();
            this.wordFrequencies = new Dictionary<string, int>();

            this.withWf = withWf;
            this.withCf = withCf;
        }

        public void PrintResult() {
            Console.WriteLine("Number of characters (with spaces): " + this.numberOfCharacters);
            Console.WriteLine("Number of characters (no spaces): " + (this.NumberOfCharacters - this.numberOfSpaces));
            Console.WriteLine("Number of vowels: " + this.numberOfVowels);
            Console.WriteLine("Number of consonants: " + this.numberOfConsonants);
            Console.WriteLine("Number of words: " + this.numberOfWords);
            Console.WriteLine("Number of unique words: " + this.wordFrequencies.Keys.Count);
            Console.WriteLine("Number of sentences: " + this.NumberOfSentences);
            Console.WriteLine("Average sentence length (words): " + (double)this.numberOfWords / (double)this.numberOfSentences);

            if (this.withCf)
            {
                Console.WriteLine("\nCharacter frequencies:");

                var sorted = from entry in this.characterFrequencies orderby entry.Value descending select entry;
                foreach (KeyValuePair<string, int> character in sorted)
                {
                    Console.WriteLine(character.Key + ": " + character.Value + "x");
                }
            }

            if (this.withWf) {
                Console.WriteLine("\nWord frequencies:");

                var sorted = from entry in this.wordFrequencies orderby entry.Value descending select entry;

                foreach (KeyValuePair<string, int> word in sorted) {
                    Console.WriteLine(word.Key + ": " + word.Value + "x");
                }
            }
        }
    }
}
