package sk.uniza.fri.lustyno.AudsLetisko.entities_ui;

import lombok.Data;

import java.util.Random;

@Data
public class PriletLietadlaUiEntity {

    private static Random rnd = new Random();

    private String nazovVyrobcu;

    private String medzinarodnyKod;

    private Integer minimalnaDlzkaOdletovejDrahy;

    private Integer priorita;

    public void flush() {
        this.nazovVyrobcu = "";
        this.medzinarodnyKod = "";
        this.minimalnaDlzkaOdletovejDrahy = null;
        this.priorita = null;
    }

    public static PriletLietadlaUiEntity getRandomPriletLietadla() {
        PriletLietadlaUiEntity priletLietadlaUiEntity = new PriletLietadlaUiEntity();

        priletLietadlaUiEntity.setMedzinarodnyKod(randomMedzinarodnyKod());
        priletLietadlaUiEntity.setNazovVyrobcu(randomVyrobca());
        priletLietadlaUiEntity.setMinimalnaDlzkaOdletovejDrahy(randomDlzkaDrahy());
        priletLietadlaUiEntity.setPriorita(rnd.nextInt(10));

        return priletLietadlaUiEntity;
    }

    private static Integer randomDlzkaDrahy() {
        Integer[] dlzky = {1500, 2000, 2500, 3000, 3500};
        return dlzky[rnd.nextInt(dlzky.length)];
    }

    private static String randomVyrobca() {
        String[] manufacturers = {"Airbus", "Boeing", "Raytheon", "Lockheed Martin", "United Technologies"};
        return manufacturers[rnd.nextInt(manufacturers.length)];
    }

    private static String randomMedzinarodnyKod() {
        String[] chars = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N"};

        StringBuilder sb = new StringBuilder();
        sb.append(chars[rnd.nextInt(chars.length)]);
        sb.append(chars[rnd.nextInt(chars.length )]);
        sb.append("-");
        sb.append(rnd.nextInt(10));
        sb.append(rnd.nextInt(10));
        sb.append(rnd.nextInt(10));
        sb.append(rnd.nextInt(10));
        sb.append(rnd.nextInt(10));
        sb.append(rnd.nextInt(10));
        sb.append(rnd.nextInt(10));
        sb.append(rnd.nextInt(10));
        sb.append(rnd.nextInt(10));
        sb.append(rnd.nextInt(10));
        sb.append(rnd.nextInt(10));
        sb.append(rnd.nextInt(10));
        sb.append(rnd.nextInt(10));
        sb.append("-");
        sb.append(chars[rnd.nextInt(chars.length)]);
        sb.append(chars[rnd.nextInt(chars.length)]);

        return sb.toString();
    }

}

