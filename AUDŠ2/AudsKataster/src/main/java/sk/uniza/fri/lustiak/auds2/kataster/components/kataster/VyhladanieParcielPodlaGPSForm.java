package sk.uniza.fri.lustiak.auds2.kataster.components.kataster;


import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.BusinessComponent;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.MENU_TITLE;
import sk.uniza.fri.lustiak.auds2.kataster.components.common.RepaintableComponent;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.CoordinatesUi;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.ParcelUiTableEntity;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.RealEstateUiTableEntity;
import sk.uniza.fri.lustiak.auds2.kataster.service.ParcelService;
import sk.uniza.fri.lustiak.auds2.kataster.service.UIServices;
import sk.uniza.fri.lustiak.auds2.kataster.utils.CustomNotification;
import sk.uniza.fri.lustiak.auds2.kataster.utils.HandledOperationExecutor;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;

import java.util.LinkedList;


public class VyhladanieParcielPodlaGPSForm extends BusinessComponent {

    private Binder<CoordinatesUi> binder;
    private CoordinatesUi entity;

    private ParcelService parcelService;

    private Grid<ParcelUiTableEntity> parcelGrid;

    public VyhladanieParcielPodlaGPSForm(RepaintableComponent component) {
        super(component, MENU_TITLE.MENU_2.getTitle());
        this.parcelService = UIServices.getParcelService();
    }

    @Override
    public void hideComponent() {

    }

    @Override
    public void showComponent() {
        this.entity = new CoordinatesUi();
        this.binder = new Binder<>(CoordinatesUi.class);
        this.parcelGrid = new Grid<>();
        this.parcelGrid.setWidth("100%");

        this.parcelGrid.addColumn(ParcelUiTableEntity::getInternalId).setCaption("#");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getCisloParcely).setCaption("Číslo parcely");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getPopisParcely).setCaption("Popis");
        this.parcelGrid.addComponentColumn(this::buildNehnutelnostiButton).setCaption("Nehnuteľnosti");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getLatitude).setCaption("Zem.výška");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getLongtitude).setCaption("Zem.šírka");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getLatitudeOrientation).setCaption("Orientácia výšky");
        this.parcelGrid.addColumn(ParcelUiTableEntity::getLongitudeOrientation).setCaption("Orientácia šírky");


        VerticalLayout layout = new VerticalLayout();

        FormLayout formLayout = new FormLayout();

        HorizontalLayout latLayout = new HorizontalLayout();
        TextField latitudeTF = new TextField("Zemepisná výška");
        RadioButtonGroup<String> latitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej výšky");
        latitudeOrientation.setItems("SEVER", "JUH");
        latitudeOrientation.setSelectedItem("SEVER");
        latLayout.addComponents(latitudeTF, latitudeOrientation);

        HorizontalLayout longLayout = new HorizontalLayout();
        TextField longitudeTF = new TextField("Zemepisná šírka");
        RadioButtonGroup<String> longitudeOrientation = new RadioButtonGroup<>("Orientácia zemepisnej šírky");
        longitudeOrientation.setItems("VÝCHOD", "ZÁPAD");
        longitudeOrientation.setSelectedItem("VÝCHOD");
        longLayout.addComponents(longitudeTF, longitudeOrientation);

        Button findParcelButton = new Button("Vyhľadať parcelu");
        findParcelButton.addClickListener(e -> {
            try {
                this.binder.writeBean(this.entity);
                if (longitudeOrientation.getSelectedItem().get().equals("VÝCHOD")) {
                    this.entity.setLongitudeOrientation(true);
                } else {
                    this.entity.setLongitudeOrientation(false);
                }
                if (latitudeOrientation.getSelectedItem().get().equals("SEVER")) {
                    this.entity.setLatitudeOrientation(true);
                } else {
                    this.entity.setLatitudeOrientation(false);
                }
                najdiParcely(this.entity);
            } catch (ValidationException valEx) {
                this.entity.flush();
                CustomNotification.error("Musíte vyplniť všetky údaje a v správnom tvare");
            }
        });

        formLayout.addComponent(latLayout);
        formLayout.addComponent(longLayout);
        formLayout.addComponent(findParcelButton);

        layout.addComponent(formLayout);
        layout.addComponent(this.parcelGrid);

        this.binder.forField(latitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLatitude, CoordinatesUi::setLatitude);

        this.binder.forField(longitudeTF)
                .asRequired()
                .withConverter(new StringToDoubleConverter("Nesprávny formát desatinného čísla"))
                .withValidator(d -> d >= -180.0 && d <= 180.0, "<-180, 180>")
                .bind(CoordinatesUi::getLongitude, CoordinatesUi::setLongitude);

        this.parentComponent.showComponent(new Panel("Vyhľadanie PARCIEL podľa GPS súradníc", layout));
    }

    @Override
    public String getComponentTitle() {
        return MENU_TITLE.MENU_2.getTitle();
    }

    @Override
    public boolean isImplemented() {
        return true;
    }

    private void najdiParcely(CoordinatesUi coordinatesUi) {
        this.parcelGrid.setItems(this.parcelService.getParcelListAttCoordinates(coordinatesUi));
    }

    public Button buildNehnutelnostiButton(ParcelUiTableEntity parcelUiTableEntity) {
        Button nehnutelnostiButton = new Button("Nehnuteľnosti");

        nehnutelnostiButton.addClickListener(e -> {
            HandledOperationExecutor.submit(() -> {
                VerticalLayout vl = new VerticalLayout();
                Grid<RealEstateUiTableEntity> realEstateUiTableEntityGrid;

                realEstateUiTableEntityGrid = new Grid<>();
                realEstateUiTableEntityGrid.setWidth("100%");
                realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getInternalId).setCaption("#");
                realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getSupisneCislo).setCaption("Súpisné číslo");
                realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getPopis).setCaption("Popis nehnuteľnosti");
                realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getLatitude).setCaption("Zem. výška");
                realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getLongtitude).setCaption("Zem. šírka");
                realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getLatitudeOrientation).setCaption("Orientácia výšky");
                realEstateUiTableEntityGrid.addColumn(RealEstateUiTableEntity::getLongitudeOrientation).setCaption("Orientácia šírky");
                realEstateUiTableEntityGrid.setItems(parcelUiTableEntity.getNehnutelnosti() == null ? new LinkedList<>() : parcelUiTableEntity.getNehnutelnosti());

                vl.addComponent(realEstateUiTableEntityGrid);

                Window modal = new Window("Nehnuteľnosti, ktoré stoja na parcele s číslom " + parcelUiTableEntity.getCisloParcely(), vl);
                modal.addStyleName("window-center");
                modal.setPositionY(100);

                UI.getCurrent().addWindow(modal);

                // Add a JS function that can be called from the client.
                JavaScript.getCurrent().addFunction("centerWindow", args -> {
                    modal.setPositionX((int) ((args.getNumber(1) - args.getNumber(0)) / 2));
                });

                // Execute the function now. In real code you might want to execute the function just after the window is displayed, probably in your enter() method.
                JavaScript.getCurrent().execute("centerWindow(document.getElementsByClassName('window-center')[0].offsetWidth, window.innerWidth)");

                return OperationMarker.OPERATION_SUCCESFUL;
            });
        });

        return nehnutelnostiButton;
    }
}
