package sk.uniza.fri.lustiak.auds2.kataster.service;

import org.springframework.beans.factory.annotation.Autowired;
import sk.uniza.fri.lustiak.auds2.kataster.entities.bo.Parcel;
import sk.uniza.fri.lustiak.auds2.kataster.entities.bo.RealEstate;
import org.springframework.stereotype.Service;
import sk.uniza.fri.lustiak.auds2.kataster.entities.ui.*;
import sk.uniza.fri.lustiak.auds2.kataster.entities.wrapper.ParcelWrapper;
import sk.uniza.fri.lustiak.auds2.kataster.entities.wrapper.RealEstateWrapper;
import sk.uniza.fri.lustiak.auds2.kataster.utils.BusinessException;
import sk.uniza.fri.lustiak.auds2.kataster.utils.OperationMarker;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo.Coordinate2D;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo.Coordinates2DKey;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo.LATITUDE_POSITION;
import sk.uniza.fri.lustyno.datastructures.entity.keys.multidimensional.geo.LONGITUDE_POSITION;
import sk.uniza.fri.lustyno.datastructures.kdtree.KDTree;
import sk.uniza.fri.lustyno.datastructures.kdtree.KDTreeNodeKeyDataWrapper;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

@Service
public class RealEstateService {

    private final String VSETKY_NEHNUTELNOSTI_FILE           = "fileResources/vsetky_nehnutelnosti.csv";

    @Autowired
    private EntityMapperService entityMapperService;

    @Autowired
    private ParcelService parcelService;

    private KDTree<Coordinates2DKey, RealEstate, Coordinate2D> realEstates = new KDTree<>(2);

    public void clearStoredItems() {
        this.realEstates = new KDTree<>(2);
    }

    public void vycistiSubor() {
        try {
            PrintWriter pw = new PrintWriter(VSETKY_NEHNUTELNOSTI_FILE);
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public OperationMarker addNewRealEstate(CreateNewRealEstateUi createNewRealEstateUi) {
        RealEstateWrapper realEstateWrapper =
                this.entityMapperService.fromCreateNewRealEstateUi(createNewRealEstateUi);

        CoordinatesUi newRealEstateCoordinates = new CoordinatesUi(
                createNewRealEstateUi.getLatitude(), createNewRealEstateUi.getLongitude(),
                createNewRealEstateUi.getLatitudeOrientation(), createNewRealEstateUi.getLongitudeOrientation());

        // Nájdenie všetkých parciel na coords novovytváranej nehnutelnosti a každej takto nájdenej nehnutelnosti
        // pridaná referencia na novovytváranú nehnutelnosti
        List<ParcelWrapper> parcelsAtNewRealEstateCoords = this.parcelService.getParcelsAtCoordinates(newRealEstateCoordinates);
        parcelsAtNewRealEstateCoords.forEach(e -> {
            e.getParcel().pridajNehnutelnost(realEstateWrapper);
        });
        // Novovytváranej entite nehnutelnosti jednoducho pridat vyssie najdeny zoznam do referencii parciel
        // na ktorých nehnutelnost stojí - nie "add", pretoze je to uplne nova entita
        realEstateWrapper.getRealEstate().setZoznamParciel(parcelsAtNewRealEstateCoords);

        this.realEstates.insert(realEstateWrapper.getCoordinates2DKey(), realEstateWrapper.getRealEstate());
        return OperationMarker.OPERATION_SUCCESFUL;
    }

    public List<RealEstateUiTableEntity> getRealEstatesListAtCoordinates(CoordinatesUi coordinatesUi) {
        Coordinates2DKey findByKey = this.entityMapperService.toCoordinatesKey(coordinatesUi);

        List<KDTreeNodeKeyDataWrapper<Coordinates2DKey, RealEstate>> realEstatesAtCoordinates =
                this.realEstates.findByDimensionalKey(findByKey);

        List<RealEstateUiTableEntity> realEstatesFound = new LinkedList<>();
        realEstatesAtCoordinates.forEach(e -> {
            realEstatesFound.add(this.entityMapperService.fromRealEstateNodeWrapper(e));
        });

        return realEstatesFound;
    }

    protected List<RealEstateWrapper> getRealEstatesAtCoordinates(CoordinatesUi coordinatesUi) {
        Coordinates2DKey findByKey = this.entityMapperService.toCoordinatesKey(coordinatesUi);

        List<KDTreeNodeKeyDataWrapper<Coordinates2DKey, RealEstate>> realEstatesAtCoordinates =
                this.realEstates.findByDimensionalKey(findByKey);

        List<RealEstateWrapper> realEstatesFound = new LinkedList<>();
        realEstatesAtCoordinates.forEach(e -> {
            realEstatesFound.add(new RealEstateWrapper(e.getKey(),e.getData()));
        });

        return realEstatesFound;
    }



    public List<RealEstateUiTableEntity> getRealEstatesBetweenCoordinates(CoordinatesUi fromCoordinates, CoordinatesUi toCoordinates) {
        Coordinates2DKey findByKeyFrom = this.entityMapperService.toCoordinatesKey(fromCoordinates);
        Coordinates2DKey findByKeyTo = this.entityMapperService.toCoordinatesKey(toCoordinates);

        List<KDTreeNodeKeyDataWrapper<Coordinates2DKey, RealEstate>> realEstatesAtCoordinates =
                this.realEstates.findInRange(findByKeyFrom, findByKeyTo);

        List<RealEstateUiTableEntity> realEstatesFound = new LinkedList<>();
        realEstatesAtCoordinates.forEach(e -> {
            realEstatesFound.add(this.entityMapperService.fromRealEstateNodeWrapper(e));
        });

        return realEstatesFound;
    }

    public OperationMarker editRealEstate(EditRealEstateUi editRealEstateUi) {
        Coordinates2DKey oldEntityKey = new Coordinates2DKey(
                editRealEstateUi.getOldRealEstateData().getLatitude(),
                editRealEstateUi.getOldRealEstateData().getLongtitude(),
                editRealEstateUi.getOldRealEstateData().getLatitudeOrientation().equals("SEVER") ? LATITUDE_POSITION.NORTH : LATITUDE_POSITION.SOUTH,
                editRealEstateUi.getOldRealEstateData().getLongitudeOrientation().equals("VÝCHOD") ? LONGITUDE_POSITION.EAST : LONGITUDE_POSITION.EAST,
                editRealEstateUi.getOldRealEstateData().getInternalId()
        );
        // V prípade, že editujem aj pozíciu nehnuteľnosti
        if (editRealEstateUi.getEditConstraints().equals(Boolean.TRUE)) {
            // Vymažem starú nehnuteľnosť
            KDTreeNodeKeyDataWrapper<Coordinates2DKey, RealEstate> deletedRealEstate =
                    this.realEstates.delete(oldEntityKey);
            deletedRealEstate.getData().getZoznamParciel().stream().forEach(e -> {
                e.getParcel().odoberNehnutelnost(new RealEstateWrapper(oldEntityKey, deletedRealEstate.getData()));
            });
            this.addNewRealEstate(this.entityMapperService.fromRealEstateTableUiEntity(editRealEstateUi.getNewRealEstateData()));
        }
        // V prípade že sa editujú iba voliteľné fieldy
        else {
            KDTreeNodeKeyDataWrapper<Coordinates2DKey, RealEstate> entity = this.realEstates.find(oldEntityKey);
            entity.getData().setSupisneCislo(editRealEstateUi.getNewRealEstateData().getSupisneCislo());
            entity.getData().setPopis(editRealEstateUi.getNewRealEstateData().getPopis());
        }
        return OperationMarker.OPERATION_SUCCESFUL;
    }

    public OperationMarker removeRealEstate(RealEstateUiTableEntity realEstateUiTableEntity) {
        Coordinates2DKey oldEntityKey = new Coordinates2DKey(
                realEstateUiTableEntity.getLatitude(),
                realEstateUiTableEntity.getLongtitude(),
                realEstateUiTableEntity.getLatitudeOrientation().equals("SEVER") ? LATITUDE_POSITION.NORTH : LATITUDE_POSITION.SOUTH,
                realEstateUiTableEntity.getLongitudeOrientation().equals("VÝCHOD") ? LONGITUDE_POSITION.EAST : LONGITUDE_POSITION.EAST,
                realEstateUiTableEntity.getInternalId()
        );
        KDTreeNodeKeyDataWrapper<Coordinates2DKey, RealEstate> deletedRealEstate = this.realEstates.delete(oldEntityKey);
        deletedRealEstate.getData().getZoznamParciel().forEach(e -> {
            e.getParcel().odoberNehnutelnost(new RealEstateWrapper(deletedRealEstate.getKey(), deletedRealEstate.getData()));
        });
        return OperationMarker.OPERATION_SUCCESFUL;
    }

    public void ulozVsetkyNehnutelnosti() {
        List<KDTreeNodeKeyDataWrapper<Coordinates2DKey, RealEstate>> levelOrderOfData = this.realEstates.listOfDataByLevelOrder();

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(VSETKY_NEHNUTELNOSTI_FILE));

            for (KDTreeNodeKeyDataWrapper<Coordinates2DKey, RealEstate> realEstateNode : levelOrderOfData) {
                StringBuilder sb = new StringBuilder();

                sb.append(realEstateNode.getKey().getValue().getLatitudeValue() + ",");
                sb.append(realEstateNode.getKey().getValue().getLongitudeValue() + ",");
                sb.append(realEstateNode.getData().getSupisneCislo() + ",");
                sb.append(realEstateNode.getData().getPopis());

                bw.write(sb.toString());
                bw.newLine();
            }

        } catch (IOException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na zápis nehnuteľností");
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        }

    }

    public void nacitajNehnutelnostiZoSuboru() {
        this.clearStoredItems();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(VSETKY_NEHNUTELNOSTI_FILE));

            String line = null;
            while ((line = br.readLine()) != null) {

                String[] splitted = line.split(",", -1);

                Double latitude  = Double.valueOf(splitted[0]);
                Double longitude = Double.valueOf(splitted[1]);
                Integer supisneCislo = Integer.parseInt(splitted[2]);
                String popis = splitted[3];

                CreateNewRealEstateUi createNewRealEstateUi = new CreateNewRealEstateUi();
                createNewRealEstateUi.setSupisneCislo(supisneCislo);
                createNewRealEstateUi.setPopisNehnutelnosti(popis);
                createNewRealEstateUi.setLatitude(latitude);
                createNewRealEstateUi.setLongitude(longitude);
                createNewRealEstateUi.setLatitudeOrientation(latitude >= 0d ? Boolean.TRUE : Boolean.FALSE);
                createNewRealEstateUi.setLongitudeOrientation(longitude >= 0d ? Boolean.TRUE : Boolean.FALSE);
                this.addNewRealEstate(createNewRealEstateUi);
            }
        } catch (IOException e) {
            throw new BusinessException("Nepodarilo sa otvoriť súbor na načítanie dát o nehnuteľnostiach");
        }
    }

    public void vypisNehnutelnostiNaKonzolu() {
        System.out.println("Nehnuteľnosti\n");
        this.realEstates.listOfDataByLevelOrder().forEach(e -> {
            System.out.println("\t-Nehnuteľnosť: #" + e.getKey().getValue().getUniqueIdentifier() +
                    ", LAT: " + e.getKey().getValue().getLatitudeValue() +
                    ", LON: " + e.getKey().getValue().getLongitudeValue());
        });
    }

}
