package sk.uniza.fri.lustyno.AudsLetisko.entities_ui;

import lombok.Data;

@Data
public class VyhladanieLietadlaUiEntity {

    private String medzinarodnyKodLietadla;

    private Integer idDrahy;

    private String vyrobca;

    public void flush() {
        this.medzinarodnyKodLietadla = "";
        this.vyrobca = "";
        this.idDrahy = null;
    }

}
