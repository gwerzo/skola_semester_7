package sk.uniza.fri.lustyno.fripd.entities_ui.vehicle;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class VozidloEntityUi {

    private String VIN;
    private String EVC;
    private String vPatrani;
    private String datumPlatnostiSTK;
    private String datumPlatnostiEK;
    private String pocetNaprav;
    private String prevadzkovaHmostnost;

}
