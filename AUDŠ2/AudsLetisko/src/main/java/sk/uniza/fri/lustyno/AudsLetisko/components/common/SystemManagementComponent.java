package sk.uniza.fri.lustyno.AudsLetisko.components.common;

import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class SystemManagementComponent extends VerticalLayout {

    public SystemManagementComponent() {
        initComponents();
    }

    private void initComponents() {

        Label lMain = new Label("Funkcionalita manažmentu systému:");

        Label lEmpty = new Label();

        Label l1 = new Label("1. Pridanie novej odletovej dráhy ľubovoľnej dĺžky");
        Label l2 = new Label("2. Zmena času v systéme");
        Label l3 = new Label("3. Export systému do súboru");
        Label l4 = new Label("4. Import systému zo súboru");
        Label l5 = new Label("5. Generovanie náhodných dát do systému");
        Label l6 = new Label("6. Zobrazenie všetkých lietadiel v systéme");
        Label l7 = new Label("7. Zobrazenie všetkých dráh v systéme");

        l1.setWidth("100%");
        l2.setWidth("100%");
        l3.setWidth("100%");
        l4.setWidth("100%");
        l5.setWidth("100%");
        l6.setWidth("100%");
        l7.setWidth("100%");
        lMain.setWidth("100%");
        lEmpty.setWidth("100%");

        VerticalLayout labelsWrapper = new VerticalLayout(
                lMain,
                lEmpty,
                l1,
                l2,
                l3,
                l4,
                l5,
                l6,
                l7);

        Panel descPanel = new Panel(
                "Manažment systému",
                labelsWrapper
        );

        addComponent(descPanel);
    }
}
