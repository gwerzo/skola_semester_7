﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DisertationThemes.Database
{
    public enum ResearchType
    {
        BasicResearch,
        AppliedResearch,
        AppliedResearchExpDevelopment,
        Unspecified
    }
}
